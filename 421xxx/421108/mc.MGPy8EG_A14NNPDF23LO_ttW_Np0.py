#based on 410066
#include('MadGraphControl/MadGraphControl_ttV_LO_Pythia8_A14_CKKWLkTMerge.py')
from MadGraphControl.MadGraphUtils import *

# General settings
minevents=5000
nevents=30000
mode=0

# MG Particle cuts
mllcut=-1

# MG merging settings
maxjetflavor=5
ickkw=0


# Pythia8 merging settings
nJetMax=2
ktdurham=30
dparameter=0.4

name='ttW_Np0'
keyword=['SM','ttW'] 

process = """
import model sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define w = w+ w-
define q = u c d s b
define q~ = u~ c~ d~ s~ b~
define zonshell = u c d s b vl
define zonshell~ = u~ c~ d~ s~ b~ vl~
generate p p > t t~ w
output -f"""

process_dir = new_process(process)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

pdflabel="nn23lo1"

#Fetch default LO run_card.dat and set parameters
settings = { 'lhe_version'  : '3.0',
           'cut_decays'   : 'F', 
           'pdlabel'      : "'"+pdflabel+"'",
           'maxjetflavor' : maxjetflavor,
           'asrwgtflavor' : maxjetflavor,
           'ickkw'        : 0,
           'ptj'          : 20,
           'ptb'          : 20,
           'mmll'         : mllcut,      
           'mmjj'         : 0,
           'drjj'         : 0,
           'drll'         : 0,
           'drjl'         : 0.4,
           'ptl'          : 0,
           'etal'         : 10,
           'etab'         : 6,
           'etaj'         : 6,
           'ktdurham'     : ktdurham,    
           'dparameter'   : dparameter,
           'nevents'      :int(nevents)  }

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)  

#### Shower 
evgenConfig.description = 'MadGraph_'+str(name)
evgenConfig.keywords+=keyword 
evgenConfig.nEventsPerJob = minevents

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")


#PYTHIA8_TMS=ktdurham
#PYTHIA8_nJetMax=nJetMax
#PYTHIA8_Dparameter=dparameter
#PYTHIA8_Process=process
#PYTHIA8_nQuarksMerge=maxjetflavor
#include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")
#genSeq.Pythia8.Commands+=["Merging:mayRemoveDecayProducts=on"]

