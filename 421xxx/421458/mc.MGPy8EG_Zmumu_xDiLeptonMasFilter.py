# based on 421417 only filter move to xAOD format
from MadGraphControl.MadGraphUtils import *

nevents = runArgs.maxEvents*40. if runArgs.maxEvents>0 else 40.*evgenConfig.nEventsPerJob
gridpack_mode=False
 
process_def = """
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
generate p p > mu+ mu-
output -f"""

#Fetch default LO run_card.dat and set parameters
settings = { 'lhe_version':'3.0', 
             'cut_decays':'F', 
             'pdlabel':"'nn23lo1'",
             'mmll':40,
             'use_syst':"False",
             'nevents':int(nevents)}
    
process_dir = new_process(process_def)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)
# saveProcDir=True for testing only
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

#### Shower 
evgenConfig.description = 'MadGraph_Zmumu'
evgenConfig.keywords+=['Z','jets']
evgenConfig.contact  = [ "zach.marshall@cern.ch","giancarlo.panizzo@cern.ch" ]
evgenConfig.generators     += ["MadGraph"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# Filter the di-muon J/psi decays
include('GeneratorFilters/xAODDiLeptonMassFilter_Common.py')
filtSeq.xAODDiLeptonMassFilter.MinPt = 3500
filtSeq.xAODDiLeptonMassFilter.MaxEta = 2.7
filtSeq.xAODDiLeptonMassFilter.MinMass = 1000
filtSeq.xAODDiLeptonMassFilter.MaxMass = 45000
filtSeq.xAODDiLeptonMassFilter.MinDilepPt = -1
filtSeq.xAODDiLeptonMassFilter.AllowElecMu =  False
filtSeq.xAODDiLeptonMassFilter.AllowSameCharge = False

