
##############################################################################
# Job options for Pythia8B_i generation of bb->muD*
##############################################################################
evgenConfig.description = "Signal bb->muD*"
evgenConfig.keywords = ["bottom","exclusive","muon"]
evgenConfig.contact = [ 'gladilin@mail.cern.ch' ]
evgenConfig.process = "pp->bb->muD*"
#evgenConfig.minevents = 200

include("Pythia8B_i/Pythia8B_A14_NNPDF23LO_EvtGen_Common.py")

#decayfile_str = "/data/vtudorac/Rel21.6_Harmonization/run/200/2022inclusive_BELLE.dec"
#genSeq.EvtInclusiveDecay.decayFile = decayfile_str

# Hard process
genSeq.Pythia8B.Commands += ['HardQCD:all = on'] # Equivalent of MSEL1
genSeq.Pythia8B.Commands += ['HadronLevel:all = off']
#
genSeq.Pythia8B.SelectBQuarks = True
genSeq.Pythia8B.SelectCQuarks = False
#
genSeq.Pythia8B.VetoDoubleBEvents = False
genSeq.Pythia8B.VetoDoubleCEvents = False

#
# Event selection
#
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 8.']
genSeq.Pythia8B.QuarkPtCut = 8.0
genSeq.Pythia8B.AntiQuarkPtCut = 8.0
genSeq.Pythia8B.QuarkEtaCut = 3.5
genSeq.Pythia8B.AntiQuarkEtaCut = 3.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = False

genSeq.Pythia8B.OutputLevel = INFO
genSeq.Pythia8B.NHadronizationLoops = 40

if not hasattr(filtSeq, "MuDstarFilter"):
    from GeneratorFilters.GeneratorFiltersConf import MuDstarFilter
    filtSeq += MuDstarFilter("MuDstarFilter")

filtSeq.MuDstarFilter.PtMinMuon = 2500.
filtSeq.MuDstarFilter.PtMaxMuon = 1e9
filtSeq.MuDstarFilter.EtaRangeMuon = 2.7
filtSeq.MuDstarFilter.PtMinDstar = 4500.
filtSeq.MuDstarFilter.PtMaxDstar = 1e9
filtSeq.MuDstarFilter.EtaRangeDstar = 2.7
filtSeq.MuDstarFilter.RxyMinDstar = -1e9
filtSeq.MuDstarFilter.PtMinPis = 450.
filtSeq.MuDstarFilter.PtMaxPis = 1e9
filtSeq.MuDstarFilter.EtaRangePis = 2.7
filtSeq.MuDstarFilter.D0Kpi_only = False
filtSeq.MuDstarFilter.PtMinKpi = 900.
filtSeq.MuDstarFilter.PtMinKpi = 500.
filtSeq.MuDstarFilter.PtMaxKpi = 1e9
filtSeq.MuDstarFilter.EtaRangeKpi = 2.7
filtSeq.MuDstarFilter.mKpiMin = 1665.
filtSeq.MuDstarFilter.mKpiMax = 2065.
filtSeq.MuDstarFilter.delta_m_Max = 220.
filtSeq.MuDstarFilter.DstarMu_m_Max = 12000.

filtSeq.MuDstarFilter.OutputLevel = INFO
