from MadGraphControl.MadGraphUtils import *


mode=0
#---------------------------------------------------------------------------------------------------
# Setting parameters for param_card.dat
#---------------------------------------------------------------------------------------------------
parameters = {}
parameters['NEW'] = {'CV':  '1.0000000',  # CV
                     'C2V': '1.0000000',  # C2V
                     'C3':  '1.0000000'}  # C3

#---------------------------------------------------------------------------------------------------
# Setting higgs mass to 125 GeV for param_card.dat
#---------------------------------------------------------------------------------------------------
higgsMass={'25':'1.250000e+02'} #MH

#---------------------------------------------------------------------------------------------------
# Setting some parameters for run_card.dat
#---------------------------------------------------------------------------------------------------
extras = { 'lhe_version':'2.0', 
           'cut_decays':'F', 
           'pdlabel':"'nn23lo1'",
           'scale':'125',
           'dsqrt_q2fact1':'125',
           'dsqrt_q2fact2':'125'}

#---------------------------------------------------------------------------------------------------
# Generating di-higgs through Heavy Higgs resonance with MadGraph
# Due to the parameters settings above, the resonant contribution is eliminated
# Thus, the non-resonant hh production with different trilinear Higgs couplings is enabled
#---------------------------------------------------------------------------------------------------
fcard = open('proc_card_mg5.dat','w')
fcard.write("""
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
import model /cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/HHVBF_UFO
generate p p > h h j j $$ z w+ w- / a j QED=4
output -f""")
fcard.close()

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------------------------------
# Filter efficiency is ~50%
# Thus, setting the number of generated events to 3 times maxEvents,
# to avoid crashing due to not having enough events
# Also putting protection to avoid from crashing when maxEvents=-1
#---------------------------------------------------------------------------------------------------
safefactor=3
nevents=10000*safefactor
if runArgs.maxEvents > 0:
    nevents=runArgs.maxEvents*safefactor

process_dir = new_process()
#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the run_card
# Build a new run_card.dat from an existing one
# Using the values given in "extras" above for the selected parameters when setting up the run_card
# If not set in "extras", default values are used 
#---------------------------------------------------------------------------------------------------
build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',
               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,xqcut=0,extras=extras)



#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# Build a new param_card.dat from an existing one
# Used values given in "parameters" for MHH and WHH, if not set there, default values are used
# Higgs mass is set to 125 GeV by "higgsMass"
#---------------------------------------------------------------------------------------------------
build_param_card(param_card_old='%s/Cards/param_card.dat' % process_dir,param_card_new='param_card_new.dat',masses=higgsMass,params=parameters)
   
print_cards()
    
runName='run_01'     

#process_dir = new_process()
generate(run_card_loc='run_card.dat',param_card_loc='param_card_new.dat',mode=mode,proc_dir=process_dir,run_name=runName)

arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz')

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators    += ["MadGraph", "Herwig7"]
evgenConfig.description    = "Non-resonant LO di-Higgs production through vector-boson-fusion (VBF) which decays to bbtautau 2L."
evgenConfig.keywords       = ["hh", "SM", "SMHiggs", "nonResonant", "bbbar","bottom","lepton","VBF"]
evgenConfig.contact        = ['Stefano Manzoni <stefano.manzoni@cern.ch']
evgenConfig.tune = "H7.1-Default"
evgenConfig.minevents = 10000
runArgs.inputGeneratorFile=runName+'._00001.events.tar.gz'


#--------------------------------------------------------------
# Herwig 7 showering setup
#--------------------------------------------------------------
# initialize Herwig7 generator configuration for showering

include("Herwig7_i/Herwig7_LHEF.py")

# configure Herwig7
Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop Yes")
Herwig7Config.me_pdf_commands(order="LO", name="NNPDF23_lo_as_0130_qed")
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order="LO")


# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

#HW7 settings and Higgs BR
Herwig7Config.add_commands ("""
#set /Herwig/Shower/KinematicsReconstructor:ReconstructionOption General
#set /Herwig/Shower/KinematicsReconstructor:InitialInitialBoostOption LongTransBoost
set /Herwig/Shower/LtoLGammaSudakov:pTmin 0.000001
set /Herwig/Shower/QtoGammaQSudakov:Alpha /Herwig/Shower/AlphaQED
set /Herwig/Shower/ShowerHandler:SpinCorrelations Yes 
do /Herwig/Particles/h0:SelectDecayModes h0->tau-,tau+; h0->b,bbar;
set /Herwig/Particles/h0/h0->b,bbar;:BranchingRatio  0.5
set /Herwig/Particles/h0/h0->tau-,tau+;:BranchingRatio  0.5
do /Herwig/Particles/h0:PrintDecayModes
do /Herwig/Particles/tau-:SelectDecayModes tau-->nu_tau,nu_ebar,e-; tau-->nu_tau,nu_mubar,mu-;
do /Herwig/Particles/tau-:PrintDecayModes
do /Herwig/Particles/tau+:SelectDecayModes tau+->nu_taubar,nu_e,e+; tau+->nu_taubar,nu_mu,mu+;
do /Herwig/Particles/tau+:PrintDecayModes
""")


#---------------------------------------------------------------------------------------------------
# Generator Filters
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("hbbFilter", PDGParent = [25], PDGChild = [5])
filtSeq += ParentChildFilter("htautauFilter", PDGParent = [25], PDGChild = [15])
filtSeq.Expression = "hbbFilter and htautauFilter"

# run Herwig7
Herwig7Config.run()
