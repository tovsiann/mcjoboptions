evgenConfig.nEventsPerJob    = 10000
evgenConfig.contact     = ['alessio.pizzini@cern.ch','olga.bylund@cern.ch' ]
evgenConfig.generators  += ['aMcAtNlo','Pythia8','EvtGen']
evgenConfig.description = 'Pythia8 showering JO for aMC@NLO lnulnubb production at NLO'
evgenConfig.keywords+=['ttbar','jets']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")


