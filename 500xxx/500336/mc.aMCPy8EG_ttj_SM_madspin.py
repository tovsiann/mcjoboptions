from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *
from MadGraphControl.MadGraphParamHelpers import *

# --------------------------------------------------------------
# Metadata 
# --------------------------------------------------------------

evgenConfig.contact = [ 'alexander.basan@cern.ch', 'baptiste.ravina@cern.ch', 'peter.berta@cern.ch' ]
evgenConfig.nEventsPerJob = 2000

# --------------------------------------------------------------
# Setting up the process 
# --------------------------------------------------------------

process ='''
import model loop_sm-no_b_mass
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
generate p p > t t~ j [QCD]
output -f'''

process_dir = new_process(process)

# --------------------------------------------------------------
# run_card
# --------------------------------------------------------------
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob
nevents = 2.5* nevents # account for filter efficiency
settings = {
    'nevents':nevents,
    'maxjetflavor': '5',
    'parton_shower':'PYTHIA8',
    'ptj':'70.0',
    'jetalgo':'-1',
    'jetradius' : '0.4',
    'fixed_ren_scale' : 'True',
    'fixed_fac_scale' : 'True',
    'muR_ref_fixed' : '172.5',
    'muF_ref_fixed' : '172.5',
}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

# --------------------------------------------------------------
#  param_card
# --------------------------------------------------------------
# Set ATLAS SM parameters
set_top_params(process_dir=process_dir,mTop=172.5,FourFS=False)


# --------------------------------------------------------------
# madspin_card
# --------------------------------------------------------------
madspin_card=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card,'w')
mscard.write('''#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
# set BW_cut 15                # cut on how far the particle can be off-shell
set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
# specify the decay for the final state particles
decay t > w+ b, w+ > all all
decay t~ > w- b~, w- > all all
decay w+ > all all
decay w- > all all
decay z > all all
# running the actual code
launch'''%runArgs.randomSeed)
mscard.close()

# --------------------------------------------------------------
# cuts.f
# --------------------------------------------------------------
# Require at least one top quark with pT > 300 GeV

cuts_f = process_dir+'/SubProcesses/cuts.f'
c_f = open(cuts_f)
contents = c_f.readlines()
c_f.close()

for iLine, content in enumerate(contents):
    if 'PUT HERE YOUR USER-DEFINED CUTS' in content :
        line=iLine+4
        break

cuttext=''
cuttext+='      if ( passcuts_user.eqv..true. ) then\n'
cuttext+='         passcuts_user=.false.\n'
cuttext+='         do i=1,nexternal ! loop over all external particles\n'
cuttext+='            if ( istatus(i).eq.1 .and. abs(ipdg(i)).eq.6 ) then\n'
cuttext+='               if( p(1,i)**2 + p(2,i)**2 .gt. 300d0**2 ) then\n'
cuttext+='                  passcuts_user=.true.\n'
cuttext+='               endif\n'
cuttext+='            endif\n'
cuttext+='         enddo\n'
cuttext+='      endif\n'

contents.insert( line, cuttext )
contents=''.join(contents)
c_f = open( cuts_f,'w')
c_f.write(contents)
c_f.close()

# --------------------------------------------------------------
# Generate
# --------------------------------------------------------------
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

# --------------------------------------------------------------
# Run Pythia 8 Showering
# --------------------------------------------------------------
evgenConfig.description = 'aMcAtNlo_ttbar'
evgenConfig.keywords+=['ttbar','jets']
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_aMcAtNlo.py')

# --------------------------------------------------------------
# Apply TTbarWToLeptonFilter
# --------------------------------------------------------------
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 1
filtSeq.TTbarWToLeptonFilter.Ptcut      = 0.0
