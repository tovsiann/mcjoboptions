mH = 125
mfd2 = 25
mfd1 = 4
mZd = 6000
nGamma = 2
avgtau = 600
decayMode = 'normal'
include("MadGraphControl_A14N23LO_FRVZdisplaced_zh.py")
evgenConfig.nEventsPerJob=2000
evgenConfig.keywords = ["exotic", "BSMHiggs", "BSM", "darkPhoton"]
