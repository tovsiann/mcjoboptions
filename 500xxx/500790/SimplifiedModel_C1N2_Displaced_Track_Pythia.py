# Generator transform pre-include
#  Gets us ready for on-the-fly SUSY SM generation
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )


def MassToFloat(s):
  if "p" in s:
    return float(s.replace("p", "."))
  return float(s)


# Set masses based on physics short
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
#print("INFO \n"*10)
#print(phys_short)

# special to handle MadSpin configuration via JO name:
madspindecays = False #"MadSpin" in phys_short

# max number of jets will be two, unless otherwise specified.
njets=2


#--------------------------------------------------------------
# MadGraph options
# 
#run_settings={}
run_settings['ptj']=10       # low matching scale, following DM group recommendations
run_settings['ptl']=0
run_settings['etal']='-1.0'
run_settings['drll']=0.0
run_settings['drjl']=0.0
run_settings['lhe_version']='3.0'
run_settings['auto_ptj_mjj']='F'
run_settings['bwcutoff']=1000 # to allow very low-mass W* and Z*
run_settings['event_norm']='sum'
run_settings["time_of_flight"] = 0.0 #mm
#run_settings['use_syst']='F'
#run_settings['sys_alpsfact']='1 0.5 2'
#run_settings['sys_scalefact']='1 0.5 2'
#run_settings['sys_pdf']='NNPDF30_nlo_as_0118'

# use minimum lead jet pT when applying MET filter
run_settings['ptj1min']=50
ktdurham  = 15         # low matching scale, following DM group recommendations

#--------------------------------------------------------------
# MadGraph configuration
#--------------------------------------------------------------

include("width_table.py")
mass_string = phys_short.split('_N2C1pm_')[1]
mass_string = mass_string.split('_MET75')[0]
mass_string = mass_string.replace(".py","")
mass_list = mass_string.split("_")
mass_list = [MassToFloat(tmp_str) for tmp_str in mass_list]
n2_mass, c1_mass, n1_mass = mass_list

masses['1000022'] = n1_mass

# C1 relevant
masses['1000024'] = c1_mass
c1_dict = chargino_width_GeV[(c1_mass,n1_mass)]
decays["1000024"]="""DECAY   1000024     %E   # chargino1+ decays
#          BR        NDA      ID1           ID2      ID3  
     0.00000000E+00   2     1000022          24  # BR(~chi_1+ -> ~chi_10  W+)
     %.8E   2     1000022         211      # BR(~chi_1+ -> ~chi_10  pi+)
     %.8E   3     1000022         211      111 # BR(~chi_1+ -> ~chi_10  pi+)
     %.8E   3     1000022         -11      12  # BR(~chi_1+ -> ~chi_10  e+ nu_e)
     %.8E   3     1000022         -13      14  # BR(~chi_1+ -> ~chi_10  mu+ nu_mu)
     0.00000000E+00   3     1000022         -15      16  # BR(~chi_1+ -> ~chi_10  tau+ nu_tau)
#
"""%(c1_dict["width"], c1_dict["BR_1pi"], c1_dict["BR_2pi"], c1_dict["BR_e"], c1_dict["BR_mu"])
# N2 relevant

masses['1000023'] = n2_mass
n2_dict = neutralino2_width_GeV[(n2_mass,n1_mass)]
decays["1000023"]="""DECAY   1000023     %E   # neutralino2 decays
#          BR         NDA      ID1         ID2     ID3
     0.00000000E+00    2     1000022        23   # BR(~chi_20 -> ~chi_10 Z)
     %.8E    2     1000022        111           # BR(~chi_20 -> ~chi_10 u ub)
     %.8E    3     1000022        211       -211    # BR(~chi_20 -> ~chi_10 d db)
     %.8E    3     1000022        11      -11   # BR(~chi_20 -> ~chi_10 e+ e-)
     %.8E    3     1000022        13      -13   # BR(~chi_20 -> ~chi_10 mu+ mu-)
     %.8E    3     1000022        12      -12   # BR(~chi_20 -> ~chi_10 nu_e nu_e)
     %.8E    3     1000022        14      -14   # BR(~chi_20 -> ~chi_10 nu_mu nu_mu)
     %.8E    3     1000022        16      -16   # BR(~chi_20 -> ~chi_10 nu_tau nu_tau)
     0.00000000E+00    3     1000022        15      -15   # BR(~chi_20 -> ~chi_10 tau+ tau-)
"""%(n2_dict["width"], n2_dict["BR_1pi"], n2_dict["BR_2pi"], n2_dict["BR_e"], n2_dict["BR_mu"], n2_dict["BR_nu"]/3.,  n2_dict["BR_nu"]/3., n2_dict["BR_nu"]/3.)

# Excluding decay to csbg for the moment
process='''
define c1 = x1+ x1-
define w = w+ w-
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define lv = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~
define f = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~ u u~ d d~ c c~ s s~ b b~  
define sleptons = el- el+ er- er+ mul- mul+ mur- mur+ ta1- ta1+ ta2- ta2+ h+ h- svt svm sve svt~ svm~ sve~
'''
#--------------------------------------------------------------
# Configure the process definitions.
# skip the 0 jet events when applying a MET filter
skip0Jet = True

# write out the actual processes.
# N2C1+-; N1C1+-; N2N1; C1C1
process += '''
generate p p > n2 x1+ j / susystrong @1 
add process p p > n2 x1+ j j / susystrong @2

add process p p > n2 x1- j / susystrong @1
add process p p > n2 x1- j j / susystrong @2

add process p p > n1 x1+ j / susystrong @1 
add process p p > n1 x1- j / susystrong @1
add process p p > n1 x1+ j j / susystrong @2
add process p p > n1 x1- j j / susystrong @2

add process p p > x1+ x1- j / susystrong @1
add process p p > x1+ x1- j j / susystrong @2

add process p p > n2 n1 j / susystrong @1
add process p p > n2 n1 j j / susystrong @2
'''

# print the process, just to confirm we got everything right
print "Final process card:"
print process  

# information about this generation
evgenLog.info('N2C1;N1C1;N2N1;C1C1 production')

evgenConfig.contact  = [ "silu@cern.ch" ]
evgenConfig.keywords += ['SUSY','gaugino', 'chargino', 'neutralino']
evgenConfig.description = 'SUSY Simplified Model with compressed chargino/neutralino production and decays via W/Z with MadGraph/MadSpin/Pythia8'

pythia = genSeq.Pythia8
pythia.Commands += ["23:mMin = 0.14"]
pythia.Commands += ["24:mMin = 0.14"]
pythia.Commands += ["-24:mMin = 0.14"]

#--------------------------------------------------------------
# Filter setup
#--------------------------------------------------------------
# MET filter alone
evt_multiplier=1
if True:#'MET100' in phys_short:
    evt_multiplier *= 5
    include ( 'GeneratorFilters/MissingEtFilter.py' )
    MissingEtFilter = filtSeq.MissingEtFilter
    filtSeq.MissingEtFilter.METCut = 75*GeV

include('MadGraphControl/SUSY_SimplifiedModel_PostInclude.py')
#--------------------------------------------------------------
# Merging options
#
if njets>0:
    genSeq.Pythia8.Commands += [ "Merging:Process = guess", "1000024:spinType = 1",
                                 "1000023:spinType = 1",
                                 "1000022:spinType = 1" ]                             
    genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']
    
    
    
    
     
