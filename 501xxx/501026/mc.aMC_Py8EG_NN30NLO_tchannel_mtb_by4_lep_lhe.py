from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphParamHelpers import set_top_params

import fileinput
import shutil
import os
import subprocess 

# General settings
nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

### PROCESS
mgproc="""define l+ = l+ ta+
define l- = l- ta-
generate p p > t b~ j $$ w+ w- [QCD]
add process p p > t~ b j $$ w+ w- [QCD]
output -f"""

name='t-channel'
process="p p > t b j"

##necessary gridpack settings
gridpack_mode=True
process_dir='madevent/'  


if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


# --------------------------------------------------------------
#  Start building the cards
# --------------------------------------------------------------


run_card_extras = {
    'pdlabel'                  : 'lhapdf',
    'lhaid'                    : 260400,
    'reweight_scale'           : True,
    'parton_shower'            :'PYTHIA8',
    'jetalgo'                  : '-1',
    'jetradius'               : '0.4',
    'ptj'                      : '0.1',
    'dynamical_scale_choice'    : '10',
        'bwcutoff'                  : '50',
        'nevents'      :int(nevents)
}


if not is_gen_from_gridpack():
  process_dir = new_process(mgproc)
else:
  process_dir = MADGRAPH_GRIDPACK_LOCATION

set_top_params(process_dir,mTop=172.5,FourFS=True)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=run_card_extras)


if is_gen_from_gridpack():
    fMadSpinCard = open(process_dir+'/Cards/madspin_card.dat','w')                    
    fMadSpinCard.write('''set Nevents_for_max_weigth 250 # number of events for the estimate of the max. weight (default: 75)
set max_weight_ps_point 1000  # number of PS to estimate the maximum for each event (default: 400)
decay t > w+ b, w+ > l+ vl
decay t~ > w- b~, w- > l- vl~
launch''')
    fMadSpinCard.close()  
    
else:
    # setscales file for the user defined dynamical scale
    fileN = process_dir+'/SubProcesses/setscales.f'
    mark  = '      elseif(dynamical_scale_choice.eq.10) then'
    rmLines = ['ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc',
               'cc      USER-DEFINED SCALE: ENTER YOUR CODE HERE                                 cc',
               'cc      to use this code you must set                                            cc',
               'cc                 dynamical_scale_choice = 10                                    cc',
               'cc      in the run_card (run_card.dat)                                           cc',
               'write(*,*) "User-defined scale not set"',
               'stop 1',
               'temp_scale_id=\'User-defined dynamical scale\' ! use a meaningful string',
               'tmp = 0',
               'cc      USER-DEFINED SCALE: END OF USER CODE                                     cc'
               ]
    
    for line in fileinput.input(fileN, inplace=1):
        toKeep = True
        for rmLine in rmLines:
            if line.find(rmLine) >= 0:
                toKeep = False
                break
        if toKeep:
            print line,
        if line.startswith(mark):
            print """
c         sum of the transverse mass divided by 4
c         m^2+pt^2=p(0)^2-p(3)^2=(p(0)+p(3))*(p(0)-p(3))
          tmp=0d0
          do i=3,4
            xm2=dot(pp(0,i),pp(0,i))
            if (xm2 < 30) then
              if(xm2.le.0.d0)xm2=0.d0
              tmp = 4d0 * sqrt(pt(pp(0,i))**2+xm2)
              temp_scale_id='4*mT(b)'
            endif
          enddo
"""


generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs,required_accuracy=0.001)
arrange_output(process_dir=process_dir,runArgs=runArgs)
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.nEventsPerJob=10000
evgenConfig.description = 'aMC@NLO+MadSpin+Pythia8+EvtGen single-top-quark t-channel (2->3) production, ME NNPDF3.04f NLO'
evgenConfig.keywords    = ['SM', 'top', 'lepton']
evgenConfig.contact     = ['olga.bylund@cern.ch' ]
evgenConfig.nEventsPerJob    = 10000
evgenConfig.generators += ['aMcAtNlo', 'Pythia8', 'EvtGen']


