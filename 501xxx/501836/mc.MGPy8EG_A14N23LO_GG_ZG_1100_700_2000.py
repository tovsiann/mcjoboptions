evt_multiplier = 25.0 
mg = 1100
mN = 700
genType = 'GG'
decayType = 'ZG2000'
shortDecayType = 'ZG'
Nctau = 2000

evgenConfig.nEventsPerJob = 5000

include ( "MadGraphControl_SimplifiedModel_GG_ZG.py" )

from GeneratorFilters.GeneratorFiltersConf import ZtoLeptonFilter
filtSeq += ZtoLeptonFilter("ZtoLeptonFilter")
filtSeq.ZtoLeptonFilter.Ptcut = 0.
filtSeq.ZtoLeptonFilter.Etacut = 10.0

from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
filtSeq += MultiMuonFilter("MultiMuonFilter")
filtSeq.MultiMuonFilter.Ptcut = 0.
filtSeq.MultiMuonFilter.Etacut = 10.0
filtSeq.MultiMuonFilter.NMuons = 2

# Add hadronic Z decay filter: semileptonic ZZ decays in final state
from GeneratorFilters.GeneratorFiltersConf import DecaysFinalStateFilter
filtSeq += DecaysFinalStateFilter()
filtSeq.DecaysFinalStateFilter.PDGAllowedParents = [23]
filtSeq.DecaysFinalStateFilter.NQuarks = 2
filtSeq.DecaysFinalStateFilter.NChargedLeptons = 2

evgenConfig.description = "GGM SUSY (Mg = 1100 GeV, Mchi = 700 GeV, ctau = 2 m)"
evgenConfig.process = "gg->GG->chi chi -> mu mu f f gravitino gravitino"
evgenConfig.keywords += ["2muon","longLived","exotic"]
evgenConfig.generators += ["MadGraph","Pythia8","EvtGen"]
evgenConfig.contact = ["Nathan Bernard <nathan.rogers.bernard@cern.ch>"]

