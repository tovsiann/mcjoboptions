import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

import os
### get MC job-options filename
FIRST_DIR = (os.environ['JOBOPTSEARCHPATH']).split(":")[0]
jofiles = [f for f in os.listdir(FIRST_DIR) if (f.startswith('mc') and f.endswith('.py'))]

runNum=-1
runNumStr=str(runArgs.jobConfig)[-8:-2]

if runNumStr.isdigit():
  runNum=int(runNumStr)


#---------------------------------------------------------------------------
# General Settings
#---------------------------------------------------------------------------
mode=0 # 0: single core; 1: cluster; 2: multicore
cluster_type="condor"
cluster_queue=None
njobs=14
if mode!=2: njobs=1

gridpack_mode=False
gridpack_dir='madevent/'


#---------------------------------------------------------------------------
# Process types 
#   proc_name should be specified in the top JO for each process
#   m4lmin, m4lmax should also be specified in the top JO for each process
#---------------------------------------------------------------------------
proc_names=["Wen_Int", "Wmn_Int", "Wtn_Int"]

model=""
process=""
name=""
description=""
keywords=[]

if proc_name and proc_name in proc_names:

    if proc_name=="Wen_Int":
        process="generate p p > j j e+ ve a QCD^2==2 QED<=5\n"
        process+="add process p p > j j e- ve~ a QCD^2==2 QED<=5"
        keywords=['VBS','Photon', 'Electron', 'Interference', 'W']
        description='VBSWgamma signal interference electron channel'

    if proc_name=="Wmn_Int":
        process="generate p p > j j mu+ vm a QCD^2==2 QED<=5\n"
        process+="add process p p > j j mu- vm~ a QCD^2==2 QED<=5"
        keywords=['VBS','Photon', 'Muon', 'Interference', 'W']
        description='VBSWgamma signal interference muon channel'

    if proc_name=="Wtn_Int":
        process="generate p p > j j ta+ vt a QCD^2==2 QED<=5\n"
        process+="add process p p > j j ta- vt~ a QCD^2==2 QED<=5"
        keywords=['VBS','Photon', 'Tau', 'Interference', 'W']
        description='VBSWgamma signal interference tau channel'

else:
    raise RuntimeError("proc_name not recognised in these jobOptions!")

#---------------------------------------------------------------------------
# MG5 Proc card, based on runNumber
#---------------------------------------------------------------------------
process_str="""
set complex_mass_scheme
import model loop_qcd_qed_sm_Gmu-atlas
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
"""+process+"""
output -f
"""

#----------------------------------------------------------------------------
# Random Seed
#----------------------------------------------------------------------------
randomSeed = 1
if hasattr(runArgs,'randomSeed'): randomSeed = runArgs.randomSeed

#----------------------------------------------------------------------------
# Beam energy
#----------------------------------------------------------------------------
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = int(runArgs.ecmEnergy) / 2.
else:
    raise RunTimeError("No center of mass energy found.")

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------
safefactor = 1.1
if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:  nevents = int(runArgs.maxEvents*safefactor)
else: nevents = int(evgenConfig.nEventsPerJob*safefactor)

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------
skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents

#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------
extras = { 'dynamical_scale_choice' : '3',
	   'event_norm'    : 'average',
	   'lhe_version'   :'3.0',
           'maxjetflavor'  : 5,
           'ptj'           : 10.0,
           'ptb'           : 10.0,
           'drll':"0.1",
           'drjl':"0.1",
           'drjj':"0.1",
           'etaj'          : -1,
           'draa':"0.0",
           'dral':"0.1",
           'etaa'          : '3.0',
           'ptgmin'        : 10.0,
           'epsgamma'      :'0.1',
           'R0gamma'       :'0.1',
           'xn'            :'2',
           'isoEM'         :'T',
           'bwcutoff'      :'15',
           'nevents'       : nevents,
         }

if not is_gen_from_gridpack():
  process_dir = new_process(process_str)
else:
  process_dir = MADGRAPH_GRIDPACK_LOCATION

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------
# MG5 parameter Card
#---------------------------------------------------------------------------
params={}
modify_param_card(process_dir=process_dir,params=params)

#---------------------------------------------------------------------------
# MG5 + Pythia8 setup and process (lhe) generation
#---------------------------------------------------------------------------
try:
    generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)
except RuntimeError as rte:
    for an_arg in rte.args:
        if 'Gridpack sucessfully created' in an_arg:
            print 'Handling exception and exiting'
            theApp.finalize()
            theApp.exit()
    print 'Unhandled exception - re-raising'
    raise rte


#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

import os
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts

#### Shower

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# to turn on the dipole shower
genSeq.Pythia8.Commands += [
            "SpaceShower:dipoleRecoil = on"]

evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = description
evgenConfig.keywords+=keywords
evgenConfig.contact = ['Benjamin Honan <Benjamin.Paul.Honan@cern.ch>']

