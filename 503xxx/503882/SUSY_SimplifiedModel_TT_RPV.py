include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

# Set masses based on physics short
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()

decayflavor = phys_short.split('_')[3]

masses['1000006'] = float(phys_short.split('_')[4]) #stop 
masses['1000022'] = float(phys_short.split('_')[5].split('.')[0]) #N1
longlived = False
if (len(phys_short.split('_'))>6):
  longlived = True
  neutralinoLifetime = phys_short.split('_')[6].replace("ns","").replace(".py","").replace("p","0.")
  hbar = 6.582119514e-16
  decayWidth = hbar/float(neutralinoLifetime)
  decayWidthStr = '%e' % decayWidth
  decayStringHeader = 'DECAY   1000022  '
  header = decayStringHeader + decayWidthStr 
  evgenLog.info('lifetime of 1000022 is set to %s ns'% neutralinoLifetime)


process = '''
import model RPVMSSM_UFO
define susylq = ul ur dl dr cl cr sl sr
define susylq~ = ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~
generate    p p > t1 t1~ QED=0 RPV=0    / go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @1
add process p p > t1 t1~ j QED=0 RPV=0  / go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @2
add process p p > t1 t1~ j j QED=0 RPV=0 / go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @3
'''

if not longlived:
  decays['1000006'] = """DECAY   1000006     5.69449678E+00   # stop decays
  #          BR          NDA       ID1       ID2       ID3
        0.5000  2    1000022  6
        0.5000  2      -5    -3 
 
  """
  decays['1000022'] = """DECAY   1000022     1.0  # neutralino decays
  #          BR          NDA       ID1       ID2       ID3
        0.5000  3    6   3   5
        0.5000  3   -6  -3  -5
  #"""   

if longlived:      
  decays['1000006'] = """DECAY   1000006     5.69449678E+00   # stop decays
  #          BR          NDA       ID1       ID2       ID3
        1.0000  2    1000022  6
  """
  branchingRatios = """
  #          BR          NDA       ID1       ID2       ID3
        0.5000  3    6   3   5
        0.5000  3   -6  -3  -5
  #"""         
  decays['1000022'] = header + branchingRatios

# Set up a default event multiplier
evt_multiplier = 2

njets=2

usePMGSettings = False

evgenConfig.contact  = ["emily.anne.thompson@cern.ch"]

if not longlived:
  evgenConfig.keywords += [ 'SUSY', 'RPV', 'stop', 'simplifiedModel', 'neutralino']
  evgenConfig.description = 'stop pair production and decay via RPV lamba323, or decay to top quarks and neutralino, which then decays via RPV lambda323, m_stop = %s GeV, m_N1 = %s GeV'%(masses['1000006'],masses['1000022'])

if longlived:
  evgenConfig.keywords += [ 'SUSY', 'RPV', 'stop', 'simplifiedModel', 'neutralino', 'longLived']
  evgenConfig.description = 'stop pair production and decay to top quarks and long-lived neutralino, which then decays via RPV lambda323, m_stop = %s GeV, m_N1 = %s GeV'%(masses['1000006'],masses['1000022'])
  testSeq.TestHepMC.MaxVtxDisp = 1e8 # in mm
  testSeq.TestHepMC.MaxTransVtxDisp = 1e8

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

genSeq.Pythia8.Commands += ["Merging:Process = pp>{t1,1000006}{t1~,1000006}"]


