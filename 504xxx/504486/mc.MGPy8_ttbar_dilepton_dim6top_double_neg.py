from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short

# --------------------------------------------------------------
# Metadata 
# --------------------------------------------------------------
evgenConfig.description   = 'dileptonic ttbar with SMEFTatNLO-NLO'
evgenConfig.generators    = ['MadGraph', 'Pythia8', 'EvtGen']
evgenConfig.keywords     += ['ttbar','dim6top']
evgenConfig.contact       = ['baptiste.ravina@cern.ch']
evgenConfig.nEventsPerJob = 10000

# --------------------------------------------------------------
# Setting up the process 
# --------------------------------------------------------------
jo_name = get_physics_short()

process='''
    set stdout_level DEBUG
    import model dim6top_LO_UFO
    define p = g u c d s u~ c~ d~ s~
    define l+ = e+ mu+ ta+
    define vl = ve vm vt
    define l- = e- mu- ta-
    define vl~ = ve~ vm~ vt~
    generate p p > t t~ QED=0 QCD=2 FCNC=0, (t > w+ b FCNC=0,  w+ > l+ vl FCNC=0 DIM6=0), (t~ > w- b~ FCNC=0, w- > l- vl~ FCNC=0 DIM6=0) @0 DIM6=1
    output -f
'''

process_dir = new_process(process)

# Fix f2py
modify_config_card(process_dir=process_dir,settings={'f2py_compiler':'f2py2','f2py_compiler_py2':'f2py2'})
# --------------------------------------------------------------
# run_card
# --------------------------------------------------------------
nevents = runArgs.maxEvents if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob
nevents *= 1.1 # safety factor

settings = {
    'nevents'               : nevents,
    'maxjetflavor'          : '5',
    'fixed_ren_scale'       : 'True',
    'fixed_fac_scale'       : 'True',
    'pdlabel'               : '"lhapdf"',
    'lhaid'                 : '260000',
    'use_syst'              : 'True',
    'systematics_program'   : 'systematics',
    'systematics_arguments' : ['--mur=0.5,1.0,2.0','--muf=0.5,1.0,2.0','--weight_info=MUR%(mur).1f_MUF%(muf).1f_PDF%(pdf)i_DYNSCALE%(dyn)i','--dyn=3','--pdf=errorset,13100@0,25200@0,265000@0,266000@0'],
    'ptj'                   : '0.0',
    'ptl'                   : '0.0',
    'etaj'                  : '-1.0',
    'etal'                  : '-1.0',
    'drjj'                  : '0.0',
    'drll'                  : '0.0',
    'drjl'                  : '0.0',
    'dynamical_scale_choice': '3',
}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

# --------------------------------------------------------------
#  param_card
# --------------------------------------------------------------
# Set ATLAS SM parameters
params = dict() 
params['mass'] = dict()
params['mass']['6'] = '1.725000e+02'
params['mass']['23'] = '9.118760e+01'
params['mass']['24'] = '8.039900e+01'
params['mass']['25'] = '1.250000e+02'
params['yukawa'] = dict()
params['yukawa']['6'] = '1.725000e+02'
params['DECAY'] = dict()
params['DECAY']['23'] = 'DECAY  23   2.495200e+00'
params['DECAY']['24'] = '''DECAY  24   2.085000e+00
    3.377000e-01   2   -1   2
    3.377000e-01   2   -3   4
    1.082000e-01   2  -11  12
    1.082000e-01   2  -13  14
    1.082000e-01   2  -15  16'''
params['DECAY']['25'] = 'DECAY  25   6.382339e-03'
params['sminputs'] = dict()
params['sminputs']['1'] = '1.323489e+02' # change aewm1 to restore correct W mass

modify_param_card(process_dir=process_dir,params=params)

# Reset dim6top parameters
params = dict() 
params['dim6'] = dict()
params['dim6']['1'] = '1.000000e+03'
# Write the updated param card
modify_param_card(process_dir=process_dir,params=params)

reweight_card=process_dir+'/Cards/reweight_card.dat'
reweight_card_f = open(reweight_card,'w')
reweight_card_f.write('''
launch --rwgt_info=reset_do_not_use
'''+process_dir+'/Cards/param_card.dat'+'''
launch --rwgt_info=ctq8_m2p0_cqd1_p1p5
set DIM6 55 -2.0
set DIM6 61 1.5
launch --rwgt_info=ctq8_m2p0_cqd8_p1p5
set DIM6 55 -2.0
set DIM6 54 1.5
launch --rwgt_info=ctq8_m2p0_ctq1_p1p5
set DIM6 55 -2.0
set DIM6 62 1.5
launch --rwgt_info=ctq8_m2p0_ctqqu8_p1p5
set DIM6 55 -2.0
set DIM6 76 1.5
launch --rwgt_info=ctq8_m2p0_ctqqu8i_p1p5
set DIM6 55 -2.0
set DIM6 77 1.5
launch --rwgt_info=ctq8_m2p0_ctu1_p1p5
set DIM6 55 -2.0
set DIM6 63 1.5
launch --rwgt_info=ctq8_m2p0_ctd8_p1p5
set DIM6 55 -2.0
set DIM6 57 1.5
launch --rwgt_info=ctq8_m2p0_cqq13_p1p5
set DIM6 55 -2.0
set DIM6 58 1.5
launch --rwgt_info=ctq8_m2p0_cqu8_p1p5
set DIM6 55 -2.0
set DIM6 53 1.5
launch --rwgt_info=ctq8_m2p0_cqq11_p1p5
set DIM6 55 -2.0
set DIM6 59 1.5
launch --rwgt_info=ctq8_m2p0_ctd1_p1p5
set DIM6 55 -2.0
set DIM6 64 1.5
launch --rwgt_info=ctq8_m2p0_cqtqd8i_p1p5
set DIM6 55 -2.0
set DIM6 85 1.5
launch --rwgt_info=ctq8_m2p0_ctqqu1_p1p5
set DIM6 55 -2.0
set DIM6 74 1.5
launch --rwgt_info=ctq8_m2p0_ctqqu1i_p1p5
set DIM6 55 -2.0
set DIM6 75 1.5
launch --rwgt_info=ctq8_m2p0_cqq83_p1p5
set DIM6 55 -2.0
set DIM6 51 1.5
launch --rwgt_info=ctq8_m2p0_ctu8_p1p5
set DIM6 55 -2.0
set DIM6 56 1.5
launch --rwgt_info=ctq8_m2p0_cqtqd8_p1p5
set DIM6 55 -2.0
set DIM6 84 1.5
launch --rwgt_info=ctq8_m2p0_cqtqd1_p1p5
set DIM6 55 -2.0
set DIM6 82 1.5
launch --rwgt_info=ctq8_m2p0_cqtqd1i_p1p5
set DIM6 55 -2.0
set DIM6 83 1.5
launch --rwgt_info=ctq8_m2p0_cpq3_p1p5
set DIM6 55 -2.0
set DIM6 5 1.5
launch --rwgt_info=ctq8_m2p0_cqq81_p1p5
set DIM6 55 -2.0
set DIM6 52 1.5
launch --rwgt_info=ctq8_m2p0_cqu1_p1p5
set DIM6 55 -2.0
set DIM6 60 1.5
launch --rwgt_info=ctq8_m2p0_ctg_p1p5
set DIM6 55 -2.0
set DIM6 16 1.5
launch --rwgt_info=ctq8_m2p0_ctgi_p1p5
set DIM6 55 -2.0
set DIM6 17 1.5
launch --rwgt_info=ctq8_m2p0_cbw_p1p5
set DIM6 55 -2.0
set DIM6 14 1.5
launch --rwgt_info=ctq8_m2p0_cbwi_p1p5
set DIM6 55 -2.0
set DIM6 15 1.5
launch --rwgt_info=ctq8_m2p0_ctw_p1p5
set DIM6 55 -2.0
set DIM6 10 1.5
launch --rwgt_info=ctq8_m2p0_ctwi_p1p5
set DIM6 55 -2.0
set DIM6 12 1.5
launch --rwgt_info=ctq8_m2p0_cptb_p1p5
set DIM6 55 -2.0
set DIM6 8 1.5
launch --rwgt_info=ctq8_m2p0_cptbi_p1p5
set DIM6 55 -2.0
set DIM6 9 1.5
launch --rwgt_info=cqd1_m2p0_cqd8_p1p5
set DIM6 61 -2.0
set DIM6 54 1.5
launch --rwgt_info=cqd1_m2p0_ctq1_p1p5
set DIM6 61 -2.0
set DIM6 62 1.5
launch --rwgt_info=cqd1_m2p0_ctqqu8_p1p5
set DIM6 61 -2.0
set DIM6 76 1.5
launch --rwgt_info=cqd1_m2p0_ctqqu8i_p1p5
set DIM6 61 -2.0
set DIM6 77 1.5
launch --rwgt_info=cqd1_m2p0_ctu1_p1p5
set DIM6 61 -2.0
set DIM6 63 1.5
launch --rwgt_info=cqd1_m2p0_ctd8_p1p5
set DIM6 61 -2.0
set DIM6 57 1.5
launch --rwgt_info=cqd1_m2p0_cqq13_p1p5
set DIM6 61 -2.0
set DIM6 58 1.5
launch --rwgt_info=cqd1_m2p0_cqu8_p1p5
set DIM6 61 -2.0
set DIM6 53 1.5
launch --rwgt_info=cqd1_m2p0_cqq11_p1p5
set DIM6 61 -2.0
set DIM6 59 1.5
launch --rwgt_info=cqd1_m2p0_ctd1_p1p5
set DIM6 61 -2.0
set DIM6 64 1.5
launch --rwgt_info=cqd1_m2p0_cqtqd8i_p1p5
set DIM6 61 -2.0
set DIM6 85 1.5
launch --rwgt_info=cqd1_m2p0_ctqqu1_p1p5
set DIM6 61 -2.0
set DIM6 74 1.5
launch --rwgt_info=cqd1_m2p0_ctqqu1i_p1p5
set DIM6 61 -2.0
set DIM6 75 1.5
launch --rwgt_info=cqd1_m2p0_cqq83_p1p5
set DIM6 61 -2.0
set DIM6 51 1.5
launch --rwgt_info=cqd1_m2p0_ctu8_p1p5
set DIM6 61 -2.0
set DIM6 56 1.5
launch --rwgt_info=cqd1_m2p0_cqtqd8_p1p5
set DIM6 61 -2.0
set DIM6 84 1.5
launch --rwgt_info=cqd1_m2p0_cqtqd1_p1p5
set DIM6 61 -2.0
set DIM6 82 1.5
launch --rwgt_info=cqd1_m2p0_cqtqd1i_p1p5
set DIM6 61 -2.0
set DIM6 83 1.5
launch --rwgt_info=cqd1_m2p0_cpq3_p1p5
set DIM6 61 -2.0
set DIM6 5 1.5
launch --rwgt_info=cqd1_m2p0_cqq81_p1p5
set DIM6 61 -2.0
set DIM6 52 1.5
launch --rwgt_info=cqd1_m2p0_cqu1_p1p5
set DIM6 61 -2.0
set DIM6 60 1.5
launch --rwgt_info=cqd1_m2p0_ctg_p1p5
set DIM6 61 -2.0
set DIM6 16 1.5
launch --rwgt_info=cqd1_m2p0_ctgi_p1p5
set DIM6 61 -2.0
set DIM6 17 1.5
launch --rwgt_info=cqd1_m2p0_cbw_p1p5
set DIM6 61 -2.0
set DIM6 14 1.5
launch --rwgt_info=cqd1_m2p0_cbwi_p1p5
set DIM6 61 -2.0
set DIM6 15 1.5
launch --rwgt_info=cqd1_m2p0_ctw_p1p5
set DIM6 61 -2.0
set DIM6 10 1.5
launch --rwgt_info=cqd1_m2p0_ctwi_p1p5
set DIM6 61 -2.0
set DIM6 12 1.5
launch --rwgt_info=cqd1_m2p0_cptb_p1p5
set DIM6 61 -2.0
set DIM6 8 1.5
launch --rwgt_info=cqd1_m2p0_cptbi_p1p5
set DIM6 61 -2.0
set DIM6 9 1.5
launch --rwgt_info=cqd8_m2p0_ctq1_p1p5
set DIM6 54 -2.0
set DIM6 62 1.5
launch --rwgt_info=cqd8_m2p0_ctqqu8_p1p5
set DIM6 54 -2.0
set DIM6 76 1.5
launch --rwgt_info=cqd8_m2p0_ctqqu8i_p1p5
set DIM6 54 -2.0
set DIM6 77 1.5
launch --rwgt_info=cqd8_m2p0_ctu1_p1p5
set DIM6 54 -2.0
set DIM6 63 1.5
launch --rwgt_info=cqd8_m2p0_ctd8_p1p5
set DIM6 54 -2.0
set DIM6 57 1.5
launch --rwgt_info=cqd8_m2p0_cqq13_p1p5
set DIM6 54 -2.0
set DIM6 58 1.5
launch --rwgt_info=cqd8_m2p0_cqu8_p1p5
set DIM6 54 -2.0
set DIM6 53 1.5
launch --rwgt_info=cqd8_m2p0_cqq11_p1p5
set DIM6 54 -2.0
set DIM6 59 1.5
launch --rwgt_info=cqd8_m2p0_ctd1_p1p5
set DIM6 54 -2.0
set DIM6 64 1.5
launch --rwgt_info=cqd8_m2p0_cqtqd8i_p1p5
set DIM6 54 -2.0
set DIM6 85 1.5
launch --rwgt_info=cqd8_m2p0_ctqqu1_p1p5
set DIM6 54 -2.0
set DIM6 74 1.5
launch --rwgt_info=cqd8_m2p0_ctqqu1i_p1p5
set DIM6 54 -2.0
set DIM6 75 1.5
launch --rwgt_info=cqd8_m2p0_cqq83_p1p5
set DIM6 54 -2.0
set DIM6 51 1.5
launch --rwgt_info=cqd8_m2p0_ctu8_p1p5
set DIM6 54 -2.0
set DIM6 56 1.5
launch --rwgt_info=cqd8_m2p0_cqtqd8_p1p5
set DIM6 54 -2.0
set DIM6 84 1.5
launch --rwgt_info=cqd8_m2p0_cqtqd1_p1p5
set DIM6 54 -2.0
set DIM6 82 1.5
launch --rwgt_info=cqd8_m2p0_cqtqd1i_p1p5
set DIM6 54 -2.0
set DIM6 83 1.5
launch --rwgt_info=cqd8_m2p0_cpq3_p1p5
set DIM6 54 -2.0
set DIM6 5 1.5
launch --rwgt_info=cqd8_m2p0_cqq81_p1p5
set DIM6 54 -2.0
set DIM6 52 1.5
launch --rwgt_info=cqd8_m2p0_cqu1_p1p5
set DIM6 54 -2.0
set DIM6 60 1.5
launch --rwgt_info=cqd8_m2p0_ctg_p1p5
set DIM6 54 -2.0
set DIM6 16 1.5
launch --rwgt_info=cqd8_m2p0_ctgi_p1p5
set DIM6 54 -2.0
set DIM6 17 1.5
launch --rwgt_info=cqd8_m2p0_cbw_p1p5
set DIM6 54 -2.0
set DIM6 14 1.5
launch --rwgt_info=cqd8_m2p0_cbwi_p1p5
set DIM6 54 -2.0
set DIM6 15 1.5
launch --rwgt_info=cqd8_m2p0_ctw_p1p5
set DIM6 54 -2.0
set DIM6 10 1.5
launch --rwgt_info=cqd8_m2p0_ctwi_p1p5
set DIM6 54 -2.0
set DIM6 12 1.5
launch --rwgt_info=cqd8_m2p0_cptb_p1p5
set DIM6 54 -2.0
set DIM6 8 1.5
launch --rwgt_info=cqd8_m2p0_cptbi_p1p5
set DIM6 54 -2.0
set DIM6 9 1.5
launch --rwgt_info=ctq1_m2p0_ctqqu8_p1p5
set DIM6 62 -2.0
set DIM6 76 1.5
launch --rwgt_info=ctq1_m2p0_ctqqu8i_p1p5
set DIM6 62 -2.0
set DIM6 77 1.5
launch --rwgt_info=ctq1_m2p0_ctu1_p1p5
set DIM6 62 -2.0
set DIM6 63 1.5
launch --rwgt_info=ctq1_m2p0_ctd8_p1p5
set DIM6 62 -2.0
set DIM6 57 1.5
launch --rwgt_info=ctq1_m2p0_cqq13_p1p5
set DIM6 62 -2.0
set DIM6 58 1.5
launch --rwgt_info=ctq1_m2p0_cqu8_p1p5
set DIM6 62 -2.0
set DIM6 53 1.5
launch --rwgt_info=ctq1_m2p0_cqq11_p1p5
set DIM6 62 -2.0
set DIM6 59 1.5
launch --rwgt_info=ctq1_m2p0_ctd1_p1p5
set DIM6 62 -2.0
set DIM6 64 1.5
launch --rwgt_info=ctq1_m2p0_cqtqd8i_p1p5
set DIM6 62 -2.0
set DIM6 85 1.5
launch --rwgt_info=ctq1_m2p0_ctqqu1_p1p5
set DIM6 62 -2.0
set DIM6 74 1.5
launch --rwgt_info=ctq1_m2p0_ctqqu1i_p1p5
set DIM6 62 -2.0
set DIM6 75 1.5
launch --rwgt_info=ctq1_m2p0_cqq83_p1p5
set DIM6 62 -2.0
set DIM6 51 1.5
launch --rwgt_info=ctq1_m2p0_ctu8_p1p5
set DIM6 62 -2.0
set DIM6 56 1.5
launch --rwgt_info=ctq1_m2p0_cqtqd8_p1p5
set DIM6 62 -2.0
set DIM6 84 1.5
launch --rwgt_info=ctq1_m2p0_cqtqd1_p1p5
set DIM6 62 -2.0
set DIM6 82 1.5
launch --rwgt_info=ctq1_m2p0_cqtqd1i_p1p5
set DIM6 62 -2.0
set DIM6 83 1.5
launch --rwgt_info=ctq1_m2p0_cpq3_p1p5
set DIM6 62 -2.0
set DIM6 5 1.5
launch --rwgt_info=ctq1_m2p0_cqq81_p1p5
set DIM6 62 -2.0
set DIM6 52 1.5
launch --rwgt_info=ctq1_m2p0_cqu1_p1p5
set DIM6 62 -2.0
set DIM6 60 1.5
launch --rwgt_info=ctq1_m2p0_ctg_p1p5
set DIM6 62 -2.0
set DIM6 16 1.5
launch --rwgt_info=ctq1_m2p0_ctgi_p1p5
set DIM6 62 -2.0
set DIM6 17 1.5
launch --rwgt_info=ctq1_m2p0_cbw_p1p5
set DIM6 62 -2.0
set DIM6 14 1.5
launch --rwgt_info=ctq1_m2p0_cbwi_p1p5
set DIM6 62 -2.0
set DIM6 15 1.5
launch --rwgt_info=ctq1_m2p0_ctw_p1p5
set DIM6 62 -2.0
set DIM6 10 1.5
launch --rwgt_info=ctq1_m2p0_ctwi_p1p5
set DIM6 62 -2.0
set DIM6 12 1.5
launch --rwgt_info=ctq1_m2p0_cptb_p1p5
set DIM6 62 -2.0
set DIM6 8 1.5
launch --rwgt_info=ctq1_m2p0_cptbi_p1p5
set DIM6 62 -2.0
set DIM6 9 1.5
launch --rwgt_info=ctqqu8_m2p0_ctqqu8i_p1p5
set DIM6 76 -2.0
set DIM6 77 1.5
launch --rwgt_info=ctqqu8_m2p0_ctu1_p1p5
set DIM6 76 -2.0
set DIM6 63 1.5
launch --rwgt_info=ctqqu8_m2p0_ctd8_p1p5
set DIM6 76 -2.0
set DIM6 57 1.5
launch --rwgt_info=ctqqu8_m2p0_cqq13_p1p5
set DIM6 76 -2.0
set DIM6 58 1.5
launch --rwgt_info=ctqqu8_m2p0_cqu8_p1p5
set DIM6 76 -2.0
set DIM6 53 1.5
launch --rwgt_info=ctqqu8_m2p0_cqq11_p1p5
set DIM6 76 -2.0
set DIM6 59 1.5
launch --rwgt_info=ctqqu8_m2p0_ctd1_p1p5
set DIM6 76 -2.0
set DIM6 64 1.5
launch --rwgt_info=ctqqu8_m2p0_cqtqd8i_p1p5
set DIM6 76 -2.0
set DIM6 85 1.5
launch --rwgt_info=ctqqu8_m2p0_ctqqu1_p1p5
set DIM6 76 -2.0
set DIM6 74 1.5
launch --rwgt_info=ctqqu8_m2p0_ctqqu1i_p1p5
set DIM6 76 -2.0
set DIM6 75 1.5
launch --rwgt_info=ctqqu8_m2p0_cqq83_p1p5
set DIM6 76 -2.0
set DIM6 51 1.5
launch --rwgt_info=ctqqu8_m2p0_ctu8_p1p5
set DIM6 76 -2.0
set DIM6 56 1.5
launch --rwgt_info=ctqqu8_m2p0_cqtqd8_p1p5
set DIM6 76 -2.0
set DIM6 84 1.5
launch --rwgt_info=ctqqu8_m2p0_cqtqd1_p1p5
set DIM6 76 -2.0
set DIM6 82 1.5
launch --rwgt_info=ctqqu8_m2p0_cqtqd1i_p1p5
set DIM6 76 -2.0
set DIM6 83 1.5
launch --rwgt_info=ctqqu8_m2p0_cpq3_p1p5
set DIM6 76 -2.0
set DIM6 5 1.5
launch --rwgt_info=ctqqu8_m2p0_cqq81_p1p5
set DIM6 76 -2.0
set DIM6 52 1.5
launch --rwgt_info=ctqqu8_m2p0_cqu1_p1p5
set DIM6 76 -2.0
set DIM6 60 1.5
launch --rwgt_info=ctqqu8_m2p0_ctg_p1p5
set DIM6 76 -2.0
set DIM6 16 1.5
launch --rwgt_info=ctqqu8_m2p0_ctgi_p1p5
set DIM6 76 -2.0
set DIM6 17 1.5
launch --rwgt_info=ctqqu8_m2p0_cbw_p1p5
set DIM6 76 -2.0
set DIM6 14 1.5
launch --rwgt_info=ctqqu8_m2p0_cbwi_p1p5
set DIM6 76 -2.0
set DIM6 15 1.5
launch --rwgt_info=ctqqu8_m2p0_ctw_p1p5
set DIM6 76 -2.0
set DIM6 10 1.5
launch --rwgt_info=ctqqu8_m2p0_ctwi_p1p5
set DIM6 76 -2.0
set DIM6 12 1.5
launch --rwgt_info=ctqqu8_m2p0_cptb_p1p5
set DIM6 76 -2.0
set DIM6 8 1.5
launch --rwgt_info=ctqqu8_m2p0_cptbi_p1p5
set DIM6 76 -2.0
set DIM6 9 1.5
launch --rwgt_info=ctqqu8i_m2p0_ctu1_p1p5
set DIM6 77 -2.0
set DIM6 63 1.5
launch --rwgt_info=ctqqu8i_m2p0_ctd8_p1p5
set DIM6 77 -2.0
set DIM6 57 1.5
launch --rwgt_info=ctqqu8i_m2p0_cqq13_p1p5
set DIM6 77 -2.0
set DIM6 58 1.5
launch --rwgt_info=ctqqu8i_m2p0_cqu8_p1p5
set DIM6 77 -2.0
set DIM6 53 1.5
launch --rwgt_info=ctqqu8i_m2p0_cqq11_p1p5
set DIM6 77 -2.0
set DIM6 59 1.5
launch --rwgt_info=ctqqu8i_m2p0_ctd1_p1p5
set DIM6 77 -2.0
set DIM6 64 1.5
launch --rwgt_info=ctqqu8i_m2p0_cqtqd8i_p1p5
set DIM6 77 -2.0
set DIM6 85 1.5
launch --rwgt_info=ctqqu8i_m2p0_ctqqu1_p1p5
set DIM6 77 -2.0
set DIM6 74 1.5
launch --rwgt_info=ctqqu8i_m2p0_ctqqu1i_p1p5
set DIM6 77 -2.0
set DIM6 75 1.5
launch --rwgt_info=ctqqu8i_m2p0_cqq83_p1p5
set DIM6 77 -2.0
set DIM6 51 1.5
launch --rwgt_info=ctqqu8i_m2p0_ctu8_p1p5
set DIM6 77 -2.0
set DIM6 56 1.5
launch --rwgt_info=ctqqu8i_m2p0_cqtqd8_p1p5
set DIM6 77 -2.0
set DIM6 84 1.5
launch --rwgt_info=ctqqu8i_m2p0_cqtqd1_p1p5
set DIM6 77 -2.0
set DIM6 82 1.5
launch --rwgt_info=ctqqu8i_m2p0_cqtqd1i_p1p5
set DIM6 77 -2.0
set DIM6 83 1.5
launch --rwgt_info=ctqqu8i_m2p0_cpq3_p1p5
set DIM6 77 -2.0
set DIM6 5 1.5
launch --rwgt_info=ctqqu8i_m2p0_cqq81_p1p5
set DIM6 77 -2.0
set DIM6 52 1.5
launch --rwgt_info=ctqqu8i_m2p0_cqu1_p1p5
set DIM6 77 -2.0
set DIM6 60 1.5
launch --rwgt_info=ctqqu8i_m2p0_ctg_p1p5
set DIM6 77 -2.0
set DIM6 16 1.5
launch --rwgt_info=ctqqu8i_m2p0_ctgi_p1p5
set DIM6 77 -2.0
set DIM6 17 1.5
launch --rwgt_info=ctqqu8i_m2p0_cbw_p1p5
set DIM6 77 -2.0
set DIM6 14 1.5
launch --rwgt_info=ctqqu8i_m2p0_cbwi_p1p5
set DIM6 77 -2.0
set DIM6 15 1.5
launch --rwgt_info=ctqqu8i_m2p0_ctw_p1p5
set DIM6 77 -2.0
set DIM6 10 1.5
launch --rwgt_info=ctqqu8i_m2p0_ctwi_p1p5
set DIM6 77 -2.0
set DIM6 12 1.5
launch --rwgt_info=ctqqu8i_m2p0_cptb_p1p5
set DIM6 77 -2.0
set DIM6 8 1.5
launch --rwgt_info=ctqqu8i_m2p0_cptbi_p1p5
set DIM6 77 -2.0
set DIM6 9 1.5
launch --rwgt_info=ctu1_m2p0_ctd8_p1p5
set DIM6 63 -2.0
set DIM6 57 1.5
launch --rwgt_info=ctu1_m2p0_cqq13_p1p5
set DIM6 63 -2.0
set DIM6 58 1.5
launch --rwgt_info=ctu1_m2p0_cqu8_p1p5
set DIM6 63 -2.0
set DIM6 53 1.5
launch --rwgt_info=ctu1_m2p0_cqq11_p1p5
set DIM6 63 -2.0
set DIM6 59 1.5
launch --rwgt_info=ctu1_m2p0_ctd1_p1p5
set DIM6 63 -2.0
set DIM6 64 1.5
launch --rwgt_info=ctu1_m2p0_cqtqd8i_p1p5
set DIM6 63 -2.0
set DIM6 85 1.5
launch --rwgt_info=ctu1_m2p0_ctqqu1_p1p5
set DIM6 63 -2.0
set DIM6 74 1.5
launch --rwgt_info=ctu1_m2p0_ctqqu1i_p1p5
set DIM6 63 -2.0
set DIM6 75 1.5
launch --rwgt_info=ctu1_m2p0_cqq83_p1p5
set DIM6 63 -2.0
set DIM6 51 1.5
launch --rwgt_info=ctu1_m2p0_ctu8_p1p5
set DIM6 63 -2.0
set DIM6 56 1.5
launch --rwgt_info=ctu1_m2p0_cqtqd8_p1p5
set DIM6 63 -2.0
set DIM6 84 1.5
launch --rwgt_info=ctu1_m2p0_cqtqd1_p1p5
set DIM6 63 -2.0
set DIM6 82 1.5
launch --rwgt_info=ctu1_m2p0_cqtqd1i_p1p5
set DIM6 63 -2.0
set DIM6 83 1.5
launch --rwgt_info=ctu1_m2p0_cpq3_p1p5
set DIM6 63 -2.0
set DIM6 5 1.5
launch --rwgt_info=ctu1_m2p0_cqq81_p1p5
set DIM6 63 -2.0
set DIM6 52 1.5
launch --rwgt_info=ctu1_m2p0_cqu1_p1p5
set DIM6 63 -2.0
set DIM6 60 1.5
launch --rwgt_info=ctu1_m2p0_ctg_p1p5
set DIM6 63 -2.0
set DIM6 16 1.5
launch --rwgt_info=ctu1_m2p0_ctgi_p1p5
set DIM6 63 -2.0
set DIM6 17 1.5
launch --rwgt_info=ctu1_m2p0_cbw_p1p5
set DIM6 63 -2.0
set DIM6 14 1.5
launch --rwgt_info=ctu1_m2p0_cbwi_p1p5
set DIM6 63 -2.0
set DIM6 15 1.5
launch --rwgt_info=ctu1_m2p0_ctw_p1p5
set DIM6 63 -2.0
set DIM6 10 1.5
launch --rwgt_info=ctu1_m2p0_ctwi_p1p5
set DIM6 63 -2.0
set DIM6 12 1.5
launch --rwgt_info=ctu1_m2p0_cptb_p1p5
set DIM6 63 -2.0
set DIM6 8 1.5
launch --rwgt_info=ctu1_m2p0_cptbi_p1p5
set DIM6 63 -2.0
set DIM6 9 1.5
launch --rwgt_info=ctd8_m2p0_cqq13_p1p5
set DIM6 57 -2.0
set DIM6 58 1.5
launch --rwgt_info=ctd8_m2p0_cqu8_p1p5
set DIM6 57 -2.0
set DIM6 53 1.5
launch --rwgt_info=ctd8_m2p0_cqq11_p1p5
set DIM6 57 -2.0
set DIM6 59 1.5
launch --rwgt_info=ctd8_m2p0_ctd1_p1p5
set DIM6 57 -2.0
set DIM6 64 1.5
launch --rwgt_info=ctd8_m2p0_cqtqd8i_p1p5
set DIM6 57 -2.0
set DIM6 85 1.5
launch --rwgt_info=ctd8_m2p0_ctqqu1_p1p5
set DIM6 57 -2.0
set DIM6 74 1.5
launch --rwgt_info=ctd8_m2p0_ctqqu1i_p1p5
set DIM6 57 -2.0
set DIM6 75 1.5
launch --rwgt_info=ctd8_m2p0_cqq83_p1p5
set DIM6 57 -2.0
set DIM6 51 1.5
launch --rwgt_info=ctd8_m2p0_ctu8_p1p5
set DIM6 57 -2.0
set DIM6 56 1.5
launch --rwgt_info=ctd8_m2p0_cqtqd8_p1p5
set DIM6 57 -2.0
set DIM6 84 1.5
launch --rwgt_info=ctd8_m2p0_cqtqd1_p1p5
set DIM6 57 -2.0
set DIM6 82 1.5
launch --rwgt_info=ctd8_m2p0_cqtqd1i_p1p5
set DIM6 57 -2.0
set DIM6 83 1.5
launch --rwgt_info=ctd8_m2p0_cpq3_p1p5
set DIM6 57 -2.0
set DIM6 5 1.5
launch --rwgt_info=ctd8_m2p0_cqq81_p1p5
set DIM6 57 -2.0
set DIM6 52 1.5
launch --rwgt_info=ctd8_m2p0_cqu1_p1p5
set DIM6 57 -2.0
set DIM6 60 1.5
launch --rwgt_info=ctd8_m2p0_ctg_p1p5
set DIM6 57 -2.0
set DIM6 16 1.5
launch --rwgt_info=ctd8_m2p0_ctgi_p1p5
set DIM6 57 -2.0
set DIM6 17 1.5
launch --rwgt_info=ctd8_m2p0_cbw_p1p5
set DIM6 57 -2.0
set DIM6 14 1.5
launch --rwgt_info=ctd8_m2p0_cbwi_p1p5
set DIM6 57 -2.0
set DIM6 15 1.5
launch --rwgt_info=ctd8_m2p0_ctw_p1p5
set DIM6 57 -2.0
set DIM6 10 1.5
launch --rwgt_info=ctd8_m2p0_ctwi_p1p5
set DIM6 57 -2.0
set DIM6 12 1.5
launch --rwgt_info=ctd8_m2p0_cptb_p1p5
set DIM6 57 -2.0
set DIM6 8 1.5
launch --rwgt_info=ctd8_m2p0_cptbi_p1p5
set DIM6 57 -2.0
set DIM6 9 1.5
launch --rwgt_info=cqq13_m2p0_cqu8_p1p5
set DIM6 58 -2.0
set DIM6 53 1.5
launch --rwgt_info=cqq13_m2p0_cqq11_p1p5
set DIM6 58 -2.0
set DIM6 59 1.5
launch --rwgt_info=cqq13_m2p0_ctd1_p1p5
set DIM6 58 -2.0
set DIM6 64 1.5
launch --rwgt_info=cqq13_m2p0_cqtqd8i_p1p5
set DIM6 58 -2.0
set DIM6 85 1.5
launch --rwgt_info=cqq13_m2p0_ctqqu1_p1p5
set DIM6 58 -2.0
set DIM6 74 1.5
launch --rwgt_info=cqq13_m2p0_ctqqu1i_p1p5
set DIM6 58 -2.0
set DIM6 75 1.5
launch --rwgt_info=cqq13_m2p0_cqq83_p1p5
set DIM6 58 -2.0
set DIM6 51 1.5
launch --rwgt_info=cqq13_m2p0_ctu8_p1p5
set DIM6 58 -2.0
set DIM6 56 1.5
launch --rwgt_info=cqq13_m2p0_cqtqd8_p1p5
set DIM6 58 -2.0
set DIM6 84 1.5
launch --rwgt_info=cqq13_m2p0_cqtqd1_p1p5
set DIM6 58 -2.0
set DIM6 82 1.5
launch --rwgt_info=cqq13_m2p0_cqtqd1i_p1p5
set DIM6 58 -2.0
set DIM6 83 1.5
launch --rwgt_info=cqq13_m2p0_cpq3_p1p5
set DIM6 58 -2.0
set DIM6 5 1.5
launch --rwgt_info=cqq13_m2p0_cqq81_p1p5
set DIM6 58 -2.0
set DIM6 52 1.5
launch --rwgt_info=cqq13_m2p0_cqu1_p1p5
set DIM6 58 -2.0
set DIM6 60 1.5
launch --rwgt_info=cqq13_m2p0_ctg_p1p5
set DIM6 58 -2.0
set DIM6 16 1.5
launch --rwgt_info=cqq13_m2p0_ctgi_p1p5
set DIM6 58 -2.0
set DIM6 17 1.5
launch --rwgt_info=cqq13_m2p0_cbw_p1p5
set DIM6 58 -2.0
set DIM6 14 1.5
launch --rwgt_info=cqq13_m2p0_cbwi_p1p5
set DIM6 58 -2.0
set DIM6 15 1.5
launch --rwgt_info=cqq13_m2p0_ctw_p1p5
set DIM6 58 -2.0
set DIM6 10 1.5
launch --rwgt_info=cqq13_m2p0_ctwi_p1p5
set DIM6 58 -2.0
set DIM6 12 1.5
launch --rwgt_info=cqq13_m2p0_cptb_p1p5
set DIM6 58 -2.0
set DIM6 8 1.5
launch --rwgt_info=cqq13_m2p0_cptbi_p1p5
set DIM6 58 -2.0
set DIM6 9 1.5
launch --rwgt_info=cqu8_m2p0_cqq11_p1p5
set DIM6 53 -2.0
set DIM6 59 1.5
launch --rwgt_info=cqu8_m2p0_ctd1_p1p5
set DIM6 53 -2.0
set DIM6 64 1.5
launch --rwgt_info=cqu8_m2p0_cqtqd8i_p1p5
set DIM6 53 -2.0
set DIM6 85 1.5
launch --rwgt_info=cqu8_m2p0_ctqqu1_p1p5
set DIM6 53 -2.0
set DIM6 74 1.5
launch --rwgt_info=cqu8_m2p0_ctqqu1i_p1p5
set DIM6 53 -2.0
set DIM6 75 1.5
launch --rwgt_info=cqu8_m2p0_cqq83_p1p5
set DIM6 53 -2.0
set DIM6 51 1.5
launch --rwgt_info=cqu8_m2p0_ctu8_p1p5
set DIM6 53 -2.0
set DIM6 56 1.5
launch --rwgt_info=cqu8_m2p0_cqtqd8_p1p5
set DIM6 53 -2.0
set DIM6 84 1.5
launch --rwgt_info=cqu8_m2p0_cqtqd1_p1p5
set DIM6 53 -2.0
set DIM6 82 1.5
launch --rwgt_info=cqu8_m2p0_cqtqd1i_p1p5
set DIM6 53 -2.0
set DIM6 83 1.5
launch --rwgt_info=cqu8_m2p0_cpq3_p1p5
set DIM6 53 -2.0
set DIM6 5 1.5
launch --rwgt_info=cqu8_m2p0_cqq81_p1p5
set DIM6 53 -2.0
set DIM6 52 1.5
launch --rwgt_info=cqu8_m2p0_cqu1_p1p5
set DIM6 53 -2.0
set DIM6 60 1.5
launch --rwgt_info=cqu8_m2p0_ctg_p1p5
set DIM6 53 -2.0
set DIM6 16 1.5
launch --rwgt_info=cqu8_m2p0_ctgi_p1p5
set DIM6 53 -2.0
set DIM6 17 1.5
launch --rwgt_info=cqu8_m2p0_cbw_p1p5
set DIM6 53 -2.0
set DIM6 14 1.5
launch --rwgt_info=cqu8_m2p0_cbwi_p1p5
set DIM6 53 -2.0
set DIM6 15 1.5
launch --rwgt_info=cqu8_m2p0_ctw_p1p5
set DIM6 53 -2.0
set DIM6 10 1.5
launch --rwgt_info=cqu8_m2p0_ctwi_p1p5
set DIM6 53 -2.0
set DIM6 12 1.5
launch --rwgt_info=cqu8_m2p0_cptb_p1p5
set DIM6 53 -2.0
set DIM6 8 1.5
launch --rwgt_info=cqu8_m2p0_cptbi_p1p5
set DIM6 53 -2.0
set DIM6 9 1.5
launch --rwgt_info=cqq11_m2p0_ctd1_p1p5
set DIM6 59 -2.0
set DIM6 64 1.5
launch --rwgt_info=cqq11_m2p0_cqtqd8i_p1p5
set DIM6 59 -2.0
set DIM6 85 1.5
launch --rwgt_info=cqq11_m2p0_ctqqu1_p1p5
set DIM6 59 -2.0
set DIM6 74 1.5
launch --rwgt_info=cqq11_m2p0_ctqqu1i_p1p5
set DIM6 59 -2.0
set DIM6 75 1.5
launch --rwgt_info=cqq11_m2p0_cqq83_p1p5
set DIM6 59 -2.0
set DIM6 51 1.5
launch --rwgt_info=cqq11_m2p0_ctu8_p1p5
set DIM6 59 -2.0
set DIM6 56 1.5
launch --rwgt_info=cqq11_m2p0_cqtqd8_p1p5
set DIM6 59 -2.0
set DIM6 84 1.5
launch --rwgt_info=cqq11_m2p0_cqtqd1_p1p5
set DIM6 59 -2.0
set DIM6 82 1.5
launch --rwgt_info=cqq11_m2p0_cqtqd1i_p1p5
set DIM6 59 -2.0
set DIM6 83 1.5
launch --rwgt_info=cqq11_m2p0_cpq3_p1p5
set DIM6 59 -2.0
set DIM6 5 1.5
launch --rwgt_info=cqq11_m2p0_cqq81_p1p5
set DIM6 59 -2.0
set DIM6 52 1.5
launch --rwgt_info=cqq11_m2p0_cqu1_p1p5
set DIM6 59 -2.0
set DIM6 60 1.5
launch --rwgt_info=cqq11_m2p0_ctg_p1p5
set DIM6 59 -2.0
set DIM6 16 1.5
launch --rwgt_info=cqq11_m2p0_ctgi_p1p5
set DIM6 59 -2.0
set DIM6 17 1.5
launch --rwgt_info=cqq11_m2p0_cbw_p1p5
set DIM6 59 -2.0
set DIM6 14 1.5
launch --rwgt_info=cqq11_m2p0_cbwi_p1p5
set DIM6 59 -2.0
set DIM6 15 1.5
launch --rwgt_info=cqq11_m2p0_ctw_p1p5
set DIM6 59 -2.0
set DIM6 10 1.5
launch --rwgt_info=cqq11_m2p0_ctwi_p1p5
set DIM6 59 -2.0
set DIM6 12 1.5
launch --rwgt_info=cqq11_m2p0_cptb_p1p5
set DIM6 59 -2.0
set DIM6 8 1.5
launch --rwgt_info=cqq11_m2p0_cptbi_p1p5
set DIM6 59 -2.0
set DIM6 9 1.5
launch --rwgt_info=ctd1_m2p0_cqtqd8i_p1p5
set DIM6 64 -2.0
set DIM6 85 1.5
launch --rwgt_info=ctd1_m2p0_ctqqu1_p1p5
set DIM6 64 -2.0
set DIM6 74 1.5
launch --rwgt_info=ctd1_m2p0_ctqqu1i_p1p5
set DIM6 64 -2.0
set DIM6 75 1.5
launch --rwgt_info=ctd1_m2p0_cqq83_p1p5
set DIM6 64 -2.0
set DIM6 51 1.5
launch --rwgt_info=ctd1_m2p0_ctu8_p1p5
set DIM6 64 -2.0
set DIM6 56 1.5
launch --rwgt_info=ctd1_m2p0_cqtqd8_p1p5
set DIM6 64 -2.0
set DIM6 84 1.5
launch --rwgt_info=ctd1_m2p0_cqtqd1_p1p5
set DIM6 64 -2.0
set DIM6 82 1.5
launch --rwgt_info=ctd1_m2p0_cqtqd1i_p1p5
set DIM6 64 -2.0
set DIM6 83 1.5
launch --rwgt_info=ctd1_m2p0_cpq3_p1p5
set DIM6 64 -2.0
set DIM6 5 1.5
launch --rwgt_info=ctd1_m2p0_cqq81_p1p5
set DIM6 64 -2.0
set DIM6 52 1.5
launch --rwgt_info=ctd1_m2p0_cqu1_p1p5
set DIM6 64 -2.0
set DIM6 60 1.5
launch --rwgt_info=ctd1_m2p0_ctg_p1p5
set DIM6 64 -2.0
set DIM6 16 1.5
launch --rwgt_info=ctd1_m2p0_ctgi_p1p5
set DIM6 64 -2.0
set DIM6 17 1.5
launch --rwgt_info=ctd1_m2p0_cbw_p1p5
set DIM6 64 -2.0
set DIM6 14 1.5
launch --rwgt_info=ctd1_m2p0_cbwi_p1p5
set DIM6 64 -2.0
set DIM6 15 1.5
launch --rwgt_info=ctd1_m2p0_ctw_p1p5
set DIM6 64 -2.0
set DIM6 10 1.5
launch --rwgt_info=ctd1_m2p0_ctwi_p1p5
set DIM6 64 -2.0
set DIM6 12 1.5
launch --rwgt_info=ctd1_m2p0_cptb_p1p5
set DIM6 64 -2.0
set DIM6 8 1.5
launch --rwgt_info=ctd1_m2p0_cptbi_p1p5
set DIM6 64 -2.0
set DIM6 9 1.5
launch --rwgt_info=cqtqd8i_m2p0_ctqqu1_p1p5
set DIM6 85 -2.0
set DIM6 74 1.5
launch --rwgt_info=cqtqd8i_m2p0_ctqqu1i_p1p5
set DIM6 85 -2.0
set DIM6 75 1.5
launch --rwgt_info=cqtqd8i_m2p0_cqq83_p1p5
set DIM6 85 -2.0
set DIM6 51 1.5
launch --rwgt_info=cqtqd8i_m2p0_ctu8_p1p5
set DIM6 85 -2.0
set DIM6 56 1.5
launch --rwgt_info=cqtqd8i_m2p0_cqtqd8_p1p5
set DIM6 85 -2.0
set DIM6 84 1.5
launch --rwgt_info=cqtqd8i_m2p0_cqtqd1_p1p5
set DIM6 85 -2.0
set DIM6 82 1.5
launch --rwgt_info=cqtqd8i_m2p0_cqtqd1i_p1p5
set DIM6 85 -2.0
set DIM6 83 1.5
launch --rwgt_info=cqtqd8i_m2p0_cpq3_p1p5
set DIM6 85 -2.0
set DIM6 5 1.5
launch --rwgt_info=cqtqd8i_m2p0_cqq81_p1p5
set DIM6 85 -2.0
set DIM6 52 1.5
launch --rwgt_info=cqtqd8i_m2p0_cqu1_p1p5
set DIM6 85 -2.0
set DIM6 60 1.5
launch --rwgt_info=cqtqd8i_m2p0_ctg_p1p5
set DIM6 85 -2.0
set DIM6 16 1.5
launch --rwgt_info=cqtqd8i_m2p0_ctgi_p1p5
set DIM6 85 -2.0
set DIM6 17 1.5
launch --rwgt_info=cqtqd8i_m2p0_cbw_p1p5
set DIM6 85 -2.0
set DIM6 14 1.5
launch --rwgt_info=cqtqd8i_m2p0_cbwi_p1p5
set DIM6 85 -2.0
set DIM6 15 1.5
launch --rwgt_info=cqtqd8i_m2p0_ctw_p1p5
set DIM6 85 -2.0
set DIM6 10 1.5
launch --rwgt_info=cqtqd8i_m2p0_ctwi_p1p5
set DIM6 85 -2.0
set DIM6 12 1.5
launch --rwgt_info=cqtqd8i_m2p0_cptb_p1p5
set DIM6 85 -2.0
set DIM6 8 1.5
launch --rwgt_info=cqtqd8i_m2p0_cptbi_p1p5
set DIM6 85 -2.0
set DIM6 9 1.5
launch --rwgt_info=ctqqu1_m2p0_ctqqu1i_p1p5
set DIM6 74 -2.0
set DIM6 75 1.5
launch --rwgt_info=ctqqu1_m2p0_cqq83_p1p5
set DIM6 74 -2.0
set DIM6 51 1.5
launch --rwgt_info=ctqqu1_m2p0_ctu8_p1p5
set DIM6 74 -2.0
set DIM6 56 1.5
launch --rwgt_info=ctqqu1_m2p0_cqtqd8_p1p5
set DIM6 74 -2.0
set DIM6 84 1.5
launch --rwgt_info=ctqqu1_m2p0_cqtqd1_p1p5
set DIM6 74 -2.0
set DIM6 82 1.5
launch --rwgt_info=ctqqu1_m2p0_cqtqd1i_p1p5
set DIM6 74 -2.0
set DIM6 83 1.5
launch --rwgt_info=ctqqu1_m2p0_cpq3_p1p5
set DIM6 74 -2.0
set DIM6 5 1.5
launch --rwgt_info=ctqqu1_m2p0_cqq81_p1p5
set DIM6 74 -2.0
set DIM6 52 1.5
launch --rwgt_info=ctqqu1_m2p0_cqu1_p1p5
set DIM6 74 -2.0
set DIM6 60 1.5
launch --rwgt_info=ctqqu1_m2p0_ctg_p1p5
set DIM6 74 -2.0
set DIM6 16 1.5
launch --rwgt_info=ctqqu1_m2p0_ctgi_p1p5
set DIM6 74 -2.0
set DIM6 17 1.5
launch --rwgt_info=ctqqu1_m2p0_cbw_p1p5
set DIM6 74 -2.0
set DIM6 14 1.5
launch --rwgt_info=ctqqu1_m2p0_cbwi_p1p5
set DIM6 74 -2.0
set DIM6 15 1.5
launch --rwgt_info=ctqqu1_m2p0_ctw_p1p5
set DIM6 74 -2.0
set DIM6 10 1.5
launch --rwgt_info=ctqqu1_m2p0_ctwi_p1p5
set DIM6 74 -2.0
set DIM6 12 1.5
launch --rwgt_info=ctqqu1_m2p0_cptb_p1p5
set DIM6 74 -2.0
set DIM6 8 1.5
launch --rwgt_info=ctqqu1_m2p0_cptbi_p1p5
set DIM6 74 -2.0
set DIM6 9 1.5
launch --rwgt_info=ctqqu1i_m2p0_cqq83_p1p5
set DIM6 75 -2.0
set DIM6 51 1.5
launch --rwgt_info=ctqqu1i_m2p0_ctu8_p1p5
set DIM6 75 -2.0
set DIM6 56 1.5
launch --rwgt_info=ctqqu1i_m2p0_cqtqd8_p1p5
set DIM6 75 -2.0
set DIM6 84 1.5
launch --rwgt_info=ctqqu1i_m2p0_cqtqd1_p1p5
set DIM6 75 -2.0
set DIM6 82 1.5
launch --rwgt_info=ctqqu1i_m2p0_cqtqd1i_p1p5
set DIM6 75 -2.0
set DIM6 83 1.5
launch --rwgt_info=ctqqu1i_m2p0_cpq3_p1p5
set DIM6 75 -2.0
set DIM6 5 1.5
launch --rwgt_info=ctqqu1i_m2p0_cqq81_p1p5
set DIM6 75 -2.0
set DIM6 52 1.5
launch --rwgt_info=ctqqu1i_m2p0_cqu1_p1p5
set DIM6 75 -2.0
set DIM6 60 1.5
launch --rwgt_info=ctqqu1i_m2p0_ctg_p1p5
set DIM6 75 -2.0
set DIM6 16 1.5
launch --rwgt_info=ctqqu1i_m2p0_ctgi_p1p5
set DIM6 75 -2.0
set DIM6 17 1.5
launch --rwgt_info=ctqqu1i_m2p0_cbw_p1p5
set DIM6 75 -2.0
set DIM6 14 1.5
launch --rwgt_info=ctqqu1i_m2p0_cbwi_p1p5
set DIM6 75 -2.0
set DIM6 15 1.5
launch --rwgt_info=ctqqu1i_m2p0_ctw_p1p5
set DIM6 75 -2.0
set DIM6 10 1.5
launch --rwgt_info=ctqqu1i_m2p0_ctwi_p1p5
set DIM6 75 -2.0
set DIM6 12 1.5
launch --rwgt_info=ctqqu1i_m2p0_cptb_p1p5
set DIM6 75 -2.0
set DIM6 8 1.5
launch --rwgt_info=ctqqu1i_m2p0_cptbi_p1p5
set DIM6 75 -2.0
set DIM6 9 1.5
launch --rwgt_info=cqq83_m2p0_ctu8_p1p5
set DIM6 51 -2.0
set DIM6 56 1.5
launch --rwgt_info=cqq83_m2p0_cqtqd8_p1p5
set DIM6 51 -2.0
set DIM6 84 1.5
launch --rwgt_info=cqq83_m2p0_cqtqd1_p1p5
set DIM6 51 -2.0
set DIM6 82 1.5
launch --rwgt_info=cqq83_m2p0_cqtqd1i_p1p5
set DIM6 51 -2.0
set DIM6 83 1.5
launch --rwgt_info=cqq83_m2p0_cpq3_p1p5
set DIM6 51 -2.0
set DIM6 5 1.5
launch --rwgt_info=cqq83_m2p0_cqq81_p1p5
set DIM6 51 -2.0
set DIM6 52 1.5
launch --rwgt_info=cqq83_m2p0_cqu1_p1p5
set DIM6 51 -2.0
set DIM6 60 1.5
launch --rwgt_info=cqq83_m2p0_ctg_p1p5
set DIM6 51 -2.0
set DIM6 16 1.5
launch --rwgt_info=cqq83_m2p0_ctgi_p1p5
set DIM6 51 -2.0
set DIM6 17 1.5
launch --rwgt_info=cqq83_m2p0_cbw_p1p5
set DIM6 51 -2.0
set DIM6 14 1.5
launch --rwgt_info=cqq83_m2p0_cbwi_p1p5
set DIM6 51 -2.0
set DIM6 15 1.5
launch --rwgt_info=cqq83_m2p0_ctw_p1p5
set DIM6 51 -2.0
set DIM6 10 1.5
launch --rwgt_info=cqq83_m2p0_ctwi_p1p5
set DIM6 51 -2.0
set DIM6 12 1.5
launch --rwgt_info=cqq83_m2p0_cptb_p1p5
set DIM6 51 -2.0
set DIM6 8 1.5
launch --rwgt_info=cqq83_m2p0_cptbi_p1p5
set DIM6 51 -2.0
set DIM6 9 1.5
launch --rwgt_info=ctu8_m2p0_cqtqd8_p1p5
set DIM6 56 -2.0
set DIM6 84 1.5
launch --rwgt_info=ctu8_m2p0_cqtqd1_p1p5
set DIM6 56 -2.0
set DIM6 82 1.5
launch --rwgt_info=ctu8_m2p0_cqtqd1i_p1p5
set DIM6 56 -2.0
set DIM6 83 1.5
launch --rwgt_info=ctu8_m2p0_cpq3_p1p5
set DIM6 56 -2.0
set DIM6 5 1.5
launch --rwgt_info=ctu8_m2p0_cqq81_p1p5
set DIM6 56 -2.0
set DIM6 52 1.5
launch --rwgt_info=ctu8_m2p0_cqu1_p1p5
set DIM6 56 -2.0
set DIM6 60 1.5
launch --rwgt_info=ctu8_m2p0_ctg_p1p5
set DIM6 56 -2.0
set DIM6 16 1.5
launch --rwgt_info=ctu8_m2p0_ctgi_p1p5
set DIM6 56 -2.0
set DIM6 17 1.5
launch --rwgt_info=ctu8_m2p0_cbw_p1p5
set DIM6 56 -2.0
set DIM6 14 1.5
launch --rwgt_info=ctu8_m2p0_cbwi_p1p5
set DIM6 56 -2.0
set DIM6 15 1.5
launch --rwgt_info=ctu8_m2p0_ctw_p1p5
set DIM6 56 -2.0
set DIM6 10 1.5
launch --rwgt_info=ctu8_m2p0_ctwi_p1p5
set DIM6 56 -2.0
set DIM6 12 1.5
launch --rwgt_info=ctu8_m2p0_cptb_p1p5
set DIM6 56 -2.0
set DIM6 8 1.5
launch --rwgt_info=ctu8_m2p0_cptbi_p1p5
set DIM6 56 -2.0
set DIM6 9 1.5
launch --rwgt_info=cqtqd8_m2p0_cqtqd1_p1p5
set DIM6 84 -2.0
set DIM6 82 1.5
launch --rwgt_info=cqtqd8_m2p0_cqtqd1i_p1p5
set DIM6 84 -2.0
set DIM6 83 1.5
launch --rwgt_info=cqtqd8_m2p0_cpq3_p1p5
set DIM6 84 -2.0
set DIM6 5 1.5
launch --rwgt_info=cqtqd8_m2p0_cqq81_p1p5
set DIM6 84 -2.0
set DIM6 52 1.5
launch --rwgt_info=cqtqd8_m2p0_cqu1_p1p5
set DIM6 84 -2.0
set DIM6 60 1.5
launch --rwgt_info=cqtqd8_m2p0_ctg_p1p5
set DIM6 84 -2.0
set DIM6 16 1.5
launch --rwgt_info=cqtqd8_m2p0_ctgi_p1p5
set DIM6 84 -2.0
set DIM6 17 1.5
launch --rwgt_info=cqtqd8_m2p0_cbw_p1p5
set DIM6 84 -2.0
set DIM6 14 1.5
launch --rwgt_info=cqtqd8_m2p0_cbwi_p1p5
set DIM6 84 -2.0
set DIM6 15 1.5
launch --rwgt_info=cqtqd8_m2p0_ctw_p1p5
set DIM6 84 -2.0
set DIM6 10 1.5
launch --rwgt_info=cqtqd8_m2p0_ctwi_p1p5
set DIM6 84 -2.0
set DIM6 12 1.5
launch --rwgt_info=cqtqd8_m2p0_cptb_p1p5
set DIM6 84 -2.0
set DIM6 8 1.5
launch --rwgt_info=cqtqd8_m2p0_cptbi_p1p5
set DIM6 84 -2.0
set DIM6 9 1.5
launch --rwgt_info=cqtqd1_m2p0_cqtqd1i_p1p5
set DIM6 82 -2.0
set DIM6 83 1.5
launch --rwgt_info=cqtqd1_m2p0_cpq3_p1p5
set DIM6 82 -2.0
set DIM6 5 1.5
launch --rwgt_info=cqtqd1_m2p0_cqq81_p1p5
set DIM6 82 -2.0
set DIM6 52 1.5
launch --rwgt_info=cqtqd1_m2p0_cqu1_p1p5
set DIM6 82 -2.0
set DIM6 60 1.5
launch --rwgt_info=cqtqd1_m2p0_ctg_p1p5
set DIM6 82 -2.0
set DIM6 16 1.5
launch --rwgt_info=cqtqd1_m2p0_ctgi_p1p5
set DIM6 82 -2.0
set DIM6 17 1.5
launch --rwgt_info=cqtqd1_m2p0_cbw_p1p5
set DIM6 82 -2.0
set DIM6 14 1.5
launch --rwgt_info=cqtqd1_m2p0_cbwi_p1p5
set DIM6 82 -2.0
set DIM6 15 1.5
launch --rwgt_info=cqtqd1_m2p0_ctw_p1p5
set DIM6 82 -2.0
set DIM6 10 1.5
launch --rwgt_info=cqtqd1_m2p0_ctwi_p1p5
set DIM6 82 -2.0
set DIM6 12 1.5
launch --rwgt_info=cqtqd1_m2p0_cptb_p1p5
set DIM6 82 -2.0
set DIM6 8 1.5
launch --rwgt_info=cqtqd1_m2p0_cptbi_p1p5
set DIM6 82 -2.0
set DIM6 9 1.5
launch --rwgt_info=cqtqd1i_m2p0_cpq3_p1p5
set DIM6 83 -2.0
set DIM6 5 1.5
launch --rwgt_info=cqtqd1i_m2p0_cqq81_p1p5
set DIM6 83 -2.0
set DIM6 52 1.5
launch --rwgt_info=cqtqd1i_m2p0_cqu1_p1p5
set DIM6 83 -2.0
set DIM6 60 1.5
launch --rwgt_info=cqtqd1i_m2p0_ctg_p1p5
set DIM6 83 -2.0
set DIM6 16 1.5
launch --rwgt_info=cqtqd1i_m2p0_ctgi_p1p5
set DIM6 83 -2.0
set DIM6 17 1.5
launch --rwgt_info=cqtqd1i_m2p0_cbw_p1p5
set DIM6 83 -2.0
set DIM6 14 1.5
launch --rwgt_info=cqtqd1i_m2p0_cbwi_p1p5
set DIM6 83 -2.0
set DIM6 15 1.5
launch --rwgt_info=cqtqd1i_m2p0_ctw_p1p5
set DIM6 83 -2.0
set DIM6 10 1.5
launch --rwgt_info=cqtqd1i_m2p0_ctwi_p1p5
set DIM6 83 -2.0
set DIM6 12 1.5
launch --rwgt_info=cqtqd1i_m2p0_cptb_p1p5
set DIM6 83 -2.0
set DIM6 8 1.5
launch --rwgt_info=cqtqd1i_m2p0_cptbi_p1p5
set DIM6 83 -2.0
set DIM6 9 1.5
launch --rwgt_info=cpq3_m2p0_cqq81_p1p5
set DIM6 5 -2.0
set DIM6 52 1.5
launch --rwgt_info=cpq3_m2p0_cqu1_p1p5
set DIM6 5 -2.0
set DIM6 60 1.5
launch --rwgt_info=cpq3_m2p0_ctg_p1p5
set DIM6 5 -2.0
set DIM6 16 1.5
launch --rwgt_info=cpq3_m2p0_ctgi_p1p5
set DIM6 5 -2.0
set DIM6 17 1.5
launch --rwgt_info=cpq3_m2p0_cbw_p1p5
set DIM6 5 -2.0
set DIM6 14 1.5
launch --rwgt_info=cpq3_m2p0_cbwi_p1p5
set DIM6 5 -2.0
set DIM6 15 1.5
launch --rwgt_info=cpq3_m2p0_ctw_p1p5
set DIM6 5 -2.0
set DIM6 10 1.5
launch --rwgt_info=cpq3_m2p0_ctwi_p1p5
set DIM6 5 -2.0
set DIM6 12 1.5
launch --rwgt_info=cpq3_m2p0_cptb_p1p5
set DIM6 5 -2.0
set DIM6 8 1.5
launch --rwgt_info=cpq3_m2p0_cptbi_p1p5
set DIM6 5 -2.0
set DIM6 9 1.5
launch --rwgt_info=cqq81_m2p0_cqu1_p1p5
set DIM6 52 -2.0
set DIM6 60 1.5
launch --rwgt_info=cqq81_m2p0_ctg_p1p5
set DIM6 52 -2.0
set DIM6 16 1.5
launch --rwgt_info=cqq81_m2p0_ctgi_p1p5
set DIM6 52 -2.0
set DIM6 17 1.5
launch --rwgt_info=cqq81_m2p0_cbw_p1p5
set DIM6 52 -2.0
set DIM6 14 1.5
launch --rwgt_info=cqq81_m2p0_cbwi_p1p5
set DIM6 52 -2.0
set DIM6 15 1.5
launch --rwgt_info=cqq81_m2p0_ctw_p1p5
set DIM6 52 -2.0
set DIM6 10 1.5
launch --rwgt_info=cqq81_m2p0_ctwi_p1p5
set DIM6 52 -2.0
set DIM6 12 1.5
launch --rwgt_info=cqq81_m2p0_cptb_p1p5
set DIM6 52 -2.0
set DIM6 8 1.5
launch --rwgt_info=cqq81_m2p0_cptbi_p1p5
set DIM6 52 -2.0
set DIM6 9 1.5
launch --rwgt_info=cqu1_m2p0_ctg_p1p5
set DIM6 60 -2.0
set DIM6 16 1.5
launch --rwgt_info=cqu1_m2p0_ctgi_p1p5
set DIM6 60 -2.0
set DIM6 17 1.5
launch --rwgt_info=cqu1_m2p0_cbw_p1p5
set DIM6 60 -2.0
set DIM6 14 1.5
launch --rwgt_info=cqu1_m2p0_cbwi_p1p5
set DIM6 60 -2.0
set DIM6 15 1.5
launch --rwgt_info=cqu1_m2p0_ctw_p1p5
set DIM6 60 -2.0
set DIM6 10 1.5
launch --rwgt_info=cqu1_m2p0_ctwi_p1p5
set DIM6 60 -2.0
set DIM6 12 1.5
launch --rwgt_info=cqu1_m2p0_cptb_p1p5
set DIM6 60 -2.0
set DIM6 8 1.5
launch --rwgt_info=cqu1_m2p0_cptbi_p1p5
set DIM6 60 -2.0
set DIM6 9 1.5
launch --rwgt_info=ctg_m2p0_ctgi_p1p5
set DIM6 16 -2.0
set DIM6 17 1.5
launch --rwgt_info=ctg_m2p0_cbw_p1p5
set DIM6 16 -2.0
set DIM6 14 1.5
launch --rwgt_info=ctg_m2p0_cbwi_p1p5
set DIM6 16 -2.0
set DIM6 15 1.5
launch --rwgt_info=ctg_m2p0_ctw_p1p5
set DIM6 16 -2.0
set DIM6 10 1.5
launch --rwgt_info=ctg_m2p0_ctwi_p1p5
set DIM6 16 -2.0
set DIM6 12 1.5
launch --rwgt_info=ctg_m2p0_cptb_p1p5
set DIM6 16 -2.0
set DIM6 8 1.5
launch --rwgt_info=ctg_m2p0_cptbi_p1p5
set DIM6 16 -2.0
set DIM6 9 1.5
launch --rwgt_info=ctgi_m2p0_cbw_p1p5
set DIM6 17 -2.0
set DIM6 14 1.5
launch --rwgt_info=ctgi_m2p0_cbwi_p1p5
set DIM6 17 -2.0
set DIM6 15 1.5
launch --rwgt_info=ctgi_m2p0_ctw_p1p5
set DIM6 17 -2.0
set DIM6 10 1.5
launch --rwgt_info=ctgi_m2p0_ctwi_p1p5
set DIM6 17 -2.0
set DIM6 12 1.5
launch --rwgt_info=ctgi_m2p0_cptb_p1p5
set DIM6 17 -2.0
set DIM6 8 1.5
launch --rwgt_info=ctgi_m2p0_cptbi_p1p5
set DIM6 17 -2.0
set DIM6 9 1.5
launch --rwgt_info=cbw_m2p0_cbwi_p1p5
set DIM6 14 -2.0
set DIM6 15 1.5
launch --rwgt_info=cbw_m2p0_ctw_p1p5
set DIM6 14 -2.0
set DIM6 10 1.5
launch --rwgt_info=cbw_m2p0_ctwi_p1p5
set DIM6 14 -2.0
set DIM6 12 1.5
launch --rwgt_info=cbw_m2p0_cptb_p1p5
set DIM6 14 -2.0
set DIM6 8 1.5
launch --rwgt_info=cbw_m2p0_cptbi_p1p5
set DIM6 14 -2.0
set DIM6 9 1.5
launch --rwgt_info=cbwi_m2p0_ctw_p1p5
set DIM6 15 -2.0
set DIM6 10 1.5
launch --rwgt_info=cbwi_m2p0_ctwi_p1p5
set DIM6 15 -2.0
set DIM6 12 1.5
launch --rwgt_info=cbwi_m2p0_cptb_p1p5
set DIM6 15 -2.0
set DIM6 8 1.5
launch --rwgt_info=cbwi_m2p0_cptbi_p1p5
set DIM6 15 -2.0
set DIM6 9 1.5
launch --rwgt_info=ctw_m2p0_ctwi_p1p5
set DIM6 10 -2.0
set DIM6 12 1.5
launch --rwgt_info=ctw_m2p0_cptb_p1p5
set DIM6 10 -2.0
set DIM6 8 1.5
launch --rwgt_info=ctw_m2p0_cptbi_p1p5
set DIM6 10 -2.0
set DIM6 9 1.5
launch --rwgt_info=ctwi_m2p0_cptb_p1p5
set DIM6 12 -2.0
set DIM6 8 1.5
launch --rwgt_info=ctwi_m2p0_cptbi_p1p5
set DIM6 12 -2.0
set DIM6 9 1.5
launch --rwgt_info=cptb_m2p0_cptbi_p1p5
set DIM6 8 -2.0
set DIM6 9 1.5
''')
reweight_card_f.close()

print_cards()

# --------------------------------------------------------------
# Generate
# --------------------------------------------------------------
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

# --------------------------------------------------------------
# Run Pythia 8 Showering
# --------------------------------------------------------------
check_reset_proc_number(opts)
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
