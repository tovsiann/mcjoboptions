import MadGraphControl.MadGraphUtils

MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf': 260000, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets
    'pdf_variations':[260000,90400,13100,25200], # pdfs for which all variations (error sets) will be included as weights
    'alternative_pdfs':[266000,265000], # pdfs for which only the central set will be included as weights
    'scale_variations':[0.5,1.,2.], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated
    'alternative_dynamic_scales' : [1,2,3,4],
}
from MadGraphControl.MadGraphUtils import *


evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]

evgenConfig.keywords = ['SM', 'diboson', 'WZ', 'electroweak', '3lepton', 'VBS']
evgenConfig.contact = ['jan-eric.nitschke@cern.ch']

gridpack_dir = 'madevent/'
Mode=0
gridpack_mode=True
gridpack_compile=False
nevents = 10000*1.2

MADGRAPH_GRIDPACK_LOCATION = gridpack_dir
MADGRAPH_CATCH_ERRORS=False
# ---------------------------------------------------------------------------
# Process type based on runNumber:
# ---------------------------------------------------------------------------
if flavorcharge == "OFM":
    runName = 'lvlljjEW5_OFMinus'
    description = 'MadGraph_lvlljj_EW5_OFMinus_masslessLeptons'
    mgproc = """
generate  p p > mu+ mu- e- ve~ j j QED^2==10 QCD^2==2 @0
add process p p > ta+ ta- e- ve~ j j QED^2==10 QCD^2==2 @0
add process p p > e+ e- mu- vm~ j j QED^2==10 QCD^2==2 @0
add process p p > ta+ ta- mu- vm~ j j QED^2==10 QCD^2==2 @0
add process p p > e+ e- ta- vt~ j j QED^2==10 QCD^2==2 @0
add process p p > mu+ mu- ta- vt~ j j QED^2==10 QCD^2==2 @0"""
elif flavorcharge == "OFP":
    runName = 'lvlljjEW5_OFPlus'
    description = 'MadGraph_lvlljj_EW5_OFPlus_masslessLeptons'
    mgproc = """
generate  p p > mu+ mu- e+ ve j j QED^2==10 QCD^2==2 @0
add process p p > ta+ ta- e+ ve j j QED^2==10 QCD^2==2 @0
add process p p > e+ e- mu+ vm j j QED^2==10 QCD^2==2 @0
add process p p > ta+ ta- mu+ vm j j QED^2==10 QCD^2==2 @0
add process p p > e+ e- ta+ vt j j QED^2==10 QCD^2==2 @0
add process p p > mu+ mu- ta+ vt j j QED^2==10 QCD^2==2 @0"""
elif flavorcharge == "SFM":
    runName = 'lvlljjEW5_SFMinus'
    description = 'MadGraph_lvlljj_EW5_SFMinus_masslessLeptons'
    mgproc = """
generate p p > e+ e- e- ve~ j j QED^2==10 QCD^2==2 @0
add process p p > mu+ mu- mu- vm~ j j QED^2==10 QCD^2==2 @0
add process p p > ta+ ta- ta- vt~ j j QED^2==10 QCD^2==2 @0"""
elif flavorcharge == "SFP":
    runName = 'lvlljjEW5_SFPlus'
    description = 'MadGraph_lvlljj_EW5_SFPlus_masslessLeptons'
    mgproc = """
generate p p > e+ e- e+ ve j j QED^2==10 QCD^2==2 @0
add process p p > mu+ mu- mu+ vm j j QED^2==10 QCD^2==2 @0
add process p p > ta+ ta- ta+ vt j j QED^2==10 QCD^2==2 @0"""
    pass
else:
    raise RuntimeError(
        "Flavor-charge combination %i not recognised in these jobOptions. Has to be one of SFM,SFP,OFM,OFP!" % flavorcharge)

evgenConfig.description = description
# ---------------------------------------------------------------------------
# write MG5 Proc card
# ---------------------------------------------------------------------------
process = """
import model sm-no_masses
# massless b and massless taus
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
%s
output -f
""" % (mgproc)

# ----------------------------------------------------------------------------
# Random Seed
# ----------------------------------------------------------------------------
randomSeed = 0
if hasattr(runArgs, 'randomSeed'):
    randomSeed = runArgs.randomSeed

# ----------------------------------------------------------------------------
# Beam energy
# ----------------------------------------------------------------------------
beamEnergy=-999
if hasattr(runArgs, 'ecmEnergy'):
    beamEnergy = int(runArgs.ecmEnergy) / 2.
else:
    raise RunTimeError("No center of mass energy found.")

# ---------------------------------------------------------------------------
# Number of Events
# ---------------------------------------------------------------------------
safefactor = 1.1
if hasattr(runArgs, 'maxEvents') and runArgs.maxEvents > 0:
    nevents = int(int(runArgs.maxEvents) * safefactor)
else:
    nevents = int(nevents * safefactor)

extras = {
    'ptl': 4.0,
    'ptj': 15.0,
    'ptb': 15.0,
    'ptheavy': 2.0,
    'mmll': 4.0,
    'drll': 0.2,
    'drjl': 0.2,
    'drjj': 0.4,
    'dral': 0.1,
    'etaj': 5.5,
    'etal': 5.0,
    'maxjetflavor': 5,
    'asrwgtflavor': 5,
    'auto_ptj_mjj': False,
    'cut_decays': True,
    'event_norm': 'average',
}
extras["nevents"] = int(nevents)

process_dir = new_process(process)

modify_run_card(process_dir=process_dir, run_card_backup='run_card_backup.dat',
                settings=extras)
print_cards()

generate(required_accuracy=0.001,process_dir=process_dir,grid_pack=gridpack_mode,gridpack_compile=gridpack_compile, runArgs=runArgs)


outputDS=arrange_output(process_dir=process_dir,lhe_version=3,saveProcDir=True,runArgs=runArgs)
runArgs.inputGeneratorFile=outputDS

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
genSeq.Pythia8.Commands += ["SpaceShower:dipoleRecoil=on"]
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")


