########################################################
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short

def MassToFloat(s):
  if "p" in s:
    return float(s.replace("p", "."))
  return float(s)
#
#--------------------------------------------------------
jobConfig = get_physics_short()
evgenLog.info("Physics short name: " + str(jobConfig))

additional_options = jobConfig[6:]
evgenLog.info("DirectStau: Additional options: " + str(additional_options))
#----------------------------------------------------------------------

splits=jobConfig.split('_')
masses['1000024'] = MassToFloat(splits[4])
masses['1000023'] = MassToFloat(splits[4])
masses['1000022'] = MassToFloat(splits[5].split('.')[0])
splitting = 0.5
if len(splits) > 7:
  splitting = MassToFloat(splits[-2].split('.')[0])
  evgenLog.info("Using mass splitting of %f for mslep" % splitting)
mslep = ( masses['1000024'] - masses['1000022'] )*splitting + masses['1000022']
evgenLog.info("  Stau / sneutrino mass is %f GeV" % mslep)
masses['1000015'] = mslep
masses['1000016'] = mslep


if masses['1000022']<0.5: masses['1000022']=0.5
gentype = str(splits[2])
decaytype = str(splits[3])

###########################
if "C1C1" in jobConfig:
	process = '''
    generate p p > x1+ x1- $ susystrong @1
	add process p p > x1+ x1- j $ susystrong @2
	add process p p > x1+ x1- j j $ susystrong @3
	'''
elif "C1N2" in jobConfig:
	process = '''
	generate p p > x1+ n2 $ susystrong @1
	add process p p > x1- n2 $ susystrong @2
	add process p p > x1+ n2 j $ susystrong @3
	add process p p > x1- n2 j $ susystrong @4
	add process p p > x1+ n2 j j $ susystrong @5
	add process p p > x1- n2 j j $ susystrong @6
	'''
#---------------------------
njets = 2
evgenLog.info('Registered generation of ~chi1+/- ~chi20 production, decay via stau; grid point '+" "+' decoded into mass point ' + str(masses['1000024']) + ' ' + str(masses['1000022']))

evgenConfig.contact  = [ "mann@cern.ch" ]
evgenConfig.keywords += ['SUSY', 'gaugino', 'chargino', 'neutralino', 'stau']

#-----------------------------------------
if "C1C1" in jobConfig:
	evgenConfig.description = '~chi1+/~chi1- production, decay via stau in simplified model, m_C1 = %s GeV, m_stau = %s GeV, m_N1 = %s GeV' % (masses['1000024'],masses['1000015'],masses['1000022'])
elif "C1N2" in jobConfig:
	evgenConfig.description = '~chi1+/- ~chi20 production, decay via stau in simplified model, m_C1N2 = %s GeV, m_stau = %s GeV, m_N1 = %s GeV'%(masses['1000024'],masses['1000015'],masses['1000022'])
#-----------------------------------------

evt_multiplier = 20 

#-------------------------------------------------------

#step 1
decays['1000023'] = """DECAY   1000023  2.07770048E-02   # neutralino2 decays
#          BR         NDA      ID1       ID2
     0.00000000E+00     2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
     0.00000000E+00     2     1000024       -24   # BR(~chi_20 -> ~chi_1+ W- )
     0.00000000E+00     2    -1000024        24   # BR(~chi_20 -> ~chi_1- W+ )
     0.00000000E+00     2     1000011       -11   # BR(~chi_20 -> ~e_L-     e+)
     0.00000000E+00     2    -1000011        11   # BR(~chi_20 -> ~e_L+     e-)
     0.00000000E+00     2     2000011       -11   # BR(~chi_20 -> ~e_R-     e+)
     0.00000000E+00     2    -2000011        11   # BR(~chi_20 -> ~e_R+     e-)
     0.00000000E+00     2     1000013       -13   # BR(~chi_20 -> ~mu_L-    mu+)
     0.00000000E+00     2    -1000013        13   # BR(~chi_20 -> ~mu_L+    mu-)
     0.00000000E+00     2     2000013       -13   # BR(~chi_20 -> ~mu_R-    mu+)
     0.00000000E+00     2    -2000013        13   # BR(~chi_20 -> ~mu_R+    mu-)
     0.25000000E+00     2     1000015       -15   # BR(~chi_20 -> ~tau_1-   tau+)
     0.25000000E+00     2    -1000015        15   # BR(~chi_20 -> ~tau_1+   tau-)
     0.00000000E+00     2     2000015       -15   # BR(~chi_20 -> ~tau_2-   tau+)
     0.00000000E+00     2    -2000015        15   # BR(~chi_20 -> ~tau_2+   tau-)
     0.00000000E+00     2     1000012       -12   # BR(~chi_20 -> nu_eL  ~nu_e)
     0.00000000E+00     2    -1000012        12   # BR(~chi_20 -> ~nu_eL  nu_e)
     0.00000000E+00     2     1000014       -14   # BR(~chi_20 -> nu_muL  ~nu_mu)
     0.00000000E+00     2    -1000014        14   # BR(~chi_20 -> ~nu_muL  nu_mu)
     0.25000000E+00     2     1000016       -16   # BR(~chi_20 -> nu_tauL  ~nu_tau)
     0.25000000E+00     2    -1000016        16   # BR(~chi_20 -> ~nu_tauL  nu_tau)

"""
#
decays['1000024'] = """DECAY   1000024     1.70414503E-02    # chargino1+ decays
#          BR         NDA      ID1           ID2        ID3
     0.00000000E+00    3     1000023         -11        12   # BR(~chi_1+ -> ~chi_20  e+  nu_e)
     0.00000000E+00    3     1000023         -13        14   # BR(~chi_1+ -> ~chi_20  mu+  nu_mu)
     0.00000000E+00    3     1000023         -15        16   # BR(~chi_1+ -> ~chi_20  tau+  nu_tau)
     0.00000000E+00    3     1000023           2        -1   # BR(~chi_1+ -> ~chi_20  u dbar)
     0.00000000E+00    3     1000023           4        -3   # BR(~chi_1+ -> ~chi_20  c sbar)
     0.00000000E+00    3     1000022         -11        12   # BR(~chi_1+ -> ~chi_10  e+  nu_e)
     0.00000000E+00    3     1000022         -13        14   # BR(~chi_1+ -> ~chi_10  mu+  nu_mu)
     0.00000000E+00    3     1000022         -15        16   # BR(~chi_1+ -> ~chi_10  tau+  nu_tau)
     0.00000000E+00    3     1000022           2        -1   # BR(~chi_1+ -> ~chi_10  u  dbar)
     0.00000000E+00    3     1000022           4        -3   # BR(~chi_1+ -> ~chi_10  c  sbar)
     0.00000000E+00    2     1000022          24             # BR(~chi_1+ -> ~chi_10  W+)
     0.00000000E+00    2     1000012         -11    # BR(~chi_1+ -> ~nu_eL  e+  )
     0.00000000E+00    2     1000014         -13    # BR(~chi_1+ -> ~nu_muL  mu+ )
     0.50000000E+00    2     1000016         -15    # BR(~chi_1+ -> ~nu_tau1 tau+)
     0.00000000E+00    2    -1000011          12    # BR(~chi_1+ -> ~e_L+    nu_e)
     0.00000000E+00    2    -2000011          12    # BR(~chi_1+ -> ~e_R+    nu_e)
     0.00000000E+00    2    -1000013          14    # BR(~chi_1+ -> ~mu_L+   nu_mu)
     0.00000000E+00    2    -2000013          14    # BR(~chi_1+ -> ~mu_R+   nu_mu)
     0.50000000E+00    2    -1000015          16    # BR(~chi_1+ -> ~tau_1+  nu_tau)
     0.00000000E+00    2    -2000015          16    # BR(~chi_1+ -> ~tau_2+  nu_tau)
"""
#step 2
decays['1000015'] = """DECAY   1000015  1.48327268E-01    # stau_1 decays
 #          BR         NDA      ID1       ID2
      1.00000000E+00    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
      0.00000000E+00    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
      0.00000000E+00    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
      0.00000000E+00    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
      0.00000000E+00    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
      0.00000000E+00    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
      0.00000000E+00    2     1000016       -37   # BR(~tau_1 -> ~nu_tauL H-)
      0.00000000E+00    2     1000016       -24   # BR(~tau_1 -> ~nu_tauL W-)
 """
#
decays['1000016'] = """DECAY   1000016  1.47518977E-01    # snu_tauL decays
 #          BR         NDA      ID1       ID2
      1.00000000E+00    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
      0.00000000E+00    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
      0.00000000E+00    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
      0.00000000E+00    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
      0.00000000E+00    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
      0.00000000E+00    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
      0.00000000E+00    2    -1000015       -37   # BR(~nu_tauL -> ~tau_1+ H-)
      0.00000000E+00    2    -2000015       -37   # BR(~nu_tauL -> ~tau_2+ H-)
      0.00000000E+00    2    -1000015       -24   # BR(~nu_tauL -> ~tau_1+ W-)
      0.00000000E+00    2    -2000015       -24   # BR(~nu_tauL -> ~tau_2+ W-)
"""
#-------------------------
#Mixing 
#------------------------
# Off-diagonal chargino mixing matrix V
param_blocks['VMIX']={}
param_blocks['VMIX']['1 1']='1.00E+00'
param_blocks['VMIX']['1 2']='0.00E+00'
param_blocks['VMIX']['2 1']='0.00E+00'
param_blocks['VMIX']['2 2']='0.00E+00'
# Off-diagonal chargino mixing matrix U
param_blocks['UMIX']={}
param_blocks['UMIX']['1 1']='1.00E+00'
param_blocks['UMIX']['1 2']='0.00E+00'
param_blocks['UMIX']['2 1']='0.00E+00'
param_blocks['UMIX']['2 2']='0.00E+00'
# Neutralino mixing matrix chi_i0 = N_ij (B,W,H_d,H_u)_j
param_blocks['NMIX']={}
param_blocks['NMIX']['1  1']=' 1.00E+00'   # N_11 
param_blocks['NMIX']['1  2']=' 0.00E+00'   # N_12
param_blocks['NMIX']['1  3']=' 0.00E+00'   # N_13
param_blocks['NMIX']['1  4']=' 0.00E+00'   # N_14 
param_blocks['NMIX']['2  1']=' 0.00E+00'   # N_21 
param_blocks['NMIX']['2  2']=' 1.00E+00'   # N_22
param_blocks['NMIX']['2  3']=' 0.00E+00'   # N_23 
param_blocks['NMIX']['2  4']=' 0.00E+00'   # N_24  
param_blocks['NMIX']['3  1']=' 0.00E+00'   # N_31 
param_blocks['NMIX']['3  2']=' 0.00E+00'   # N_32 
param_blocks['NMIX']['3  3']=' 0.00E+00'   # N_33 
param_blocks['NMIX']['3  4']=' 0.00E+00'   # N_34 
param_blocks['NMIX']['4  1']=' 0.00E+00'   # N_41
param_blocks['NMIX']['4  2']=' 0.00E+00'   # N_42 
param_blocks['NMIX']['4  3']=' 0.00E+00'   # N_43
param_blocks['NMIX']['4  4']=' 0.00E+00'   # N_44
#-------------------------------------------------------

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

#-----------------------------
if njets>0:
	genSeq.Pythia8.Commands += ["Merging:Process = guess"] 
	genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]
	#run_settings["pdgs_for_merging_cut"]= "1, 2, 3, 4,21"
	#run_settings["asrwgtflavor"]=4
	#run_settings["maxjetflavor"]=4

#--------------------------------------------------------------
# Lepton filter
#--------------------------------------------------------------
if '2L1T' in additional_options:
  evgenLog.info('2L1T filter is applied')
  from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
  filtSeq += MultiElecMuTauFilter("DileptonFilter")
  filtSeq += MultiElecMuTauFilter("TauFilter")
  
  MultiElecMuTauFilter1 = filtSeq.DileptonFilter
  MultiElecMuTauFilter1.NLeptons  = 2
  MultiElecMuTauFilter1.MinPt = 5000.
  MultiElecMuTauFilter1.MaxEta = 2.7
  MultiElecMuTauFilter1.MinVisPtHadTau = 15000. # pt-cut on the visible hadronic tau
  MultiElecMuTauFilter1.IncludeHadTaus = 1      # include hadronic taus
  
  MultiElecMuTauFilter2 = filtSeq.TauFilter
  MultiElecMuTauFilter2.NLeptons  = 1
  MultiElecMuTauFilter2.MinPt = 1e10
  MultiElecMuTauFilter2.MaxEta = 2.7
  MultiElecMuTauFilter2.MinVisPtHadTau = 15000. # pt-cut on the visible hadronic tau
  MultiElecMuTauFilter2.IncludeHadTaus = 1      # include hadronic taus
  filtSeq.Expression = "(DileptonFilter and TauFilter)"
