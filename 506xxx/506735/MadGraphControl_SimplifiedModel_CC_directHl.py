
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
splitConfig = get_physics_short().split('_')


masses['1000024'] = float(splitConfig[4]) # C1 mass

gentype = str(splitConfig[2])
decaytype = str(splitConfig[3])

process = '''
import model RPVMSSM_UFO
define susystrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
generate p p > x1+ x1-   RPV=0   / susystrong @1
add process p p > x1+ x1- j RPV=0   / susystrong @2
add process p p > x1+ x1- j j RPV=0  / susystrong @3
'''

# Set up the decays 
chargino_decay = {'1000024':'''DECAY   1000024    1.70414503E-02   # chargino1+ decays
#          BR        NDA      ID1       ID2      ID3  
        0.0000000000E+00    2    23    -11    # BR(C1 -> Z e+)
        0.0000000000E+00    2    23    -13    # BR(C1 -> Z mu+)
        0.0000000000E+00    2    23    -15    # BR(C1 -> Z tau+)
        3.3300000000E-01    2    25    -11    # BR(C1 -> H e+)
        3.3300000000E-01    2    25    -13    # BR(C1 -> H mu+)
        3.3400000000E-01    2    25    -15    # BR(C1 -> H tau+)
        0.0000000000E+00    2    24    -12    # BR(C1 -> W+ nu_ebar)
        0.0000000000E+00    2    24    -14    # BR(C1 -> W+ nu_mubar)
        0.0000000000E+00    2    24    -16    # BR(C1 -> W+ nu_taubar)
#
  '''}
decays.update(chargino_decay)

print "gentype", gentype
print "decaytype", decaytype
print "decays", decays
print "masses", masses

# Debug the SM higgs mass/branching ratio in the default param_card (which uses the values of 110.8GeV higgs)
masses['25'] = 125.00

njets = 2
evgenConfig.contact  = ["elodie.deborah.resseguie@cern.ch"]
evgenConfig.keywords += ["SUSY", "RPV", "chargino", "neutralino", "2photon"]
evgenConfig.description = '~chi1+/~chi1- production, decay via Hl in simplified model, m_C1 = %s GeV'%(masses['1000024'])

from GeneratorFilters.GeneratorFiltersConf import DirectPhotonFilter
filtSeq += DirectPhotonFilter()

filtSeq.DirectPhotonFilter.NPhotons = 2

filtSeq.Expression = "DirectPhotonFilter"

evt_multiplier = 300.

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = pp>{x1+,1000024}{x1-,-1000024}",
                              "1000024:spinType = 1"]
