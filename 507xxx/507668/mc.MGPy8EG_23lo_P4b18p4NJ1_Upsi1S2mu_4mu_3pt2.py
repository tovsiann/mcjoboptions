##
## 18.4GeV tetrab (4b bound state) pseduoscalar --> Upsilon+2mu --> 4mu with 1 jet
##
m4b=18.4
cVV4=0.0
cGG6=0.0
tcVV6=0.001
tcGG6=0.001
include("./MGCtrl_Py8EG_A14NNPDF23LO_tetrabNJ1_4mu_3pt2.py")
