#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.keywords    = ["SM","Z","bottom"]
evgenConfig.description = "MadGraph Pythia8 pp->Z(->bb)jj production, QED order not included, all 2->4 processes."
evgenConfig.process     = "QCD Z->bb"
evgenConfig.contact     = ["yicong.huang@cern.ch"]
evgenConfig.nEventsPerJob = 10000
nevents=10000

from MadGraphControl.MadGraphUtils import *
process = """
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
generate p p > z j j, z > b b~
output -f"""

process_dir = new_process(process)

beamEnergy=-999.
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

# safety factor 2 should be OK
safefactor=2
nevents=nevents*safefactor
if runArgs.maxEvents > 0:
    nevents=runArgs.maxEvents*safefactor

#Fetch default LO run_card.dat and set parameters
settings = { 'lhe_version':'2.0',
             'cut_decays' :'F',
             'pdlabel'    :"'nn23lo1'",
             'ptj'        : 20,
             'ptb'        : 0,
             'etab'       : 5.0,
             'etaj'       : 5.0,
             #'use_syst'   :"False",
             'nevents'    :int(nevents)
           }

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=2,saveProcDir=True)

#### Shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
