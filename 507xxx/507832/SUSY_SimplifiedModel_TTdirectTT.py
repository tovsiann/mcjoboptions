# --------------------------------------------------------------
# Standard pre-include
#
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

# uncomment this if you want to keep MadGraph tarball, e.g. for Feynman diagrams
# keepOutput=True
# --------------------------------------------------------------

from MadGraphControl.MadGraphUtilsHelpers import *
JOName = get_physics_short()
evgenLog.info("JOName: " + str(JOName))

flavourScheme = 5
decay = JOName.split('_')[2]
mt1 = JOName.split('_')[3]
mn1 = JOName.split('_')[4]
filter_choice = 'None'
if len(JOName.split('_'))>5 and JOName.split('_')[5] != 'MS': filter_choice = JOName.split('_')[5]
madSpin = False
if len(JOName.split('_'))>6 and JOName.split('_')[6] == 'MS': madSpin = True
if len(JOName.split('_'))>5 and JOName.split('_')[5] == 'MS': madSpin = True

evgenLog.info("Now checking parsing: decoded mass point (mt1,mn1) = (" + str(mt1) + ", " + str(mn1) + ")" +
              " - Decay " + str(decay) +
              " - Madspin used for decay " + str(madSpin) +
              " - Custom Filter " + str(filter_choice) )

mt1 = JOName.split('_')[3]
mn1 = JOName.split('_')[4]

masses['1000006'] = float(mt1)
masses['1000022'] = float(mn1)

#if masses['1000022']<0.5: masses['1000022']=0.5

process = '''
generate    p p > t1 t1~     $ go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @1
add process p p > t1 t1~ j   $ go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @2
add process p p > t1 t1~ j j $ go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @3
'''

njets = 2

# Now introducing decay table for the 4body decay, from https://gitlab.cern.ch/atlas-physics/pmg/infrastructure/mc15joboptions/-/blob/master/common/MadGraph/param_card.SM.TT.bffN.dat
decays['1000006']="""DECAY   1000006     1.34259598E-01              # Total BR
    1.00000000E+00    2     1000022         6   # BR(~t_1 -> ~chi_10 t )"""
# Renaming features from "extras" to "run_settings"
#run_settings["use_syst"]='F' # Bug fix for syst calc issue in AGENE-1542 -> Now removed because the framework is different, making the code crash!
run_settings["event_norm"]='sum'
# MadGraph 2.6.X uses 5-flav for merging: force 4-flav merging as use a 4-flav scheme
run_settings['pdgs_for_merging_cut']='1, 2, 3, 4, 21, 1000001, 1000002, 1000003, 1000004, 1000021, 2000001, 2000002, 2000003, 2000004'

# Some general information
evgenConfig.contact = ["jared.little@cern.ch"]
evgenConfig.keywords += ['simplifiedModel', 'stop']
evgenConfig.description = 'stop direct pair production, st->t+LSP in simplified model, m_stop = %s GeV, m_N1 = %s GeV'%(masses['1000006'],masses['1000022'])

if madSpin:
    evgenLog.info('Running w/ MadSpin option')
    madspin_card='madspin_card_test.dat'

#    fixEventWeightsForBridgeMode = True

    mscard = open(madspin_card,'w')
    decay_chains = [ "t1 > n1 fu fd~ b" , "t1~ > n1 fu~ fd b~" ]

    mscard.write("""#************************************************************
    #*                        MadSpin                           *
    #*                                                          *
    #*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
    #*                                                          *
    #*    Part of the MadGraph5_aMC@NLO Framework:              *
    #*    The MadGraph5_aMC@NLO Development Team - Find us at   *
    #*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
    #*                                                          *
    #************************************************************
    #Some options (uncomment to apply)
    set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
    #
    set seed %i
    set spinmode none
    # specify the decay for the final state particles
    decay %s
    decay %s
    # running the actual code
    launch"""%(runArgs.randomSeed,decay_chains[0],decay_chains[1]))

    mscard.close()

if filter_choice == '1L20orMET100':

    include ( 'GeneratorFilters/LeptonFilter.py' )
    filtSeq.LeptonFilter.Ptcut  = 20000
    filtSeq.LeptonFilter.Etacut = 2.8
    include('MC15JobOptions/MissingEtFilter.py')
    filtSeq.MissingEtFilter.METCut = 100000
    filtSeq.Expression = "LeptonFilter or MissingEtFilter"
    evt_multiplier = 10.

elif filter_choice == '1L20andMET50':

    evgenLog.info('1lepton and MET 50 filter is applied')
    include ( 'GeneratorFilters/LeptonFilter.py' )
    filtSeq.LeptonFilter.Ptcut  = 20*GeV
    filtSeq.LeptonFilter.Etacut = 2.8 
    include('GeneratorFilters/MissingEtFilter.py')
    filtSeq.MissingEtFilter.METCut = 50*GeV
    filtSeq.Expression = "LeptonFilter and MissingEtFilter"
    evt_multiplier = 10.

elif filter_choice == 'MET100':
    evgenLog.info('MET100 filter is applied')
    include ( 'GeneratorFilters/MissingEtFilter.py' )
    filtSeq.MissingEtFilter.METCut = 100*GeV
    evt_multiplier = 100.

elif filter_choice == '2L15':
    evgenLog.info('2Lepton15 filter is applied')
    if not hasattr(filtSeq, "MultiElecMuTauFilter"):
        from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
        filtSeq += MultiElecMuTauFilter("MultiElecMuTauFilter")

    filtSeq.MultiElecMuTauFilter.MinPt  = 15000.
    filtSeq.MultiElecMuTauFilter.MaxEta = 2.8
    filtSeq.MultiElecMuTauFilter.NLeptons = 2
    filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 0
    filtSeq.Expression = "MultiElecMuTauFilter"
    evt_multiplier = 50

# ----------------------------------------------------------------------------------------------------------------
# Standard post-include
#
include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )
# --------------------------------------------------------------

if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = pp>{t1,1000006}{t1~,-1000006}"]
