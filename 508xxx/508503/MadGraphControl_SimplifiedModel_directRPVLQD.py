from MadGraphControl.MadGraphUtils import *
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py')

#Read filename of jobOptions to obtain: productionmode, ewkino mass.
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
tokens = get_physics_short().split('_')
try:
    _  = float(tokens[-1])
    event_filter = ''
except ValueError:
    event_filter = tokens[-1]
    tokens = tokens[:-1]
    
productionmode = str(tokens[2])

if productionmode == 'SmuonSmuon':
    smuonMass = float(tokens[4])
    neutralinoMass = float(tokens[5])
else:
    neutralinoMass = float(tokens[4])

branchingRatiosN1 = '''
        #          BR          NDA       ID1       ID2       ID3
        0.125      3    13   6  -5
        0.125      3    15   6  -5
        0.125      3    14   5  -5
        0.125      3    16   5  -5
        0.125      3   -13  -6   5
        0.125      3   -15  -6   5
        0.125      3   -14  -5   5
        0.125      3   -16  -5   5
        #'''
branchingRatiosC1 = '''
        #          BR          NDA       ID1       ID2       ID3
        0.25      3    -13  5  -5
        0.25      3    -15  5  -5
        0.25      3    -14  6  -5
        0.25      3    -16  6  -5
        '''
branchingRatiosSmuonR = '''
        #          BR          NDA       ID1       ID2       ID3
        1.0      2    13  1000022
'''
masses['1000022'] = neutralinoMass #N1
masses['1000023'] = neutralinoMass #N2
masses['1000024'] = neutralinoMass #C1
decays['1000022'] = 'DECAY   1000022  1.0' + branchingRatiosN1
decays['1000023'] = 'DECAY   1000023  1.0' + branchingRatiosN1 #N2 has same decay as N1
decays['1000024'] = 'DECAY   1000024  1.0' + branchingRatiosC1
if productionmode == 'SmuonSmuon':
    masses['2000013'] = smuonMass
    decays['2000013'] = 'DECAY   2000013  1.0' + branchingRatiosSmuonR

njets = 2
# __And now production. Taking cues from higgsinoRPV.py generator__
process = '''
import model RPVMSSM_UFO
define susystrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
define c1 = x1+ x1-
'''

if productionmode == 'C1N2N1' : #production modes cover all of C1N1, N2N1, C1+C1-, and C1N2
    process += '''
    define X = n1 c1
    define Y = n2 c1
    generate    p p > X Y     RPV=0 QED<=2 / susystrong @1
    add process p p > X Y j   RPV=0 QED<=2 / susystrong @2
    add process p p > X Y j j RPV=0 QED<=2 / susystrong @3
    '''
elif productionmode == 'C1N1' :                                                                                           
    process += '''
    generate    p p > n1 c1     RPV=0 QED<=2 / susystrong @1
    add process p p > n1 c1 j   RPV=0 QED<=2 / susystrong @2
    add process p p > n1 c1 j j RPV=0 QED<=2 / susystrong @3
    '''
elif productionmode == 'N1N2' :
    process += '''
    generate    p p > n1 n2     RPV=0 QED<=2 / susystrong @1
    add process p p > n1 n2 j   RPV=0 QED<=2 / susystrong @2
    add process p p > n1 n2 j j RPV=0 QED<=2 / susystrong @3
    '''
elif productionmode == 'C1C1' :
    process += '''
    generate    p p > x1+ x1-     RPV=0 QED<=2 / susystrong @1
    add process p p > x1+ x1- j   RPV=0 QED<=2 / susystrong @2
    add process p p > x1+ x1- j j RPV=0 QED<=2 / susystrong @3
    '''
elif productionmode == 'SmuonSmuon' :
    process += '''
    generate    p p > mur+ mur-     RPV=0 QED<=2 / susystrong @1
    add process p p > mur+ mur- j   RPV=0 QED<=2 / susystrong @2
    add process p p > mur+ mur- j j RPV=0 QED<=2 / susystrong @3
    '''
else:
    raise RunTimeError("ERROR: did not recognize productionmode arg")

if njets==1:
    process = '\n'.join([x for x in process.split('\n') if not "j j" in x])


if '1L20' in event_filter:
    from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
    filtSeq += LeptonFilter()
    filtSeq.LeptonFilter.Ptcut  = 20*GeV
    evt_multiplier = 3.

evgenConfig.contact = ["jmontejo@cern.ch"]
evgenConfig.keywords +=['SUSY', 'RPV', 'neutralino']
if productionmode == 'C1N2N1' :
    evgenConfig.description = 'C1N1, C1N2, N1N2, C1C1 ewk production'
elif productionmode == 'C1N1' :
    evgenConfig.description = 'chargino - neutralino1'
elif productionmode == 'N1N2' :
    evgenConfig.description = 'neutralino1 - neutralino2'
elif productionmode == 'C1C1' :
    evgenConfig.description = 'chargino+ - chargino-'
evgenConfig.description += 'Electroweak production, and decays via RPV LQD i33 coupling (i=2,3), m_N1 = m_C1 = m_N2 = %.1f GeV'%(masses['1000022'])

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

if njets>0:
    genSeq.Pythia8.Commands += ["Merging:Process = guess"]
    genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]
