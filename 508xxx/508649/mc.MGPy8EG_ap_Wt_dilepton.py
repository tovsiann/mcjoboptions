from MadGraphControl import MadGraphUtils as MGU
from MadGraphControl.MadGraphUtils import *

# ------------------------------------------------------------------------------
# Metadata 
# ------------------------------------------------------------------------------

evgenConfig.description   = 'semi-elastic ttbar, mediated by a photon'
evgenConfig.generators    = ['MadGraph', 'Pythia8', 'EvtGen']
evgenConfig.keywords     += ['ttbar']
evgenConfig.contact       = ['jhowarth@cern.ch']
nevents = 1.1*runArgs.maxEvents if runArgs.maxEvents>0 else 10000

# ------------------------------------------------------------------------------
# Hard process (PDF)
# ------------------------------------------------------------------------------

from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *

# ------------------------------------------------------------------------------
# Hard process (ME)
# ------------------------------------------------------------------------------

process="""
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l = e+ e- mu+ mu- ta+ ta-
define vl = ve ve~ vm vm~ vt vt~
generate    a b  > t  w-, (t  > w+ b,  w+ > l vl), (w- > l vl)
add process a b~ > t~ w+, (t~ > w- b~, w- > l vl), (w+ > l vl)
output -f"""

process_dir = MGU.new_process(process)

settings = { 'lhe_version'     : '3.0',
             'cut_decays'      : 'F',
             'lpp1'            : '2',
             'lpp2'            : '1',
             'fixed_ren_scale' : 'True',
             'fixed_fac_scale' : 'True',
             'scale'           : '172.5',
             'dsqrt_q2fact1'   : '172.5',
             'dsqrt_q2fact2'   : '172.5',
             'nevents'         : int(nevents)}

MGU.modify_run_card(process_dir = process_dir,
                    runArgs     = runArgs,
                    settings    = settings)

MGU.generate(process_dir = process_dir,
             runArgs     = runArgs)

MGU.arrange_output(process_dir = process_dir,
                   runArgs     = runArgs,
                   lhe_version = 3,
                   saveProcDir = True)  


# --------------------------------------------------------------
# Parton Shower
# --------------------------------------------------------------

include("Pythia8_i/Py8_NNPDF23_NNLO_as118_QED_DS_Common.py") 
include("Pythia8_i/Pythia8_ShowerWeights.py")
include("Pythia8_i/Pythia8_MadGraph.py")

genSeq.Pythia8.Commands += ['BeamRemnants:unresolvedHadron = 1']
genSeq.Pythia8.Commands += ['Diffraction:hardDiffSide      = 1'] #0 = both side, 1 = A side, 2 = C side
genSeq.Pythia8.Commands += ['TimeShower:QEDshowerByQ       = on']
genSeq.Pythia8.Commands += ['SpaceShower:QEDshowerByQ      = on']

# Don't change these during studies
genSeq.Pythia8.Commands += ['PartonLevel:MPI               = off']
genSeq.Pythia8.Commands += ['Diffraction:sampleType        = 3'] #3 = no MPI check, 4 = MPI check
genSeq.Pythia8.Commands += ['TimeShower:QEDshowerByL       = on']
genSeq.Pythia8.Commands += ['TimeShower:QEDshowerByGamma   = on']
genSeq.Pythia8.Commands += ['SpaceShower:QEDshowerByL      = on']

### Print the output by WriteHepMC
#from TruthIO.TruthIOConf import WriteHepMC
#genSeq += WriteHepMC()
