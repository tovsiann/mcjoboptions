from MadGraphControl.MadGraphUtils import *
import os

# General settings
nevents=int(1.1*runArgs.maxEvents)
mode=0 #0 single machine, 1 cluster, 2 multicore

# ME settings
maxjetflavor=5
dyn_scale = '3' #-1 default, 1 Et, 2 mT, 3 mT/2, 4 \hat{s}

print("Selected restriction card: ", restriction)

if not (channel == "production" or  channel == "decay"):
    print("ERROR: invalidchannel ", channel, " not production nor decay")
    exit(1)

if not (top_decay == "hadronic" or top_decay == "leptonic"):
    print("ERROR: invalid top decay channel ", top_decay, "not hadronic nor leptonic")
    exit(1)


#kinematic cuts
req_dR = 0.0
req_eta = -1.0
req_pt = 0.1
hard_survey = 0

if (channel == "production" ):

    req_dR = 0.4
    req_eta = 5.0
    req_pt = 10.0
    hard_survey = 2

    if ( top_decay == "hadronic" ):
        proc_str_1 = "generate p p > t l+ l- , ( t >  w+ q , ( w+ > q q~ ))"
        proc_str_2 = "add process p p > t~ l+ l- , ( t~ >  w- q~ , ( w- > q q~ ))"

    if ( top_decay == "leptonic" ):
        proc_str_1 = "generate p p > t l+ l- , ( t >  w+ q , ( w+ > l+ vl ))"
        proc_str_2 = "add process p p > t~ l+ l- , ( t~ >  w- q~ , ( w- > l- vl~ ))"

if ( channel == "decay" ):

    if ( top_decay == "hadronic" ):
        proc_str_1 = "generate p p > t t~ , ( t >  l+ l- uq ) , ( t~ >  w- q~ , ( w- > q q~ ))"
        proc_str_2 = "add process p p > t t~ , ( t >  w+ q , ( w+ > q q~ )), ( t~ > l+ l- uq~ )"

    if ( top_decay == "leptonic" ):
        dyn_scale = '4'
        proc_str_1 = "generate p p > t t~ , ( t >  l+ l- uq ) , ( t~ >  w- q~, ( w- > l- vl~ ) )"
        proc_str_2 = "add process p p > t t~ , ( t >  w+ q , ( w+ > l+ vl )), ( t~ > l+ l- uq~ )"


process ="""
import model dim6top_CLFV_LO_UFO-{r}
set zerowidth_tchannel False
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define q = u c d s b
define q~ = u~ c~ d~ s~ b~
define uq = u c
define uq~ = u~ c~
{p1}
{p2}
output -f
""".format(r=restriction, p1=proc_str_1, p2=proc_str_2)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

process_dir = new_process(process)


extras = {'pdlabel'       : "'lhapdf'",
          'hard_survey'   : hard_survey,
          'lhaid'         : 303400, #315000 = NNPDF31_lo_as_0118, 303400 NNPDF31_nlo_as_0118, 90900 = PDF4LHC15 with 30 variations,
          'lhe_version'   : '3.0',
          'maxjetflavor'  : maxjetflavor,
          'dynamical_scale_choice': dyn_scale,
          'nevents'       : int(nevents),
          'use_syst'      : 'True',
          'sys_scalefact' : '0.5 1 2',
          'sys_alpsfact'  : '0.5 1 2',
          'sys_pdf'       : 'NNPDF31_nlo_as_0118',
          'drjj'          : req_dR,
          'drll'          : req_dR,
          'drjl'          : req_dR,
          'ptl'           : req_pt, # minimum lepton pT
          'ptj'           : req_pt, # minimum jet pT
          'ptb'           : req_pt, # minimum jet pT
          'etaj'          : req_eta,
          'etab'          : req_eta,
          'etal'          : req_eta}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)
print_cards()

generate(process_dir=process_dir,runArgs=runArgs)
outDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

# Go to serial mode for Pythia8
if 'ATHENA_PROC_NUMBER' in os.environ:
    print 'You run with an athena MP-like whole-node setup. Re-configureing to run remainder of the job serially.'
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print 'Did not see option!'
    else: opts.nprocs = 0
    print opts

evgenConfig.description = 'MGPy8_'
evgenConfig.keywords+=["ttbar", "BSM"]
evgenConfig.description = 'ttbar_clfv'
evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.contact = ["carlo.gottardo@cern.ch"]
runArgs.inputGeneratorFile = outDS

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

