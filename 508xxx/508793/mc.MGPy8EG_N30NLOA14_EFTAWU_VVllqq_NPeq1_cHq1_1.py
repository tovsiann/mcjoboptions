from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
include('MadGraphControl/setupEFT.py')

# create dictionary of processes this JO can create
definitions="""define p = g u c d s b u~ c~ d~ s~ b~
define j = p
define l+ = e+ mu+ ta+ 
define l- = e- mu- ta-
define ll = l+ l-
define vv = vl vl~
"""
processes={ 'VVlvqq':'generate p p > ll vv j j EFTORDER\n',
            'VVllqq':'generate p p > l+ l- j j EFTORDER\n',
            'VVvvqq':'generate p p > vl vl~ j j EFTORDER\n',
}
for p in processes:
    processes[p]=definitions+processes[p]

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob
    
# define cuts
settings={
    'lhaid' : '260400',
    'pdlabel' : 'lhapdf',
    'dynamical_scale_choice':'4',
    'scalefact' : 0.5,
    'cut_decays': "True",
    'mmjj' : 60,
    'mmjjmax' : 110,
    'drjj': 0.01,
    'drjl': 0.01,
    'nevents' :int(nevents),
}

# get EFT cards, determined from 4 last blocks of JO name
process_dir = setup_SMEFT_from_JOname(processes)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
opts.nprocs = 0 #multi-core cleanup after generation

generate(runArgs=runArgs,process_dir=process_dir)
outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)  
runArgs.inputGeneratorFile=outputDS

# add meta data
evgenConfig.description = 'Semileptonic diboson production with SMEFTSIM'
evgenConfig.keywords+=['diboson']
evgenConfig.contact = ['Hannes Mildner <hannes.mildner@cern.ch>', 'Robert Les <robert.les@cern.ch>']

# shower
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# turn off mpi for faster generation and smaller files in 'noMPI' is part of name
if 'noMPI' in get_physics_short():
    genSeq.Pythia8.Commands += [' PartonLevel:MPI = off']

