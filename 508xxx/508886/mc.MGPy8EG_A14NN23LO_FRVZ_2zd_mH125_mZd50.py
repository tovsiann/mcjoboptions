params={}
params['mZDinput']  = 5.000000e-02
params['MHSinput']  = 2.000000e+00
params['epsilon']   = 1.000000e-03
params['gXmu']      = 1.200000e-03
params['gXe']       = 1.200000e-03
params['gXpi']      = 1.000000e+00
params['MChi1']     = 2.000000e+00
params['MChi2']     = 5.000000e+00
params['WZp']       = "DECAY 3000001 1.000000e-03 # WZp"
params['mH']        = 125
params['nGamma']    = 2
params['avgtau']    = 7.0
params['decayMode'] = 'electrons'




                

include ("MadGraphControl_A14N23LO_FRVZ_ggF.py")

evgenConfig.keywords = ["exotic", "BSMHiggs", "BSM", "darkPhoton"]

                    

