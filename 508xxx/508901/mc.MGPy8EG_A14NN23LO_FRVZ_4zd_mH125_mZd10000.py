params={}
params['mZDinput']  = 1.000000e+01
params['MHSinput']  = 2.500000e+01
params['epsilon']   = 1.000000e-03
params['gXmu']      = 6.334000e-04
params['gXe']       = 6.334000e-04
params['gXpi']      = 1.000000e-11
params['MChi1']     = 6.000000e+00
params['MChi2']     = 3.500000e+01
params['WZp']       = "DECAY 3000001 1.623360e-07 # WZp"
params['mH']        = 125
params['nGamma']    = 4
params['avgtau']    = 900.0
params['decayMode'] = 'normal'




                

include ("MadGraphControl_A14N23LO_FRVZ_ggF.py")

evgenConfig.keywords = ["exotic", "BSMHiggs", "BSM", "darkPhoton"]

                    
