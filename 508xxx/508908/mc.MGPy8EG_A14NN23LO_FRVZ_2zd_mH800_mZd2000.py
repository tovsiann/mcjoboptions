params={}
params['mZDinput']  = 2.000000e+00
params['MHSinput']  = 5.000000e+00
params['epsilon']   = 1.000000e-03
params['gXmu']      = 6.150000e-04
params['gXe']       = 6.150000e-04
params['gXpi']      = 1.000000e-11
params['MChi1']     = 2.000000e+00
params['MChi2']     = 1.000000e+01
params['WZp']       = "DECAY 3000001 2.033460e-08 # WZp"
params['mH']        = 800
params['nGamma']    = 2
params['avgtau']    = 30.0
params['decayMode'] = 'normal'




                

include ("MadGraphControl_A14N23LO_FRVZ_ggF.py")

evgenConfig.keywords = ["exotic", "BSMHiggs", "BSM", "darkPhoton"]

                    
