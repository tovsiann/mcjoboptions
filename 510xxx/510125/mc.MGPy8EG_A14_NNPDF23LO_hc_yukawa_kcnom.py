from MadGraphControl.MadGraphUtils import *


import fileinput
import math
import os
import sys
import subprocess
#---------------------------------------------------------------------------------------------------
# Get run info and determine number of events - and setting it to higher nevents
#---------------------------------------------------------------------------------------------------

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob
gridpack_mode=False

#import model sm-c_mass
#import model Higgs_Effective_Couplings_UFO
#---------------------------------------------------------------------------------------------------
#Setup proc_card
#---------------------------------------------------------------------------------------------------
#HIG=0  switch off the effective
#QED<=0 switch off Yukawa
#QED<=1 yukawa
process = """
import model Higgs_Effective_Couplings_UFO
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
generate p p > h c HIG=0
add process p p > h c~ HIG=0
output -f
"""
print('process string: ',process)


beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")


#---------------------------------------------------------------------------------------------------
# Setting some parameters for run_card.dat
#---------------------------------------------------------------------------------------------------
#Fetch default LO run_card.dat and set parameters //nn23lo1 with the systematics
settings = { 'lhe_version':'3.0',
             'cut_decays':'F',
             'pdlabel':"'lhapdf'",
             'lhaid'  : '260000',
             'use_syst':"True",
             'sys_scalefact':'1.0 0.5 2.0',
             'sys_pdf'      : 'NNPDF30_nlo_as_0118',
             'ptj':'10',
             'etaj':'4.7',
             'nevents'      : int(nevents)
}


#old version of PDF 247000, NNPDF23_lo_as_0130_qed
#new version of PDF 260000, NNPDF30_nlo_as_0118
#(recommendations from https://indico.cern.ch/event/777484/contributions/3244052/attachments/1766720/2871333/Recommendation.pdf)

runName='run_01'
#---------------------------------------------------------------------------------------------------
# Setting higgs mass to 125 GeV for param_card.dat
#---------------------------------------------------------------------------------------------------
#kc changes were done according to this suggestion from Olivier https://answers.launchpad.net/mg5amcnlo/+question/699778
mass_kc = 0.630000e+00;

masses={'25': '1.250000e+02',
		'4':  '0.000000e+00'}
yukawas={'4': str(mass_kc)} #at the normal scale for kc =1

params={}
params['YUKAWA'] = yukawas
params['MASS'] = masses



process_dir = new_process(process)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
modify_param_card(process_dir=process_dir,params=params)

print_cards()

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)



#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.description = 'MGPy8_hcYc'
evgenConfig.keywords+=['Higgs','SMHiggs']
evgenConfig.contact = ['Anna Ivina <anna.ivina@cern.ch>']
#evgenConfig.inputfilecheck = runName
#evgenConfig.nEventsPerJob = 10000


#Showeing
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

#Decaying Higgs
genSeq.Pythia8.Commands += [
	'25:onMode = off', # switch OFF all Higgs decay channels
    '25:onIfMatch = 22 22' # H -> gamma gamma
    ]
