## pythia shower
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 2
evgenConfig.keywords += ['SM', 'top',  'photon','singleTop','lepton']
evgenConfig.generators += ["aMcAtNlo", "Pythia8"]
evgenConfig.description = 'aMcAtNlo+Py8_Var3cDown_tqGammaSM_tchan_4fl'
evgenConfig.contact = ["bjorn.wendland@cern.ch"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_Var3cDown_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")
