include("MadGraphControl/SUSY_SimplifiedModel_PreInclude.py")

import os
from MadGraphControl.MadGraphUtils import mglog
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
from MadGraphControl.MadGraphParamHelpers import get_masses

# get the file name and split it on _ to extract relavent information
JOName = get_physics_short()
jobConfigParts = JOName.split("_")
prodType = jobConfigParts[3] # JM now we use prod type
#processId = int(jobConfigParts[4]) # JM there is no processID, there will be a list for prodType
slha_file = jobConfigParts[5].replace(".py", "") + ".dat"

processNumbers={"QCD":  [1,2,3,4,51,52,61,62],
                "Stop": [51,52,61,62],
                "EW":   [111,112,113,114,115,116,117,118,122,123,124,125,126,127,128,
                        133,134,135,136,137,138,144,145,146,147,148,157,158,167,168,
                        201,202,203,204,205,206,207,208,209,210,211,212,213,216,217,218,219,220],
                "LL":   [201,202,203,204,205,206,207,208,209,210,211,212,213,216,217,218,219,220],
                "ALL":  [1,2,3,4,51,52,61,62,
                        111,112,113,114,115,116,117,118,122,123,124,125,126,127,128,
                        133,134,135,136,137,138,144,145,146,147,148,147,148,157,158,167,168,
                        201,202,203,204,205,206,207,208,209,210,211,212,213,216,217,218,219,220],
                "TRIAL":[127],
                }

# Giordon: this is a bad idea, fix below line
#processId = int(runArgs.jobConfig[0][-3:])  # int(jobConfigParts[5].replace(".py",""))
#processId = int(runArgs.jobConfig[0][4:7])

# Set default SM masses and decays for SLHA1, they will be overidden.
mTop = 172.5 # top
masses['6'] = mTop # top
masses['5'] = 4.950000e+00 # b, FourFS
masses['15'] = 1.777000e+00 # tau
masses['23'] = 9.118760e+01 # Z
masses['24'] = 8.039900e+01 # W

# Calculate the top width based on the mass
# From https://gitlab.cern.ch/atlasphys-top/reco/MC/blob/master/MCinfo/get_t_width.py
import math
# ATLAS MC11 conventions == PDG2010
# Vtb=0.999152
# using Vtb=1.0 since part of the inputs was already produced using this approximation
Vtb=1.0
M_W=80.399
# PDG2010
G_F=1.16637*(math.pow(10,-5))
# MSbar alpha_s(mt)
alpha_s=0.108
# Born gamma coeff.
C1=G_F/(8*math.pi*math.sqrt(2))
# Born approximation (taking intermediate W-boson to be on-shell) [1]
wTop_B=C1*math.pow(float(mTop),3)*math.pow(Vtb,2)*pow((1-math.pow((M_W/float(mTop)),2)),2)*(1+2*pow((M_W/float(mTop)),2))
# EW and QCD corrections to Born: QCD dominates, EW can be neglected [1],[2],[3]
wTop=wTop_B*(1-0.81*alpha_s-1.81*pow(alpha_s,2))

# TODO: Make sure to set the decays to PMG values
decays['6'] = """DECAY  6   """+str(wTop)+"""
#          BR         NDA          ID1       ID2       ID3       ID4
    1.000000e+00   2    5  24 # 1.32"""
decays['23'] = """DECAY  23   2.495200e+00 # Z decays"""
decays['24'] = """DECAY  24   2.085000e+00 # W decays
#          BR         NDA          ID1       ID2       ID3       ID4
    3.377000e-01   2   -1   2
    3.377000e-01   2   -3   4
    1.082000e-01   2  -11  12
    1.082000e-01   2  -13  14
    1.082000e-01   2  -15  16
"""

#JM Loop on all processes in prodType
for processId in processNumbers[prodType]:
    mglog.info("{0:s} production for {1:s} - process {2:d}".format(prodType, slha_file, processId))

# Update PMG defaults with SLHA values for masses and decays
subprocess.Popen(['get_files','-data', slha_file]).wait()
# Set input param card, keep in same directory as JO
param_card = slha_file

# get more events out of madgraph to feed through athena (esp. for filters)
evt_multiplier = 3.0

processStrings = {
    1: ["go sstrong", "go sstrongbar"],
    2: ["go go"],
    3: ["sstrong sstrong", "sstrongbar sstrongbar"],
    4: ["sstrong sstrongbar"],
    51: ["b1 b1~"],
    52: ["b2 b2~"],
    61: ["t1 t1~"],
    62: ["t2 t2~"],
    111: ["n1 n1"],
    112: ["n1 n2"],
    113: ["n1 n3"],
    114: ["n1 n4"],
    115: ["n1 x1+"],
    116: ["n1 x2+"],
    117: ["n1 x1-"],
    118: ["n1 x2-"],
    122: ["n2 n2"],
    123: ["n2 n3"],
    124: ["n2 n4"],
    125: ["n2 x1+"],
    126: ["n2 x2+"],
    127: ["n2 x1-"],
    128: ["n2 x2-"],
    133: ["n3 n3"],
    134: ["n3 n4"],
    135: ["n3 x1+"],
    136: ["n3 x2+"],
    137: ["n3 x1-"],
    138: ["n3 x2-"],
    144: ["n4 n4"],
    145: ["n4 x1+"],
    146: ["n4 x2+"],
    147: ["n4 x1-"],
    148: ["n4 x2-"],
    157: ["x1+ x1-"],
    158: ["x1+ x2-"],
    167: ["x2+ x1-"],
    168: ["x2+ x2-"],
    201: ["el- el+"],
    202: ["er- er+"],
    203: ["sve sve~"],
    204: ["el+ sve"],
    205: ["el- sve~"],
    206: ["ta1- ta1+"],
    207: ["ta2- ta2+"],
    208: ["ta1- ta2+", "ta1+ ta2-"],
    209: ["svt svt~"],
    210: ["ta1+ svt"],
    211: ["ta1- svt~"],
    212: ["ta2+ svt"],
    213: ["ta2- svt~"],
    216: ["mul- mul+"],
    217: ["mur- mur+"],
    218: ["svm svm~"],
    219: ["mul+ svm"],
    220: ["mul- svm~"],
}

processes = []
defines = []
massDict = get_masses(param_card)
sq_pid_mapping = dict(
    [
        ("b1", "1000005"),
        ("t1", "1000006"),
        ("b2", "2000005"),
        ("t2", "2000006"),
        ("b1~", "1000005"),
        ("t1~", "1000006"),
        ("b2~", "2000005"),
        ("t2~", "2000006"),
        ("go", "1000021"),
        ("n1", "1000022"),
        ("n2", "1000023"),
        ("n3", "1000025"),
        ("n4", "1000035"),
        ("x1-", "1000024"),
        ("x1+", "1000024"),
        ("x2-", "1000037"),
        ("x2+", "1000037"),
        ("el-", "1000011"),
        ("el+", "1000011"),
        ("er-", "2000011"),
        ("er+", "2000011"),
        ("sve", "1000012"),
        ("sve~", "1000012"),
        ("mul-", "1000013"),
        ("mul+", "1000013"),
        ("mur-", "2000013"),
        ("mur+", "2000013"),
        ("svm", "1000014"),
        ("svm~", "1000014"),
        ("ta1-", "1000015"),
        ("ta1+", "1000015"),
        ("ta2-", "2000015"),
        ("ta2+", "2000015"),
        ("svt", "1000016"),
        ("svt~", "1000016"),
    ]
)

if prodType == "QCD":
    parts = []
    partsbar = []
    minsquarkMass = 10000
    maxsquark = 3000
    maxgluino = 3000
    for sq, pid in [
        ("dl", "1000001"),
        ("dr", "2000001"),
        ("ul", "1000002"),
        ("ur", "2000002"),
        ("sl", "1000003"),
        ("sr", "2000003"),
        ("cl", "1000004"),
        ("cr", "2000004"),
    ]:
        mass = float(massDict[pid])
        if mass > maxsquark:
            continue
        if mass < minsquarkMass:
            minsquarkMass = mass
        parts.append(sq)
        partsbar.append(sq + "~")
    massDict["sstrong"] = minsquarkMass
    massDict["sstrongbar"] = minsquarkMass

    if parts:
        defines.append("define sstrong = {0:s}".format(" ".join(parts)))
        defines.append("define sstrongbar = {0:s}".format(" ".join(partsbar)))
    else:
        processNumbers["QCD"]=[2,51,52,61,62]

minMass = 10000

# note: preinclude decouples masses
for pid, mass in masses.items():
    if pid in ["sstrong","sstrongbar"]: continue # these are for internal use only
    masses[pid] = float(massDict.get(pid, mass))

if prodType=="EW":
    suppress=" $ susystrong " # suppresses squark-neutralino production in EW
else:
    suppress=""
# Add processes for each process in genType
for processId in processNumbers[prodType]:
    for process in processStrings[processId]:
        for part in process.split():
            pid = sq_pid_mapping.get(part, part)
            if pid in ["sstrong","sstrongbar"]:
                minMass = min(massDict[pid],minMass)
            else:
                masses[pid] = float(massDict[pid])
                minMass = min(masses[pid], minMass)
        processes.append("add process p p > {0:s}   {1:s} @1".format(process,suppress))
        processes.append("add process p p > {0:s} j {1:s} @2".format(process,suppress))

if not processes:
    mglog.error("No processes to generate!")
    raise RuntimeError("Error in MadGraph generation")

mglog.info("pMSSM process")
processes[0] = processes[0].replace("add process", "generate")
process = "\n".join(processes)

# generate the new process
# Somehow add "update to_slha2" after launch for Run1 points https://answers.launchpad.net/mg5amcnlo/+question/658000
process = """
import model MSSM_SLHA2-full
# define multiparticle labels
{0:s}
# Specify process(es) to run
{1:s}
""".format(
    "\n".join(defines), process
)

print "JM", process

# do not go below 25 GeV for qcut
#run_settings["qcut"] = max(minMass * 0.25, 25)
PYTHIA8_qCut = max(minMass * 0.25, 25)

# choose pdf set
#run_settings["pdlabel"] = "'nn23lo1'"
#run_settings["lhaid"] = 247000

#needed for MadGraph to properly handle narrow decay widths
run_settings["small_width_treatment"] = 1e-19
if prodType=="EW":
    #run_settings["hard_survey"] = 2
    run_settings["sde_strategy"] = 1

fixEventWeightsForBridgeMode = True

include("MadGraphControl/SUSY_SimplifiedModel_PostInclude.py")

# Pythia8 setup
genSeq.Pythia8.Commands += [
    "Init:showAllParticleData = on",
    "Next:numberShowLHA = 10",
    "Next:numberShowEvent = 10",
    "Merging:process = guess",
    "1000037:spinType = 1",
    "1000035:spinType = 1",
    "1000025:spinType = 1",
    "1000024:spinType = 1",
    "1000023:spinType = 1",
    "1000022:spinType = 1",
]
genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]

# configuration for EvgenJobTransforms
evgenLog.info("Registered generation of pMSSM")
evgenConfig.contact = ["Brian.Petersen@cern.ch", "Giordon.Stark@cern.ch"]
evgenConfig.keywords += ["SUSY", "pMSSM"]
evgenConfig.description = "pMSSM"
evgenConfig.generators += ["EvtGen"]

testSeq.TestHepMC.MaxVtxDisp = 1e16  # in mm
testSeq.TestHepMC.MaxTransVtxDisp = 1e16  # in mm
