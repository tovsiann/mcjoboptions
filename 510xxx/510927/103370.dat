# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  16:44
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.68246895E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.45620834E+03  # scale for input parameters
    1    4.88912758E+01  # M_1
    2    1.71969574E+03  # M_2
    3    3.63418956E+03  # M_3
   11   -3.71735792E+03  # A_t
   12    1.69341882E+03  # A_b
   13   -1.64626281E+03  # A_tau
   23   -1.62943558E+02  # mu
   25    3.57723205E+01  # tan(beta)
   26    9.08030094E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.82339400E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.34035677E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.06531566E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.45620834E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.45620834E+03  # (SUSY scale)
  1  1     8.38764751E-06   # Y_u(Q)^DRbar
  2  2     4.26092494E-03   # Y_c(Q)^DRbar
  3  3     1.01329519E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.45620834E+03  # (SUSY scale)
  1  1     6.02930886E-04   # Y_d(Q)^DRbar
  2  2     1.14556868E-02   # Y_s(Q)^DRbar
  3  3     5.97918508E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.45620834E+03  # (SUSY scale)
  1  1     1.05215849E-04   # Y_e(Q)^DRbar
  2  2     2.17553005E-02   # Y_mu(Q)^DRbar
  3  3     3.65888365E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.45620834E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -3.71736078E+03   # A_t(Q)^DRbar
Block Ad Q=  3.45620834E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.69341905E+03   # A_b(Q)^DRbar
Block Ae Q=  3.45620834E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.64626247E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.45620834E+03  # soft SUSY breaking masses at Q
   1    4.88912758E+01  # M_1
   2    1.71969574E+03  # M_2
   3    3.63418956E+03  # M_3
  21    6.28480561E+05  # M^2_(H,d)
  22    9.12011613E+04  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.82339400E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.34035677E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.06531566E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.23710652E+02  # h0
        35     9.08032313E+02  # H0
        36     9.08030094E+02  # A0
        37     9.13390443E+02  # H+
   1000001     1.00972871E+04  # ~d_L
   2000001     1.00739921E+04  # ~d_R
   1000002     1.00969221E+04  # ~u_L
   2000002     1.00772698E+04  # ~u_R
   1000003     1.00972916E+04  # ~s_L
   2000003     1.00739999E+04  # ~s_R
   1000004     1.00969266E+04  # ~c_L
   2000004     1.00772709E+04  # ~c_R
   1000005     3.18645786E+03  # ~b_1
   2000005     4.88388793E+03  # ~b_2
   1000006     2.44418056E+03  # ~t_1
   2000006     4.88727236E+03  # ~t_2
   1000011     1.00201173E+04  # ~e_L-
   2000011     1.00087666E+04  # ~e_R-
   1000012     1.00193547E+04  # ~nu_eL
   1000013     1.00201377E+04  # ~mu_L-
   2000013     1.00088060E+04  # ~mu_R-
   1000014     1.00193749E+04  # ~nu_muL
   1000015     1.00200743E+04  # ~tau_1-
   2000015     1.00259484E+04  # ~tau_2-
   1000016     1.00251562E+04  # ~nu_tauL
   1000021     4.09916168E+03  # ~g
   1000022     4.65572165E+01  # ~chi_10
   1000023     1.80178064E+02  # ~chi_20
   1000025     1.81011823E+02  # ~chi_30
   1000035     1.82701561E+03  # ~chi_40
   1000024     1.74588086E+02  # ~chi_1+
   1000037     1.82698267E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.79042289E-02   # alpha
Block Hmix Q=  3.45620834E+03  # Higgs mixing parameters
   1   -1.62943558E+02  # mu
   2    3.57723205E+01  # tan[beta](Q)
   3    2.43108873E+02  # v(Q)
   4    8.24518652E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     2.97325126E-02   # Re[R_st(1,1)]
   1  2     9.99557891E-01   # Re[R_st(1,2)]
   2  1    -9.99557891E-01   # Re[R_st(2,1)]
   2  2     2.97325126E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -1.17377782E-03   # Re[R_sb(1,1)]
   1  2     9.99999311E-01   # Re[R_sb(1,2)]
   2  1    -9.99999311E-01   # Re[R_sb(2,1)]
   2  2    -1.17377782E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -6.25209069E-02   # Re[R_sta(1,1)]
   1  2     9.98043654E-01   # Re[R_sta(1,2)]
   2  1    -9.98043654E-01   # Re[R_sta(2,1)]
   2  2    -6.25209069E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.61516107E-01   # Re[N(1,1)]
   1  2    -2.49611341E-03   # Re[N(1,2)]
   1  3    -2.67026875E-01   # Re[N(1,3)]
   1  4    -6.46312063E-02   # Re[N(1,4)]
   2  1    -2.34589363E-01   # Re[N(2,1)]
   2  2    -3.28771691E-02   # Re[N(2,2)]
   2  3    -6.75365227E-01   # Re[N(2,3)]
   2  4    -6.98404419E-01   # Re[N(2,4)]
   3  1    -1.43015027E-01   # Re[N(3,1)]
   3  2     2.91577244E-02   # Re[N(3,2)]
   3  3    -6.87437600E-01   # Re[N(3,3)]
   3  4     7.11425383E-01   # Re[N(3,4)]
   4  1     1.14369655E-03   # Re[N(4,1)]
   4  2    -9.99030875E-01   # Re[N(4,2)]
   4  3     2.82925198E-03   # Re[N(4,3)]
   4  4     4.39089856E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -4.00876293E-03   # Re[U(1,1)]
   1  2    -9.99991965E-01   # Re[U(1,2)]
   2  1     9.99991965E-01   # Re[U(2,1)]
   2  2    -4.00876293E-03   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -6.21183750E-02   # Re[V(1,1)]
   1  2     9.98068789E-01   # Re[V(1,2)]
   2  1     9.98068789E-01   # Re[V(2,1)]
   2  2     6.21183750E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02859179E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.24555548E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     5.50014239E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     2.04417614E-02    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.36254977E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.45723039E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     8.09569494E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     7.21996963E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.01565758E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.05033850E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.04744796E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.21238361E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     5.56472140E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     2.12472175E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.86594754E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.36349578E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.45631340E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     8.40549928E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.04827610E-03    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.01357230E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.04615470E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.03918329E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     4.70568066E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.41069674E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.28159798E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.43918695E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.55876829E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     2.88644606E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.62796083E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     7.97349703E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     8.32281788E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     7.99979538E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.51651166E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.00329636E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.04887401E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.36258434E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.62235454E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.76419308E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     3.60403517E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02270545E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     2.49625374E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.02641427E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.36353024E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.61639043E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.76228109E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     3.60154226E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02061548E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     3.18596926E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.02224755E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.63031380E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     7.21056811E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.31159787E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     3.01392842E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.52798202E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.65760657E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.04009933E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.59279607E+02   # ~d_R
#    BR                NDA      ID1      ID2
     9.29671713E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     5.53120765E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     2.05611515E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.89944408E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.82108241E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.96616939E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.94343479E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     6.08661931E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.21917302E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.15048696E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.59332805E+02   # ~s_R
#    BR                NDA      ID1      ID2
     9.29918361E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     5.74443053E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     2.27737507E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.89851629E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.82138664E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.96883044E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     2.12535470E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     6.08635094E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.21911946E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.15012994E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     4.66974568E+01   # ~b_1
#    BR                NDA      ID1      ID2
     6.99212185E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.21970634E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.28575223E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     4.79530502E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
DECAY   2000005     2.47631094E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.25837904E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     6.38298931E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     6.63833737E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     6.41147204E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     3.99274872E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.29882515E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.35968872E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     1.18938475E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     8.26315596E-04    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
     4.04062376E-03    2     1000005        36   # BR(~b_2 -> ~b_1 A^0)
     4.04065869E-03    2     1000005        35   # BR(~b_2 -> ~b_1 H^0)
DECAY   2000002     5.76481109E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.60889603E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     2.14693403E-03    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     7.97926412E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.60966131E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.82091167E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.85475877E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     3.62867617E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     6.08156068E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     5.02634891E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.21446868E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.15016377E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.76488462E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.60885288E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     2.14998530E-03    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     8.01109284E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.60954009E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.82121560E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.85469924E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     3.65457449E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     6.08129376E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     5.41127805E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.21441518E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.14980669E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.03571440E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.05374354E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.32615606E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.40407617E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     4.76188836E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.70421299E-04    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     2.53394256E+02   # ~t_2
#    BR                NDA      ID1      ID2
     3.72517914E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.92315390E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.97572810E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     6.36862284E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.36753184E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.25760064E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.28435696E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     7.83187838E-03    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     5.82371839E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     4.05412649E-04    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     8.47916779E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     4.03594680E-04    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     7.77194966E-02   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99999542E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     2.51728435E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     5.17525164E-04    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     1.54157578E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.55599052E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     1.26662304E-02    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     8.25694318E-02    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     8.29236508E-02    2          37   1000025   # BR(chi^+_2 -> H^+ chi^0_3)
     1.54706702E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     9.07256473E-02    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     1.55941129E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     9.06960494E-02    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     7.80117272E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     5.72285579E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     5.94617597E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     4.75788733E-02   # chi^0_2
#    BR                NDA      ID1      ID2
     6.73274468E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     3.26715463E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     1.13930718E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     8.24621197E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.75338610E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     2.59871963E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.50554816E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.50554816E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     8.72486241E-02    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     8.72486241E-02    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     3.45656991E-04    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     6.42027482E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     8.54724933E-02    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     5.70593600E-03    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     2.76979308E-02    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     3.21645651E-02    2     1000025        36   # BR(chi^0_4 -> chi^0_3 A^0)
     1.92043242E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     8.55581602E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     9.65660914E-02    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
     6.65000863E-03    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     5.28140120E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
     4.87995843E-02    2     1000025        35   # BR(chi^0_4 -> chi^0_3 H^0)
#    BR                NDA      ID1      ID2       ID3
     3.59879354E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     3.69502030E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     5.45447643E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     5.45447643E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     6.90979991E+01   # ~g
#    BR                NDA      ID1      ID2
     3.60716169E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     3.60716169E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.37500913E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.37500913E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
#    BR                NDA      ID1      ID2       ID3
     4.91115417E-04    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     1.65344100E-04    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     5.09034539E-04    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.71958334E-04    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     6.88890342E-04    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     6.88890342E-04    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.96294637E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     1.96294637E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.93285749E-03   # Gamma(h0)
     2.21173252E-03   2        22        22   # BR(h0 -> photon photon)
     1.31292544E-03   2        22        23   # BR(h0 -> photon Z)
     2.31753709E-02   2        23        23   # BR(h0 -> Z Z)
     1.96452691E-01   2       -24        24   # BR(h0 -> W W)
     6.93257973E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.82169406E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.14476899E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.18394488E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.30290909E-07   2        -2         2   # BR(h0 -> Up up)
     2.52849838E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.59270963E-07   2        -1         1   # BR(h0 -> Down down)
     2.02275444E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.38733844E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     8.12457592E-02   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     1.83789519E+01   # Gamma(HH)
     5.59090156E-08   2        22        22   # BR(HH -> photon photon)
     3.22890541E-08   2        22        23   # BR(HH -> photon Z)
     4.64892221E-06   2        23        23   # BR(HH -> Z Z)
     6.91178234E-06   2       -24        24   # BR(HH -> W W)
     6.09167212E-05   2        21        21   # BR(HH -> gluon gluon)
     9.90007983E-09   2       -11        11   # BR(HH -> Electron electron)
     4.40636384E-04   2       -13        13   # BR(HH -> Muon muon)
     1.27235450E-01   2       -15        15   # BR(HH -> Tau tau)
     1.07691759E-13   2        -2         2   # BR(HH -> Up up)
     2.08839481E-08   2        -4         4   # BR(HH -> Charm charm)
     1.32871972E-03   2        -6         6   # BR(HH -> Top top)
     8.52943857E-07   2        -1         1   # BR(HH -> Down down)
     3.08501098E-04   2        -3         3   # BR(HH -> Strange strange)
     8.15239349E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.45291475E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     8.13013869E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     2.01623318E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.88707747E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.42300540E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     3.61307866E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.68085775E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     4.07925995E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.79284906E+01   # Gamma(A0)
     4.62731839E-08   2        22        22   # BR(A0 -> photon photon)
     2.78425135E-08   2        22        23   # BR(A0 -> photon Z)
     9.47835270E-05   2        21        21   # BR(A0 -> gluon gluon)
     9.88092946E-09   2       -11        11   # BR(A0 -> Electron electron)
     4.39784049E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.26991367E-01   2       -15        15   # BR(A0 -> Tau tau)
     1.02251217E-13   2        -2         2   # BR(A0 -> Up up)
     1.98230926E-08   2        -4         4   # BR(A0 -> Charm charm)
     1.48974685E-03   2        -6         6   # BR(A0 -> Top top)
     8.51293988E-07   2        -1         1   # BR(A0 -> Down down)
     3.07904403E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.13689606E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.69656678E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     8.18597803E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.90431780E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     2.11266176E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.50926257E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     3.17315362E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     2.24394166E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     7.15465455E-06   2        23        25   # BR(A0 -> Z h0)
     1.04785137E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.80786640E+01   # Gamma(Hp)
     1.13299758E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     4.84391841E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.37012619E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     8.45314463E-07   2        -1         2   # BR(Hp -> Down up)
     1.40544375E-05   2        -3         2   # BR(Hp -> Strange up)
     8.96426941E-06   2        -5         2   # BR(Hp -> Bottom up)
     3.98904060E-08   2        -1         4   # BR(Hp -> Down charm)
     3.04652657E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.25531407E-03   2        -5         4   # BR(Hp -> Bottom charm)
     1.02938475E-07   2        -1         6   # BR(Hp -> Down top)
     2.66000326E-06   2        -3         6   # BR(Hp -> Strange top)
     8.05356433E-01   2        -5         6   # BR(Hp -> Bottom top)
     5.05364173E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     4.69826225E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     3.17949895E-04   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     7.28092306E-06   2        24        25   # BR(Hp -> W h0)
     3.30642688E-10   2        24        35   # BR(Hp -> W HH)
     3.31327455E-10   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.96921139E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.27966199E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.27965891E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00000241E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    7.79052236E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    7.81458238E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.96921139E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.27966199E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.27965891E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999998E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.85479043E-09        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999998E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.85479043E-09        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26297375E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.84138475E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.46141196E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.85479043E-09        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999998E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.71718208E-04   # BR(b -> s gamma)
    2    1.58738170E-06   # BR(b -> s mu+ mu-)
    3    3.52552278E-05   # BR(b -> s nu nu)
    4    1.49397265E-15   # BR(Bd -> e+ e-)
    5    6.38173900E-11   # BR(Bd -> mu+ mu-)
    6    1.31608381E-08   # BR(Bd -> tau+ tau-)
    7    5.05511068E-14   # BR(Bs -> e+ e-)
    8    2.15943088E-09   # BR(Bs -> mu+ mu-)
    9    4.51522299E-07   # BR(Bs -> tau+ tau-)
   10    8.74964738E-05   # BR(B_u -> tau nu)
   11    9.03805076E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42157535E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93243207E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15854640E-03   # epsilon_K
   17    2.28168179E-15   # Delta(M_K)
   18    2.47988572E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29033371E-11   # BR(K^+ -> pi^+ nu nu)
   20   -3.76385548E-16   # Delta(g-2)_electron/2
   21   -1.60917054E-11   # Delta(g-2)_muon/2
   22   -4.55582503E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.62215002E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.41522125E-01   # C7
     0305 4322   00   2    -9.00357498E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.47562978E-01   # C8
     0305 6321   00   2    -9.62952110E-04   # C8'
 03051111 4133   00   0     1.62176922E+00   # C9 e+e-
 03051111 4133   00   2     1.62217374E+00   # C9 e+e-
 03051111 4233   00   2     5.88905697E-05   # C9' e+e-
 03051111 4137   00   0    -4.44446132E+00   # C10 e+e-
 03051111 4137   00   2    -4.44306359E+00   # C10 e+e-
 03051111 4237   00   2    -4.37709007E-04   # C10' e+e-
 03051313 4133   00   0     1.62176922E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62217357E+00   # C9 mu+mu-
 03051313 4233   00   2     5.88770013E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.44446132E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44306377E+00   # C10 mu+mu-
 03051313 4237   00   2    -4.37695654E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50504839E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     9.47530418E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50504839E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     9.47559680E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50504839E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     9.55805762E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85814510E-07   # C7
     0305 4422   00   2    -6.36356980E-06   # C7
     0305 4322   00   2    -1.81965665E-06   # C7'
     0305 6421   00   0     3.30475643E-07   # C8
     0305 6421   00   2     5.25530584E-06   # C8
     0305 6321   00   2    -6.40602626E-07   # C8'
 03051111 4133   00   2     2.20216712E-07   # C9 e+e-
 03051111 4233   00   2     4.66714148E-06   # C9' e+e-
 03051111 4137   00   2    -4.55962574E-07   # C10 e+e-
 03051111 4237   00   2    -3.47791763E-05   # C10' e+e-
 03051313 4133   00   2     2.20215080E-07   # C9 mu+mu-
 03051313 4233   00   2     4.66714050E-06   # C9' mu+mu-
 03051313 4137   00   2    -4.55961378E-07   # C10 mu+mu-
 03051313 4237   00   2    -3.47791792E-05   # C10' mu+mu-
 03051212 4137   00   2     1.13684030E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     7.52887609E-06   # C11' nu_1 nu_1
 03051414 4137   00   2     1.13684087E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     7.52887609E-06   # C11' nu_2 nu_2
 03051616 4137   00   2     1.13700330E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     7.52887607E-06   # C11' nu_3 nu_3
