# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  15:10
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.48909675E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.58060507E+03  # scale for input parameters
    1    4.58249952E+01  # M_1
    2   -4.58968324E+02  # M_2
    3    3.85763076E+03  # M_3
   11    2.63994718E+03  # A_t
   12    1.17407924E+03  # A_b
   13    1.92865231E+03  # A_tau
   23    1.61801572E+02  # mu
   25    3.37418474E+01  # tan(beta)
   26    3.23714795E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.37077832E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.78050792E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.13309966E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.58060507E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.58060507E+03  # (SUSY scale)
  1  1     8.38805349E-06   # Y_u(Q)^DRbar
  2  2     4.26113117E-03   # Y_c(Q)^DRbar
  3  3     1.01334424E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.58060507E+03  # (SUSY scale)
  1  1     5.68735445E-04   # Y_d(Q)^DRbar
  2  2     1.08059735E-02   # Y_s(Q)^DRbar
  3  3     5.64007346E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.58060507E+03  # (SUSY scale)
  1  1     9.92484941E-05   # Y_e(Q)^DRbar
  2  2     2.05214408E-02   # Y_mu(Q)^DRbar
  3  3     3.45136875E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.58060507E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     2.63994696E+03   # A_t(Q)^DRbar
Block Ad Q=  3.58060507E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.17407936E+03   # A_b(Q)^DRbar
Block Ae Q=  3.58060507E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.92865231E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.58060507E+03  # soft SUSY breaking masses at Q
   1    4.58249952E+01  # M_1
   2   -4.58968324E+02  # M_2
   3    3.85763076E+03  # M_3
  21    1.04271937E+07  # M^2_(H,d)
  22    2.32916404E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.37077832E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.78050792E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.13309966E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.23910543E+02  # h0
        35     3.23625822E+03  # H0
        36     3.23714795E+03  # A0
        37     3.23760240E+03  # H+
   1000001     1.00981646E+04  # ~d_L
   2000001     1.00734022E+04  # ~d_R
   1000002     1.00977949E+04  # ~u_L
   2000002     1.00762778E+04  # ~u_R
   1000003     1.00981679E+04  # ~s_L
   2000003     1.00734077E+04  # ~s_R
   1000004     1.00977982E+04  # ~c_L
   2000004     1.00762788E+04  # ~c_R
   1000005     4.23315201E+03  # ~b_1
   2000005     4.45362709E+03  # ~b_2
   1000006     2.87697123E+03  # ~t_1
   2000006     4.45632981E+03  # ~t_2
   1000011     1.00213142E+04  # ~e_L-
   2000011     1.00091249E+04  # ~e_R-
   1000012     1.00205429E+04  # ~nu_eL
   1000013     1.00213291E+04  # ~mu_L-
   2000013     1.00091538E+04  # ~mu_R-
   1000014     1.00205577E+04  # ~nu_muL
   1000015     1.00175697E+04  # ~tau_1-
   2000015     1.00256632E+04  # ~tau_2-
   1000016     1.00248742E+04  # ~nu_tauL
   1000021     4.33328222E+03  # ~g
   1000022     4.26215970E+01  # ~chi_10
   1000023     1.74230037E+02  # ~chi_20
   1000025     1.91571260E+02  # ~chi_30
   1000035     5.17896376E+02  # ~chi_40
   1000024     1.75156209E+02  # ~chi_1+
   1000037     5.18027559E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.87107181E-02   # alpha
Block Hmix Q=  3.58060507E+03  # Higgs mixing parameters
   1    1.61801572E+02  # mu
   2    3.37418474E+01  # tan[beta](Q)
   3    2.43070477E+02  # v(Q)
   4    1.04791269E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -3.00178595E-02   # Re[R_st(1,1)]
   1  2     9.99549363E-01   # Re[R_st(1,2)]
   2  1    -9.99549363E-01   # Re[R_st(2,1)]
   2  2    -3.00178595E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     3.78625457E-03   # Re[R_sb(1,1)]
   1  2     9.99992832E-01   # Re[R_sb(1,2)]
   2  1    -9.99992832E-01   # Re[R_sb(2,1)]
   2  2     3.78625457E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     3.68937788E-02   # Re[R_sta(1,1)]
   1  2     9.99319193E-01   # Re[R_sta(1,2)]
   2  1    -9.99319193E-01   # Re[R_sta(2,1)]
   2  2     3.68937788E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.65158469E-01   # Re[N(1,1)]
   1  2    -1.12483740E-02   # Re[N(1,2)]
   1  3    -2.52705516E-01   # Re[N(1,3)]
   1  4     6.69516720E-02   # Re[N(1,4)]
   2  1     1.34506711E-01   # Re[N(2,1)]
   2  2     1.60661983E-01   # Re[N(2,2)]
   2  3    -7.01375857E-01   # Re[N(2,3)]
   2  4    -6.81298451E-01   # Re[N(2,4)]
   3  1     2.24026303E-01   # Re[N(3,1)]
   3  2    -8.45338364E-02   # Re[N(3,2)]
   3  3    -6.64251882E-01   # Re[N(3,3)]
   3  4     7.08121235E-01   # Re[N(3,4)]
   4  1     1.37582847E-02   # Re[N(4,1)]
   4  2    -9.83318479E-01   # Re[N(4,2)]
   4  3    -5.46009774E-02   # Re[N(4,3)]
   4  4    -1.72957252E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -7.78142011E-02   # Re[U(1,1)]
   1  2     9.96967878E-01   # Re[U(1,2)]
   2  1    -9.96967878E-01   # Re[U(2,1)]
   2  2    -7.78142011E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     2.45904383E-01   # Re[V(1,1)]
   1  2     9.69294091E-01   # Re[V(1,2)]
   2  1     9.69294091E-01   # Re[V(2,1)]
   2  2    -2.45904383E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02880503E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.31573891E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.80826028E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     5.01551730E-02    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     1.88293828E-04    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.44221202E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.48920094E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     1.65253395E-02    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.77512292E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     2.89766011E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     3.70152818E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.04737600E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.04558000E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.28585509E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.88396108E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     5.07213450E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.92596691E-04    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     1.65092904E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.44305323E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.48798021E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     1.68017633E-02    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     6.33815813E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.89599258E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     3.69937594E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.04385985E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     9.78771017E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     4.97147856E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.30079555E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.29393741E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     2.43281973E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
DECAY   2000015     1.67930558E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.04418345E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     8.28571710E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     6.46514527E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.49810474E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.80837049E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.20430697E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.44225845E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     7.77885216E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.41029706E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     1.27438867E-02    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     2.98527752E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     3.69614145E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.71568128E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.44309958E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     7.77432965E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.40889576E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     1.27364777E-02    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     2.98354197E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     3.75177965E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.71239336E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.68030605E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.67971472E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.06972756E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     1.09432056E-02    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.56347510E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.72182800E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.91659610E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.33706286E+02   # ~d_R
#    BR                NDA      ID1      ID2
     9.81562338E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.90589936E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     5.28522743E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.89463158E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.64617859E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.72529506E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.24919893E-03    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.03468527E-03    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     6.46451290E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     8.09393974E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.32244351E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.98291947E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.33753482E+02   # ~s_R
#    BR                NDA      ID1      ID2
     9.81755267E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     2.12067952E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     5.47753511E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.89376697E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.64645256E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.72747209E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.26645171E-03    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.05016200E-03    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     6.46425903E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     8.14513462E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.32239274E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.98259536E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     5.56611848E+01   # ~b_1
#    BR                NDA      ID1      ID2
     7.06587538E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.36916855E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.13207958E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.24507809E-03    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.75464748E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.50475138E-03    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.88041789E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.21382189E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     7.53889434E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     6.76270234E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     9.90656082E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     4.52810158E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     2.30903353E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     5.30869540E-03    2     1000021         5   # BR(~b_2 -> ~g b)
     5.67515160E-02    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
DECAY   2000002     5.50870584E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.80499629E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     7.38585384E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     2.04859702E-03    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.59155163E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.64600718E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.24240190E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     2.27792736E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.33173497E-04    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     6.40033438E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     8.08305010E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.25003180E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.98256923E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.50877928E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.80494875E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     7.41640176E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     2.05188144E-03    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.59142502E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.64628081E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.24233484E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     2.28038088E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.35918231E-04    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     6.40008878E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     8.11768327E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.24998286E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.98224509E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.22366444E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.01433829E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.21315976E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.41975890E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.13127888E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.52139524E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.31091312E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.90846753E+02   # ~t_2
#    BR                NDA      ID1      ID2
     6.05893535E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.22880348E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.35884253E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.11219412E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.54614539E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.93018416E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.80254795E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     4.82952032E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     7.40399073E-02   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99999625E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     3.95103928E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     1.47765457E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.78328176E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.39560225E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.48250604E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.17693975E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     8.03532700E-04    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     5.23782708E-04    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     8.94960881E-02   # chi^0_2
#    BR                NDA      ID1      ID2
     8.75885646E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     1.24073254E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     5.73063369E-02   # chi^0_3
#    BR                NDA      ID1      ID2
     5.62133728E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     4.37729378E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     4.79544110E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.34865953E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.34865953E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     6.29394302E-03    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     4.65873202E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.72916688E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     6.33081228E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.36751362E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.59594885E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     8.66596540E-04    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     8.66596540E-04    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     4.14920161E+01   # ~g
#    BR                NDA      ID1      ID2
     2.13195352E-04    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     2.13195352E-04    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     4.85260503E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     4.85260503E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     3.22157174E-03    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     3.22157174E-03    2    -1000005         5   # BR(~g -> ~b^*_1 b)
#    BR                NDA      ID1      ID2       ID3
     1.56871070E-04    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     2.61758638E-03    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     9.69365767E-04    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     2.73909966E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     8.70451912E-04    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     1.31583271E-03    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     1.35778797E-03    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     3.83209469E-03    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     3.83209469E-03    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     2.23843931E-03    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     2.23843931E-03    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     4.19582995E-03   # Gamma(h0)
     2.08189790E-03   2        22        22   # BR(h0 -> photon photon)
     1.25054109E-03   2        22        23   # BR(h0 -> photon Z)
     2.22849792E-02   2        23        23   # BR(h0 -> Z Z)
     1.88145086E-01   2       -24        24   # BR(h0 -> W W)
     6.54701864E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.22687609E-09   2       -11        11   # BR(h0 -> Electron electron)
     1.88018514E-04   2       -13        13   # BR(h0 -> Muon muon)
     5.42113628E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.20265782E-07   2        -2         2   # BR(h0 -> Up up)
     2.33398779E-02   2        -4         4   # BR(h0 -> Charm charm)
     4.90019475E-07   2        -1         1   # BR(h0 -> Down down)
     1.77228359E-04   2        -3         3   # BR(h0 -> Strange strange)
     4.71028320E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     1.71821887E-01   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     8.58696082E+01   # Gamma(HH)
     4.05951294E-08   2        22        22   # BR(HH -> photon photon)
     9.97048644E-08   2        22        23   # BR(HH -> photon Z)
     2.49874350E-07   2        23        23   # BR(HH -> Z Z)
     6.83775940E-08   2       -24        24   # BR(HH -> W W)
     4.05040559E-06   2        21        21   # BR(HH -> gluon gluon)
     6.49783280E-09   2       -11        11   # BR(HH -> Electron electron)
     2.89320340E-04   2       -13        13   # BR(HH -> Muon muon)
     8.35602247E-02   2       -15        15   # BR(HH -> Tau tau)
     7.29818531E-14   2        -2         2   # BR(HH -> Up up)
     1.41578937E-08   2        -4         4   # BR(HH -> Charm charm)
     1.02070544E-03   2        -6         6   # BR(HH -> Top top)
     4.70834871E-07   2        -1         1   # BR(HH -> Down down)
     1.70299581E-04   2        -3         3   # BR(HH -> Strange strange)
     4.29659275E-01   2        -5         5   # BR(HH -> Bottom bottom)
     2.19807733E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.35137315E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     1.35137315E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     2.14445980E-03   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     5.47558875E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     2.30670872E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.03093002E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     7.89986311E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.55846443E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.43341011E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     7.22909610E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     6.02569684E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     6.17332880E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     1.09988366E-03   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     1.76766756E-06   2        25        25   # BR(HH -> h0 h0)
DECAY        36     8.32530674E+01   # Gamma(A0)
     2.98002563E-09   2        22        22   # BR(A0 -> photon photon)
     4.29254037E-08   2        22        23   # BR(A0 -> photon Z)
     9.85990689E-06   2        21        21   # BR(A0 -> gluon gluon)
     6.30386787E-09   2       -11        11   # BR(A0 -> Electron electron)
     2.80684547E-04   2       -13        13   # BR(A0 -> Muon muon)
     8.10661853E-02   2       -15        15   # BR(A0 -> Tau tau)
     6.74241862E-14   2        -2         2   # BR(A0 -> Up up)
     1.30792263E-08   2        -4         4   # BR(A0 -> Charm charm)
     9.53726093E-04   2        -6         6   # BR(A0 -> Top top)
     4.56762507E-07   2        -1         1   # BR(A0 -> Down down)
     1.65210550E-04   2        -3         3   # BR(A0 -> Strange strange)
     4.16824077E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.21045400E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.39990562E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     1.39990562E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.66166859E-03   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     5.84189834E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     2.14703042E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.23168488E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     8.78496182E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     1.45313055E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.10605586E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     6.88904222E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     7.11939439E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     6.91067786E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     8.62162518E-04   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     4.44441833E-07   2        23        25   # BR(A0 -> Z h0)
     1.47081610E-40   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     9.03580732E+01   # Gamma(Hp)
     7.21345359E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.08397670E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     8.72324066E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     4.53492818E-07   2        -1         2   # BR(Hp -> Down up)
     7.66134434E-06   2        -3         2   # BR(Hp -> Strange up)
     4.76148160E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.18832716E-08   2        -1         4   # BR(Hp -> Down charm)
     1.63445185E-04   2        -3         4   # BR(Hp -> Strange charm)
     6.66777837E-04   2        -5         4   # BR(Hp -> Bottom charm)
     7.51929806E-08   2        -1         6   # BR(Hp -> Down top)
     1.88187358E-06   2        -3         6   # BR(Hp -> Strange top)
     4.50724878E-01   2        -5         6   # BR(Hp -> Bottom top)
     4.54211313E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     1.56302685E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     3.90516265E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.42506465E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     2.58889528E-04   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.20273580E-01   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     1.32893129E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     1.95466684E-07   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     4.09886382E-07   2        24        25   # BR(Hp -> W h0)
     6.61147958E-14   2        24        35   # BR(Hp -> W HH)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.39047975E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.13857322E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.13851227E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00005354E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    8.24802686E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    8.78339241E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.39047975E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.13857322E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.13851227E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999158E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    8.41618977E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999158E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    8.41618977E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26824444E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    4.21713817E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.09820676E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    8.41618977E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999158E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.27800453E-04   # BR(b -> s gamma)
    2    1.58861113E-06   # BR(b -> s mu+ mu-)
    3    3.52357693E-05   # BR(b -> s nu nu)
    4    2.48518308E-15   # BR(Bd -> e+ e-)
    5    1.06164294E-10   # BR(Bd -> mu+ mu-)
    6    2.22299501E-08   # BR(Bd -> tau+ tau-)
    7    8.33271022E-14   # BR(Bs -> e+ e-)
    8    3.55973470E-09   # BR(Bs -> mu+ mu-)
    9    7.55260686E-07   # BR(Bs -> tau+ tau-)
   10    9.61297702E-05   # BR(B_u -> tau nu)
   11    9.92983723E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42158859E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93683200E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15808421E-03   # epsilon_K
   17    2.28166664E-15   # Delta(M_K)
   18    2.47871646E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28746838E-11   # BR(K^+ -> pi^+ nu nu)
   20   -3.11745085E-16   # Delta(g-2)_electron/2
   21   -1.33281812E-11   # Delta(g-2)_muon/2
   22   -3.77883194E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.76717800E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.96915403E-01   # C7
     0305 4322   00   2    -1.37333853E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.03901035E-01   # C8
     0305 6321   00   2    -1.62816638E-04   # C8'
 03051111 4133   00   0     1.62293603E+00   # C9 e+e-
 03051111 4133   00   2     1.62323088E+00   # C9 e+e-
 03051111 4233   00   2     2.13174390E-04   # C9' e+e-
 03051111 4137   00   0    -4.44562813E+00   # C10 e+e-
 03051111 4137   00   2    -4.44235463E+00   # C10 e+e-
 03051111 4237   00   2    -1.58572994E-03   # C10' e+e-
 03051313 4133   00   0     1.62293603E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62323077E+00   # C9 mu+mu-
 03051313 4233   00   2     2.13174209E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44562813E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44235474E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.58572995E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50464289E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     3.43183321E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50464289E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     3.43183349E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50464289E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     3.43191312E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85816739E-07   # C7
     0305 4422   00   2    -3.69938434E-06   # C7
     0305 4322   00   2    -3.59704485E-07   # C7'
     0305 6421   00   0     3.30477553E-07   # C8
     0305 6421   00   2    -3.75467382E-06   # C8
     0305 6321   00   2    -1.91782624E-07   # C8'
 03051111 4133   00   2     4.44994980E-07   # C9 e+e-
 03051111 4233   00   2     4.29482201E-06   # C9' e+e-
 03051111 4137   00   2     9.78616798E-09   # C10 e+e-
 03051111 4237   00   2    -3.19506562E-05   # C10' e+e-
 03051313 4133   00   2     4.44992941E-07   # C9 mu+mu-
 03051313 4233   00   2     4.29482113E-06   # C9' mu+mu-
 03051313 4137   00   2     9.78826851E-09   # C10 mu+mu-
 03051313 4237   00   2    -3.19506589E-05   # C10' mu+mu-
 03051212 4137   00   2     1.96328800E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     6.91475544E-06   # C11' nu_1 nu_1
 03051414 4137   00   2     1.96329858E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     6.91475544E-06   # C11' nu_2 nu_2
 03051616 4137   00   2     1.96630535E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     6.91475542E-06   # C11' nu_3 nu_3
