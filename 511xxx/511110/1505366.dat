# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  17:34
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    1.84194121E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.21081820E+03  # scale for input parameters
    1    1.97153867E+02  # M_1
    2   -4.87272463E+02  # M_2
    3    1.94031048E+03  # M_3
   11   -4.34651713E+03  # A_t
   12    1.15832097E+03  # A_b
   13   -7.73603755E+02  # A_tau
   23   -1.94529705E+02  # mu
   25    1.76268481E+01  # tan(beta)
   26    3.59630391E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.18688884E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.58471624E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.55297057E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.21081820E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.21081820E+03  # (SUSY scale)
  1  1     8.39785376E-06   # Y_u(Q)^DRbar
  2  2     4.26610971E-03   # Y_c(Q)^DRbar
  3  3     1.01452819E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.21081820E+03  # (SUSY scale)
  1  1     2.97456331E-04   # Y_d(Q)^DRbar
  2  2     5.65167029E-03   # Y_s(Q)^DRbar
  3  3     2.94983471E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.21081820E+03  # (SUSY scale)
  1  1     5.19083049E-05   # Y_e(Q)^DRbar
  2  2     1.07329912E-02   # Y_mu(Q)^DRbar
  3  3     1.80511254E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.21081820E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -4.34651711E+03   # A_t(Q)^DRbar
Block Ad Q=  3.21081820E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.15832095E+03   # A_b(Q)^DRbar
Block Ae Q=  3.21081820E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -7.73603750E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.21081820E+03  # soft SUSY breaking masses at Q
   1    1.97153867E+02  # M_1
   2   -4.87272463E+02  # M_2
   3    1.94031048E+03  # M_3
  21    4.74218551E+04  # M^2_(H,d)
  22    7.84981889E+04  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.18688884E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.58471624E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.55297057E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.25799017E+02  # h0
        35     3.59871481E+02  # H0
        36     3.59630391E+02  # A0
        37     3.75344054E+02  # H+
   1000001     1.01163940E+04  # ~d_L
   2000001     1.00910489E+04  # ~d_R
   1000002     1.01160303E+04  # ~u_L
   2000002     1.00948877E+04  # ~u_R
   1000003     1.01163959E+04  # ~s_L
   2000003     1.00910512E+04  # ~s_R
   1000004     1.01160321E+04  # ~c_L
   2000004     1.00948890E+04  # ~c_R
   1000005     2.25681067E+03  # ~b_1
   2000005     2.62509788E+03  # ~b_2
   1000006     2.25470921E+03  # ~t_1
   2000006     4.57236502E+03  # ~t_2
   1000011     1.00217150E+04  # ~e_L-
   2000011     1.00083266E+04  # ~e_R-
   1000012     1.00209451E+04  # ~nu_eL
   1000013     1.00217210E+04  # ~mu_L-
   2000013     1.00083383E+04  # ~mu_R-
   1000014     1.00209510E+04  # ~nu_muL
   1000015     1.00116399E+04  # ~tau_1-
   2000015     1.00234209E+04  # ~tau_2-
   1000016     1.00226447E+04  # ~nu_tauL
   1000021     2.32284186E+03  # ~g
   1000022     1.75023216E+02  # ~chi_10
   1000023     1.95679441E+02  # ~chi_20
   1000025     2.33920028E+02  # ~chi_30
   1000035     5.42549386E+02  # ~chi_40
   1000024     1.97042883E+02  # ~chi_1+
   1000037     5.42819406E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -6.59410773E-02   # alpha
Block Hmix Q=  3.21081820E+03  # Higgs mixing parameters
   1   -1.94529705E+02  # mu
   2    1.76268481E+01  # tan[beta](Q)
   3    2.43165951E+02  # v(Q)
   4    1.29334018E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.99231568E-01   # Re[R_st(1,1)]
   1  2     3.91953174E-02   # Re[R_st(1,2)]
   2  1    -3.91953174E-02   # Re[R_st(2,1)]
   2  2     9.99231568E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99986067E-01   # Re[R_sb(1,1)]
   1  2     5.27879128E-03   # Re[R_sb(1,2)]
   2  1    -5.27879128E-03   # Re[R_sb(2,1)]
   2  2    -9.99986067E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -1.97903687E-02   # Re[R_sta(1,1)]
   1  2     9.99804151E-01   # Re[R_sta(1,2)]
   2  1    -9.99804151E-01   # Re[R_sta(2,1)]
   2  2    -1.97903687E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     7.40143233E-01   # Re[N(1,1)]
   1  2     4.70556458E-02   # Re[N(1,2)]
   1  3    -5.12613440E-01   # Re[N(1,3)]
   1  4    -4.32667565E-01   # Re[N(1,4)]
   2  1     7.97640813E-02   # Re[N(2,1)]
   2  2     1.75736538E-01   # Re[N(2,2)]
   2  3     7.06233878E-01   # Re[N(2,3)]
   2  4    -6.81166698E-01   # Re[N(2,4)]
   3  1     6.67616046E-01   # Re[N(3,1)]
   3  2    -5.74303679E-02   # Re[N(3,2)]
   3  3     4.82743831E-01   # Re[N(3,3)]
   3  4     5.63869631E-01   # Re[N(3,4)]
   4  1     1.07004773E-02   # Re[N(4,1)]
   4  2    -9.81633429E-01   # Re[N(4,2)]
   4  3     7.36176888E-02   # Re[N(4,3)]
   4  4    -1.75675119E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.04480374E-01   # Re[U(1,1)]
   1  2    -9.94526949E-01   # Re[U(1,2)]
   2  1    -9.94526949E-01   # Re[U(2,1)]
   2  2     1.04480374E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     2.49938727E-01   # Re[V(1,1)]
   1  2     9.68261655E-01   # Re[V(1,2)]
   2  1     9.68261655E-01   # Re[V(2,1)]
   2  2    -2.49938727E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02465790E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     5.47930329E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     6.36271495E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     4.45593023E-01    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     1.13922551E-04    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.44152288E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     5.98684202E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     1.45777861E-02    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     2.73679910E-02    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     2.89734358E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.67555314E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.01775892E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02924723E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     5.47550740E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     6.58420796E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     4.45293086E-01    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.16275823E-04    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     4.50740985E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.44175324E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     5.99007234E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     1.46548590E-02    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     2.74007186E-02    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.89689099E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.67449057E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.01680110E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     6.32674090E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     4.64017689E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     5.71839386E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     3.75655972E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.37086183E-04    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.02901346E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.03968613E-04    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.50634303E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     6.78849466E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     3.50285298E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     3.71725918E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.77759308E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.75045874E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.76404165E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.44157506E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     3.71045039E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     5.41828733E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     5.24058071E-02    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     2.96528791E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     3.81975785E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.70345032E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.44180538E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     3.70985985E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     5.41742499E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     5.23974665E-02    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     2.96481599E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     3.83489176E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.70255993E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.50673955E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     3.55058116E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     5.18483416E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     5.01478479E-02    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.83752998E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     7.91679697E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.46240538E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.19975730E+02   # ~d_R
#    BR                NDA      ID1      ID2
     4.28401820E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     3.48391716E-03    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.92181390E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.50779303E+02   # ~d_L
#    BR                NDA      ID1      ID2
     3.75646349E-04    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.36461763E-03    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.62527891E-03    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     5.03376784E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.14177843E-03    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.02936811E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.42218189E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.19988709E+02   # ~s_R
#    BR                NDA      ID1      ID2
     4.28627453E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     3.48592253E-03    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.92163780E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.50789557E+02   # ~s_L
#    BR                NDA      ID1      ID2
     3.77620738E-04    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.36835695E-03    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.62701361E-03    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.03371217E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.14579732E-03    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.02935859E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.42208231E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     7.55463852E+01   # ~b_1
#    BR                NDA      ID1      ID2
     1.45148503E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.88728520E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.56183528E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.13095047E-01    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     5.59838617E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.68060280E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     2.02125176E+01   # ~b_2
#    BR                NDA      ID1      ID2
     9.72727130E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.10994282E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     8.40916261E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.39026934E-03    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.17534474E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     2.77121470E-03    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     4.82851771E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     2.84216572E-04    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.36969881E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     1.20036687E-03    2     1000005        36   # BR(~b_2 -> ~b_1 A^0)
     2.93731680E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
     1.17836521E-03    2     1000005        35   # BR(~b_2 -> ~b_1 H^0)
DECAY   2000002     7.37204355E+02   # ~u_R
#    BR                NDA      ID1      ID2
     1.67419479E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.94412375E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     1.36151552E-02    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.69445003E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.50768063E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.67319686E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.88673884E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.97443753E-04    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     4.99471881E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     6.53387725E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     9.75692597E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.42192296E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.37211742E+02   # ~c_R
#    BR                NDA      ID1      ID2
     1.67427080E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.96708915E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     1.36165966E-02    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.69435401E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.50778274E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.67398281E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.88871316E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.98808547E-04    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     4.99467254E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     6.54124652E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     9.75681885E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.42182335E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     7.52465631E+01   # ~t_1
#    BR                NDA      ID1      ID2
     1.19993870E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.80393001E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.83562025E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.27227113E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     5.99942377E-02    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.26466423E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     2.36298640E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     4.90552250E+02   # ~t_2
#    BR                NDA      ID1      ID2
     4.56951090E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     8.87364974E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     7.11868188E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     4.60618067E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.80393886E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     9.38061948E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     4.01219723E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     8.79646925E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.32022558E-03    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     4.39698559E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     6.61021915E-04    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     6.40745573E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     7.81750732E-04    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     2.86103231E-06   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.36173768E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.30800116E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.12056149E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.12056537E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.08909412E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     4.66732364E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     1.31465384E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.79550236E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.23890975E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.48541723E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.13237633E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.15480344E-03    3     1000024         5        -5   # BR(chi^+_2 -> chi^+_1 b b_bar)
     1.43193437E-04    3     1000024        15       -15   # BR(chi^+_2 -> chi^+_1 tau^- tau^+)
     7.27073697E-04    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     9.13013988E-04    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     2.60907854E-04    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     5.09297820E-06   # chi^0_2
#    BR                NDA      ID1      ID2
     7.87694786E-02    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.17121237E-01    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     1.11437046E-01    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.50171713E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.50135525E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     9.33165383E-02    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     3.38883279E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.38754358E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     3.04643303E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     2.00820368E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     2.34120113E-04   # chi^0_3
#    BR                NDA      ID1      ID2
     5.39445933E-04    2     1000022        22   # BR(chi^0_3 -> chi^0_1 photon)
     1.52502151E-02    2     1000023        22   # BR(chi^0_3 -> chi^0_2 photon)
#    BR                NDA      ID1      ID2       ID3
     6.99761448E-02    3     1000023         2        -2   # BR(chi^0_3 -> chi^0_2 u u_bar)
     6.90182537E-02    3     1000023         4        -4   # BR(chi^0_3 -> chi^0_2 c c_bar)
     8.97233982E-02    3     1000023         1        -1   # BR(chi^0_3 -> chi^0_2 d d_bar)
     8.97174926E-02    3     1000023         3        -3   # BR(chi^0_3 -> chi^0_2 s s_bar)
     7.90249921E-02    3     1000023         5        -5   # BR(chi^0_3 -> chi^0_2 b b_bar)
     2.02485335E-02    3     1000023        11       -11   # BR(chi^0_3 -> chi^0_2 e^- e^+)
     2.02464133E-02    3     1000023        13       -13   # BR(chi^0_3 -> chi^0_2 mu^- mu^+)
     1.96604883E-02    3     1000023        15       -15   # BR(chi^0_3 -> chi^0_2 tau^- tau^+)
     1.19987601E-01    3     1000023        12       -12   # BR(chi^0_3 -> chi^0_2 nu_e nu_bar_e)
     6.79469341E-02    3     1000024         1        -2   # BR(chi^0_3 -> chi^+_1 d u_bar)
     6.79469341E-02    3    -1000024        -1         2   # BR(chi^0_3 -> chi^-_1 d_bar u)
     6.75775353E-02    3     1000024         3        -4   # BR(chi^0_3 -> chi^+_1 s c_bar)
     6.75775353E-02    3    -1000024        -3         4   # BR(chi^0_3 -> chi^-_1 s_bar c)
     2.26493177E-02    3     1000024        11       -12   # BR(chi^0_3 -> chi^+_1 e^- nu_bar_e)
     2.26493177E-02    3    -1000024       -11        12   # BR(chi^0_3 -> chi^-_1 e^+ nu_e)
     2.26492485E-02    3     1000024        13       -14   # BR(chi^0_3 -> chi^+_1 mu^- nu_bar_mu)
     2.26492485E-02    3    -1000024       -13        14   # BR(chi^0_3 -> chi^-_1 mu^+ nu_mu)
     2.24092543E-02    3     1000024        15       -16   # BR(chi^0_3 -> chi^+_1 tau^- nu_bar_tau)
     2.24092543E-02    3    -1000024       -15        16   # BR(chi^0_3 -> chi^-_1 tau^+ nu_tau)
DECAY   1000035     5.59489676E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.40849598E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.40849598E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     9.59373449E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     3.14185906E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.04323867E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.35964881E-04    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     6.22659465E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.47203350E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     6.30954999E-02    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
     1.15102230E-02    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     5.17276790E-04    3     1000023         5        -5   # BR(chi^0_4 -> chi^0_2 b b_bar)
     7.98698022E-04    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     7.98698022E-04    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     5.77108112E-01   # ~g
#    BR                NDA      ID1      ID2
     1.86877153E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.86877153E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.34026149E-03    2     1000022        21   # BR(~g -> chi^0_1 g)
     1.61558955E-03    2     1000023        21   # BR(~g -> chi^0_2 g)
     7.76458716E-04    2     1000025        21   # BR(~g -> chi^0_3 g)
     3.54437369E-04    2     1000035        21   # BR(~g -> chi^0_4 g)
#    BR                NDA      ID1      ID2       ID3
     7.50296137E-02    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     3.81806794E-03    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     1.79093876E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     4.17638646E-03    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.15729047E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     3.26312342E-03    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     6.65873236E-02    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     3.08896014E-02    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     3.08896014E-02    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     5.61321764E-02    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     5.61321764E-02    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     4.48279263E-03   # Gamma(h0)
     2.10183769E-03   2        22        22   # BR(h0 -> photon photon)
     1.39473187E-03   2        22        23   # BR(h0 -> photon Z)
     2.64271886E-02   2        23        23   # BR(h0 -> Z Z)
     2.14910672E-01   2       -24        24   # BR(h0 -> W W)
     6.26118214E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.36877953E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.38813375E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.88589404E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.02809263E-07   2        -2         2   # BR(h0 -> Up up)
     1.99547206E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.21151988E-07   2        -1         1   # BR(h0 -> Down down)
     2.24657781E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.03275887E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.87640132E+00   # Gamma(HH)
     1.08208058E-06   2        22        22   # BR(HH -> photon photon)
     6.86440448E-07   2        22        23   # BR(HH -> photon Z)
     3.82599603E-04   2        23        23   # BR(HH -> Z Z)
     8.37972364E-04   2       -24        24   # BR(HH -> W W)
     4.23624499E-04   2        21        21   # BR(HH -> gluon gluon)
     8.95068471E-09   2       -11        11   # BR(HH -> Electron electron)
     3.98268998E-04   2       -13        13   # BR(HH -> Muon muon)
     1.14972139E-01   2       -15        15   # BR(HH -> Tau tau)
     2.14595384E-12   2        -2         2   # BR(HH -> Up up)
     4.15997082E-07   2        -4         4   # BR(HH -> Charm charm)
     1.06076449E-03   2        -6         6   # BR(HH -> Top top)
     8.76038346E-07   2        -1         1   # BR(HH -> Down down)
     3.16855427E-04   2        -3         3   # BR(HH -> Strange strange)
     8.65199376E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.76492060E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.02386192E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     4.40179050E-03   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.87846370E+00   # Gamma(A0)
     1.11065445E-06   2        22        22   # BR(A0 -> photon photon)
     9.82336276E-07   2        22        23   # BR(A0 -> photon Z)
     3.15934253E-04   2        21        21   # BR(A0 -> gluon gluon)
     8.90333950E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.96162400E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.14375328E-01   2       -15        15   # BR(A0 -> Tau tau)
     1.33611333E-12   2        -2         2   # BR(A0 -> Up up)
     2.59335970E-07   2        -4         4   # BR(A0 -> Charm charm)
     9.21615724E-03   2        -6         6   # BR(A0 -> Top top)
     8.71490595E-07   2        -1         1   # BR(A0 -> Down down)
     3.15210555E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.60889081E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.36971126E-02   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     2.35156702E-04   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     5.56624422E-04   2        23        25   # BR(A0 -> Z h0)
     1.75648403E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.51447941E+00   # Gamma(Hp)
     1.39052359E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     5.94492170E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.68148797E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     1.18041212E-06   2        -1         2   # BR(Hp -> Down up)
     1.93579321E-05   2        -3         2   # BR(Hp -> Strange up)
     1.26127370E-05   2        -5         2   # BR(Hp -> Bottom up)
     7.56681385E-08   2        -1         4   # BR(Hp -> Down charm)
     4.25877136E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.76619519E-03   2        -5         4   # BR(Hp -> Bottom charm)
     1.50868070E-06   2        -1         6   # BR(Hp -> Down top)
     3.32737616E-05   2        -3         6   # BR(Hp -> Strange top)
     7.84570205E-01   2        -5         6   # BR(Hp -> Bottom top)
     4.27019054E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     8.27241088E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     8.95671753E-04   2        24        25   # BR(Hp -> W h0)
     7.65201672E-07   2        24        35   # BR(Hp -> W HH)
     8.26564892E-07   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.35340371E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    3.10352370E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    3.10705774E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.98862578E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    4.35590137E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.21847897E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.35340371E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    3.10352370E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    3.10705774E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99914066E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    8.59338363E-05        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99914066E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    8.59338363E-05        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.23964721E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    5.86568261E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    7.79921903E-02        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    8.59338363E-05        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99914066E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    4.57662429E-04   # BR(b -> s gamma)
    2    1.59626029E-06   # BR(b -> s mu+ mu-)
    3    3.52684394E-05   # BR(b -> s nu nu)
    4    2.16524754E-15   # BR(Bd -> e+ e-)
    5    9.24969483E-11   # BR(Bd -> mu+ mu-)
    6    1.93651831E-08   # BR(Bd -> tau+ tau-)
    7    7.36565013E-14   # BR(Bs -> e+ e-)
    8    3.14660450E-09   # BR(Bs -> mu+ mu-)
    9    6.67488583E-07   # BR(Bs -> tau+ tau-)
   10    8.25660792E-05   # BR(B_u -> tau nu)
   11    8.52875988E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42395830E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93326986E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15981437E-03   # epsilon_K
   17    2.28170087E-15   # Delta(M_K)
   18    2.48133289E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29359896E-11   # BR(K^+ -> pi^+ nu nu)
   20    1.32228179E-16   # Delta(g-2)_electron/2
   21    5.65317648E-12   # Delta(g-2)_muon/2
   22    1.59978082E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    5.27807632E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -3.21494137E-01   # C7
     0305 4322   00   2    -2.42649505E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -2.23421016E-01   # C8
     0305 6321   00   2    -2.41365716E-03   # C8'
 03051111 4133   00   0     1.61866275E+00   # C9 e+e-
 03051111 4133   00   2     1.61917947E+00   # C9 e+e-
 03051111 4233   00   2    -1.91048387E-05   # C9' e+e-
 03051111 4137   00   0    -4.44135485E+00   # C10 e+e-
 03051111 4137   00   2    -4.44122723E+00   # C10 e+e-
 03051111 4237   00   2     1.45289802E-04   # C10' e+e-
 03051313 4133   00   0     1.61866275E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61917944E+00   # C9 mu+mu-
 03051313 4233   00   2    -1.91196460E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.44135485E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44122725E+00   # C10 mu+mu-
 03051313 4237   00   2     1.45304602E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50532532E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -3.14750848E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50532532E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -3.14718741E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50532532E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -3.05670788E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85817808E-07   # C7
     0305 4422   00   2     5.54514220E-06   # C7
     0305 4322   00   2    -5.24121256E-07   # C7'
     0305 6421   00   0     3.30478468E-07   # C8
     0305 6421   00   2     3.12653536E-05   # C8
     0305 6321   00   2     2.59039563E-07   # C8'
 03051111 4133   00   2     1.82402033E-06   # C9 e+e-
 03051111 4233   00   2     2.29723923E-06   # C9' e+e-
 03051111 4137   00   2     2.91725682E-06   # C10 e+e-
 03051111 4237   00   2    -1.71708171E-05   # C10' e+e-
 03051313 4133   00   2     1.82401898E-06   # C9 mu+mu-
 03051313 4233   00   2     2.29723910E-06   # C9' mu+mu-
 03051313 4137   00   2     2.91725826E-06   # C10 mu+mu-
 03051313 4237   00   2    -1.71708175E-05   # C10' mu+mu-
 03051212 4137   00   2    -5.80795325E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     3.71966589E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -5.80795256E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     3.71966589E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -5.80775628E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     3.71966588E-06   # C11' nu_3 nu_3
