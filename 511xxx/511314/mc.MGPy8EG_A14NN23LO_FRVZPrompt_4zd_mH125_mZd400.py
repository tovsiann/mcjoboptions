params={}
params['mZDinput']  = 4.000000e-01
params['MHSinput']  = 2.000000e+00
params['epsilon']   = 1.000000e-03
params['gXmu']      = 1.220000e-03
params['gXe']       = 1.200000e-03
params['gXpi']      = 9.000000e-01
params['MChi1']     = 2.000000e+00
params['MChi2']     = 5.000000e+00
params['WZp']       = "DECAY 3000001 1.000000e-03 # WZp"
params['mH']        = 125
params['nGamma']    = 4
params['decayMode'] = 'normal'




                

include ("MadGraphControl_A14N23LO_FRVZprompt_ggfusion.py")

evgenConfig.keywords = ["exotic", "BSMHiggs", "BSM", "darkPhoton"]

                    

