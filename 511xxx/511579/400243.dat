# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 15.03.2021,  09:24
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    5.92438394E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.57230171E+03  # scale for input parameters
    1    3.39044132E+02  # M_1
    2   -1.45283015E+03  # M_2
    3    1.16849754E+03  # M_3
   11   -2.53508365E+03  # A_t
   12   -1.11223895E+03  # A_b
   13    9.20776841E+02  # A_tau
   23    2.59397125E+02  # mu
   25    5.93347519E+01  # tan(beta)
   26    1.80758800E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.45423663E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.72219343E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.03168808E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.57230171E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.57230171E+03  # (SUSY scale)
  1  1     8.38556280E-06   # Y_u(Q)^DRbar
  2  2     4.25986590E-03   # Y_c(Q)^DRbar
  3  3     1.01304334E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.57230171E+03  # (SUSY scale)
  1  1     9.99819480E-04   # Y_d(Q)^DRbar
  2  2     1.89965701E-02   # Y_s(Q)^DRbar
  3  3     9.91507626E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.57230171E+03  # (SUSY scale)
  1  1     1.74475811E-04   # Y_e(Q)^DRbar
  2  2     3.60760640E-02   # Y_mu(Q)^DRbar
  3  3     6.06740048E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.57230171E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -2.53508433E+03   # A_t(Q)^DRbar
Block Ad Q=  3.57230171E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.11223873E+03   # A_b(Q)^DRbar
Block Ae Q=  3.57230171E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     9.20776829E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.57230171E+03  # soft SUSY breaking masses at Q
   1    3.39044132E+02  # M_1
   2   -1.45283015E+03  # M_2
   3    1.16849754E+03  # M_3
  21    3.08581218E+06  # M^2_(H,d)
  22    2.00291115E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.45423663E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.72219343E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.03168808E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.22993589E+02  # h0
        35     1.80655436E+03  # H0
        36     1.80758800E+03  # A0
        37     1.81526357E+03  # H+
   1000001     1.01222296E+04  # ~d_L
   2000001     1.00979386E+04  # ~d_R
   1000002     1.01218669E+04  # ~u_L
   2000002     1.01011385E+04  # ~u_R
   1000003     1.01222382E+04  # ~s_L
   2000003     1.00979545E+04  # ~s_R
   1000004     1.01218754E+04  # ~c_L
   2000004     1.01011394E+04  # ~c_R
   1000005     3.44836808E+03  # ~b_1
   2000005     4.02545735E+03  # ~b_2
   1000006     3.44037175E+03  # ~t_1
   2000006     3.70929086E+03  # ~t_2
   1000011     1.00204927E+04  # ~e_L-
   2000011     1.00089275E+04  # ~e_R-
   1000012     1.00197262E+04  # ~nu_eL
   1000013     1.00205359E+04  # ~mu_L-
   2000013     1.00090103E+04  # ~mu_R-
   1000014     1.00197688E+04  # ~nu_muL
   1000015     1.00314851E+04  # ~tau_1-
   2000015     1.00340384E+04  # ~tau_2-
   1000016     1.00319737E+04  # ~nu_tauL
   1000021     1.48669585E+03  # ~g
   1000022     2.63103232E+02  # ~chi_10
   1000023     2.73380058E+02  # ~chi_20
   1000025     3.55691902E+02  # ~chi_30
   1000035     1.55131050E+03  # ~chi_40
   1000024     2.73732979E+02  # ~chi_1+
   1000037     1.55127629E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.69830812E-02   # alpha
Block Hmix Q=  3.57230171E+03  # Higgs mixing parameters
   1    2.59397125E+02  # mu
   2    5.93347519E+01  # tan[beta](Q)
   3    2.43044735E+02  # v(Q)
   4    3.26737438E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.82347430E-01   # Re[R_st(1,1)]
   1  2     1.87065567E-01   # Re[R_st(1,2)]
   2  1    -1.87065567E-01   # Re[R_st(2,1)]
   2  2     9.82347430E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99967444E-01   # Re[R_sb(1,1)]
   1  2     8.06910416E-03   # Re[R_sb(1,2)]
   2  1    -8.06910416E-03   # Re[R_sb(2,1)]
   2  2     9.99967444E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     7.19562132E-01   # Re[R_sta(1,1)]
   1  2     6.94428066E-01   # Re[R_sta(1,2)]
   2  1    -6.94428066E-01   # Re[R_sta(2,1)]
   2  2     7.19562132E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -3.70855437E-01   # Re[N(1,1)]
   1  2    -2.91677731E-02   # Re[N(1,2)]
   1  3    -6.69269016E-01   # Re[N(1,3)]
   1  4     6.43190851E-01   # Re[N(1,4)]
   2  1     4.99836023E-02   # Re[N(2,1)]
   2  2     4.35108056E-02   # Re[N(2,2)]
   2  3    -7.06623121E-01   # Re[N(2,3)]
   2  4    -7.04480102E-01   # Re[N(2,4)]
   3  1    -9.27343657E-01   # Re[N(3,1)]
   3  2     1.26369258E-02   # Re[N(3,2)]
   3  3     2.29550129E-01   # Re[N(3,3)]
   3  4    -2.95263928E-01   # Re[N(3,4)]
   4  1     1.27493336E-03   # Re[N(4,1)]
   4  2    -9.98547124E-01   # Re[N(4,2)]
   4  3    -8.33595762E-03   # Re[N(4,3)]
   4  4    -5.32214942E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.18029718E-02   # Re[U(1,1)]
   1  2     9.99930343E-01   # Re[U(1,2)]
   2  1    -9.99930343E-01   # Re[U(2,1)]
   2  2    -1.18029718E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     7.53111995E-02   # Re[V(1,1)]
   1  2     9.97160079E-01   # Re[V(1,2)]
   2  1     9.97160079E-01   # Re[V(2,1)]
   2  2    -7.53111995E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.01723001E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     1.37669298E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     2.50060649E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     8.59828423E-01    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.38617451E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     1.64143146E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     1.56628094E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     7.39704656E-02    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.01819663E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.06140756E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.06902375E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     1.38550535E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     5.02397806E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     8.51318767E-01    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     5.10416663E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.38877161E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     1.72193367E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.49478110E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     7.39306439E-02    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.01256790E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.05010094E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     2.04913871E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     2.13784720E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.82893239E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.14222159E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.03660562E-01    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.77274508E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     2.08164812E-01    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     2.04284909E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     1.52368316E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.76880155E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.85179838E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.01143119E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.81337745E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     2.03090827E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.38621528E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     9.06880443E-03    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     8.19094663E-02    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02609931E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     3.60358036E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.02718467E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.38881209E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     9.05188613E-03    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     8.17566609E-02    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02045520E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     5.46179475E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.01594555E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     2.12168274E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     5.93242517E-03    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     5.35818298E-02    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     1.97977575E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     3.48085350E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     3.94364109E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.68509073E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.00766350E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     6.29288371E-03    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.92680750E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.93751617E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.57152314E-03    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     4.74189912E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     9.50578812E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.55810216E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.68654766E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.04954525E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     6.29664518E-03    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.92494294E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.93828454E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.04042431E-04    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.00156814E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.57565132E-03    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     4.74149646E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     9.50498204E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.55737438E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     3.45566851E+02   # ~b_1
#    BR                NDA      ID1      ID2
     8.66286857E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     9.62963425E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.12564543E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     2.78748318E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     1.98998752E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     5.68695525E-02    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     5.22075382E-01    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     3.95028701E+02   # ~b_2
#    BR                NDA      ID1      ID2
     8.91122307E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     9.85947275E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.53324972E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.96690401E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     5.99880432E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     1.34357570E-04    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.24013918E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     7.85652531E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.94349781E-03    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     2.46300627E-02    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.71354767E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.93741130E+02   # ~u_L
#    BR                NDA      ID1      ID2
     4.51237960E-04    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.36732715E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.15640413E-03    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     4.73745806E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     5.64857130E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     9.45292993E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.55786888E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.85659857E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.94537753E-03    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     2.46302399E-02    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.71345765E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.93817921E+02   # ~c_L
#    BR                NDA      ID1      ID2
     4.52888937E-04    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.38747312E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.15666085E-03    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     4.73705612E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     6.45754922E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     9.45212751E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.55714112E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     4.25728810E+02   # ~t_1
#    BR                NDA      ID1      ID2
     1.23569087E-04    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     7.03861638E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     8.00077042E-02    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.11004782E-02    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.24863615E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     1.53664601E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     4.52548226E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     1.63599436E-01    2     1000021         4   # BR(~t_1 -> ~g c)
     4.26913751E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
     2.64257014E-02    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     3.66479094E+02   # ~t_2
#    BR                NDA      ID1      ID2
     8.34509457E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.01694926E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     4.15061532E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.74046002E-04    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.06715341E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     3.31581332E-04    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     5.50168572E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     7.13724008E-03    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     3.63393607E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     5.18725810E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.52069360E-07   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.46004654E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.22679429E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.15334710E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.15277686E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.00703442E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     1.36956334E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     2.09551187E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.44685379E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     3.48765354E-02    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.43382599E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.41385055E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     9.15264505E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     8.12648894E-04    3     1000024         5        -5   # BR(chi^+_2 -> chi^+_1 b b_bar)
     6.78541729E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     8.12079979E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     1.08581344E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     4.01999200E-07   # chi^0_2
#    BR                NDA      ID1      ID2
     3.29828016E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     9.64315843E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     7.86375460E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.23644456E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.23521882E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     8.89470084E-03    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     2.79028984E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     2.78593454E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     1.79315324E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.65348039E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     1.09078852E-02   # chi^0_3
#    BR                NDA      ID1      ID2
     2.10749147E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.10749147E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     8.45825799E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.05735211E-04    2     1000022        22   # BR(chi^0_3 -> chi^0_1 photon)
     3.82881703E-03    2     1000023        22   # BR(chi^0_3 -> chi^0_2 photon)
#    BR                NDA      ID1      ID2       ID3
     5.82894651E-02    3     1000023         2        -2   # BR(chi^0_3 -> chi^0_2 u u_bar)
     5.81777534E-02    3     1000023         4        -4   # BR(chi^0_3 -> chi^0_2 c c_bar)
     7.47389546E-02    3     1000023         1        -1   # BR(chi^0_3 -> chi^0_2 d d_bar)
     7.47383439E-02    3     1000023         3        -3   # BR(chi^0_3 -> chi^0_2 s s_bar)
     7.35477932E-02    3     1000023         5        -5   # BR(chi^0_3 -> chi^0_2 b b_bar)
     1.68666889E-02    3     1000023        11       -11   # BR(chi^0_3 -> chi^0_2 e^- e^+)
     1.68664488E-02    3     1000023        13       -13   # BR(chi^0_3 -> chi^0_2 mu^- mu^+)
     1.67958759E-02    3     1000023        15       -15   # BR(chi^0_3 -> chi^0_2 tau^- tau^+)
     9.99476506E-02    3     1000023        12       -12   # BR(chi^0_3 -> chi^0_2 nu_e nu_bar_e)
DECAY   1000035     1.49396357E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.25066449E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.25066449E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.23895724E-01    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     7.75376102E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     2.25839223E-02    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.31908264E-01    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.44584016E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     2.39143457E-02    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.52796309E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     3.31416954E-04    3     1000022         5        -5   # BR(chi^0_4 -> chi^0_1 b b_bar)
     3.94189567E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     3.73778299E-04    3     1000023         5        -5   # BR(chi^0_4 -> chi^0_2 b b_bar)
     5.52044340E-04    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     8.25034001E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     8.25034001E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     6.51790926E-03   # ~g
#    BR                NDA      ID1      ID2
     6.57231471E-03    2     1000022        21   # BR(~g -> chi^0_1 g)
     9.74747267E-03    2     1000023        21   # BR(~g -> chi^0_2 g)
     1.28390467E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     9.64725400E-02    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     1.13542176E-01    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     1.19161435E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     1.24194123E-01    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.50624908E-04    3     1000025         2        -2   # BR(~g -> chi^0_3 u u_bar)
     1.50632802E-04    3     1000025         4        -4   # BR(~g -> chi^0_3 c c_bar)
     3.11757909E-02    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.37502699E-02    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     2.41809760E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.41809760E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     3.29104295E-03   # Gamma(h0)
     2.58160403E-03   2        22        22   # BR(h0 -> photon photon)
     1.46737855E-03   2        22        23   # BR(h0 -> photon Z)
     2.52498930E-02   2        23        23   # BR(h0 -> Z Z)
     2.17166320E-01   2       -24        24   # BR(h0 -> W W)
     8.16204020E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.16913218E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.29931120E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.62944591E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.45975000E-07   2        -2         2   # BR(h0 -> Up up)
     2.83297183E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.00022221E-07   2        -1         1   # BR(h0 -> Down down)
     2.17013778E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.76842529E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     7.35102388E+01   # Gamma(HH)
     3.95945031E-08   2        22        22   # BR(HH -> photon photon)
     2.42300006E-08   2        22        23   # BR(HH -> photon Z)
     2.14934622E-07   2        23        23   # BR(HH -> Z Z)
     1.31871803E-07   2       -24        24   # BR(HH -> W W)
     2.66626319E-05   2        21        21   # BR(HH -> gluon gluon)
     1.13190367E-08   2       -11        11   # BR(HH -> Electron electron)
     5.03898606E-04   2       -13        13   # BR(HH -> Muon muon)
     1.45520158E-01   2       -15        15   # BR(HH -> Tau tau)
     2.16404201E-14   2        -2         2   # BR(HH -> Up up)
     4.19724426E-09   2        -4         4   # BR(HH -> Charm charm)
     2.80182317E-04   2        -6         6   # BR(HH -> Top top)
     8.80199756E-07   2        -1         1   # BR(HH -> Down down)
     3.18372373E-04   2        -3         3   # BR(HH -> Strange strange)
     8.04191507E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.14629958E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     7.50319279E-03   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     7.50319279E-03   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     1.62331228E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.29704690E-03   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     8.52389733E-03   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     5.98558451E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     3.60448236E-05   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.27882589E-02   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.19127981E-03   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     1.55888877E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     9.14354562E-07   2        25        25   # BR(HH -> h0 h0)
DECAY        36     6.90424746E+01   # Gamma(A0)
     1.75539484E-08   2        22        22   # BR(A0 -> photon photon)
     3.96820421E-08   2        22        23   # BR(A0 -> photon Z)
     3.76862018E-05   2        21        21   # BR(A0 -> gluon gluon)
     1.12728596E-08   2       -11        11   # BR(A0 -> Electron electron)
     5.01841172E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.44926615E-01   2       -15        15   # BR(A0 -> Tau tau)
     2.07002389E-14   2        -2         2   # BR(A0 -> Up up)
     4.01396648E-09   2        -4         4   # BR(A0 -> Charm charm)
     2.79309009E-04   2        -6         6   # BR(A0 -> Top top)
     8.76541741E-07   2        -1         1   # BR(A0 -> Down down)
     3.17048851E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.00871534E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.31595880E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     8.03668581E-03   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     8.03668581E-03   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     2.00062452E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.20812661E-03   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.08494940E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.32490201E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     3.90937102E-05   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.12987514E-02   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     6.81168628E-03   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     2.14270377E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     3.04539305E-07   2        23        25   # BR(A0 -> Z h0)
     1.87696004E-14   2        23        35   # BR(A0 -> Z HH)
     1.37207207E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     8.63962757E+01   # Gamma(Hp)
     1.21952787E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     5.21386249E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.47477501E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     8.18590085E-07   2        -1         2   # BR(Hp -> Down up)
     1.37354710E-05   2        -3         2   # BR(Hp -> Strange up)
     8.59954694E-06   2        -5         2   # BR(Hp -> Bottom up)
     3.81497774E-08   2        -1         4   # BR(Hp -> Down charm)
     2.95019323E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.20424350E-03   2        -5         4   # BR(Hp -> Bottom charm)
     1.61535095E-08   2        -1         6   # BR(Hp -> Down top)
     7.81387831E-07   2        -3         6   # BR(Hp -> Strange top)
     8.07578590E-01   2        -5         6   # BR(Hp -> Bottom top)
     3.78263843E-03   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     6.56319853E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     2.87305043E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     6.91648333E-03   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     1.83459156E-02   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.44595707E-04   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     6.85887328E-03   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     2.46950614E-07   2        24        25   # BR(Hp -> W h0)
     7.89546896E-10   2        24        35   # BR(Hp -> W HH)
     4.19520528E-10   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.01562368E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    3.52059716E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    3.52061278E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99995562E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    2.88479235E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    2.84041461E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.01562368E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    3.52059716E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    3.52061278E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999983E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.71995317E-08        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999983E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.71995317E-08        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26296283E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    2.89039251E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.84611066E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.71995317E-08        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999983E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.19955598E-04   # BR(b -> s gamma)
    2    1.58976003E-06   # BR(b -> s mu+ mu-)
    3    3.52385859E-05   # BR(b -> s nu nu)
    4    3.07103521E-14   # BR(Bd -> e+ e-)
    5    1.31114528E-09   # BR(Bd -> mu+ mu-)
    6    2.31064975E-07   # BR(Bd -> tau+ tau-)
    7    9.58585358E-13   # BR(Bs -> e+ e-)
    8    4.09279709E-08   # BR(Bs -> mu+ mu-)
    9    7.37414063E-06   # BR(Bs -> tau+ tau-)
   10    9.16280760E-05   # BR(B_u -> tau nu)
   11    9.46482946E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41883949E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.92686361E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15775906E-03   # epsilon_K
   17    2.28166978E-15   # Delta(M_K)
   18    2.47930688E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28877655E-11   # BR(K^+ -> pi^+ nu nu)
   20   -6.50653432E-16   # Delta(g-2)_electron/2
   21   -2.78177996E-11   # Delta(g-2)_muon/2
   22   -7.89279848E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    2.28493372E-04   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.85124746E-01   # C7
     0305 4322   00   2    -2.23039803E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.06933886E-01   # C8
     0305 6321   00   2    -3.76600540E-04   # C8'
 03051111 4133   00   0     1.62404237E+00   # C9 e+e-
 03051111 4133   00   2     1.62446473E+00   # C9 e+e-
 03051111 4233   00   2     7.19382597E-04   # C9' e+e-
 03051111 4137   00   0    -4.44673447E+00   # C10 e+e-
 03051111 4137   00   2    -4.44388621E+00   # C10 e+e-
 03051111 4237   00   2    -5.34167611E-03   # C10' e+e-
 03051313 4133   00   0     1.62404237E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62446446E+00   # C9 mu+mu-
 03051313 4233   00   2     7.19373920E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44673447E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44388649E+00   # C10 mu+mu-
 03051313 4237   00   2    -5.34166968E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50473512E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.15575823E-03   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50473512E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.15575998E-03   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50473513E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.15625458E-03   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85825942E-07   # C7
     0305 4422   00   2    -1.20978596E-05   # C7
     0305 4322   00   2     1.21238939E-06   # C7'
     0305 6421   00   0     3.30485435E-07   # C8
     0305 6421   00   2    -4.41723358E-05   # C8
     0305 6321   00   2    -2.61188135E-07   # C8'
 03051111 4133   00   2     2.66913042E-07   # C9 e+e-
 03051111 4233   00   2     1.62530374E-05   # C9' e+e-
 03051111 4137   00   2     1.27315195E-06   # C10 e+e-
 03051111 4237   00   2    -1.20710313E-04   # C10' e+e-
 03051313 4133   00   2     2.66907438E-07   # C9 mu+mu-
 03051313 4233   00   2     1.62530274E-05   # C9' mu+mu-
 03051313 4137   00   2     1.27315867E-06   # C10 mu+mu-
 03051313 4237   00   2    -1.20710343E-04   # C10' mu+mu-
 03051212 4137   00   2    -2.52700642E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     2.61176367E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -2.52700386E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     2.61176367E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -2.52630025E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     2.61176367E-05   # C11' nu_3 nu_3
