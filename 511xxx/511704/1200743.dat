# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 15.03.2021,  09:32
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    5.13051215E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    4.50104205E+03  # scale for input parameters
    1   -7.65877015E+02  # M_1
    2   -8.36591185E+02  # M_2
    3    4.82044831E+03  # M_3
   11    4.47392337E+02  # A_t
   12   -9.33873218E+02  # A_b
   13    1.64685216E+03  # A_tau
   23    1.72545486E+02  # mu
   25    5.06061234E+01  # tan(beta)
   26    4.97322942E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.35250535E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.40672368E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.16122261E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  4.50104205E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  4.50104205E+03  # (SUSY scale)
  1  1     8.38600892E-06   # Y_u(Q)^DRbar
  2  2     4.26009253E-03   # Y_c(Q)^DRbar
  3  3     1.01309724E+00   # Y_t(Q)^DRbar
Block Yd Q=  4.50104205E+03  # (SUSY scale)
  1  1     8.52783203E-04   # Y_d(Q)^DRbar
  2  2     1.62028809E-02   # Y_s(Q)^DRbar
  3  3     8.45693714E-01   # Y_b(Q)^DRbar
Block Ye Q=  4.50104205E+03  # (SUSY scale)
  1  1     1.48816905E-04   # Y_e(Q)^DRbar
  2  2     3.07706161E-02   # Y_mu(Q)^DRbar
  3  3     5.17511143E-01   # Y_tau(Q)^DRbar
Block Au Q=  4.50104205E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     4.47392341E+02   # A_t(Q)^DRbar
Block Ad Q=  4.50104205E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -9.33873259E+02   # A_b(Q)^DRbar
Block Ae Q=  4.50104205E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.64685215E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  4.50104205E+03  # soft SUSY breaking masses at Q
   1   -7.65877015E+02  # M_1
   2   -8.36591185E+02  # M_2
   3    4.82044831E+03  # M_3
  21    2.50942896E+07  # M^2_(H,d)
  22    4.19136255E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.35250535E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.40672368E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.16122261E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.23158395E+02  # h0
        35     4.96982716E+03  # H0
        36     4.97322942E+03  # A0
        37     4.96697028E+03  # H+
   1000001     1.01063281E+04  # ~d_L
   2000001     1.00818463E+04  # ~d_R
   1000002     1.01059619E+04  # ~u_L
   2000002     1.00843619E+04  # ~u_R
   1000003     1.01063289E+04  # ~s_L
   2000003     1.00818477E+04  # ~s_R
   1000004     1.01059628E+04  # ~c_L
   2000004     1.00843621E+04  # ~c_R
   1000005     3.30349578E+03  # ~b_1
   2000005     4.48231533E+03  # ~b_2
   1000006     4.48364270E+03  # ~t_1
   2000006     4.51850892E+03  # ~t_2
   1000011     1.00209834E+04  # ~e_L-
   2000011     1.00094349E+04  # ~e_R-
   1000012     1.00202116E+04  # ~nu_eL
   1000013     1.00209871E+04  # ~mu_L-
   2000013     1.00094416E+04  # ~mu_R-
   1000014     1.00202152E+04  # ~nu_muL
   1000015     1.00116989E+04  # ~tau_1-
   2000015     1.00222041E+04  # ~tau_2-
   1000016     1.00213985E+04  # ~nu_tauL
   1000021     5.30931343E+03  # ~g
   1000022     1.89924276E+02  # ~chi_10
   1000023     1.99832770E+02  # ~chi_20
   1000025     7.71535420E+02  # ~chi_30
   1000035     9.11582303E+02  # ~chi_40
   1000024     1.94629840E+02  # ~chi_1+
   1000037     9.11439172E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.95052922E-02   # alpha
Block Hmix Q=  4.50104205E+03  # Higgs mixing parameters
   1    1.72545486E+02  # mu
   2    5.06061234E+01  # tan[beta](Q)
   3    2.42841585E+02  # v(Q)
   4    2.47330109E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.96065769E-01   # Re[R_st(1,1)]
   1  2     8.86170627E-02   # Re[R_st(1,2)]
   2  1    -8.86170627E-02   # Re[R_st(2,1)]
   2  2    -9.96065769E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     2.10518318E-03   # Re[R_sb(1,1)]
   1  2     9.99997784E-01   # Re[R_sb(1,2)]
   2  1    -9.99997784E-01   # Re[R_sb(2,1)]
   2  2     2.10518318E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     5.67347153E-02   # Re[R_sta(1,1)]
   1  2     9.98389289E-01   # Re[R_sta(1,2)]
   2  1    -9.98389289E-01   # Re[R_sta(2,1)]
   2  2     5.67347153E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -5.20596605E-02   # Re[N(1,1)]
   1  2     7.58729156E-02   # Re[N(1,2)]
   1  3    -7.15052432E-01   # Re[N(1,3)]
   1  4    -6.92988537E-01   # Re[N(1,4)]
   2  1     3.32739352E-02   # Re[N(2,1)]
   2  2    -5.31352680E-02   # Re[N(2,2)]
   2  3    -6.98716885E-01   # Re[N(2,3)]
   2  4     7.12645917E-01   # Re[N(2,4)]
   3  1    -9.97594650E-01   # Re[N(3,1)]
   3  2    -3.70756304E-02   # Re[N(3,2)]
   3  3     1.34520236E-02   # Re[N(3,3)]
   3  4     5.70031081E-02   # Re[N(3,4)]
   4  1    -3.14253182E-02   # Re[N(4,1)]
   4  2     9.95010222E-01   # Re[N(4,2)]
   4  3     1.77137337E-02   # Re[N(4,3)]
   4  4     9.30232842E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -2.47605598E-02   # Re[U(1,1)]
   1  2     9.99693410E-01   # Re[U(1,2)]
   2  1    -9.99693410E-01   # Re[U(2,1)]
   2  2    -2.47605598E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     1.29900395E-01   # Re[V(1,1)]
   1  2     9.91527048E-01   # Re[V(1,2)]
   2  1     9.91527048E-01   # Re[V(2,1)]
   2  2    -1.29900395E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     4.96998025E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     2.74066821E-03    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.11953647E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     9.95156863E-01    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     9.82843814E-04    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.42592982E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     7.14017444E-04    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.86458298E-04    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     9.93157571E-02    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     2.91120523E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     3.78999425E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.08084245E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.00766270E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     4.64371292E-03    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     2.94775108E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     9.87669701E-01    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     9.76610619E-04    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     3.75995371E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.42781650E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     1.38852521E-03    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     1.03084094E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     9.91847976E-02    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.90736365E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     3.78498766E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.07280973E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.56447719E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.77680079E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.64945236E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     3.15314983E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     6.50919425E-04    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     3.40630278E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     7.78504704E-04    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.95837023E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     1.37756707E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.34684872E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     7.31373561E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.11895310E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.69499040E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.42356255E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.42596432E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     3.32265283E-03    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.55400266E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     7.51492573E-02    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.11415783E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     1.04302774E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.98128027E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.42785083E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     3.31826406E-03    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.55195003E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     7.50499959E-02    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.11004448E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     1.17365598E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.97338782E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.95967748E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     2.41802361E-03    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.13090815E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     5.46891805E-02    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.26630226E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     2.79685288E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.35446374E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     4.21168398E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.31447623E-02    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.86790584E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     5.50850666E+02   # ~d_L
#    BR                NDA      ID1      ID2
     5.85137787E-04    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     2.81589633E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.57713344E-03    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     7.95414387E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.58793572E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.59122184E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.21273487E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.00049522E-04    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.31415075E-02    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     1.24824691E-04    2    -1000024         4   # BR(~s_R -> chi^-_1 c)
     9.86544768E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     5.50907004E+02   # ~s_L
#    BR                NDA      ID1      ID2
     6.33897286E-04    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     3.28172003E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.57698940E-03    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     7.95333408E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.05440893E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.58777457E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.59044703E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     9.47382611E+01   # ~b_1
#    BR                NDA      ID1      ID2
     2.52138307E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.40413001E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.73927574E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.32155846E-04    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.89699315E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.24462814E-04    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     2.09461397E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.55746447E-01    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.48420459E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.80912028E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     8.67651988E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     4.26720974E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.80351472E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.25100038E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     4.38125961E+02   # ~u_R
#    BR                NDA      ID1      ID2
     1.39208572E-04    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     5.05568980E-02    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.49197094E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     5.50832378E+02   # ~u_L
#    BR                NDA      ID1      ID2
     3.57960556E-04    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.79841870E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     3.67986952E-03    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     7.77716789E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     2.72353115E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.56209182E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.59077936E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     4.38133225E+02   # ~c_R
#    BR                NDA      ID1      ID2
     1.43194487E-04    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     5.05560839E-02    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.49181335E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     5.50888693E+02   # ~c_L
#    BR                NDA      ID1      ID2
     3.61102860E-04    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.83184919E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     3.67951518E-03    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     7.77637858E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     2.81867294E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.56193284E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.59000454E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     2.32265177E+02   # ~t_1
#    BR                NDA      ID1      ID2
     1.76043283E-04    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     1.90890163E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.00762169E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     4.33877551E-03    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     7.54468511E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.83053040E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.47127408E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     1.10124864E-04    2     1000005        24   # BR(~t_1 -> ~b_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     9.80070069E-02    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.92743499E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.25640609E-01    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.39410982E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     5.09135818E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     7.92666007E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     4.60687539E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.54194924E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
DECAY   1000024     8.06327969E-09   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.93914793E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     2.77446462E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.31305460E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.30976128E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     6.63571579E-02    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     7.72567988E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     2.42770570E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.53922735E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     4.81599038E-03    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.47926117E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.40304090E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.82258888E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     3.74816147E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     3.68777117E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     3.69885978E-07   # chi^0_2
#    BR                NDA      ID1      ID2
     2.95191954E-01    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     9.85666343E-02    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     7.91306986E-02    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.26382022E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.26247173E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     6.37783489E-03    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     2.85208402E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     2.84729221E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     1.76996396E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.69009322E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
     4.67974083E-03    3     1000024         1        -2   # BR(chi^0_2 -> chi^+_1 d u_bar)
     4.67974083E-03    3    -1000024        -1         2   # BR(chi^0_2 -> chi^-_1 d_bar u)
     3.51107890E-03    3     1000024         3        -4   # BR(chi^0_2 -> chi^+_1 s c_bar)
     3.51107890E-03    3    -1000024        -3         4   # BR(chi^0_2 -> chi^-_1 s_bar c)
     1.55991402E-03    3     1000024        11       -12   # BR(chi^0_2 -> chi^+_1 e^- nu_bar_e)
     1.55991402E-03    3    -1000024       -11        12   # BR(chi^0_2 -> chi^-_1 e^+ nu_e)
     1.55670542E-03    3     1000024        13       -14   # BR(chi^0_2 -> chi^+_1 mu^- nu_bar_mu)
     1.55670542E-03    3    -1000024       -13        14   # BR(chi^0_2 -> chi^-_1 mu^+ nu_mu)
     8.93040329E-04    3     1000024        15       -16   # BR(chi^0_2 -> chi^+_1 tau^- nu_bar_tau)
     8.93040329E-04    3    -1000024       -15        16   # BR(chi^0_2 -> chi^-_1 tau^+ nu_tau)
DECAY   1000025     2.11342913E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     2.58614997E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.58614997E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     5.59488312E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.53766238E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     1.22246571E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.43536316E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     4.06581700E-04    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     4.27339500E-04    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     3.21346340E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     3.21346340E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     8.62790575E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.21388901E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.21388901E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     7.10721082E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.61696988E-01    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.44058205E-01    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.68659114E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.13410517E-03    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.12530814E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     1.20989858E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     4.09522311E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     4.09522311E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.02497208E+02   # ~g
#    BR                NDA      ID1      ID2
     2.95681624E-02    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     2.95681624E-02    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     6.43695280E-02    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     6.43695280E-02    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     5.46963846E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     5.46963846E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     2.87939923E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     2.87939923E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     6.32643224E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     6.32643224E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.38509325E-03   # Gamma(h0)
     2.52199964E-03   2        22        22   # BR(h0 -> photon photon)
     1.44779646E-03   2        22        23   # BR(h0 -> photon Z)
     2.50781845E-02   2        23        23   # BR(h0 -> Z Z)
     2.14970338E-01   2       -24        24   # BR(h0 -> W W)
     7.96759010E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.20245405E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.31413424E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.67223827E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.48534167E-07   2        -2         2   # BR(h0 -> Up up)
     2.88254032E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.03776552E-07   2        -1         1   # BR(h0 -> Down down)
     2.18371469E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.80307452E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.96294107E+02   # Gamma(HH)
     1.50057385E-08   2        22        22   # BR(HH -> photon photon)
     2.73910564E-08   2        22        23   # BR(HH -> photon Z)
     2.03374329E-08   2        23        23   # BR(HH -> Z Z)
     3.38505040E-09   2       -24        24   # BR(HH -> W W)
     3.50785546E-06   2        21        21   # BR(HH -> gluon gluon)
     9.19575656E-09   2       -11        11   # BR(HH -> Electron electron)
     4.09506840E-04   2       -13        13   # BR(HH -> Muon muon)
     1.18279947E-01   2       -15        15   # BR(HH -> Tau tau)
     2.19655024E-14   2        -2         2   # BR(HH -> Up up)
     4.26161139E-09   2        -4         4   # BR(HH -> Charm charm)
     3.08252509E-04   2        -6         6   # BR(HH -> Top top)
     6.29980805E-07   2        -1         1   # BR(HH -> Down down)
     2.27861990E-04   2        -3         3   # BR(HH -> Strange strange)
     5.60033286E-01   2        -5         5   # BR(HH -> Bottom bottom)
     4.04818115E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     9.54341289E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     9.54341289E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     1.39415727E-04   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     1.26146058E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     3.39139153E-05   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.15438011E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     5.33817911E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     5.46498730E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.04526842E-02   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     4.83362629E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     5.46822153E-06   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     4.15829185E-05   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     7.74431707E-05   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     1.66794868E-07   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.83539626E+02   # Gamma(A0)
     2.22798666E-10   2        22        22   # BR(A0 -> photon photon)
     3.89968790E-09   2        22        23   # BR(A0 -> photon Z)
     5.97477409E-06   2        21        21   # BR(A0 -> gluon gluon)
     8.89148268E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.95950960E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.14364611E-01   2       -15        15   # BR(A0 -> Tau tau)
     1.98227568E-14   2        -2         2   # BR(A0 -> Up up)
     3.84581711E-09   2        -4         4   # BR(A0 -> Charm charm)
     2.79109261E-04   2        -6         6   # BR(A0 -> Top top)
     6.09061893E-07   2        -1         1   # BR(A0 -> Down down)
     2.20298654E-04   2        -3         3   # BR(A0 -> Strange strange)
     5.41444049E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     4.29112594E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.02185058E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     1.02185058E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.10234478E-04   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     1.25791681E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     5.89116617E-05   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.17184530E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     5.43073910E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     6.36764960E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.18346331E-02   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     5.46045078E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     4.42670741E-06   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     3.33837565E-05   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     6.14550299E-05   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     5.99096595E-08   2        23        25   # BR(A0 -> Z h0)
     2.72936537E-12   2        23        35   # BR(A0 -> Z HH)
     1.75241165E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     2.15636627E+02   # Gamma(Hp)
     1.00265411E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     4.28665947E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.21251159E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     5.95746664E-07   2        -1         2   # BR(Hp -> Down up)
     1.01115514E-05   2        -3         2   # BR(Hp -> Strange up)
     6.20678941E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.81707438E-08   2        -1         4   # BR(Hp -> Down charm)
     2.14702481E-04   2        -3         4   # BR(Hp -> Strange charm)
     8.69172789E-04   2        -5         4   # BR(Hp -> Bottom charm)
     2.14687901E-08   2        -1         6   # BR(Hp -> Down top)
     7.89098021E-07   2        -3         6   # BR(Hp -> Strange top)
     5.85579736E-01   2        -5         6   # BR(Hp -> Bottom top)
     7.80984027E-05   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     9.12725422E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     4.02490931E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     8.58494804E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     3.15850273E-02   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     7.87454983E-05   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     8.23721799E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     1.84767301E-07   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     5.08110689E-08   2        24        25   # BR(Hp -> W h0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.74598037E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.56100513E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.56097973E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00000992E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.80556717E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.90475563E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.74598037E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.56100513E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.56097973E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999936E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    6.38021782E-08        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999936E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    6.38021782E-08        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26909107E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    9.99617442E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.30940001E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    6.38021782E-08        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999936E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.20979244E-04   # BR(b -> s gamma)
    2    1.59008816E-06   # BR(b -> s mu+ mu-)
    3    3.52501175E-05   # BR(b -> s nu nu)
    4    2.60963225E-15   # BR(Bd -> e+ e-)
    5    1.11480383E-10   # BR(Bd -> mu+ mu-)
    6    2.33290530E-08   # BR(Bd -> tau+ tau-)
    7    8.73252765E-14   # BR(Bs -> e+ e-)
    8    3.73052966E-09   # BR(Bs -> mu+ mu-)
    9    7.91081673E-07   # BR(Bs -> tau+ tau-)
   10    9.62358527E-05   # BR(B_u -> tau nu)
   11    9.94079514E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41839656E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93570500E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15683152E-03   # epsilon_K
   17    2.28165384E-15   # Delta(M_K)
   18    2.47983959E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29011689E-11   # BR(K^+ -> pi^+ nu nu)
   20   -4.74133301E-16   # Delta(g-2)_electron/2
   21   -2.02710293E-11   # Delta(g-2)_muon/2
   22   -5.76147589E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.98754200E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.89251515E-01   # C7
     0305 4322   00   2    -4.35530152E-05   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -9.82278217E-02   # C8
     0305 6321   00   2    -7.75010597E-05   # C8'
 03051111 4133   00   0     1.63173932E+00   # C9 e+e-
 03051111 4133   00   2     1.63192286E+00   # C9 e+e-
 03051111 4233   00   2     5.26195462E-04   # C9' e+e-
 03051111 4137   00   0    -4.45443142E+00   # C10 e+e-
 03051111 4137   00   2    -4.45265996E+00   # C10 e+e-
 03051111 4237   00   2    -3.86614076E-03   # C10' e+e-
 03051313 4133   00   0     1.63173932E+00   # C9 mu+mu-
 03051313 4133   00   2     1.63192269E+00   # C9 mu+mu-
 03051313 4233   00   2     5.26195118E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.45443142E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.45266013E+00   # C10 mu+mu-
 03051313 4237   00   2    -3.86614128E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50496868E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     8.35056700E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50496868E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     8.35056727E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50496869E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     8.35064320E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85817210E-07   # C7
     0305 4422   00   2    -5.19714444E-06   # C7
     0305 4322   00   2     2.63576322E-07   # C7'
     0305 6421   00   0     3.30477956E-07   # C8
     0305 6421   00   2    -5.90484132E-06   # C8
     0305 6321   00   2     1.65244883E-11   # C8'
 03051111 4133   00   2     2.59627047E-07   # C9 e+e-
 03051111 4233   00   2     9.91591088E-06   # C9' e+e-
 03051111 4137   00   2     2.84592711E-07   # C10 e+e-
 03051111 4237   00   2    -7.28575564E-05   # C10' e+e-
 03051313 4133   00   2     2.59622350E-07   # C9 mu+mu-
 03051313 4233   00   2     9.91590699E-06   # C9' mu+mu-
 03051313 4137   00   2     2.84597795E-07   # C10 mu+mu-
 03051313 4237   00   2    -7.28575680E-05   # C10' mu+mu-
 03051212 4137   00   2    -4.03019600E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     1.57366730E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -4.03017345E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     1.57366730E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -4.02386469E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     1.57366730E-05   # C11' nu_3 nu_3
