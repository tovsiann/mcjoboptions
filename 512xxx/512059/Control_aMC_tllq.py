from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
from MadGraphControl.MadGraphParamHelpers import set_top_params
import os, fileinput, shutil, subprocess, sys

nevents = int(1.1*runArgs.maxEvents) if runArgs.maxEvents > 0 else int(1.1*evgenConfig.nEventsPerJob)

beamEnergy = -999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

# Write the proc_card
if flavour_scheme == 4:
    process = '''
import model loop_sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
generate p p > t b~ j l+ l- $$ w+ w- [QCD]
add process p p > t~ b j l+ l- $$ w+ w- [QCD]

output -f
'''

elif flavour_scheme == 5:
    process = '''
import model loop_sm-no_b_mass
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
generate p p > t j l+ l- $$ w+ w- [QCD]
add process p p > t~ j l+ l- $$ w+ w- [QCD]
output -f
'''

process_dir = new_process(process)

# Write the run_card
settings = {
    'nevents':        nevents,
    'parton_shower':  psname,
    'ptgmin':         20,
    'mll_sf':         5,
    'reweight_scale': '.true.',
    'reweight_PDF':   '.true.',
    'pdlabel':        'lhapdf',
}

if flavour_scheme == 4:
    settings['maxjetflavor'] = 4
    settings['lhaid']        = 260400
    settings['PDF_set_min']  = 260401
    settings['PDF_set_max']  = 260500

elif flavour_scheme == 5:
    settings['maxjetflavor'] = 5
    settings['lhaid']        = 260000
    settings['PDF_set_min']  = 260001
    settings['PDF_set_max']  = 260100

if scale_settings == "old":
    print("CUSTOM SCALE SETTINGS: SET TO OLD DEFINITION")
    dyn_scale_fact = 1.0
    fileN = process_dir+'/SubProcesses/setscales.f'
    mark = '      elseif(dynamical_scale_choice.eq.10) then'
    rmLines = ['ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc',
               'cc      USER-DEFINED SCALE: ENTER YOUR CODE HERE                                 cc',
               'cc      to use this code you must set                                            cc',
               'cc                 dynamical_scale_choice = 10                                   cc',
               'cc      in the run_card (run_card.dat)                                           cc',
               'ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc',
               'write(*,*) "User-defined scale not set"',
               'stop 1',
               'temp_scale_id=\'User-defined dynamical scale\' ! use a meaningful string',
               'tmp = 0d0']
    flag=0
    for line in fileinput.input(fileN, inplace=1):
        toKeep = True
        for rmLine in rmLines:
            if line.find(rmLine) >= 0:
                toKeep = False
                break
        if toKeep:
            print line,
        if line.startswith(mark) and flag==0:
            flag +=1
            print """
c         Q^2= mb^2 + 0.5*(pt^2+ptbar^2)
c          rscale=0d0             !factorization scale**2 for pdf1
c                                  !factorization scale**2 for pdf2
c          xmtc=dot(P(0,6),P(0,6))
c          rscale = 4d0 * sqrt(pt(P(0,6))**2+xmtc)
          do i=3,nexternal
            xm2=dot(pp(0,i),pp(0,i))
            if ( xm2 < 30 .and. xm2 > 10 ) then
              tmp = 4d0 * dsqrt(pt(pp(0,i))**2+xm2)
c write(*,*) i, pt(P(0,i))**2, xmtc, sqrt(pt(P(0,i))**2+xmtc), rscale
c write(*,*) i, xmtc, pt(P(0,i)), rscale
            endif
          enddo
          tmp = %f*tmp""" % dyn_scale_fact
    settings['dynamical_scale_choice'] = '10'
elif scale_settings == "HT/2":
    print("CUSTOM SCALE SETTINGS: SET TO HT/2")
    settings['dynamical_scale_choice'] = '3'
elif scale_settings == "MT/4":
    print("CUSTOM SCALE SETTINGS: SET TO MT/4")
    settings['dynamical_scale_choice'] = '0'
    settings['fixed_ren_scale'] = 'True'
    settings['fixed_fac_scale'] = 'True'
    settings['muR_ref_fixed']   = '{:.2f}'.format((91.1876+172.5)/4)
    settings['muF_ref_fixed']   = '{:.2f}'.format((91.1876+172.5)/4)
elif scale_settings == "HT/6":
    print("CUSTOM SCALE SETTINGS: SET TO HT/6")
    dyn_scale_fact = 1.0
    fileN = process_dir+'/SubProcesses/setscales.f'
    mark = '      elseif(dynamical_scale_choice.eq.10) then'
    rmLines = ['ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc',
               'cc      USER-DEFINED SCALE: ENTER YOUR CODE HERE                                 cc',
               'cc      to use this code you must set                                            cc',
               'cc                 dynamical_scale_choice = 10                                   cc',
               'cc      in the run_card (run_card.dat)                                           cc',
               'ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc',
               'write(*,*) "User-defined scale not set"',
               'stop 1',
               'temp_scale_id=\'User-defined dynamical scale\' ! use a meaningful string',
               'tmp = 0d0']
    flag=0
    for line in fileinput.input(fileN, inplace=1):
        toKeep = True
        for rmLine in rmLines:
            if line.find(rmLine) >= 0:
                toKeep = False
                break
        if toKeep:
            print line,
        if line.startswith(mark) and flag==0:
            flag +=1
            print """
          tmp=0d0
          do i=3,nexternal
            tmp=tmp+dsqrt(max(0d0,(pp(0,i)+pp(3,i))*(pp(0,i)-pp(3,i))))
          enddo
          tmp=%f*tmp/6d0
          temp_scale_id='H_T/6 := sum_i mT(i)/6, i=final state'"""% dyn_scale_fact
    settings['dynamical_scale_choice'] = '10'

modify_run_card(process_dir=process_dir,
                runArgs=runArgs,
                settings=settings)

# SM param card settings
params = dict()
params['mass'] = dict()
params['mass']['6'] = '1.725000e+02'
params['mass']['23'] = '9.118760e+01'
params['mass']['24'] = '8.039900e+01'
params['mass']['25'] = '1.250000e+02'
if flavour_scheme == 4:
    params['mass']['5']="4.950000e+00"
params['yukawa'] = dict()
params['yukawa']['6'] = '1.725000e+02'
params['DECAY'] = dict()
params['DECAY']['23'] = 'DECAY  23   2.495200e+00'
params['DECAY']['24'] = '''DECAY  24   2.085000e+00
   3.377000e-01   2   -1   2
   3.377000e-01   2   -3   4
   1.082000e-01   2  -11  12
   1.082000e-01   2  -13  14
   1.082000e-01   2  -15  16'''
params['DECAY']['25'] = 'DECAY  25   6.382339e-03'

modify_param_card(process_dir=process_dir,params=params)

# Write the MadSpin card
madspin_card = process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)
mscard = open(madspin_card,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
set seed %i
set Nevents_for_max_weight 75 # number of events for the estimate of the max. weight
set BW_cut 15                 # cut on how far the particle can be off-shell
set max_weight_ps_point 400   # number of PS to estimate the maximum for each event
# specify the decay for the final state particles
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
decay t > w+ b, w+ > l+ vl
decay t~ > w- b~, w- > l- vl~
# running the actual code
launch"""%(runArgs.randomSeed))
mscard.close()

# Print and generate
print_cards()
generate(process_dir=process_dir,
         grid_pack=True,
         gridpack_compile=False,
         required_accuracy=0.001,
         runArgs=runArgs)
outputDS = arrange_output(process_dir=process_dir,
                          runArgs=runArgs,
                          saveProcDir=True,
                          lhe_version=3)

# Metadata
evgenConfig.description      = 'aMcAtNlo_tllq'
evgenConfig.keywords        += ['SM','tZ']
evgenConfig.contact          = ['baptiste.ravina@cern.ch']
evgenConfig.generators       = ['aMcAtNlo','EvtGen']


check_reset_proc_number(opts)

if psname == "PYTHIA8":
    evgenConfig.generators += ['Pythia8']
    include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
    include("Pythia8_i/Pythia8_aMcAtNlo.py")
elif psname == "HERWIGPP":
    evgenConfig.generators += ['Herwig7']
    evgenConfig.tune = "H7.1-Default"
    runArgs.inputGeneratorFile = outputDS+'.events'
    include("Herwig7_i/Herwig7_LHEF.py")
    Herwig7Config.me_pdf_commands(order="NLO",
                                  name="NNPDF30_nlo_as_0118")
    Herwig7Config.tune_commands()
    Herwig7Config.lhef_mg5amc_commands(lhe_filename=runArgs.inputGeneratorFile,
                                       me_pdf_order="NLO")
    include("Herwig7_i/Herwig71_EvtGen.py")
    Herwig7Config.run()

