evgenConfig.description      = 'aMcAtNlo_tllq'
evgenConfig.keywords        += ['SM','tZ']
evgenConfig.contact          = ['baptiste.ravina@cern.ch']
evgenConfig.generators       = ['aMcAtNlo','EvtGen','Pythia8']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_Var3cDown_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")
