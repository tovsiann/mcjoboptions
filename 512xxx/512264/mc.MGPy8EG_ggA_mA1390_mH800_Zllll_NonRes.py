import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
joparts = get_physics_short().split('_')

# Generate A->ZH, H->4l, Z->inc inc

#: parse the job arguments to get mA,mH and Z decay mode
mA = float(joparts[2][2:])
mH = float(joparts[3][2:])

width = 0.0
# so far only support LW A not for H

print "mA ",mA
print "mH ",mH
# a safe margin for the number of generated events
nevents=int(runArgs.maxEvents*14) 
#runNumber=runArgs.runNumber 
mode=0 

#print 'nevents,runNumber',nevents,runNumber

process="""
import model 2HDM_GF
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~ a
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define l+ = e+ mu+ 
define l- = e- mu- 
define inc = u c d s u~ c~ d~ s~ b b~ e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~
# Define multiparticle labels
# Specify process(es) to run
"""
if mA > mH:
      process += """
generate     g g > h3 > z h2 , z > l+ l- , h2 > z z
output -f
"""
else:
      process += """
generate     g g > h2 > z h3 , h3 > z z  
output -f
"""

print 'process string: ',process
#generate     g g > h2 > z h3 , z > inc inc, h3 > z z
#generate     g g > h3 > z h2 , z > vl vl~, h2 > z z

#---------------------------------------------------------------------------------------------------
# Set masses and widths in param_card.dat
#---------------------------------------------------------------------------------------------------
mh1=125
mh2=mH
mh3=mA
mhc=(mA if mA > mH else mH)
print 'mh1,mh2,mh3,mhc ',mh1,mh2,mh3,mhc
masses ={'25':mh1,  
         '35':mh2,
         '36':mh3, 
         '37':mhc} 

print masses

decays ={'25':'DECAY 25 4.070000e-03 # Wh1',  
         '35':'DECAY 35 1.000000e-03 # Wh2',  
         '36':'DECAY 36 1.000000e-03 # Wh3',  
         '37':'DECAY 37 1.000000e-03 # whc'}  

if width != 0:
  decays ={'25':'DECAY 25 4.070000e-03 # Wh1',  
           '35':'DECAY 35 1.000000e-03 # Wh2',  
           '36':'DECAY 36 %f # Wh3' % (mh3*width/100.),
           '37':'DECAY 37 1.000000e-03 # whc'}  
higgsmix={    '1 1':'  0.9553365   # TH1x1',
	      '1 2':'  0.2955202   # TH1x2',
	      '1 3':'  0.0000000   # TH1x3',
	      '2 1':' -0.2955202   # TH2x1',
	      '2 2':'  0.9553365   # TH2x2',
	      '2 3':'  0.0000000   # TH2x3',
	      '3 1':'  0.0000000   # TH3x1',
	      '3 2':'  0.0000000   # TH3x2',
	      '3 3':'  1.0000000   # TH3x3'}
 
print decays


beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


#Fetch default LO run_card.dat and set parameters (extras)
extras = { 'lhe_version':'3.0', 
           'cut_decays':'F', 
	   'nevents':nevents,
			}	
#           'pdlabel':"'lhapdf'",
#           'lhaid':260000} # NNPDF30_nlo_as_0118

#           'lhaid':247000} # NNPDF23_lo_as_0130_qed


#runName='run_01'     

# set up process
process_dir = new_process(process) 
#modify_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat',xqcut=0,nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)  
modify_run_card(runArgs=runArgs,process_dir=process_dir,settings=extras)



#build_run_card(run_card_old=get_default_runcard(process_dir),run_card_new='run_card.dat',
#               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# Build a new param_card.dat from an existing one
#---------------------------------------------------------------------------------------------------
#build_param_card(param_card_old='param_card_AZH.dat',param_card_new='param_card_new.dat',masses=masses,decays=decays)
#modify_param_card(process_dir=process_dir,masses=masses,decays=decays)
modify_param_card(process_dir=process_dir,params={'MASS':masses,'DECAY':decays,'higgsmix':higgsmix})
print_cards()

# and the generation
generate(process_dir=process_dir,runArgs=runArgs)


arrange_output(runArgs=runArgs,process_dir=process_dir,lhe_version=3)
#generate(run_card_loc='run_card.dat',param_card_loc='param_card_new.dat',mode=mode,proc_dir=process_dir,run_name=runName)
#arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00001.events.tar.gz',saveProcDir=True)  


#### Pythia8 Showering with A14_NNPDF23LO
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")


#include("Pythia8_i/Pythia8_aMcAtNlo.py")   
evgenConfig.generators += ["MadGraph", "Pythia8", "EvtGen"]
#if isZll:
#    if mA > mH:
#      evgenConfig.description = 'mass splitting A(%s GeV)->ZH(%s GeV)->ll4l' % (mA,mH)
#    else:
#      evgenConfig.description = 'mass splitting H(%s GeV)->ZA(%s GeV)->ll4l' % (mH,mA)
#else:
if mA > mH:
  evgenConfig.description = 'mass splitting A(%s GeV)->ZH(%s GeV)->all4l' % (mA,mH)
else:
  evgenConfig.description = 'mass splitting H(%s GeV)->ZA(%s GeV)->all4l' % (mH,mA)

evgenConfig.keywords+=['BSMHiggs','bottom']
evgenConfig.contact = [ 'xifeng.ruan@cern.ch' ]

if not hasattr( filtSeq, "MultiLeptonWithParentFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import MultiLeptonWithParentFilter
    filtSeq += MultiLeptonWithParentFilter()
    pass

## Setup Defaults as example
MultiLeptonWithParentFilter = filtSeq.MultiLeptonWithParentFilter
MultiLeptonWithParentFilter.PDGOrigin = [35,36]
MultiLeptonWithParentFilter.PDGIntermediate = [23]
MultiLeptonWithParentFilter.MinPt = 4000.
MultiLeptonWithParentFilter.NLeptonsMin = 4
MultiLeptonWithParentFilter.NLeptonsMax = 4
MultiLeptonWithParentFilter.IncludeLepTaus = False
MultiLeptonWithParentFilter.IncludeHadTaus = False
MultiLeptonWithParentFilter.VetoHadTaus = True


#genSeq.Pythia8.Commands += [
#    '23:onMode = off',
#    '23:onIfAny = 11 11',
#    '23:onIfAny = 13 13',
#                           ]


