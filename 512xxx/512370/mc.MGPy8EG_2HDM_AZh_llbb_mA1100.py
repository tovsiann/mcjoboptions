#---------------------------------------------------------------------------
# Process-specific parameter settings in MadGraph
#---------------------------------------------------------------------------
THDMparams = {}
THDMparams['tanbeta'] = 1 # The ratio of the vacuum expectation values $\tan \beta = v_2/v_1$ of the Higgs doublets $H_2$ and $H_1$, as defined in Section 2.1 of arXiv:1701.07427.
THDMparams['sinbma'] = 0.5 # The sine of the difference of the mixing angles $\sin (\beta - \alpha)$ in the scalar potential containing only the Higgs doublets.  This quantity is defined in Section 3.1 of arXiv:1701.07427.
THDMparams['lam3'] = 3.0 # The quartic coupling of the scalar doublets $H_1$ and $H_2$. This parameter corresponds to the coefficient $\lambda_3$ in (2.1) of arXiv:1701.07427.
THDMparams['laP1'] = 3.0 # The quartic coupling between the scalar doublets $H_1$ and the pseudoscalar $P$. This parameter corresponds to the coefficient $\lambda_{P1}$ in (2.2) of arXiv:1701.07427.
THDMparams['laP2'] = 3.0 # The quartic coupling between the scalar doublets $H_2$ and the pseudoscalar $P$. This parameter corresponds to the coefficient $\lambda_{P2}$ in (2.2) of arXiv:1701.07427.
THDMparams['sinp'] = 0 # The sine of the mixing angle $\theta$, as defined in Section 2.1 of arXiv:1701.07427.
THDMparams['mt'] = 172.5 # The top quark mass
mass = 1100
THDMparams['mh2'] = mass # The mass of the heavy scalar mass eigenstate $H$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams['mh3'] = mass # The mass of the heavy pseudoscalar mass eigenstate $A$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams['mhc'] = mass # The mass of the charged scalar eigenstate $H^\pm$. See Section 2.1 of arXiv:1701.07427 for further details.


Decay = "llbb"

# Reweighting in width
reweight = True

reweights = []
for each in range(0, 21):
    if each == 0:
        reweightname = "width_" + str(0.1)
    else:
        reweightname = "width_" + str(each) + ".0"
    reweights.append(reweightname)

#---------------------------------------------------------------------------
# Generation settings
#---------------------------------------------------------------------------

# Number of events to generate
evgenConfig.nEventsPerJob=10000

#---------------------------------------------------------------------------
# Load main control file
#---------------------------------------------------------------------------
include('MadGraphControl_Py8EG_2HDM_AZh_common.py')
