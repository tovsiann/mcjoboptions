from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
import os

mode=0
gridpack_mode=True

nevents = 10000
ktdurham = 25.0
dparameter = 0.4
maxjetflavor = 4

safetyFactor = 1.5
physShort = get_physics_short()

isLL = physShort.find("_LL") != -1
isTL = physShort.find("_WpTWmL") != -1
isLT = physShort.find("_WmTWpL") != -1
isTT = physShort.find("_TT") != -1
isWWIncl = physShort.find("_WWIncl") != -1


proc_str = ""

if(isLL):
    proc_str = "w+{0} w-{0}"
elif(isTL):
    proc_str = "w+{T} w-{0}"
elif(isLT):
    proc_str = "w+{0} w-{T}"
elif(isTT):
    proc_str = "w+{T} w-{T}"
elif(isWWIncl):
    proc_str = "w+w-"
else:
    print("ERROR: pol_state has to be one of W0W0, Wm0WpT, WmTWp0, WTWT, WWIncl ")

process  = """
import model sm
define wpm = w+ w-
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm
define vl~ = ve~ vm~
define wpm = w+ w-
generate p p > %s, w+ > l+ vl, w- > l- vl~ @0
add process p p > %s j, w+ > l+ vl, w- > l- vl~ @1
output -f
""" % (proc_str, proc_str)

process_dir = new_process(process)

if not is_gen_from_gridpack(): # When generating the gridpack
  # Todo: replace in line to be safer
  os.system("cp collect_events.f "+process_dir+"/SubProcesses")

#Fetch default LO run_card.dat and set parameters                                                                                                

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

#----------------------------------------------------------------------------
# Random Seed
#----------------------------------------------------------------------------                                                                     
randomSeed = 0
if hasattr(runArgs,'randomSeed'): randomSeed = runArgs.randomSeed

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------                                                                     
if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:  nevents = int(runArgs.maxEvents)*safetyFactor
else: nevents = nevents*safetyFactor

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents

extras = { 'me_frame' : '3,4,5,6',
           'lhe_version' : '3.0',
           'cut_decays' : 'True',
           'clusinfo' : 'T',
           'auto_ptj_mjj' : 'False',
           'maxjetflavor' : 4,
           'asrwgtflavor' : 4,
           'ickkw' : 0,
           'xqcut' : 0.0,
           'drjl' : 0.0,
           'drll' : 0.0,
           'etal' : -1.0,
           'etaj' : -1.0,
           'ptl' : 0.0,
           'ptj' : ktdurham,
           'ktdurham' : 25.0,
           'dparameter' : 0.4,
           'nevents'      : nevents,
}

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

print_cards()

generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

############################                                                                                                
# Shower JOs will go here                                                                                                             
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

evgenConfig.generators = ["MadGraph","Pythia8","EvtGen"]
evgenConfig.description = 'MadGraph_'+str(physShort)
evgenConfig.contact = [ 'Kristin Lohwasser <kristin.lohwasser@cern.ch>' ]
evgenConfig.keywords+=["inclusive","WW","LO"]

nJetMax = 1

PYTHIA8_TMS=ktdurham
PYTHIA8_nJetMax=nJetMax
PYTHIA8_Dparameter=dparameter
PYTHIA8_Process= "guess"
PYTHIA8_nQuarksMerge=maxjetflavor
genSeq.Pythia8.Commands += [ 'SpaceShower:dipoleRecoil = on' ]
genSeq.Pythia8.UserHooks += ['JetMergingaMCatNLO']
include("Pythia8_i/Pythia8_CKKWL_kTMerge.py")
