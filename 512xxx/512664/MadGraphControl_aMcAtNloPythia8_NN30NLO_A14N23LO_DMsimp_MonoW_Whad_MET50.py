
from MadGraphControl.MadGraphUtils import *
import math

# safefactor=2.

process="""
import model DMsimp_s_spin1 -modelname

define pb = p b b~
define w = w+ w-
generate pb pb > xd xd~ w [QCD]

output -f
"""

#---------------------------------------------------------------------------
# Number of events to generate
#---------------------------------------------------------------------------
nevents=evgenConfig.nEventsPerJob
if LHE_EventMultiplier>0:
  nevents=runArgs.maxEvents*LHE_EventMultiplier


#Fetch default LO run_card.dat and set parameters
extras = {'lhe_version':'3.0',
          'pdlabel'    : "'lhapdf'",
	  'lhaid'      : '260000 264000 265000 266000 267000 25100 13100',
          'parton_shower' :'PYTHIA8',
	  'reweight_scale' : 'True', 	  
	  'reweight_PDF' : 'True False False False False False False',
      'nevents'     	: nevents,
          }
	       
process_dir = new_process(process)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

params = {
    "mass": {
                    'MXd' : MXd,
                    'MY1' : MY1,
            },
    "dminputs": {
                    'gVXd' : gVXd,
                    'gAXd' : gAXd,
                    'gAd11' : gAd11,
                    'gAu11' : gAu11,
                    'gAd22' : gAd22,
                    'gAu22' : gAu22,
                    'gAd33' : gAd33,
                    'gAu33' : gAu33,
                    'gAl11' : gAl11,
                    'gAl22' : gAl22,
                    'gAl33' : gAl33,
                    'gVd11' : gVd11,
                    'gVu11' : gVu11,
                    'gVd22' : gVd22,
                    'gVu22' : gVu22,
                    'gVd33' : gVd33,
                    'gVu33' : gVu33,
                    'gVl11' : gVl11,
                    'gVl22' : gVl22,
                    'gVl33' : gVl33,
                },
    'decay':    {
                    '5000001' : str(WY1),
                }
}

print("Updating parameters:")
print(params)
modify_param_card(process_dir=process_dir,params=params)

print_cards()

### creating mad spin card to specify decay of w boson
madspin_card_loc='madspin_card.dat'
mscard = open(process_dir+ "/Cards/" + madspin_card_loc,'w')

mscard.write("""
#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
#Some options (uncomment to apply)
#
# set seed 1
# set Nevents_for_max_weigth 75 # number of events for the estimate of the max. weight
# set BW_cut 15                # cut on how far the particle can be off-shell
 set max_weight_ps_point 400  # number of PS to estimate the maximum for each event
#
set seed %i
# specify the decay for the final state particles
set spinmode full
define jb = j b b~
define w = w+ w-
decay w > jb jb
# running the actual code
launch"""%runArgs.randomSeed)

mscard.close()

#---------------------------------------------------------------------------
# Generate the events    
#---------------------------------------------------------------------------
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True) 

# Multi-core capability
check_reset_proc_number(opts)

#### Showering with Pythia 8
evgenConfig.description = "Wimp dmA mediator from DMSimp"
evgenConfig.keywords = ["exotic","BSM"]
evgenConfig.process = "generate pb pb > xd xd~ w [QCD], decay  w > jb jb"
evgenConfig.contact = ["Sergio Gonzalez <sergio.gonzalez.fernandez@cern.ch>"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

#particle data = name antiname spin=2s+1 3xcharge colour mass width (left out, so set to 0: mMin mMax tau0)
genSeq.Pythia8.Commands += ["1000022:all = xd xd~ 2 0 0 %d 0" %(MXd),
                            "1000022:isVisible = false"]



## MET filter
include("GeneratorFilters/MissingEtFilter.py")
filtSeq.MissingEtFilter.METCut = 50*GeV
