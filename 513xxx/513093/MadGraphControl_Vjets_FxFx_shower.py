from __future__ import print_function
import os, sys, glob, math

from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()

# Hack the LHE files to restore resonances
# this allows Pythia8 internal tau decays to handle polarisation
# a new line of an intermediate particle will be added that corresponds to the four-vector sum
# Z->tautau example: restoreResonances = [[-15, 15, 23]]
# W->taunu example: restoreResonances = [[-15, 16, 24], [15, -16, -24]]

try:
    restoreResonances
    lheToPatch = glob.glob("*.events")
except:
    lheToPatch = []

for infile in lheToPatch:
    print ("Patching in the resonances", restoreResonances, "into file", infile)

    f1 = open(infile)
    newfile = infile+'.temp'
    f2 = open(newfile,'w')

    startEvent = False
    nParticles = -1
    eventBuffer = []
    
    for line in f1:
        if line.startswith('<event'):
            # flag new event, write out line, go to next line
            startEvent = True
            f2.write(line)
            continue

        if startEvent:
            # start buffering the event, record number of particles, go to next line
            nParticles = float(line.split()[0])
            startEvent = False
            eventBuffer += [line]
            continue

        if nParticles > 0:
            # continue buffering the event, go to next line
            nParticles -= 1
            eventBuffer += [line]
            continue

        if nParticles == 0:
            # we're finish buffering the event, process event and write buffer

            for myRes in restoreResonances:

                imatch = 0
                for l in eventBuffer:
                    if int(l.split()[0]) in myRes:
                        imatch += 1

                if imatch == len(myRes):
                    #print ('Got full resonance', myRes, ' - all ok')
                    break
                elif imatch > 0:
                    #print ('Got partial resonance', myRes, ' - will patch')

                    # event info line patched to have one extra particle in record
                    eventInfo = eventBuffer[0]
                    NUP = int(eventInfo.split()[0])
                    eventInfo = ("%2i" % (NUP+1))+eventInfo[2:]
                    eventBuffer[0] = eventInfo

                    # construct the resonance summing all relevant particles and adding it after the status -1 particles
                    ires = 1
                    pxsum = 0.
                    pysum = 0.
                    pzsum = 0.
                    Esum = 0.
                    iline = -1

                    for l in eventBuffer:
                        lsplit = l.split()
                        iline += 1
                        if int(lsplit[1]) == -1:
                            # count lines before adding the resonance
                            ires += 1
                        elif int(lsplit[0]) in myRes:
                            # add up decay product four-vectors
                            # patch the decay products to originate from resonance
                            pxsum += float(lsplit[6])
                            pysum += float(lsplit[7])
                            pzsum += float(lsplit[8])
                            Esum += float(lsplit[9])
                            pLine = "%9s%3i%5i%5i%5i%5i %s\n" \
                                    % (lsplit[0], int(lsplit[1]), ires, ires, int(lsplit[4]), int(lsplit[5]), ' '.join(lsplit[6:]))
                            eventBuffer[iline] = pLine

                    # add the resonance line
                    Msum = math.sqrt(Esum**2-pxsum**2-pysum**2-pzsum**2)
                    resonance = "%9i  2    1    2    0    0 %+.10e %+.10e %+.10e %.10e %.10e 0.0000e+00 0.0000e+00\n"\
                                % (myRes[-1], pxsum, pysum, pzsum, Esum, Msum)
                    eventBuffer.insert(ires, resonance)
                    break

            # done patching the event (if needed) - write it out
            f2.write(''.join(eventBuffer))

            # clear event buffer
            eventBuffer = []
            nParticles = -1
            # still need to write the last line
            f2.write(line)
            continue

        # this will write any other lines
        f2.write(line)


    f1.close()
    f2.close()
    os.system('rm %s' % (infile) )
    os.system('mv %s %s' % (newfile, infile) )

# Shower: Py8 with A14 Tune, with modifications to make it simil-NLO                                                 
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

if "_SW" in phys_short:
    include("Pythia8_i/Pythia8_ShowerWeights.py")

# FxFx Matching settings
PYTHIA8_nJetMax=3
PYTHIA8_qCut=20.
include("Pythia8_i/Pythia8_FxFx_A14mod.py")

if "Wtaunu_H" in phys_short:
    # force hadronic tau decays
    genSeq.Pythia8.Commands += [ "15:offIfAny = 11 13" ]
elif "Wtaunu_L" in phys_short:
    # force leptonic tau decays
    genSeq.Pythia8.Commands += [ "15:onMode = off",
                                 "15:onIfAny = 11 13" ]


# B/C/L jet slicing
include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)

if "BFilter" in phys_short or "BVeto" in phys_short:
    include("GeneratorFilters/BHadronFilter.py")
    HeavyFlavorBHadronFilter.BottomEtaMax = 2.9
    HeavyFlavorBHadronFilter.BottomPtMin = 5*GeV
    HeavyFlavorBHadronFilter.RequireTruthJet = True
    HeavyFlavorBHadronFilter.JetPtMin = 10*GeV
    HeavyFlavorBHadronFilter.JetEtaMax = 2.9
    HeavyFlavorBHadronFilter.TruthContainerName = "AntiKt4TruthJets"
    HeavyFlavorBHadronFilter.DeltaRFromTruth = 0.4
    filtSeq += HeavyFlavorBHadronFilter 

if "CFilt" in phys_short or "CVeto" in phys_short:
    include("GeneratorFilters/CHadronPt4Eta3_Filter.py")
    filtSeq += HeavyFlavorCHadronPt4Eta3_Filter

if "CFilterBVeto" in phys_short or "CFiltBVeto" in phys_short:
    filtSeq.Expression = "(not HeavyFlavorBHadronFilter) and (HeavyFlavorCHadronPt4Eta3_Filter)"
elif "CVetoBVeto" in phys_short:
    filtSeq.Expression = "(not HeavyFlavorBHadronFilter) and (not HeavyFlavorCHadronPt4Eta3_Filter)"
