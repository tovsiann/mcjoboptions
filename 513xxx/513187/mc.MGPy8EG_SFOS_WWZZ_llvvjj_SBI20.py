import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment # 5 flavour scheme
from MadGraphControl.MadGraphUtils import *

evgenConfig.nEventsPerJob = 2000

mode=0
gridpack_mode=False
saveRundetails=False
runPythia=False

if runArgs.runNumber:#:
    gridpack_mode=True
    saveRundetails=True
    if is_gen_from_gridpack():
        saveRundetails = False
        runPythia=True
        print("Generating from Gridpack, doing so serially")

    if gridpack_mode:
        from MadGraphControl import MadGraphUtils
        MadGraphUtils.MADGRAPH_CATCH_ERRORS=False


#Set The Higgs mass and Higgs width
masses={'25': '1.250000e+02'}        ## Higgs mass 
decays={'25': 'DECAY  25 4.07e-03'}  ## Higgs width



model="""
set zerowidth_tchannel False
import model /data/atlas/users/bkortman/MG5inputs/models/sm_20hvv-no_b_mass
"""
diagrams = """
define p = g u c d s b u~ c~ d~ s~ b~
define j = g u c d s b u~ c~ d~ s~ b~
define top = t t~

generate      p p > j j e+ ve  e- ve~            QCD=0 QED==6  / top
add process   p p > j j mu+ vm  mu- vm~         QCD=0 QED==6  / top
add process   p p > j j ta+ vt  ta- vt~         QCD=0 QED==6  / top

output -f
"""
masses={'25': '1.250000e+02'}        ## Higgs mass 
decays={'25': 'DECAY  25 0.0814'}    ## 20X Higgs width



# ----------------------------------------------
#  Some global production settings              
# ----------------------------------------------
# Make some excess events to allow for Pythia8 failures


nevents=1.2*runArgs.maxEvents if runArgs.maxEvents>0 else 5500

if is_gen_from_gridpack():
    process_dir = MADGRAPH_GRIDPACK_LOCATION
else:
    process_str="""
    """+model+diagrams
    process_dir = new_process(process_str,keepJpegs=True)





beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")


#dynamic=1 #Total transverse energy of the event.
#dynamic=2 #sum of the transverse mass
#dynamic=3 #sum of the transverse mass divide by 2
#dynamic=4 #\sqrt(s), partonic energy
#dynamic=5 #\decaying particle mass, for decays



#process_dir = new_process(grid_pack="madevent/")


extras = { 
'asrwgtflavor':"5.0",     
'lhe_version':"3.0", 
'ptj':"10",
'ptb':"10",
'pta':"10",
'ptl':"10",
'etaj':"5",
'etab':"5",
'etal':"3.0",
'drjj':"0.1",
'drll':"0.1",
'draa':"0.1",
'draj':"0.1",
'drjl':"0.1",
'dral':"0.1",
'mmjj':"10",
'mmbb':"10",
'maxjetflavor':"5" ,    
'auto_ptj_mjj': 'F',   
'nevents'      : nevents
}

extras_cuts={
             'mmll':"20",  #min invariant mass of l+l- (same flavour) lepton pair
             'mmnl':"10",  #min invariant mass for all letpons (l+- and vl)
            }

extras.update(extras_cuts)
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)
print_cards()
params={}
params['MASS']=masses 
params['DECAY']=decays
modify_param_card(process_dir=process_dir,params=params)
#os.system("cp setscales.f  "+process_dir+"/SubProcesses/setscales.f")

generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=saveRundetails)




if runPythia:
    ###shower
    include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
    include("Pythia8_i/Pythia8_MadGraph.py")



    genSeq.Pythia8.Commands += [ 'SpaceShower:dipoleRecoil = on' ]

    evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
    evgenConfig.description = 'MGPy8EG_SFOS_WWZZ_llvvjj_SBI'
    evgenConfig.keywords = ['SM','diboson','VBS','WW','ZZ','electroweak','2lepton','2jet']
    evgenConfig.contact = ["bryan.kortman@cern.ch"]
else:
    evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
    evgenConfig.description = 'MGPy8EG_SFOS_WWZZ_llvvjj_SBI'
    evgenConfig.keywords = ['SM','diboson','VBS','WW','ZZ','electroweak','2lepton','2jet']
    evgenConfig.contact = ["bryan.kortman@cern.ch"]
    ############################
    # Shower JOs will go here
    theApp.finalize()
    theApp.exit()
