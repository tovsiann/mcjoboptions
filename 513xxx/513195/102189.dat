# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  16:03
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.78018264E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    4.57316399E+03  # scale for input parameters
    1    6.43883783E+01  # M_1
    2    1.41838167E+03  # M_2
    3    1.17987079E+03  # M_3
   11    7.57171430E+03  # A_t
   12   -1.74543567E+03  # A_b
   13    1.77053570E+03  # A_tau
   23   -2.80442371E+02  # mu
   25    2.66567537E+01  # tan(beta)
   26    7.21564776E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.53908299E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.77097499E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.17346383E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  4.57316399E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  4.57316399E+03  # (SUSY scale)
  1  1     8.39026971E-06   # Y_u(Q)^DRbar
  2  2     4.26225701E-03   # Y_c(Q)^DRbar
  3  3     1.01361197E+00   # Y_t(Q)^DRbar
Block Yd Q=  4.57316399E+03  # (SUSY scale)
  1  1     4.49431417E-04   # Y_d(Q)^DRbar
  2  2     8.53919693E-03   # Y_s(Q)^DRbar
  3  3     4.45695134E-01   # Y_b(Q)^DRbar
Block Ye Q=  4.57316399E+03  # (SUSY scale)
  1  1     7.84290688E-05   # Y_e(Q)^DRbar
  2  2     1.62166440E-02   # Y_mu(Q)^DRbar
  3  3     2.72737274E-01   # Y_tau(Q)^DRbar
Block Au Q=  4.57316399E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     7.57171435E+03   # A_t(Q)^DRbar
Block Ad Q=  4.57316399E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.74543566E+03   # A_b(Q)^DRbar
Block Ae Q=  4.57316399E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.77053569E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  4.57316399E+03  # soft SUSY breaking masses at Q
   1    6.43883783E+01  # M_1
   2    1.41838167E+03  # M_2
   3    1.17987079E+03  # M_3
  21    4.41537763E+05  # M^2_(H,d)
  22    3.42277402E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.53908299E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.77097499E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.17346383E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.28380802E+02  # h0
        35     7.21614806E+02  # H0
        36     7.21564776E+02  # A0
        37     7.26923607E+02  # H+
   1000001     1.01226705E+04  # ~d_L
   2000001     1.00978749E+04  # ~d_R
   1000002     1.01223144E+04  # ~u_L
   2000002     1.01012260E+04  # ~u_R
   1000003     1.01226705E+04  # ~s_L
   2000003     1.00978749E+04  # ~s_R
   1000004     1.01223145E+04  # ~c_L
   2000004     1.01012261E+04  # ~c_R
   1000005     4.20984612E+03  # ~b_1
   2000005     4.51029781E+03  # ~b_2
   1000006     4.44954564E+03  # ~t_1
   2000006     4.70021674E+03  # ~t_2
   1000011     1.00210492E+04  # ~e_L-
   2000011     1.00088707E+04  # ~e_R-
   1000012     1.00202865E+04  # ~nu_eL
   1000013     1.00210493E+04  # ~mu_L-
   2000013     1.00088705E+04  # ~mu_R-
   1000014     1.00202865E+04  # ~nu_muL
   1000015     1.00087853E+04  # ~tau_1-
   2000015     1.00210892E+04  # ~tau_2-
   1000016     1.00202749E+04  # ~nu_tauL
   1000021     1.51368071E+03  # ~g
   1000022     6.36762063E+01  # ~chi_10
   1000023     3.01894276E+02  # ~chi_20
   1000025     3.05047467E+02  # ~chi_30
   1000035     1.51477130E+03  # ~chi_40
   1000024     2.99987698E+02  # ~chi_1+
   1000037     1.51473527E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -3.75449093E-02   # alpha
Block Hmix Q=  4.57316399E+03  # Higgs mixing parameters
   1   -2.80442371E+02  # mu
   2    2.66567537E+01  # tan[beta](Q)
   3    2.42765168E+02  # v(Q)
   4    5.20655726E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -8.80111671E-01   # Re[R_st(1,1)]
   1  2     4.74766728E-01   # Re[R_st(1,2)]
   2  1    -4.74766728E-01   # Re[R_st(2,1)]
   2  2    -8.80111671E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -3.85820343E-03   # Re[R_sb(1,1)]
   1  2     9.99992557E-01   # Re[R_sb(1,2)]
   2  1    -9.99992557E-01   # Re[R_sb(2,1)]
   2  2    -3.85820343E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -6.63910299E-02   # Re[R_sta(1,1)]
   1  2     9.97793682E-01   # Re[R_sta(1,2)]
   2  1    -9.97793682E-01   # Re[R_sta(2,1)]
   2  2    -6.63910299E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.88023350E-01   # Re[N(1,1)]
   1  2     1.08894001E-03   # Re[N(1,2)]
   1  3     1.51947890E-01   # Re[N(1,3)]
   1  4     2.68423523E-02   # Re[N(1,4)]
   2  1    -1.26605630E-01   # Re[N(2,1)]
   2  2    -4.45762597E-02   # Re[N(2,2)]
   2  3    -6.98798834E-01   # Re[N(2,3)]
   2  4    -7.02612383E-01   # Re[N(2,4)]
   3  1     8.81928409E-02   # Re[N(3,1)]
   3  2    -3.25247795E-02   # Re[N(3,2)]
   3  3     6.98940731E-01   # Re[N(3,3)]
   3  4    -7.08975328E-01   # Re[N(3,4)]
   4  1     1.70184854E-03   # Re[N(4,1)]
   4  2    -9.98475793E-01   # Re[N(4,2)]
   4  3     8.59550864E-03   # Re[N(4,3)]
   4  4     5.44913841E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.21678382E-02   # Re[U(1,1)]
   1  2    -9.99925969E-01   # Re[U(1,2)]
   2  1     9.99925969E-01   # Re[U(2,1)]
   2  2    -1.21678382E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -7.70812423E-02   # Re[V(1,1)]
   1  2     9.97024815E-01   # Re[V(1,2)]
   2  1     9.97024815E-01   # Re[V(2,1)]
   2  2     7.70812423E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02847486E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.76230903E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.60018082E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     7.76449869E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.38942295E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.80701542E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.98996613E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.01640081E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.06138513E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.03892852E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.74229605E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.64751342E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     8.25510122E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.03717979E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.38994629E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.80457025E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     4.17231883E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     2.51348241E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.01526536E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.05910296E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     8.01830714E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     6.17842655E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     9.93810096E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     9.25752516E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.96940408E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.84275654E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     3.95602588E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.53416480E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.12786334E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     5.10932373E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     4.84335077E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.72166514E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.30490332E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.46897617E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.38946215E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.87906803E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.68473589E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     2.00948687E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02706980E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     3.76518350E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.02559195E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.38998544E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.87572533E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.68410164E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     2.00873036E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02593020E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     4.14018405E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.02332402E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.53747935E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.02424744E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.52254015E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     1.81602616E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.73564235E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     9.96631688E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.44561842E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.67278060E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.17198120E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.17588196E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     9.92653287E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.92847550E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.56379370E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.15889004E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     4.75760748E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     9.53710521E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.55334239E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.67307224E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.17214816E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.26863635E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     9.92615558E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.92865819E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.56414035E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.23884155E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     4.75751024E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     9.53691242E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.55316738E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     2.87157720E+02   # ~b_1
#    BR                NDA      ID1      ID2
     9.36128822E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.81229997E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.80431460E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     5.71504175E-02    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     8.77319429E-01    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     4.36601619E+02   # ~b_2
#    BR                NDA      ID1      ID2
     2.34004434E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.97847024E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.98844018E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     3.57071215E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.07415752E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     7.27790803E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     6.42081559E-01    2     1000021         5   # BR(~b_2 -> ~g b)
DECAY   2000002     7.84472132E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.80684509E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     4.60095936E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     2.23250648E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.71248123E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.92837465E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.52551627E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     2.24418835E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     4.75170613E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     5.91981487E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     9.48158716E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.55311059E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.84479396E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.80681935E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     4.62385477E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     2.25583098E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.71239101E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.92855712E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.52548801E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     2.26433021E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     4.75161024E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     6.08338972E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     9.48139369E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.55293553E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     4.29928337E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.12911375E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.05140649E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.00937472E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     3.15479607E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     6.99288321E-02    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     6.20280461E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     3.83533120E-03    2     1000021         4   # BR(~t_1 -> ~g c)
     6.20337128E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
     1.10433911E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     5.11537768E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.69131098E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     9.04915985E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     9.71354796E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     4.57607902E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.59519155E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     8.89257823E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     5.93541085E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     1.14877276E-02    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     1.09031044E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     6.53664204E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.81071749E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99847394E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.51617841E-04    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     2.07775560E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.53481531E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.53326028E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     4.11855211E-03    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     9.29211590E-02    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     8.59575326E-02    2          37   1000025   # BR(chi^+_2 -> H^+ chi^0_3)
     1.52548560E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     9.53159355E-02    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     1.52232107E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     9.51173981E-02    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     5.56228117E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     4.52367321E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     4.87208555E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.49581733E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     4.51580152E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     5.48403602E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     2.43152484E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     5.90006576E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     4.09956114E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     2.21305014E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.44152328E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.44152328E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     8.86887663E-02    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     8.86887663E-02    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     4.95278085E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     9.44809105E-02    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.73861443E-03    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     1.99416788E-02    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     2.30058879E-02    2     1000025        36   # BR(chi^0_4 -> chi^0_3 A^0)
     9.40354938E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.09694816E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
     2.15725322E-03    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     6.78411916E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
     5.85407297E-02    2     1000025        35   # BR(chi^0_4 -> chi^0_3 H^0)
#    BR                NDA      ID1      ID2       ID3
     2.42874331E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     2.55550394E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     4.17708348E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     4.17708348E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.67359612E-03   # ~g
#    BR                NDA      ID1      ID2
     1.65568809E-02    2     1000023        21   # BR(~g -> chi^0_2 g)
     1.51275137E-02    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     8.98767199E-04    3     1000022         2        -2   # BR(~g -> chi^0_1 u u_bar)
     8.98767443E-04    3     1000022         4        -4   # BR(~g -> chi^0_1 c c_bar)
     1.65652707E-02    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     2.65148665E-04    3     1000022         1        -1   # BR(~g -> chi^0_1 d d_bar)
     2.65184566E-04    3     1000022         3        -3   # BR(~g -> chi^0_1 s s_bar)
     1.23793160E-02    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     1.86096827E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     5.01634667E-02    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.80828082E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     4.98443662E-02    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     2.34980593E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.34980593E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     4.32309594E-03   # Gamma(h0)
     2.35552665E-03   2        22        22   # BR(h0 -> photon photon)
     1.79606550E-03   2        22        23   # BR(h0 -> photon Z)
     3.74011512E-02   2        23        23   # BR(h0 -> Z Z)
     2.89924507E-01   2       -24        24   # BR(h0 -> W W)
     7.00949257E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.62783486E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.05856033E-04   2       -13        13   # BR(h0 -> Muon muon)
     5.93592581E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.20136651E-07   2        -2         2   # BR(h0 -> Up up)
     2.33171182E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.33238172E-07   2        -1         1   # BR(h0 -> Down down)
     1.92861135E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.15170634E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     1.81438721E-04   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     9.01769552E+00   # Gamma(HH)
     1.67775126E-08   2        22        22   # BR(HH -> photon photon)
     6.57672595E-09   2        22        23   # BR(HH -> photon Z)
     1.85722702E-05   2        23        23   # BR(HH -> Z Z)
     3.35998540E-05   2       -24        24   # BR(HH -> W W)
     8.45397148E-05   2        21        21   # BR(HH -> gluon gluon)
     9.24939001E-09   2       -11        11   # BR(HH -> Electron electron)
     4.11646667E-04   2       -13        13   # BR(HH -> Muon muon)
     1.18858924E-01   2       -15        15   # BR(HH -> Tau tau)
     3.17444536E-13   2        -2         2   # BR(HH -> Up up)
     6.15547073E-08   2        -4         4   # BR(HH -> Charm charm)
     3.36169075E-03   2        -6         6   # BR(HH -> Top top)
     8.19797471E-07   2        -1         1   # BR(HH -> Down down)
     2.96527202E-04   2        -3         3   # BR(HH -> Strange strange)
     8.10004192E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.13197600E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     4.47693166E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     3.05135773E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     2.89454338E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     4.77851359E-05   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.17416748E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     4.65906073E-04   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     1.73616528E-04   2        25        25   # BR(HH -> h0 h0)
DECAY        36     8.89659703E+00   # Gamma(A0)
     1.53732696E-07   2        22        22   # BR(A0 -> photon photon)
     1.16724475E-07   2        22        23   # BR(A0 -> photon Z)
     1.39820424E-04   2        21        21   # BR(A0 -> gluon gluon)
     9.21319109E-09   2       -11        11   # BR(A0 -> Electron electron)
     4.10035434E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.18396668E-01   2       -15        15   # BR(A0 -> Tau tau)
     2.92090717E-13   2        -2         2   # BR(A0 -> Up up)
     5.66078294E-08   2        -4         4   # BR(A0 -> Charm charm)
     4.06335387E-03   2        -6         6   # BR(A0 -> Top top)
     8.16596087E-07   2        -1         1   # BR(A0 -> Down down)
     2.95369325E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.06884436E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.81199300E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     4.55234902E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     3.13819786E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     2.90417607E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.07086453E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     4.19405164E-04   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     1.46437026E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     3.02200262E-05   2        23        25   # BR(A0 -> Z h0)
     1.85832007E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     8.51279551E+00   # Gamma(Hp)
     1.09150038E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     4.66650485E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.31993813E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     8.37881569E-07   2        -1         2   # BR(Hp -> Down up)
     1.38857397E-05   2        -3         2   # BR(Hp -> Strange up)
     9.17750232E-06   2        -5         2   # BR(Hp -> Bottom up)
     4.14825913E-08   2        -1         4   # BR(Hp -> Down charm)
     3.02036366E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.28517138E-03   2        -5         4   # BR(Hp -> Bottom charm)
     3.00524787E-07   2        -1         6   # BR(Hp -> Down top)
     6.94537575E-06   2        -3         6   # BR(Hp -> Strange top)
     7.94801969E-01   2        -5         6   # BR(Hp -> Bottom top)
     6.93461454E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     1.73812901E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     2.13576244E-06   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     3.27488988E-05   2        24        25   # BR(Hp -> W h0)
     6.68971244E-10   2        24        35   # BR(Hp -> W HH)
     7.01054919E-10   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.00258988E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    7.10579928E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    7.10582518E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99996355E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.41094082E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.40729609E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.00258988E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    7.10579928E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    7.10582518E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999998E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.35680201E-09        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999998E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.35680201E-09        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26506450E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.43709448E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.24975073E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.35680201E-09        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999998E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.69182780E-04   # BR(b -> s gamma)
    2    1.58739320E-06   # BR(b -> s mu+ mu-)
    3    3.52584339E-05   # BR(b -> s nu nu)
    4    3.45667875E-15   # BR(Bd -> e+ e-)
    5    1.47664188E-10   # BR(Bd -> mu+ mu-)
    6    3.08416014E-08   # BR(Bd -> tau+ tau-)
    7    1.17568275E-13   # BR(Bs -> e+ e-)
    8    5.02247154E-09   # BR(Bs -> mu+ mu-)
    9    1.06285477E-06   # BR(Bs -> tau+ tau-)
   10    8.85002217E-05   # BR(B_u -> tau nu)
   11    9.14173408E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41904084E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93253536E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15737552E-03   # epsilon_K
   17    2.28166919E-15   # Delta(M_K)
   18    2.48062427E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29183267E-11   # BR(K^+ -> pi^+ nu nu)
   20   -2.59049915E-16   # Delta(g-2)_electron/2
   21   -1.10752162E-11   # Delta(g-2)_muon/2
   22   -3.13480323E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    5.88757070E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.36785044E-01   # C7
     0305 4322   00   2    -1.06201477E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.52308540E-01   # C8
     0305 6321   00   2    -1.20278582E-03   # C8'
 03051111 4133   00   0     1.63125069E+00   # C9 e+e-
 03051111 4133   00   2     1.63143754E+00   # C9 e+e-
 03051111 4233   00   2     8.78402720E-05   # C9' e+e-
 03051111 4137   00   0    -4.45394279E+00   # C10 e+e-
 03051111 4137   00   2    -4.45286378E+00   # C10 e+e-
 03051111 4237   00   2    -6.44839894E-04   # C10' e+e-
 03051313 4133   00   0     1.63125069E+00   # C9 mu+mu-
 03051313 4133   00   2     1.63143746E+00   # C9 mu+mu-
 03051313 4233   00   2     8.78307092E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.45394279E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.45286386E+00   # C10 mu+mu-
 03051313 4237   00   2    -6.44830429E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50511860E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.39295012E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50511860E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.39297073E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50511860E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.39877897E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85829062E-07   # C7
     0305 4422   00   2    -1.04185910E-05   # C7
     0305 4322   00   2     9.17469378E-07   # C7'
     0305 6421   00   0     3.30488108E-07   # C8
     0305 6421   00   2     2.91902345E-05   # C8
     0305 6321   00   2     9.32770966E-07   # C8'
 03051111 4133   00   2    -9.56059385E-08   # C9 e+e-
 03051111 4233   00   2     4.41843887E-06   # C9' e+e-
 03051111 4137   00   2     3.83750230E-06   # C10 e+e-
 03051111 4237   00   2    -3.24798177E-05   # C10' e+e-
 03051313 4133   00   2    -9.56074782E-08   # C9 mu+mu-
 03051313 4233   00   2     4.41843841E-06   # C9' mu+mu-
 03051313 4137   00   2     3.83750417E-06   # C10 mu+mu-
 03051313 4237   00   2    -3.24798190E-05   # C10' mu+mu-
 03051212 4137   00   2    -8.00930646E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     7.01615111E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -8.00930573E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     7.01615111E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -8.00909864E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     7.01615109E-06   # C11' nu_3 nu_3
