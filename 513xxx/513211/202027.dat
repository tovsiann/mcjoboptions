# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  15:59
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    4.58527630E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    4.52985424E+03  # scale for input parameters
    1    3.42898448E+02  # M_1
    2   -8.24562826E+02  # M_2
    3    3.78783489E+03  # M_3
   11   -7.43938618E+03  # A_t
   12   -1.97288148E+03  # A_b
   13    9.43032368E+02  # A_tau
   23   -9.77786124E+02  # mu
   25    4.51125643E+01  # tan(beta)
   26    7.09039950E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.11041383E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.97407102E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.84773819E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  4.52985424E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  4.52985424E+03  # (SUSY scale)
  1  1     8.38643178E-06   # Y_u(Q)^DRbar
  2  2     4.26030734E-03   # Y_c(Q)^DRbar
  3  3     1.01314832E+00   # Y_t(Q)^DRbar
Block Yd Q=  4.52985424E+03  # (SUSY scale)
  1  1     7.60247464E-04   # Y_d(Q)^DRbar
  2  2     1.44447018E-02   # Y_s(Q)^DRbar
  3  3     7.53927257E-01   # Y_b(Q)^DRbar
Block Ye Q=  4.52985424E+03  # (SUSY scale)
  1  1     1.32668742E-04   # Y_e(Q)^DRbar
  2  2     2.74316881E-02   # Y_mu(Q)^DRbar
  3  3     4.61355867E-01   # Y_tau(Q)^DRbar
Block Au Q=  4.52985424E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -7.43938606E+03   # A_t(Q)^DRbar
Block Ad Q=  4.52985424E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.97288138E+03   # A_b(Q)^DRbar
Block Ae Q=  4.52985424E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     9.43032363E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  4.52985424E+03  # soft SUSY breaking masses at Q
   1    3.42898448E+02  # M_1
   2   -8.24562826E+02  # M_2
   3    3.78783489E+03  # M_3
  21   -3.42242733E+05  # M^2_(H,d)
  22   -5.31476071E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.11041383E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.97407102E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.84773819E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.28222105E+02  # h0
        35     7.09130729E+02  # H0
        36     7.09039950E+02  # A0
        37     7.13435164E+02  # H+
   1000001     1.01113867E+04  # ~d_L
   2000001     1.00866685E+04  # ~d_R
   1000002     1.01110299E+04  # ~u_L
   2000002     1.00901696E+04  # ~u_R
   1000003     1.01113880E+04  # ~s_L
   2000003     1.00866697E+04  # ~s_R
   1000004     1.01110308E+04  # ~c_L
   2000004     1.00901698E+04  # ~c_R
   1000005     3.96213500E+03  # ~b_1
   2000005     4.18058024E+03  # ~b_2
   1000006     4.16301698E+03  # ~t_1
   2000006     4.92901651E+03  # ~t_2
   1000011     1.00213605E+04  # ~e_L-
   2000011     1.00086481E+04  # ~e_R-
   1000012     1.00205984E+04  # ~nu_eL
   1000013     1.00213687E+04  # ~mu_L-
   2000013     1.00086513E+04  # ~mu_R-
   1000014     1.00206023E+04  # ~nu_muL
   1000015     1.00094892E+04  # ~tau_1-
   2000015     1.00237151E+04  # ~tau_2-
   1000016     1.00216645E+04  # ~nu_tauL
   1000021     4.30698749E+03  # ~g
   1000022     3.43191422E+02  # ~chi_10
   1000023     8.60145828E+02  # ~chi_20
   1000025     9.86680186E+02  # ~chi_30
   1000035     1.01142322E+03  # ~chi_40
   1000024     8.60300199E+02  # ~chi_1+
   1000037     1.01197684E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.30758175E-02   # alpha
Block Hmix Q=  4.52985424E+03  # Higgs mixing parameters
   1   -9.77786124E+02  # mu
   2    4.51125643E+01  # tan[beta](Q)
   3    2.42794850E+02  # v(Q)
   4    5.02737651E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.89630606E-01   # Re[R_st(1,1)]
   1  2     1.43635870E-01   # Re[R_st(1,2)]
   2  1    -1.43635870E-01   # Re[R_st(2,1)]
   2  2     9.89630606E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -5.42938433E-02   # Re[R_sb(1,1)]
   1  2     9.98525001E-01   # Re[R_sb(1,2)]
   2  1    -9.98525001E-01   # Re[R_sb(2,1)]
   2  2    -5.42938433E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -3.01320528E-01   # Re[R_sta(1,1)]
   1  2     9.53522910E-01   # Re[R_sta(1,2)]
   2  1    -9.53522910E-01   # Re[R_sta(2,1)]
   2  2    -3.01320528E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.98574259E-01   # Re[N(1,1)]
   1  2     1.05743347E-03   # Re[N(1,2)]
   1  3    -5.06807425E-02   # Re[N(1,3)]
   1  4    -1.67270358E-02   # Re[N(1,4)]
   2  1     9.97758341E-03   # Re[N(2,1)]
   2  2     9.15572330E-01   # Re[N(2,2)]
   2  3     3.02929490E-01   # Re[N(2,3)]
   2  4    -2.64313223E-01   # Re[N(2,4)]
   3  1     4.76507048E-02   # Re[N(3,1)]
   3  2    -2.96042125E-02   # Re[N(3,2)]
   3  3     7.04875703E-01   # Re[N(3,3)]
   3  4     7.07109075E-01   # Re[N(3,4)]
   4  1     2.18930973E-02   # Re[N(4,1)]
   4  2    -4.01060819E-01   # Re[N(4,2)]
   4  3     6.39386761E-01   # Re[N(4,3)]
   4  4    -6.55633649E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.01759587E-01   # Re[U(1,1)]
   1  2    -4.32237952E-01   # Re[U(1,2)]
   2  1    -4.32237952E-01   # Re[U(2,1)]
   2  2     9.01759587E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     9.25737592E-01   # Re[V(1,1)]
   1  2     3.78166512E-01   # Re[V(1,2)]
   2  1     3.78166512E-01   # Re[V(2,1)]
   2  2    -9.25737592E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.01691834E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.97198819E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     2.23205782E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     4.70725139E-04    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.42814116E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.80362526E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.58085431E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     4.58753923E-02    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     4.94931782E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     1.13065918E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.04630820E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.91399032E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     3.66210659E-04    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     3.66572056E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.65713540E-03    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     5.46564611E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
     2.36533692E-03    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
DECAY   1000013     1.42961373E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.79483319E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.57914711E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     5.16543990E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     4.62484667E-02    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     4.94422396E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     1.12949551E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.38057681E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     3.43387578E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.00868131E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.43482918E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     8.09178544E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.95497590E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.35845928E-01    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.79723311E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.58134469E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.70276370E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.20402926E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.63528250E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.86465722E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     1.73513285E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.42818142E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.73318476E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.52117917E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     9.18177513E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     5.15474349E-02    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     5.21546889E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     8.65377346E-02    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.42965322E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.72419755E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.51858468E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     9.17232641E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     5.14943888E-02    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     5.21203296E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     8.72846392E-02    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.84453936E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.76261275E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.95230081E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     7.11000842E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     3.99163309E-02    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     4.46209251E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     2.50307209E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.37931369E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.04144782E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.89555838E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.67646993E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.07799659E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     5.48601396E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.07141624E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.06848377E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     2.44117329E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.00992628E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.38013410E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.04130903E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.89405164E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.67691786E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.07801815E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     5.48621404E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.25512281E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     1.07385575E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.06841976E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     2.44147092E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.00939087E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     8.10717895E+01   # ~b_1
#    BR                NDA      ID1      ID2
     2.91353865E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     5.58920157E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.40371561E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.88935450E-01    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     1.12757343E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     3.72908245E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.68852142E+02   # ~b_2
#    BR                NDA      ID1      ID2
     3.59508701E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.02022088E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.24824365E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.21626744E-01    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.14351450E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     4.32674619E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     3.42186001E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     5.63462034E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     5.55142574E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.03804576E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.59506086E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.67630176E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.12799603E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     5.52870085E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.03053637E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.12604950E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.86860358E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.00959479E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.55149710E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.03799394E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.59493732E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.67674889E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.12785519E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     5.52836923E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.03069692E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.12608953E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.87347535E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.00905929E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.70616513E+02   # ~t_1
#    BR                NDA      ID1      ID2
     7.17756197E-04    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     5.15750174E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.75373871E-02    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     9.97917874E-02    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.17653682E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.15828179E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     1.82912159E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.57911890E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     5.33829778E-04    2     1000005        24   # BR(~t_1 -> ~b_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.95582755E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     3.02345903E+02   # ~t_2
#    BR                NDA      ID1      ID2
     3.48442648E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     3.29767322E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.55359057E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.20799568E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     6.73281594E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     2.39605387E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     6.36266360E-02    2     1000021         6   # BR(~t_2 -> ~g t)
     7.18499016E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.20384359E-01    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     5.26187231E-04    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     1.43018508E-03    2     2000005        37   # BR(~t_2 -> ~b_2 H^+)
     6.27667113E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     9.67005454E-04    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     9.77687331E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     8.98516849E-04    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     6.15576266E-02   # chi^+_1
#    BR                NDA      ID1      ID2
     9.88698179E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.08501094E-02    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
     4.48715116E-04    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     2.79274822E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     2.05443371E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     5.04866028E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.90097398E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     9.50105634E-02    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     4.34257381E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     2.29995658E-04    3     1000022       -15        16   # BR(chi^+_2 -> chi^0_1 tau^+ nu_tau)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.02823747E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     5.07262069E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     4.89429342E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     8.90259553E-04    3     1000022         6        -6   # BR(chi^0_2 -> chi^0_1 t t_bar)
     2.12314506E-03    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     2.60537974E-04    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
DECAY   1000025     2.29949005E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     1.50141920E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     1.50141920E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     5.63504119E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     4.43814917E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     1.96100380E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     2.54412771E-04    2     1000023        22   # BR(chi^0_3 -> chi^0_2 photon)
#    BR                NDA      ID1      ID2       ID3
     9.54537920E-04    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     1.98217389E-03    3     1000022         5        -5   # BR(chi^0_3 -> chi^0_1 b b_bar)
     2.44881010E-04    3     1000022        15       -15   # BR(chi^0_3 -> chi^0_1 tau^- tau^+)
DECAY   1000035     3.79839152E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     3.40957280E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     3.40957280E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.23257798E-01    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.12203577E-03    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.18831445E-01    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     7.26006060E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     7.30344124E-04    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     1.35922113E-03    3     1000022         5        -5   # BR(chi^0_4 -> chi^0_1 b b_bar)
     1.68199926E-04    3     1000022        15       -15   # BR(chi^0_4 -> chi^0_1 tau^- tau^+)
DECAY   1000021     5.00043565E+00   # ~g
#    BR                NDA      ID1      ID2
     1.16635699E-02    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     1.16635699E-02    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     3.01636223E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     3.01636223E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     4.24648672E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     4.24648672E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
     4.34896696E-04    2     1000025        21   # BR(~g -> chi^0_3 g)
     3.20784263E-04    2     1000035        21   # BR(~g -> chi^0_4 g)
#    BR                NDA      ID1      ID2       ID3
     3.16937431E-03    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     3.21896714E-02    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     7.08096560E-02    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     6.32659907E-02    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     1.03763963E-04    3     1000024         1        -2   # BR(~g -> chi^+_1 d u_bar)
     1.03763963E-04    3    -1000024        -1         2   # BR(~g -> chi^-_1 d_bar u)
     1.03788290E-04    3     1000024         3        -4   # BR(~g -> chi^+_1 s c_bar)
     1.03788290E-04    3    -1000024        -3         4   # BR(~g -> chi^-_1 s_bar c)
     2.57335312E-02    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.57335312E-02    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     3.28070710E-02    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     3.28070710E-02    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     4.31909292E-03   # Gamma(h0)
     2.34847841E-03   2        22        22   # BR(h0 -> photon photon)
     1.77597395E-03   2        22        23   # BR(h0 -> photon Z)
     3.67421938E-02   2        23        23   # BR(h0 -> Z Z)
     2.85611430E-01   2       -24        24   # BR(h0 -> W W)
     6.97903535E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.66578950E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.07544254E-04   2       -13        13   # BR(h0 -> Muon muon)
     5.98452014E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.18429188E-07   2        -2         2   # BR(h0 -> Up up)
     2.29859277E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.39153940E-07   2        -1         1   # BR(h0 -> Down down)
     1.94997816E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.20497237E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     2.30880940E+01   # Gamma(HH)
     7.93270815E-08   2        22        22   # BR(HH -> photon photon)
     5.35310517E-08   2        22        23   # BR(HH -> photon Z)
     4.34049561E-06   2        23        23   # BR(HH -> Z Z)
     7.94664433E-06   2       -24        24   # BR(HH -> W W)
     1.01788787E-04   2        21        21   # BR(HH -> gluon gluon)
     9.32927305E-09   2       -11        11   # BR(HH -> Electron electron)
     4.15199539E-04   2       -13        13   # BR(HH -> Muon muon)
     1.19884302E-01   2       -15        15   # BR(HH -> Tau tau)
     3.76433567E-14   2        -2         2   # BR(HH -> Up up)
     7.29929983E-09   2        -4         4   # BR(HH -> Charm charm)
     4.57180572E-04   2        -6         6   # BR(HH -> Top top)
     8.66893962E-07   2        -1         1   # BR(HH -> Down down)
     3.13504917E-04   2        -3         3   # BR(HH -> Strange strange)
     8.78752036E-01   2        -5         5   # BR(HH -> Bottom bottom)
     3.19566398E-06   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     5.94889422E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     2.25075435E+01   # Gamma(A0)
     7.77154729E-08   2        22        22   # BR(A0 -> photon photon)
     1.82703084E-08   2        22        23   # BR(A0 -> photon Z)
     1.30790642E-04   2        21        21   # BR(A0 -> gluon gluon)
     9.32770220E-09   2       -11        11   # BR(A0 -> Electron electron)
     4.15129754E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.19867272E-01   2       -15        15   # BR(A0 -> Tau tau)
     3.43431432E-14   2        -2         2   # BR(A0 -> Up up)
     6.65772913E-09   2        -4         4   # BR(A0 -> Charm charm)
     5.59101791E-04   2        -6         6   # BR(A0 -> Top top)
     8.66762496E-07   2        -1         1   # BR(A0 -> Down down)
     3.13457367E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.78659599E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     4.64884154E-05   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     7.18215702E-06   2        23        25   # BR(A0 -> Z h0)
     1.83805704E-35   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     2.19677377E+01   # Gamma(Hp)
     1.12917399E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     4.82757126E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.36549574E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     9.19573758E-07   2        -1         2   # BR(Hp -> Down up)
     1.52321921E-05   2        -3         2   # BR(Hp -> Strange up)
     1.00361930E-05   2        -5         2   # BR(Hp -> Bottom up)
     4.25797048E-08   2        -1         4   # BR(Hp -> Down charm)
     3.31351799E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.40541769E-03   2        -5         4   # BR(Hp -> Bottom charm)
     4.19731995E-08   2        -1         6   # BR(Hp -> Down top)
     1.34436305E-06   2        -3         6   # BR(Hp -> Strange top)
     8.61195667E-01   2        -5         6   # BR(Hp -> Bottom top)
     7.60402378E-06   2        24        25   # BR(Hp -> W h0)
     9.09395195E-11   2        24        35   # BR(Hp -> W HH)
     1.00929891E-10   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.08404042E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.03505942E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.03514346E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99958705E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    5.32660446E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    4.91365852E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.08404042E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.03505942E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.03514346E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999167E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    8.32972651E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999167E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    8.32972651E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.25968995E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    4.18327664E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.87632759E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    8.32972651E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999167E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.87701316E-04   # BR(b -> s gamma)
    2    1.58796858E-06   # BR(b -> s mu+ mu-)
    3    3.52607384E-05   # BR(b -> s nu nu)
    4    9.88361031E-16   # BR(Bd -> e+ e-)
    5    4.22081742E-11   # BR(Bd -> mu+ mu-)
    6    8.06276868E-09   # BR(Bd -> tau+ tau-)
    7    3.44616105E-14   # BR(Bs -> e+ e-)
    8    1.47179044E-09   # BR(Bs -> mu+ mu-)
    9    2.88461193E-07   # BR(Bs -> tau+ tau-)
   10    7.08634479E-05   # BR(B_u -> tau nu)
   11    7.31992287E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41621468E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.91962679E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15711882E-03   # epsilon_K
   17    2.28168534E-15   # Delta(M_K)
   18    2.48071223E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29208067E-11   # BR(K^+ -> pi^+ nu nu)
   20    3.23099013E-16   # Delta(g-2)_electron/2
   21    1.38135501E-11   # Delta(g-2)_muon/2
   22    3.91138287E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.68858892E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.56702688E-01   # C7
     0305 4322   00   2    -1.21596869E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.63701921E-01   # C8
     0305 6321   00   2    -1.29150571E-03   # C8'
 03051111 4133   00   0     1.63168228E+00   # C9 e+e-
 03051111 4133   00   2     1.63183657E+00   # C9 e+e-
 03051111 4233   00   2     1.99597690E-04   # C9' e+e-
 03051111 4137   00   0    -4.45437438E+00   # C10 e+e-
 03051111 4137   00   2    -4.45355437E+00   # C10 e+e-
 03051111 4237   00   2    -1.46590743E-03   # C10' e+e-
 03051313 4133   00   0     1.63168228E+00   # C9 mu+mu-
 03051313 4133   00   2     1.63183638E+00   # C9 mu+mu-
 03051313 4233   00   2     1.99523983E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.45437438E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.45355455E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.46583434E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50517490E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     3.16628360E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50517490E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     3.16644252E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50517491E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     3.21122837E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85766227E-07   # C7
     0305 4422   00   2     4.75846219E-06   # C7
     0305 4322   00   2    -1.61830268E-06   # C7'
     0305 6421   00   0     3.30434286E-07   # C8
     0305 6421   00   2     1.77035072E-05   # C8
     0305 6321   00   2    -4.87430577E-07   # C8'
 03051111 4133   00   2     1.05022704E-07   # C9 e+e-
 03051111 4233   00   2     1.14238656E-05   # C9' e+e-
 03051111 4137   00   2     3.39274141E-06   # C10 e+e-
 03051111 4237   00   2    -8.39601282E-05   # C10' e+e-
 03051313 4133   00   2     1.05017294E-07   # C9 mu+mu-
 03051313 4233   00   2     1.14238627E-05   # C9' mu+mu-
 03051313 4137   00   2     3.39274748E-06   # C10 mu+mu-
 03051313 4237   00   2    -8.39601369E-05   # C10' mu+mu-
 03051212 4137   00   2    -6.98757214E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     1.81349765E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -6.98756934E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     1.81349765E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -6.98679115E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     1.81349765E-05   # C11' nu_3 nu_3
