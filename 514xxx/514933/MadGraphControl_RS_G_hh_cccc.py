from MadGraphControl.MadGraphUtils import *
import re

#Getting the Physics short, as while running in release 21, need to pass the directory itself##
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()

mh  = 125

#Getting Graviton Mass from JO name
parameter_re = re.compile('RS_G_hh_cccc_c([0-9]+)_M([0-9]+)')
c_str, mhh = parameter_re.search(jofile).groups()
c = float(c_str) * 0.1

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

#Change defaults for run_card.dat
extras = { 'lhe_version':'2.0',
           'cut_decays':'F',
           'pdlabel':"'nn23lo1'",
           'lhaid':'247000',
           'scale':'91.18',
           'dsqrt_q2fact1':'91.18',
           'dsqrt_q2fact2':'91.18',
           'nevents':int(nevents)}

# MG5 Proc cards
process_1 = """
set group_subprocesses Auto
set ignore_six_quark_processes False
set gauge unitary
set complex_mass_scheme False
import model SMRS_Decay
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~

#Calculate widths first:
generate hh > all all
output SMRS_widths
launch
set c """+str(c)+"""
set Mh """+str(mh)+"""
set Mhh """+str(mhh)+"""
"""

process_2 = """
set group_subprocesses Auto
set ignore_six_quark_processes False
set gauge unitary
set complex_mass_scheme False
import model SMRS_Decay
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~

generate p p > hh, (hh > h h)
output -f
"""

# first process to compute widths and set parameters card
process_dir = new_process(process_1)
modify_run_card(process_dir=process_dir,runArgs=runArgs, settings=extras)
generate(process_dir=process_dir,grid_pack=False,runArgs=runArgs)

# second process to generate hh -> cc cc events using parameters from first process
process_dir = new_process(process_2)
modify_run_card(process_dir=process_dir,runArgs=runArgs, settings=extras)
#modify_param_card(param_card_input='SMRS_widths/Cards/param_card.dat',process_dir=process_dir,params=params)
modify_param_card(param_card_input='SMRS_widths/Cards/param_card.dat',process_dir=process_dir)
generate(process_dir=process_dir,runArgs=runArgs)

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py") 

genSeq.Pythia8.Commands += ["25:oneChannel = on 1.0 100 4 -4 "] #cc final state

# MC21 metadata
evgenConfig.contact = ["valentina.vecchio@cern.ch"]
evgenConfig.description = "Bulk Randall-Sundrum model KK graviton -> hh -> cccc with NNPDF2.3 LO A14 tune"
evgenConfig.keywords = ["exotic", "RandallSundrum", "warpedED", "graviton", "Higgs"]
evgenConfig.process = "RS_G_hh_cccc"
