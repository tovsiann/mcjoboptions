selected_operators = ['cHbox','cHW','ctHRe','ctWRe','cHQ3','cHl311','cll1221','ctWIm','ctHIm']

process_definition = 'set group_subprocesses False\n'
process_definition = 'generate p p > t h j QCD=0 QED=3 NP=1 $$ w+ w-, (t > w+ b NP=0, w+ > l+ vl NP=0) @0 NPprop=0 SMHLOOP=0\n'
process_definition+= 'add process p p > t~ h j QCD=0 QED=3 NP=1 $$ w+ w-, (t~ > w- b~ NP=0, w- > l- vl~ NP=0) @1 NPprop=0 SMHLOOP=0\n'

fixed_scale = 297.5  # ~ m(top)+m(Higgs)

gridpack = True

evgenConfig.description = 'SMEFTsim 3.0 tHq, multilepton, top model, reweighted, EFT vertices, no propagator correction'
evgenConfig.nEventsPerJob=10000

include("Common_SMEFTsim_topmW_tHq_reweighted.py")
