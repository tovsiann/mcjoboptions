selected_operators = ['ctWRe','cHQ3']

process_definition = 'set group_subprocesses False\ndefine uc = u c\ndefine uc~ = u~ c~\ndefine ds = d s\ndefine ds~ = d~ s~\n' # define W hadronic decay
process_definition+= 'generate p p > t t~ QCD=2 QED=2 / w+ w- z h a, (t > l+ vl b a), (t~ > ds uc~ b~) @0 NPprop=2 SMHLOOP=0 NP=0\n' # semi-leptonic +, photon in top decay
process_definition+= 'add process p p > t t~ QCD=2 QED=2 / w+ w- z h a, (t > l+ vl b), (t~ > ds uc~ b~ a) @1 NPprop=2 SMHLOOP=0 NP=0\n' # semi-leptonic +, photon in tbar decay
process_definition+= 'add process p p > t t~ QCD=2 QED=2 / w+ w- z h a, (t > uc ds~ b a), (t~ > l- vl~ b~) @2 NPprop=2 SMHLOOP=0 NP=0\n' # semi-leptonic -, photon in top decay
process_definition+= 'add process p p > t t~ QCD=2 QED=2 / w+ w- z h a, (t > uc ds~ b), (t~ > l- vl~ b~ a) @3 NPprop=2 SMHLOOP=0 NP=0\n' # semi-leptonic -, photon in tbar decay
process_definition+= 'add process p p > t t~ QCD=2 QED=2 / w+ w- z h a, (t > l+ vl b a), (t~ > l- vl~ b~) @4 NPprop=2 SMHLOOP=0 NP=0\n' # di-leptonic, photon in top decay
process_definition+= 'add process p p > t t~ QCD=2 QED=2 / w+ w- z h a, (t > l+ vl b), (t~ > l- vl~ b~ a) @5 NPprop=2 SMHLOOP=0 NP=0\n' # di-leptonic, photon in tbar decay

fixed_scale = 431.25 # ~ 2.5*m(top)

gridpack = True

evgenConfig.description = 'SMEFTsim 3.0 tt+photon, decay mode, top model, inclusive, reweighted, no EFT vertices, propagator correction'
evgenConfig.nEventsPerJob=10000

include("Common_SMEFTsim_topmW_ttgamma_decay_prop_reweighted.py")
