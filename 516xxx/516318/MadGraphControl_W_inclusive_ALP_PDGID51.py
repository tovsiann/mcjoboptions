from MadGraphControl.MadGraphUtils import *
import sys

mode=0

# merging parameters
bwcutoff=15
dparameter=0.4
#nJetMax=1
maxjetflavor=6 # was 4
ptj=0

ickkw_value=0
scalefact=1.0
alpsfact=1


if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
   raise RuntimeError("No center of mass energy found")
param_card_extras = {
      "ALPPARS": { 'CWtil':CWtil, 'fa':fa, 'CBtil':CBtil, 'CGtil' : CGtil, 'CaPhi' : CaPhi},
      "MASS": { 'Ma':Ma }
      }

process= """  
import model sm
import model ALP_linear_UFO_WIDTH
define p = g u c d s u~ c~ d~ s~
define j = u c d s b u~ c~ d~ s~ b~
define l+ = e+ mu+                                                               
define vl = ve vm
define vl~ = ve~ vm~
define dj = g a
generate p p > ax W+, (ax > g g) 
add process p p > ax W-, (ax > g g)
output -f
launch
set Ma """+str(Ma)+"""
set CGtil """+str(CGtil)+"""
set CWtil """+str(CWtil)+"""
set CaPhi """+str(CaPhi)+"""
set CBtil """+str(CBtil)+"""
set decay 9000005 auto
"""
Runname='run_01'     

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob
nevents = int(nevents)

#process_dir = new_process()
process_dir = new_process(process)

#Fetch default LO run_card.dat and set parameters
extras = {'lhe_version'  :'3.0',
          'pdlabel'      :"'nn23lo1'",
          'lhaid'        :247000,
          'ickkw'        :ickkw_value, 
          'maxjetflavor' :maxjetflavor,'asrwgtflavor' :maxjetflavor,
          #'ptj'          :ptj, #'drjj'         :0.0,'etaj'         :5,'etab'         :5,
          'ptj':'0',
          'ptb':'0',
          'pta':'0',
          'ptl':'0',
          'etaj':'-1',
          'etab':'-1',
          'etaa':'-1',
          'etal':'-1',
          'drjj':'0',
          'drbb':'0',
          'drll':'0',
          'draa':'0',
          'drbj':'0',
          'draj':'0',
          'drjl':'0',
          'drab':'0',
          'drbl':'0',
          'dral':'0',
          'ktdurham'     :ktdurham,
          #'dparameter'   :'0.0', #dparameter,
          'bwcutoff'     :bwcutoff,
          'scalefact'    :scalefact,
          'pt_min_pdg'   :pt_min_pdg,
          'alpsfact'     :alpsfact,
          'nevents'      :nevents ,
          'time_of_flight': '1e-2'} 


print "MAX EVENTS: " + str(runArgs.maxEvents)

modify_run_card(process_dir=process_dir,runArgs=runArgs, settings=extras)
modify_param_card(process_dir=process_dir,params=param_card_extras)

generate(process_dir=process_dir,grid_pack=False,runArgs=runArgs)

#This is old version:
#build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),run_card_new='run_card.dat', xqcut = qcut,
#               nevts=runArgs.maxEvents*extraEvents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy,extras=extras)


#if( build_param_card(param_card_old=process_dir + "/Cards/param_card.dat",param_card_new='param_card.dat',params=param_card_extras) == -1):
#    raise RuntimeError("Could not create param_card.dat")
    
#print_cards()
    
#generate(run_card_loc='run_card.dat',param_card_loc='param_card.dat',mode=mode,proc_dir=process_dir,run_name=runName)
#arrange_output(run_name=runName,proc_dir=process_dir,outputDS=runName+'._00000.events.tar.gz',lhe_version=3)

# replacing PDGID of ALP manually (so that Pythia recognises it)
unzip1 = subprocess.Popen(['gunzip',process_dir+'/Events/'+Runname+'/unweighted_events.lhe.gz'])
unzip1.wait()
    
oldlhe = open(process_dir+'/Events/'+Runname+'/unweighted_events.lhe','r')
newlhe = open(process_dir+'/Events/'+Runname+'/unweighted_events2.lhe','w')
init = True
for line in oldlhe:
    if init==True:
        newlhe.write(line)
        if '</init>' in line:
            init = False
    else:  
        if 'vent' in line or line.startswith("<"):
            newlhe.write(line)
            continue
        newline = line.rstrip('\n')
        columns = (' '.join(newline.split())).split()
        pdgid = int(columns[0])
        if pdgid == 9000005:
            part1 = "       %d" % (51)
            part2 = line[9:]
            newlhe.write(part1+part2)
        else:
            newlhe.write(line)

oldlhe.close()
newlhe.close()
    
zip1 = subprocess.Popen(['gzip',process_dir+'/Events/'+Runname+'/unweighted_events2.lhe'])
zip1.wait()
shutil.move(process_dir+'/Events/'+Runname+'/unweighted_events2.lhe.gz',
            process_dir+'/Events/'+Runname+'/unweighted_events.lhe.gz')
os.remove(process_dir+'/Events/'+Runname+'/unweighted_events.lhe')

arrange_output(runArgs=runArgs, process_dir=process_dir, saveProcDir=False)  

############################

evgenConfig.description = 'ALP linear with vector V'
evgenConfig.keywords+=['WZ','exotic','BSM']
evgenConfig.contact = ["Victoria Sanchez <victoria.sanchez.sebastian@cern.ch>"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
genSeq.Pythia8.Commands += ["Main:timesAllowErrors = 60000"]

PYTHIA8_Dparameter=0#dparameter
PYTHIA8_Process='pp>axZ'
PYTHIA8_TMS=ktdurham
#PYTHIA8_nQuarksMerge=maxjetflavor

##Check which pdgid corresponds now to DM ax -> didnt do anything so changed to 51
bonus_file = open('pdg_extras.dat','w')
bonus_file.write('9000005\n')
bonus_file.close()
testSeq.TestHepMC.G4ExtraWhiteFile='pdg_extras.dat'

# Turn off checks for displaced vertices.
# Other checks are fine.
genSeq.Pythia8.Commands += ["ParticleDecays:limitTau0 = off"] # Allow long-lived particles to decay 
testSeq.TestHepMC.MaxVtxDisp = 100000000 #In mm 
testSeq.TestHepMC.MaxTransVtxDisp = 100000000

evgenConfig.inputconfcheck=""
