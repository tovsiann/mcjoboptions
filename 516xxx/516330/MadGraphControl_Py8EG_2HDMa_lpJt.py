import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment # 4-flavour systematics
from MadGraphControl.MadGraphUtils import *
import re
		
# This control file makes use of UFO v2, registered in https://its.cern.ch/jira/browse/AGENE-1652 
# Z's don't contribute because sinbma (sin(beta-alpha)) is 1.0, so excluding them with '/z' reduces runtime for the LHE generation,
# without changing the physics/cross-sections/kinematic ditributions

from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short().split('_')
model_string = phys_short[2]

#----------------------------------------------------------------------------
# Process definition
#----------------------------------------------------------------------------
THDMparams = {}
# parameters tuned in the model
THDMparams['tanbeta'] = float(phys_short[5].replace("tb","").replace("p",".")) # The ratio of the vacuum expectation values $\tan \beta = v_2/v_1$ of the Higgs doublets $H_2$ and $H_1$, as defined in Section 2.1 of arXiv:1701.07427.
THDMparams['sinp'] = float(phys_short[6].replace("sp","").replace("p",".")) # The sine of the mixing angle $\theta$, as defined in Section 2.1 of arXiv:1701.07427.
THDMparams['mh3'] = float(phys_short[3].replace("mA","")) # The mass of the heavy pseudoscalar mass eigenstate $A$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams['mh2'] = THDMparams['mh3'] # The mass of the heavy scalar mass eigenstate $H$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams['mhc'] = THDMparams['mh3'] # The mass of the charged scalar eigenstate $H^\pm$. See Section 2.1 of arXiv:1701.07427 for further details.
THDMparams['mh4'] = float(phys_short[4].replace("ma","")) # The mass of the pseudoscalar mass eigenstate $a$ that decouples for $\sin \theta = 0$. See Section 2.1 of arXiv:1701.07427 for further details.
#THDMparams['MB'] = 4.7 # no need to set b quark mass as already 4.7 by default

# fixed parameters
THDMparams['gPXd'] = 1.0 # The coupling of the additional pseudoscalar mediator to dark matter (DM). This coupling is called $y_\chi$ in (2.5) of arXiv:1701.07427.
THDMparams['sinbma'] = 1.0 # The sine of the difference of the mixing angles $\sin (\beta - \alpha)$ in the scalar potential containing only the Higgs doublets.  This quantity is defined in Section 3.1 of arXiv:1701.07427. 
THDMparams['lam3'] = 3.0 # The quartic coupling of the scalar doublets $H_1$ and $H_2$. This parameter corresponds to the coefficient $\lambda_3$ in (2.1) of arXiv:1701.07427.
THDMparams['laP1'] = 3.0 # The quartic coupling between the scalar doublets $H_1$ and the pseudoscalar $P$. This parameter corresponds to the coefficient $\lambda_{P1}$ in (2.2) of arXiv:1701.07427.
THDMparams['laP2'] = 3.0 # The quartic coupling between the scalar doublets $H_2$ and the pseudoscalar $P$. This parameter corresponds to the coefficient $\lambda_{P2}$ in (2.2) of arXiv:1701.07427.
THDMparams['MXd'] = float(phys_short[7].replace("mX", "")) # The mass of the fermionic DM candidate denoted by $m_\chi$ in arXiv:1701.07427.
THDMparams['mh1'] = 125. # The mass of the lightest scalar mass eigenstate $h$, which is identified in arXiv:1701.07427 with the Higgs-like resonance found at the LHC.

# For the gluon-gluon fusion production use the 4FS to take into account the b-quark loops
process="""
import model Pseudoscalar_2HDM -modelname
define p = g d u s c d~ u~ s~ c~ 
define j = g d u s c d~ u~ s~ c~
generate p p > xd xd~ j QCD=99 [noborn=QCD]
output -f
"""
process_dir = new_process(process)

#----------------------------------------------------------------------------
# Beam energy
#----------------------------------------------------------------------------
beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
  beamEnergy = runArgs.ecmEnergy / 2.
else: 
  raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------
# Number of events to generate
#---------------------------------------------------------------------------
nevents=evgenConfig.nEventsPerJob

if runArgs.maxEvents>0:
    nevents = runArgs.maxEvents * multiplier
else:
    nevents *= multiplier
nevents = int(nevents)

skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents

#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------
extras = {
	'maxjetflavor'  : 4,
	'asrwgtflavor'  : 4,
	'lhe_version'	: '3.0',
	'cut_decays'	: 'F',
	'nevents'     	: nevents,
	'ickkw'      	: '0', # no merging
  }
pythiaprocess = 'pp>{Xd,1000022}'
nJetMax = 1


# Build run_card
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------
# MG5 parameter Card
#---------------------------------------------------------------------------

# Build param_card.dat

params={}
## blocks might be modified
dict_blocks={
"mass": ["MXd", "mh2", "mh3", "mhc", "mh4"],
"DMINPUTS" : ["gPXd", ],
"FRBlock": ["tanbeta", "sinbma", ],
"Higgs": ["lam3", "laP1", "laP2", "sinp"],
}

for bl in dict_blocks.keys():
  for pa in dict_blocks[bl]:
    if pa in THDMparams.keys():
      if bl not in params: params[bl]={}
      params[bl][pa]=THDMparams[pa]

## auto calculation of decay width
THDMparams_decay={
"25": "Auto",
"35": "Auto",
"36": "Auto",
"37": "Auto",
"55": "Auto",
}

params["decay"]=THDMparams_decay

print("Updating parameters:")
print(params)

modify_param_card(process_dir=process_dir,params=params)

# Build reweight_card.dat
# if reweight:
# 	# Create reweighting card
# 	reweight_card_loc=process_dir+'/Cards/reweight_card.dat'
# 	rwcard = open(reweight_card_loc,'w')

# 	for rw_name in reweights:
# 		rwcard.write("launch --rwgt_name=%s\n" % rw_name)
# 		for param in rw_name.split('-'):
# 			param_name, value = param.split('_')
# 			if param_name == "SINP":
# 				rwcard.write("set Higgs 5  %s\n" % value)
# 			elif param_name == "TANB":
# 				rwcard.write("set FRBlock 2  %s\n" % value)
# 			rwcard.write("set decay 25 Auto\nset decay 35 Auto\nset decay 36 Auto\nset decay 37 Auto\nset decay 55 Auto\n") 
# 	rwcard.close()

# print_cards()
    
#---------------------------------------------------------------------------
# Generate the events    
#---------------------------------------------------------------------------
generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)                                                                         

#---------------------------------------------------------------------------                                                                                                                                                                       
# Metadata                                                                                                                                                          
#---------------------------------------------------------------------------
evgenConfig.process = "p p > xd xd~ + X [noborn=QCD]"

evgenConfig.keywords = ["exotic","BSMHiggs","spin0","WIMP", "simplifiedModel"]
evgenConfig.contact = ["Martin Habedank <martin.habedank@cern.ch>"]
evgenConfig.description = "inclusive Pseudoscalar Mediator simplified Model"

#---------------------------------------------------------------------------
# Filters
#---------------------------------------------------------------------------
include('GeneratorFilters/MissingEtFilter.py')
metCut = 150
filtSeq.MissingEtFilter.METCut = metCut * 1000
evgenLog.info('MET %s is applied' % metCut)

#---------------------------------------------------------------------------
# Shower
#---------------------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# Teach pythia about the dark matter particle
genSeq.Pythia8.Commands += ["SLHA:allowUserOverride = on",
                            "1000022:all = xd xd~ 2 0 0 %d 0.0 0.0 0.0 0.0" % (int(THDMparams['MXd'])),
                            "1000022:isVisible = false",
                            "1000022:mayDecay = off",
                       ]
