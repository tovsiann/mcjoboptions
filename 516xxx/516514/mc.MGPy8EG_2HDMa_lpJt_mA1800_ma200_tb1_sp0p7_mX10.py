# Reweighting in tanb and sinp
# reweight = True # Only save weights for b-initiated samples - for ggF reweighting is broken
# reweights=[
# 'SINP_0.1-TANB_1.0',
# 'SINP_0.2-TANB_1.0',
# 'SINP_0.3-TANB_1.0',
# 'SINP_0.4-TANB_1.0',
# 'SINP_0.5-TANB_1.0',
# 'SINP_0.6-TANB_1.0',
# 'SINP_0.7-TANB_1.0',
# 'SINP_0.8-TANB_1.0',
# 'SINP_0.9-TANB_1.0',
# ]

# Define that we want H->bb decays
#decayChannel="monoHbb"

#---------------------------------------------------------------------------
# Generation settings
#---------------------------------------------------------------------------

# Number of events to generate
evgenConfig.nEventsPerJob=500
multiplier=30


#---------------------------------------------------------------------------
# Load main control file
#---------------------------------------------------------------------------    
include("MadGraphControl_Py8EG_2HDMa_lpJt.py")
