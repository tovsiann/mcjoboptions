########################################################
include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short

def MassToFloat(s):
    if "p" in s:
        return float(s.replace("p", "."))
    return float(s)

def GetDecayType(s):
    decaytype=[]
    while len(s) >= 5:
        if s[0:5] in ['C1RPV','N1RPV','N2RPV','C1RPC']:
            decaytype.append(s[0:5])
            s=s[5:]
    return decaytype

######################################
# split up the JO file input name to interpret it
######################################
jobConfig = get_physics_short()
evgenLog.info(" >>> Physics short name: " + str(jobConfig))

additional_options = jobConfig[5:]
evgenLog.info(" >>> ************************* Additional options: %s " % additional_options )
#----------------------------------------------------------------------
#  0;1     1;2      2;3                       3;4   4;5    5;6
#  MGPy8EG_C1/N2/N1_C1RPV/N2RPV/N1RPV|133/233_600p0_400p0[_0p001ns]
#
#  DirectRPV : C1,N1,N2 prompt decay
#  N1RPV : Only N1 do non-prompt decay
#----------------------------------------------------------------------

keepOutput = True # For debugging

splits=jobConfig.split('_')
gentype = str(splits[1])
decaytype = GetDecayType( str(splits[2][:-3]) )
lambdatype = str(splits[2][-3:])
if len(decaytype) <= 0 : #['C1RPV','N1RPV','N2RPV']:
    raise RuntimeError("decaytype must be in N1RPV(N1 RPV decays), C1RPV(N1&C1 RPV decay), N2RPV(N1&C1&N2 RPV decay), and C1RPC(C1C1 pair production), but not %s" %( str(splits[2][:-3]) ) )
if lambdatype not in ['133','233']:
    raise RuntimeError("LLEtype must be either 133 or 233, but not %s" %(lambdatype) )

evgenLog.info(" >>> production: %s, decay: %s, LLE: %s " % (gentype , str(splits[2][:-3]) , lambdatype))



######################################
# exact mass spectrum and mixing matrices depend on scenario
######################################
# https://twiki.cern.ch/twiki/bin/view/LHCPhysics/SUSYCrossSections13TeVhinosplit
masses['1000022'] = MassToFloat(splits[4].split('.')[0])
masses['1000023'] = -1.0 * MassToFloat(splits[3]) # N2, make LSP higgsino like 
masses['1000024'] = 0.5 * (MassToFloat(splits[3]) + MassToFloat(splits[4].split('.')[0]) ) # Chargino is between N1 and N2

# Off-diagonal chargino mixing matrix V , (C1+,C2+) = V * (iW+,H+_u)
param_blocks['VMIX']={}
param_blocks['VMIX']['1 1']='0.00E+00'
param_blocks['VMIX']['1 2']='1.00E+00'
param_blocks['VMIX']['2 1']='1.00E+00'
param_blocks['VMIX']['2 2']='0.00E+00'
# Off-diagonal chargino mixing matrix U , (C1-,C2-) = U * (iW-,H-_d)
param_blocks['UMIX']={}
param_blocks['UMIX']['1 1']='0.00E+00'
param_blocks['UMIX']['1 2']='1.00E+00'
param_blocks['UMIX']['2 1']='1.00E+00'
param_blocks['UMIX']['2 2']='0.00E+00'
# Neutralino mixing matrix chi_i0 = N_ij (B,W,H_d,H_u)_j
param_blocks['NMIX']={}
param_blocks['NMIX']['1  1']=' 0.00E+00'   # N_11 bino
param_blocks['NMIX']['1  2']=' 0.00E+00'   # N_12
param_blocks['NMIX']['1  3']=' 7.07E-01'   # N_13
param_blocks['NMIX']['1  4']='-7.07E-01'   # N_14
param_blocks['NMIX']['2  1']=' 0.00E+00'   # N_21
param_blocks['NMIX']['2  2']=' 0.00E+00'   # N_22
param_blocks['NMIX']['2  3']='-7.07E-01'   # N_23 higgsino
param_blocks['NMIX']['2  4']='-7.07E-01'   # N_24 higgsino
param_blocks['NMIX']['3  1']=' 1.00E+00'   # N_31
param_blocks['NMIX']['3  2']=' 0.00E+00'   # N_32
param_blocks['NMIX']['3  3']=' 0.00E+00'   # N_33 higgsino
param_blocks['NMIX']['3  4']=' 0.00E+00'   # N_34 higgsino
param_blocks['NMIX']['4  1']=' 0.00E+00'   # N_41
param_blocks['NMIX']['4  2']='-1.00E+00'   # N_42 wino
param_blocks['NMIX']['4  3']=' 0.00E+00'   # N_43
param_blocks['NMIX']['4  4']=' 0.00E+00'   # N_44



######################################
# LLP and Decay Width
######################################
C1_RPV_decay = False
C1_Mix_decay = False
N2_RPV_decay = False
longlived_LSP = False

if "N2RPV" in decaytype :
    decaytype.remove("N2RPV")
    evgenLog.info(" >>> Set N2 prompt RPV decay" )
    N2_RPV_decay = True
if "C1RPV" in decaytype :
    decaytype.remove("C1RPV")
    if "C1RPC" in decaytype :
        decaytype.remove("C1RPV")
        evgenLog.info(" >>> Set C1 half prompt RPV decay, half RPC decay" )
        C1_Mix_decay = True
    else:
        evgenLog.info(" >>> Set C1 prompt RPV decay" )
        C1_RPV_decay = True
elif decaytype == "N1RPV":
    evgenLog.info(" >>> Only N1 RPV decay " )

if len(splits) == 6 :
    neutralinoLifetime = MassToFloat(splits[5].replace("ns","").split('.')[0])
    evgenLog.info(" >>> Using Lifetime of %f for LSP RPV decay" % neutralinoLifetime)
    longlived_LSP = True
else :
    longlived_LSP = False
    evgenLog.info(" >>> N1 do prompt RPV decay" )

# calculate lifetime
if longlived_LSP :
    hbar = 6.582119514e-16
    decayWidth = hbar/float(neutralinoLifetime)
    decayWidthStr = '%e' % decayWidth
    decayStringHeader = 'DECAY   1000022  '
    header = decayStringHeader + decayWidthStr 
    evgenLog.info(' >>> lifetime of 1000022 is set to %s ns'% neutralinoLifetime)



######################################
# Add process
######################################
process='''
import model RPVMSSM_UFO
define p = g u c d s u~ c~ d~ s~
define f = e+ mu+ ta+ e- mu- ta- ve vm vt ve~ vm~ vt~ u u~ d d~ c c~ s s~ b b~ g
define j = g u c d s u~ c~ d~ s~
define pb = g u c d s b u~ c~ d~ s~ b~
define jb = g u c d s b u~ c~ d~ s~ b~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define fu = u c e+ mu+ ta+
define fu~ = u~ c~ e- mu- ta-
define fd = d s ve~ vm~ vt~
define fd~ = d~ s~ ve vm vt
define susystrong = go ul ur dl dr cl cr sl sr t1 t2 b1 b2 ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
define susyweak = el- el+ er- er+ mul- mul+ mur- mur+ ta1- ta1+ ta2- ta2+ n1 n2 n3 n4 x1- x1+ x2- x2+ sve sve~ svm svm~ svt svt~
define susylq = ul ur dl dr cl cr sl sr
define susylq~ = ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~
define susysq = ul ur dl dr cl cr sl sr t1 t2 b1 b2
define susysq~ = ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
define susysl = el- el+ er- er+ mul- mul+ mur- mur+ ta1- ta1+ ta2- ta2+
define susyv = sve svm svt
define susyv~ = sve~ svm~ svt~
''' 


if "C1C1" in gentype :
	process += '''
    generate p p > x1+ x1- $ susystrong RPV=0 @1
    add process p p > x1+ x1- j $ susystrong RPV=0 @2
    add process p p > x1+ x1- j j $ susystrong RPV=0 QED=2 QCD=99 @3
	'''
elif "C1N1" in gentype or "N1C1" in gentype :
	process += '''
    generate p p > x1+ n1 $ susystrong RPV=0 @1
    add process p p > x1- n1 $ susystrong RPV=0 @2
    add process p p > x1+ n1 j $ susystrong RPV=0 @3
    add process p p > x1- n1 j $ susystrong RPV=0 @4
    add process p p > x1+ n1 j j $ susystrong RPV=0 QED=2 QCD=99 @5
    add process p p > x1- n1 j j $ susystrong RPV=0 QED=2 QCD=99 @6
	'''
elif "C1N2" in gentype or "N2C1" in gentype :
	process += '''
	generate p p > x1+ n2 $ susystrong RPV=0 @1
	add process p p > x1- n2 $ susystrong RPV=0 @2
    add process p p > x1+ n2 j $ susystrong RPV=0 @3
    add process p p > x1- n2 j $ susystrong RPV=0 @4
    add process p p > x1+ n2 j j $ susystrong RPV=0 QED=2 QCD=99 @5
    add process p p > x1- n2 j j $ susystrong RPV=0 QED=2 QCD=99 @6
	'''
elif "N1N2" in gentype or "N2N1" in gentype :
	process += '''
	generate p p > n1 n2 $ susystrong RPV=0 @1
    add process p p > n1 n2 j $ susystrong RPV=0 @2
    add process p p > n1 n2 j j $ susystrong RPV=0 QED=2 QCD=99 @3
	'''
#elif "N2N2" in gentype :
#	process += '''
#	generate p p > n2 n2 RPV=0 $ susystrong @1
#    add process p p > n2 n2 j RPV=0 $ susystrong @2
#    add process p p > n2 n2 j j RPV=0 $ susystrong @3
#	'''

#-------------------------------------------------------


decays['1000023'] = """DECAY   1000023  2.07770048E-02   # neutralino2 decays
#          BR         NDA      ID1       ID2
     3.00000000E-01    3     1000022         -1          1   # BR(Chi_2 -> Chi_1 Fd_1^* Fd_1 )
     3.00000000E-01    3     1000022         -2          2   # BR(Chi_2 -> Chi_1 Fu_1^* Fu_1 )
     3.00000000E-01    3     1000022         -3          3   # BR(Chi_2 -> Chi_1 Fd_2^* Fd_2 )
     0.50000000E-01    3     1000022        -11         11   # BR(Chi_2 -> Chi_1 Fe_1^* Fe_1 )
     0.50000000E-01    3     1000022        -13         13   # BR(Chi_2 -> Chi_1 Fe_2^* Fe_2 )
"""
#
decays['1000024'] = """DECAY   1000024     1.70414503E-02    # chargino1+ decays
#          BR         NDA      ID1           ID2        ID3
     6.00000000E-01    3     1000022          2         -1   # BR(Cha_1^* -> Chi_1 Fu_1 Fd_1^* )
     2.00000000E-01    3     1000022         12        -11   # BR(Cha_1^* -> Chi_1 Fv_1 Fe_1^* )
     2.00000000E-01    3     1000022         14        -13   # BR(Cha_1^* -> Chi_1 Fv_2 Fe_2^* )
"""
if C1_RPV_decay and not C1_Mix_decay :
    evgenLog.info(' >>> Adding C1 RPV decay only ' )
    #
    if lambdatype == "233":
        decays['1000024'] = """DECAY   1000024     1.70414503E-02    # chargino1+ decays
        #          BR         NDA      ID1           ID2        ID3
             5.00000000E-01    3         -13         15       -15   # BR(Cha_1^* -> Fe_2^* Fe_3 Fe_3^* )
             2.50000000E-01    3          16        -14       -15   # BR(Cha_1^* -> Fv_3 Fv_2^* Fe_3^* )
             2.50000000E-01    3          16        -16       -13   # BR(Cha_1^* -> Fv_3 Fv_3^* Fe_2^* )
        """
    elif lambdatype == "133":
        decays['1000024'] = """DECAY   1000024     1.70414503E-02    # chargino1+ decays
        #          BR         NDA      ID1           ID2        ID3
             5.00000000E-01    3         -11         15       -15   # BR(Cha_1^* -> Fe_1^* Fe_3 Fe_3^* )
             2.50000000E-01    3          16        -12       -15   # BR(Cha_1^* -> Fv_3 Fv_1^* Fe_3^* )
             2.50000000E-01    3          16        -16       -11   # BR(Cha_1^* -> Fv_3 Fv_3^* Fe_1^* )
        """
    #
if N2_RPV_decay and not C1_Mix_decay :
    evgenLog.info(' >>> Adding N2 RPV decay ' )
    #
    if lambdatype == "233":
        decays['1000023'] = """DECAY   1000023  2.07770048E-02   # neutralino2 decays
        #          BR         NDA      ID1       ID2
             1.65000000E-01    3         13        -15         16   # BR(Chi_1 -> Fe_2 Fe_3^* Fv_3 )
             1.65000000E-01    3        -13         15        -16   # BR(Chi_1 -> Fe_2^* Fe_3 Fv_3^* )
             3.35000000E-01    3         15        -15         14   # BR(Chi_1 -> Fe_3 Fe_3^* Fv_2 )
             3.35000000E-01    3        -15         15        -14   # BR(Chi_1 -> Fe_3^* Fe_3 Fv_2^* )
        """
    elif lambdatype == "133":
        decays['1000023'] = """DECAY   1000023  2.07770048E-02   # neutralino2 decays
        #          BR         NDA      ID1       ID2
             1.65000000E-01    3         11        -15         16   # BR(Chi_1 -> Fe_1 Fe_3^* Fv_3 )
             1.65000000E-01    3        -11         15        -16   # BR(Chi_1 -> Fe_1^* Fe_3 Fv_3^* )
             3.35000000E-01    3         15        -15         12   # BR(Chi_1 -> Fe_3 Fe_3^* Fv_1 )
             3.35000000E-01    3        -15         15        -12   # BR(Chi_1 -> Fe_3^* Fe_3 Fv_1^* )
        """
    #
if C1_Mix_decay :
    evgenLog.info(' >>> Adding C1 half prompt RPV decay + half RPC decay ' )
    if lambdatype == "233":
        decays['1000024'] = """DECAY   1000024     1.70414503E-02    # chargino1+ decays
        #          BR         NDA      ID1           ID2        ID3
             2.50000000E-01    3         -13         15       -15   # BR(Cha_1^* -> Fe_2^* Fe_3 Fe_3^* )
             1.25000000E-01    3          16        -14       -15   # BR(Cha_1^* -> Fv_3 Fv_2^* Fe_3^* )
             1.25000000E-01    3          16        -16       -13   # BR(Cha_1^* -> Fv_3 Fv_3^* Fe_2^* )
             3.00000000E-01    3     1000022          2         -1   # BR(Cha_1^* -> Chi_1 Fu_1 Fd_1^* )
             1.00000000E-01    3     1000022         12        -11   # BR(Cha_1^* -> Chi_1 Fv_1 Fe_1^* )
             1.00000000E-01    3     1000022         14        -13   # BR(Cha_1^* -> Chi_1 Fv_2 Fe_2^* )
        """
    elif lambdatype == "133":
        decays['1000024'] = """DECAY   1000024     1.70414503E-02    # chargino1+ decays
        #          BR         NDA      ID1           ID2        ID3
             2.50000000E-01    3         -11         15       -15   # BR(Cha_1^* -> Fe_1^* Fe_3 Fe_3^* )
             1.25000000E-01    3          16        -12       -15   # BR(Cha_1^* -> Fv_3 Fv_1^* Fe_3^* )
             1.25000000E-01    3          16        -16       -11   # BR(Cha_1^* -> Fv_3 Fv_3^* Fe_1^* )
             3.00000000E-01    3     1000022          2         -1   # BR(Cha_1^* -> Chi_1 Fu_1 Fd_1^* )
             1.00000000E-01    3     1000022         12        -11   # BR(Cha_1^* -> Chi_1 Fv_1 Fe_1^* )
             1.00000000E-01    3     1000022         14        -13   # BR(Cha_1^* -> Chi_1 Fv_2 Fe_2^* )
        """


if longlived_LSP :
    evgenLog.info(' >>> Adding longlived LSP decay ' )
    if lambdatype == "233":
        branchingRatios = """
        #    BR                NDA      ID1      ID2       ID3
             1.65000000E-01    3         13        -15         16   # BR(Chi_1 -> Fe_2 Fe_3^* Fv_3 )
             1.65000000E-01    3        -13         15        -16   # BR(Chi_1 -> Fe_2^* Fe_3 Fv_3^* )
             3.35000000E-01    3         15        -15         14   # BR(Chi_1 -> Fe_3 Fe_3^* Fv_2 )
             3.35000000E-01    3        -15         15        -14   # BR(Chi_1 -> Fe_3^* Fe_3 Fv_2^* )
        """
    elif lambdatype == "133":
        branchingRatios = """
        #    BR                NDA      ID1      ID2       ID3
             1.65000000E-01    3         11        -15         16   # BR(Chi_1 -> Fe_1 Fe_3^* Fv_3 )
             1.65000000E-01    3        -11         15        -16   # BR(Chi_1 -> Fe_1^* Fe_3 Fv_3^* )
             3.35000000E-01    3         15        -15         12   # BR(Chi_1 -> Fe_3 Fe_3^* Fv_1 )
             3.35000000E-01    3        -15         15        -12   # BR(Chi_1 -> Fe_3^* Fe_3 Fv_1^* )
        """
    decays['1000022'] = header + branchingRatios
else:
    evgenLog.info(' >>> Adding prompt LSP decay ' )
    if lambdatype == "233":
        decays['1000022'] = """DECAY   1000022     5.00000000E-02   # Chi_1
        #    BR                NDA      ID1      ID2       ID3
             1.65000000E-01    3         13        -15         16   # BR(Chi_1 -> Fe_2 Fe_3^* Fv_3 )
             1.65000000E-01    3        -13         15        -16   # BR(Chi_1 -> Fe_2^* Fe_3 Fv_3^* )
             3.35000000E-01    3         15        -15         14   # BR(Chi_1 -> Fe_3 Fe_3^* Fv_2 )
             3.35000000E-01    3        -15         15        -14   # BR(Chi_1 -> Fe_3^* Fe_3 Fv_2^* )
        """
    elif lambdatype == "133":
        decays['1000022'] = """DECAY   1000022     5.00000000E-02   # Chi_1
        #    BR                NDA      ID1      ID2       ID3
             1.65000000E-01    3         11        -15         16   # BR(Chi_1 -> Fe_1 Fe_3^* Fv_3 )
             1.65000000E-01    3        -11         15        -16   # BR(Chi_1 -> Fe_1^* Fe_3 Fv_3^* )
             3.35000000E-01    3         15        -15         12   # BR(Chi_1 -> Fe_3 Fe_3^* Fv_1 )
             3.35000000E-01    3        -15         15        -12   # BR(Chi_1 -> Fe_3^* Fe_3 Fv_1^* )
        """


if longlived_LSP:
    testSeq.TestHepMC.MaxVtxDisp = 1e8 # in mm
    testSeq.TestHepMC.MaxTransVtxDisp = 1e8



######################################
# MadGraph5 Options
######################################
evt_multiplier = 10 #20 
njets = 2
usePMGSettings = False ###WHY???????????????????????
#run_settings['auto_ptj_mjj']='F'
#run_settings['bwcutoff']=100000 # Set to something big like 10000 to allow very low-mass W* and Z*
#run_settings['event_norm']='sum' # this gets overridden in MGC for 21.6.12+, leaving in here for older releases.
#run_settings['lhe_version']='3.0'
#run_settings['auto_ptj_mjj']='F'
#run_settings['bwcutoff']=10000 # Set to something big like 10000 to allow very low-mass W* and Z*
#run_settings['event_norm']='sum' # this gets overridden in MGC for 21.6.12+, leaving in here for older releases.



######################################
# information about this generation
######################################
evgenConfig.contact  = [ "caiy@cern.ch" ]
evgenConfig.keywords += ['SUSY', 'simplifiedmodel', 'higgsino', 'longlived']

if "C1C1" in gentype:
	evgenConfig.description = 'higgsino like ~chi1+/~chi1- production, m_C1 = %s GeV, m_N1 = %s GeV' % (masses['1000024'],masses['1000022'])
elif "C1N1" in gentype or "N1C1" in gentype:
	evgenConfig.description = 'higgsino like ~chi1+/- ~chi10 production, m_C1 = %s GeV, m_N1 = %s GeV'%(masses['1000024'],masses['1000022'])
elif "C1N2" in gentype or "N2C1" in gentype:
	evgenConfig.description = 'higgsino like ~chi1+/- ~chi20 production, m_C1 = %s GeV, m_N2 = %s GeV, m_N1 = %s GeV'%(masses['1000024'],masses['1000023'],masses['1000022'])
elif "N1N2" in gentype or "N2N1" in gentype:
	evgenConfig.description = 'higgsino like ~chi10 ~chi20 production, m_N2 = %s GeV, m_N1 = %s GeV'%(masses['1000023'],masses['1000022'])



######################################
# Standard post-include
######################################
include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )


######################################
# Pythia options
######################################
if njets>0:
	genSeq.Pythia8.Commands += ["Merging:Process = guess"] 
	genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]
	#run_settings["pdgs_for_merging_cut"]= "1, 2, 3, 4,21"
	#run_settings["asrwgtflavor"]=4
	#run_settings["maxjetflavor"]=4



#--------------------------------------------------------------
#--------------------------------------------------------------
# RPC BR
#--------------------------------------------------------------
##step 1
#decays['1000023'] = """DECAY   1000023  2.07770048E-02   # neutralino2 decays
##          BR         NDA      ID1       ID2
#     0.00000000E+00     2     1000022        23   # BR(~chi_20 -> ~chi_10   Z )
#     0.00000000E+00     2     1000024       -24   # BR(~chi_20 -> ~chi_1+ W- )
#     0.00000000E+00     2    -1000024        24   # BR(~chi_20 -> ~chi_1- W+ )
#     0.00000000E+00     2     1000011       -11   # BR(~chi_20 -> ~e_L-     e+)
#     0.00000000E+00     2    -1000011        11   # BR(~chi_20 -> ~e_L+     e-)
#     0.00000000E+00     2     2000011       -11   # BR(~chi_20 -> ~e_R-     e+)
#     0.00000000E+00     2    -2000011        11   # BR(~chi_20 -> ~e_R+     e-)
#     0.00000000E+00     2     1000013       -13   # BR(~chi_20 -> ~mu_L-    mu+)
#     0.00000000E+00     2    -1000013        13   # BR(~chi_20 -> ~mu_L+    mu-)
#     0.00000000E+00     2     2000013       -13   # BR(~chi_20 -> ~mu_R-    mu+)
#     0.00000000E+00     2    -2000013        13   # BR(~chi_20 -> ~mu_R+    mu-)
#     0.25000000E+00     2     1000015       -15   # BR(~chi_20 -> ~tau_1-   tau+)
#     0.25000000E+00     2    -1000015        15   # BR(~chi_20 -> ~tau_1+   tau-)
#     0.00000000E+00     2     2000015       -15   # BR(~chi_20 -> ~tau_2-   tau+)
#     0.00000000E+00     2    -2000015        15   # BR(~chi_20 -> ~tau_2+   tau-)
#     0.00000000E+00     2     1000012       -12   # BR(~chi_20 -> nu_eL  ~nu_e)
#     0.00000000E+00     2    -1000012        12   # BR(~chi_20 -> ~nu_eL  nu_e)
#     0.00000000E+00     2     1000014       -14   # BR(~chi_20 -> nu_muL  ~nu_mu)
#     0.00000000E+00     2    -1000014        14   # BR(~chi_20 -> ~nu_muL  nu_mu)
#     0.25000000E+00     2     1000016       -16   # BR(~chi_20 -> nu_tauL  ~nu_tau)
#     0.25000000E+00     2    -1000016        16   # BR(~chi_20 -> ~nu_tauL  nu_tau)
#
#"""
##
#decays['1000024'] = """DECAY   1000024     1.70414503E-02    # chargino1+ decays
##          BR         NDA      ID1           ID2        ID3
#     0.00000000E+00    3     1000023         -11        12   # BR(~chi_1+ -> ~chi_20  e+  nu_e)
#     0.00000000E+00    3     1000023         -13        14   # BR(~chi_1+ -> ~chi_20  mu+  nu_mu)
#     0.00000000E+00    3     1000023         -15        16   # BR(~chi_1+ -> ~chi_20  tau+  nu_tau)
#     0.00000000E+00    3     1000023           2        -1   # BR(~chi_1+ -> ~chi_20  u dbar)
#     0.00000000E+00    3     1000023           4        -3   # BR(~chi_1+ -> ~chi_20  c sbar)
#     0.00000000E+00    3     1000022         -11        12   # BR(~chi_1+ -> ~chi_10  e+  nu_e)
#     0.00000000E+00    3     1000022         -13        14   # BR(~chi_1+ -> ~chi_10  mu+  nu_mu)
#     0.00000000E+00    3     1000022         -15        16   # BR(~chi_1+ -> ~chi_10  tau+  nu_tau)
#     0.00000000E+00    3     1000022           2        -1   # BR(~chi_1+ -> ~chi_10  u  dbar)
#     0.00000000E+00    3     1000022           4        -3   # BR(~chi_1+ -> ~chi_10  c  sbar)
#     0.00000000E+00    2     1000022          24             # BR(~chi_1+ -> ~chi_10  W+)
#     0.00000000E+00    2     1000012         -11    # BR(~chi_1+ -> ~nu_eL  e+  )
#     0.00000000E+00    2     1000014         -13    # BR(~chi_1+ -> ~nu_muL  mu+ )
#     0.50000000E+00    2     1000016         -15    # BR(~chi_1+ -> ~nu_tau1 tau+)
#     0.00000000E+00    2    -1000011          12    # BR(~chi_1+ -> ~e_L+    nu_e)
#     0.00000000E+00    2    -2000011          12    # BR(~chi_1+ -> ~e_R+    nu_e)
#     0.00000000E+00    2    -1000013          14    # BR(~chi_1+ -> ~mu_L+   nu_mu)
#     0.00000000E+00    2    -2000013          14    # BR(~chi_1+ -> ~mu_R+   nu_mu)
#     0.50000000E+00    2    -1000015          16    # BR(~chi_1+ -> ~tau_1+  nu_tau)
#     0.00000000E+00    2    -2000015          16    # BR(~chi_1+ -> ~tau_2+  nu_tau)
#"""
##step 2
#decays['1000015'] = """DECAY   1000015  1.48327268E-01    # stau_1 decays
# #          BR         NDA      ID1       ID2
#      1.00000000E+00    2     1000022        15   # BR(~tau_1 -> ~chi_10  tau-)
#      0.00000000E+00    2     1000023        15   # BR(~tau_1 -> ~chi_20  tau-)
#      0.00000000E+00    2     1000025        15   # BR(~tau_1 -> ~chi_30  tau-)
#      0.00000000E+00    2     1000035        15   # BR(~tau_1 -> ~chi_40  tau-)
#      0.00000000E+00    2    -1000024        16   # BR(~tau_1 -> ~chi_1-  nu_tau)
#      0.00000000E+00    2    -1000037        16   # BR(~tau_1 -> ~chi_2-  nu_tau)
#      0.00000000E+00    2     1000016       -37   # BR(~tau_1 -> ~nu_tauL H-)
#      0.00000000E+00    2     1000016       -24   # BR(~tau_1 -> ~nu_tauL W-)
# """
##
#decays['1000016'] = """DECAY   1000016  1.47518977E-01    # snu_tauL decays
# #          BR         NDA      ID1       ID2
#      1.00000000E+00    2     1000022        16   # BR(~nu_tauL -> ~chi_10 nu_tau)
#      0.00000000E+00    2     1000023        16   # BR(~nu_tauL -> ~chi_20 nu_tau)
#      0.00000000E+00    2     1000025        16   # BR(~nu_tauL -> ~chi_30 nu_tau)
#      0.00000000E+00    2     1000035        16   # BR(~nu_tauL -> ~chi_40 nu_tau)
#      0.00000000E+00    2     1000024        15   # BR(~nu_tauL -> ~chi_1+ tau-)
#      0.00000000E+00    2     1000037        15   # BR(~nu_tauL -> ~chi_2+ tau-)
#      0.00000000E+00    2    -1000015       -37   # BR(~nu_tauL -> ~tau_1+ H-)
#      0.00000000E+00    2    -2000015       -37   # BR(~nu_tauL -> ~tau_2+ H-)
#      0.00000000E+00    2    -1000015       -24   # BR(~nu_tauL -> ~tau_1+ W-)
#      0.00000000E+00    2    -2000015       -24   # BR(~nu_tauL -> ~tau_2+ W-)
#"""
