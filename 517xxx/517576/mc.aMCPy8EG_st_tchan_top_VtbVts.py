from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphParamHelpers import set_top_params

import fileinput


# Make some excess of events - make sure we protect against maxEvents=-1
nevents=1.1*runArgs.maxEvents if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':260400,
    'scale_variations':[0.5,1.,2.],
}

gridpack_mode=False

if not is_gen_from_gridpack():

    process="""
    import model loop_sm-ckm
    define p = g u c d s u~ c~ d~ s~
    define j = g u c d s u~ c~ d~ s~
    generate p p > t b~ j $$ w+ w- [QCD]
    output -f"""

    process_dir = new_process(process)

else:
    process_dir = MADGRAPH_GRIDPACK_LOCATION


#Fetch default NLO run_card.dat and set parameters
settings = {'parton_shower' : 'PYTHIA8',
            'maxjetflavor'  : 4,
            'dynamical_scale_choice' : '10', #user-defined scale -> Dominic's definition of mt+1/2*(pt^2+ptx^2)
            'jetalgo'   : '-1',  # use anti-kT jet algorithm
            'jetradius' : '0.4', # set jet cone size of 0.4
            'ptj'	: '10', # minimum jet pT
            'req_acc'   : '0.001',
            'bwcutoff'  : '50',
            'nevents':int(nevents)
}


modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
# Modify the param card
#set_top_params(process_dir,mTop=172.5,FourFS=True)
WT = 1.320000e+00
WW = 2.085000e+00
decays = {'6':"""DECAY  6  """+str(WT)+""" #WT
        #  BR             NDA  ID1    ID2   ...
        0.000000e+00   2   1  24 # 1.32
        1.000000e+00   2   3  24 # 1.32
        0.000000e+00   2   5  24 # 1.32
        #""",
        '24':"""DECAY  24  """+str(WW)+""" #WW
        #  BR             NDA  ID1    ID2   ...
        3.377000e-01   2    -1  2
        3.377000e-01   2    -3  4
        1.082000e-01   2   -11 12
        1.082000e-01   2   -13 14
        1.082000e-01   2   -15 16
        #"""}

modify_param_card(process_dir=process_dir,params={'DECAY':decays})


# Cook the setscales file for the user defined dynamical scale
fileN = process_dir+'/SubProcesses/setscales.f'
mark  = '      elseif(dynamical_scale_choice.eq.10) then'
rmLines = ['ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc',
           'cc      USER-DEFINED SCALE: ENTER YOUR CODE HERE                                 cc',
           'cc      to use this code you must set                                            cc',
           'cc                 dynamical_scale_choice = 0                                    cc',
           'cc      in the run_card (run_card.dat)                                           cc',
           'write(*,*) "User-defined scale not set"',
           'stop 1',
           'temp_scale_id=\'User-defined dynamical scale\' ! use a meaningful string',
           'tmp = 0',
           'cc      USER-DEFINED SCALE: END OF USER CODE                                     cc'
           ]

for line in fileinput.input(fileN, inplace=1):
    toKeep = True
    for rmLine in rmLines:
        if line.find(rmLine) >= 0:
            toKeep = False
            break
    if toKeep:
        print(line)
    if line.startswith(mark):
        print("""
c 4 times the bottom transverse mass
         do i=3,4
           xm2=dot(pp(0,i),pp(0,i))
           if (xm2 < 30) then
             if(xm2.le.0.d0)xm2=0.d0
             tmp = 4d0 * sqrt(pt(pp(0,i))**2+xm2)
             temp_scale_id='4*mT(b)'
           endif
         enddo
	              """)



### Decay with MadSpin
madspin_card_loc=process_dir + '/Cards/madspin_card.dat'
mscard = open(madspin_card_loc,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
 set Nevents_for_max_weight 250 # number of events for the estimate of the max. weight
 set max_weight_ps_point 1000  # number of PS to estimate the maximum for each event
 set BW_cut 50
 set seed %i
 define j = g u c d s u~ c~ d~ s~
 define lv = e+ mu+ ta+ ve vm vt e- mu- ta- ve~ vm~ vt~
 decay t > w+ s, w+ > lv lv
 decay t~ > w- s~, w- > lv lv
 launch
"""%(runArgs.randomSeed))
mscard.close()


generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)


### Shower

# Go to serial mode for Pythia8
if 'ATHENA_PROC_NUMBER' in os.environ:
    print('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): print('Did not see option!')
    else: opts.nprocs = 0
    print(opts)

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

### Metadata
evgenConfig.description = 'MG5_aMC@NLO+Pythia8+EvtGen singleTop t-channel (with b->t and t->s)'
evgenConfig.generators    = ['aMcAtNlo', 'Pythia8', 'EvtGen']
evgenConfig.keywords+=['top','singleTop','tChannel','CKM']
evgenConfig.contact  = [ "benedikt.gocke@cern.ch" ]
