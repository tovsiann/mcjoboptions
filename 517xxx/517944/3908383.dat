# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 15.05.2022,  19:01
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    1.66410921E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.72563690E+03  # scale for input parameters
    1    2.31390666E+02  # M_1
    2    6.79557983E+02  # M_2
    3    4.36082730E+03  # M_3
   11    4.78179546E+03  # A_t
   12   -1.92727841E+03  # A_b
   13   -1.23016691E+03  # A_tau
   23   -1.65300418E+03  # mu
   25    1.58721113E+01  # tan(beta)
   26    1.69423126E+03  # m_A, pole mass
   31    7.12121850E+02  # M_L11
   32    7.12121850E+02  # M_L22
   33    1.03938351E+03  # M_L33
   34    2.15141056E+02  # M_E11
   35    2.15141056E+02  # M_E22
   36    1.40156465E+03  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.72040813E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.83070473E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.24666523E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.72563690E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.72563690E+03  # (SUSY scale)
  1  1     8.40099634E-06   # Y_u(Q)^DRbar
  2  2     4.26770614E-03   # Y_c(Q)^DRbar
  3  3     1.01490784E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.72563690E+03  # (SUSY scale)
  1  1     2.67945053E-04   # Y_d(Q)^DRbar
  2  2     5.09095601E-03   # Y_s(Q)^DRbar
  3  3     2.65717531E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.72563690E+03  # (SUSY scale)
  1  1     4.67583711E-05   # Y_e(Q)^DRbar
  2  2     9.66814816E-03   # Y_mu(Q)^DRbar
  3  3     1.62602347E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.72563690E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     4.78179546E+03   # A_t(Q)^DRbar
Block Ad Q=  3.72563690E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.92727839E+03   # A_b(Q)^DRbar
Block Ae Q=  3.72563690E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.23016690E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.72563690E+03  # soft SUSY breaking masses at Q
   1    2.31390666E+02  # M_1
   2    6.79557983E+02  # M_2
   3    4.36082730E+03  # M_3
  21    7.35189481E+04  # M^2_(H,d)
  22   -2.55038312E+06  # M^2_(H,u)
  31    7.12121850E+02  # M_(L,11)
  32    7.12121850E+02  # M_(L,22)
  33    1.03938351E+03  # M_(L,33)
  34    2.15141056E+02  # M_(E,11)
  35    2.15141056E+02  # M_(E,22)
  36    1.40156465E+03  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.72040813E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.83070473E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.24666523E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.24951951E+02  # h0
        35     1.69428986E+03  # H0
        36     1.69423126E+03  # A0
        37     1.70003530E+03  # H+
   1000001     1.00952087E+04  # ~d_L
   2000001     1.00706406E+04  # ~d_R
   1000002     1.00948486E+04  # ~u_L
   2000002     1.00739337E+04  # ~u_R
   1000003     1.00952098E+04  # ~s_L
   2000003     1.00706415E+04  # ~s_R
   1000004     1.00948496E+04  # ~c_L
   2000004     1.00739345E+04  # ~c_R
   1000005     2.85817055E+03  # ~b_1
   2000005     3.39213640E+03  # ~b_2
   1000006     2.85590454E+03  # ~t_1
   2000006     4.86023608E+03  # ~t_2
   1000011     7.25335707E+02  # ~e_L-
   2000011     2.29958392E+02  # ~e_R-
   1000012     7.20657125E+02  # ~nu_eL
   1000013     7.25343702E+02  # ~mu_L-
   2000013     2.29902942E+02  # ~mu_R-
   1000014     7.20651746E+02  # ~nu_muL
   1000015     1.04601109E+03  # ~tau_1-
   2000015     1.40358612E+03  # ~tau_2-
   1000016     1.04369170E+03  # ~nu_tauL
   1000021     4.81290567E+03  # ~g
   1000022     2.27974047E+02  # ~chi_10
   1000023     7.21441857E+02  # ~chi_20
   1000025     1.67090870E+03  # ~chi_30
   1000035     1.67255984E+03  # ~chi_40
   1000024     7.21558697E+02  # ~chi_1+
   1000037     1.67371086E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -6.04767498E-02   # alpha
Block Hmix Q=  3.72563690E+03  # Higgs mixing parameters
   1   -1.65300418E+03  # mu
   2    1.58721113E+01  # tan[beta](Q)
   3    2.43024430E+02  # v(Q)
   4    2.87041956E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.99093755E-01   # Re[R_st(1,1)]
   1  2     4.25637129E-02   # Re[R_st(1,2)]
   2  1    -4.25637129E-02   # Re[R_st(2,1)]
   2  2    -9.99093755E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99838263E-01   # Re[R_sb(1,1)]
   1  2     1.79846532E-02   # Re[R_sb(1,2)]
   2  1    -1.79846532E-02   # Re[R_sb(2,1)]
   2  2    -9.99838263E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -9.98711844E-01   # Re[R_sta(1,1)]
   1  2     5.07410323E-02   # Re[R_sta(1,2)]
   2  1    -5.07410323E-02   # Re[R_sta(2,1)]
   2  2    -9.98711844E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.99647465E-01   # Re[N(1,1)]
   1  2    -9.88422599E-06   # Re[N(1,2)]
   1  3     2.64787869E-02   # Re[N(1,3)]
   1  4     1.95456839E-03   # Re[N(1,4)]
   2  1    -1.51717939E-03   # Re[N(2,1)]
   2  2    -9.98206414E-01   # Re[N(2,2)]
   2  3    -5.61146833E-02   # Re[N(2,3)]
   2  4    -2.08037153E-02   # Re[N(2,4)]
   3  1     1.73292887E-02   # Re[N(3,1)]
   3  2    -2.50003850E-02   # Re[N(3,2)]
   3  3     7.06417858E-01   # Re[N(3,3)]
   3  4    -7.07141065E-01   # Re[N(3,4)]
   4  1    -2.00584275E-02   # Re[N(4,1)]
   4  2     5.43960912E-02   # Re[N(4,2)]
   4  3    -7.05070086E-01   # Re[N(4,3)]
   4  4    -7.06763680E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.96848409E-01   # Re[U(1,1)]
   1  2    -7.93300003E-02   # Re[U(1,2)]
   2  1     7.93300003E-02   # Re[U(2,1)]
   2  2    -9.96848409E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -9.99562909E-01   # Re[V(1,1)]
   1  2     2.95633302E-02   # Re[V(1,2)]
   2  1     2.95633302E-02   # Re[V(2,1)]
   2  2     9.99562909E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     3.40947578E-04   # ~e^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000011     7.40569856E-01   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.98582143E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     4.93318735E-04    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     9.24438737E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     3.21844199E-04   # ~mu^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
DECAY   1000013     7.43597992E-01   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.94532184E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     4.93168101E-04    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     9.24555619E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     4.05008704E-03    2     2000013        25   # BR(~mu^-_L -> ~mu^-_R h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     4.97805141E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     2.41598707E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     2.53720607E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     5.04680685E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000015     7.45897848E+00   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.93615044E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     4.42978544E-04    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     8.73637188E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.36517495E-02    2     1000016       -24   # BR(~tau^-_2 -> ~nu_tau W^-)
     2.56812308E-02    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     2.57353599E-02    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
DECAY   1000012     7.32598771E-01   # ~nu_e
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
DECAY   1000014     7.32590870E-01   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
DECAY   1000016     4.93710410E+00   # ~nu_tau
#    BR                NDA      ID1      ID2
     2.40635036E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.52400331E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     5.06964633E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
DECAY   2000001     4.78875953E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.17204575E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.88271658E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.09099859E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.30903887E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     7.19098267E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     2.31873616E-04    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.43505796E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     8.68411463E-04    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.81120606E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.78885840E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.17202241E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.88251445E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.09108356E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.30901289E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     7.19088583E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     2.35877075E-04    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.43503808E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     8.74080885E-04    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.81109893E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     6.00185929E+01   # ~b_1
#    BR                NDA      ID1      ID2
     6.66948694E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.83333731E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.44856855E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.45881367E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     3.62071119E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     4.18851841E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     7.79874598E+00   # ~b_2
#    BR                NDA      ID1      ID2
     2.40299323E-01    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     3.31948565E-04    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.75445367E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.75054699E-01    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     6.60708419E-04    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     3.51505676E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     2.62239087E-02    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.28463800E-02    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     1.76319891E-02    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     4.96070451E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.52716692E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.54698094E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.09081391E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.30953923E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     7.19872819E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.78234474E-04    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.44287538E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.20623884E-04    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.81083642E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     4.96077407E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.52710363E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.54684810E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.09089846E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.30950734E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     7.19862892E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.81071465E-04    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.44285604E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.28630957E-04    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.81072918E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     6.02140021E+01   # ~t_1
#    BR                NDA      ID1      ID2
     6.60522543E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.79693373E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.07560487E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.10370934E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     3.63918452E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     3.00533652E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     1.77365071E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     2.62112019E+02   # ~t_2
#    BR                NDA      ID1      ID2
     4.10409771E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     5.93886967E-04    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.47137677E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.46234381E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.19854528E-03    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     2.93957834E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.50709780E-01    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     9.84713790E-03    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     7.53768329E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     4.99567663E-03    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     1.23835993E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     5.04782657E-03    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     1.23227246E-03   # chi^+_1
#    BR                NDA      ID1      ID2
     2.76595262E-03    2    -2000013        14   # BR(chi^+_1 -> ~mu^+_R nu_mu)
     1.60606440E-02    2     1000012       -11   # BR(chi^+_1 -> ~nu_e e^+)
     1.61378918E-02    2     1000014       -13   # BR(chi^+_1 -> ~nu_mu mu^+)
     3.28914143E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     2.50682687E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
     2.94543994E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     2.94468906E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     4.45995730E-02    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     1.08216735E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     2.82605126E-03    2    -1000011        12   # BR(chi^+_2 -> ~e^+_L nu_e)
     1.37557978E-04    2    -2000013        14   # BR(chi^+_2 -> ~mu^+_R nu_mu)
     2.82602245E-03    2    -1000013        14   # BR(chi^+_2 -> ~mu^+_L nu_mu)
     1.13031863E-03    2    -1000015        16   # BR(chi^+_2 -> ~tau^+_1 nu_tau)
     3.66837491E-03    2    -2000015        16   # BR(chi^+_2 -> ~tau^+_2 nu_tau)
     3.94814552E-04    2     1000012       -11   # BR(chi^+_2 -> ~nu_e e^+)
     4.89701287E-04    2     1000014       -13   # BR(chi^+_2 -> ~nu_mu mu^+)
     1.53319536E-02    2     1000016       -15   # BR(chi^+_2 -> ~nu_tau tau^+)
     9.75925770E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.83137775E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.81275070E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.92226554E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     8.60130409E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     3.11017520E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     7.19446216E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.37604157E-03   # chi^0_2
#    BR                NDA      ID1      ID2
     2.44715962E-03    2     2000011       -11   # BR(chi^0_2 -> ~e^-_R e^+)
     2.44715962E-03    2    -2000011        11   # BR(chi^0_2 -> ~e^+_R e^-)
     3.68760319E-03    2     2000013       -13   # BR(chi^0_2 -> ~mu^-_R mu^+)
     3.68760319E-03    2    -2000013        13   # BR(chi^0_2 -> ~mu^+_R mu^-)
     5.42632286E-03    2     1000012       -12   # BR(chi^0_2 -> ~nu_e nu_bar_e)
     5.42632286E-03    2    -1000012        12   # BR(chi^0_2 -> ~nu^*_e nu_e)
     5.50092700E-03    2     1000014       -14   # BR(chi^0_2 -> ~nu_mu nu_bar_mu)
     5.50092700E-03    2    -1000014        14   # BR(chi^0_2 -> ~nu^*_mu nu_mu)
     1.46421320E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     1.57128630E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
     3.36769792E-04    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     2.95640890E-04    3     1000022         6        -6   # BR(chi^0_2 -> chi^0_1 t t_bar)
     3.10897178E-01    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.10826437E-01    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     1.98848132E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.99895709E-02    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     1.39825824E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     1.40160790E-04    2     2000013       -13   # BR(chi^0_3 -> ~mu^-_R mu^+)
     1.40160790E-04    2    -2000013        13   # BR(chi^0_3 -> ~mu^+_R mu^-)
     5.74186818E-03    2     1000015       -15   # BR(chi^0_3 -> ~tau^-_1 tau^+)
     5.74186818E-03    2    -1000015        15   # BR(chi^0_3 -> ~tau^+_1 tau^-)
     1.38869081E-03    2     2000015       -15   # BR(chi^0_3 -> ~tau^-_2 tau^+)
     1.38869081E-03    2    -2000015        15   # BR(chi^0_3 -> ~tau^+_2 tau^-)
     2.04500672E-04    2     1000012       -12   # BR(chi^0_3 -> ~nu_e nu_bar_e)
     2.04500672E-04    2    -1000012        12   # BR(chi^0_3 -> ~nu^*_e nu_e)
     2.04502068E-04    2     1000014       -14   # BR(chi^0_3 -> ~nu_mu nu_bar_mu)
     2.04502068E-04    2    -1000014        14   # BR(chi^0_3 -> ~nu^*_mu nu_mu)
     1.14789014E-04    2     1000016       -16   # BR(chi^0_3 -> ~nu_tau nu_bar_tau)
     1.14789014E-04    2    -1000016        16   # BR(chi^0_3 -> ~nu^*_tau nu_tau)
     2.21036353E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.21036353E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     4.33959111E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.82645528E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     5.27214407E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     2.41752370E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.71707692E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     3.52258201E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     8.04149321E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     8.04149321E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     1.07940884E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.50762152E-04    2     2000011       -11   # BR(chi^0_4 -> ~e^-_R e^+)
     1.50762152E-04    2    -2000011        11   # BR(chi^0_4 -> ~e^+_R e^-)
     4.28927464E-04    2     1000011       -11   # BR(chi^0_4 -> ~e^-_L e^+)
     4.28927464E-04    2    -1000011        11   # BR(chi^0_4 -> ~e^+_L e^-)
     2.19730793E-04    2     2000013       -13   # BR(chi^0_4 -> ~mu^-_R mu^+)
     2.19730793E-04    2    -2000013        13   # BR(chi^0_4 -> ~mu^+_R mu^-)
     4.76181478E-04    2     1000013       -13   # BR(chi^0_4 -> ~mu^-_L mu^+)
     4.76181478E-04    2    -1000013        13   # BR(chi^0_4 -> ~mu^+_L mu^-)
     7.55472719E-03    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     7.55472719E-03    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     1.83755173E-03    2     2000015       -15   # BR(chi^0_4 -> ~tau^-_2 tau^+)
     1.83755173E-03    2    -2000015        15   # BR(chi^0_4 -> ~tau^+_2 tau^-)
     9.58759710E-04    2     1000012       -12   # BR(chi^0_4 -> ~nu_e nu_bar_e)
     9.58759710E-04    2    -1000012        12   # BR(chi^0_4 -> ~nu^*_e nu_e)
     9.58766236E-04    2     1000014       -14   # BR(chi^0_4 -> ~nu_mu nu_bar_mu)
     9.58766236E-04    2    -1000014        14   # BR(chi^0_4 -> ~nu^*_mu nu_mu)
     5.39038339E-04    2     1000016       -16   # BR(chi^0_4 -> ~nu_tau nu_bar_tau)
     5.39038339E-04    2    -1000016        16   # BR(chi^0_4 -> ~nu^*_tau nu_tau)
     2.81885351E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.81885351E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     4.17667338E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     4.97160718E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     5.35874008E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     2.44556982E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.96831635E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     3.58319585E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     7.35843579E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     7.35843579E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.56447812E+02   # ~g
#    BR                NDA      ID1      ID2
     1.92501022E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.92501022E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.90809484E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.90809484E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.15298514E-01    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     1.15298514E-01    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
     2.29872623E-04    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     5.87826757E-04    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     5.85222882E-04    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     5.92854014E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     5.92854014E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.46851112E-03   # Gamma(h0)
     2.59201469E-03   2        22        22   # BR(h0 -> photon photon)
     1.67040160E-03   2        22        23   # BR(h0 -> photon Z)
     3.07491042E-02   2        23        23   # BR(h0 -> Z Z)
     2.54252968E-01   2       -24        24   # BR(h0 -> W W)
     8.10378455E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.84601435E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.15558660E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.21285196E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.36148815E-07   2        -2         2   # BR(h0 -> Up up)
     2.64245398E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.61154745E-07   2        -1         1   # BR(h0 -> Down down)
     2.02956613E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.40725389E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     7.17918395E+00   # Gamma(HH)
     4.79961653E-08   2        22        22   # BR(HH -> photon photon)
     2.50147545E-07   2        22        23   # BR(HH -> photon Z)
     3.41951885E-05   2        23        23   # BR(HH -> Z Z)
     2.29997789E-05   2       -24        24   # BR(HH -> W W)
     1.43530932E-05   2        21        21   # BR(HH -> gluon gluon)
     8.39075904E-09   2       -11        11   # BR(HH -> Electron electron)
     3.73530949E-04   2       -13        13   # BR(HH -> Muon muon)
     1.06940197E-01   2       -15        15   # BR(HH -> Tau tau)
     2.24053308E-12   2        -2         2   # BR(HH -> Up up)
     4.34558691E-07   2        -4         4   # BR(HH -> Charm charm)
     3.37987274E-02   2        -6         6   # BR(HH -> Top top)
     7.17255464E-07   2        -1         1   # BR(HH -> Down down)
     2.59447553E-04   2        -3         3   # BR(HH -> Strange strange)
     8.52060378E-01   2        -5         5   # BR(HH -> Bottom bottom)
     2.78107349E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     3.42843892E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.83711948E-03   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.38606332E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     9.93240538E-05   2        25        25   # BR(HH -> h0 h0)
     4.99246594E-06   2  -1000011   1000011   # BR(HH -> Selectron1 selectron1)
     1.49387953E-11   2  -2000011   1000011   # BR(HH -> Selectron2 selectron1)
     1.49387953E-11   2  -1000011   2000011   # BR(HH -> Selectron1 selectron2)
     4.30323461E-06   2  -2000011   2000011   # BR(HH -> Selectron2 selectron2)
     4.93478300E-06   2  -1000013   1000013   # BR(HH -> Smuon1 smuon1)
     6.38580801E-07   2  -2000013   1000013   # BR(HH -> Smuon2 smuon1)
     6.38580801E-07   2  -1000013   2000013   # BR(HH -> Smuon1 smuon2)
     4.33170367E-06   2  -2000013   2000013   # BR(HH -> Smuon2 smuon2)
DECAY        36     7.07617991E+00   # Gamma(A0)
     2.86455803E-07   2        22        22   # BR(A0 -> photon photon)
     9.89670929E-07   2        22        23   # BR(A0 -> photon Z)
     1.04504667E-04   2        21        21   # BR(A0 -> gluon gluon)
     8.32864131E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.70765560E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.06149559E-01   2       -15        15   # BR(A0 -> Tau tau)
     2.14221517E-12   2        -2         2   # BR(A0 -> Up up)
     4.15427537E-07   2        -4         4   # BR(A0 -> Charm charm)
     3.38633762E-02   2        -6         6   # BR(A0 -> Top top)
     7.11932119E-07   2        -1         1   # BR(A0 -> Down down)
     2.57521800E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.45733315E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     7.20703840E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     3.68642880E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     2.30481006E-03   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     3.59089621E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     4.58720377E-05   2        23        25   # BR(A0 -> Z h0)
     4.67681866E-37   2        25        25   # BR(A0 -> h0 h0)
     1.50452309E-11   2  -2000011   1000011   # BR(A0 -> Selectron2 selectron1)
     1.50452309E-11   2  -1000011   2000011   # BR(A0 -> Selectron1 selectron2)
     6.43232673E-07   2  -2000013   1000013   # BR(A0 -> Smuon2 smuon1)
     6.43232673E-07   2  -1000013   2000013   # BR(A0 -> Smuon1 smuon2)
DECAY        37     8.07836220E+00   # Gamma(Hp)
     9.63737889E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     4.12028045E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.16544789E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     6.77201961E-07   2        -1         2   # BR(Hp -> Down up)
     1.13543487E-05   2        -3         2   # BR(Hp -> Strange up)
     9.02836900E-06   2        -5         2   # BR(Hp -> Bottom up)
     5.04523180E-08   2        -1         4   # BR(Hp -> Down charm)
     2.44483594E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.26429435E-03   2        -5         4   # BR(Hp -> Bottom charm)
     2.05383607E-06   2        -1         6   # BR(Hp -> Down top)
     4.51342211E-05   2        -3         6   # BR(Hp -> Strange top)
     8.77724485E-01   2        -5         6   # BR(Hp -> Bottom top)
     3.66960234E-03   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     6.51694316E-09   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     4.06823484E-05   2        24        25   # BR(Hp -> W h0)
     1.05328218E-09   2        24        35   # BR(Hp -> W HH)
     1.10811119E-09   2        24        36   # BR(Hp -> W A0)
     2.49386319E-11   2  -1000011   1000012   # BR(Hp -> Selectron1 snu_e1)
     1.51092012E-05   2  -2000011   1000012   # BR(Hp -> Selectron2 snu_e1)
     1.06619136E-06   2  -1000013   1000014   # BR(Hp -> Smuon1 snu_mu1)
     1.51440184E-05   2  -2000013   1000014   # BR(Hp -> Smuon2 snu_mu1)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.23926135E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.51999991E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.51923917E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00030197E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.66748082E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.96945241E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.23926135E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.51999991E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.51923917E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99994028E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    5.97153751E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99994028E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    5.97153751E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26919778E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    2.05787865E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.03069370E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    5.97153751E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99994028E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.35159022E-04   # BR(b -> s gamma)
    2    1.58875195E-06   # BR(b -> s mu+ mu-)
    3    3.52569219E-05   # BR(b -> s nu nu)
    4    2.49899144E-15   # BR(Bd -> e+ e-)
    5    1.06754044E-10   # BR(Bd -> mu+ mu-)
    6    2.23459529E-08   # BR(Bd -> tau+ tau-)
    7    8.46310395E-14   # BR(Bs -> e+ e-)
    8    3.61543412E-09   # BR(Bs -> mu+ mu-)
    9    7.66794822E-07   # BR(Bs -> tau+ tau-)
   10    9.62725988E-05   # BR(B_u -> tau nu)
   11    9.94459088E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41877638E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93590457E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15692315E-03   # epsilon_K
   17    2.28165476E-15   # Delta(M_K)
   18    2.48027053E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29112896E-11   # BR(K^+ -> pi^+ nu nu)
   20   -1.46434566E-14   # Delta(g-2)_electron/2
   21   -6.26159350E-10   # Delta(g-2)_muon/2
   22   -3.82744592E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    4.69437804E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.03618157E-01   # C7
     0305 4322   00   2    -3.21833303E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.14655416E-01   # C8
     0305 6321   00   2    -3.98461442E-04   # C8'
 03051111 4133   00   0     1.62382881E+00   # C9 e+e-
 03051111 4133   00   2     1.62394080E+00   # C9 e+e-
 03051111 4233   00   2     6.01450290E-05   # C9' e+e-
 03051111 4137   00   0    -4.44652091E+00   # C10 e+e-
 03051111 4137   00   2    -4.44521370E+00   # C10 e+e-
 03051111 4237   00   2    -4.46893145E-04   # C10' e+e-
 03051313 4133   00   0     1.62382881E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62394060E+00   # C9 mu+mu-
 03051313 4233   00   2     6.01449425E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.44652091E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44521391E+00   # C10 mu+mu-
 03051313 4237   00   2    -4.46893124E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50508465E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     9.66875757E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50508465E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     9.66875906E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50508457E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     9.66920288E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85842476E-07   # C7
     0305 4422   00   2    -4.87042008E-06   # C7
     0305 4322   00   2     1.06552100E-07   # C7'
     0305 6421   00   0     3.30499598E-07   # C8
     0305 6421   00   2     2.48607278E-06   # C8
     0305 6321   00   2     1.23513954E-07   # C8'
 03051111 4133   00   2    -5.34560720E-07   # C9 e+e-
 03051111 4233   00   2     1.38273852E-06   # C9' e+e-
 03051111 4137   00   2     1.98590892E-06   # C10 e+e-
 03051111 4237   00   2    -1.02756546E-05   # C10' e+e-
 03051313 4133   00   2    -5.34557865E-07   # C9 mu+mu-
 03051313 4233   00   2     1.38273821E-06   # C9' mu+mu-
 03051313 4137   00   2     1.98590673E-06   # C10 mu+mu-
 03051313 4237   00   2    -1.02756554E-05   # C10' mu+mu-
 03051212 4137   00   2    -1.17801907E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     2.22323940E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.17801884E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     2.22323940E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.19236732E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     2.22324378E-06   # C11' nu_3 nu_3
