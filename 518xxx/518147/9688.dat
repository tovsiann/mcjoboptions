# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 22.11.2021,  16:36
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.29580668E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.90710971E+03  # scale for input parameters
    1    2.43067408E+02  # M_1
    2   -1.41853873E+03  # M_2
    3    2.58247585E+03  # M_3
   11   -5.62504340E+03  # A_t
   12   -1.73175651E+03  # A_b
   13   -6.17909949E+02  # A_tau
   23   -8.52992844E+02  # mu
   25    2.19966601E+01  # tan(beta)
   26    1.68107994E+03  # m_A, pole mass
   31    1.03387465E+03  # M_L11
   32    1.03387465E+03  # M_L22
   33    1.06466575E+03  # M_L33
   34    3.93352062E+02  # M_E11
   35    3.93352062E+02  # M_E22
   36    8.42213706E+02  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.39819239E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.47620309E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.07341072E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.90710971E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.90710971E+03  # (SUSY scale)
  1  1     8.39303183E-06   # Y_u(Q)^DRbar
  2  2     4.26366017E-03   # Y_c(Q)^DRbar
  3  3     1.01394566E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.90710971E+03  # (SUSY scale)
  1  1     3.70984583E-04   # Y_d(Q)^DRbar
  2  2     7.04870707E-03   # Y_s(Q)^DRbar
  3  3     3.67900456E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.90710971E+03  # (SUSY scale)
  1  1     6.47395226E-05   # Y_e(Q)^DRbar
  2  2     1.33860800E-02   # Y_mu(Q)^DRbar
  3  3     2.25131844E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.90710971E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -5.62504913E+03   # A_t(Q)^DRbar
Block Ad Q=  3.90710971E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.73175618E+03   # A_b(Q)^DRbar
Block Ae Q=  3.90710971E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -6.17909729E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.90710971E+03  # soft SUSY breaking masses at Q
   1    2.43067408E+02  # M_1
   2   -1.41853873E+03  # M_2
   3    2.58247585E+03  # M_3
  21    2.09002643E+06  # M^2_(H,d)
  22   -4.37877949E+05  # M^2_(H,u)
  31    1.03387465E+03  # M_(L,11)
  32    1.03387465E+03  # M_(L,22)
  33    1.06466575E+03  # M_(L,33)
  34    3.93352062E+02  # M_(E,11)
  35    3.93352062E+02  # M_(E,22)
  36    8.42213706E+02  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.39819239E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.47620309E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.07341072E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.25727322E+02  # h0
        35     1.68115890E+03  # H0
        36     1.68107994E+03  # A0
        37     1.68494444E+03  # H+
   1000001     1.01129163E+04  # ~d_L
   2000001     1.00886722E+04  # ~d_R
   1000002     1.01125612E+04  # ~u_L
   2000002     1.00916365E+04  # ~u_R
   1000003     1.01129175E+04  # ~s_L
   2000003     1.00886736E+04  # ~s_R
   1000004     1.01125623E+04  # ~c_L
   2000004     1.00916372E+04  # ~c_R
   1000005     3.45366654E+03  # ~b_1
   2000005     4.14532145E+03  # ~b_2
   1000006     3.44421315E+03  # ~t_1
   2000006     4.43221880E+03  # ~t_2
   1000011     1.06042221E+03  # ~e_L-
   2000011     4.11736683E+02  # ~e_R-
   1000012     1.05703284E+03  # ~nu_eL
   1000013     1.06041751E+03  # ~mu_L-
   2000013     4.11703982E+02  # ~mu_R-
   1000014     1.05702336E+03  # ~nu_muL
   1000015     8.44576440E+02  # ~tau_1-
   2000015     1.08937534E+03  # ~tau_2-
   1000016     1.08453360E+03  # ~nu_tauL
   1000021     3.04532165E+03  # ~g
   1000022     2.39053107E+02  # ~chi_10
   1000023     8.60325990E+02  # ~chi_20
   1000025     8.67133143E+02  # ~chi_30
   1000035     1.48200461E+03  # ~chi_40
   1000024     8.60424119E+02  # ~chi_1+
   1000037     1.48196505E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -4.39284844E-02   # alpha
Block Hmix Q=  3.90710971E+03  # Higgs mixing parameters
   1   -8.52992844E+02  # mu
   2    2.19966601E+01  # tan[beta](Q)
   3    2.42947650E+02  # v(Q)
   4    2.82602976E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.95082892E-01   # Re[R_st(1,1)]
   1  2     9.90456393E-02   # Re[R_st(1,2)]
   2  1    -9.90456393E-02   # Re[R_st(2,1)]
   2  2     9.95082892E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99975087E-01   # Re[R_sb(1,1)]
   1  2     7.05867688E-03   # Re[R_sb(1,2)]
   2  1    -7.05867688E-03   # Re[R_sb(2,1)]
   2  2    -9.99975087E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -7.12984759E-02   # Re[R_sta(1,1)]
   1  2     9.97455025E-01   # Re[R_sta(1,2)]
   2  1    -9.97455025E-01   # Re[R_sta(2,1)]
   2  2    -7.12984759E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.98440310E-01   # Re[N(1,1)]
   1  2     5.08752730E-04   # Re[N(1,2)]
   1  3    -5.43422462E-02   # Re[N(1,3)]
   1  4    -1.27909805E-02   # Re[N(1,4)]
   2  1     2.93477261E-02   # Re[N(2,1)]
   2  2     9.42549887E-02   # Re[N(2,2)]
   2  3     7.05323978E-01   # Re[N(2,3)]
   2  4    -7.01977773E-01   # Re[N(2,4)]
   3  1     4.74429518E-02   # Re[N(3,1)]
   3  2    -2.29305990E-02   # Re[N(3,2)]
   3  3     7.04990938E-01   # Re[N(3,3)]
   3  4     7.07256058E-01   # Re[N(3,4)]
   4  1    -2.19659300E-03   # Re[N(4,1)]
   4  2     9.95283842E-01   # Re[N(4,2)]
   4  3    -5.05250766E-02   # Re[N(4,3)]
   4  4     8.27796214E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -7.16393128E-02   # Re[U(1,1)]
   1  2    -9.97430604E-01   # Re[U(1,2)]
   2  1    -9.97430604E-01   # Re[U(2,1)]
   2  2     7.16393128E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     1.17330329E-01   # Re[V(1,1)]
   1  2     9.93092943E-01   # Re[V(1,2)]
   2  1     9.93092943E-01   # Re[V(2,1)]
   2  2    -1.17330329E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     9.06303281E-01   # ~e^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000011     1.21083606E+00   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.89937549E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     5.44198942E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     4.61797026E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     9.06087704E-01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
DECAY   1000013     1.21227915E+00   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.88761817E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     5.61744323E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.72611851E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     4.61229503E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     8.35832620E-04    2     2000013        25   # BR(~mu^-_L -> ~mu^-_R h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     3.57431824E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
DECAY   2000015     1.63019702E+00   # ~tau^-_2
#    BR                NDA      ID1      ID2
     7.67811499E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     5.10703945E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     4.58380993E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.86927296E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     6.90106342E-02    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     6.44000979E-02    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.20846828E+00   # ~nu_e
#    BR                NDA      ID1      ID2
     9.84264544E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.70855217E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     9.59602776E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     1.20673007E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
DECAY   1000014     1.20887812E+00   # ~nu_mu
#    BR                NDA      ID1      ID2
     9.83920131E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.70741901E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     9.59198033E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     1.24132523E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
DECAY   1000016     1.62355522E+00   # ~nu_tau
#    BR                NDA      ID1      ID2
     7.55745772E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.49501547E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     8.91650615E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     1.02691213E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     1.38176349E-01    2     1000015        24   # BR(~nu_tau -> ~tau^-_1 W^+)
DECAY   2000001     6.63451320E+02   # ~d_R
#    BR                NDA      ID1      ID2
     8.45363197E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.91520213E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.89335865E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.77046093E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     4.40542704E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     5.35298734E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     5.70479541E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.07437874E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.36196040E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.63471045E+02   # ~s_R
#    BR                NDA      ID1      ID2
     8.45342605E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.91490926E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.89349417E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.77046801E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     4.46727438E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     5.35289915E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     5.74971079E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.07436107E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.36181822E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.14200763E+02   # ~b_1
#    BR                NDA      ID1      ID2
     4.42509670E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     3.66474850E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     3.56000251E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     8.79137733E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     5.33462139E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.83808553E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     1.18142928E-01    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     9.21832241E+01   # ~b_2
#    BR                NDA      ID1      ID2
     2.51530772E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     5.50684666E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     5.51477323E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     3.26357089E-04    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     1.09799721E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     6.54327741E-04    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     7.52091239E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     7.54923915E-04    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     3.64967868E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     6.39186775E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     6.80606291E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.29719247E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.66926383E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.89322250E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.79080946E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     5.50023375E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.34446690E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.53023934E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.06503216E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.36169389E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.80613516E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.29715765E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.66916167E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.89335758E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.79077971E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     5.52265931E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.34437856E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.54259448E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.06501469E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.36155168E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.12534865E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.35449499E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.67523026E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.70105192E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     9.12411201E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     7.16344039E-02    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.81319110E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     3.81004254E-04    2     1000021         4   # BR(~t_1 -> ~g c)
     1.10695411E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
     1.72830787E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     3.51450886E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.74381146E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.18863683E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.20198886E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.87028368E-04    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.37176870E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     5.56276628E-04    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.68851914E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     9.91991082E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     5.02348030E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     7.71931726E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     5.30509828E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     1.70983424E-03    2    -2000013        14   # BR(chi^+_1 -> ~mu^+_R nu_mu)
     1.11109795E-03    2    -1000015        16   # BR(chi^+_1 -> ~tau^+_1 nu_tau)
     9.85961839E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.11971478E-02    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     2.09349571E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     7.38158153E-02    2    -1000011        12   # BR(chi^+_2 -> ~e^+_L nu_e)
     7.38171884E-02    2    -1000013        14   # BR(chi^+_2 -> ~mu^+_L nu_mu)
     3.12874486E-04    2    -1000015        16   # BR(chi^+_2 -> ~tau^+_1 nu_tau)
     6.53843955E-02    2    -2000015        16   # BR(chi^+_2 -> ~tau^+_2 nu_tau)
     7.41585067E-02    2     1000012       -11   # BR(chi^+_2 -> ~nu_e e^+)
     7.41613540E-02    2     1000014       -13   # BR(chi^+_2 -> ~nu_mu mu^+)
     6.63055340E-02    2     1000016       -15   # BR(chi^+_2 -> ~nu_tau tau^+)
     1.06798052E-03    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     1.46185256E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.39020748E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     1.40087505E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     1.39739295E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.66872233E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     2.05407538E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     2.18502947E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     8.12181797E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     1.36238562E-03    2     2000011       -11   # BR(chi^0_2 -> ~e^-_R e^+)
     1.36238562E-03    2    -2000011        11   # BR(chi^0_2 -> ~e^+_R e^-)
     1.92139555E-03    2     2000013       -13   # BR(chi^0_2 -> ~mu^-_R mu^+)
     1.92139555E-03    2    -2000013        13   # BR(chi^0_2 -> ~mu^+_R mu^-)
     3.63863653E-04    2     1000015       -15   # BR(chi^0_2 -> ~tau^-_1 tau^+)
     3.63863653E-04    2    -1000015        15   # BR(chi^0_2 -> ~tau^+_1 tau^-)
     4.79010349E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     5.11427404E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.23046831E-03    3     1000022         6        -6   # BR(chi^0_2 -> chi^0_1 t t_bar)
DECAY   1000025     5.24842340E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     5.60486632E-03    2     2000011       -11   # BR(chi^0_3 -> ~e^-_R e^+)
     5.60486632E-03    2    -2000011        11   # BR(chi^0_3 -> ~e^+_R e^-)
     6.48489235E-03    2     2000013       -13   # BR(chi^0_3 -> ~mu^-_R mu^+)
     6.48489235E-03    2    -2000013        13   # BR(chi^0_3 -> ~mu^+_R mu^-)
     1.10420836E-03    2     1000015       -15   # BR(chi^0_3 -> ~tau^-_1 tau^+)
     1.10420836E-03    2    -1000015        15   # BR(chi^0_3 -> ~tau^+_1 tau^-)
     2.91421699E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     6.79015070E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.14668011E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
DECAY   1000035     2.34619029E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     3.27184219E-02    2     1000011       -11   # BR(chi^0_4 -> ~e^-_L e^+)
     3.27184219E-02    2    -1000011        11   # BR(chi^0_4 -> ~e^+_L e^-)
     3.27191258E-02    2     1000013       -13   # BR(chi^0_4 -> ~mu^-_L mu^+)
     3.27191258E-02    2    -1000013        13   # BR(chi^0_4 -> ~mu^+_L mu^-)
     1.38577308E-04    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     1.38577308E-04    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     2.90062195E-02    2     2000015       -15   # BR(chi^0_4 -> ~tau^-_2 tau^+)
     2.90062195E-02    2    -2000015        15   # BR(chi^0_4 -> ~tau^+_2 tau^-)
     3.33147867E-02    2     1000012       -12   # BR(chi^0_4 -> ~nu_e nu_bar_e)
     3.33147867E-02    2    -1000012        12   # BR(chi^0_4 -> ~nu^*_e nu_e)
     3.33160243E-02    2     1000014       -14   # BR(chi^0_4 -> ~nu_mu nu_bar_mu)
     3.33160243E-02    2    -1000014        14   # BR(chi^0_4 -> ~nu^*_mu nu_mu)
     2.97772814E-02    2     1000016       -16   # BR(chi^0_4 -> ~nu_tau nu_bar_tau)
     2.97772814E-02    2    -1000016        16   # BR(chi^0_4 -> ~nu^*_tau nu_tau)
     1.32215707E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.32215707E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     6.33594067E-04    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     7.15108311E-03    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.22624378E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     5.57233116E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.17357196E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.00239291E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     5.20380747E-04    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     4.38683252E-04    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     2.00808401E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     2.00808401E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.99155308E-01   # ~g
#    BR                NDA      ID1      ID2
     2.55411246E-03    2     1000023        21   # BR(~g -> chi^0_2 g)
     2.87078274E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     2.75942813E-04    3     1000022         2        -2   # BR(~g -> chi^0_1 u u_bar)
     2.75942749E-04    3     1000022         4        -4   # BR(~g -> chi^0_1 c c_bar)
     1.18062565E-02    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     6.64842752E-03    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     1.82444613E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     3.02377827E-02    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.81221638E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     2.94137721E-02    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     1.23300552E-04    3     1000035         2        -2   # BR(~g -> chi^0_4 u u_bar)
     1.23300642E-04    3     1000035         4        -4   # BR(~g -> chi^0_4 c c_bar)
     2.31800509E-02    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     1.23476008E-04    3     1000035         1        -1   # BR(~g -> chi^0_4 d d_bar)
     1.23476095E-04    3     1000035         3        -3   # BR(~g -> chi^0_4 s s_bar)
     2.98648620E-02    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     2.22949336E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.22949336E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     2.60763933E-02    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     2.60763933E-02    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.63468369E-03   # Gamma(h0)
     2.55687759E-03   2        22        22   # BR(h0 -> photon photon)
     1.70608753E-03   2        22        23   # BR(h0 -> photon Z)
     3.23111962E-02   2        23        23   # BR(h0 -> Z Z)
     2.63127272E-01   2       -24        24   # BR(h0 -> W W)
     7.87009907E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.80333549E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.13661299E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.15980041E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.34258168E-07   2        -2         2   # BR(h0 -> Up up)
     2.60574131E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.54738001E-07   2        -1         1   # BR(h0 -> Down down)
     2.00635514E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.33527168E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.27456947E+01   # Gamma(HH)
     4.99330451E-08   2        22        22   # BR(HH -> photon photon)
     9.40759910E-08   2        22        23   # BR(HH -> photon Z)
     1.43101234E-05   2        23        23   # BR(HH -> Z Z)
     9.73254935E-06   2       -24        24   # BR(HH -> W W)
     1.52090135E-05   2        21        21   # BR(HH -> gluon gluon)
     1.07230044E-08   2       -11        11   # BR(HH -> Electron electron)
     4.77353909E-04   2       -13        13   # BR(HH -> Muon muon)
     1.37812620E-01   2       -15        15   # BR(HH -> Tau tau)
     5.33383702E-13   2        -2         2   # BR(HH -> Up up)
     1.03458891E-07   2        -4         4   # BR(HH -> Charm charm)
     8.22593763E-03   2        -6         6   # BR(HH -> Top top)
     7.81537982E-07   2        -1         1   # BR(HH -> Down down)
     2.82669506E-04   2        -3         3   # BR(HH -> Strange strange)
     7.74686711E-01   2        -5         5   # BR(HH -> Bottom bottom)
     8.05276354E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     4.43511367E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     3.31944384E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.66284738E-05   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.04092342E-04   2        25        25   # BR(HH -> h0 h0)
     1.36688467E-06   2  -1000011   1000011   # BR(HH -> Selectron1 selectron1)
     1.38332771E-12   2  -2000011   1000011   # BR(HH -> Selectron2 selectron1)
     1.38332771E-12   2  -1000011   2000011   # BR(HH -> Selectron1 selectron2)
     1.35910291E-06   2  -1000013   1000013   # BR(HH -> Smuon1 smuon1)
     5.91406364E-08   2  -2000013   1000013   # BR(HH -> Smuon2 smuon1)
     5.91406364E-08   2  -1000013   2000013   # BR(HH -> Smuon1 smuon2)
DECAY        36     1.24787244E+01   # Gamma(A0)
     7.67292148E-08   2        22        22   # BR(A0 -> photon photon)
     1.08210418E-07   2        22        23   # BR(A0 -> photon Z)
     6.21484617E-05   2        21        21   # BR(A0 -> gluon gluon)
     1.07070005E-08   2       -11        11   # BR(A0 -> Electron electron)
     4.76641295E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.37606120E-01   2       -15        15   # BR(A0 -> Tau tau)
     5.22287984E-13   2        -2         2   # BR(A0 -> Up up)
     1.01300621E-07   2        -4         4   # BR(A0 -> Charm charm)
     8.45552551E-03   2        -6         6   # BR(A0 -> Top top)
     7.80300632E-07   2        -1         1   # BR(A0 -> Down down)
     2.82222188E-04   2        -3         3   # BR(A0 -> Strange strange)
     7.73474779E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     8.59735017E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     3.57872238E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     4.29747738E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     3.17605458E-07   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     1.93171577E-05   2        23        25   # BR(A0 -> Z h0)
     3.04473940E-36   2        25        25   # BR(A0 -> h0 h0)
     1.39276462E-12   2  -2000011   1000011   # BR(A0 -> Selectron2 selectron1)
     1.39276462E-12   2  -1000011   2000011   # BR(A0 -> Selectron1 selectron2)
     5.95453064E-08   2  -2000013   1000013   # BR(A0 -> Smuon2 smuon1)
     5.95453064E-08   2  -1000013   2000013   # BR(A0 -> Smuon1 smuon2)
DECAY        37     1.35668466E+01   # Gamma(Hp)
     1.08252508E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     4.62813278E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.30909714E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     7.59458969E-07   2        -1         2   # BR(Hp -> Down up)
     1.27302505E-05   2        -3         2   # BR(Hp -> Strange up)
     8.34892126E-06   2        -5         2   # BR(Hp -> Bottom up)
     4.11331040E-08   2        -1         4   # BR(Hp -> Down charm)
     2.73808242E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.16914697E-03   2        -5         4   # BR(Hp -> Bottom charm)
     6.37291137E-07   2        -1         6   # BR(Hp -> Down top)
     1.42916157E-05   2        -3         6   # BR(Hp -> Strange top)
     7.92000897E-01   2        -5         6   # BR(Hp -> Bottom top)
     7.51104418E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     1.83230537E-05   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     1.79285520E-05   2        24        25   # BR(Hp -> W h0)
     7.78808945E-11   2        24        35   # BR(Hp -> W HH)
     8.63475705E-11   2        24        36   # BR(Hp -> W A0)
     2.51658714E-12   2  -1000011   1000012   # BR(Hp -> Selectron1 snu_e1)
     1.07593795E-07   2  -1000013   1000014   # BR(Hp -> Smuon1 snu_mu1)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.35024959E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    4.83918031E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    4.83853056E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00013429E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.93245645E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    2.06674317E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.35024959E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    4.83918031E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    4.83853056E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99997745E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.25504851E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99997745E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.25504851E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26719943E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    2.53372823E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.03706466E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.25504851E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99997745E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.44128689E-04   # BR(b -> s gamma)
    2    1.58812821E-06   # BR(b -> s mu+ mu-)
    3    3.52563705E-05   # BR(b -> s nu nu)
    4    2.19750501E-15   # BR(Bd -> e+ e-)
    5    9.38751031E-11   # BR(Bd -> mu+ mu-)
    6    1.96626180E-08   # BR(Bd -> tau+ tau-)
    7    7.45101282E-14   # BR(Bs -> e+ e-)
    8    3.18307641E-09   # BR(Bs -> mu+ mu-)
    9    6.75521590E-07   # BR(Bs -> tau+ tau-)
   10    9.55946888E-05   # BR(B_u -> tau nu)
   11    9.87456537E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42019292E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93600031E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15752873E-03   # epsilon_K
   17    2.28166156E-15   # Delta(M_K)
   18    2.48052291E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29161804E-11   # BR(K^+ -> pi^+ nu nu)
   20   -3.68387028E-15   # Delta(g-2)_electron/2
   21   -1.57572946E-10   # Delta(g-2)_muon/2
   22   -1.90584459E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    5.56580777E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.14095568E-01   # C7
     0305 4322   00   2    -3.78190234E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.19795718E-01   # C8
     0305 6321   00   2    -4.18913509E-04   # C8'
 03051111 4133   00   0     1.62562387E+00   # C9 e+e-
 03051111 4133   00   2     1.62581870E+00   # C9 e+e-
 03051111 4233   00   2     1.35150959E-04   # C9' e+e-
 03051111 4137   00   0    -4.44831597E+00   # C10 e+e-
 03051111 4137   00   2    -4.44701466E+00   # C10 e+e-
 03051111 4237   00   2    -1.00159121E-03   # C10' e+e-
 03051313 4133   00   0     1.62562387E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62581823E+00   # C9 mu+mu-
 03051313 4233   00   2     1.35150630E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44831597E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44701513E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.00159115E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50507763E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     2.16625214E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50507763E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     2.16625269E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50507764E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     2.16640951E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85801246E-07   # C7
     0305 4422   00   2     4.07786758E-06   # C7
     0305 4322   00   2    -8.07863265E-07   # C7'
     0305 6421   00   0     3.30464282E-07   # C8
     0305 6421   00   2     1.42500669E-05   # C8
     0305 6321   00   2    -1.32217679E-07   # C8'
 03051111 4133   00   2    -5.64572182E-07   # C9 e+e-
 03051111 4233   00   2     3.00936937E-06   # C9' e+e-
 03051111 4137   00   2     4.20758398E-06   # C10 e+e-
 03051111 4237   00   2    -2.23052054E-05   # C10' e+e-
 03051313 4133   00   2    -5.64569229E-07   # C9 mu+mu-
 03051313 4233   00   2     3.00936811E-06   # C9' mu+mu-
 03051313 4137   00   2     4.20758506E-06   # C10 mu+mu-
 03051313 4237   00   2    -2.23052089E-05   # C10' mu+mu-
 03051212 4137   00   2    -7.48093110E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     4.82423064E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -7.48093147E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     4.82423064E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -7.47998095E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     4.82423091E-06   # C11' nu_3 nu_3
