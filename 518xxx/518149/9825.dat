# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 22.11.2021,  16:39
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.11881776E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    4.13112006E+03  # scale for input parameters
    1    2.63280276E+01  # M_1
    2    8.03834694E+02  # M_2
    3    3.37760917E+03  # M_3
   11    7.22393878E+03  # A_t
   12   -1.88800385E+03  # A_b
   13   -8.79891511E+02  # A_tau
   23    1.81605528E+03  # mu
   25    2.01919450E+01  # tan(beta)
   26    1.66435152E+03  # m_A, pole mass
   31    2.60834435E+02  # M_L11
   32    2.60834435E+02  # M_L22
   33    1.33760300E+03  # M_L33
   34    6.30326043E+02  # M_E11
   35    6.30326043E+02  # M_E22
   36    1.43059737E+03  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.81885158E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.47522560E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.28099580E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  4.13112006E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  4.13112006E+03  # (SUSY scale)
  1  1     8.39464799E-06   # Y_u(Q)^DRbar
  2  2     4.26448118E-03   # Y_c(Q)^DRbar
  3  3     1.01414090E+00   # Y_t(Q)^DRbar
Block Yd Q=  4.13112006E+03  # (SUSY scale)
  1  1     3.40612743E-04   # Y_d(Q)^DRbar
  2  2     6.47164212E-03   # Y_s(Q)^DRbar
  3  3     3.37781109E-01   # Y_b(Q)^DRbar
Block Ye Q=  4.13112006E+03  # (SUSY scale)
  1  1     5.94394144E-05   # Y_e(Q)^DRbar
  2  2     1.22901857E-02   # Y_mu(Q)^DRbar
  3  3     2.06700706E-01   # Y_tau(Q)^DRbar
Block Au Q=  4.13112006E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     7.22393490E+03   # A_t(Q)^DRbar
Block Ad Q=  4.13112006E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.88800498E+03   # A_b(Q)^DRbar
Block Ae Q=  4.13112006E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -8.79891734E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  4.13112006E+03  # soft SUSY breaking masses at Q
   1    2.63280276E+01  # M_1
   2    8.03834694E+02  # M_2
   3    3.37760917E+03  # M_3
  21   -5.92801570E+05  # M^2_(H,d)
  22   -2.98976944E+06  # M^2_(H,u)
  31    2.60834435E+02  # M_(L,11)
  32    2.60834435E+02  # M_(L,22)
  33    1.33760300E+03  # M_(L,33)
  34    6.30326043E+02  # M_(E,11)
  35    6.30326043E+02  # M_(E,22)
  36    1.43059737E+03  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.81885158E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.47522560E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.28099580E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.27494316E+02  # h0
        35     1.66440143E+03  # H0
        36     1.66435152E+03  # A0
        37     1.66903290E+03  # H+
   1000001     1.01096144E+04  # ~d_L
   2000001     1.00847979E+04  # ~d_R
   1000002     1.01092628E+04  # ~u_L
   2000002     1.00882156E+04  # ~u_R
   1000003     1.01096152E+04  # ~s_L
   2000003     1.00847984E+04  # ~s_R
   1000004     1.01092634E+04  # ~c_L
   2000004     1.00882160E+04  # ~c_R
   1000005     3.88499448E+03  # ~b_1
   2000005     4.38433072E+03  # ~b_2
   1000006     3.86268148E+03  # ~t_1
   2000006     4.41821389E+03  # ~t_2
   1000011     3.01968730E+02  # ~e_L-
   2000011     6.34544537E+02  # ~e_R-
   1000012     2.91195886E+02  # ~nu_eL
   1000013     3.01873925E+02  # ~mu_L-
   2000013     6.34566748E+02  # ~mu_R-
   1000014     2.91181889E+02  # ~nu_muL
   1000015     1.33861270E+03  # ~tau_1-
   2000015     1.43650042E+03  # ~tau_2-
   1000016     1.34267863E+03  # ~nu_tauL
   1000021     3.88047501E+03  # ~g
   1000022     2.56876624E+01  # ~chi_10
   1000023     8.47943455E+02  # ~chi_20
   1000025     1.83545515E+03  # ~chi_30
   1000035     1.83815679E+03  # ~chi_40
   1000024     8.48034038E+02  # ~chi_1+
   1000037     1.83836889E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -4.75875651E-02   # alpha
Block Hmix Q=  4.13112006E+03  # Higgs mixing parameters
   1    1.81605528E+03  # mu
   2    2.01919450E+01  # tan[beta](Q)
   3    2.42885082E+02  # v(Q)
   4    2.77006598E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.80869792E-01   # Re[R_st(1,1)]
   1  2     1.94664974E-01   # Re[R_st(1,2)]
   2  1    -1.94664974E-01   # Re[R_st(2,1)]
   2  2    -9.80869792E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99842174E-01   # Re[R_sb(1,1)]
   1  2     1.77658730E-02   # Re[R_sb(1,2)]
   2  1    -1.77658730E-02   # Re[R_sb(2,1)]
   2  2     9.99842174E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     9.65947412E-01   # Re[R_sta(1,1)]
   1  2     2.58738472E-01   # Re[R_sta(1,2)]
   2  1    -2.58738472E-01   # Re[R_sta(2,1)]
   2  2     9.65947412E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.99707067E-01   # Re[N(1,1)]
   1  2    -2.45170817E-04   # Re[N(1,2)]
   1  3     2.41554030E-02   # Re[N(1,3)]
   1  4    -1.49570688E-03   # Re[N(1,4)]
   2  1    -1.61202009E-03   # Re[N(2,1)]
   2  2    -9.98126773E-01   # Re[N(2,2)]
   2  3     5.49186891E-02   # Re[N(2,3)]
   2  4    -2.69125233E-02   # Re[N(2,4)]
   3  1     1.60097081E-02   # Re[N(3,1)]
   3  2    -1.98351986E-02   # Re[N(3,2)]
   3  3    -7.06575381E-01   # Re[N(3,3)]
   3  4    -7.07178538E-01   # Re[N(3,4)]
   4  1    -1.80795847E-02   # Re[N(4,1)]
   4  2     5.78744306E-02   # Re[N(4,2)]
   4  3     7.05089842E-01   # Re[N(4,3)]
   4  4    -7.06521050E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.96959241E-01   # Re[U(1,1)]
   1  2     7.79247780E-02   # Re[U(1,2)]
   2  1     7.79247780E-02   # Re[U(2,1)]
   2  2     9.96959241E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -9.99260614E-01   # Re[V(1,1)]
   1  2     3.84477083E-02   # Re[V(1,2)]
   2  1     3.84477083E-02   # Re[V(2,1)]
   2  2     9.99260614E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   1000011     3.73288934E-01   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.99999321E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000011     3.17605152E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.99999959E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
DECAY   1000013     3.73168792E-01   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.99999346E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     3.18168026E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.98266297E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.73370253E-03    2     1000013        25   # BR(~mu^-_R -> ~mu^-_L h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     7.99341170E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     2.54277895E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     2.49352716E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     4.96369389E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
DECAY   2000015     7.31759843E+00   # ~tau^-_2
#    BR                NDA      ID1      ID2
     9.33321185E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.03058721E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     4.03554717E-02    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.07494179E-03    2     1000016       -24   # BR(~tau^-_2 -> ~nu_tau W^-)
     9.42528915E-04    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
DECAY   1000012     3.60236995E-01   # ~nu_e
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
DECAY   1000014     3.60219136E-01   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
DECAY   1000016     8.07614319E+00   # ~nu_tau
#    BR                NDA      ID1      ID2
     2.08755902E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.63017774E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     5.28226324E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.83388710E+02   # ~d_R
#    BR                NDA      ID1      ID2
     9.64519593E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.90349444E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.13163090E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.98281887E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     6.12545478E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     2.17758375E-04    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.22292621E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     7.08472048E-04    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.13513736E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.83404428E+02   # ~s_R
#    BR                NDA      ID1      ID2
     9.64494493E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.90322845E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.13174455E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.98279431E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     6.12536114E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     2.23230479E-04    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.22290684E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     7.13254856E-04    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.13500881E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     9.99306704E+01   # ~b_1
#    BR                NDA      ID1      ID2
     5.55038512E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.54935575E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.65861365E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     2.66942142E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     3.07817901E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     4.78410750E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     3.27105503E+01   # ~b_2
#    BR                NDA      ID1      ID2
     7.47466891E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     2.58420166E-04    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.03540312E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.03263355E-01    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     5.22226334E-04    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     2.06987196E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     4.96361552E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     5.75513121E-03    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     2.60489369E-03    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     5.96022405E-03    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     6.00618144E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.74867489E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.62492707E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.13147162E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.97190140E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     6.13242311E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.74260812E-04    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.22856222E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.72497031E-04    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.13484056E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.00624976E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.74863226E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.62481772E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.13158473E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.97187017E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     6.13232629E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.76653742E-04    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.22854351E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.83440485E-04    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.13471193E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.00028371E+02   # ~t_1
#    BR                NDA      ID1      ID2
     8.28503834E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.43798060E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.38287622E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.44532892E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.89452065E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     7.29659195E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     2.58443880E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     2.12907399E+02   # ~t_2
#    BR                NDA      ID1      ID2
     4.45917575E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     5.33416255E-03    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.43491202E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.39777968E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.07729584E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     2.77282475E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     8.97976692E-02    2     1000021         6   # BR(~t_2 -> ~g t)
     1.17672833E-01    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     6.31092048E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.08169769E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.14594207E+01   # chi^+_1
#    BR                NDA      ID1      ID2
     2.46855265E-01    2    -1000011        12   # BR(chi^+_1 -> ~e^+_L nu_e)
     2.46900274E-01    2    -1000013        14   # BR(chi^+_1 -> ~mu^+_L nu_mu)
     2.53068982E-01    2     1000012       -11   # BR(chi^+_1 -> ~nu_e e^+)
     2.53076216E-01    2     1000014       -13   # BR(chi^+_1 -> ~nu_mu mu^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     1.37453895E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     3.38432863E-03    2    -1000011        12   # BR(chi^+_2 -> ~e^+_L nu_e)
     3.38444647E-03    2    -1000013        14   # BR(chi^+_2 -> ~mu^+_L nu_mu)
     1.54971794E-04    2    -2000013        14   # BR(chi^+_2 -> ~mu^+_R nu_mu)
     9.10715545E-03    2    -2000015        16   # BR(chi^+_2 -> ~tau^+_2 nu_tau)
     8.27086321E-04    2     1000012       -11   # BR(chi^+_2 -> ~nu_e e^+)
     1.01682847E-03    2     1000014       -13   # BR(chi^+_2 -> ~nu_mu mu^+)
     1.24750202E-02    2     1000016       -15   # BR(chi^+_2 -> ~nu_tau tau^+)
     8.92611211E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.83274639E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.48248040E-03    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     2.79268702E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.93716940E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.00115943E-02    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     3.17216145E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     8.38886550E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.14573486E+01   # chi^0_2
#    BR                NDA      ID1      ID2
     1.23931951E-01    2     1000011       -11   # BR(chi^0_2 -> ~e^-_L e^+)
     1.23931951E-01    2    -1000011        11   # BR(chi^0_2 -> ~e^+_L e^-)
     1.23954910E-01    2     1000013       -13   # BR(chi^0_2 -> ~mu^-_L mu^+)
     1.23954910E-01    2    -1000013        13   # BR(chi^0_2 -> ~mu^+_L mu^-)
     1.26031875E-01    2     1000012       -12   # BR(chi^0_2 -> ~nu_e nu_bar_e)
     1.26031875E-01    2    -1000012        12   # BR(chi^0_2 -> ~nu^*_e nu_e)
     1.26035115E-01    2     1000014       -14   # BR(chi^0_2 -> ~nu_mu nu_bar_mu)
     1.26035115E-01    2    -1000014        14   # BR(chi^0_2 -> ~nu^*_mu nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     1.61564061E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     1.10786224E-04    2     1000013       -13   # BR(chi^0_3 -> ~mu^-_L mu^+)
     1.10786224E-04    2    -1000013        13   # BR(chi^0_3 -> ~mu^+_L mu^-)
     1.22759623E-04    2     2000013       -13   # BR(chi^0_3 -> ~mu^-_R mu^+)
     1.22759623E-04    2    -2000013        13   # BR(chi^0_3 -> ~mu^+_R mu^-)
     5.05742927E-03    2     1000015       -15   # BR(chi^0_3 -> ~tau^-_1 tau^+)
     5.05742927E-03    2    -1000015        15   # BR(chi^0_3 -> ~tau^+_1 tau^-)
     3.78390750E-03    2     2000015       -15   # BR(chi^0_3 -> ~tau^-_2 tau^+)
     3.78390750E-03    2    -2000015        15   # BR(chi^0_3 -> ~tau^+_2 tau^-)
     1.91468338E-04    2     1000012       -12   # BR(chi^0_3 -> ~nu_e nu_bar_e)
     1.91468338E-04    2    -1000012        12   # BR(chi^0_3 -> ~nu^*_e nu_e)
     1.91469289E-04    2     1000014       -14   # BR(chi^0_3 -> ~nu_mu nu_bar_mu)
     1.91469289E-04    2    -1000014        14   # BR(chi^0_3 -> ~nu^*_mu nu_mu)
     2.41302031E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.41302031E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     4.27585415E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     2.14276098E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     8.42966385E-04    2     1000022        36   # BR(chi^0_3 -> chi^0_1 A^0)
     3.29086314E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.87458973E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
     1.40668541E-03    2     1000022        35   # BR(chi^0_3 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     1.89067378E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     3.12152119E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     6.73782594E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     6.73782594E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     1.35447612E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     6.57361448E-04    2     1000011       -11   # BR(chi^0_4 -> ~e^-_L e^+)
     6.57361448E-04    2    -1000011        11   # BR(chi^0_4 -> ~e^+_L e^-)
     7.53300222E-04    2     1000013       -13   # BR(chi^0_4 -> ~mu^-_L mu^+)
     7.53300222E-04    2    -1000013        13   # BR(chi^0_4 -> ~mu^+_L mu^-)
     1.65092911E-04    2     2000013       -13   # BR(chi^0_4 -> ~mu^-_R mu^+)
     1.65092911E-04    2    -2000013        13   # BR(chi^0_4 -> ~mu^+_R mu^-)
     5.78597320E-03    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     5.78597320E-03    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     4.82733886E-03    2     2000015       -15   # BR(chi^0_4 -> ~tau^-_2 tau^+)
     4.82733886E-03    2    -2000015        15   # BR(chi^0_4 -> ~tau^+_2 tau^-)
     1.29461030E-03    2     1000012       -12   # BR(chi^0_4 -> ~nu_e nu_bar_e)
     1.29461030E-03    2    -1000012        12   # BR(chi^0_4 -> ~nu^*_e nu_e)
     1.29461671E-03    2     1000014       -14   # BR(chi^0_4 -> ~nu_mu nu_bar_mu)
     1.29461671E-03    2    -1000014        14   # BR(chi^0_4 -> ~nu^*_mu nu_mu)
     2.96357973E-04    2     1000016       -16   # BR(chi^0_4 -> ~nu_tau nu_bar_tau)
     2.96357973E-04    2    -1000016        16   # BR(chi^0_4 -> ~nu^*_tau nu_tau)
     2.79160835E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.79160835E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     3.97943264E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     2.98796163E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.25474401E-03    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     4.74035726E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     2.68237161E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.40999648E-03    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     2.09732712E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     4.30191547E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     8.46625540E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     8.46625540E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     8.92971240E-01   # ~g
#    BR                NDA      ID1      ID2
     1.73712555E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
     1.55668608E-03    2     1000035        21   # BR(~g -> chi^0_4 g)
#    BR                NDA      ID1      ID2       ID3
     2.03038863E-04    3     1000022         2        -2   # BR(~g -> chi^0_1 u u_bar)
     2.03038828E-04    3     1000022         4        -4   # BR(~g -> chi^0_1 c c_bar)
     1.51188049E-02    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     5.86466589E-03    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     3.40614248E-04    3     1000023         2        -2   # BR(~g -> chi^0_2 u u_bar)
     3.40614208E-04    3     1000023         4        -4   # BR(~g -> chi^0_2 c c_bar)
     6.79674937E-02    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     3.40171691E-04    3     1000023         1        -1   # BR(~g -> chi^0_2 d d_bar)
     3.40172027E-04    3     1000023         3        -3   # BR(~g -> chi^0_2 s s_bar)
     8.02221191E-02    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.11392402E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.35665036E-02    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     1.15020008E-01    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     1.35886527E-02    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     6.80725465E-04    3     1000024         1        -2   # BR(~g -> chi^+_1 d u_bar)
     6.80725465E-04    3    -1000024        -1         2   # BR(~g -> chi^-_1 d_bar u)
     6.80725766E-04    3     1000024         3        -4   # BR(~g -> chi^+_1 s c_bar)
     6.80725766E-04    3    -1000024        -3         4   # BR(~g -> chi^-_1 s_bar c)
     1.48086821E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     1.48086821E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.36573114E-01    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     1.36573114E-01    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.92794260E-03   # Gamma(h0)
     2.49968083E-03   2        22        22   # BR(h0 -> photon photon)
     1.83342089E-03   2        22        23   # BR(h0 -> photon Z)
     3.70553862E-02   2        23        23   # BR(h0 -> Z Z)
     2.91837895E-01   2       -24        24   # BR(h0 -> W W)
     7.58569779E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.58071880E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.03759716E-04   2       -13        13   # BR(h0 -> Muon muon)
     5.87529986E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.27836685E-07   2        -2         2   # BR(h0 -> Up up)
     2.48117160E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.27743485E-07   2        -1         1   # BR(h0 -> Down down)
     1.90872292E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.06570519E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     3.86113171E-04   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     7.98386517E+00   # Gamma(HH)
     7.32129253E-08   2        22        22   # BR(HH -> photon photon)
     2.09113754E-07   2        22        23   # BR(HH -> photon Z)
     2.53989275E-05   2        23        23   # BR(HH -> Z Z)
     1.75228235E-05   2       -24        24   # BR(HH -> W W)
     1.36027401E-05   2        21        21   # BR(HH -> gluon gluon)
     1.53386427E-08   2       -11        11   # BR(HH -> Electron electron)
     6.82824085E-04   2       -13        13   # BR(HH -> Muon muon)
     1.85488760E-01   2       -15        15   # BR(HH -> Tau tau)
     9.04143813E-13   2        -2         2   # BR(HH -> Up up)
     1.75374200E-07   2        -4         4   # BR(HH -> Charm charm)
     1.10649600E-02   2        -6         6   # BR(HH -> Top top)
     1.02473080E-06   2        -1         1   # BR(HH -> Down down)
     3.70606355E-04   2        -3         3   # BR(HH -> Strange strange)
     8.00107625E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.97882581E-04   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     2.72513280E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.37285462E-03   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     9.85764629E-05   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     2.43445313E-04   2        25        25   # BR(HH -> h0 h0)
     4.29336495E-06   2  -1000011   1000011   # BR(HH -> Selectron1 selectron1)
     1.73558028E-11   2  -2000011   1000011   # BR(HH -> Selectron2 selectron1)
     1.73558028E-11   2  -1000011   2000011   # BR(HH -> Selectron1 selectron2)
     1.90757679E-06   2  -2000011   2000011   # BR(HH -> Selectron2 selectron2)
     4.19032413E-06   2  -1000013   1000013   # BR(HH -> Smuon1 smuon1)
     7.41566067E-07   2  -2000013   1000013   # BR(HH -> Smuon2 smuon1)
     7.41566067E-07   2  -1000013   2000013   # BR(HH -> Smuon1 smuon2)
     1.95476260E-06   2  -2000013   2000013   # BR(HH -> Smuon2 smuon2)
DECAY        36     7.85600935E+00   # Gamma(A0)
     2.03081884E-07   2        22        22   # BR(A0 -> photon photon)
     7.61368519E-07   2        22        23   # BR(A0 -> photon Z)
     7.82151657E-05   2        21        21   # BR(A0 -> gluon gluon)
     1.52640502E-08   2       -11        11   # BR(A0 -> Electron electron)
     6.79503332E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.84570549E-01   2       -15        15   # BR(A0 -> Tau tau)
     9.15577881E-13   2        -2         2   # BR(A0 -> Up up)
     1.77573349E-07   2        -4         4   # BR(A0 -> Charm charm)
     1.17766140E-02   2        -6         6   # BR(A0 -> Top top)
     1.01961277E-06   2        -1         1   # BR(A0 -> Down down)
     3.68755725E-04   2        -3         3   # BR(A0 -> Strange strange)
     7.96124430E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     3.03614391E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     2.80483462E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.53480389E-03   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.51276734E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     3.40925391E-05   2        23        25   # BR(A0 -> Z h0)
     1.16864915E-34   2        25        25   # BR(A0 -> h0 h0)
     1.71369176E-11   2  -2000011   1000011   # BR(A0 -> Selectron2 selectron1)
     1.71369176E-11   2  -1000011   2000011   # BR(A0 -> Selectron1 selectron2)
     7.32661420E-07   2  -2000013   1000013   # BR(A0 -> Smuon2 smuon1)
     7.32661420E-07   2  -1000013   2000013   # BR(A0 -> Smuon1 smuon2)
DECAY        37     8.48277365E+00   # Gamma(Hp)
     1.46074663E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     6.24514805E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.76648024E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     9.76900513E-07   2        -1         2   # BR(Hp -> Down up)
     1.63719608E-05   2        -3         2   # BR(Hp -> Strange up)
     8.54219040E-06   2        -5         2   # BR(Hp -> Bottom up)
     5.63069088E-08   2        -1         4   # BR(Hp -> Down charm)
     3.52251346E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.19621168E-03   2        -5         4   # BR(Hp -> Bottom charm)
     1.18611937E-06   2        -1         6   # BR(Hp -> Down top)
     2.63711074E-05   2        -3         6   # BR(Hp -> Strange top)
     8.18334767E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.72596860E-03   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     2.40631641E-09   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     3.19104888E-05   2        24        25   # BR(Hp -> W h0)
     3.41393986E-10   2        24        35   # BR(Hp -> W HH)
     3.60188389E-10   2        24        36   # BR(Hp -> W A0)
     1.57490829E-05   2  -1000011   1000012   # BR(Hp -> Selectron1 snu_e1)
     3.43580666E-11   2  -2000011   1000012   # BR(Hp -> Selectron2 snu_e1)
     1.56122887E-05   2  -1000013   1000014   # BR(Hp -> Smuon1 snu_mu1)
     1.46859603E-06   2  -2000013   1000014   # BR(Hp -> Smuon2 snu_mu1)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.24867055E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    4.07789776E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    4.07714643E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00018428E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    2.26841756E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    2.45269582E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.24867055E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    4.07789776E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    4.07714643E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99996403E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    3.59748095E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99996403E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    3.59748095E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26873586E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    2.15702418E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.02465293E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    3.59748095E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99996403E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.41121304E-04   # BR(b -> s gamma)
    2    1.58832515E-06   # BR(b -> s mu+ mu-)
    3    3.52576550E-05   # BR(b -> s nu nu)
    4    2.70647198E-15   # BR(Bd -> e+ e-)
    5    1.15617530E-10   # BR(Bd -> mu+ mu-)
    6    2.42100438E-08   # BR(Bd -> tau+ tau-)
    7    9.07613907E-14   # BR(Bs -> e+ e-)
    8    3.87732784E-09   # BR(Bs -> mu+ mu-)
    9    8.22651124E-07   # BR(Bs -> tau+ tau-)
   10    9.56718744E-05   # BR(B_u -> tau nu)
   11    9.88253835E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42048299E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93621924E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15758984E-03   # epsilon_K
   17    2.28166203E-15   # Delta(M_K)
   18    2.48039789E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29139488E-11   # BR(K^+ -> pi^+ nu nu)
   20    1.85734431E-14   # Delta(g-2)_electron/2
   21    7.96570387E-10   # Delta(g-2)_muon/2
   22    2.55416327E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    1.48769087E-04   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.10228786E-01   # C7
     0305 4322   00   2    -3.65038813E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.19281008E-01   # C8
     0305 6321   00   2    -4.40308091E-04   # C8'
 03051111 4133   00   0     1.62752462E+00   # C9 e+e-
 03051111 4133   00   2     1.62761961E+00   # C9 e+e-
 03051111 4233   00   2     1.00022255E-04   # C9' e+e-
 03051111 4137   00   0    -4.45021672E+00   # C10 e+e-
 03051111 4137   00   2    -4.44899895E+00   # C10 e+e-
 03051111 4237   00   2    -7.39368109E-04   # C10' e+e-
 03051313 4133   00   0     1.62752462E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62761914E+00   # C9 mu+mu-
 03051313 4233   00   2     1.00022022E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.45021672E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44899942E+00   # C10 mu+mu-
 03051313 4237   00   2    -7.39368028E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50510278E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.59837990E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50510278E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.59838032E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50510281E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.59851249E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85780940E-07   # C7
     0305 4422   00   2     6.93195704E-06   # C7
     0305 4322   00   2    -4.58351401E-07   # C7'
     0305 6421   00   0     3.30446888E-07   # C8
     0305 6421   00   2    -5.94962829E-06   # C8
     0305 6321   00   2    -3.78187396E-07   # C8'
 03051111 4133   00   2    -7.12331431E-07   # C9 e+e-
 03051111 4233   00   2     2.29994311E-06   # C9' e+e-
 03051111 4137   00   2     3.10672899E-06   # C10 e+e-
 03051111 4237   00   2    -1.70031836E-05   # C10' e+e-
 03051313 4133   00   2    -7.12285675E-07   # C9 mu+mu-
 03051313 4233   00   2     2.29994237E-06   # C9' mu+mu-
 03051313 4137   00   2     3.10669008E-06   # C10 mu+mu-
 03051313 4237   00   2    -1.70031856E-05   # C10' mu+mu-
 03051212 4137   00   2    -3.85090000E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     3.67583808E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -3.85074779E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     3.67583808E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -3.84476767E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     3.67586624E-06   # C11' nu_3 nu_3
