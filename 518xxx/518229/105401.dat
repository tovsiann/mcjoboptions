# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 22.11.2021,  14:48
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    1.69916948E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    4.41558791E+03  # scale for input parameters
    1    4.99930512E+01  # M_1
    2    3.42397398E+02  # M_2
    3    4.38667295E+03  # M_3
   11    1.50603432E+03  # A_t
   12   -4.79728460E+02  # A_b
   13   -6.65725580E+02  # A_tau
   23   -7.74048128E+02  # mu
   25    1.61858355E+01  # tan(beta)
   26    4.08292274E+03  # m_A, pole mass
   31    1.29023911E+03  # M_L11
   32    1.29023911E+03  # M_L22
   33    7.24254949E+02  # M_L33
   34    6.93908662E+02  # M_E11
   35    6.93908662E+02  # M_E22
   36    9.68341241E+02  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.33465817E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.26559476E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.00040287E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  4.41558791E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  4.41558791E+03  # (SUSY scale)
  1  1     8.40035875E-06   # Y_u(Q)^DRbar
  2  2     4.26738224E-03   # Y_c(Q)^DRbar
  3  3     1.01483081E+00   # Y_t(Q)^DRbar
Block Yd Q=  4.41558791E+03  # (SUSY scale)
  1  1     2.73220451E-04   # Y_d(Q)^DRbar
  2  2     5.19118856E-03   # Y_s(Q)^DRbar
  3  3     2.70949072E-01   # Y_b(Q)^DRbar
Block Ye Q=  4.41558791E+03  # (SUSY scale)
  1  1     4.76789666E-05   # Y_e(Q)^DRbar
  2  2     9.85849811E-03   # Y_mu(Q)^DRbar
  3  3     1.65803720E-01   # Y_tau(Q)^DRbar
Block Au Q=  4.41558791E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     1.50603434E+03   # A_t(Q)^DRbar
Block Ad Q=  4.41558791E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -4.79728442E+02   # A_b(Q)^DRbar
Block Ae Q=  4.41558791E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -6.65725572E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  4.41558791E+03  # soft SUSY breaking masses at Q
   1    4.99930512E+01  # M_1
   2    3.42397398E+02  # M_2
   3    4.38667295E+03  # M_3
  21    1.60150633E+07  # M^2_(H,d)
  22   -8.78730201E+04  # M^2_(H,u)
  31    1.29023911E+03  # M_(L,11)
  32    1.29023911E+03  # M_(L,22)
  33    7.24254949E+02  # M_(L,33)
  34    6.93908662E+02  # M_(E,11)
  35    6.93908662E+02  # M_(E,22)
  36    9.68341241E+02  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.33465817E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.26559476E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.00040287E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.22787801E+02  # h0
        35     4.08237102E+03  # H0
        36     4.08292274E+03  # A0
        37     4.08347433E+03  # H+
   1000001     1.01069029E+04  # ~d_L
   2000001     1.00820816E+04  # ~d_R
   1000002     1.01065420E+04  # ~u_L
   2000002     1.00843509E+04  # ~u_R
   1000003     1.01069032E+04  # ~s_L
   2000003     1.00820818E+04  # ~s_R
   1000004     1.01065423E+04  # ~c_L
   2000004     1.00843512E+04  # ~c_R
   1000005     3.18424166E+03  # ~b_1
   2000005     4.46762974E+03  # ~b_2
   1000006     4.35912963E+03  # ~t_1
   2000006     4.47277743E+03  # ~t_2
   1000011     1.28995307E+03  # ~e_L-
   2000011     7.12098893E+02  # ~e_R-
   1000012     1.28714913E+03  # ~nu_eL
   1000013     1.28994699E+03  # ~mu_L-
   2000013     7.12075630E+02  # ~mu_R-
   1000014     1.28714196E+03  # ~nu_muL
   1000015     7.18137172E+02  # ~tau_1-
   2000015     9.77905427E+02  # ~tau_2-
   1000016     7.14049637E+02  # ~nu_tauL
   1000021     4.88169755E+03  # ~g
   1000022     4.96056727E+01  # ~chi_10
   1000023     3.68546976E+02  # ~chi_20
   1000025     7.80925311E+02  # ~chi_30
   1000035     7.84507068E+02  # ~chi_40
   1000024     3.68612160E+02  # ~chi_1+
   1000037     7.86401250E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -5.88615873E-02   # alpha
Block Hmix Q=  4.41558791E+03  # Higgs mixing parameters
   1   -7.74048128E+02  # mu
   2    1.61858355E+01  # tan[beta](Q)
   3    2.42856567E+02  # v(Q)
   4    1.66702581E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -1.76031677E-01   # Re[R_st(1,1)]
   1  2     9.84384502E-01   # Re[R_st(1,2)]
   2  1    -9.84384502E-01   # Re[R_st(2,1)]
   2  2    -1.76031677E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -2.81831228E-03   # Re[R_sb(1,1)]
   1  2     9.99996029E-01   # Re[R_sb(1,2)]
   2  1    -9.99996029E-01   # Re[R_sb(2,1)]
   2  2    -2.81831228E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -9.98850117E-01   # Re[R_sta(1,1)]
   1  2     4.79420870E-02   # Re[R_sta(1,2)]
   2  1    -4.79420870E-02   # Re[R_sta(2,1)]
   2  2    -9.98850117E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.98404192E-01   # Re[N(1,1)]
   1  2    -9.43053476E-04   # Re[N(1,2)]
   1  3     5.64633423E-02   # Re[N(1,3)]
   1  4     2.66221289E-04   # Re[N(1,4)]
   2  1    -6.19889722E-03   # Re[N(2,1)]
   2  2    -9.90636972E-01   # Re[N(2,2)]
   2  3    -1.25909587E-01   # Re[N(2,3)]
   2  4    -5.24093339E-02   # Re[N(2,4)]
   3  1     3.96651606E-02   # Re[N(3,1)]
   3  2    -5.22844899E-02   # Re[N(3,2)]
   3  3     7.03834719E-01   # Re[N(3,3)]
   3  4    -7.07325735E-01   # Re[N(3,4)]
   4  1    -3.97154625E-02   # Re[N(4,1)]
   4  2     1.26110394E-01   # Re[N(4,2)]
   4  3    -6.96832372E-01   # Re[N(4,3)]
   4  4    -7.04942193E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.84130306E-01   # Re[U(1,1)]
   1  2    -1.77447292E-01   # Re[U(1,2)]
   2  1     1.77447292E-01   # Re[U(2,1)]
   2  2    -9.84130306E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -9.97277154E-01   # Re[V(1,1)]
   1  2     7.37446802E-02   # Re[V(1,2)]
   2  1     7.37446802E-02   # Re[V(2,1)]
   2  2     9.97277154E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     3.53216848E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.99979135E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
DECAY   1000011     1.57903895E+01   # ~e^-_L
#    BR                NDA      ID1      ID2
     1.02349878E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.99605327E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.39524772E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     1.57089177E-03    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     5.87394149E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     8.94022819E-03    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     3.53209011E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.99969250E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
DECAY   1000013     1.57916109E+01   # ~mu^-_L
#    BR                NDA      ID1      ID2
     1.02341975E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.99582429E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.70890379E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     1.60112737E-03    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     5.87344967E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     8.93939493E-03    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.93405683E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.52386211E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     2.86756953E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     5.60856836E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000015     5.25064468E+00   # ~tau^-_2
#    BR                NDA      ID1      ID2
     9.25939317E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     6.98015645E-03    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     6.81057579E-03    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.31544418E-02    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
     2.43983090E-02    2     1000016       -24   # BR(~tau^-_2 -> ~nu_tau W^-)
     1.11066527E-02    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     1.15468307E-02    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
DECAY   1000012     1.58371037E+01   # ~nu_e
#    BR                NDA      ID1      ID2
     1.01107235E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.93885111E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     7.71135730E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.06732486E-03    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     5.99640924E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     1.52826971E-03    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
DECAY   1000014     1.58379998E+01   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.01100948E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.93866262E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     7.71077797E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.06709384E-03    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     5.99606786E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     1.58783257E-03    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
DECAY   1000016     5.90236929E+00   # ~nu_tau
#    BR                NDA      ID1      ID2
     1.49492726E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.79349764E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     5.71157510E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
DECAY   2000001     4.72033336E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.18858786E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.88076484E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.03437133E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.30547488E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     7.19936458E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     2.56577184E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     1.29193172E-03    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.42419121E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     4.58652150E-03    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.77146728E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.72044009E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.18856465E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.88054185E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.03446125E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.30546914E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     7.19927170E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     2.60956208E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     1.29620781E-03    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.42417029E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     4.59242229E-03    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.77135199E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     9.97657685E+00   # ~b_1
#    BR                NDA      ID1      ID2
     1.79158978E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     7.76247674E-03    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.04011326E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.99533263E-01    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     1.53097034E-02    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     3.94224253E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.50816222E+02   # ~b_2
#    BR                NDA      ID1      ID2
     4.20207677E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.26565887E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.05930095E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.19411737E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.51149606E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     5.74649242E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     4.16325989E-04    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.88807687E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     2.93870462E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     4.89156748E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.58895343E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.53965393E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.03419163E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.35489991E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     7.23148072E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.48959722E-04    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     1.03207038E-03    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.46248754E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     7.92165924E-04    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.77108343E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     4.89163967E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.58888567E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.53951315E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.03428123E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.35486492E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     7.23137487E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.51956211E-04    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     1.03503657E-03    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.46246868E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     8.00721346E-04    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.77096804E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.76088937E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.33078994E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     4.89310387E-04    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.34143138E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.44564718E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     9.36965042E-04    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     4.66481520E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.51565536E+02   # ~t_2
#    BR                NDA      ID1      ID2
     6.05546474E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.28143710E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.86803144E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.72825255E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.58342217E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     4.72996772E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     3.68634647E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.61896673E-04    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
DECAY   1000024     1.26834575E-03   # chi^+_1
#    BR                NDA      ID1      ID2
     9.89906765E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     9.03887514E-04    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
     6.35362204E-04    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     6.35498980E-04    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     7.91838125E-03    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     4.57646048E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     5.71267146E-04    2    -1000015        16   # BR(chi^+_2 -> ~tau^+_1 nu_tau)
     1.55043136E-03    2     1000016       -15   # BR(chi^+_2 -> ~nu_tau tau^+)
     1.11205802E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     3.01244911E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.94540011E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.87477468E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.89605209E-04    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     1.28892028E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     1.81968902E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.21359865E-03   # chi^0_2
#    BR                NDA      ID1      ID2
     6.93054489E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     2.97331483E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.34296592E-04    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.34432724E-04    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     4.14112115E-03    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     4.77440295E-03    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     6.26903461E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     3.98564432E-04    2     1000015       -15   # BR(chi^0_3 -> ~tau^-_1 tau^+)
     3.98564432E-04    2    -1000015        15   # BR(chi^0_3 -> ~tau^+_1 tau^-)
     2.32814927E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.32814927E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     4.08952523E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     2.03336021E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     4.65981328E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     2.38080805E-01    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.33121323E-04    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     2.03330547E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     2.03330547E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     4.64843685E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     6.73856700E-04    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     6.73856700E-04    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     2.37269312E-04    2     1000016       -16   # BR(chi^0_4 -> ~nu_tau nu_bar_tau)
     2.37269312E-04    2    -1000016        16   # BR(chi^0_4 -> ~nu^*_tau nu_tau)
     2.99165504E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.99165504E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     5.37783274E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     4.57103580E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     4.98028907E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     2.46384307E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
#    BR                NDA      ID1      ID2       ID3
     4.24106125E-04    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     1.79105749E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.79105749E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     6.07341176E+01   # ~g
#    BR                NDA      ID1      ID2
     5.19544965E-02    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     5.19544965E-02    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.37249355E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     2.37249355E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     3.92671827E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     3.92671827E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     3.13887505E-02    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     3.13887505E-02    2    -2000005         5   # BR(~g -> ~b^*_2 b)
     1.16296899E-04    2     1000025        21   # BR(~g -> chi^0_3 g)
     1.23039906E-04    2     1000035        21   # BR(~g -> chi^0_4 g)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.30861597E-03   # Gamma(h0)
     2.54174707E-03   2        22        22   # BR(h0 -> photon photon)
     1.43006316E-03   2        22        23   # BR(h0 -> photon Z)
     2.44527037E-02   2        23        23   # BR(h0 -> Z Z)
     2.11189300E-01   2       -24        24   # BR(h0 -> W W)
     8.07986460E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.22215929E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.32289742E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.69696802E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.49093836E-07   2        -2         2   # BR(h0 -> Up up)
     2.89341877E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.06598589E-07   2        -1         1   # BR(h0 -> Down down)
     2.19392094E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.83087977E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     1.43252144E-04   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     6.45622943E+01   # Gamma(HH)
     1.25777644E-08   2        22        22   # BR(HH -> photon photon)
     9.01256390E-08   2        22        23   # BR(HH -> photon Z)
     9.49620233E-07   2        23        23   # BR(HH -> Z Z)
     1.95903508E-07   2       -24        24   # BR(HH -> W W)
     3.77238004E-07   2        21        21   # BR(HH -> gluon gluon)
     2.45306186E-09   2       -11        11   # BR(HH -> Electron electron)
     1.09233333E-04   2       -13        13   # BR(HH -> Muon muon)
     3.07554264E-02   2       -15        15   # BR(HH -> Tau tau)
     5.03905838E-13   2        -2         2   # BR(HH -> Up up)
     9.77606548E-08   2        -4         4   # BR(HH -> Charm charm)
     7.61321264E-03   2        -6         6   # BR(HH -> Top top)
     1.83633890E-07   2        -1         1   # BR(HH -> Down down)
     6.64210653E-05   2        -3         3   # BR(HH -> Strange strange)
     1.84011843E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.55856615E-02   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     2.26950631E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     2.26950631E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     2.39589961E-03   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     4.50766991E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     3.78662610E-03   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     2.93613337E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     3.55998914E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     7.69993567E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.01286676E-01   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.15578478E-01   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     9.88622252E-04   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     5.30025895E-03   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     4.49035140E-03   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     5.54635595E-06   2        25        25   # BR(HH -> h0 h0)
     2.13838310E-07   2  -1000011   1000011   # BR(HH -> Selectron1 selectron1)
     1.62386755E-13   2  -2000011   1000011   # BR(HH -> Selectron2 selectron1)
     1.62386755E-13   2  -1000011   2000011   # BR(HH -> Selectron1 selectron2)
     2.72252992E-07   2  -2000011   2000011   # BR(HH -> Selectron2 selectron2)
     2.13276959E-07   2  -1000013   1000013   # BR(HH -> Smuon1 smuon1)
     6.94250280E-09   2  -2000013   1000013   # BR(HH -> Smuon2 smuon1)
     6.94250280E-09   2  -1000013   2000013   # BR(HH -> Smuon1 smuon2)
     2.72064455E-07   2  -2000013   2000013   # BR(HH -> Smuon2 smuon2)
     3.89830715E-06   2  -1000015   1000015   # BR(HH -> Stau1 stau1)
     4.97816192E-04   2  -2000015   1000015   # BR(HH -> Stau2 stau1)
     4.97816192E-04   2  -1000015   2000015   # BR(HH -> Stau1 stau2)
     7.28126211E-06   2  -2000015   2000015   # BR(HH -> Stau2 stau2)
DECAY        36     6.39079979E+01   # Gamma(A0)
     1.73376173E-09   2        22        22   # BR(A0 -> photon photon)
     6.20909496E-08   2        22        23   # BR(A0 -> photon Z)
     7.33373898E-06   2        21        21   # BR(A0 -> gluon gluon)
     2.36775821E-09   2       -11        11   # BR(A0 -> Electron electron)
     1.05433477E-04   2       -13        13   # BR(A0 -> Muon muon)
     2.96853272E-02   2       -15        15   # BR(A0 -> Tau tau)
     4.58953129E-13   2        -2         2   # BR(A0 -> Up up)
     8.90366588E-08   2        -4         4   # BR(A0 -> Charm charm)
     6.99184282E-03   2        -6         6   # BR(A0 -> Top top)
     1.77243551E-07   2        -1         1   # BR(A0 -> Down down)
     6.41099333E-05   2        -3         3   # BR(A0 -> Strange strange)
     1.77608616E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.47952090E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     2.30240697E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     2.30240697E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.52909002E-03   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     4.55087406E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     3.65161412E-03   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     3.73384080E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     2.84702926E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     7.29961057E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.21941841E-01   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     9.79575857E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     1.48168492E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     4.92760938E-03   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     4.18884657E-03   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     2.06945885E-06   2        23        25   # BR(A0 -> Z h0)
     2.83309061E-37   2        25        25   # BR(A0 -> h0 h0)
     1.64102948E-13   2  -2000011   1000011   # BR(A0 -> Selectron2 selectron1)
     1.64102948E-13   2  -1000011   2000011   # BR(A0 -> Selectron1 selectron2)
     7.01591371E-09   2  -2000013   1000013   # BR(A0 -> Smuon2 smuon1)
     7.01591371E-09   2  -1000013   2000013   # BR(A0 -> Smuon1 smuon2)
     5.08323603E-04   2  -2000015   1000015   # BR(A0 -> Stau2 stau1)
     5.08323603E-04   2  -1000015   2000015   # BR(A0 -> Stau1 stau2)
DECAY        37     6.63253545E+01   # Gamma(Hp)
     2.93957343E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     1.25675945E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     3.55483152E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     1.83340857E-07   2        -1         2   # BR(Hp -> Down up)
     3.10538265E-06   2        -3         2   # BR(Hp -> Strange up)
     2.11190118E-06   2        -5         2   # BR(Hp -> Bottom up)
     1.34426328E-08   2        -1         4   # BR(Hp -> Down charm)
     6.61774469E-05   2        -3         4   # BR(Hp -> Strange charm)
     2.95741934E-04   2        -5         4   # BR(Hp -> Bottom charm)
     5.32371378E-07   2        -1         6   # BR(Hp -> Down top)
     1.17059269E-05   2        -3         6   # BR(Hp -> Strange top)
     2.07464995E-01   2        -5         6   # BR(Hp -> Bottom top)
     7.18078332E-03   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     6.16520631E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     2.82480972E-07   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     2.33796888E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     2.23130770E-01   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     4.24591110E-03   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     2.24347656E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     1.14205689E-03   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     1.99551136E-06   2        24        25   # BR(Hp -> W h0)
     3.35638769E-14   2        24        35   # BR(Hp -> W HH)
     3.09519497E-13   2  -1000011   1000012   # BR(Hp -> Selectron1 snu_e1)
     1.04133289E-06   2  -2000011   1000012   # BR(Hp -> Selectron2 snu_e1)
     1.32329821E-08   2  -1000013   1000014   # BR(Hp -> Smuon1 snu_mu1)
     1.04107392E-06   2  -2000013   1000014   # BR(Hp -> Smuon2 snu_mu1)
     4.35862929E-07   2  -1000015   1000016   # BR(Hp -> Stau1 snu_tau1)
     9.80501370E-04   2  -2000015   1000016   # BR(Hp -> Stau2 snu_tau1)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.10095854E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.62071175E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.61981271E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00034317E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.47389663E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.81706676E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.10095854E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.62071175E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.61981271E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99991921E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    8.07920278E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99991921E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    8.07920278E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.27324568E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.02934670E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    9.67700355E-02        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    8.07920278E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99991921E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.24411170E-04   # BR(b -> s gamma)
    2    1.58957149E-06   # BR(b -> s mu+ mu-)
    3    3.52500131E-05   # BR(b -> s nu nu)
    4    2.51795027E-15   # BR(Bd -> e+ e-)
    5    1.07563988E-10   # BR(Bd -> mu+ mu-)
    6    2.25181948E-08   # BR(Bd -> tau+ tau-)
    7    8.50776688E-14   # BR(Bs -> e+ e-)
    8    3.63451563E-09   # BR(Bs -> mu+ mu-)
    9    7.70933051E-07   # BR(Bs -> tau+ tau-)
   10    9.67032852E-05   # BR(B_u -> tau nu)
   11    9.98907914E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41935034E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93631909E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15705096E-03   # epsilon_K
   17    2.28165547E-15   # Delta(M_K)
   18    2.47970239E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28980979E-11   # BR(K^+ -> pi^+ nu nu)
   20   -2.92110520E-15   # Delta(g-2)_electron/2
   21   -1.24894363E-10   # Delta(g-2)_muon/2
   22   -6.95880971E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    3.50753532E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.93263407E-01   # C7
     0305 4322   00   2    -8.71692474E-05   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.00623627E-01   # C8
     0305 6321   00   2    -1.06008693E-04   # C8'
 03051111 4133   00   0     1.62975557E+00   # C9 e+e-
 03051111 4133   00   2     1.62991475E+00   # C9 e+e-
 03051111 4233   00   2     6.14723844E-05   # C9' e+e-
 03051111 4137   00   0    -4.45244768E+00   # C10 e+e-
 03051111 4137   00   2    -4.45048492E+00   # C10 e+e-
 03051111 4237   00   2    -4.52781877E-04   # C10' e+e-
 03051313 4133   00   0     1.62975557E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62991455E+00   # C9 mu+mu-
 03051313 4233   00   2     6.14723642E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.45244768E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.45048512E+00   # C10 mu+mu-
 03051313 4237   00   2    -4.52781916E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50493700E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     9.78304065E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50493700E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     9.78304072E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50493759E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     9.78298651E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85825096E-07   # C7
     0305 4422   00   2    -2.46297902E-06   # C7
     0305 4322   00   2    -9.83188968E-08   # C7'
     0305 6421   00   0     3.30484710E-07   # C8
     0305 6421   00   2     2.09296448E-06   # C8
     0305 6321   00   2     2.32999223E-08   # C8'
 03051111 4133   00   2    -7.58096058E-08   # C9 e+e-
 03051111 4233   00   2     1.17933318E-06   # C9' e+e-
 03051111 4137   00   2     8.27409608E-07   # C10 e+e-
 03051111 4237   00   2    -8.68703286E-06   # C10' e+e-
 03051313 4133   00   2    -7.58189974E-08   # C9 mu+mu-
 03051313 4233   00   2     1.17933288E-06   # C9' mu+mu-
 03051313 4137   00   2     8.27420142E-07   # C10 mu+mu-
 03051313 4237   00   2    -8.68703362E-06   # C10' mu+mu-
 03051212 4137   00   2     1.84433616E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     1.87697975E-06   # C11' nu_1 nu_1
 03051414 4137   00   2     1.84438039E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     1.87697975E-06   # C11' nu_2 nu_2
 03051616 4137   00   2     2.89209594E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     1.87696650E-06   # C11' nu_3 nu_3
