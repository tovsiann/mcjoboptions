# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 18.08.2022,  12:29
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.70463103E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.67648352E+03  # scale for input parameters
    1    1.59776004E+02  # M_1
    2    1.69087138E+03  # M_2
    3    2.30489637E+03  # M_3
   11   -6.61499138E+03  # A_t
   12    1.75263950E+03  # A_b
   13   -1.46986798E+03  # A_tau
   23    1.56278250E+03  # mu
   25    2.60445110E+01  # tan(beta)
   26    1.14430271E+03  # m_A, pole mass
   31    9.57838954E+02  # M_L11
   32    9.57838954E+02  # M_L22
   33    1.86244012E+03  # M_L33
   34    4.78934027E+02  # M_E11
   35    4.78934027E+02  # M_E22
   36    1.23229487E+03  # M_E33
   41    3.22308908E+03  # M_Q11
   42    3.22308908E+03  # M_Q22
   43    2.44801468E+03  # M_Q33
   44    3.29887385E+03  # M_U11
   45    3.29887385E+03  # M_U22
   46    3.01223200E+03  # M_U33
   47    3.87866365E+03  # M_D11
   48    3.87866365E+03  # M_D22
   49    8.78848848E+02  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.67648352E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.67648352E+03  # (SUSY scale)
  1  1     8.39055014E-06   # Y_u(Q)^DRbar
  2  2     4.26239947E-03   # Y_c(Q)^DRbar
  3  3     1.01364585E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.67648352E+03  # (SUSY scale)
  1  1     4.39123715E-04   # Y_d(Q)^DRbar
  2  2     8.34335059E-03   # Y_s(Q)^DRbar
  3  3     4.35473124E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.67648352E+03  # (SUSY scale)
  1  1     7.66302994E-05   # Y_e(Q)^DRbar
  2  2     1.58447155E-02   # Y_mu(Q)^DRbar
  3  3     2.66482049E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.67648352E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -6.61499278E+03   # A_t(Q)^DRbar
Block Ad Q=  2.67648352E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.75263867E+03   # A_b(Q)^DRbar
Block Ae Q=  2.67648352E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.46986793E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  2.67648352E+03  # soft SUSY breaking masses at Q
   1    1.59776004E+02  # M_1
   2    1.69087138E+03  # M_2
   3    2.30489637E+03  # M_3
  21   -1.16958694E+06  # M^2_(H,d)
  22   -2.35549856E+06  # M^2_(H,u)
  31    9.57838954E+02  # M_(L,11)
  32    9.57838954E+02  # M_(L,22)
  33    1.86244012E+03  # M_(L,33)
  34    4.78934027E+02  # M_(E,11)
  35    4.78934027E+02  # M_(E,22)
  36    1.23229487E+03  # M_(E,33)
  41    3.22308908E+03  # M_(Q,11)
  42    3.22308908E+03  # M_(Q,22)
  43    2.44801468E+03  # M_(Q,33)
  44    3.29887385E+03  # M_(U,11)
  45    3.29887385E+03  # M_(U,22)
  46    3.01223200E+03  # M_(U,33)
  47    3.87866365E+03  # M_(D,11)
  48    3.87866365E+03  # M_(D,22)
  49    8.78848848E+02  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.25340794E+02  # h0
        35     1.14430150E+03  # H0
        36     1.14430271E+03  # A0
        37     1.14755224E+03  # H+
   1000001     3.30384480E+03  # ~d_L
   2000001     3.93700157E+03  # ~d_R
   1000002     3.30300838E+03  # ~u_L
   2000002     3.36450611E+03  # ~u_R
   1000003     3.30384351E+03  # ~s_L
   2000003     3.93699947E+03  # ~s_R
   1000004     3.30300723E+03  # ~c_L
   2000004     3.36450564E+03  # ~c_R
   1000005     1.04736428E+03  # ~b_1
   2000005     2.49043135E+03  # ~b_2
   1000006     2.42844804E+03  # ~t_1
   2000006     2.94985272E+03  # ~t_2
   1000011     9.83608556E+02  # ~e_L-
   2000011     4.97267765E+02  # ~e_R-
   1000012     9.80118350E+02  # ~nu_eL
   1000013     9.83615987E+02  # ~mu_L-
   2000013     4.97228893E+02  # ~mu_R-
   1000014     9.80112066E+02  # ~nu_muL
   1000015     1.23538912E+03  # ~tau_1-
   2000015     1.87641617E+03  # ~tau_2-
   1000016     1.87357052E+03  # ~nu_tauL
   1000021     2.54715945E+03  # ~g
   1000022     1.57202485E+02  # ~chi_10
   1000023     1.55150786E+03  # ~chi_20
   1000025     1.57276846E+03  # ~chi_30
   1000035     1.73695766E+03  # ~chi_40
   1000024     1.55220677E+03  # ~chi_1+
   1000037     1.73723170E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -3.77379110E-02   # alpha
Block Hmix Q=  2.67648352E+03  # Higgs mixing parameters
   1    1.56278250E+03  # mu
   2    2.60445110E+01  # tan[beta](Q)
   3    2.43328916E+02  # v(Q)
   4    1.30942869E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.45450950E-01   # Re[R_st(1,1)]
   1  2     3.25764487E-01   # Re[R_st(1,2)]
   2  1    -3.25764487E-01   # Re[R_st(2,1)]
   2  2     9.45450950E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     1.54285718E-02   # Re[R_sb(1,1)]
   1  2     9.99880973E-01   # Re[R_sb(1,2)]
   2  1    -9.99880973E-01   # Re[R_sb(2,1)]
   2  2     1.54285718E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     3.81831410E-02   # Re[R_sta(1,1)]
   1  2     9.99270758E-01   # Re[R_sta(1,2)]
   2  1    -9.99270758E-01   # Re[R_sta(2,1)]
   2  2     3.81831410E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.99583639E-01   # Re[N(1,1)]
   1  2    -2.74898678E-04   # Re[N(1,2)]
   1  3     2.85598612E-02   # Re[N(1,3)]
   1  4    -4.09964151E-03   # Re[N(1,4)]
   2  1     2.19587819E-02   # Re[N(2,1)]
   2  2     3.33938435E-01   # Re[N(2,2)]
   2  3    -6.70250353E-01   # Re[N(2,3)]
   2  4     6.62395198E-01   # Re[N(2,4)]
   3  1    -1.72815669E-02   # Re[N(3,1)]
   3  2     1.62654667E-02   # Re[N(3,2)]
   3  3     7.06531817E-01   # Re[N(3,3)]
   3  4     7.07283234E-01   # Re[N(3,4)]
   4  1     7.19080264E-03   # Re[N(4,1)]
   4  2    -9.42454498E-01   # Re[N(4,2)]
   4  3    -2.25303329E-01   # Re[N(4,3)]
   4  4     2.46913390E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -3.16061617E-01   # Re[U(1,1)]
   1  2     9.48738665E-01   # Re[U(1,2)]
   2  1     9.48738665E-01   # Re[U(2,1)]
   2  2     3.16061617E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -3.46754502E-01   # Re[V(1,1)]
   1  2     9.37955924E-01   # Re[V(1,2)]
   2  1     9.37955924E-01   # Re[V(2,1)]
   2  2     3.46754502E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     2.02244285E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000011     1.17107065E+00   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.99999911E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     2.02221540E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
DECAY   1000013     1.17552217E+00   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.96224723E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.77527673E-03    2     2000013        25   # BR(~mu^-_L -> ~mu^-_R h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     6.00125529E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
DECAY   2000015     4.38171429E+00   # ~tau^-_2
#    BR                NDA      ID1      ID2
     5.30993452E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     4.77016880E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     2.68757037E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     3.50605103E-02    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     3.41149670E-02    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     6.98477655E-02    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
     1.31523773E-01    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     1.23882141E-01    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.16888318E+00   # ~nu_e
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
DECAY   1000014     1.16887490E+00   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
DECAY   1000016     4.41952079E+00   # ~nu_tau
#    BR                NDA      ID1      ID2
     5.25155714E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.91298334E-02    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     1.05802346E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     3.29910756E-02    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     9.66761804E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     6.59635485E-02    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
     2.59977845E-01    2     1000015        24   # BR(~nu_tau -> ~tau^-_1 W^+)
DECAY   1000001     6.65785841E+01   # ~d_L
#    BR                NDA      ID1      ID2
     6.91030204E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.44540629E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.01855533E-01    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     2.64995507E-02    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     2.05827296E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     6.44404320E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000001     1.07241972E+02   # ~d_R
#    BR                NDA      ID1      ID2
     2.04133888E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.79574347E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000003     6.65818857E+01   # ~s_L
#    BR                NDA      ID1      ID2
     6.91001200E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.44719520E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.01852288E-01    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     2.65072620E-02    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     2.05818471E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     6.44370615E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   2000003     1.07249563E+02   # ~s_R
#    BR                NDA      ID1      ID2
     2.04119740E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.79502978E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000005     5.62985176E-01   # ~b_1
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
DECAY   2000005     3.49261787E+01   # ~b_2
#    BR                NDA      ID1      ID2
     9.98390893E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     5.71521597E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     4.86496990E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     7.79644761E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     4.59853094E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     2.19459070E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.98302767E-02    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     3.37646933E-02    2     1000005        36   # BR(~b_2 -> ~b_1 A^0)
     3.96490145E-02    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
     3.36936074E-02    2     1000005        35   # BR(~b_2 -> ~b_1 H^0)
DECAY   1000002     6.65437668E+01   # ~u_L
#    BR                NDA      ID1      ID2
     6.86958292E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.51468995E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     1.01291608E-01    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     3.18957729E-02    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     2.01151710E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     6.43621706E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000002     5.58653528E+01   # ~u_R
#    BR                NDA      ID1      ID2
     1.33795260E-01    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     8.66136265E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000004     6.65469889E+01   # ~c_L
#    BR                NDA      ID1      ID2
     6.86924582E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.51506501E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     1.01287416E-01    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     3.19314797E-02    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     2.01145544E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     6.43587590E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   2000004     5.58666977E+01   # ~c_R
#    BR                NDA      ID1      ID2
     1.33791974E-01    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     8.66112991E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000006     3.14654903E+01   # ~t_1
#    BR                NDA      ID1      ID2
     2.85923206E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.42030838E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.34386883E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     7.10904402E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.30721744E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     8.91517320E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     3.53299111E-02    2     1000005        24   # BR(~t_1 -> ~b_1 W^+)
     6.71959269E-02    2     1000005        37   # BR(~t_1 -> ~b_1 H^+)
#    BR                NDA      ID1      ID2       ID3
     1.49940693E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.75444974E+02   # ~t_2
#    BR                NDA      ID1      ID2
     3.31108240E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     7.14135547E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     9.05830726E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     2.23120245E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.21935353E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     4.12372885E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     5.73914556E-02    2     1000021         6   # BR(~t_2 -> ~g t)
     1.67299323E-03    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     2.38784901E-01    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     7.09489099E-04    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     1.50770839E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.70078204E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     4.71304751E+00   # chi^+_1
#    BR                NDA      ID1      ID2
     5.18611766E-02    2    -1000011        12   # BR(chi^+_1 -> ~e^+_L nu_e)
     5.96165704E-04    2    -2000013        14   # BR(chi^+_1 -> ~mu^+_R nu_mu)
     5.18601250E-02    2    -1000013        14   # BR(chi^+_1 -> ~mu^+_L nu_mu)
     2.99084684E-02    2    -1000015        16   # BR(chi^+_1 -> ~tau^+_1 nu_tau)
     6.30176383E-02    2     1000012       -11   # BR(chi^+_1 -> ~nu_e e^+)
     6.32882233E-02    2     1000014       -13   # BR(chi^+_1 -> ~nu_mu mu^+)
     4.98839820E-01    2    -1000005         6   # BR(chi^+_1 -> ~b^*_1 t)
     1.97951387E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
     3.65498042E-02    2          37   1000022   # BR(chi^+_1 -> H^+ chi^0_1)
#    BR                NDA      ID1      ID2       ID3
     6.10919729E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     1.73209702E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.83427925E-01    2    -1000011        12   # BR(chi^+_2 -> ~e^+_L nu_e)
     1.83425310E-01    2    -1000013        14   # BR(chi^+_2 -> ~mu^+_L nu_mu)
     8.79478969E-04    2    -1000015        16   # BR(chi^+_2 -> ~tau^+_1 nu_tau)
     1.80482690E-01    2     1000012       -11   # BR(chi^+_2 -> ~nu_e e^+)
     1.80495961E-01    2     1000014       -13   # BR(chi^+_2 -> ~nu_mu mu^+)
     2.00024162E-02    2    -1000005         6   # BR(chi^+_2 -> ~b^*_1 t)
     7.64081187E-03    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     6.34854926E-02    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     8.56535219E-02    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     1.12635648E-03    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     5.45914312E-02    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     3.84418020E-02    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.59521470E-04    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     4.69672177E+00   # chi^0_2
#    BR                NDA      ID1      ID2
     3.22189569E-04    2     2000011       -11   # BR(chi^0_2 -> ~e^-_R e^+)
     3.22189569E-04    2    -2000011        11   # BR(chi^0_2 -> ~e^+_R e^-)
     3.10725193E-02    2     1000011       -11   # BR(chi^0_2 -> ~e^-_L e^+)
     3.10725193E-02    2    -1000011        11   # BR(chi^0_2 -> ~e^+_L e^-)
     6.20480039E-04    2     2000013       -13   # BR(chi^0_2 -> ~mu^-_R mu^+)
     6.20480039E-04    2    -2000013        13   # BR(chi^0_2 -> ~mu^+_R mu^-)
     3.12053703E-02    2     1000013       -13   # BR(chi^0_2 -> ~mu^-_L mu^+)
     3.12053703E-02    2    -1000013        13   # BR(chi^0_2 -> ~mu^+_L mu^-)
     1.50161196E-02    2     1000015       -15   # BR(chi^0_2 -> ~tau^-_1 tau^+)
     1.50161196E-02    2    -1000015        15   # BR(chi^0_2 -> ~tau^+_1 tau^-)
     2.72553425E-02    2     1000012       -12   # BR(chi^0_2 -> ~nu_e nu_bar_e)
     2.72553425E-02    2    -1000012        12   # BR(chi^0_2 -> ~nu^*_e nu_e)
     2.72558067E-02    2     1000014       -14   # BR(chi^0_2 -> ~nu_mu nu_bar_mu)
     2.72558067E-02    2    -1000014        14   # BR(chi^0_2 -> ~nu^*_mu nu_mu)
     2.52844388E-01    2     1000005        -5   # BR(chi^0_2 -> ~b_1 b_bar)
     2.52844388E-01    2    -1000005         5   # BR(chi^0_2 -> ~b^*_1 b)
     7.09200854E-02    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     1.16639014E-02    2     1000022        36   # BR(chi^0_2 -> chi^0_1 A^0)
     1.17285819E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
     2.55414089E-02    2     1000022        35   # BR(chi^0_2 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     3.38457011E-03    3     1000022         6        -6   # BR(chi^0_2 -> chi^0_1 t t_bar)
DECAY   1000025     4.38278189E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     2.18117572E-04    2     2000011       -11   # BR(chi^0_3 -> ~e^-_R e^+)
     2.18117572E-04    2    -2000011        11   # BR(chi^0_3 -> ~e^+_R e^-)
     5.80426418E-04    2     2000013       -13   # BR(chi^0_3 -> ~mu^-_R mu^+)
     5.80426418E-04    2    -2000013        13   # BR(chi^0_3 -> ~mu^+_R mu^-)
     1.80303799E-04    2     1000013       -13   # BR(chi^0_3 -> ~mu^-_L mu^+)
     1.80303799E-04    2    -1000013        13   # BR(chi^0_3 -> ~mu^+_L mu^-)
     1.85097343E-02    2     1000015       -15   # BR(chi^0_3 -> ~tau^-_1 tau^+)
     1.85097343E-02    2    -1000015        15   # BR(chi^0_3 -> ~tau^+_1 tau^-)
     1.92082873E-04    2     1000012       -12   # BR(chi^0_3 -> ~nu_e nu_bar_e)
     1.92082873E-04    2    -1000012        12   # BR(chi^0_3 -> ~nu^*_e nu_e)
     1.92086001E-04    2     1000014       -14   # BR(chi^0_3 -> ~nu_mu nu_bar_mu)
     1.92086001E-04    2    -1000014        14   # BR(chi^0_3 -> ~nu^*_mu nu_mu)
     3.13656361E-01    2     1000005        -5   # BR(chi^0_3 -> ~b_1 b_bar)
     3.13656361E-01    2    -1000005         5   # BR(chi^0_3 -> ~b^*_1 b)
     1.56376750E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     1.26964991E-02    2     1000022        36   # BR(chi^0_3 -> chi^0_1 A^0)
     1.23524881E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     3.58668288E-02    2     1000022        35   # BR(chi^0_3 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     4.42408153E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
DECAY   1000035     1.80217864E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     8.62366616E-02    2     1000011       -11   # BR(chi^0_4 -> ~e^-_L e^+)
     8.62366616E-02    2    -1000011        11   # BR(chi^0_4 -> ~e^+_L e^-)
     8.62408193E-02    2     1000013       -13   # BR(chi^0_4 -> ~mu^-_L mu^+)
     8.62408193E-02    2    -1000013        13   # BR(chi^0_4 -> ~mu^+_L mu^-)
     4.35920235E-04    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     4.35920235E-04    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     8.82414331E-02    2     1000012       -12   # BR(chi^0_4 -> ~nu_e nu_bar_e)
     8.82414331E-02    2    -1000012        12   # BR(chi^0_4 -> ~nu^*_e nu_e)
     8.82424902E-02    2     1000014       -14   # BR(chi^0_4 -> ~nu_mu nu_bar_mu)
     8.82424902E-02    2    -1000014        14   # BR(chi^0_4 -> ~nu^*_mu nu_mu)
     9.70060758E-03    2     1000005        -5   # BR(chi^0_4 -> ~b_1 b_bar)
     9.70060758E-03    2    -1000005         5   # BR(chi^0_4 -> ~b^*_1 b)
     5.69107924E-02    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     5.69107924E-02    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     2.84196260E-03    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.65899892E-04    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     8.65187890E-02    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     4.38132332E-04    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     4.28226698E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     3.64704700E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     3.62896954E-02    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
     6.82863197E-04    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     1.49632264E-04    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
DECAY   1000021     5.22910998E+01   # ~g
#    BR                NDA      ID1      ID2
     4.97689698E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     4.97689698E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.39813544E-03    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     1.39813544E-03    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
     1.57543048E-04    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     4.03469832E-04    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     4.31394405E-04    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.35032566E-04    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     1.33275312E-04    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     1.33275312E-04    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     4.00439989E-03   # Gamma(h0)
     2.29585381E-03   2        22        22   # BR(h0 -> photon photon)
     1.49654470E-03   2        22        23   # BR(h0 -> photon Z)
     2.79573756E-02   2        23        23   # BR(h0 -> Z Z)
     2.29400719E-01   2       -24        24   # BR(h0 -> W W)
     7.03232877E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.17874229E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.30359897E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.63666483E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.40950213E-07   2        -2         2   # BR(h0 -> Up up)
     2.73520711E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.96062202E-07   2        -1         1   # BR(h0 -> Down down)
     2.15583814E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.74360813E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.09174334E+01   # Gamma(HH)
     3.55107081E-08   2        22        22   # BR(HH -> photon photon)
     2.91141319E-08   2        22        23   # BR(HH -> photon Z)
     1.86237158E-05   2        23        23   # BR(HH -> Z Z)
     2.13265751E-05   2       -24        24   # BR(HH -> W W)
     3.34150507E-05   2        21        21   # BR(HH -> gluon gluon)
     1.42739939E-08   2       -11        11   # BR(HH -> Electron electron)
     6.35357324E-04   2       -13        13   # BR(HH -> Muon muon)
     1.77084534E-01   2       -15        15   # BR(HH -> Tau tau)
     4.21239136E-13   2        -2         2   # BR(HH -> Up up)
     8.16875824E-08   2        -4         4   # BR(HH -> Charm charm)
     5.04489924E-03   2        -6         6   # BR(HH -> Top top)
     9.31842203E-07   2        -1         1   # BR(HH -> Down down)
     3.37113886E-04   2        -3         3   # BR(HH -> Strange strange)
     8.16499618E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.67969625E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.53969107E-04   2        25        25   # BR(HH -> h0 h0)
     1.05107397E-06   2  -1000011   1000011   # BR(HH -> Selectron1 selectron1)
     1.03014879E-06   2  -1000013   1000013   # BR(HH -> Smuon1 smuon1)
DECAY        36     1.06819095E+01   # Gamma(A0)
     9.50467363E-08   2        22        22   # BR(A0 -> photon photon)
     2.57855546E-08   2        22        23   # BR(A0 -> photon Z)
     8.57608674E-05   2        21        21   # BR(A0 -> gluon gluon)
     1.42779263E-08   2       -11        11   # BR(A0 -> Electron electron)
     6.35532390E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.77133486E-01   2       -15        15   # BR(A0 -> Tau tau)
     3.65004759E-13   2        -2         2   # BR(A0 -> Up up)
     7.07904453E-08   2        -4         4   # BR(A0 -> Charm charm)
     4.80564177E-03   2        -6         6   # BR(A0 -> Top top)
     9.32102962E-07   2        -1         1   # BR(A0 -> Down down)
     3.37208056E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.16784618E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.90058180E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     2.65562409E-05   2        23        25   # BR(A0 -> Z h0)
     1.01355329E-34   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.03586033E+01   # Gamma(Hp)
     1.34013315E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     5.72948769E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.62061831E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     8.67803526E-07   2        -1         2   # BR(Hp -> Down up)
     1.44795617E-05   2        -3         2   # BR(Hp -> Strange up)
     9.02946718E-06   2        -5         2   # BR(Hp -> Bottom up)
     4.40202074E-08   2        -1         4   # BR(Hp -> Down charm)
     3.12941634E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.26444594E-03   2        -5         4   # BR(Hp -> Bottom charm)
     4.18029501E-07   2        -1         6   # BR(Hp -> Down top)
     9.55437007E-06   2        -3         6   # BR(Hp -> Strange top)
     8.35725676E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.77498937E-05   2        24        25   # BR(Hp -> W h0)
     4.75716843E-11   2        24        35   # BR(Hp -> W HH)
     4.74836170E-11   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.66989443E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    6.78349564E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    6.78316553E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00004867E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.42557253E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.47423794E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.66989443E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    6.78349564E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    6.78316553E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999592E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    4.08376929E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999592E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    4.08376929E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.25088897E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    6.74962048E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.15428943E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    4.08376929E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999592E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.26922774E-04   # BR(b -> s gamma)
    2    1.59129160E-06   # BR(b -> s mu+ mu-)
    3    3.52908440E-05   # BR(b -> s nu nu)
    4    2.45124182E-15   # BR(Bd -> e+ e-)
    5    1.04713724E-10   # BR(Bd -> mu+ mu-)
    6    2.18881689E-08   # BR(Bd -> tau+ tau-)
    7    8.12717127E-14   # BR(Bs -> e+ e-)
    8    3.47190900E-09   # BR(Bs -> mu+ mu-)
    9    7.35449588E-07   # BR(Bs -> tau+ tau-)
   10    9.44146546E-05   # BR(B_u -> tau nu)
   11    9.75267236E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41853360E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93479759E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15697395E-03   # epsilon_K
   17    2.28165847E-15   # Delta(M_K)
   18    2.48278709E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29669795E-11   # BR(K^+ -> pi^+ nu nu)
   20    6.99668545E-15   # Delta(g-2)_electron/2
   21    2.99163434E-10   # Delta(g-2)_muon/2
   22    2.28508464E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.99734346E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.91206749E-01   # C7
     0305 4322   00   2    -1.57503202E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.18398786E-01   # C8
     0305 6321   00   2    -4.96244719E-04   # C8'
 03051111 4133   00   0     1.61246224E+00   # C9 e+e-
 03051111 4133   00   2     1.61317235E+00   # C9 e+e-
 03051111 4233   00   2     1.67664910E-04   # C9' e+e-
 03051111 4137   00   0    -4.43515434E+00   # C10 e+e-
 03051111 4137   00   2    -4.43724166E+00   # C10 e+e-
 03051111 4237   00   2    -1.26565380E-03   # C10' e+e-
 03051313 4133   00   0     1.61246224E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61317125E+00   # C9 mu+mu-
 03051313 4233   00   2     1.67662886E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.43515434E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43724275E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.26565258E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50581523E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     2.74531331E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50581523E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     2.74531724E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50581631E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     2.74644173E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85884404E-07   # C7
     0305 4422   00   2     5.14874127E-05   # C7
     0305 4322   00   2     7.31961385E-06   # C7'
     0305 6421   00   0     3.30535512E-07   # C8
     0305 6421   00   2    -6.21701752E-05   # C8
     0305 6321   00   2     3.24828937E-06   # C8'
 03051111 4133   00   2    -9.84566180E-07   # C9 e+e-
 03051111 4233   00   2     4.39538558E-06   # C9' e+e-
 03051111 4137   00   2     1.04297620E-05   # C10 e+e-
 03051111 4237   00   2    -3.31952095E-05   # C10' e+e-
 03051313 4133   00   2    -9.84582848E-07   # C9 mu+mu-
 03051313 4233   00   2     4.39538187E-06   # C9' mu+mu-
 03051313 4137   00   2     1.04297276E-05   # C10 mu+mu-
 03051313 4237   00   2    -3.31952200E-05   # C10' mu+mu-
 03051212 4137   00   2    -2.04449271E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     7.20056918E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -2.04449269E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     7.20056918E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -2.02528140E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     7.20059805E-06   # C11' nu_3 nu_3
