# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 18.08.2022,  09:49
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.67386761E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    1.91068497E+03  # scale for input parameters
    1   -7.21306787E+02  # M_1
    2   -8.92566081E+02  # M_2
    3    3.54045600E+03  # M_3
   11   -4.35823268E+03  # A_t
   12    3.99699510E+03  # A_b
   13   -1.26255553E+03  # A_tau
   23    3.90489715E+03  # mu
   25    3.56706287E+01  # tan(beta)
   26    1.40116540E+03  # m_A, pole mass
   31    1.57684461E+03  # M_L11
   32    1.57684461E+03  # M_L22
   33    1.31713404E+03  # M_L33
   34    8.69460355E+02  # M_E11
   35    8.69460355E+02  # M_E22
   36    1.72943044E+03  # M_E33
   41    2.78115668E+03  # M_Q11
   42    2.78115668E+03  # M_Q22
   43    3.44682608E+03  # M_Q33
   44    2.37260257E+03  # M_U11
   45    2.37260257E+03  # M_U22
   46    1.06408067E+03  # M_U33
   47    2.63871980E+03  # M_D11
   48    2.63871980E+03  # M_D22
   49    3.00622616E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  1.91068497E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  1.91068497E+03  # (SUSY scale)
  1  1     8.38766621E-06   # Y_u(Q)^DRbar
  2  2     4.26093443E-03   # Y_c(Q)^DRbar
  3  3     1.01329745E+00   # Y_t(Q)^DRbar
Block Yd Q=  1.91068497E+03  # (SUSY scale)
  1  1     6.01218244E-04   # Y_d(Q)^DRbar
  2  2     1.14231466E-02   # Y_s(Q)^DRbar
  3  3     5.96220103E-01   # Y_b(Q)^DRbar
Block Ye Q=  1.91068497E+03  # (SUSY scale)
  1  1     1.04916980E-04   # Y_e(Q)^DRbar
  2  2     2.16935039E-02   # Y_mu(Q)^DRbar
  3  3     3.64849049E-01   # Y_tau(Q)^DRbar
Block Au Q=  1.91068497E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -4.35823264E+03   # A_t(Q)^DRbar
Block Ad Q=  1.91068497E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     3.99699521E+03   # A_b(Q)^DRbar
Block Ae Q=  1.91068497E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.26255553E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  1.91068497E+03  # soft SUSY breaking masses at Q
   1   -7.21306787E+02  # M_1
   2   -8.92566081E+02  # M_2
   3    3.54045600E+03  # M_3
  21   -1.33255400E+07  # M^2_(H,d)
  22   -1.53330770E+07  # M^2_(H,u)
  31    1.57684461E+03  # M_(L,11)
  32    1.57684461E+03  # M_(L,22)
  33    1.31713404E+03  # M_(L,33)
  34    8.69460355E+02  # M_(E,11)
  35    8.69460355E+02  # M_(E,22)
  36    1.72943044E+03  # M_(E,33)
  41    2.78115668E+03  # M_(Q,11)
  42    2.78115668E+03  # M_(Q,22)
  43    3.44682608E+03  # M_(Q,33)
  44    2.37260257E+03  # M_(U,11)
  45    2.37260257E+03  # M_(U,22)
  46    1.06408067E+03  # M_(U,33)
  47    2.63871980E+03  # M_(D,11)
  48    2.63871980E+03  # M_(D,22)
  49    3.00622616E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.23615710E+02  # h0
        35     1.40052630E+03  # H0
        36     1.40116540E+03  # A0
        37     1.40364196E+03  # H+
   1000001     2.84587366E+03  # ~d_L
   2000001     2.69500660E+03  # ~d_R
   1000002     2.84485151E+03  # ~u_L
   2000002     2.42313092E+03  # ~u_R
   1000003     2.84587539E+03  # ~s_L
   2000003     2.69499973E+03  # ~s_R
   1000004     2.84484936E+03  # ~c_L
   2000004     2.42313062E+03  # ~c_R
   1000005     3.05716270E+03  # ~b_1
   2000005     3.47211351E+03  # ~b_2
   1000006     1.05007239E+03  # ~t_1
   2000006     3.47663371E+03  # ~t_2
   1000011     1.58189713E+03  # ~e_L-
   2000011     8.81741568E+02  # ~e_R-
   1000012     1.57959926E+03  # ~nu_eL
   1000013     1.58187276E+03  # ~mu_L-
   2000013     8.81493509E+02  # ~mu_R-
   1000014     1.57952976E+03  # ~nu_muL
   1000015     1.28748614E+03  # ~tau_1-
   2000015     1.71732714E+03  # ~tau_2-
   1000016     1.30565490E+03  # ~nu_tauL
   1000021     3.53899095E+03  # ~g
   1000022     7.15566410E+02  # ~chi_10
   1000023     9.36093872E+02  # ~chi_20
   1000025     3.88527724E+03  # ~chi_30
   1000035     3.88554989E+03  # ~chi_40
   1000024     9.36206107E+02  # ~chi_1+
   1000037     3.88618172E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.74082806E-02   # alpha
Block Hmix Q=  1.91068497E+03  # Higgs mixing parameters
   1    3.90489715E+03  # mu
   2    3.56706287E+01  # tan[beta](Q)
   3    2.43730587E+02  # v(Q)
   4    1.96326448E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     5.87089421E-02   # Re[R_st(1,1)]
   1  2     9.98275142E-01   # Re[R_st(1,2)]
   2  1    -9.98275142E-01   # Re[R_st(2,1)]
   2  2     5.87089421E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.81212484E-02   # Re[R_sb(1,1)]
   1  2     9.95174467E-01   # Re[R_sb(1,2)]
   2  1    -9.95174467E-01   # Re[R_sb(2,1)]
   2  2     9.81212484E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     9.79556595E-01   # Re[R_sta(1,1)]
   1  2     2.01168777E-01   # Re[R_sta(1,2)]
   2  1    -2.01168777E-01   # Re[R_sta(2,1)]
   2  2     9.79556595E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.99924265E-01   # Re[N(1,1)]
   1  2     2.50411464E-04   # Re[N(1,2)]
   1  3    -1.21707888E-02   # Re[N(1,3)]
   1  4    -1.80910142E-03   # Re[N(1,4)]
   2  1     5.08990371E-04   # Re[N(2,1)]
   2  2     9.99778532E-01   # Re[N(2,2)]
   2  3    -2.06452755E-02   # Re[N(2,3)]
   2  4    -4.04981715E-03   # Re[N(2,4)]
   3  1     7.32297134E-03   # Re[N(3,1)]
   3  2    -1.17386192E-02   # Re[N(3,2)]
   3  3    -7.06984603E-01   # Re[N(3,3)]
   3  4     7.07093594E-01   # Re[N(3,4)]
   4  1     9.87819175E-03   # Re[N(4,1)]
   4  2    -1.74650968E-02   # Re[N(4,2)]
   4  3    -7.06822761E-01   # Re[N(4,3)]
   4  4    -7.07106057E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.99574347E-01   # Re[U(1,1)]
   1  2     2.91740310E-02   # Re[U(1,2)]
   2  1    -2.91740310E-02   # Re[U(2,1)]
   2  2    -9.99574347E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     9.99983600E-01   # Re[V(1,1)]
   1  2     5.72711863E-03   # Re[V(1,2)]
   2  1     5.72711863E-03   # Re[V(2,1)]
   2  2    -9.99983600E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.16326347E-01   # ~e^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000011     1.00701267E+01   # ~e^-_L
#    BR                NDA      ID1      ID2
     1.24700009E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.92002210E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     5.83297710E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.15060408E-01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
DECAY   1000013     1.01002235E+01   # ~mu^-_L
#    BR                NDA      ID1      ID2
     1.24324695E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.91118133E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     5.81531308E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     3.02586467E-03    2     2000013        25   # BR(~mu^-_L -> ~mu^-_R h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     4.51764647E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.93115435E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     2.69246949E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     5.37637616E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
DECAY   2000015     2.54380121E+01   # ~tau^-_2
#    BR                NDA      ID1      ID2
     2.24147237E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     5.05282970E-03    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.00910791E-02    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     3.88144054E-01    2     1000016       -24   # BR(~tau^-_2 -> ~nu_tau W^-)
     2.05898311E-01    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     1.66666489E-01    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
DECAY   1000012     1.00299729E+01   # ~nu_e
#    BR                NDA      ID1      ID2
     1.25064445E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.91510154E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     5.83425401E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
DECAY   1000014     1.00286450E+01   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.25069818E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.91508153E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     5.83422028E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
DECAY   1000016     4.87344703E+00   # ~nu_tau
#    BR                NDA      ID1      ID2
     1.64864201E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.78226994E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     5.56908071E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     1.29973937E+00   # ~d_R
#    BR                NDA      ID1      ID2
     9.99999753E-01    2     1000022         1   # BR(~d_R -> chi^0_1 d)
DECAY   1000001     3.02119881E+01   # ~d_L
#    BR                NDA      ID1      ID2
     1.15682188E-02    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     3.29539932E-01    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     6.58891812E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
DECAY   2000003     1.29974272E+00   # ~s_R
#    BR                NDA      ID1      ID2
     9.99994452E-01    2     1000022         3   # BR(~s_R -> chi^0_1 s)
DECAY   1000003     3.02124154E+01   # ~s_L
#    BR                NDA      ID1      ID2
     1.15680940E-02    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     3.29535628E-01    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     6.58882669E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
DECAY   1000005     3.97163354E+00   # ~b_1
#    BR                NDA      ID1      ID2
     3.86663647E-01    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     4.28907152E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     8.49945171E-02    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.11129539E-01    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
     3.74321582E-01    2     1000006       -37   # BR(~b_1 -> ~t_1 H^-)
DECAY   2000005     1.73998480E+02   # ~b_2
#    BR                NDA      ID1      ID2
     2.52386278E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     7.44164920E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.47772781E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     4.18130399E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     3.31606420E-01    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
     8.22143649E-03    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     1.73286077E-02    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     4.50794661E+00   # ~u_R
#    BR                NDA      ID1      ID2
     9.99999775E-01    2     1000022         2   # BR(~u_R -> chi^0_1 u)
DECAY   1000002     3.02138398E+01   # ~u_L
#    BR                NDA      ID1      ID2
     1.14973745E-02    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     3.29463391E-01    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     6.59039234E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
DECAY   2000004     4.50794232E+00   # ~c_R
#    BR                NDA      ID1      ID2
     9.99999768E-01    2     1000022         4   # BR(~c_R -> chi^0_1 c)
DECAY   1000004     3.02138061E+01   # ~c_L
#    BR                NDA      ID1      ID2
     1.14973695E-02    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     3.29463197E-01    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     6.59039427E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
DECAY   1000006     5.53954378E-01   # ~t_1
#    BR                NDA      ID1      ID2
     6.11404053E-04    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     9.94789267E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.84012867E-03    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     1.75915683E-03    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.90892983E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.42505420E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     6.84184321E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.37676408E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.58354131E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.92537370E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.55239443E-01    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     2.74500382E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     1.53367498E-01    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     1.57985126E-05   # chi^+_1
#    BR                NDA      ID1      ID2
     3.03977007E-03    2    -2000013        14   # BR(chi^+_1 -> ~mu^+_R nu_mu)
     9.20841110E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     2.61439464E-04    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     2.61415072E-04    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.12460084E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
     1.32829791E-02    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.32853002E-02    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     4.79033145E-02    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     1.69889413E+02   # chi^+_2
#    BR                NDA      ID1      ID2
     5.32788199E-04    2    -1000015        16   # BR(chi^+_2 -> ~tau^+_1 nu_tau)
     1.92177211E-02    2    -2000015        16   # BR(chi^+_2 -> ~tau^+_2 nu_tau)
     2.38197207E-02    2     1000016       -15   # BR(chi^+_2 -> ~nu_tau tau^+)
     6.01267224E-01    2     1000006        -5   # BR(chi^+_2 -> ~t_1 b_bar)
     9.69674577E-03    2     2000006        -5   # BR(chi^+_2 -> ~t_2 b_bar)
     3.76875914E-02    2    -1000005         6   # BR(chi^+_2 -> ~b^*_1 t)
     2.43121418E-02    2    -2000005         6   # BR(chi^+_2 -> ~b^*_2 t)
     1.60061842E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     4.33589441E-02    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.04235233E-02    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     3.59706806E-02    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     4.33634380E-02    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     3.81074183E-02    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     4.87637704E-02    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     3.80808371E-02    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     4.80235790E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     9.19066811E-04    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     3.20855180E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.91720444E-05   # chi^0_2
#    BR                NDA      ID1      ID2
     2.65549004E-04    2     2000011       -11   # BR(chi^0_2 -> ~e^-_R e^+)
     2.65549004E-04    2    -2000011        11   # BR(chi^0_2 -> ~e^+_R e^-)
     1.08723060E-03    2     2000013       -13   # BR(chi^0_2 -> ~mu^-_R mu^+)
     1.08723060E-03    2    -2000013        13   # BR(chi^0_2 -> ~mu^+_R mu^-)
     4.97246346E-02    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     8.34801773E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.06664164E-04    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     2.06593773E-04    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     2.07427929E-04    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     2.07454736E-04    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.60527096E-04    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     1.03574585E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     1.03583873E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     3.69756636E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     5.40745726E-02    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     1.74627567E+02   # chi^0_3
#    BR                NDA      ID1      ID2
     1.15441827E-02    2     1000015       -15   # BR(chi^0_3 -> ~tau^-_1 tau^+)
     1.15441827E-02    2    -1000015        15   # BR(chi^0_3 -> ~tau^+_1 tau^-)
     9.63795019E-03    2     2000015       -15   # BR(chi^0_3 -> ~tau^-_2 tau^+)
     9.63795019E-03    2    -2000015        15   # BR(chi^0_3 -> ~tau^+_2 tau^-)
     2.95520838E-01    2     1000006        -6   # BR(chi^0_3 -> ~t_1 t_bar)
     2.95520838E-01    2    -1000006         6   # BR(chi^0_3 -> ~t^*_1 t)
     1.16779862E-02    2     2000006        -6   # BR(chi^0_3 -> ~t_2 t_bar)
     1.16779862E-02    2    -2000006         6   # BR(chi^0_3 -> ~t^*_2 t)
     1.70711568E-02    2     1000005        -5   # BR(chi^0_3 -> ~b_1 b_bar)
     1.70711568E-02    2    -1000005         5   # BR(chi^0_3 -> ~b^*_1 b)
     4.78938394E-03    2     2000005        -5   # BR(chi^0_3 -> ~b_2 b_bar)
     4.78938394E-03    2    -2000005         5   # BR(chi^0_3 -> ~b^*_2 b)
     4.22609479E-02    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     4.22609479E-02    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     3.70144796E-02    2          37  -1000024   # BR(chi^0_3 -> H^+ chi^-_1)
     3.70144796E-02    2         -37   1000024   # BR(chi^0_3 -> H^- chi^+_1)
     1.00706357E-02    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     2.91358219E-02    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     3.25968204E-03    2     1000022        36   # BR(chi^0_3 -> chi^0_1 A^0)
     9.12722763E-03    2     1000023        36   # BR(chi^0_3 -> chi^0_2 A^0)
     9.97414316E-03    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     3.73551087E-02    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
     6.93251346E-03    2     1000022        35   # BR(chi^0_3 -> chi^0_1 H^0)
     2.58221235E-02    2     1000023        35   # BR(chi^0_3 -> chi^0_2 H^0)
#    BR                NDA      ID1      ID2       ID3
     7.02315369E-04    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     2.40579832E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     2.92506427E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     2.92506427E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     1.70583308E+02   # chi^0_4
#    BR                NDA      ID1      ID2
     1.17560197E-02    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     1.17560197E-02    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     9.91462217E-03    2     2000015       -15   # BR(chi^0_4 -> ~tau^-_2 tau^+)
     9.91462217E-03    2    -2000015        15   # BR(chi^0_4 -> ~tau^+_2 tau^-)
     3.03918657E-01    2     1000006        -6   # BR(chi^0_4 -> ~t_1 t_bar)
     3.03918657E-01    2    -1000006         6   # BR(chi^0_4 -> ~t^*_1 t)
     1.21274329E-02    2     2000006        -6   # BR(chi^0_4 -> ~t_2 t_bar)
     1.21274329E-02    2    -2000006         6   # BR(chi^0_4 -> ~t^*_2 t)
     1.74499369E-02    2     1000005        -5   # BR(chi^0_4 -> ~b_1 b_bar)
     1.74499369E-02    2    -1000005         5   # BR(chi^0_4 -> ~b^*_1 b)
     4.91456541E-03    2     2000005        -5   # BR(chi^0_4 -> ~b_2 b_bar)
     4.91456541E-03    2    -2000005         5   # BR(chi^0_4 -> ~b^*_2 b)
     4.33207801E-02    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     4.33207801E-02    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     3.57448058E-02    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     3.57448058E-02    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     5.66118592E-03    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.34634871E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     2.98039515E-03    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     8.34591043E-03    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     9.14820581E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     3.42590425E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     7.91385674E-03    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     2.94785306E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
#    BR                NDA      ID1      ID2       ID3
     7.11661913E-04    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     2.40134574E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     3.43683666E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     3.43683666E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     2.42223426E+02   # ~g
#    BR                NDA      ID1      ID2
     6.10138398E-02    2     2000002        -2   # BR(~g -> ~u_R u_bar)
     6.10138398E-02    2    -2000002         2   # BR(~g -> ~u^*_R u)
     2.70685493E-02    2     1000002        -2   # BR(~g -> ~u_L u_bar)
     2.70685493E-02    2    -1000002         2   # BR(~g -> ~u^*_L u)
     6.10138403E-02    2     2000004        -4   # BR(~g -> ~c_R c_bar)
     6.10138403E-02    2    -2000004         4   # BR(~g -> ~c^*_R c)
     2.70686627E-02    2     1000004        -4   # BR(~g -> ~c_L c_bar)
     2.70686627E-02    2    -1000004         4   # BR(~g -> ~c^*_L c)
     1.45283684E-03    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     1.45283684E-03    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     1.77487703E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.77487703E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     3.81600784E-02    2     2000001        -1   # BR(~g -> ~d_R d_bar)
     3.81600784E-02    2    -2000001         1   # BR(~g -> ~d^*_R d)
     2.69975321E-02    2     1000001        -1   # BR(~g -> ~d_L d_bar)
     2.69975321E-02    2    -1000001         1   # BR(~g -> ~d^*_L d)
     3.81606153E-02    2     2000003        -3   # BR(~g -> ~s_R s_bar)
     3.81606153E-02    2    -2000003         3   # BR(~g -> ~s^*_R s)
     2.69974118E-02    2     1000003        -3   # BR(~g -> ~s_L s_bar)
     2.69974118E-02    2    -1000003         3   # BR(~g -> ~s^*_L s)
     1.38985328E-02    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.38985328E-02    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     3.06213528E-04    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     3.06213528E-04    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
     2.68814152E-04    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     2.28815262E-04    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.28815262E-04    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     3.73441112E-03   # Gamma(h0)
     2.32782325E-03   2        22        22   # BR(h0 -> photon photon)
     1.37173322E-03   2        22        23   # BR(h0 -> photon Z)
     2.41117524E-02   2        23        23   # BR(h0 -> Z Z)
     2.04782435E-01   2       -24        24   # BR(h0 -> W W)
     7.26484835E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.34337115E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.37681392E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.85084875E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.50076292E-07   2        -2         2   # BR(h0 -> Up up)
     2.91218885E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.16579124E-07   2        -1         1   # BR(h0 -> Down down)
     2.23004189E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.96665939E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.66181625E+01   # Gamma(HH)
     3.64140696E-08   2        22        22   # BR(HH -> photon photon)
     7.89656148E-10   2        22        23   # BR(HH -> photon Z)
     1.49765804E-06   2        23        23   # BR(HH -> Z Z)
     1.31605473E-06   2       -24        24   # BR(HH -> W W)
     3.85906922E-05   2        21        21   # BR(HH -> gluon gluon)
     1.69138147E-08   2       -11        11   # BR(HH -> Electron electron)
     7.52906321E-04   2       -13        13   # BR(HH -> Muon muon)
     2.11674235E-01   2       -15        15   # BR(HH -> Tau tau)
     1.02544451E-13   2        -2         2   # BR(HH -> Up up)
     1.98924729E-08   2        -4         4   # BR(HH -> Charm charm)
     9.51150373E-04   2        -6         6   # BR(HH -> Top top)
     6.16590357E-07   2        -1         1   # BR(HH -> Down down)
     2.23078388E-04   2        -3         3   # BR(HH -> Strange strange)
     7.86343986E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.25496779E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.61440628E+01   # Gamma(A0)
     9.80863481E-08   2        22        22   # BR(A0 -> photon photon)
     7.63563029E-08   2        22        23   # BR(A0 -> photon Z)
     6.07460343E-05   2        21        21   # BR(A0 -> gluon gluon)
     1.69201874E-08   2       -11        11   # BR(A0 -> Electron electron)
     7.53190342E-04   2       -13        13   # BR(A0 -> Muon muon)
     2.11755882E-01   2       -15        15   # BR(A0 -> Tau tau)
     6.12323011E-14   2        -2         2   # BR(A0 -> Up up)
     1.18808079E-08   2        -4         4   # BR(A0 -> Charm charm)
     5.35987428E-04   2        -6         6   # BR(A0 -> Top top)
     6.16819266E-07   2        -1         1   # BR(A0 -> Down down)
     2.23161121E-04   2        -3         3   # BR(A0 -> Strange strange)
     7.86668147E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.06540420E-06   2        23        25   # BR(A0 -> Z h0)
     3.73714874E-37   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.83162355E+01   # Gamma(Hp)
     1.71051845E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     7.31300056E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     2.06852725E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     5.19601729E-07   2        -1         2   # BR(Hp -> Down up)
     8.69334579E-06   2        -3         2   # BR(Hp -> Strange up)
     8.47763382E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.54968553E-08   2        -1         4   # BR(Hp -> Down charm)
     1.87363657E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.18717058E-03   2        -5         4   # BR(Hp -> Bottom charm)
     1.55405057E-07   2        -1         6   # BR(Hp -> Down top)
     3.65661005E-06   2        -3         6   # BR(Hp -> Strange top)
     7.91018060E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.83568656E-06   2        24        25   # BR(Hp -> W h0)
     2.17784385E-11   2        24        35   # BR(Hp -> W HH)
     6.91318873E-12   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.56351506E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.27243740E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.27239375E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00003430E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    7.51616003E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    7.85920238E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.56351506E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.27243740E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.27239375E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999617E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    3.82724826E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999617E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    3.82724826E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.24642659E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.51230977E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.32700697E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    3.82724826E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999617E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.30750721E-04   # BR(b -> s gamma)
    2    1.58862343E-06   # BR(b -> s mu+ mu-)
    3    3.52466559E-05   # BR(b -> s nu nu)
    4    2.35401382E-15   # BR(Bd -> e+ e-)
    5    1.00558352E-10   # BR(Bd -> mu+ mu-)
    6    2.09086395E-08   # BR(Bd -> tau+ tau-)
    7    7.90777220E-14   # BR(Bs -> e+ e-)
    8    3.37811928E-09   # BR(Bs -> mu+ mu-)
    9    7.11904217E-07   # BR(Bs -> tau+ tau-)
   10    9.43957329E-05   # BR(B_u -> tau nu)
   11    9.75071782E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41644524E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93404169E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15638758E-03   # epsilon_K
   17    2.28165298E-15   # Delta(M_K)
   18    2.48036600E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28962023E-11   # BR(K^+ -> pi^+ nu nu)
   20   -5.66887587E-15   # Delta(g-2)_electron/2
   21   -2.42305590E-10   # Delta(g-2)_muon/2
   22   -5.18550550E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.86487953E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.98233429E-01   # C7
     0305 4322   00   2    -1.52353370E-04   # C7'
     0305 6421   00   0    -9.52278349E-02   # C8
     0305 6421   00   2    -1.12672119E-01   # C8
     0305 6321   00   2    -3.14421776E-04   # C8'
 03051111 4133   00   0     1.60081004E+00   # C9 e+e-
 03051111 4133   00   2     1.60067503E+00   # C9 e+e-
 03051111 4233   00   2    -4.41980515E-05   # C9' e+e-
 03051111 4137   00   0    -4.42350214E+00   # C10 e+e-
 03051111 4137   00   2    -4.42122513E+00   # C10 e+e-
 03051111 4237   00   2     3.40009199E-04   # C10' e+e-
 03051313 4133   00   0     1.60081004E+00   # C9 mu+mu-
 03051313 4133   00   2     1.60067334E+00   # C9 mu+mu-
 03051313 4233   00   2    -4.42010821E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.42350214E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.42122682E+00   # C10 mu+mu-
 03051313 4237   00   2     3.40012058E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50485866E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -7.39525686E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50485866E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -7.39519193E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50485867E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -7.37689542E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85909318E-07   # C7
     0305 4422   00   2    -9.49230072E-07   # C7
     0305 4322   00   2     4.57014709E-06   # C7'
     0305 6421   00   0     3.30556853E-07   # C8
     0305 6421   00   2    -5.55002281E-06   # C8
     0305 6321   00   2     3.29296486E-06   # C8'
 03051111 4133   00   2    -3.15749013E-06   # C9 e+e-
 03051111 4233   00   2     1.07969680E-06   # C9' e+e-
 03051111 4137   00   2     2.44975943E-05   # C10 e+e-
 03051111 4237   00   2    -8.30609265E-06   # C10' e+e-
 03051313 4133   00   2    -3.15746422E-06   # C9 mu+mu-
 03051313 4233   00   2     1.07969603E-06   # C9' mu+mu-
 03051313 4137   00   2     2.44976073E-05   # C10 mu+mu-
 03051313 4237   00   2    -8.30609497E-06   # C10' mu+mu-
 03051212 4137   00   2    -5.25578932E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     1.80660397E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -5.25578937E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     1.80660397E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -5.25564238E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     1.80660403E-06   # C11' nu_3 nu_3
