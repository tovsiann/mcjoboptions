# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 18.08.2022,  12:21
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.85828916E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.82465881E+03  # scale for input parameters
    1   -8.70451669E+02  # M_1
    2    1.12362755E+03  # M_2
    3    3.94831562E+03  # M_3
   11    8.01796915E+02  # A_t
   12   -1.19835613E+03  # A_b
   13   -1.75932465E+03  # A_tau
   23   -3.09079417E+03  # mu
   25    3.92724937E+01  # tan(beta)
   26    1.68887812E+03  # m_A, pole mass
   31    9.30559261E+02  # M_L11
   32    9.30559261E+02  # M_L22
   33    1.83058475E+03  # M_L33
   34    1.26718701E+03  # M_E11
   35    1.26718701E+03  # M_E22
   36    1.13540837E+03  # M_E33
   41    3.83747659E+03  # M_Q11
   42    3.83747659E+03  # M_Q22
   43    3.95982981E+03  # M_Q33
   44    2.34275889E+03  # M_U11
   45    2.34275889E+03  # M_U22
   46    3.47038888E+03  # M_U33
   47    1.27709457E+03  # M_D11
   48    1.27709457E+03  # M_D22
   49    7.30570728E+02  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.82465881E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.82465881E+03  # (SUSY scale)
  1  1     8.38708978E-06   # Y_u(Q)^DRbar
  2  2     4.26064161E-03   # Y_c(Q)^DRbar
  3  3     1.01322781E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.82465881E+03  # (SUSY scale)
  1  1     6.61881158E-04   # Y_d(Q)^DRbar
  2  2     1.25757420E-02   # Y_s(Q)^DRbar
  3  3     6.56378705E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.82465881E+03  # (SUSY scale)
  1  1     1.15503102E-04   # Y_e(Q)^DRbar
  2  2     2.38823783E-02   # Y_mu(Q)^DRbar
  3  3     4.01662314E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.82465881E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     8.01796912E+02   # A_t(Q)^DRbar
Block Ad Q=  3.82465881E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.19835617E+03   # A_b(Q)^DRbar
Block Ae Q=  3.82465881E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.75932448E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.82465881E+03  # soft SUSY breaking masses at Q
   1   -8.70451669E+02  # M_1
   2    1.12362755E+03  # M_2
   3    3.94831562E+03  # M_3
  21   -6.34498545E+06  # M^2_(H,d)
  22   -9.21332006E+06  # M^2_(H,u)
  31    9.30559261E+02  # M_(L,11)
  32    9.30559261E+02  # M_(L,22)
  33    1.83058475E+03  # M_(L,33)
  34    1.26718701E+03  # M_(E,11)
  35    1.26718701E+03  # M_(E,22)
  36    1.13540837E+03  # M_(E,33)
  41    3.83747659E+03  # M_(Q,11)
  42    3.83747659E+03  # M_(Q,22)
  43    3.95982981E+03  # M_(Q,33)
  44    2.34275889E+03  # M_(U,11)
  45    2.34275889E+03  # M_(U,22)
  46    3.47038888E+03  # M_(U,33)
  47    1.27709457E+03  # M_(D,11)
  48    1.27709457E+03  # M_(D,22)
  49    7.30570728E+02  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.14691547E+02  # h0
        35     1.67904820E+03  # H0
        36     1.68887812E+03  # A0
        37     1.66787314E+03  # H+
   1000001     3.98920382E+03  # ~d_L
   2000001     1.51274891E+03  # ~d_R
   1000002     3.98839295E+03  # ~u_L
   2000002     2.51847552E+03  # ~u_R
   1000003     3.98919543E+03  # ~s_L
   2000003     1.51273805E+03  # ~s_R
   1000004     3.98838382E+03  # ~c_L
   2000004     2.51847553E+03  # ~c_R
   1000005     1.01228880E+03  # ~b_1
   2000005     4.06523932E+03  # ~b_2
   1000006     3.59795936E+03  # ~t_1
   2000006     4.06564209E+03  # ~t_2
   1000011     9.49806488E+02  # ~e_L-
   2000011     1.28047745E+03  # ~e_R-
   1000012     9.46101387E+02  # ~nu_eL
   1000013     9.49696665E+02  # ~mu_L-
   2000013     1.28047386E+03  # ~mu_R-
   1000014     9.46075709E+02  # ~nu_muL
   1000015     1.12845422E+03  # ~tau_1-
   2000015     1.83677267E+03  # ~tau_2-
   1000016     1.82966529E+03  # ~nu_tauL
   1000021     4.09514049E+03  # ~g
   1000022     8.52944175E+02  # ~chi_10
   1000023     1.16798544E+03  # ~chi_20
   1000025     3.05038135E+03  # ~chi_30
   1000035     3.05097115E+03  # ~chi_40
   1000024     1.16808833E+03  # ~chi_1+
   1000037     3.05173477E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.59821238E-02   # alpha
Block Hmix Q=  3.82465881E+03  # Higgs mixing parameters
   1   -3.09079417E+03  # mu
   2    3.92724937E+01  # tan[beta](Q)
   3    2.43021417E+02  # v(Q)
   4    2.85230930E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -2.59961469E-02   # Re[R_st(1,1)]
   1  2     9.99662043E-01   # Re[R_st(1,2)]
   2  1    -9.99662043E-01   # Re[R_st(2,1)]
   2  2    -2.59961469E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -3.25618404E-02   # Re[R_sb(1,1)]
   1  2     9.99469723E-01   # Re[R_sb(1,2)]
   2  1    -9.99469723E-01   # Re[R_sb(2,1)]
   2  2    -3.25618404E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -9.00759842E-02   # Re[R_sta(1,1)]
   1  2     9.95934896E-01   # Re[R_sta(1,2)]
   2  1    -9.95934896E-01   # Re[R_sta(2,1)]
   2  2    -9.00759842E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.99854999E-01   # Re[N(1,1)]
   1  2    -2.14375850E-04   # Re[N(1,2)]
   1  3     1.62658955E-02   # Re[N(1,3)]
   1  4    -5.03537860E-03   # Re[N(1,4)]
   2  1    -2.17869188E-04   # Re[N(2,1)]
   2  2    -9.99495362E-01   # Re[N(2,2)]
   2  3    -2.98915842E-02   # Re[N(2,3)]
   2  4    -1.07455912E-02   # Re[N(2,4)]
   3  1    -1.50627235E-02   # Re[N(3,1)]
   3  2     1.35399952E-02   # Re[N(3,2)]
   3  3    -7.06823831E-01   # Re[N(3,3)]
   3  4     7.07099608E-01   # Re[N(3,4)]
   4  1     7.94022942E-03   # Re[N(4,1)]
   4  2    -2.87340316E-02   # Re[N(4,2)]
   4  3     7.06570581E-01   # Re[N(4,3)]
   4  4     7.07014372E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -9.99104944E-01   # Re[U(1,1)]
   1  2    -4.23002412E-02   # Re[U(1,2)]
   2  1     4.23002412E-02   # Re[U(2,1)]
   2  2    -9.99104944E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -9.99883309E-01   # Re[V(1,1)]
   1  2     1.52764168E-02   # Re[V(1,2)]
   2  1     1.52764168E-02   # Re[V(2,1)]
   2  2     9.99883309E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   1000011     4.47242118E-02   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.99999977E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000011     1.99047193E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.99999786E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
DECAY   1000013     4.46328304E-02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.99999980E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     2.00827779E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.91122762E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     8.87668689E-03    2     1000013        25   # BR(~mu^-_R -> ~mu^-_L h^0)
DECAY   1000015     1.03636105E+00   # ~tau^-_1
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
DECAY   2000015     1.87265368E+01   # ~tau^-_2
#    BR                NDA      ID1      ID2
     7.74078472E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.51223477E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     3.01838064E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     2.12887061E-01    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     2.56643550E-01    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     4.16173216E-02   # ~nu_e
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
DECAY   1000014     4.15965813E-02   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
DECAY   1000016     1.77627462E+01   # ~nu_tau
#    BR                NDA      ID1      ID2
     7.91773339E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.58963126E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     3.18410023E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.43449517E-01    2     1000015        24   # BR(~nu_tau -> ~tau^-_1 W^+)
DECAY   2000001     3.92810554E-01   # ~d_R
#    BR                NDA      ID1      ID2
     9.99999969E-01    2     1000022         1   # BR(~d_R -> chi^0_1 d)
DECAY   1000001     4.44822886E+01   # ~d_L
#    BR                NDA      ID1      ID2
     1.13679967E-02    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     3.29598144E-01    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     6.58710775E-01    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     2.43023403E-04    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
DECAY   2000003     3.92805350E-01   # ~s_R
#    BR                NDA      ID1      ID2
     9.99994683E-01    2     1000022         3   # BR(~s_R -> chi^0_1 s)
DECAY   1000003     4.44877826E+01   # ~s_L
#    BR                NDA      ID1      ID2
     1.13666335E-02    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     3.29556714E-01    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     6.58627304E-01    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     2.48655440E-04    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
DECAY   1000005     4.81484574E-02   # ~b_1
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
DECAY   2000005     9.97104813E+01   # ~b_2
#    BR                NDA      ID1      ID2
     5.15842115E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.50585135E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     3.33790249E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     3.33655582E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.98900050E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.58002699E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     3.00900876E-03    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.99995316E-01    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     1.26979419E-02    2     1000005        36   # BR(~b_2 -> ~b_1 A^0)
     9.21910687E-02    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
     1.27157771E-02    2     1000005        35   # BR(~b_2 -> ~b_1 H^0)
DECAY   2000002     4.40670616E+00   # ~u_R
#    BR                NDA      ID1      ID2
     9.99999963E-01    2     1000022         2   # BR(~u_R -> chi^0_1 u)
DECAY   1000002     4.45099079E+01   # ~u_L
#    BR                NDA      ID1      ID2
     1.14130252E-02    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     3.29352685E-01    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     6.59143854E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
DECAY   2000004     4.40670315E+00   # ~c_R
#    BR                NDA      ID1      ID2
     9.99999918E-01    2     1000022         4   # BR(~c_R -> chi^0_1 c)
DECAY   1000004     4.45121809E+01   # ~c_L
#    BR                NDA      ID1      ID2
     1.14124080E-02    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     3.29334721E-01    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     6.59108575E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
DECAY   1000006     1.86890004E+01   # ~t_1
#    BR                NDA      ID1      ID2
     3.81089947E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.27829232E-03    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     1.47737647E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.48580811E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     3.09433907E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     9.46916417E-04    2     1000005        24   # BR(~t_1 -> ~b_1 W^+)
     1.08675852E-02    2     1000005        37   # BR(~t_1 -> ~b_1 H^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.10776463E+02   # ~t_2
#    BR                NDA      ID1      ID2
     4.70063379E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.35241173E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     6.96039838E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     6.99214965E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     2.72516370E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     5.98558752E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     3.60439473E-01    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     2.26802938E-02    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     1.34041305E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     3.70028739E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     2.39564709E+00   # chi^+_1
#    BR                NDA      ID1      ID2
     2.45934511E-01    2    -1000011        12   # BR(chi^+_1 -> ~e^+_L nu_e)
     2.46156514E-01    2    -1000013        14   # BR(chi^+_1 -> ~mu^+_L nu_mu)
     1.27263347E-04    2    -1000015        16   # BR(chi^+_1 -> ~tau^+_1 nu_tau)
     2.53860233E-01    2     1000012       -11   # BR(chi^+_1 -> ~nu_e e^+)
     2.53912956E-01    2     1000014       -13   # BR(chi^+_1 -> ~nu_mu mu^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     6.56947292E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     2.98406548E-04    2    -1000011        12   # BR(chi^+_2 -> ~e^+_L nu_e)
     2.98421351E-04    2    -1000013        14   # BR(chi^+_2 -> ~mu^+_L nu_mu)
     1.78603691E-04    2    -2000013        14   # BR(chi^+_2 -> ~mu^+_R nu_mu)
     5.43108830E-02    2    -1000015        16   # BR(chi^+_2 -> ~tau^+_1 nu_tau)
     7.73910556E-04    2    -2000015        16   # BR(chi^+_2 -> ~tau^+_2 nu_tau)
     2.53943764E-04    2     1000014       -13   # BR(chi^+_2 -> ~nu_mu mu^+)
     3.05543002E-02    2     1000016       -15   # BR(chi^+_2 -> ~nu_tau tau^+)
     1.24507797E-04    2    -2000003         4   # BR(chi^+_2 -> ~s^*_R c)
     4.77156903E-01    2    -1000005         6   # BR(chi^+_2 -> ~b^*_1 t)
     3.34460670E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     9.26004063E-02    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.27410528E-02    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     3.30694776E-02    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     9.22276783E-02    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     3.11066929E-02    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     9.59571571E-02    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     3.19553166E-02    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     7.01427719E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     1.60824092E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     4.24555333E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.39903328E+00   # chi^0_2
#    BR                NDA      ID1      ID2
     1.22822969E-01    2     1000011       -11   # BR(chi^0_2 -> ~e^-_L e^+)
     1.22822969E-01    2    -1000011        11   # BR(chi^0_2 -> ~e^+_L e^-)
     1.22933970E-01    2     1000013       -13   # BR(chi^0_2 -> ~mu^-_L mu^+)
     1.22933970E-01    2    -1000013        13   # BR(chi^0_2 -> ~mu^+_L mu^-)
     1.26526892E-01    2     1000012       -12   # BR(chi^0_2 -> ~nu_e nu_bar_e)
     1.26526892E-01    2    -1000012        12   # BR(chi^0_2 -> ~nu^*_e nu_e)
     1.26553104E-01    2     1000014       -14   # BR(chi^0_2 -> ~nu_mu nu_bar_mu)
     1.26553104E-01    2    -1000014        14   # BR(chi^0_2 -> ~nu^*_mu nu_mu)
     1.09572703E-03    2     1000005        -5   # BR(chi^0_2 -> ~b_1 b_bar)
     1.09572703E-03    2    -1000005         5   # BR(chi^0_2 -> ~b^*_1 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     6.91071822E+01   # chi^0_3
#    BR                NDA      ID1      ID2
     1.04428288E-04    2     1000013       -13   # BR(chi^0_3 -> ~mu^-_L mu^+)
     1.04428288E-04    2    -1000013        13   # BR(chi^0_3 -> ~mu^+_L mu^-)
     1.01987181E-04    2     2000013       -13   # BR(chi^0_3 -> ~mu^-_R mu^+)
     1.01987181E-04    2    -2000013        13   # BR(chi^0_3 -> ~mu^+_R mu^-)
     2.62108753E-02    2     1000015       -15   # BR(chi^0_3 -> ~tau^-_1 tau^+)
     2.62108753E-02    2    -1000015        15   # BR(chi^0_3 -> ~tau^+_1 tau^-)
     1.44777198E-02    2     2000015       -15   # BR(chi^0_3 -> ~tau^-_2 tau^+)
     1.44777198E-02    2    -2000015        15   # BR(chi^0_3 -> ~tau^+_2 tau^-)
     2.24160519E-01    2     1000005        -5   # BR(chi^0_3 -> ~b_1 b_bar)
     2.24160519E-01    2    -1000005         5   # BR(chi^0_3 -> ~b^*_1 b)
     8.80682751E-02    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     8.80682751E-02    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     2.84800760E-02    2          37  -1000024   # BR(chi^0_3 -> H^+ chi^-_1)
     2.84800760E-02    2         -37   1000024   # BR(chi^0_3 -> H^- chi^+_1)
     6.90618775E-03    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     7.18613354E-02    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     1.63905047E-03    2     1000022        36   # BR(chi^0_3 -> chi^0_1 A^0)
     1.30435361E-03    2     1000023        36   # BR(chi^0_3 -> chi^0_2 A^0)
     2.19128939E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     8.30815001E-02    2     1000023        25   # BR(chi^0_3 -> chi^0_2 h^0)
     9.37860676E-03    2     1000022        35   # BR(chi^0_3 -> chi^0_1 H^0)
     2.61261343E-02    2     1000023        35   # BR(chi^0_3 -> chi^0_2 H^0)
#    BR                NDA      ID1      ID2       ID3
     1.01863317E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     3.23917050E-03    3     1000023         6        -6   # BR(chi^0_3 -> chi^0_2 t t_bar)
     4.95879075E-03    3     1000024         5        -6   # BR(chi^0_3 -> chi^+_1 b t_bar)
     4.95879075E-03    3    -1000024        -5         6   # BR(chi^0_3 -> chi^-_1 b_bar t)
DECAY   1000035     6.61296432E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.56270042E-04    2     1000013       -13   # BR(chi^0_4 -> ~mu^-_L mu^+)
     1.56270042E-04    2    -1000013        13   # BR(chi^0_4 -> ~mu^+_L mu^-)
     2.72721416E-02    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     2.72721416E-02    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     1.52042145E-02    2     2000015       -15   # BR(chi^0_4 -> ~tau^-_2 tau^+)
     1.52042145E-02    2    -2000015        15   # BR(chi^0_4 -> ~tau^+_2 tau^-)
     2.33968400E-01    2     1000005        -5   # BR(chi^0_4 -> ~b_1 b_bar)
     2.33968400E-01    2    -1000005         5   # BR(chi^0_4 -> ~b^*_1 b)
     9.11030649E-02    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     9.11030649E-02    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     3.27115074E-02    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     3.27115074E-02    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     2.59801005E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.66556465E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.54646862E-03    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     1.23420878E-03    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     2.06700049E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     7.81246461E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.08650335E-02    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     3.02920090E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
#    BR                NDA      ID1      ID2       ID3
     9.76293725E-04    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     3.07440542E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     4.42065021E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     4.42065021E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     3.88952365E+02   # ~g
#    BR                NDA      ID1      ID2
     6.02442597E-02    2     2000002        -2   # BR(~g -> ~u_R u_bar)
     6.02442597E-02    2    -2000002         2   # BR(~g -> ~u^*_R u)
     4.12550495E-04    2     1000002        -2   # BR(~g -> ~u_L u_bar)
     4.12550495E-04    2    -1000002         2   # BR(~g -> ~u^*_L u)
     6.02442478E-02    2     2000004        -4   # BR(~g -> ~c_R c_bar)
     6.02442478E-02    2    -2000004         4   # BR(~g -> ~c^*_R c)
     4.12591704E-04    2     1000004        -4   # BR(~g -> ~c_L c_bar)
     4.12591704E-04    2    -1000004         4   # BR(~g -> ~c^*_L c)
     7.80203277E-03    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     7.80203277E-03    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.16198884E-01    2     2000001        -1   # BR(~g -> ~d_R d_bar)
     1.16198884E-01    2    -2000001         1   # BR(~g -> ~d^*_R d)
     4.06388229E-04    2     1000001        -1   # BR(~g -> ~d_L d_bar)
     4.06388229E-04    2    -1000001         1   # BR(~g -> ~d^*_L d)
     1.16199411E-01    2     2000003        -3   # BR(~g -> ~s_R s_bar)
     1.16199411E-01    2    -2000003         3   # BR(~g -> ~s^*_R s)
     4.06451600E-04    2     1000003        -3   # BR(~g -> ~s_L s_bar)
     4.06451600E-04    2    -1000003         3   # BR(~g -> ~s^*_L s)
     1.37382137E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.37382137E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
#    BR                NDA      ID1      ID2       ID3
     1.52513007E-04    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     1.31838124E-04    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     1.31838124E-04    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     2.91855582E-03   # Gamma(h0)
     2.21010724E-03   2        22        22   # BR(h0 -> photon photon)
     6.76286402E-04   2        22        23   # BR(h0 -> photon Z)
     8.85859785E-03   2        23        23   # BR(h0 -> Z Z)
     8.88824670E-02   2       -24        24   # BR(h0 -> W W)
     7.31012823E-02   2        21        21   # BR(h0 -> gluon gluon)
     6.23962449E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.77543702E-04   2       -13        13   # BR(h0 -> Muon muon)
     8.00560782E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.79759061E-07   2        -2         2   # BR(h0 -> Up up)
     3.48756592E-02   2        -4         4   # BR(h0 -> Charm charm)
     7.37863302E-07   2        -1         1   # BR(h0 -> Down down)
     2.66867238E-04   2        -3         3   # BR(h0 -> Strange strange)
     7.10794187E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     1.17817529E+02   # Gamma(HH)
     5.97418435E-09   2        22        22   # BR(HH -> photon photon)
     8.26827114E-09   2        22        23   # BR(HH -> photon Z)
     4.72231922E-08   2        23        23   # BR(HH -> Z Z)
     3.21748992E-08   2       -24        24   # BR(HH -> W W)
     2.14943150E-05   2        21        21   # BR(HH -> gluon gluon)
     2.56157539E-09   2       -11        11   # BR(HH -> Electron electron)
     1.14032721E-04   2       -13        13   # BR(HH -> Muon muon)
     3.66963643E-02   2       -15        15   # BR(HH -> Tau tau)
     4.78977933E-14   2        -2         2   # BR(HH -> Up up)
     9.28924055E-09   2        -4         4   # BR(HH -> Charm charm)
     5.52455131E-04   2        -6         6   # BR(HH -> Top top)
     7.94222158E-07   2        -1         1   # BR(HH -> Down down)
     2.87299100E-04   2        -3         3   # BR(HH -> Strange strange)
     9.62327062E-01   2        -5         5   # BR(HH -> Bottom bottom)
     3.92330125E-07   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.02574886E+02   # Gamma(A0)
     3.42535143E-08   2        22        22   # BR(A0 -> photon photon)
     3.86468605E-08   2        22        23   # BR(A0 -> photon Z)
     4.13740749E-05   2        21        21   # BR(A0 -> gluon gluon)
     2.55889240E-09   2       -11        11   # BR(A0 -> Electron electron)
     1.13913323E-04   2       -13        13   # BR(A0 -> Muon muon)
     3.66635497E-02   2       -15        15   # BR(A0 -> Tau tau)
     5.98971857E-14   2        -2         2   # BR(A0 -> Up up)
     1.16137910E-08   2        -4         4   # BR(A0 -> Charm charm)
     7.41133008E-04   2        -6         6   # BR(A0 -> Top top)
     7.93862267E-07   2        -1         1   # BR(A0 -> Down down)
     2.87168868E-04   2        -3         3   # BR(A0 -> Strange strange)
     9.62151906E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     7.31890574E-08   2        23        25   # BR(A0 -> Z h0)
     9.81435748E-10   2        23        35   # BR(A0 -> Z HH)
     8.96075590E-40   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.03786086E+02   # Gamma(Hp)
     3.95616195E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     1.69138279E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     4.78418484E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     7.53504088E-07   2        -1         2   # BR(Hp -> Down up)
     1.26307549E-05   2        -3         2   # BR(Hp -> Strange up)
     1.01410256E-05   2        -5         2   # BR(Hp -> Bottom up)
     3.52133888E-08   2        -1         4   # BR(Hp -> Down charm)
     2.71582789E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.42010528E-03   2        -5         4   # BR(Hp -> Bottom charm)
     2.92661079E-08   2        -1         6   # BR(Hp -> Down top)
     1.03142530E-06   2        -3         6   # BR(Hp -> Strange top)
     9.50272630E-01   2        -5         6   # BR(Hp -> Bottom top)
     6.97706503E-08   2        24        25   # BR(Hp -> W h0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.04162169E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.54228714E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.54232876E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99973014E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    6.75356461E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    6.48370195E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.04162169E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.54228714E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.54232876E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999725E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.75111167E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999725E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.75111167E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.25722152E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.06436465E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.36028859E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.75111167E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999725E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.37898320E-04   # BR(b -> s gamma)
    2    1.58856616E-06   # BR(b -> s mu+ mu-)
    3    3.52572886E-05   # BR(b -> s nu nu)
    4    2.20457934E-15   # BR(Bd -> e+ e-)
    5    9.41773558E-11   # BR(Bd -> mu+ mu-)
    6    1.97275456E-08   # BR(Bd -> tau+ tau-)
    7    7.55478772E-14   # BR(Bs -> e+ e-)
    8    3.22741018E-09   # BR(Bs -> mu+ mu-)
    9    6.84960380E-07   # BR(Bs -> tau+ tau-)
   10    9.36979344E-05   # BR(B_u -> tau nu)
   11    9.67863791E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41792411E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93417310E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15673837E-03   # epsilon_K
   17    2.28165631E-15   # Delta(M_K)
   18    2.48018294E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29096872E-11   # BR(K^+ -> pi^+ nu nu)
   20    3.99893010E-18   # Delta(g-2)_electron/2
   21    1.56825475E-13   # Delta(g-2)_muon/2
   22   -1.56559084E-08   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.27970216E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.06872293E-01   # C7
     0305 4322   00   2    -3.66314858E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.16111515E-01   # C8
     0305 6321   00   2    -4.22012202E-04   # C8'
 03051111 4133   00   0     1.62546970E+00   # C9 e+e-
 03051111 4133   00   2     1.62556498E+00   # C9 e+e-
 03051111 4233   00   2     2.02220405E-05   # C9' e+e-
 03051111 4137   00   0    -4.44816180E+00   # C10 e+e-
 03051111 4137   00   2    -4.44694073E+00   # C10 e+e-
 03051111 4237   00   2    -1.49705963E-04   # C10' e+e-
 03051313 4133   00   0     1.62546970E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62556361E+00   # C9 mu+mu-
 03051313 4233   00   2     2.02194612E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.44816180E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44694210E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.49703871E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50508989E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     3.23807835E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50508989E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     3.23813147E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50508985E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     3.25310095E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85832900E-07   # C7
     0305 4422   00   2    -5.87180771E-06   # C7
     0305 4322   00   2    -5.86067077E-07   # C7'
     0305 6421   00   0     3.30491396E-07   # C8
     0305 6421   00   2     5.20961453E-06   # C8
     0305 6321   00   2    -1.56551024E-07   # C8'
 03051111 4133   00   2     2.79687807E-08   # C9 e+e-
 03051111 4233   00   2     2.20458990E-06   # C9' e+e-
 03051111 4137   00   2    -4.18853101E-07   # C10 e+e-
 03051111 4237   00   2    -1.63476382E-05   # C10' e+e-
 03051313 4133   00   2     2.79730992E-08   # C9 mu+mu-
 03051313 4233   00   2     2.20458777E-06   # C9' mu+mu-
 03051313 4137   00   2    -4.18859530E-07   # C10 mu+mu-
 03051313 4237   00   2    -1.63476448E-05   # C10' mu+mu-
 03051212 4137   00   2     1.62090710E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     3.53594105E-06   # C11' nu_1 nu_1
 03051414 4137   00   2     1.62090724E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     3.53594105E-06   # C11' nu_2 nu_2
 03051616 4137   00   2     1.61054319E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     3.53594113E-06   # C11' nu_3 nu_3
