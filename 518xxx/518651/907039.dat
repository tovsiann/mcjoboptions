# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 18.08.2022,  12:50
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.85473514E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    1.25007579E+03  # scale for input parameters
    1   -3.27430180E+02  # M_1
    2   -1.61791646E+03  # M_2
    3    5.58460861E+02  # M_3
   11    1.18552665E+02  # A_t
   12    3.67035853E+03  # A_b
   13    1.50543976E+03  # A_tau
   23    6.33728935E+02  # mu
   25    3.75972555E+01  # tan(beta)
   26    1.32234139E+03  # m_A, pole mass
   31    7.32742640E+02  # M_L11
   32    7.32742640E+02  # M_L22
   33    7.59805063E+02  # M_L33
   34    1.96271775E+03  # M_E11
   35    1.96271775E+03  # M_E22
   36    1.47589303E+03  # M_E33
   41    7.65066893E+02  # M_Q11
   42    7.65066893E+02  # M_Q22
   43    1.15450655E+03  # M_Q33
   44    3.63603709E+02  # M_U11
   45    3.63603709E+02  # M_U22
   46    1.30088090E+03  # M_U33
   47    7.39318354E+02  # M_D11
   48    7.39318354E+02  # M_D22
   49    1.79044530E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  1.25007579E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  1.25007579E+03  # (SUSY scale)
  1  1     8.38733732E-06   # Y_u(Q)^DRbar
  2  2     4.26076736E-03   # Y_c(Q)^DRbar
  3  3     1.01325772E+00   # Y_t(Q)^DRbar
Block Yd Q=  1.25007579E+03  # (SUSY scale)
  1  1     6.33666139E-04   # Y_d(Q)^DRbar
  2  2     1.20396566E-02   # Y_s(Q)^DRbar
  3  3     6.28398248E-01   # Y_b(Q)^DRbar
Block Ye Q=  1.25007579E+03  # (SUSY scale)
  1  1     1.10579375E-04   # Y_e(Q)^DRbar
  2  2     2.28643077E-02   # Y_mu(Q)^DRbar
  3  3     3.84540041E-01   # Y_tau(Q)^DRbar
Block Au Q=  1.25007579E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     1.18552663E+02   # A_t(Q)^DRbar
Block Ad Q=  1.25007579E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     3.67036537E+03   # A_b(Q)^DRbar
Block Ae Q=  1.25007579E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.50543993E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  1.25007579E+03  # soft SUSY breaking masses at Q
   1   -3.27430180E+02  # M_1
   2   -1.61791646E+03  # M_2
   3    5.58460861E+02  # M_3
  21    1.33569725E+06  # M^2_(H,d)
  22   -3.74780765E+05  # M^2_(H,u)
  31    7.32742640E+02  # M_(L,11)
  32    7.32742640E+02  # M_(L,22)
  33    7.59805063E+02  # M_(L,33)
  34    1.96271775E+03  # M_(E,11)
  35    1.96271775E+03  # M_(E,22)
  36    1.47589303E+03  # M_(E,33)
  41    7.65066893E+02  # M_(Q,11)
  42    7.65066893E+02  # M_(Q,22)
  43    1.15450655E+03  # M_(Q,33)
  44    3.63603709E+02  # M_(U,11)
  45    3.63603709E+02  # M_(U,22)
  46    1.30088090E+03  # M_(U,33)
  47    7.39318354E+02  # M_(D,11)
  48    7.39318354E+02  # M_(D,22)
  49    1.79044530E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.12505438E+02  # h0
        35     1.32231329E+03  # H0
        36     1.32234139E+03  # A0
        37     1.32513477E+03  # H+
   1000001     8.08180009E+02  # ~d_L
   2000001     7.69924402E+02  # ~d_R
   1000002     8.04419503E+02  # ~u_L
   2000002     4.16618387E+02  # ~u_R
   1000003     8.08186756E+02  # ~s_L
   2000003     7.69914570E+02  # ~s_R
   1000004     8.04418831E+02  # ~c_L
   2000004     4.16618927E+02  # ~c_R
   1000005     1.18131758E+03  # ~b_1
   2000005     1.80167695E+03  # ~b_2
   1000006     1.18881595E+03  # ~t_1
   2000006     1.31449236E+03  # ~t_2
   1000011     7.45766058E+02  # ~e_L-
   2000011     1.96495970E+03  # ~e_R-
   1000012     7.41137401E+02  # ~nu_eL
   1000013     7.45761406E+02  # ~mu_L-
   2000013     1.96494887E+03  # ~mu_R-
   1000014     7.41133981E+02  # ~nu_muL
   1000015     7.70513894E+02  # ~tau_1-
   2000015     1.47350982E+03  # ~tau_2-
   1000016     7.66639830E+02  # ~nu_tauL
   1000021     6.46699809E+02  # ~g
   1000022     3.21729024E+02  # ~chi_10
   1000023     6.41203647E+02  # ~chi_20
   1000025     6.43842410E+02  # ~chi_30
   1000035     1.59300566E+03  # ~chi_40
   1000024     6.40051589E+02  # ~chi_1+
   1000037     1.59308990E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.61998849E-02   # alpha
Block Hmix Q=  1.25007579E+03  # Higgs mixing parameters
   1    6.33728935E+02  # mu
   2    3.75972555E+01  # tan[beta](Q)
   3    2.44331206E+02  # v(Q)
   4    1.74858675E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.99728543E-01   # Re[R_st(1,1)]
   1  2     2.32989222E-02   # Re[R_st(1,2)]
   2  1    -2.32989222E-02   # Re[R_st(2,1)]
   2  2    -9.99728543E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99744173E-01   # Re[R_sb(1,1)]
   1  2     2.26183046E-02   # Re[R_sb(1,2)]
   2  1    -2.26183046E-02   # Re[R_sb(2,1)]
   2  2     9.99744173E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     9.99710040E-01   # Re[R_sta(1,1)]
   1  2     2.40797641E-02   # Re[R_sta(1,2)]
   2  1    -2.40797641E-02   # Re[R_sta(2,1)]
   2  2     9.99710040E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.94825016E-01   # Re[N(1,1)]
   1  2     2.53972369E-03   # Re[N(1,2)]
   1  3    -9.14163454E-02   # Re[N(1,3)]
   1  4    -4.42695113E-02   # Re[N(1,4)]
   2  1     9.59915285E-02   # Re[N(2,1)]
   2  2     5.59185726E-02   # Re[N(2,2)]
   2  3    -7.02777499E-01   # Re[N(2,3)]
   2  4    -7.02682379E-01   # Re[N(2,4)]
   3  1     3.32390152E-02   # Re[N(3,1)]
   3  2    -2.55520915E-02   # Re[N(3,2)]
   3  3    -7.05182480E-01   # Re[N(3,3)]
   3  4     7.07785227E-01   # Re[N(3,4)]
   4  1     1.99558371E-03   # Re[N(4,1)]
   4  2    -9.98105081E-01   # Re[N(4,2)]
   4  3    -2.15524397E-02   # Re[N(4,3)]
   4  4    -5.75999681E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -3.06826326E-02   # Re[U(1,1)]
   1  2     9.99529177E-01   # Re[U(1,2)]
   2  1    -9.99529177E-01   # Re[U(2,1)]
   2  2    -3.06826326E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     8.17762943E-02   # Re[V(1,1)]
   1  2     9.96650710E-01   # Re[V(1,2)]
   2  1     9.96650710E-01   # Re[V(2,1)]
   2  2    -8.17762943E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   1000011     6.11293789E-01   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.95081502E-01    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     4.19580036E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     7.01522511E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000011     9.33572596E+00   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.91288391E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     7.78003555E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     9.31035508E-04    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000013     6.11839862E-01   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.94254131E-01    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     4.61490878E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     4.30111874E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     7.00843121E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     9.36920835E+00   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.87756925E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     8.61242180E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     1.79201619E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.74124777E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000015     8.78381552E-01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     7.58014295E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.24096534E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.16414187E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.47498323E-03    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000015     1.33454336E+01   # ~tau^-_2
#    BR                NDA      ID1      ID2
     4.99306032E-01    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.08793886E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.06541575E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.12934630E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     3.59087358E-02    2     1000016       -24   # BR(~tau^-_2 -> ~nu_tau W^-)
     1.76282004E-02    2     1000015        23   # BR(~tau^-_2 -> ~tau^-_1 Z)
     1.88869416E-02    2     1000015        25   # BR(~tau^-_2 -> ~tau^-_1 h^0)
DECAY   1000012     6.15844090E-01   # ~nu_e
#    BR                NDA      ID1      ID2
     9.94817547E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     5.97945433E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     4.57730093E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
DECAY   1000014     6.16338239E-01   # ~nu_mu
#    BR                NDA      ID1      ID2
     9.94011104E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     5.97429348E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     5.38426606E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
DECAY   1000016     8.65387401E-01   # ~nu_tau
#    BR                NDA      ID1      ID2
     7.54731091E-01    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     6.35169588E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.44626168E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
DECAY   2000001     5.56034050E+00   # ~d_R
#    BR                NDA      ID1      ID2
     5.21211514E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.47804032E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.33679788E+00   # ~d_L
#    BR                NDA      ID1      ID2
     9.75665690E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.11648457E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     9.89986882E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.56003988E+00   # ~s_R
#    BR                NDA      ID1      ID2
     5.21247930E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.47725919E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.33771434E+00   # ~s_L
#    BR                NDA      ID1      ID2
     9.75730789E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.07153050E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.16126925E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     9.89944373E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     6.12942428E+01   # ~b_1
#    BR                NDA      ID1      ID2
     3.64277757E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     3.72508010E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     3.71717508E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     1.75697667E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     7.46237004E-01    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     1.31128593E+02   # ~b_2
#    BR                NDA      ID1      ID2
     7.78648133E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     4.07149955E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     4.09387061E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     8.07571918E-02    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     8.22981660E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     2.71799386E-03    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.38917024E-03    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     2.69646690E-03    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     1.50022371E-01   # ~u_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022         2   # BR(~u_R -> chi^0_1 u)
DECAY   1000002     8.02835008E+00   # ~u_L
#    BR                NDA      ID1      ID2
     9.48871747E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     3.12574731E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     7.94204415E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     9.89382547E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     1.50005083E-01   # ~c_R
#    BR                NDA      ID1      ID2
     1.00000000E+00    2     1000022         4   # BR(~c_R -> chi^0_1 c)
DECAY   1000004     8.02826437E+00   # ~c_L
#    BR                NDA      ID1      ID2
     9.48870033E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     3.15354809E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     8.33392356E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     9.89338138E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     5.85286337E+01   # ~t_1
#    BR                NDA      ID1      ID2
     2.83716407E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     9.76623623E-02    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     9.67413825E-02    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     8.16913266E-02    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.74341532E-03    2     1000021         4   # BR(~t_1 -> ~g c)
     7.19188094E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
     1.22172876E-04    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     8.88777411E+01   # ~t_2
#    BR                NDA      ID1      ID2
     2.86619451E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     7.97000542E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     8.18939109E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.74125681E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     6.35502424E-01    2     1000021         6   # BR(~t_2 -> ~g t)
DECAY   1000024     4.10074939E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     2.78963455E-04    2     2000004        -3   # BR(chi^+_1 -> ~c_R s_bar)
     9.97257513E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     2.43289274E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     9.20344117E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     4.64118441E-02    2    -1000011        12   # BR(chi^+_2 -> ~e^+_L nu_e)
     4.64121691E-02    2    -1000013        14   # BR(chi^+_2 -> ~mu^+_L nu_mu)
     4.46068152E-02    2    -1000015        16   # BR(chi^+_2 -> ~tau^+_1 nu_tau)
     4.64659776E-02    2     1000012       -11   # BR(chi^+_2 -> ~nu_e e^+)
     4.64662830E-02    2     1000014       -13   # BR(chi^+_2 -> ~nu_mu mu^+)
     4.47051714E-02    2     1000016       -15   # BR(chi^+_2 -> ~nu_tau tau^+)
     1.26023344E-01    2     1000002        -1   # BR(chi^+_2 -> ~u_L d_bar)
     1.26023550E-01    2     1000004        -3   # BR(chi^+_2 -> ~c_L s_bar)
     4.43668048E-02    2     1000006        -5   # BR(chi^+_2 -> ~t_1 b_bar)
     5.08362036E-04    2     2000006        -5   # BR(chi^+_2 -> ~t_2 b_bar)
     1.25940622E-01    2    -1000001         2   # BR(chi^+_2 -> ~d^*_L u)
     1.25938841E-01    2    -1000003         4   # BR(chi^+_2 -> ~s^*_L c)
     3.85467829E-02    2    -1000005         6   # BR(chi^+_2 -> ~b^*_1 t)
     3.32564739E-02    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     3.31871538E-02    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     3.31686672E-02    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     3.49178809E-02    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     9.83018493E-04    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     1.03726626E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     9.31649250E-04    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     3.63553960E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     1.81764871E-02    2     2000002        -2   # BR(chi^0_2 -> ~u_R u_bar)
     1.81764871E-02    2    -2000002         2   # BR(chi^0_2 -> ~u^*_R u)
     1.83569587E-02    2     2000004        -4   # BR(chi^0_2 -> ~c_R c_bar)
     1.83569587E-02    2    -2000004         4   # BR(chi^0_2 -> ~c^*_R c)
     1.24785490E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     8.02090196E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     7.22812472E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     1.11389177E-03    2     2000002        -2   # BR(chi^0_3 -> ~u_R u_bar)
     1.11389177E-03    2    -2000002         2   # BR(chi^0_3 -> ~u^*_R u)
     1.19146455E-03    2     2000004        -4   # BR(chi^0_3 -> ~c_R c_bar)
     1.19146455E-03    2    -2000004         4   # BR(chi^0_3 -> ~c^*_R c)
     5.34635505E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     4.60718434E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     9.55655918E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.22347427E-02    2     1000011       -11   # BR(chi^0_4 -> ~e^-_L e^+)
     2.22347427E-02    2    -1000011        11   # BR(chi^0_4 -> ~e^+_L e^-)
     2.22349150E-02    2     1000013       -13   # BR(chi^0_4 -> ~mu^-_L mu^+)
     2.22349150E-02    2    -1000013        13   # BR(chi^0_4 -> ~mu^+_L mu^-)
     2.13743880E-02    2     1000015       -15   # BR(chi^0_4 -> ~tau^-_1 tau^+)
     2.13743880E-02    2    -1000015        15   # BR(chi^0_4 -> ~tau^+_1 tau^-)
     2.24853293E-02    2     1000012       -12   # BR(chi^0_4 -> ~nu_e nu_bar_e)
     2.24853293E-02    2    -1000012        12   # BR(chi^0_4 -> ~nu^*_e nu_e)
     2.24854439E-02    2     1000014       -14   # BR(chi^0_4 -> ~nu_mu nu_bar_mu)
     2.24854439E-02    2    -1000014        14   # BR(chi^0_4 -> ~nu^*_mu nu_mu)
     2.16240810E-02    2     1000016       -16   # BR(chi^0_4 -> ~nu_tau nu_bar_tau)
     2.16240810E-02    2    -1000016        16   # BR(chi^0_4 -> ~nu^*_tau nu_tau)
     6.08096572E-02    2     1000002        -2   # BR(chi^0_4 -> ~u_L u_bar)
     6.08096572E-02    2    -1000002         2   # BR(chi^0_4 -> ~u^*_L u)
     6.08098440E-02    2     1000004        -4   # BR(chi^0_4 -> ~c_L c_bar)
     6.08098440E-02    2    -1000004         4   # BR(chi^0_4 -> ~c^*_L c)
     2.24540986E-02    2     1000006        -6   # BR(chi^0_4 -> ~t_1 t_bar)
     2.24540986E-02    2    -1000006         6   # BR(chi^0_4 -> ~t^*_1 t)
     2.04855480E-04    2     2000006        -6   # BR(chi^0_4 -> ~t_2 t_bar)
     2.04855480E-04    2    -2000006         6   # BR(chi^0_4 -> ~t^*_2 t)
     6.05062864E-02    2     1000001        -1   # BR(chi^0_4 -> ~d_L d_bar)
     6.05062864E-02    2    -1000001         1   # BR(chi^0_4 -> ~d^*_L d)
     6.05055936E-02    2     1000003        -3   # BR(chi^0_4 -> ~s_L s_bar)
     6.05055936E-02    2    -1000003         3   # BR(chi^0_4 -> ~s^*_L s)
     2.21879477E-02    2     1000005        -5   # BR(chi^0_4 -> ~b_1 b_bar)
     2.21879477E-02    2    -1000005         5   # BR(chi^0_4 -> ~b^*_1 b)
     3.27752150E-02    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     3.27752150E-02    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     5.49294924E-03    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     2.66946114E-02    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     2.79205072E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     3.12865624E-02    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     4.09578636E-04    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     4.57787566E-04    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     1.12492854E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     1.12492854E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.31013700E+01   # ~g
#    BR                NDA      ID1      ID2
     2.49992168E-01    2     2000002        -2   # BR(~g -> ~u_R u_bar)
     2.49992168E-01    2    -2000002         2   # BR(~g -> ~u^*_R u)
     2.49988910E-01    2     2000004        -4   # BR(~g -> ~c_R c_bar)
     2.49988910E-01    2    -2000004         4   # BR(~g -> ~c^*_R c)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     2.82430644E-03   # Gamma(h0)
     2.11220706E-03   2        22        22   # BR(h0 -> photon photon)
     5.24221126E-04   2        22        23   # BR(h0 -> photon Z)
     6.51426386E-03   2        23        23   # BR(h0 -> Z Z)
     6.72859587E-02   2       -24        24   # BR(h0 -> W W)
     7.39945705E-02   2        21        21   # BR(h0 -> gluon gluon)
     6.45212718E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.86993029E-04   2       -13        13   # BR(h0 -> Muon muon)
     8.27284219E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.83928303E-07   2        -2         2   # BR(h0 -> Up up)
     3.56808907E-02   2        -4         4   # BR(h0 -> Charm charm)
     7.56484380E-07   2        -1         1   # BR(h0 -> Down down)
     2.73600828E-04   2        -3         3   # BR(h0 -> Strange strange)
     7.30597925E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     2.12653465E+01   # Gamma(HH)
     2.24292062E-09   2        22        22   # BR(HH -> photon photon)
     7.73353696E-10   2        22        23   # BR(HH -> photon Z)
     1.76165670E-06   2        23        23   # BR(HH -> Z Z)
     1.67341664E-06   2       -24        24   # BR(HH -> W W)
     2.89644477E-05   2        21        21   # BR(HH -> gluon gluon)
     1.22045935E-08   2       -11        11   # BR(HH -> Electron electron)
     5.43268497E-04   2       -13        13   # BR(HH -> Muon muon)
     1.57338453E-01   2       -15        15   # BR(HH -> Tau tau)
     5.50080390E-14   2        -2         2   # BR(HH -> Up up)
     1.06475336E-08   2        -4         4   # BR(HH -> Charm charm)
     9.14331743E-04   2        -6         6   # BR(HH -> Top top)
     6.29586929E-07   2        -1         1   # BR(HH -> Down down)
     2.27710497E-04   2        -3         3   # BR(HH -> Strange strange)
     8.06678411E-01   2        -5         5   # BR(HH -> Bottom bottom)
     8.71533730E-05   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     8.17084261E-04   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.20597604E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     2.11933348E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.16072278E-07   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     5.65640233E-05   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     9.01408342E-06   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     1.45871140E-05   2        25        25   # BR(HH -> h0 h0)
     2.62220841E-06   2  -1000002   1000002   # BR(HH -> Sup1 sup1)
     4.24549610E-11   2  -2000002   1000002   # BR(HH -> Sup2 sup1)
     4.24549610E-11   2  -1000002   2000002   # BR(HH -> Sup1 sup2)
     2.62202781E-06   2  -1000004   1000004   # BR(HH -> Scharm1 scharm1)
     1.09560163E-05   2  -2000004   1000004   # BR(HH -> Scharm2 scharm1)
     1.09560163E-05   2  -1000004   2000004   # BR(HH -> Scharm1 scharm2)
DECAY        36     2.06652333E+01   # Gamma(A0)
     1.67723527E-07   2        22        22   # BR(A0 -> photon photon)
     1.43956832E-07   2        22        23   # BR(A0 -> photon Z)
     5.72156557E-05   2        21        21   # BR(A0 -> gluon gluon)
     1.21742146E-08   2       -11        11   # BR(A0 -> Electron electron)
     5.41916330E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.56948237E-01   2       -15        15   # BR(A0 -> Tau tau)
     5.01040218E-14   2        -2         2   # BR(A0 -> Up up)
     9.69363216E-09   2        -4         4   # BR(A0 -> Charm charm)
     9.14915225E-04   2        -6         6   # BR(A0 -> Top top)
     6.28034965E-07   2        -1         1   # BR(A0 -> Down down)
     2.27149112E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.04673494E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.00547629E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.05425000E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     2.23155362E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.20911401E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.31361914E-06   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     3.99620532E-06   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     1.39388938E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     2.45977553E-06   2        23        25   # BR(A0 -> Z h0)
     1.21404130E-37   2        25        25   # BR(A0 -> h0 h0)
     4.36909251E-11   2  -2000002   1000002   # BR(A0 -> Sup2 sup1)
     4.36909251E-11   2  -1000002   2000002   # BR(A0 -> Sup1 sup2)
     1.12749719E-05   2  -2000004   1000004   # BR(A0 -> Scharm2 scharm1)
     1.12749719E-05   2  -1000004   2000004   # BR(A0 -> Scharm1 scharm2)
DECAY        37     2.15519845E+01   # Gamma(Hp)
     1.51085402E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     6.45937273E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.82707265E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     5.85870882E-07   2        -1         2   # BR(Hp -> Down up)
     9.79102519E-06   2        -3         2   # BR(Hp -> Strange up)
     8.40435757E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.81554023E-08   2        -1         4   # BR(Hp -> Down charm)
     2.11168473E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.17690904E-03   2        -5         4   # BR(Hp -> Bottom charm)
     1.13613959E-07   2        -1         6   # BR(Hp -> Down top)
     2.77816278E-06   2        -3         6   # BR(Hp -> Strange top)
     7.81198623E-01   2        -5         6   # BR(Hp -> Bottom top)
     3.37392435E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     2.39292078E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     5.39058474E-05   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     2.38164384E-06   2        24        25   # BR(Hp -> W h0)
     1.12718528E-11   2        24        35   # BR(Hp -> W HH)
     1.07218599E-11   2        24        36   # BR(Hp -> W A0)
     2.44791419E-17   2  -1000001   1000002   # BR(Hp -> Sdown1 sup1)
     1.77443146E-11   2  -2000001   1000002   # BR(Hp -> Sdown2 sup1)
     4.72107910E-16   2  -1000003   1000002   # BR(Hp -> Sstrange1 sup1)
     9.48317829E-13   2  -2000003   1000002   # BR(Hp -> Sstrange2 sup1)
     2.49260149E-13   2  -1000001   1000004   # BR(Hp -> Sdown1 scharm1)
     1.80682297E-07   2  -2000001   1000004   # BR(Hp -> Sdown2 scharm1)
     1.68035912E-09   2  -1000003   1000004   # BR(Hp -> Sstrange1 scharm1)
     3.37531614E-06   2  -2000003   1000004   # BR(Hp -> Sstrange2 scharm1)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.70775501E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.41358285E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.41355362E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00002067E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    6.86762417E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    7.07436906E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.70775501E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.41358285E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.41355362E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999847E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.53297142E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999847E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.53297142E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26717177E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.29595436E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.39247507E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.53297142E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999847E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.40615452E-04   # BR(b -> s gamma)
    2    1.58596354E-06   # BR(b -> s mu+ mu-)
    3    3.51830455E-05   # BR(b -> s nu nu)
    4    2.47327967E-15   # BR(Bd -> e+ e-)
    5    1.05656052E-10   # BR(Bd -> mu+ mu-)
    6    2.21383799E-08   # BR(Bd -> tau+ tau-)
    7    8.19776604E-14   # BR(Bs -> e+ e-)
    8    3.50209498E-09   # BR(Bs -> mu+ mu-)
    9    7.43522324E-07   # BR(Bs -> tau+ tau-)
   10    9.21955771E-05   # BR(B_u -> tau nu)
   11    9.52345015E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.44865122E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.94484752E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16972418E-03   # epsilon_K
   17    2.28179347E-15   # Delta(M_K)
   18    2.47492222E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.27836986E-11   # BR(K^+ -> pi^+ nu nu)
   20   -1.15835596E-14   # Delta(g-2)_electron/2
   21   -4.95243932E-10   # Delta(g-2)_muon/2
   22   -1.38492070E-07   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    1.53745639E-04   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.07719511E-01   # C7
     0305 4322   00   2    -4.48317943E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.25171696E-01   # C8
     0305 6321   00   2    -6.73761890E-04   # C8'
 03051111 4133   00   0     1.58571937E+00   # C9 e+e-
 03051111 4133   00   2     1.58958761E+00   # C9 e+e-
 03051111 4233   00   2    -3.81597876E-06   # C9' e+e-
 03051111 4137   00   0    -4.40841147E+00   # C10 e+e-
 03051111 4137   00   2    -4.39995287E+00   # C10 e+e-
 03051111 4237   00   2     2.24840465E-05   # C10' e+e-
 03051313 4133   00   0     1.58571937E+00   # C9 mu+mu-
 03051313 4133   00   2     1.58958081E+00   # C9 mu+mu-
 03051313 4233   00   2    -3.82197694E-06   # C9' mu+mu-
 03051313 4137   00   0    -4.40841147E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.39995966E+00   # C10 mu+mu-
 03051313 4237   00   2     2.24857420E-05   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50350287E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -4.89791025E-06   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50350287E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -4.89696193E-06   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50350291E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -4.62977897E-06   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85816769E-07   # C7
     0305 4422   00   2    -1.15707794E-05   # C7
     0305 4322   00   2    -1.33001211E-06   # C7'
     0305 6421   00   0     3.30477579E-07   # C8
     0305 6421   00   2    -8.90305868E-05   # C8
     0305 6321   00   2    -3.14290812E-06   # C8'
 03051111 4133   00   2     2.13135823E-06   # C9 e+e-
 03051111 4233   00   2     2.10144153E-06   # C9' e+e-
 03051111 4137   00   2    -3.26288402E-06   # C10 e+e-
 03051111 4237   00   2    -1.66256142E-05   # C10' e+e-
 03051313 4133   00   2     2.13133903E-06   # C9 mu+mu-
 03051313 4233   00   2     2.10143065E-06   # C9' mu+mu-
 03051313 4137   00   2    -3.26290910E-06   # C10 mu+mu-
 03051313 4237   00   2    -1.66256801E-05   # C10' mu+mu-
 03051212 4137   00   2     6.88747751E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     3.62867603E-06   # C11' nu_1 nu_1
 03051414 4137   00   2     6.88744818E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     3.62867603E-06   # C11' nu_2 nu_2
 03051616 4137   00   2     6.89486755E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     3.62867552E-06   # C11' nu_3 nu_3
