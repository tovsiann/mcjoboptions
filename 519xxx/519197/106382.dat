# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  18:27
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.15731059E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    4.13860387E+03  # scale for input parameters
    1    4.40026056E+01  # M_1
    2   -7.71011775E+02  # M_2
    3    4.87880334E+03  # M_3
   11    2.61279523E+03  # A_t
   12    1.94457561E+03  # A_b
   13    1.24809964E+03  # A_tau
   23   -2.02118468E+02  # mu
   25    3.04330629E+01  # tan(beta)
   26    1.38967582E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.97030264E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.08550188E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.91436611E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  4.13860387E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  4.13860387E+03  # (SUSY scale)
  1  1     8.38889727E-06   # Y_u(Q)^DRbar
  2  2     4.26155982E-03   # Y_c(Q)^DRbar
  3  3     1.01344617E+00   # Y_t(Q)^DRbar
Block Yd Q=  4.13860387E+03  # (SUSY scale)
  1  1     5.13015855E-04   # Y_d(Q)^DRbar
  2  2     9.74730125E-03   # Y_s(Q)^DRbar
  3  3     5.08750972E-01   # Y_b(Q)^DRbar
Block Ye Q=  4.13860387E+03  # (SUSY scale)
  1  1     8.95250182E-05   # Y_e(Q)^DRbar
  2  2     1.85109344E-02   # Y_mu(Q)^DRbar
  3  3     3.11323465E-01   # Y_tau(Q)^DRbar
Block Au Q=  4.13860387E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     2.61279512E+03   # A_t(Q)^DRbar
Block Ad Q=  4.13860387E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.94457579E+03   # A_b(Q)^DRbar
Block Ae Q=  4.13860387E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.24809963E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  4.13860387E+03  # soft SUSY breaking masses at Q
   1    4.40026056E+01  # M_1
   2   -7.71011775E+02  # M_2
   3    4.87880334E+03  # M_3
  21    1.90101698E+06  # M^2_(H,d)
  22    3.37216583E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.97030264E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.08550188E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.91436611E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.23803976E+02  # h0
        35     1.38944565E+03  # H0
        36     1.38967582E+03  # A0
        37     1.39377165E+03  # H+
   1000001     1.00990575E+04  # ~d_L
   2000001     1.00745604E+04  # ~d_R
   1000002     1.00986905E+04  # ~u_L
   2000002     1.00776606E+04  # ~u_R
   1000003     1.00990589E+04  # ~s_L
   2000003     1.00745627E+04  # ~s_R
   1000004     1.00986920E+04  # ~c_L
   2000004     1.00776611E+04  # ~c_R
   1000005     4.05877864E+03  # ~b_1
   2000005     4.10008098E+03  # ~b_2
   1000006     4.08371303E+03  # ~t_1
   2000006     4.19423251E+03  # ~t_2
   1000011     1.00212780E+04  # ~e_L-
   2000011     1.00089397E+04  # ~e_R-
   1000012     1.00205076E+04  # ~nu_eL
   1000013     1.00212843E+04  # ~mu_L-
   2000013     1.00089518E+04  # ~mu_R-
   1000014     1.00205138E+04  # ~nu_muL
   1000015     1.00123622E+04  # ~tau_1-
   2000015     1.00230935E+04  # ~tau_2-
   1000016     1.00222828E+04  # ~nu_tauL
   1000021     5.34518340E+03  # ~g
   1000022     4.31144474E+01  # ~chi_10
   1000023     2.20201163E+02  # ~chi_20
   1000025     2.29733912E+02  # ~chi_30
   1000035     8.39833895E+02  # ~chi_40
   1000024     2.19276001E+02  # ~chi_1+
   1000037     8.39912827E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -3.20687388E-02   # alpha
Block Hmix Q=  4.13860387E+03  # Higgs mixing parameters
   1   -2.02118468E+02  # mu
   2    3.04330629E+01  # tan[beta](Q)
   3    2.42922647E+02  # v(Q)
   4    1.93119888E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.14814351E-01   # Re[R_st(1,1)]
   1  2     4.03874614E-01   # Re[R_st(1,2)]
   2  1    -4.03874614E-01   # Re[R_st(2,1)]
   2  2    -9.14814351E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -4.86771083E-02   # Re[R_sb(1,1)]
   1  2     9.98814567E-01   # Re[R_sb(1,2)]
   2  1    -9.98814567E-01   # Re[R_sb(2,1)]
   2  2    -4.86771083E-02   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -6.04371032E-02   # Re[R_sta(1,1)]
   1  2     9.98172008E-01   # Re[R_sta(1,2)]
   2  1    -9.98172008E-01   # Re[R_sta(2,1)]
   2  2    -6.04371032E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.78948130E-01   # Re[N(1,1)]
   1  2     2.55494677E-03   # Re[N(1,2)]
   1  3    -2.01444452E-01   # Re[N(1,3)]
   1  4    -3.27744482E-02   # Re[N(1,4)]
   2  1     1.20724069E-01   # Re[N(2,1)]
   2  2     9.40332345E-02   # Re[N(2,2)]
   2  3     7.01167830E-01   # Re[N(2,3)]
   2  4    -6.96381450E-01   # Re[N(2,4)]
   3  1     1.64493998E-01   # Re[N(3,1)]
   3  2    -5.22452736E-02   # Re[N(3,2)]
   3  3     6.83293648E-01   # Re[N(3,3)]
   3  4     7.09451863E-01   # Re[N(3,4)]
   4  1     5.28991452E-03   # Re[N(4,1)]
   4  2    -9.94193972E-01   # Re[N(4,2)]
   4  3     2.98930957E-02   # Re[N(4,3)]
   4  4    -1.03231609E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -4.22947771E-02   # Re[U(1,1)]
   1  2    -9.99105176E-01   # Re[U(1,2)]
   2  1    -9.99105176E-01   # Re[U(2,1)]
   2  2     4.22947771E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     1.46202156E-01   # Re[V(1,1)]
   1  2     9.89254734E-01   # Re[V(1,2)]
   2  1     9.89254734E-01   # Re[V(2,1)]
   2  2    -1.46202156E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02873180E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.58379112E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.45613544E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     2.70319093E-02    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.43073679E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.51420771E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     7.74018305E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.90991463E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     2.98754485E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     1.10193008E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.06870333E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.04237013E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.55843004E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.51865747E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     2.75899081E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.34941687E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.43142017E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.51208479E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     7.97089379E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     6.13387868E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.98612468E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     1.10140470E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.06580998E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     8.91507853E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     5.52227337E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.15410832E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.12423851E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     7.95962495E-04    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.17533267E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.60874997E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.62135222E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     7.85177223E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     6.49203965E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     5.80478612E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.63460459E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.05911321E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.34947650E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.43078187E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.34853481E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.68731910E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     6.04604675E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02137293E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     1.31656209E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.94896959E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.43146518E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.34455480E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.68603797E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     6.04316441E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.01993260E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     1.36352164E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.94614207E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.62410276E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     7.35609151E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.36786121E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     5.32731673E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.66221647E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.30262463E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.24390872E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     4.16153670E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.29520344E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.96854071E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     3.65386891E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.86485225E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     5.46024136E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.40187707E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     4.27965016E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     5.40718892E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     7.95077377E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     2.90981984E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.60286087E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.56544632E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.16191854E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.29527006E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     2.19246641E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     3.86634162E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.86395296E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     5.46046962E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.40319164E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     4.45070204E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     5.56955787E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     7.95044556E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     2.97502479E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.60279551E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.56513273E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     4.39918017E+01   # ~b_1
#    BR                NDA      ID1      ID2
     7.30158166E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.33820368E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.17712647E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     4.75064644E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.89492062E-04    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.53991964E+02   # ~b_2
#    BR                NDA      ID1      ID2
     7.75304174E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     6.73735315E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     6.55525059E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.06714196E-01    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     5.26202414E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     2.26404311E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
DECAY   2000002     4.33357142E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.97667420E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     7.56151271E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     1.40373253E-03    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.48071941E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     5.46005390E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.54689783E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.08556361E-03    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     7.92067358E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     3.47705287E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.57140587E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.56500316E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     4.33364437E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.97659138E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     7.60208408E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     1.40793431E-03    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.48056031E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     5.46028194E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.54679893E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.08875651E-03    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     7.92035035E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     3.51167367E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.57134111E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.56468953E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.58503277E+02   # ~t_1
#    BR                NDA      ID1      ID2
     1.61972134E-04    2     1000022         4   # BR(~t_1 -> chi^0_1 c)
     9.40464754E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.58947090E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.81405881E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     7.30611137E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.34462876E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.38367525E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     4.12018095E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.75040508E+02   # ~t_2
#    BR                NDA      ID1      ID2
     4.66754131E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.31938625E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.28111531E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     3.60480294E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     3.85366694E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     7.07748435E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     5.73476235E-04    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     4.96571228E-04    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
DECAY   1000024     1.18494817E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99999669E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     7.38383198E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     4.02202419E-03    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.56503970E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.45949312E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.47790635E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.37862667E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.95915563E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     2.88837232E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     2.94243391E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.39798535E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     6.73714946E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     3.26251783E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     9.79579707E-02   # chi^0_3
#    BR                NDA      ID1      ID2
     5.34071709E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     4.65914872E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     8.29758648E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.29360591E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.29360591E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     2.13816969E-03    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     5.19173815E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.72384953E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.52259851E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.61388326E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.44210448E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     6.81367093E-04    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     6.22580469E-04    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     3.15810301E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     3.15810301E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.07086752E+02   # ~g
#    BR                NDA      ID1      ID2
     5.84621581E-03    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     5.84621581E-03    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     1.41701998E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.41701998E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     9.46895581E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     9.46895581E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     1.32485821E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.32485821E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.25116385E-01    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     1.25116385E-01    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.58390727E-03   # Gamma(h0)
     2.44315238E-03   2        22        22   # BR(h0 -> photon photon)
     1.45472040E-03   2        22        23   # BR(h0 -> photon Z)
     2.57373229E-02   2        23        23   # BR(h0 -> Z Z)
     2.17759398E-01   2       -24        24   # BR(h0 -> W W)
     7.63252501E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.97643061E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.21359894E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.38244179E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.38611079E-07   2        -2         2   # BR(h0 -> Up up)
     2.69004122E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.77075646E-07   2        -1         1   # BR(h0 -> Down down)
     2.08714864E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.55649746E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     2.94747845E-02   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     2.78603590E+01   # Gamma(HH)
     7.35694857E-08   2        22        22   # BR(HH -> photon photon)
     1.86048298E-07   2        22        23   # BR(HH -> photon Z)
     3.76141549E-06   2        23        23   # BR(HH -> Z Z)
     3.34141794E-06   2       -24        24   # BR(HH -> W W)
     2.08387178E-05   2        21        21   # BR(HH -> gluon gluon)
     7.13392308E-09   2       -11        11   # BR(HH -> Electron electron)
     3.17560657E-04   2       -13        13   # BR(HH -> Muon muon)
     9.17039152E-02   2       -15        15   # BR(HH -> Tau tau)
     1.26208886E-13   2        -2         2   # BR(HH -> Up up)
     2.44785278E-08   2        -4         4   # BR(HH -> Charm charm)
     1.77190971E-03   2        -6         6   # BR(HH -> Top top)
     5.82207393E-07   2        -1         1   # BR(HH -> Down down)
     2.10589236E-04   2        -3         3   # BR(HH -> Strange strange)
     5.78750844E-01   2        -5         5   # BR(HH -> Bottom bottom)
     8.68507727E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     8.67254024E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     8.67254024E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     5.10161285E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     2.54859703E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     2.12096502E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     4.01571251E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.82074299E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     1.23161352E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     3.00848189E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     3.99398725E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     5.37545421E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     2.05024434E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     2.72842391E+01   # Gamma(A0)
     2.82266919E-07   2        22        22   # BR(A0 -> photon photon)
     4.55202837E-07   2        22        23   # BR(A0 -> photon Z)
     4.35438451E-05   2        21        21   # BR(A0 -> gluon gluon)
     7.06441832E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.14466638E-04   2       -13        13   # BR(A0 -> Muon muon)
     9.08110711E-02   2       -15        15   # BR(A0 -> Tau tau)
     1.21260910E-13   2        -2         2   # BR(A0 -> Up up)
     2.35161748E-08   2        -4         4   # BR(A0 -> Charm charm)
     1.82209912E-03   2        -6         6   # BR(A0 -> Top top)
     5.76523103E-07   2        -1         1   # BR(A0 -> Down down)
     2.08533049E-04   2        -3         3   # BR(A0 -> Strange strange)
     5.73111919E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.00227110E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     8.78182483E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     8.78182483E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     5.11115439E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     2.89250566E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.91090472E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     3.48586011E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     2.30111950E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.39744689E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     5.73070518E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     3.93164339E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     2.85252958E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     5.14671686E-06   2        23        25   # BR(A0 -> Z h0)
     6.16525070E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     2.88988299E+01   # Gamma(Hp)
     7.95072339E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.39918254E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     9.61479672E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     5.64297413E-07   2        -1         2   # BR(Hp -> Down up)
     9.43671334E-06   2        -3         2   # BR(Hp -> Strange up)
     6.29735597E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.73484578E-08   2        -1         4   # BR(Hp -> Down charm)
     2.03396549E-04   2        -3         4   # BR(Hp -> Strange charm)
     8.81854034E-04   2        -5         4   # BR(Hp -> Bottom charm)
     1.32484202E-07   2        -1         6   # BR(Hp -> Down top)
     3.17967402E-06   2        -3         6   # BR(Hp -> Strange top)
     5.87640738E-01   2        -5         6   # BR(Hp -> Bottom top)
     5.48844771E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     7.24644325E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     2.76191103E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     8.59383773E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     1.14007924E-05   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     7.90574582E-02   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     8.48614941E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     4.91779149E-06   2        24        25   # BR(Hp -> W h0)
     7.11918860E-11   2        24        35   # BR(Hp -> W HH)
     5.41675282E-11   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.53179881E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    9.26218138E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    9.26171317E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00005055E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.02916152E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.07971385E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.53179881E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    9.26218138E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    9.26171317E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999394E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    6.05972285E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999394E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    6.05972285E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26896348E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    7.47021482E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.20763847E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    6.05972285E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999394E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.41671602E-04   # BR(b -> s gamma)
    2    1.58803739E-06   # BR(b -> s mu+ mu-)
    3    3.52509821E-05   # BR(b -> s nu nu)
    4    2.61635141E-15   # BR(Bd -> e+ e-)
    5    1.11767460E-10   # BR(Bd -> mu+ mu-)
    6    2.33914614E-08   # BR(Bd -> tau+ tau-)
    7    8.88679342E-14   # BR(Bs -> e+ e-)
    8    3.79643119E-09   # BR(Bs -> mu+ mu-)
    9    8.05012076E-07   # BR(Bs -> tau+ tau-)
   10    9.38423413E-05   # BR(B_u -> tau nu)
   11    9.69355458E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41895862E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93473892E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15707589E-03   # epsilon_K
   17    2.28165922E-15   # Delta(M_K)
   18    2.47978465E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29000508E-11   # BR(K^+ -> pi^+ nu nu)
   20    2.73324895E-16   # Delta(g-2)_electron/2
   21    1.16855394E-11   # Delta(g-2)_muon/2
   22    3.30906508E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.60229616E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.10178004E-01   # C7
     0305 4322   00   2    -4.35302049E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.21815529E-01   # C8
     0305 6321   00   2    -5.33673502E-04   # C8'
 03051111 4133   00   0     1.62789211E+00   # C9 e+e-
 03051111 4133   00   2     1.62811149E+00   # C9 e+e-
 03051111 4233   00   2     1.39270818E-04   # C9' e+e-
 03051111 4137   00   0    -4.45058421E+00   # C10 e+e-
 03051111 4137   00   2    -4.44878703E+00   # C10 e+e-
 03051111 4237   00   2    -1.02844676E-03   # C10' e+e-
 03051313 4133   00   0     1.62789211E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62811143E+00   # C9 mu+mu-
 03051313 4233   00   2     1.39269028E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.45058421E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44878710E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.02844511E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50496285E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     2.22328150E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50496285E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     2.22328530E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50496285E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     2.22435464E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85823798E-07   # C7
     0305 4422   00   2     4.39223382E-06   # C7
     0305 4322   00   2     1.53373434E-07   # C7'
     0305 6421   00   0     3.30483599E-07   # C8
     0305 6421   00   2     3.70850184E-06   # C8
     0305 6321   00   2     9.73653993E-08   # C8'
 03051111 4133   00   2     3.24536291E-07   # C9 e+e-
 03051111 4233   00   2     3.97792407E-06   # C9' e+e-
 03051111 4137   00   2     6.45073645E-08   # C10 e+e-
 03051111 4237   00   2    -2.93859722E-05   # C10' e+e-
 03051313 4133   00   2     3.24534632E-07   # C9 mu+mu-
 03051313 4233   00   2     3.97792345E-06   # C9' mu+mu-
 03051313 4137   00   2     6.45091014E-08   # C10 mu+mu-
 03051313 4237   00   2    -2.93859741E-05   # C10' mu+mu-
 03051212 4137   00   2     8.16059758E-09   # C11 nu_1 nu_1
 03051212 4237   00   2     6.35262432E-06   # C11' nu_1 nu_1
 03051414 4137   00   2     8.16068102E-09   # C11 nu_2 nu_2
 03051414 4237   00   2     6.35262432E-06   # C11' nu_2 nu_2
 03051616 4137   00   2     8.18418118E-09   # C11 nu_3 nu_3
 03051616 4237   00   2     6.35262431E-06   # C11' nu_3 nu_3
