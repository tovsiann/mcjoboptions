# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  18:52
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.96490876E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.72595693E+03  # scale for input parameters
    1    4.32548164E+01  # M_1
    2    1.41564416E+03  # M_2
    3    4.83727792E+03  # M_3
   11    3.62781962E+03  # A_t
   12   -8.98789717E+02  # A_b
   13   -4.03727035E+01  # A_tau
   23   -3.75863759E+02  # mu
   25    3.86039165E+01  # tan(beta)
   26    2.18768204E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.63430357E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.84503807E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.93379323E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.72595693E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.72595693E+03  # (SUSY scale)
  1  1     8.38718471E-06   # Y_u(Q)^DRbar
  2  2     4.26068983E-03   # Y_c(Q)^DRbar
  3  3     1.01323928E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.72595693E+03  # (SUSY scale)
  1  1     6.50620620E-04   # Y_d(Q)^DRbar
  2  2     1.23617918E-02   # Y_s(Q)^DRbar
  3  3     6.45211780E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.72595693E+03  # (SUSY scale)
  1  1     1.13538056E-04   # Y_e(Q)^DRbar
  2  2     2.34760690E-02   # Y_mu(Q)^DRbar
  3  3     3.94828860E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.72595693E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     3.62781874E+03   # A_t(Q)^DRbar
Block Ad Q=  3.72595693E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -8.98789442E+02   # A_b(Q)^DRbar
Block Ae Q=  3.72595693E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -4.03727054E+01   # A_tau(Q)^DRbar
Block MSOFT Q=  3.72595693E+03  # soft SUSY breaking masses at Q
   1    4.32548164E+01  # M_1
   2    1.41564416E+03  # M_2
   3    4.83727792E+03  # M_3
  21    4.59084147E+06  # M^2_(H,d)
  22    9.09483167E+04  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.63430357E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.84503807E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.93379323E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.23993095E+02  # h0
        35     2.18724090E+03  # H0
        36     2.18768204E+03  # A0
        37     2.18784438E+03  # H+
   1000001     1.00898334E+04  # ~d_L
   2000001     1.00663119E+04  # ~d_R
   1000002     1.00894671E+04  # ~u_L
   2000002     1.00691194E+04  # ~u_R
   1000003     1.00898371E+04  # ~s_L
   2000003     1.00663183E+04  # ~s_R
   1000004     1.00894708E+04  # ~c_L
   2000004     1.00691202E+04  # ~c_R
   1000005     4.73219925E+03  # ~b_1
   2000005     5.03592155E+03  # ~b_2
   1000006     2.93156458E+03  # ~t_1
   2000006     4.73561289E+03  # ~t_2
   1000011     1.00204192E+04  # ~e_L-
   2000011     1.00091453E+04  # ~e_R-
   1000012     1.00196564E+04  # ~nu_eL
   1000013     1.00204366E+04  # ~mu_L-
   2000013     1.00091778E+04  # ~mu_R-
   1000014     1.00196733E+04  # ~nu_muL
   1000015     1.00182662E+04  # ~tau_1-
   2000015     1.00254546E+04  # ~tau_2-
   1000016     1.00244533E+04  # ~nu_tauL
   1000021     5.30726752E+03  # ~g
   1000022     4.32365328E+01  # ~chi_10
   1000023     3.85581795E+02  # ~chi_20
   1000025     3.89544484E+02  # ~chi_30
   1000035     1.51689211E+03  # ~chi_40
   1000024     3.84556934E+02  # ~chi_1+
   1000037     1.51685802E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.53262498E-02   # alpha
Block Hmix Q=  3.72595693E+03  # Higgs mixing parameters
   1   -3.75863759E+02  # mu
   2    3.86039165E+01  # tan[beta](Q)
   3    2.43026348E+02  # v(Q)
   4    4.78595271E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -3.60670048E-02   # Re[R_st(1,1)]
   1  2     9.99349374E-01   # Re[R_st(1,2)]
   2  1    -9.99349374E-01   # Re[R_st(2,1)]
   2  2    -3.60670048E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -9.99945822E-01   # Re[R_sb(1,1)]
   1  2     1.04092813E-02   # Re[R_sb(1,2)]
   2  1    -1.04092813E-02   # Re[R_sb(2,1)]
   2  2    -9.99945822E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -1.83465879E-01   # Re[R_sta(1,1)]
   1  2     9.83026079E-01   # Re[R_sta(1,2)]
   2  1    -9.83026079E-01   # Re[R_sta(2,1)]
   2  2    -1.83465879E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.93024333E-01   # Re[N(1,1)]
   1  2     3.36053517E-04   # Re[N(1,2)]
   1  3     1.17452629E-01   # Re[N(1,3)]
   1  4     1.03653619E-02   # Re[N(1,4)]
   2  1    -9.05248165E-02   # Re[N(2,1)]
   2  2    -4.85419066E-02   # Re[N(2,2)]
   2  3    -7.03125017E-01   # Re[N(2,3)]
   2  4    -7.03607953E-01   # Re[N(2,4)]
   3  1     7.55305342E-02   # Re[N(3,1)]
   3  2    -3.06016747E-02   # Re[N(3,2)]
   3  3     7.01183238E-01   # Re[N(3,3)]
   3  4    -7.08308367E-01   # Re[N(3,4)]
   4  1     1.75206409E-03   # Re[N(4,1)]
   4  2    -9.98352196E-01   # Re[N(4,2)]
   4  3     1.27341012E-02   # Re[N(4,3)]
   4  4     5.59255314E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.80263189E-02   # Re[U(1,1)]
   1  2    -9.99837513E-01   # Re[U(1,2)]
   2  1     9.99837513E-01   # Re[U(2,1)]
   2  2    -1.80263189E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -7.91719594E-02   # Re[V(1,1)]
   1  2     9.96860974E-01   # Re[V(1,2)]
   2  1     9.96860974E-01   # Re[V(2,1)]
   2  2     7.91719594E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02883555E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.86137851E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     8.17110403E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     5.68806366E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.38917840E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.92330181E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.97243235E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.01541945E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     2.05728712E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.06016787E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.05073502E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.81895220E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     9.20669732E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     6.72844318E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     2.16571087E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.39027641E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.91735990E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.35961576E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     4.17417162E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.01304485E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     2.05566590E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.05539316E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.14252296E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     4.34510532E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.37660975E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.29925109E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.09618694E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.64907278E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     2.20342363E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.68030244E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     7.88710064E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     9.36584730E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     9.47265562E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.42033220E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     4.35172959E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.86359015E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.38921671E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.94501340E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.59329823E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02641342E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     3.96807963E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.02347135E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.39031456E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.93796509E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.59204279E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02402921E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     4.75252677E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.01872848E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.69991102E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     7.31362499E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.30271678E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.47456819E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.85534715E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.92569490E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     4.19847056E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.31991239E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.09470281E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     9.86614931E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     5.45377477E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.55588968E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.57626571E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     7.75839808E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.55533110E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.64031200E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.19908585E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.31982008E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.45279733E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     1.11848594E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.86471954E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     5.45412047E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.55650230E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.13066750E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.85108965E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     7.75791026E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.55523354E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.63983481E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     2.06612330E+02   # ~b_1
#    BR                NDA      ID1      ID2
     5.96067830E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     9.26441329E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     9.20528681E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     8.09267795E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.57508010E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.65024776E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     1.05882755E-01    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
DECAY   2000005     8.52012465E+01   # ~b_2
#    BR                NDA      ID1      ID2
     3.88215671E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     2.39443724E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.38351772E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.67107675E-04    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     4.82480916E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     3.35055764E-04    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.52904302E-04    2     2000006       -24   # BR(~b_2 -> ~t_2 W^-)
     1.28753681E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     4.37005126E+02   # ~u_R
#    BR                NDA      ID1      ID2
     5.07376914E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     4.20424129E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     2.92665205E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.48549068E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     5.45358450E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.53652854E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     3.39482894E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     7.74866878E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.01804189E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.54607699E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.63987676E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     4.37012430E+02   # ~c_R
#    BR                NDA      ID1      ID2
     5.07368463E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     4.24525467E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     2.96821834E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.48533336E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     5.45392989E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.53636946E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     3.42758704E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     7.74818320E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.07388266E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.54597986E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.63939947E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.21370522E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.26859080E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.35154769E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.36620744E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.38610872E-03    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.71360838E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.79145474E-03    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     2.13424140E+02   # ~t_2
#    BR                NDA      ID1      ID2
     3.22593876E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.20991141E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.24343642E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     7.89289641E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.84519001E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.54798719E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     5.13939727E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     8.17986215E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     2.56361698E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.98708883E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.29103949E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     1.30412106E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     2.55765361E-04    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.44900183E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.44862147E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.43757790E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.43238266E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     8.33442837E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     7.03806631E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     7.50350127E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.21525670E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     4.93357398E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     5.06637373E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     2.87724639E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     5.55974611E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     4.43994088E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.47014895E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.17996816E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.17996816E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     6.14523539E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.56065370E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.15950789E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.54870645E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.72184119E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.36523132E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     3.51753958E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     6.13520999E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     6.13520999E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     9.05677559E+01   # ~g
#    BR                NDA      ID1      ID2
     4.21045299E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     4.21045299E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     3.37178552E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     3.37178552E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     3.64413580E-02    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     3.64413580E-02    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     8.60674692E-03    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     8.60674692E-03    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.52926201E-03   # Gamma(h0)
     2.48929206E-03   2        22        22   # BR(h0 -> photon photon)
     1.50191408E-03   2        22        23   # BR(h0 -> photon Z)
     2.67741874E-02   2        23        23   # BR(h0 -> Z Z)
     2.25670612E-01   2       -24        24   # BR(h0 -> W W)
     7.79323877E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.10882652E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.27249188E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.55227921E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.44624914E-07   2        -2         2   # BR(h0 -> Up up)
     2.80670258E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.92257700E-07   2        -1         1   # BR(h0 -> Down down)
     2.14205793E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.69946399E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     1.65319352E-03   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     5.43016089E+01   # Gamma(HH)
     2.98498078E-08   2        22        22   # BR(HH -> photon photon)
     3.86984318E-08   2        22        23   # BR(HH -> photon Z)
     5.49006018E-07   2        23        23   # BR(HH -> Z Z)
     2.56103688E-07   2       -24        24   # BR(HH -> W W)
     1.38866502E-05   2        21        21   # BR(HH -> gluon gluon)
     8.97555408E-09   2       -11        11   # BR(HH -> Electron electron)
     3.99595831E-04   2       -13        13   # BR(HH -> Muon muon)
     1.15402333E-01   2       -15        15   # BR(HH -> Tau tau)
     7.37734305E-14   2        -2         2   # BR(HH -> Up up)
     1.43096548E-08   2        -4         4   # BR(HH -> Charm charm)
     1.08407944E-03   2        -6         6   # BR(HH -> Top top)
     6.99013833E-07   2        -1         1   # BR(HH -> Down down)
     2.52842215E-04   2        -3         3   # BR(HH -> Strange strange)
     7.03425523E-01   2        -5         5   # BR(HH -> Bottom bottom)
     2.01740918E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     4.41723061E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     4.41723061E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     1.23788799E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     2.27873430E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.96906022E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     5.96843028E-04   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     4.16363446E-06   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     3.60224177E-04   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.39723269E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     6.57762725E-04   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     2.97488987E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     2.07044057E-06   2        25        25   # BR(HH -> h0 h0)
DECAY        36     5.23183693E+01   # Gamma(A0)
     1.39223479E-08   2        22        22   # BR(A0 -> photon photon)
     6.35467430E-08   2        22        23   # BR(A0 -> photon Z)
     2.55607656E-05   2        21        21   # BR(A0 -> gluon gluon)
     8.89922091E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.96196530E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.14420955E-01   2       -15        15   # BR(A0 -> Tau tau)
     7.00281746E-14   2        -2         2   # BR(A0 -> Up up)
     1.35815343E-08   2        -4         4   # BR(A0 -> Charm charm)
     1.05981391E-03   2        -6         6   # BR(A0 -> Top top)
     6.93050563E-07   2        -1         1   # BR(A0 -> Down down)
     2.50684943E-04   2        -3         3   # BR(A0 -> Strange strange)
     6.97433103E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.31617764E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     4.57805764E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     4.57805764E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.27549920E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     2.16857371E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     2.23817334E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     6.91307878E-04   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     4.42946245E-06   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     2.92842207E-04   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     3.10850524E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     8.60246300E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     1.42579270E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     7.87633950E-07   2        23        25   # BR(A0 -> Z h0)
     4.43671465E-40   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     5.80215986E+01   # Gamma(Hp)
     9.80288253E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     4.19103844E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.18546326E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     6.57657022E-07   2        -1         2   # BR(Hp -> Down up)
     1.10605749E-05   2        -3         2   # BR(Hp -> Strange up)
     7.55416957E-06   2        -5         2   # BR(Hp -> Bottom up)
     3.11839131E-08   2        -1         4   # BR(Hp -> Down charm)
     2.37033672E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.05785370E-03   2        -5         4   # BR(Hp -> Bottom charm)
     6.35618796E-08   2        -1         6   # BR(Hp -> Down top)
     1.73344414E-06   2        -3         6   # BR(Hp -> Strange top)
     7.12601252E-01   2        -5         6   # BR(Hp -> Bottom top)
     4.15317610E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     1.18466791E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     8.71247353E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     4.13752265E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     2.45724259E-05   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     4.06875836E-02   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     4.14415506E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     7.11208207E-07   2        24        25   # BR(Hp -> W h0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.56319418E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.49030605E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.49026237E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00002931E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    6.41712116E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    6.71022781E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.56319418E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.49030605E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.49026237E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999673E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    3.27259328E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999673E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    3.27259328E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26747560E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    8.76607291E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.27430295E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    3.27259328E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999673E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.28972475E-04   # BR(b -> s gamma)
    2    1.58898352E-06   # BR(b -> s mu+ mu-)
    3    3.52469084E-05   # BR(b -> s nu nu)
    4    2.76325724E-15   # BR(Bd -> e+ e-)
    5    1.18042498E-10   # BR(Bd -> mu+ mu-)
    6    2.46695792E-08   # BR(Bd -> tau+ tau-)
    7    9.37053411E-14   # BR(Bs -> e+ e-)
    8    4.00306409E-09   # BR(Bs -> mu+ mu-)
    9    8.47609023E-07   # BR(Bs -> tau+ tau-)
   10    9.50356983E-05   # BR(B_u -> tau nu)
   11    9.81682379E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41955688E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93555549E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15730925E-03   # epsilon_K
   17    2.28166022E-15   # Delta(M_K)
   18    2.47957506E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28947791E-11   # BR(K^+ -> pi^+ nu nu)
   20   -4.10774605E-16   # Delta(g-2)_electron/2
   21   -1.75619865E-11   # Delta(g-2)_muon/2
   22   -4.97422072E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.82297023E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.97317247E-01   # C7
     0305 4322   00   2    -2.06820894E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.07785427E-01   # C8
     0305 6321   00   2    -2.68852278E-04   # C8'
 03051111 4133   00   0     1.62452294E+00   # C9 e+e-
 03051111 4133   00   2     1.62477615E+00   # C9 e+e-
 03051111 4233   00   2     2.27459865E-04   # C9' e+e-
 03051111 4137   00   0    -4.44721504E+00   # C10 e+e-
 03051111 4137   00   2    -4.44504523E+00   # C10 e+e-
 03051111 4237   00   2    -1.68840256E-03   # C10' e+e-
 03051313 4133   00   0     1.62452294E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62477602E+00   # C9 mu+mu-
 03051313 4233   00   2     2.27458908E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44721504E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44504537E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.68840189E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50488157E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     3.65273545E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50488157E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     3.65273736E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50488158E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     3.65327658E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85831089E-07   # C7
     0305 4422   00   2    -5.45138197E-06   # C7
     0305 4322   00   2     2.40131447E-07   # C7'
     0305 6421   00   0     3.30489844E-07   # C8
     0305 6421   00   2     1.58521555E-06   # C8
     0305 6321   00   2     1.63921146E-07   # C8'
 03051111 4133   00   2     9.53604239E-08   # C9 e+e-
 03051111 4233   00   2     5.23848120E-06   # C9' e+e-
 03051111 4137   00   2     5.12908583E-07   # C10 e+e-
 03051111 4237   00   2    -3.88903059E-05   # C10' e+e-
 03051313 4133   00   2     9.53587531E-08   # C9 mu+mu-
 03051313 4233   00   2     5.23847992E-06   # C9' mu+mu-
 03051313 4137   00   2     5.12910466E-07   # C10 mu+mu-
 03051313 4237   00   2    -3.88903097E-05   # C10' mu+mu-
 03051212 4137   00   2    -9.53173886E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     8.41363567E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -9.53173097E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     8.41363567E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -9.52953722E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     8.41363565E-06   # C11' nu_3 nu_3
