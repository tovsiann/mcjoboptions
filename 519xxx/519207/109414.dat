# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  22:11
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    1.16162231E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.96538896E+03  # scale for input parameters
    1    6.16316619E+01  # M_1
    2    1.54527231E+03  # M_2
    3    2.11514937E+03  # M_3
   11    4.60025107E+02  # A_t
   12    1.15192266E+03  # A_b
   13   -1.04304037E+03  # A_tau
   23    2.27371550E+02  # mu
   25    1.10772803E+01  # tan(beta)
   26    2.95405188E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.14677537E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.70451900E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.98596336E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.96538896E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.96538896E+03  # (SUSY scale)
  1  1     8.41846725E-06   # Y_u(Q)^DRbar
  2  2     4.27658136E-03   # Y_c(Q)^DRbar
  3  3     1.01701846E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.96538896E+03  # (SUSY scale)
  1  1     1.87390003E-04   # Y_d(Q)^DRbar
  2  2     3.56041007E-03   # Y_s(Q)^DRbar
  3  3     1.85832164E-01   # Y_b(Q)^DRbar
Block Ye Q=  2.96538896E+03  # (SUSY scale)
  1  1     3.27009259E-05   # Y_e(Q)^DRbar
  2  2     6.76151434E-03   # Y_mu(Q)^DRbar
  3  3     1.13717548E-01   # Y_tau(Q)^DRbar
Block Au Q=  2.96538896E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     4.60025111E+02   # A_t(Q)^DRbar
Block Ad Q=  2.96538896E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.15192266E+03   # A_b(Q)^DRbar
Block Ae Q=  2.96538896E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.04304036E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  2.96538896E+03  # soft SUSY breaking masses at Q
   1    6.16316619E+01  # M_1
   2    1.54527231E+03  # M_2
   3    2.11514937E+03  # M_3
  21    8.55388160E+06  # M^2_(H,d)
  22    1.92148388E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.14677537E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.70451900E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.98596336E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.19280159E+02  # h0
        35     2.95387190E+03  # H0
        36     2.95405188E+03  # A0
        37     2.95628399E+03  # H+
   1000001     1.01119105E+04  # ~d_L
   2000001     1.00883636E+04  # ~d_R
   1000002     1.01115488E+04  # ~u_L
   2000002     1.00915509E+04  # ~u_R
   1000003     1.01119118E+04  # ~s_L
   2000003     1.00883647E+04  # ~s_R
   1000004     1.01115501E+04  # ~c_L
   2000004     1.00915525E+04  # ~c_R
   1000005     3.05340769E+03  # ~b_1
   2000005     3.20244038E+03  # ~b_2
   1000006     2.74367759E+03  # ~t_1
   2000006     3.20501640E+03  # ~t_2
   1000011     1.00199763E+04  # ~e_L-
   2000011     1.00088988E+04  # ~e_R-
   1000012     1.00192154E+04  # ~nu_eL
   1000013     1.00199791E+04  # ~mu_L-
   2000013     1.00089042E+04  # ~mu_R-
   1000014     1.00192182E+04  # ~nu_muL
   1000015     1.00104308E+04  # ~tau_1-
   2000015     1.00207755E+04  # ~tau_2-
   1000016     1.00200040E+04  # ~nu_tauL
   1000021     2.51333032E+03  # ~g
   1000022     5.82380505E+01  # ~chi_10
   1000023     2.41963832E+02  # ~chi_20
   1000025     2.42122219E+02  # ~chi_30
   1000035     1.64870208E+03  # ~chi_40
   1000024     2.36927071E+02  # ~chi_1+
   1000037     1.64866809E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -8.60949012E-02   # alpha
Block Hmix Q=  2.96538896E+03  # Higgs mixing parameters
   1    2.27371550E+02  # mu
   2    1.10772803E+01  # tan[beta](Q)
   3    2.43273387E+02  # v(Q)
   4    8.72642251E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -1.01442826E-02   # Re[R_st(1,1)]
   1  2     9.99948545E-01   # Re[R_st(1,2)]
   2  1    -9.99948545E-01   # Re[R_st(2,1)]
   2  2    -1.01442826E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     3.42928619E-03   # Re[R_sb(1,1)]
   1  2     9.99994120E-01   # Re[R_sb(1,2)]
   2  1    -9.99994120E-01   # Re[R_sb(2,1)]
   2  2     3.42928619E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     3.04752628E-02   # Re[R_sta(1,1)]
   1  2     9.99535521E-01   # Re[R_sta(1,2)]
   2  1    -9.99535521E-01   # Re[R_sta(2,1)]
   2  2     3.04752628E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.77492275E-01   # Re[N(1,1)]
   1  2     4.14504412E-03   # Re[N(1,2)]
   1  3    -2.00475689E-01   # Re[N(1,3)]
   1  4     6.55833023E-02   # Re[N(1,4)]
   2  1     1.88354270E-01   # Re[N(2,1)]
   2  2     4.32958947E-02   # Re[N(2,2)]
   2  3    -6.88974420E-01   # Re[N(2,3)]
   2  4     6.98543043E-01   # Re[N(2,4)]
   3  1     9.50227414E-02   # Re[N(3,1)]
   3  2    -2.75925301E-02   # Re[N(3,2)]
   3  3    -6.96413966E-01   # Re[N(3,3)]
   3  4    -7.10786127E-01   # Re[N(3,4)]
   4  1     1.48326910E-03   # Re[N(4,1)]
   4  2    -9.98672587E-01   # Re[N(4,2)]
   4  3    -1.14601335E-02   # Re[N(4,3)]
   4  4     5.01949087E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.61996369E-02   # Re[U(1,1)]
   1  2     9.99868777E-01   # Re[U(1,2)]
   2  1     9.99868777E-01   # Re[U(2,1)]
   2  2     1.61996369E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -7.10089411E-02   # Re[V(1,1)]
   1  2     9.97475679E-01   # Re[V(1,2)]
   2  1     9.97475679E-01   # Re[V(2,1)]
   2  2     7.10089411E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02851851E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.55538083E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     3.54400179E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     9.01981255E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.37838402E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.58650010E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     6.61952898E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.71580544E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.01589440E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     1.67746864E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.05586703E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.03033981E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.55199905E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     3.55130089E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     9.10421707E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.80720104E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.37847547E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.58619853E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     6.65043671E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     2.03595180E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.01569533E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     1.67735782E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.05546714E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.55178235E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     8.68100971E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     5.36233896E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     2.99983447E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     5.75609160E-04    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     4.65486914E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.15299458E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.40345487E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.48277052E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.53994367E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     9.32212894E-03    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.96005634E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     5.94369361E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.37842051E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.86260097E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.05018186E-03    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     1.96195159E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02514598E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     3.22274228E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.02624517E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.37851196E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.86201552E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.05011249E-03    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     1.96182199E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02494624E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     3.28854223E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.02584744E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.40428904E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.70002710E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.03091771E-03    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     1.92596226E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.96967926E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     2.14949420E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.91579981E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.06159158E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.62040162E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     2.82643191E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     9.92024990E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.30531179E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.70183950E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.06067901E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     5.06703006E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.01530030E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.45958568E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.06164323E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.62049108E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     2.84344720E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     9.92017869E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.30537526E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.70194976E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.07550456E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.06699211E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.01529290E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.45952241E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     3.08972090E+01   # ~b_1
#    BR                NDA      ID1      ID2
     5.55281187E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     3.37155189E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     3.29457851E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     6.66523189E-02    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     8.11157383E-01    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     1.27521670E+02   # ~b_2
#    BR                NDA      ID1      ID2
     4.16948563E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     8.11855720E-03    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     8.50547895E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     5.95823462E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     5.05491244E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.21437953E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     2.92370082E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     3.23732242E-04    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
DECAY   2000002     7.23340998E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.97669646E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.10404879E-03    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     2.80990603E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.68847931E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.30518484E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.54715618E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     3.16018619E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     5.06154888E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     5.39830513E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.01042141E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.45933270E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.23348445E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.97666834E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.10651237E-03    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     2.83548977E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.68838109E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.30524800E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.54716362E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     3.18175011E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     5.06151218E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     5.42884125E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.01041387E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.45926937E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.20101813E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.01078044E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.26439013E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.31947977E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     5.69216349E-04    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.60277042E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.15624804E-03    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     2.95026637E-02    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     1.26746421E+02   # ~t_2
#    BR                NDA      ID1      ID2
     5.59974009E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.49737493E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.58560945E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     6.08927848E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.88213110E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.19502555E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.85462142E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     1.63274631E-04    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.25875724E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.42831397E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99999684E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     1.54997460E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     8.07838559E-04    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.42795395E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.45600550E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.43573916E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.41191308E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.05816188E-02    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     8.12711378E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     7.27968494E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.08496301E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     3.54657241E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     6.45338522E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     2.11890153E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     7.40657549E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     2.59306572E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.63436074E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.32207487E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.32207487E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     6.46772140E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.67015810E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     7.29769629E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.62445528E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.17334416E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     4.41888448E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     4.06858317E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     7.38648372E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     7.38648372E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     2.91594879E-01   # ~g
#    BR                NDA      ID1      ID2
     3.56216476E-03    2     1000023        21   # BR(~g -> chi^0_2 g)
     3.69137484E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     2.65057113E-02    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     4.78938101E-03    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     2.30468590E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     5.37846910E-03    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     2.29198213E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     5.43429130E-03    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     1.57971378E-03    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     1.80803220E-03    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     2.40239452E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.40239452E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     3.42244251E-03    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     3.42244251E-03    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     2.91503944E-03   # Gamma(h0)
     2.58125467E-03   2        22        22   # BR(h0 -> photon photon)
     1.14611698E-03   2        22        23   # BR(h0 -> photon Z)
     1.73040796E-02   2        23        23   # BR(h0 -> Z Z)
     1.60294120E-01   2       -24        24   # BR(h0 -> W W)
     8.44029457E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.55070208E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.46901656E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.11820163E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.58859965E-07   2        -2         2   # BR(h0 -> Up up)
     3.08282211E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.47887181E-07   2        -1         1   # BR(h0 -> Down down)
     2.34324740E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.22423565E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     9.35564177E-03   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     2.75511002E+01   # Gamma(HH)
     1.08205199E-07   2        22        22   # BR(HH -> photon photon)
     1.00757712E-07   2        22        23   # BR(HH -> photon Z)
     9.17505568E-06   2        23        23   # BR(HH -> Z Z)
     2.82754814E-06   2       -24        24   # BR(HH -> W W)
     3.53524664E-06   2        21        21   # BR(HH -> gluon gluon)
     1.93991033E-09   2       -11        11   # BR(HH -> Electron electron)
     8.63733922E-05   2       -13        13   # BR(HH -> Muon muon)
     2.49456220E-02   2       -15        15   # BR(HH -> Tau tau)
     1.77203611E-12   2        -2         2   # BR(HH -> Up up)
     3.43758660E-07   2        -4         4   # BR(HH -> Charm charm)
     2.53131404E-02   2        -6         6   # BR(HH -> Top top)
     1.42693026E-07   2        -1         1   # BR(HH -> Down down)
     5.16130474E-05   2        -3         3   # BR(HH -> Strange strange)
     1.33834242E-01   2        -5         5   # BR(HH -> Bottom bottom)
     4.61818737E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     2.28450505E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     2.28450505E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     9.41942802E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     4.40174616E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     6.35099158E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     8.27633039E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.06171454E-03   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     3.75043455E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     7.81905349E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     3.10940348E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     1.42851530E-01   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     4.68223158E-05   2        25        25   # BR(HH -> h0 h0)
DECAY        36     2.73799858E+01   # Gamma(A0)
     2.18689871E-07   2        22        22   # BR(A0 -> photon photon)
     2.56866245E-07   2        22        23   # BR(A0 -> photon Z)
     2.18269964E-05   2        21        21   # BR(A0 -> gluon gluon)
     1.89448143E-09   2       -11        11   # BR(A0 -> Electron electron)
     8.43507385E-05   2       -13        13   # BR(A0 -> Muon muon)
     2.43614956E-02   2       -15        15   # BR(A0 -> Tau tau)
     1.66835379E-12   2        -2         2   # BR(A0 -> Up up)
     3.23631837E-07   2        -4         4   # BR(A0 -> Charm charm)
     2.41625603E-02   2        -6         6   # BR(A0 -> Top top)
     1.39351363E-07   2        -1         1   # BR(A0 -> Down down)
     5.04039930E-05   2        -3         3   # BR(A0 -> Strange strange)
     1.30699674E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     5.15012018E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     2.29566997E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     2.29566997E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.06506895E-02   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     6.37550842E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     4.36503555E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     9.44855456E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     1.55459004E-03   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     3.90018358E-03   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     1.40645504E-01   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     2.25338897E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     8.04614282E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     1.48540871E-05   2        23        25   # BR(A0 -> Z h0)
     2.55287683E-40   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     2.84310934E+01   # Gamma(Hp)
     2.32029468E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     9.91998445E-05   2       -13        14   # BR(Hp -> Muon nu_mu)
     2.80593564E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     1.48408802E-07   2        -1         2   # BR(Hp -> Down up)
     2.50466913E-06   2        -3         2   # BR(Hp -> Strange up)
     1.59534126E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.50777033E-08   2        -1         4   # BR(Hp -> Down charm)
     5.38722862E-05   2        -3         4   # BR(Hp -> Strange charm)
     2.23405454E-04   2        -5         4   # BR(Hp -> Bottom charm)
     1.98507997E-06   2        -1         6   # BR(Hp -> Down top)
     4.33604713E-05   2        -3         6   # BR(Hp -> Strange top)
     1.81101716E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.15212806E-01   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     1.71929654E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     7.19953060E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     2.12610814E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     3.73909038E-05   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     2.16093989E-01   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     2.22050986E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     1.43467234E-05   2        24        25   # BR(Hp -> W h0)
     3.90892763E-12   2        24        35   # BR(Hp -> W HH)
     2.65260266E-12   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.14687629E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.22791451E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.22706139E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00069526E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    7.45429396E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    8.14955152E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.14687629E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.22791451E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.22706139E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99984509E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.54914196E-05        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99984509E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.54914196E-05        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26960180E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    4.15672087E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.14923921E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.54914196E-05        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99984509E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.25605974E-04   # BR(b -> s gamma)
    2    1.58885995E-06   # BR(b -> s mu+ mu-)
    3    3.52328921E-05   # BR(b -> s nu nu)
    4    2.55010506E-15   # BR(Bd -> e+ e-)
    5    1.08937593E-10   # BR(Bd -> mu+ mu-)
    6    2.28050051E-08   # BR(Bd -> tau+ tau-)
    7    8.58665458E-14   # BR(Bs -> e+ e-)
    8    3.66821611E-09   # BR(Bs -> mu+ mu-)
    9    7.78061097E-07   # BR(Bs -> tau+ tau-)
   10    9.67152330E-05   # BR(B_u -> tau nu)
   11    9.99031330E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42662095E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93893063E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16007134E-03   # epsilon_K
   17    2.28168621E-15   # Delta(M_K)
   18    2.47842622E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28678226E-11   # BR(K^+ -> pi^+ nu nu)
   20    1.15603228E-16   # Delta(g-2)_electron/2
   21    4.94240260E-12   # Delta(g-2)_muon/2
   22    1.39829563E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    3.35806448E-07   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.93988315E-01   # C7
     0305 4322   00   2    -1.26365915E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.03605298E-01   # C8
     0305 6321   00   2    -1.75626494E-04   # C8'
 03051111 4133   00   0     1.61578032E+00   # C9 e+e-
 03051111 4133   00   2     1.61667618E+00   # C9 e+e-
 03051111 4233   00   2     3.03052878E-05   # C9' e+e-
 03051111 4137   00   0    -4.43847242E+00   # C10 e+e-
 03051111 4137   00   2    -4.43486724E+00   # C10 e+e-
 03051111 4237   00   2    -2.26593656E-04   # C10' e+e-
 03051313 4133   00   0     1.61578032E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61667617E+00   # C9 mu+mu-
 03051313 4233   00   2     3.03052845E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.43847242E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43486726E+00   # C10 mu+mu-
 03051313 4237   00   2    -2.26593656E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50456973E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     4.91174723E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50456973E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     4.91174728E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50456973E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     4.91176111E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85822099E-07   # C7
     0305 4422   00   2     4.63434829E-06   # C7
     0305 4322   00   2     1.48345690E-07   # C7'
     0305 6421   00   0     3.30482144E-07   # C8
     0305 6421   00   2    -2.34218745E-06   # C8
     0305 6321   00   2    -3.51403796E-08   # C8'
 03051111 4133   00   2     3.59174202E-07   # C9 e+e-
 03051111 4233   00   2     6.03605420E-07   # C9' e+e-
 03051111 4137   00   2    -2.10730009E-07   # C10 e+e-
 03051111 4237   00   2    -4.51603742E-06   # C10' e+e-
 03051313 4133   00   2     3.59174012E-07   # C9 mu+mu-
 03051313 4233   00   2     6.03605403E-07   # C9' mu+mu-
 03051313 4137   00   2    -2.10729816E-07   # C10 mu+mu-
 03051313 4237   00   2    -4.51603747E-06   # C10' mu+mu-
 03051212 4137   00   2     6.50347966E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     9.78918979E-07   # C11' nu_1 nu_1
 03051414 4137   00   2     6.50348043E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     9.78918979E-07   # C11' nu_2 nu_2
 03051616 4137   00   2     6.50369868E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     9.78918976E-07   # C11' nu_3 nu_3
