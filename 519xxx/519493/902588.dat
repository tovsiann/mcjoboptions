# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  16:31
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    8.56251326E+00  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.13534948E+03  # scale for input parameters
    1   -1.93980861E+02  # M_1
    2    1.77995276E+03  # M_2
    3    3.10222981E+03  # M_3
   11    7.35325156E+03  # A_t
   12   -1.64976874E+03  # A_b
   13    1.67292428E+03  # A_tau
   23    1.90315114E+02  # mu
   25    8.13522816E+00  # tan(beta)
   26    2.10455893E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.23840702E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.10369313E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.26910453E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.13534948E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.13534948E+03  # (SUSY scale)
  1  1     8.44747800E-06   # Y_u(Q)^DRbar
  2  2     4.29131883E-03   # Y_c(Q)^DRbar
  3  3     1.02052319E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.13534948E+03  # (SUSY scale)
  1  1     1.38094714E-04   # Y_d(Q)^DRbar
  2  2     2.62379956E-03   # Y_s(Q)^DRbar
  3  3     1.36946684E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.13534948E+03  # (SUSY scale)
  1  1     2.40985374E-05   # Y_e(Q)^DRbar
  2  2     4.98281323E-03   # Y_mu(Q)^DRbar
  3  3     8.38027214E-02   # Y_tau(Q)^DRbar
Block Au Q=  3.13534948E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     7.35325185E+03   # A_t(Q)^DRbar
Block Ad Q=  3.13534948E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.64976873E+03   # A_b(Q)^DRbar
Block Ae Q=  3.13534948E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.67292429E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.13534948E+03  # soft SUSY breaking masses at Q
   1   -1.93980861E+02  # M_1
   2    1.77995276E+03  # M_2
   3    3.10222981E+03  # M_3
  21    4.26371286E+06  # M^2_(H,d)
  22    1.96338787E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.23840702E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.10369313E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.26910453E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.27810893E+02  # h0
        35     2.10460228E+03  # H0
        36     2.10455893E+03  # A0
        37     2.10604241E+03  # H+
   1000001     1.01012569E+04  # ~d_L
   2000001     1.00781874E+04  # ~d_R
   1000002     1.01009088E+04  # ~u_L
   2000002     1.00811939E+04  # ~u_R
   1000003     1.01012578E+04  # ~s_L
   2000003     1.00781879E+04  # ~s_R
   1000004     1.01009097E+04  # ~c_L
   2000004     1.00811952E+04  # ~c_R
   1000005     3.25268130E+03  # ~b_1
   2000005     4.34675132E+03  # ~b_2
   1000006     2.95761476E+03  # ~t_1
   2000006     3.32376498E+03  # ~t_2
   1000011     1.00197181E+04  # ~e_L-
   2000011     1.00090039E+04  # ~e_R-
   1000012     1.00189722E+04  # ~nu_eL
   1000013     1.00197193E+04  # ~mu_L-
   2000013     1.00090063E+04  # ~mu_R-
   1000014     1.00189734E+04  # ~nu_muL
   1000015     1.00096948E+04  # ~tau_1-
   2000015     1.00200721E+04  # ~tau_2-
   1000016     1.00193254E+04  # ~nu_tauL
   1000021     3.54889981E+03  # ~g
   1000022     1.73534822E+02  # ~chi_10
   1000023     2.03289637E+02  # ~chi_20
   1000025     2.28770763E+02  # ~chi_30
   1000035     1.88813863E+03  # ~chi_40
   1000024     2.01309445E+02  # ~chi_1+
   1000037     1.88810401E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.16873160E-01   # alpha
Block Hmix Q=  3.13534948E+03  # Higgs mixing parameters
   1    1.90315114E+02  # mu
   2    8.13522816E+00  # tan[beta](Q)
   3    2.43155663E+02  # v(Q)
   4    4.42916829E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -4.63810192E-01   # Re[R_st(1,1)]
   1  2     8.85934595E-01   # Re[R_st(1,2)]
   2  1    -8.85934595E-01   # Re[R_st(2,1)]
   2  2    -4.63810192E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99999669E-01   # Re[R_sb(1,1)]
   1  2     8.13889518E-04   # Re[R_sb(1,2)]
   2  1    -8.13889518E-04   # Re[R_sb(2,1)]
   2  2     9.99999669E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -1.59834457E-03   # Re[R_sta(1,1)]
   1  2     9.99998723E-01   # Re[R_sta(1,2)]
   2  1    -9.99998723E-01   # Re[R_sta(2,1)]
   2  2    -1.59834457E-03   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     7.29974511E-01   # Re[N(1,1)]
   1  2     1.43523822E-02   # Re[N(1,2)]
   1  3     5.27657488E-01   # Re[N(1,3)]
   1  4     4.34175999E-01   # Re[N(1,4)]
   2  1    -8.75847762E-02   # Re[N(2,1)]
   2  2    -3.72103521E-02   # Re[N(2,2)]
   2  3     7.02511005E-01   # Re[N(2,3)]
   2  4    -7.05281918E-01   # Re[N(2,4)]
   3  1    -6.77838594E-01   # Re[N(3,1)]
   3  2     1.88192431E-02   # Re[N(3,2)]
   3  3     4.77455616E-01   # Re[N(3,3)]
   3  4     5.58763645E-01   # Re[N(3,4)]
   4  1     9.80478127E-04   # Re[N(4,1)]
   4  2    -9.99027144E-01   # Re[N(4,2)]
   4  3    -9.59151779E-03   # Re[N(4,3)]
   4  4     4.30326218E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.35561034E-02   # Re[U(1,1)]
   1  2     9.99908112E-01   # Re[U(1,2)]
   2  1     9.99908112E-01   # Re[U(2,1)]
   2  2     1.35561034E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -6.08567556E-02   # Re[V(1,1)]
   1  2     9.98146510E-01   # Re[V(1,2)]
   2  1     9.98146510E-01   # Re[V(2,1)]
   2  2     6.08567556E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02510212E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     5.32972395E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     7.67095416E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     4.59355754E-01    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.35663340E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     5.31111476E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.29190522E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.82719917E-02    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.01502018E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     1.19383749E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.04703554E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02609128E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     5.32894998E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     7.71795086E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     4.59287864E-01    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
DECAY   1000013     1.35668303E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     5.31193620E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.30980997E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     3.82789028E-02    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.01491032E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     1.19379396E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.04681513E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.30494021E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     5.12132822E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     2.02698648E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     4.41243918E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     2.63272200E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
DECAY   2000015     1.37067432E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     5.54477766E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     7.30375962E-03    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     4.01740943E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.98424115E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.21700860E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.98528553E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.35666451E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     4.58331048E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     4.71196186E-02    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02102262E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     2.40575207E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.02509460E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.35671414E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     4.58314337E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     4.71179006E-02    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02091252E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     2.44210370E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.02487509E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.37070589E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     4.53651961E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     4.66385774E-02    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.99019649E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.25837012E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.96363378E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     6.16073126E+02   # ~d_R
#    BR                NDA      ID1      ID2
     4.86373754E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     4.19196075E-03    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.90874281E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     7.38278702E+02   # ~d_L
#    BR                NDA      ID1      ID2
     8.04812168E-04    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.17115175E-03    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     5.59981480E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.12154998E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.29820661E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.16075924E+02   # ~s_R
#    BR                NDA      ID1      ID2
     4.86433730E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     4.19245082E-03    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.90869857E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     7.38283875E+02   # ~s_L
#    BR                NDA      ID1      ID2
     8.05326548E-04    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.17156925E-03    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.59977615E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.12154241E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.29814961E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.10260253E+02   # ~b_1
#    BR                NDA      ID1      ID2
     4.78494401E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     5.44923771E-03    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     4.97695299E-03    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     5.69912671E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     6.00823771E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.15626925E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     2.11346902E-01    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
DECAY   2000005     4.37879247E+01   # ~b_2
#    BR                NDA      ID1      ID2
     3.96730990E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.86241687E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     3.37600116E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     3.67535280E-02    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     8.71004868E-01    2     1000021         5   # BR(~b_2 -> ~g b)
DECAY   2000002     6.33231354E+02   # ~u_R
#    BR                NDA      ID1      ID2
     1.89334394E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     2.72505662E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     1.63183676E-02    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.64475655E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     7.38264254E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.25394359E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     1.67795761E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     6.24502423E-04    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     5.59579025E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     4.45717141E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.11757943E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.29792196E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     6.33238827E+02   # ~c_R
#    BR                NDA      ID1      ID2
     1.89343179E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     2.75401644E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     1.63199934E-02    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.64464419E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     7.38269396E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.25487894E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     1.70285689E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     6.26061886E-04    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     5.59575272E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     4.47580866E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.11757176E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.29786489E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.14620292E+02   # ~t_1
#    BR                NDA      ID1      ID2
     1.57311499E-01    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.65210574E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.42437710E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.19647957E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     3.99209172E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.37404797E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     1.25768821E-04    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.30544447E+02   # ~t_2
#    BR                NDA      ID1      ID2
     7.39827325E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.56597085E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.09608265E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     3.69451077E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.37310262E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     7.27213005E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.32393195E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     8.04420519E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     2.93474362E-05   # chi^+_1
#    BR                NDA      ID1      ID2       ID3
     3.35130964E-01    3     1000022        -1         2   # BR(chi^+_1 -> chi^0_1 d_bar u)
     3.31856623E-01    3     1000022        -3         4   # BR(chi^+_1 -> chi^0_1 s_bar c)
     1.11709946E-01    3     1000022       -11        12   # BR(chi^+_1 -> chi^0_1 e^+ nu_e)
     1.11704706E-01    3     1000022       -13        14   # BR(chi^+_1 -> chi^0_1 mu^+ nu_mu)
     1.09597760E-01    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     1.77359712E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     9.96520259E-02    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.42915050E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.42104379E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.41466009E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.42357590E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.31738964E-02    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     3.53996215E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     9.72495862E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     5.03187065E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     3.25832283E-05   # chi^0_2
#    BR                NDA      ID1      ID2
     2.90595937E-02    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.19291345E-01    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     1.16522826E-01    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.52954750E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.52937454E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.22840568E-01    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     3.45169030E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.45107056E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     3.28198339E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     2.04544490E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     8.49096250E-05   # chi^0_3
#    BR                NDA      ID1      ID2
     1.50572778E-03    2     1000022        22   # BR(chi^0_3 -> chi^0_1 photon)
     1.59216473E-02    2     1000023        22   # BR(chi^0_3 -> chi^0_2 photon)
#    BR                NDA      ID1      ID2       ID3
     2.23421698E-04    3     1000022         2        -2   # BR(chi^0_3 -> chi^0_1 u u_bar)
     2.22242187E-04    3     1000022         4        -4   # BR(chi^0_3 -> chi^0_1 c c_bar)
     2.85919335E-04    3     1000022         1        -1   # BR(chi^0_3 -> chi^0_1 d d_bar)
     2.85912184E-04    3     1000022         3        -3   # BR(chi^0_3 -> chi^0_1 s s_bar)
     2.72599321E-04    3     1000022         5        -5   # BR(chi^0_3 -> chi^0_1 b b_bar)
     3.83092426E-04    3     1000022        12       -12   # BR(chi^0_3 -> chi^0_1 nu_e nu_bar_e)
     2.51988385E-02    3     1000023         2        -2   # BR(chi^0_3 -> chi^0_2 u u_bar)
     2.43969779E-02    3     1000023         4        -4   # BR(chi^0_3 -> chi^0_2 c c_bar)
     3.23100696E-02    3     1000023         1        -1   # BR(chi^0_3 -> chi^0_2 d d_bar)
     3.23050247E-02    3     1000023         3        -3   # BR(chi^0_3 -> chi^0_2 s s_bar)
     2.38319882E-02    3     1000023         5        -5   # BR(chi^0_3 -> chi^0_2 b b_bar)
     7.29158044E-03    3     1000023        11       -11   # BR(chi^0_3 -> chi^0_2 e^- e^+)
     7.28977910E-03    3     1000023        13       -13   # BR(chi^0_3 -> chi^0_2 mu^- mu^+)
     6.80350089E-03    3     1000023        15       -15   # BR(chi^0_3 -> chi^0_2 tau^- tau^+)
     4.32078239E-02    3     1000023        12       -12   # BR(chi^0_3 -> chi^0_2 nu_e nu_bar_e)
     1.30388494E-01    3     1000024         1        -2   # BR(chi^0_3 -> chi^+_1 d u_bar)
     1.30388494E-01    3    -1000024        -1         2   # BR(chi^0_3 -> chi^-_1 d_bar u)
     1.29092742E-01    3     1000024         3        -4   # BR(chi^0_3 -> chi^+_1 s c_bar)
     1.29092742E-01    3    -1000024        -3         4   # BR(chi^0_3 -> chi^-_1 s_bar c)
     4.34629622E-02    3     1000024        11       -12   # BR(chi^0_3 -> chi^+_1 e^- nu_bar_e)
     4.34629622E-02    3    -1000024       -11        12   # BR(chi^0_3 -> chi^-_1 e^+ nu_e)
     4.34609078E-02    3     1000024        13       -14   # BR(chi^0_3 -> chi^+_1 mu^- nu_bar_mu)
     4.34609078E-02    3    -1000024       -13        14   # BR(chi^0_3 -> chi^-_1 mu^+ nu_mu)
     4.26310065E-02    3     1000024        15       -16   # BR(chi^0_3 -> chi^+_1 tau^- nu_bar_tau)
     4.26310065E-02    3    -1000024       -15        16   # BR(chi^0_3 -> chi^-_1 tau^+ nu_tau)
DECAY   1000035     1.83745563E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.33850490E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.33850490E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     6.85905897E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     6.71978939E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     9.78544153E-02    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     3.94928378E-02    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.66485006E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     6.46175282E-02    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.10161752E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     5.66609891E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     2.99480600E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     8.63150472E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     8.63150472E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.49835588E+01   # ~g
#    BR                NDA      ID1      ID2
     3.97157219E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     3.97157219E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.23817439E-02    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     1.23817439E-02    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     8.96932380E-02    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     8.96932380E-02    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     3.22059587E-04    2     1000022        21   # BR(~g -> chi^0_1 g)
     6.25571928E-04    2     1000023        21   # BR(~g -> chi^0_2 g)
     3.05500357E-04    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.89258292E-03   # Gamma(h0)
     2.57895933E-03   2        22        22   # BR(h0 -> photon photon)
     1.90632050E-03   2        22        23   # BR(h0 -> photon Z)
     3.88300341E-02   2        23        23   # BR(h0 -> Z Z)
     3.04062248E-01   2       -24        24   # BR(h0 -> W W)
     7.69908015E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.45460642E-09   2       -11        11   # BR(h0 -> Electron electron)
     1.98150200E-04   2       -13        13   # BR(h0 -> Muon muon)
     5.71369972E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.25262431E-07   2        -2         2   # BR(h0 -> Up up)
     2.43129323E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.13554674E-07   2        -1         1   # BR(h0 -> Down down)
     1.85740799E-04   2        -3         3   # BR(h0 -> Strange strange)
     4.93797173E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     6.93351119E+00   # Gamma(HH)
     5.68364415E-07   2        22        22   # BR(HH -> photon photon)
     2.69030270E-07   2        22        23   # BR(HH -> photon Z)
     1.18933328E-04   2        23        23   # BR(HH -> Z Z)
     5.86131116E-05   2       -24        24   # BR(HH -> W W)
     4.53968980E-05   2        21        21   # BR(HH -> gluon gluon)
     3.04831199E-09   2       -11        11   # BR(HH -> Electron electron)
     1.35710759E-04   2       -13        13   # BR(HH -> Muon muon)
     3.91927060E-02   2       -15        15   # BR(HH -> Tau tau)
     8.99172710E-12   2        -2         2   # BR(HH -> Up up)
     1.74415904E-06   2        -4         4   # BR(HH -> Charm charm)
     1.26949461E-01   2        -6         6   # BR(HH -> Top top)
     2.33805983E-07   2        -1         1   # BR(HH -> Down down)
     8.45675356E-05   2        -3         3   # BR(HH -> Strange strange)
     2.19075801E-01   2        -5         5   # BR(HH -> Bottom bottom)
     9.30047915E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     8.69568722E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     8.69568722E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     1.06412390E-01   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     5.16900547E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     9.66371060E-03   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     4.65816062E-02   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     4.78131970E-05   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     8.16343028E-02   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.27741663E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     9.45262400E-02   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     2.71226293E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     6.68855085E-04   2        25        25   # BR(HH -> h0 h0)
DECAY        36     6.91086261E+00   # Gamma(A0)
     9.07177590E-07   2        22        22   # BR(A0 -> photon photon)
     4.45889459E-07   2        22        23   # BR(A0 -> photon Z)
     1.29970823E-04   2        21        21   # BR(A0 -> gluon gluon)
     3.00287698E-09   2       -11        11   # BR(A0 -> Electron electron)
     1.33687485E-04   2       -13        13   # BR(A0 -> Muon muon)
     3.86085118E-02   2       -15        15   # BR(A0 -> Tau tau)
     8.80741877E-12   2        -2         2   # BR(A0 -> Up up)
     1.70829222E-06   2        -4         4   # BR(A0 -> Charm charm)
     1.28034225E-01   2        -6         6   # BR(A0 -> Top top)
     2.30320831E-07   2        -1         1   # BR(A0 -> Down down)
     8.33069693E-05   2        -3         3   # BR(A0 -> Strange strange)
     2.15813741E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.06908228E-02   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     8.69397346E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     8.69397346E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     7.34761773E-02   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     8.19794337E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     8.89705281E-03   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.05719629E-02   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     7.94976480E-05   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.24864121E-01   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     7.32167022E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     5.61173936E-02   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     3.25776900E-03   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     1.62859145E-04   2        23        25   # BR(A0 -> Z h0)
     2.85016425E-36   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     7.42712652E+00   # Gamma(Hp)
     3.43802848E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     1.46986455E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     4.15760978E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     2.29208476E-07   2        -1         2   # BR(Hp -> Down up)
     3.85270983E-06   2        -3         2   # BR(Hp -> Strange up)
     2.43594946E-06   2        -5         2   # BR(Hp -> Bottom up)
     1.05232489E-07   2        -1         4   # BR(Hp -> Down charm)
     8.46292229E-05   2        -3         4   # BR(Hp -> Strange charm)
     3.41123086E-04   2        -5         4   # BR(Hp -> Bottom charm)
     1.02755507E-05   2        -1         6   # BR(Hp -> Down top)
     2.24161940E-04   2        -3         6   # BR(Hp -> Strange top)
     3.87791937E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.67141928E-01   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     5.37643212E-02   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     5.77539867E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     8.11442692E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     1.51274886E-01   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     2.86280933E-02   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     8.19372108E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     1.52055945E-04   2        24        25   # BR(Hp -> W h0)
     1.13500374E-12   2        24        35   # BR(Hp -> W HH)
     1.31642547E-12   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.13490013E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    6.62684472E+01    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    6.61819372E+01        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00130715E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.38027089E-02    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.51098629E-02        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.13490013E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    6.62684472E+01    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    6.61819372E+01        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99970456E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.95439664E-05        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99970456E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.95439664E-05        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26393457E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.02047673E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.41420139E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.95439664E-05        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99970456E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.35770334E-04   # BR(b -> s gamma)
    2    1.58899919E-06   # BR(b -> s mu+ mu-)
    3    3.52608924E-05   # BR(b -> s nu nu)
    4    2.42681531E-15   # BR(Bd -> e+ e-)
    5    1.03670805E-10   # BR(Bd -> mu+ mu-)
    6    2.17030316E-08   # BR(Bd -> tau+ tau-)
    7    8.16387719E-14   # BR(Bs -> e+ e-)
    8    3.48760606E-09   # BR(Bs -> mu+ mu-)
    9    7.39771258E-07   # BR(Bs -> tau+ tau-)
   10    9.67069355E-05   # BR(B_u -> tau nu)
   11    9.98945620E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42442274E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93814237E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15921876E-03   # epsilon_K
   17    2.28167773E-15   # Delta(M_K)
   18    2.48093782E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29257096E-11   # BR(K^+ -> pi^+ nu nu)
   20    7.38003929E-17   # Delta(g-2)_electron/2
   21    3.15519733E-12   # Delta(g-2)_muon/2
   22    8.92548823E-10   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -7.57844935E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.05046591E-01   # C7
     0305 4322   00   2    -2.53472117E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.12729058E-01   # C8
     0305 6321   00   2    -3.05712796E-04   # C8'
 03051111 4133   00   0     1.61770881E+00   # C9 e+e-
 03051111 4133   00   2     1.61808847E+00   # C9 e+e-
 03051111 4233   00   2     1.85958591E-05   # C9' e+e-
 03051111 4137   00   0    -4.44040091E+00   # C10 e+e-
 03051111 4137   00   2    -4.43954860E+00   # C10 e+e-
 03051111 4237   00   2    -1.38983901E-04   # C10' e+e-
 03051313 4133   00   0     1.61770881E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61808846E+00   # C9 mu+mu-
 03051313 4233   00   2     1.85958566E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.44040091E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43954860E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.38983900E-04   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50516671E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     3.01136939E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50516671E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     3.01136943E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50516671E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     3.01138310E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85818986E-07   # C7
     0305 4422   00   2     3.48370776E-06   # C7
     0305 4322   00   2    -3.83189885E-07   # C7'
     0305 6421   00   0     3.30479477E-07   # C8
     0305 6421   00   2    -1.81501155E-06   # C8
     0305 6321   00   2    -2.40755480E-07   # C8'
 03051111 4133   00   2    -3.05380054E-07   # C9 e+e-
 03051111 4233   00   2     3.90034002E-07   # C9' e+e-
 03051111 4137   00   2     4.73890237E-06   # C10 e+e-
 03051111 4237   00   2    -2.91670124E-06   # C10' e+e-
 03051313 4133   00   2    -3.05380157E-07   # C9 mu+mu-
 03051313 4233   00   2     3.90033997E-07   # C9' mu+mu-
 03051313 4137   00   2     4.73890250E-06   # C10 mu+mu-
 03051313 4237   00   2    -2.91670126E-06   # C10' mu+mu-
 03051212 4137   00   2    -1.00415864E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     6.31964751E-07   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.00415863E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     6.31964751E-07   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.00415730E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     6.31964750E-07   # C11' nu_3 nu_3
