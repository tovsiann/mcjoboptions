# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  19:39
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    4.20295876E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.86457552E+03  # scale for input parameters
    1    4.23211704E+01  # M_1
    2    9.06234690E+02  # M_2
    3    3.74714802E+03  # M_3
   11   -6.97701331E+03  # A_t
   12   -1.98039231E+03  # A_b
   13   -8.64614389E+02  # A_tau
   23   -3.22410346E+02  # mu
   25    4.10536368E+01  # tan(beta)
   26    4.57548090E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.40928162E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.33414598E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.82130447E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.86457552E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.86457552E+03  # (SUSY scale)
  1  1     8.38685912E-06   # Y_u(Q)^DRbar
  2  2     4.26052443E-03   # Y_c(Q)^DRbar
  3  3     1.01319995E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.86457552E+03  # (SUSY scale)
  1  1     6.91880724E-04   # Y_d(Q)^DRbar
  2  2     1.31457338E-02   # Y_s(Q)^DRbar
  3  3     6.86128874E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.86457552E+03  # (SUSY scale)
  1  1     1.20738246E-04   # Y_e(Q)^DRbar
  2  2     2.49648399E-02   # Y_mu(Q)^DRbar
  3  3     4.19867538E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.86457552E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -6.97701319E+03   # A_t(Q)^DRbar
Block Ad Q=  3.86457552E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.98039231E+03   # A_b(Q)^DRbar
Block Ae Q=  3.86457552E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -8.64614393E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.86457552E+03  # soft SUSY breaking masses at Q
   1    4.23211704E+01  # M_1
   2    9.06234690E+02  # M_2
   3    3.74714802E+03  # M_3
  21    2.09312828E+07  # M^2_(H,d)
  22    1.93159859E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.40928162E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.33414598E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.82130447E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.28294631E+02  # h0
        35     4.57391686E+03  # H0
        36     4.57548090E+03  # A0
        37     4.57430214E+03  # H+
   1000001     1.01036546E+04  # ~d_L
   2000001     1.00792443E+04  # ~d_R
   1000002     1.01032936E+04  # ~u_L
   2000002     1.00820511E+04  # ~u_R
   1000003     1.01036577E+04  # ~s_L
   2000003     1.00792496E+04  # ~s_R
   1000004     1.01032967E+04  # ~c_L
   2000004     1.00820517E+04  # ~c_R
   1000005     2.95547351E+03  # ~b_1
   2000005     4.43951358E+03  # ~b_2
   1000006     3.35325376E+03  # ~t_1
   2000006     4.45386632E+03  # ~t_2
   1000011     1.00209335E+04  # ~e_L-
   2000011     1.00092225E+04  # ~e_R-
   1000012     1.00201708E+04  # ~nu_eL
   1000013     1.00209480E+04  # ~mu_L-
   2000013     1.00092494E+04  # ~mu_R-
   1000014     1.00201848E+04  # ~nu_muL
   1000015     1.00168931E+04  # ~tau_1-
   2000015     1.00250902E+04  # ~tau_2-
   1000016     1.00241755E+04  # ~nu_tauL
   1000021     4.22665803E+03  # ~g
   1000022     4.21996019E+01  # ~chi_10
   1000023     3.34555442E+02  # ~chi_20
   1000025     3.41440071E+02  # ~chi_30
   1000035     9.87292730E+02  # ~chi_40
   1000024     3.34131320E+02  # ~chi_1+
   1000037     9.86809294E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.38130163E-02   # alpha
Block Hmix Q=  3.86457552E+03  # Higgs mixing parameters
   1   -3.22410346E+02  # mu
   2    4.10536368E+01  # tan[beta](Q)
   3    2.42958023E+02  # v(Q)
   4    2.09350255E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     1.13066465E-01   # Re[R_st(1,1)]
   1  2     9.93587427E-01   # Re[R_st(1,2)]
   2  1    -9.93587427E-01   # Re[R_st(2,1)]
   2  2     1.13066465E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -2.07606573E-03   # Re[R_sb(1,1)]
   1  2     9.99997845E-01   # Re[R_sb(1,2)]
   2  1    -9.99997845E-01   # Re[R_sb(2,1)]
   2  2    -2.07606573E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -1.34839482E-01   # Re[R_sta(1,1)]
   1  2     9.90867455E-01   # Re[R_sta(1,2)]
   2  1    -9.90867455E-01   # Re[R_sta(2,1)]
   2  2    -1.34839482E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.90913670E-01   # Re[N(1,1)]
   1  2     8.28229743E-04   # Re[N(1,2)]
   1  3     1.33784200E-01   # Re[N(1,3)]
   1  4     1.38275013E-02   # Re[N(1,4)]
   2  1     1.04646671E-01   # Re[N(2,1)]
   2  2     8.49835775E-02   # Re[N(2,2)]
   2  3     7.02328014E-01   # Re[N(2,3)]
   2  4     6.98972264E-01   # Re[N(2,4)]
   3  1    -8.43815219E-02   # Re[N(3,1)]
   3  2     4.43417337E-02   # Re[N(3,2)]
   3  3    -6.98569089E-01   # Re[N(3,3)]
   3  4     7.09164859E-01   # Re[N(3,4)]
   4  1     4.35095816E-03   # Re[N(4,1)]
   4  2    -9.95394854E-01   # Re[N(4,2)]
   4  3     2.89547275E-02   # Re[N(4,3)]
   4  4     9.12785664E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -4.09957687E-02   # Re[U(1,1)]
   1  2    -9.99159320E-01   # Re[U(1,2)]
   2  1     9.99159320E-01   # Re[U(2,1)]
   2  2    -4.09957687E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -1.29216369E-01   # Re[V(1,1)]
   1  2     9.91616423E-01   # Re[V(1,2)]
   2  1     9.91616423E-01   # Re[V(2,1)]
   2  2     1.29216369E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02888558E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.81949796E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.09273312E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     7.10424899E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.42367244E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.65443683E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     6.13594204E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     2.99632565E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     1.03906594E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.06647881E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.05366350E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.77181911E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.20823769E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     8.26501616E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     2.44615470E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.42491426E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.64846759E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     6.55975560E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     4.24706544E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     2.99372601E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     1.03816191E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.06120099E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.21483112E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     4.12560035E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.48664596E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.38287462E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     4.17450476E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.87842696E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     8.47070646E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.76455565E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     7.30159317E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.01770316E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.00764401E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.39308731E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     9.84487473E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.84156132E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.42371808E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.70782648E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.61366141E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     2.47124224E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02410785E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     1.03217620E-02    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     5.97456580E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.42495972E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.70025107E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.61138766E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     2.46909239E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02147718E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     1.11812548E-02    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     5.96938285E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.77507954E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.98697995E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.09715156E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     1.98287731E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.42651956E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     2.05565828E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.79719824E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.46011193E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.01191716E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.12697763E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     9.89694466E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.75108059E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.07029186E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     2.89418693E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     2.31712026E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     6.41270704E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     2.20935759E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     1.29027557E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.04033015E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.46080680E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.01190219E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.43828351E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     1.04098479E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.89569493E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.75146611E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.07109257E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     3.14654917E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     2.56679176E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     6.41234707E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     2.26224965E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     1.29020319E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.03987558E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     5.53950369E+01   # ~b_1
#    BR                NDA      ID1      ID2
     3.82774433E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.40541150E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.37495283E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     2.99425854E-04    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     4.82792850E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     5.93846943E-04    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     2.46985322E+02   # ~b_2
#    BR                NDA      ID1      ID2
     5.46485385E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     8.24352485E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     8.14877073E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     7.10747226E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     3.55918780E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.48831417E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     1.24265178E-02    2     1000021         5   # BR(~b_2 -> ~g b)
     2.42092387E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.79214414E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     5.63176413E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.92539022E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     4.36837894E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     2.84003944E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.60024514E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.75092004E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.03175807E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     7.05706069E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     6.39268692E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     2.19505704E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.27085096E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.03999032E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.63183706E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.92533959E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     4.39984428E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     2.87243474E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.60012132E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.75130524E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.03164373E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     7.08299689E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     6.39232867E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     2.24604154E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.27077969E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.03953569E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.41116078E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.16489659E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.32404466E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.40449985E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     6.18341339E-04    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.73624718E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.21901494E-03    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     2.62543320E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.54140688E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.67000587E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.69671698E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     7.17625272E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.55093825E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.37304623E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     6.99744049E-03    2     1000021         6   # BR(~t_2 -> ~g t)
     1.70023156E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.16199511E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.73258358E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     2.14519033E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99411929E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     5.88066449E-04    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     8.19366072E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     7.53616611E-04    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.51026759E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.52968162E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.47261157E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.38024935E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.57248128E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     3.52831782E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     3.85167067E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.79338847E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     4.98700880E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     5.01294047E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     2.47784661E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     5.75942089E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     4.24023743E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     9.54485145E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     2.16411421E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.16411421E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     2.33111181E-04    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     4.59114040E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.72697944E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     3.34482062E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.60439458E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.79765259E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     8.52393427E-04    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     8.75885901E-04    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     3.02542270E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     3.02542270E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     4.96658032E+01   # ~g
#    BR                NDA      ID1      ID2
     1.14323422E-03    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     1.14323422E-03    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     1.61837107E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.61837107E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     3.28959330E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     3.28959330E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
#    BR                NDA      ID1      ID2       ID3
     1.82412774E-03    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     8.83441458E-04    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.87379006E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     8.72369579E-04    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     8.25919159E-04    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     7.89421744E-04    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     2.74581969E-03    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.74581969E-03    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.61812835E-03    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     1.61812835E-03    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     4.09961208E-03   # Gamma(h0)
     2.47691415E-03   2        22        22   # BR(h0 -> photon photon)
     1.88045500E-03   2        22        23   # BR(h0 -> photon Z)
     3.90418571E-02   2        23        23   # BR(h0 -> Z Z)
     3.03100349E-01   2       -24        24   # BR(h0 -> W W)
     7.40571995E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.45582417E-09   2       -11        11   # BR(h0 -> Electron electron)
     1.98204593E-04   2       -13        13   # BR(h0 -> Muon muon)
     5.71531991E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.26263788E-07   2        -2         2   # BR(h0 -> Up up)
     2.45063366E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.13378745E-07   2        -1         1   # BR(h0 -> Down down)
     1.85677103E-04   2        -3         3   # BR(h0 -> Strange strange)
     4.93526533E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     3.87263122E-03   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     1.44919407E+02   # Gamma(HH)
     1.91351545E-08   2        22        22   # BR(HH -> photon photon)
     4.27547699E-08   2        22        23   # BR(HH -> photon Z)
     5.35895576E-08   2        23        23   # BR(HH -> Z Z)
     9.73751193E-09   2       -24        24   # BR(HH -> W W)
     3.09200598E-06   2        21        21   # BR(HH -> gluon gluon)
     7.70313530E-09   2       -11        11   # BR(HH -> Electron electron)
     3.43031315E-04   2       -13        13   # BR(HH -> Muon muon)
     9.90781583E-02   2       -15        15   # BR(HH -> Tau tau)
     3.88248211E-14   2        -2         2   # BR(HH -> Up up)
     7.53217739E-09   2        -4         4   # BR(HH -> Charm charm)
     5.91209271E-04   2        -6         6   # BR(HH -> Top top)
     5.47218776E-07   2        -1         1   # BR(HH -> Down down)
     1.97914415E-04   2        -3         3   # BR(HH -> Strange strange)
     5.09907595E-01   2        -5         5   # BR(HH -> Bottom bottom)
     5.02432580E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.15022024E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     1.15022024E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     4.88220684E-04   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     1.32391840E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.91436768E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.56489524E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.58276951E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.76851843E-04   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     2.18270847E-04   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     5.85227151E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     9.93560804E-04   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     5.64642861E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     2.46134017E-04   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     5.80854402E-07   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.38563932E+02   # Gamma(A0)
     3.76833443E-10   2        22        22   # BR(A0 -> photon photon)
     1.10020977E-08   2        22        23   # BR(A0 -> photon Z)
     6.36064432E-06   2        21        21   # BR(A0 -> gluon gluon)
     7.47573123E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.32899864E-04   2       -13        13   # BR(A0 -> Muon muon)
     9.61519498E-02   2       -15        15   # BR(A0 -> Tau tau)
     3.63042051E-14   2        -2         2   # BR(A0 -> Up up)
     7.04304936E-09   2        -4         4   # BR(A0 -> Charm charm)
     5.56397570E-04   2        -6         6   # BR(A0 -> Top top)
     5.31035093E-07   2        -1         1   # BR(A0 -> Down down)
     1.92061277E-04   2        -3         3   # BR(A0 -> Strange strange)
     4.94834171E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     5.19622745E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.20405557E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     1.20405557E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     4.54924199E-04   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     1.37162633E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.82881904E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.80551566E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.72208521E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     1.71696367E-04   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     1.79445606E-04   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     5.90879261E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     1.16912949E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     6.11880402E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     2.29901983E-04   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     1.38377390E-07   2        23        25   # BR(A0 -> Z h0)
     7.42155525E-14   2        23        35   # BR(A0 -> Z HH)
     2.69575823E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.54917675E+02   # Gamma(Hp)
     8.62570894E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.68775998E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.04310864E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     5.28933676E-07   2        -1         2   # BR(Hp -> Down up)
     8.96893829E-06   2        -3         2   # BR(Hp -> Strange up)
     5.60941893E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.51924245E-08   2        -1         4   # BR(Hp -> Down charm)
     1.90610667E-04   2        -3         4   # BR(Hp -> Strange charm)
     7.85519517E-04   2        -5         4   # BR(Hp -> Bottom charm)
     4.13010719E-08   2        -1         6   # BR(Hp -> Down top)
     1.18510856E-06   2        -3         6   # BR(Hp -> Strange top)
     5.29783634E-01   2        -5         6   # BR(Hp -> Bottom top)
     3.46140421E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     3.03453183E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     1.26442293E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.09881312E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     2.75242271E-04   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.06968645E-01   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     1.08505903E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     4.77057261E-09   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     1.23706805E-07   2        24        25   # BR(Hp -> W h0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.56109431E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.68544499E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.68540109E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00002604E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    5.67288958E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    5.93330575E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.56109431E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.68544499E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.68540109E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999708E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.92189741E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999708E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.92189741E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26388613E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    5.67843610E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.15586886E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.92189741E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999708E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.38125585E-04   # BR(b -> s gamma)
    2    1.58893955E-06   # BR(b -> s mu+ mu-)
    3    3.52622431E-05   # BR(b -> s nu nu)
    4    1.42723916E-15   # BR(Bd -> e+ e-)
    5    6.09710215E-11   # BR(Bd -> mu+ mu-)
    6    1.28211917E-08   # BR(Bd -> tau+ tau-)
    7    4.86260894E-14   # BR(Bs -> e+ e-)
    8    2.07733878E-09   # BR(Bs -> mu+ mu-)
    9    4.42620224E-07   # BR(Bs -> tau+ tau-)
   10    9.62943236E-05   # BR(B_u -> tau nu)
   11    9.94683497E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42001461E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93596610E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15755822E-03   # epsilon_K
   17    2.28166107E-15   # Delta(M_K)
   18    2.48055962E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29185837E-11   # BR(K^+ -> pi^+ nu nu)
   20   -3.48710000E-16   # Delta(g-2)_electron/2
   21   -1.49086029E-11   # Delta(g-2)_muon/2
   22   -4.22950934E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.50900308E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.09641823E-01   # C7
     0305 4322   00   2    -2.06964045E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.08172294E-01   # C8
     0305 6321   00   2    -1.37983358E-04   # C8'
 03051111 4133   00   0     1.62591170E+00   # C9 e+e-
 03051111 4133   00   2     1.62620741E+00   # C9 e+e-
 03051111 4233   00   2     4.31827329E-04   # C9' e+e-
 03051111 4137   00   0    -4.44860380E+00   # C10 e+e-
 03051111 4137   00   2    -4.44800355E+00   # C10 e+e-
 03051111 4237   00   2    -3.19940960E-03   # C10' e+e-
 03051313 4133   00   0     1.62591170E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62620720E+00   # C9 mu+mu-
 03051313 4233   00   2     4.31827143E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44860380E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44800376E+00   # C10 mu+mu-
 03051313 4237   00   2    -3.19940985E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50522185E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     6.91952700E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50522185E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     6.91952717E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50522185E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     6.91957481E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85798970E-07   # C7
     0305 4422   00   2    -1.15115570E-05   # C7
     0305 4322   00   2    -2.51818235E-06   # C7'
     0305 6421   00   0     3.30462332E-07   # C8
     0305 6421   00   2     1.12680962E-05   # C8
     0305 6321   00   2    -9.00193467E-07   # C8'
 03051111 4133   00   2     1.22800275E-07   # C9 e+e-
 03051111 4233   00   2     8.11141241E-06   # C9' e+e-
 03051111 4137   00   2     2.24389785E-06   # C10 e+e-
 03051111 4237   00   2    -6.00990127E-05   # C10' e+e-
 03051313 4133   00   2     1.22796013E-07   # C9 mu+mu-
 03051313 4233   00   2     8.11141050E-06   # C9' mu+mu-
 03051313 4137   00   2     2.24390171E-06   # C10 mu+mu-
 03051313 4237   00   2    -6.00990184E-05   # C10' mu+mu-
 03051212 4137   00   2    -4.57771202E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     1.29979222E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -4.57771009E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     1.29979222E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -4.57717359E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     1.29979222E-05   # C11' nu_3 nu_3
