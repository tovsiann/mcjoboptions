# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  18:12
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    8.47782554E+00  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.95832879E+03  # scale for input parameters
    1    6.11716167E+01  # M_1
    2    1.31088388E+03  # M_2
    3    1.21351608E+03  # M_3
   11   -4.69538342E+03  # A_t
   12    8.71299053E+02  # A_b
   13   -1.98180313E+03  # A_tau
   23    4.58720941E+02  # mu
   25    8.05228416E+00  # tan(beta)
   26    7.85975753E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.45612461E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.57616802E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.81750812E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.95832879E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.95832879E+03  # (SUSY scale)
  1  1     8.44877979E-06   # Y_u(Q)^DRbar
  2  2     4.29198013E-03   # Y_c(Q)^DRbar
  3  3     1.02068046E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.95832879E+03  # (SUSY scale)
  1  1     1.36707811E-04   # Y_d(Q)^DRbar
  2  2     2.59744842E-03   # Y_s(Q)^DRbar
  3  3     1.35571311E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.95832879E+03  # (SUSY scale)
  1  1     2.38565128E-05   # Y_e(Q)^DRbar
  2  2     4.93277021E-03   # Y_mu(Q)^DRbar
  3  3     8.29610801E-02   # Y_tau(Q)^DRbar
Block Au Q=  3.95832879E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -4.69538340E+03   # A_t(Q)^DRbar
Block Ad Q=  3.95832879E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     8.71299049E+02   # A_b(Q)^DRbar
Block Ae Q=  3.95832879E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.98180312E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.95832879E+03  # soft SUSY breaking masses at Q
   1    6.11716167E+01  # M_1
   2    1.31088388E+03  # M_2
   3    1.21351608E+03  # M_3
  21    3.51785824E+05  # M^2_(H,d)
  22    1.21014533E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.45612461E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.57616802E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.81750812E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.23834239E+02  # h0
        35     7.86306301E+02  # H0
        36     7.85975753E+02  # A0
        37     7.89909135E+02  # H+
   1000001     1.01222567E+04  # ~d_L
   2000001     1.00976201E+04  # ~d_R
   1000002     1.01219061E+04  # ~u_L
   2000002     1.01011537E+04  # ~u_R
   1000003     1.01222571E+04  # ~s_L
   2000003     1.00976203E+04  # ~s_R
   1000004     1.01219065E+04  # ~c_L
   2000004     1.01011543E+04  # ~c_R
   1000005     3.46763678E+03  # ~b_1
   2000005     3.86380350E+03  # ~b_2
   1000006     3.46215867E+03  # ~t_1
   2000006     4.52560623E+03  # ~t_2
   1000011     1.00209385E+04  # ~e_L-
   2000011     1.00086651E+04  # ~e_R-
   1000012     1.00201832E+04  # ~nu_eL
   1000013     1.00209391E+04  # ~mu_L-
   2000013     1.00086661E+04  # ~mu_R-
   1000014     1.00201837E+04  # ~nu_muL
   1000015     1.00089362E+04  # ~tau_1-
   2000015     1.00211096E+04  # ~tau_2-
   1000016     1.00203328E+04  # ~nu_tauL
   1000021     1.54286692E+03  # ~g
   1000022     5.97320029E+01  # ~chi_10
   1000023     4.65612275E+02  # ~chi_20
   1000025     4.69537091E+02  # ~chi_30
   1000035     1.40073453E+03  # ~chi_40
   1000024     4.63865930E+02  # ~chi_1+
   1000037     1.39999470E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.21898194E-01   # alpha
Block Hmix Q=  3.95832879E+03  # Higgs mixing parameters
   1    4.58720941E+02  # mu
   2    8.05228416E+00  # tan[beta](Q)
   3    2.42935333E+02  # v(Q)
   4    6.17757884E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.96994204E-01   # Re[R_st(1,1)]
   1  2     7.74761750E-02   # Re[R_st(1,2)]
   2  1    -7.74761750E-02   # Re[R_st(2,1)]
   2  2     9.96994204E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99997385E-01   # Re[R_sb(1,1)]
   1  2     2.28681601E-03   # Re[R_sb(1,2)]
   2  1    -2.28681601E-03   # Re[R_sb(2,1)]
   2  2     9.99997385E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     4.20612328E-02   # Re[R_sta(1,1)]
   1  2     9.99115035E-01   # Re[R_sta(1,2)]
   2  1    -9.99115035E-01   # Re[R_sta(2,1)]
   2  2     4.20612328E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.94955342E-01   # Re[N(1,1)]
   1  2    -2.12996376E-03   # Re[N(1,2)]
   1  3     9.73110107E-02   # Re[N(1,3)]
   1  4    -2.42878087E-02   # Re[N(1,4)]
   2  1     8.61005245E-02   # Re[N(2,1)]
   2  2     6.79254204E-02   # Re[N(2,2)]
   2  3    -7.03619528E-01   # Re[N(2,3)]
   2  4     7.02062958E-01   # Re[N(2,4)]
   3  1     5.14295513E-02   # Re[N(3,1)]
   3  2    -2.67513874E-02   # Re[N(3,2)]
   3  3    -7.03273529E-01   # Re[N(3,3)]
   3  4    -7.08551839E-01   # Re[N(3,4)]
   4  1     2.35968539E-03   # Re[N(4,1)]
   4  2    -9.97329416E-01   # Re[N(4,2)]
   4  3    -2.92655346E-02   # Re[N(4,3)]
   4  4     6.68729884E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -4.13962325E-02   # Re[U(1,1)]
   1  2     9.99142809E-01   # Re[U(1,2)]
   2  1     9.99142809E-01   # Re[U(2,1)]
   2  2     4.13962325E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -9.46816138E-02   # Re[V(1,1)]
   1  2     9.95507605E-01   # Re[V(1,2)]
   2  1     9.95507605E-01   # Re[V(2,1)]
   2  2     9.46816138E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02841325E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.89978880E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     7.38209308E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     2.63367158E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.39808646E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.84112493E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     4.07677085E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.00896520E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     1.07662883E-03    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.05538666E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02937851E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.89789890E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     7.42816331E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     2.68060203E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
DECAY   1000013     1.39813485E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.84085233E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     4.09373199E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     3.00886155E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     1.07659164E-03    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.05517749E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.31722866E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     9.35867868E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.98819167E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.47117240E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.07912614E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.62944129E-02    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     2.16495198E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.41019402E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.79752279E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     8.76436401E-03    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     4.96920204E-03    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.97929246E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     8.12575039E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.99549385E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.39812547E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.98305460E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.51036744E-04    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     9.23507323E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02388620E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     5.63158449E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.01074706E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.39817383E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.98274429E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.51031527E-04    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     9.23475422E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02378175E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     5.66587233E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.01054002E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.41180856E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.89612467E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.49575165E-04    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     9.14570568E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.99462731E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.52368424E-02    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.95275034E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.65894158E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.28599788E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.92640234E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.92307695E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.60517125E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.37528193E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     4.78205601E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.70408612E-04    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     9.59123562E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.54289865E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.65896865E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.28598896E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.92636745E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.92312775E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.60517654E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.38275915E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     4.78202912E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.74509642E-04    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     9.59118509E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.54285038E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     2.78766249E+02   # ~b_1
#    BR                NDA      ID1      ID2
     1.80602571E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.31807264E-03    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.23351450E-03    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     3.81708150E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     2.45162668E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     7.87112731E-02    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     6.31597631E-01    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     2.20373016E+02   # ~b_2
#    BR                NDA      ID1      ID2
     9.74069787E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     3.15162287E-03    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     3.10690305E-03    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     6.18943107E-03    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     9.77776570E-01    2     1000021         5   # BR(~b_2 -> ~g b)
DECAY   2000002     7.83103907E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.85134894E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     2.12636436E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     9.71197859E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.92297943E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.52976973E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     3.44638605E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     4.77387755E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     8.91441367E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     9.52131910E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.54266796E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.83111297E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.85132237E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     2.14955049E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     9.71188718E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.92302993E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.52976354E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     3.46676331E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     4.77385256E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     8.92945585E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     9.52126589E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.54261965E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     2.78746723E+02   # ~t_1
#    BR                NDA      ID1      ID2
     2.21027182E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.21892385E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.24266936E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     3.79753898E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     9.11555098E-03    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     7.36703945E-02    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     6.30138750E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
     7.30285650E-04    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     5.29717496E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.86566092E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     8.52901467E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     8.63836376E-02    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.51831310E-03    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.69159811E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     3.03032740E-03    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     5.22337814E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     4.98623898E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     2.50562327E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     3.86344086E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     3.21884310E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.96928263E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     3.07132801E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     1.78334950E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.94687483E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.96724539E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     1.05836027E-03    2          37   1000022   # BR(chi^+_2 -> H^+ chi^0_1)
     4.67103560E-02    2          37   1000023   # BR(chi^+_2 -> H^+ chi^0_2)
     6.95511020E-02    2          37   1000025   # BR(chi^+_2 -> H^+ chi^0_3)
     1.94090939E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     4.76809770E-02    2     1000024        36   # BR(chi^+_2 -> chi^+_1 A^0)
     1.88808486E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
     4.72142369E-02    2     1000024        35   # BR(chi^+_2 -> chi^+_1 H^0)
#    BR                NDA      ID1      ID2       ID3
     5.22563789E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     4.75258320E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     3.41349623E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.84780344E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     3.10819183E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     6.89115175E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     3.76405190E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     6.69256609E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     3.30639267E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.92653233E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.81607065E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.81607065E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     4.33883328E-02    2          37  -1000024   # BR(chi^0_4 -> H^+ chi^-_1)
     4.33883328E-02    2         -37   1000024   # BR(chi^0_4 -> H^- chi^+_1)
     2.39787049E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.58455171E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     4.15040735E-04    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     5.60814898E-03    2     1000023        36   # BR(chi^0_4 -> chi^0_2 A^0)
     3.28740982E-03    2     1000025        36   # BR(chi^0_4 -> chi^0_3 A^0)
     1.51285228E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     9.37200369E-02    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
     5.70676476E-04    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
     3.81738672E-02    2     1000023        35   # BR(chi^0_4 -> chi^0_2 H^0)
     6.23167843E-02    2     1000025        35   # BR(chi^0_4 -> chi^0_3 H^0)
#    BR                NDA      ID1      ID2       ID3
     2.01946730E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     1.46245810E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     4.31767616E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     4.31767616E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     2.04012215E-03   # ~g
#    BR                NDA      ID1      ID2
     1.68904185E-04    2     1000022        21   # BR(~g -> chi^0_1 g)
     1.91154987E-02    2     1000023        21   # BR(~g -> chi^0_2 g)
     2.02722999E-02    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     8.19753273E-04    3     1000022         2        -2   # BR(~g -> chi^0_1 u u_bar)
     8.19753290E-04    3     1000022         4        -4   # BR(~g -> chi^0_1 c c_bar)
     1.83910572E-02    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     2.42584529E-04    3     1000022         1        -1   # BR(~g -> chi^0_1 d d_bar)
     2.42585739E-04    3     1000022         3        -3   # BR(~g -> chi^0_1 s s_bar)
     1.44322211E-02    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     2.11575221E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     6.70077878E-03    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     2.13495763E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     6.51060523E-03    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     2.43558089E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.43558089E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     3.53508407E-03   # Gamma(h0)
     2.47985032E-03   2        22        22   # BR(h0 -> photon photon)
     1.47950912E-03   2        22        23   # BR(h0 -> photon Z)
     2.61933610E-02   2        23        23   # BR(h0 -> Z Z)
     2.21482663E-01   2       -24        24   # BR(h0 -> W W)
     7.71176363E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.15524641E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.29313936E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.61179418E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.36476170E-07   2        -2         2   # BR(h0 -> Up up)
     2.64867690E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.97699290E-07   2        -1         1   # BR(h0 -> Down down)
     2.16174621E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.76285628E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     1.91041402E-03   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     1.54920942E+00   # Gamma(HH)
     1.25634786E-06   2        22        22   # BR(HH -> photon photon)
     4.13541089E-07   2        22        23   # BR(HH -> photon Z)
     1.16802870E-03   2        23        23   # BR(HH -> Z Z)
     1.98026330E-03   2       -24        24   # BR(HH -> W W)
     3.01514511E-04   2        21        21   # BR(HH -> gluon gluon)
     5.29335022E-09   2       -11        11   # BR(HH -> Electron electron)
     2.35588191E-04   2       -13        13   # BR(HH -> Muon muon)
     6.80250240E-02   2       -15        15   # BR(HH -> Tau tau)
     1.86146362E-11   2        -2         2   # BR(HH -> Up up)
     3.60972833E-06   2        -4         4   # BR(HH -> Charm charm)
     2.08257282E-01   2        -6         6   # BR(HH -> Top top)
     4.61469609E-07   2        -1         1   # BR(HH -> Down down)
     1.66916779E-04   2        -3         3   # BR(HH -> Strange strange)
     4.46786935E-01   2        -5         5   # BR(HH -> Bottom bottom)
     9.85514934E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     8.40095363E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.69699925E-01   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     9.50809045E-03   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.57664804E+00   # Gamma(A0)
     1.85936458E-06   2        22        22   # BR(A0 -> photon photon)
     5.83363942E-07   2        22        23   # BR(A0 -> photon Z)
     6.61222779E-04   2        21        21   # BR(A0 -> gluon gluon)
     5.13523238E-09   2       -11        11   # BR(A0 -> Electron electron)
     2.28550870E-04   2       -13        13   # BR(A0 -> Muon muon)
     6.59944235E-02   2       -15        15   # BR(A0 -> Tau tau)
     1.67609787E-11   2        -2         2   # BR(A0 -> Up up)
     3.24990737E-06   2        -4         4   # BR(A0 -> Charm charm)
     2.34496056E-01   2        -6         6   # BR(A0 -> Top top)
     4.47712380E-07   2        -1         1   # BR(A0 -> Down down)
     1.61940721E-04   2        -3         3   # BR(A0 -> Strange strange)
     4.33525586E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.11560254E-02   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.74841123E-01   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     7.71239089E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.80501711E-03   2        23        25   # BR(A0 -> Z h0)
     7.31123965E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.67413852E+00   # Gamma(Hp)
     5.60809501E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     2.39763569E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     6.78181348E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     4.25794516E-07   2        -1         2   # BR(Hp -> Down up)
     7.06467681E-06   2        -3         2   # BR(Hp -> Strange up)
     4.57515788E-06   2        -5         2   # BR(Hp -> Bottom up)
     1.99869576E-07   2        -1         4   # BR(Hp -> Down charm)
     1.57368125E-04   2        -3         4   # BR(Hp -> Strange charm)
     6.40688627E-04   2        -5         4   # BR(Hp -> Bottom charm)
     1.79966403E-05   2        -1         6   # BR(Hp -> Down top)
     3.92589134E-04   2        -3         6   # BR(Hp -> Strange top)
     6.80561298E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.48415313E-01   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     1.74457547E-03   2        24        25   # BR(Hp -> W h0)
     4.91052553E-10   2        24        35   # BR(Hp -> W HH)
     7.61321788E-10   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.73480968E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    6.48657992E+01    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    6.48392802E+01        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00040900E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.50137535E-02    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.54227499E-02        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.73480968E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    6.48657992E+01    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    6.48392802E+01        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99997252E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.74754255E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99997252E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.74754255E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26623823E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.39211019E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.41348802E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.74754255E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99997252E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.68198358E-04   # BR(b -> s gamma)
    2    1.58921783E-06   # BR(b -> s mu+ mu-)
    3    3.52956252E-05   # BR(b -> s nu nu)
    4    2.64393702E-15   # BR(Bd -> e+ e-)
    5    1.12945980E-10   # BR(Bd -> mu+ mu-)
    6    2.36436902E-08   # BR(Bd -> tau+ tau-)
    7    8.90503417E-14   # BR(Bs -> e+ e-)
    8    3.80422752E-09   # BR(Bs -> mu+ mu-)
    9    8.06895451E-07   # BR(Bs -> tau+ tau-)
   10    9.61036909E-05   # BR(B_u -> tau nu)
   11    9.92714334E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.43132401E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.94035120E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16207244E-03   # epsilon_K
   17    2.28170851E-15   # Delta(M_K)
   18    2.48271884E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29702074E-11   # BR(K^+ -> pi^+ nu nu)
   20    7.08437740E-17   # Delta(g-2)_electron/2
   21    3.02879281E-12   # Delta(g-2)_muon/2
   22    8.56766545E-10   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -6.33202653E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.35727417E-01   # C7
     0305 4322   00   2    -9.71568082E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.51654634E-01   # C8
     0305 6321   00   2    -1.13260077E-03   # C8'
 03051111 4133   00   0     1.62584406E+00   # C9 e+e-
 03051111 4133   00   2     1.62645341E+00   # C9 e+e-
 03051111 4233   00   2     1.12485792E-05   # C9' e+e-
 03051111 4137   00   0    -4.44853616E+00   # C10 e+e-
 03051111 4137   00   2    -4.45110478E+00   # C10 e+e-
 03051111 4237   00   2    -8.23495133E-05   # C10' e+e-
 03051313 4133   00   0     1.62584406E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62645340E+00   # C9 mu+mu-
 03051313 4233   00   2     1.12485152E-05   # C9' mu+mu-
 03051313 4137   00   0    -4.44853616E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.45110479E+00   # C10 mu+mu-
 03051313 4237   00   2    -8.23494504E-05   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50590734E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     1.78095342E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50590734E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     1.78095480E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50590734E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     1.78134344E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85825825E-07   # C7
     0305 4422   00   2     4.71059894E-06   # C7
     0305 4322   00   2     3.81332420E-07   # C7'
     0305 6421   00   0     3.30485336E-07   # C8
     0305 6421   00   2    -7.39377568E-06   # C8
     0305 6321   00   2    -1.00147972E-08   # C8'
 03051111 4133   00   2     5.03733829E-07   # C9 e+e-
 03051111 4233   00   2     4.38755520E-07   # C9' e+e-
 03051111 4137   00   2     6.38415489E-07   # C10 e+e-
 03051111 4237   00   2    -3.23416563E-06   # C10' e+e-
 03051313 4133   00   2     5.03733655E-07   # C9 mu+mu-
 03051313 4233   00   2     4.38755515E-07   # C9' mu+mu-
 03051313 4137   00   2     6.38415657E-07   # C10 mu+mu-
 03051313 4237   00   2    -3.23416565E-06   # C10' mu+mu-
 03051212 4137   00   2    -1.08147436E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     6.99464901E-07   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.08147428E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     6.99464901E-07   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.08145350E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     6.99464900E-07   # C11' nu_3 nu_3
