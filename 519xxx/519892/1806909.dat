# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  18:55
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.83948430E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.15900266E+03  # scale for input parameters
    1   -4.46437403E+01  # M_1
    2   -1.17073603E+03  # M_2
    3    3.82170804E+03  # M_3
   11   -6.26294270E+03  # A_t
   12    1.17655822E+03  # A_b
   13    7.50159118E+02  # A_tau
   23   -3.02727736E+02  # mu
   25    3.74076751E+01  # tan(beta)
   26    2.91633343E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.97541541E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.44393068E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.51659998E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.15900266E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.15900266E+03  # (SUSY scale)
  1  1     8.38736744E-06   # Y_u(Q)^DRbar
  2  2     4.26078266E-03   # Y_c(Q)^DRbar
  3  3     1.01326136E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.15900266E+03  # (SUSY scale)
  1  1     6.30473206E-04   # Y_d(Q)^DRbar
  2  2     1.19789909E-02   # Y_s(Q)^DRbar
  3  3     6.25231858E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.15900266E+03  # (SUSY scale)
  1  1     1.10022185E-04   # Y_e(Q)^DRbar
  2  2     2.27490984E-02   # Y_mu(Q)^DRbar
  3  3     3.82602410E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.15900266E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -6.26294261E+03   # A_t(Q)^DRbar
Block Ad Q=  3.15900266E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.17655823E+03   # A_b(Q)^DRbar
Block Ae Q=  3.15900266E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     7.50159120E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.15900266E+03  # soft SUSY breaking masses at Q
   1   -4.46437403E+01  # M_1
   2   -1.17073603E+03  # M_2
   3    3.82170804E+03  # M_3
  21    8.30123873E+06  # M^2_(H,d)
  22    4.02121633E+04  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.97541541E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.44393068E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.51659998E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.27616581E+02  # h0
        35     2.91552602E+03  # H0
        36     2.91633343E+03  # A0
        37     2.91716641E+03  # H+
   1000001     1.00919666E+04  # ~d_L
   2000001     1.00681407E+04  # ~d_R
   1000002     1.00916031E+04  # ~u_L
   2000002     1.00711158E+04  # ~u_R
   1000003     1.00919726E+04  # ~s_L
   2000003     1.00681510E+04  # ~s_R
   1000004     1.00916091E+04  # ~c_L
   2000004     1.00711171E+04  # ~c_R
   1000005     2.63110042E+03  # ~b_1
   2000005     4.01809695E+03  # ~b_2
   1000006     2.47620607E+03  # ~t_1
   2000006     4.03007567E+03  # ~t_2
   1000011     1.00205760E+04  # ~e_L-
   2000011     1.00089883E+04  # ~e_R-
   1000012     1.00198165E+04  # ~nu_eL
   1000013     1.00206030E+04  # ~mu_L-
   2000013     1.00090402E+04  # ~mu_R-
   1000014     1.00198433E+04  # ~nu_muL
   1000015     1.00235629E+04  # ~tau_1-
   2000015     1.00284331E+04  # ~tau_2-
   1000016     1.00274158E+04  # ~nu_tauL
   1000021     4.25716791E+03  # ~g
   1000022     4.36434751E+01  # ~chi_10
   1000023     3.08037189E+02  # ~chi_20
   1000025     3.11696712E+02  # ~chi_30
   1000035     1.26338660E+03  # ~chi_40
   1000024     3.05706726E+02  # ~chi_1+
   1000037     1.26289660E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -2.61198525E-02   # alpha
Block Hmix Q=  3.15900266E+03  # Higgs mixing parameters
   1   -3.02727736E+02  # mu
   2    3.74076751E+01  # tan[beta](Q)
   3    2.43174099E+02  # v(Q)
   4    8.50500067E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     8.76471045E-02   # Re[R_st(1,1)]
   1  2     9.96151587E-01   # Re[R_st(1,2)]
   2  1    -9.96151587E-01   # Re[R_st(2,1)]
   2  2     8.76471045E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -3.06365980E-03   # Re[R_sb(1,1)]
   1  2     9.99995307E-01   # Re[R_sb(1,2)]
   2  1    -9.99995307E-01   # Re[R_sb(2,1)]
   2  2    -3.06365980E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -2.30708805E-01   # Re[R_sta(1,1)]
   1  2     9.73022840E-01   # Re[R_sta(1,2)]
   2  1    -9.73022840E-01   # Re[R_sta(2,1)]
   2  2    -2.30708805E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.88555170E-01   # Re[N(1,1)]
   1  2    -1.81958151E-03   # Re[N(1,2)]
   1  3    -1.48753484E-01   # Re[N(1,3)]
   1  4     2.50552663E-02   # Re[N(1,4)]
   2  1     1.23160847E-01   # Re[N(2,1)]
   2  2     6.08195261E-02   # Re[N(2,2)]
   2  3     6.99630245E-01   # Re[N(2,3)]
   2  4    -7.01177518E-01   # Re[N(2,4)]
   3  1    -8.70816441E-02   # Re[N(3,1)]
   3  2     3.53391937E-02   # Re[N(3,2)]
   3  3    -6.98612528E-01   # Re[N(3,3)]
   3  4    -7.09301392E-01   # Re[N(3,4)]
   4  1     2.62092894E-03   # Re[N(4,1)]
   4  2    -9.97521336E-01   # Re[N(4,2)]
   4  3     1.81785040E-02   # Re[N(4,3)]
   4  4    -6.79253780E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -2.57106734E-02   # Re[U(1,1)]
   1  2    -9.99669426E-01   # Re[U(1,2)]
   2  1    -9.99669426E-01   # Re[U(2,1)]
   2  2     2.57106734E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     9.61395825E-02   # Re[V(1,1)]
   1  2     9.95367862E-01   # Re[V(1,2)]
   2  1     9.95367862E-01   # Re[V(2,1)]
   2  2    -9.61395825E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02874514E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.77283035E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.51411189E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     7.56914561E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.40744514E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.67946961E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     5.01431305E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.01141588E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     4.13535187E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.06596754E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.04934200E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.73346787E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.60764871E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     8.53241935E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     2.03572130E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.40847889E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.67474342E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     5.36850372E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     3.95884103E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.00921662E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     4.13232791E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.06153283E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.11923620E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     4.39258088E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.33707193E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.22055796E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     1.77172040E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.51557399E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     3.57043213E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.66718921E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.14016859E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     9.01740897E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     9.10025038E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.42650181E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     6.12332892E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.88648211E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.40748666E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.79899139E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.09566201E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02803746E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     5.78156567E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.01321370E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.40852030E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.79255779E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.09412973E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02582396E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     6.50788598E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.00882274E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.70008949E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     7.29012000E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.73629804E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.50890527E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.76125217E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.98340343E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.41563272E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.01425627E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.57214427E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     9.89621379E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.68848277E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.10072086E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.00304824E-04    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     1.71429468E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     6.40887538E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.28612945E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.04838205E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.41621521E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.01426532E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.83052439E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     1.04398274E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     9.89516842E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.68881271E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.10156892E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.21285619E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     1.92344562E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     6.40856465E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.28606731E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.04799397E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     4.11051765E+01   # ~b_1
#    BR                NDA      ID1      ID2
     4.60946896E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.37539678E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.36312454E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     4.79832738E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.46008815E-04    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     2.35987512E+02   # ~b_2
#    BR                NDA      ID1      ID2
     5.25153855E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     6.41709561E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     6.40764319E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     6.06548073E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     3.39312778E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.24784397E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     3.41327708E-01    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     1.62499948E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     2.58883951E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     5.58727595E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.93355255E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     6.09441225E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     3.04663413E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.59750102E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.68831714E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.01560780E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     4.53926540E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     6.39680564E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.22553479E-03    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.27506759E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.04804044E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.58734956E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.93350150E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     6.12627137E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     3.07928925E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.59737636E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.68864674E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.01551301E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     4.56579108E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     6.39649637E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.26831983E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.27500585E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.04765233E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.02083364E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.18136332E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.33451870E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.36745549E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     3.09282252E-03    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.68537712E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     6.35347538E-03    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     2.56571088E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.87842944E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.55983958E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.60620904E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     5.44945164E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     1.26885022E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.07779902E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     3.03796493E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.58702139E-01    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     2.32351333E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     2.08907940E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99714046E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     2.85944921E-04    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     1.13680638E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     2.49273488E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.46422479E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.45797073E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.41079769E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     6.12817657E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     5.48097384E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     5.70286022E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.62358442E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     4.51412443E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     5.48560756E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     2.35138485E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     6.39256705E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     3.60734759E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.26404356E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.25049642E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.25049642E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     5.54980880E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.68040731E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.62259509E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.47600693E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.45958211E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     2.38722103E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     5.76547043E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     5.76547043E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.04570539E+02   # ~g
#    BR                NDA      ID1      ID2
     2.80457733E-04    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     2.80457733E-04    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     2.57477275E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     2.57477275E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     4.84296931E-03    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     4.84296931E-03    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     2.30141476E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     2.30141476E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     7.17789332E-03    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     7.17789332E-03    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     4.03312156E-03   # Gamma(h0)
     2.46522328E-03   2        22        22   # BR(h0 -> photon photon)
     1.80775844E-03   2        22        23   # BR(h0 -> photon Z)
     3.66197820E-02   2        23        23   # BR(h0 -> Z Z)
     2.87764949E-01   2       -24        24   # BR(h0 -> W W)
     7.40476241E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.44359620E-09   2       -11        11   # BR(h0 -> Electron electron)
     1.97660350E-04   2       -13        13   # BR(h0 -> Muon muon)
     5.69955126E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.25537679E-07   2        -2         2   # BR(h0 -> Up up)
     2.43654114E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.12460975E-07   2        -1         1   # BR(h0 -> Down down)
     1.85345066E-04   2        -3         3   # BR(h0 -> Strange strange)
     4.92641006E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     2.29090858E-02   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     7.86227019E+01   # Gamma(HH)
     1.19157467E-08   2        22        22   # BR(HH -> photon photon)
     3.89107934E-08   2        22        23   # BR(HH -> photon Z)
     4.17905519E-07   2        23        23   # BR(HH -> Z Z)
     1.31040188E-07   2       -24        24   # BR(HH -> W W)
     6.56394686E-06   2        21        21   # BR(HH -> gluon gluon)
     7.60078783E-09   2       -11        11   # BR(HH -> Electron electron)
     3.38421072E-04   2       -13        13   # BR(HH -> Muon muon)
     9.77396695E-02   2       -15        15   # BR(HH -> Tau tau)
     5.76563196E-14   2        -2         2   # BR(HH -> Up up)
     1.11848839E-08   2        -4         4   # BR(HH -> Charm charm)
     9.00176770E-04   2        -6         6   # BR(HH -> Top top)
     5.68398407E-07   2        -1         1   # BR(HH -> Down down)
     2.05578336E-04   2        -3         3   # BR(HH -> Strange strange)
     5.46307745E-01   2        -5         5   # BR(HH -> Bottom bottom)
     3.02960629E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.02552680E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     1.02552680E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     4.34687008E-05   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     1.77674188E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.95014894E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     2.02854127E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.87503413E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     7.01872964E-07   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     4.46799878E-04   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     4.29666306E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     1.00023485E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     5.84445949E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     2.19124336E-05   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     2.67136255E-06   2        25        25   # BR(HH -> h0 h0)
DECAY        36     7.59099292E+01   # Gamma(A0)
     7.08838594E-08   2        22        22   # BR(A0 -> photon photon)
     1.01356258E-07   2        22        23   # BR(A0 -> photon Z)
     1.36127682E-05   2        21        21   # BR(A0 -> gluon gluon)
     7.44985879E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.31699451E-04   2       -13        13   # BR(A0 -> Muon muon)
     9.57985540E-02   2       -15        15   # BR(A0 -> Tau tau)
     5.60567570E-14   2        -2         2   # BR(A0 -> Up up)
     1.08738937E-08   2        -4         4   # BR(A0 -> Charm charm)
     8.88477947E-04   2        -6         6   # BR(A0 -> Top top)
     5.57089653E-07   2        -1         1   # BR(A0 -> Down down)
     2.01488490E-04   2        -3         3   # BR(A0 -> Strange strange)
     5.35449073E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     3.37564614E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.06072388E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     1.06072388E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     1.87956835E-04   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     1.87513341E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     2.25648710E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.87051266E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.97675969E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     8.43471963E-07   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     4.94289621E-04   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     6.05680658E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     9.75379858E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     4.43522539E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     9.45533354E-05   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     6.90110555E-07   2        23        25   # BR(A0 -> Z h0)
     4.06638583E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     8.37988448E+01   # Gamma(Hp)
     8.48651983E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     3.62825228E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.02627602E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     5.52860269E-07   2        -1         2   # BR(Hp -> Down up)
     9.32838077E-06   2        -3         2   # BR(Hp -> Strange up)
     5.96017399E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.63678754E-08   2        -1         4   # BR(Hp -> Down charm)
     1.99238931E-04   2        -3         4   # BR(Hp -> Strange charm)
     8.34637603E-04   2        -5         4   # BR(Hp -> Bottom charm)
     6.09464005E-08   2        -1         6   # BR(Hp -> Down top)
     1.62360603E-06   2        -3         6   # BR(Hp -> Strange top)
     5.63602567E-01   2        -5         6   # BR(Hp -> Bottom top)
     3.96804865E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     3.56428201E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     1.40895796E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     9.62034560E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     6.29378907E-05   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     9.48459770E-02   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     9.65888441E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     5.62818395E-10   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     6.26093397E-07   2        24        25   # BR(Hp -> W h0)
     1.92940547E-13   2        24        35   # BR(Hp -> W HH)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.55156103E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.39937900E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.39933416E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00003205E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    6.82578995E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    7.14625592E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.55156103E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.39937900E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.39933416E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999632E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    3.67556344E-07        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999632E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    3.67556344E-07        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.25922187E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    6.12587513E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.18305318E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    3.67556344E-07        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999632E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.44774113E-04   # BR(b -> s gamma)
    2    1.58792458E-06   # BR(b -> s mu+ mu-)
    3    3.52508712E-05   # BR(b -> s nu nu)
    4    1.36018552E-15   # BR(Bd -> e+ e-)
    5    5.81051092E-11   # BR(Bd -> mu+ mu-)
    6    1.21367472E-08   # BR(Bd -> tau+ tau-)
    7    4.62984727E-14   # BR(Bs -> e+ e-)
    8    1.97785581E-09   # BR(Bs -> mu+ mu-)
    9    4.18742252E-07   # BR(Bs -> tau+ tau-)
   10    9.57337501E-05   # BR(B_u -> tau nu)
   11    9.88892988E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42163820E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93609933E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15839410E-03   # epsilon_K
   17    2.28167016E-15   # Delta(M_K)
   18    2.48036889E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29113203E-11   # BR(K^+ -> pi^+ nu nu)
   20    3.40772999E-16   # Delta(g-2)_electron/2
   21    1.45691719E-11   # Delta(g-2)_muon/2
   22    4.12564407E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.56894980E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.16445891E-01   # C7
     0305 4322   00   2    -2.68988136E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.15001574E-01   # C8
     0305 6321   00   2    -2.30095161E-04   # C8'
 03051111 4133   00   0     1.61866640E+00   # C9 e+e-
 03051111 4133   00   2     1.61891459E+00   # C9 e+e-
 03051111 4233   00   2     3.07658423E-04   # C9' e+e-
 03051111 4137   00   0    -4.44135850E+00   # C10 e+e-
 03051111 4137   00   2    -4.43960469E+00   # C10 e+e-
 03051111 4237   00   2    -2.30308270E-03   # C10' e+e-
 03051313 4133   00   0     1.61866640E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61891439E+00   # C9 mu+mu-
 03051313 4233   00   2     3.07658069E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44135850E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43960489E+00   # C10 mu+mu-
 03051313 4237   00   2    -2.30308265E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50497147E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     4.98911780E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50497147E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     4.98911841E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50497148E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     4.98928853E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85796940E-07   # C7
     0305 4422   00   2     4.20454934E-06   # C7
     0305 4322   00   2    -2.35463457E-06   # C7'
     0305 6421   00   0     3.30460594E-07   # C8
     0305 6421   00   2     1.16689802E-05   # C8
     0305 6321   00   2    -9.51015249E-07   # C8'
 03051111 4133   00   2    -6.41316590E-07   # C9 e+e-
 03051111 4233   00   2     6.18655356E-06   # C9' e+e-
 03051111 4137   00   2     7.16638772E-06   # C10 e+e-
 03051111 4237   00   2    -4.63153282E-05   # C10' e+e-
 03051313 4133   00   2    -6.41318659E-07   # C9 mu+mu-
 03051313 4233   00   2     6.18655223E-06   # C9' mu+mu-
 03051313 4137   00   2     7.16639066E-06   # C10 mu+mu-
 03051313 4237   00   2    -4.63153323E-05   # C10' mu+mu-
 03051212 4137   00   2    -1.52942989E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     1.00331907E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.52942978E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     1.00331907E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.52939838E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     1.00331907E-05   # C11' nu_3 nu_3
