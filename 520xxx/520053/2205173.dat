# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 08.09.2021,  10:05
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.78815716E+00  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    2.58785035E+03  # scale for input parameters
    1    4.53969891E+01  # M_1
    2    1.42869790E+03  # M_2
    3    2.34186296E+03  # M_3
   11    2.16149564E+03  # A_t
   12   -9.47418796E+02  # A_b
   13   -4.74589604E+02  # A_tau
   23   -3.39360494E+02  # mu
   25    2.64167659E+00  # tan(beta)
   26    3.02598958E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.35552486E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.73375217E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.69768949E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  2.58785035E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  2.58785035E+03  # (SUSY scale)
  1  1     8.96500021E-06   # Y_u(Q)^DRbar
  2  2     4.55422010E-03   # Y_c(Q)^DRbar
  3  3     1.08304403E+00   # Y_t(Q)^DRbar
Block Yd Q=  2.58785035E+03  # (SUSY scale)
  1  1     4.75893967E-05   # Y_d(Q)^DRbar
  2  2     9.04198537E-04   # Y_s(Q)^DRbar
  3  3     4.71937691E-02   # Y_b(Q)^DRbar
Block Ye Q=  2.58785035E+03  # (SUSY scale)
  1  1     8.30469772E-06   # Y_e(Q)^DRbar
  2  2     1.71714810E-03   # Y_mu(Q)^DRbar
  3  3     2.88796062E-02   # Y_tau(Q)^DRbar
Block Au Q=  2.58785035E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     2.16149565E+03   # A_t(Q)^DRbar
Block Ad Q=  2.58785035E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -9.47418793E+02   # A_b(Q)^DRbar
Block Ae Q=  2.58785035E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -4.74589602E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  2.58785035E+03  # soft SUSY breaking masses at Q
   1    4.53969891E+01  # M_1
   2    1.42869790E+03  # M_2
   3    2.34186296E+03  # M_3
  21    7.88201353E+06  # M^2_(H,d)
  22    1.17898765E+06  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.35552486E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.73375217E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.69768949E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.09580443E+02  # h0
        35     3.02633746E+03  # H0
        36     3.02598958E+03  # A0
        37     3.02886777E+03  # H+
   1000001     1.01067509E+04  # ~d_L
   2000001     1.00834105E+04  # ~d_R
   1000002     1.01064614E+04  # ~u_L
   2000002     1.00862203E+04  # ~u_R
   1000003     1.01067521E+04  # ~s_L
   2000003     1.00834106E+04  # ~s_R
   1000004     1.01064626E+04  # ~c_L
   2000004     1.00862226E+04  # ~c_R
   1000005     2.43187969E+03  # ~b_1
   2000005     4.74738422E+03  # ~b_2
   1000006     2.42560823E+03  # ~t_1
   2000006     2.76094440E+03  # ~t_2
   1000011     1.00197279E+04  # ~e_L-
   2000011     1.00091691E+04  # ~e_R-
   1000012     1.00190425E+04  # ~nu_eL
   1000013     1.00197282E+04  # ~mu_L-
   2000013     1.00091696E+04  # ~mu_R-
   1000014     1.00190428E+04  # ~nu_muL
   1000015     1.00092963E+04  # ~tau_1-
   2000015     1.00197934E+04  # ~tau_2-
   1000016     1.00191076E+04  # ~nu_tauL
   1000021     2.73446311E+03  # ~g
   1000022     4.95549467E+01  # ~chi_10
   1000023     3.50019849E+02  # ~chi_20
   1000025     3.56385060E+02  # ~chi_30
   1000035     1.52666543E+03  # ~chi_40
   1000024     3.51493032E+02  # ~chi_1+
   1000037     1.52662905E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -3.45053092E-01   # alpha
Block Hmix Q=  2.58785035E+03  # Higgs mixing parameters
   1   -3.39360494E+02  # mu
   2    2.64167659E+00  # tan[beta](Q)
   3    2.43461044E+02  # v(Q)
   4    9.15661294E+06  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -9.86327390E-01   # Re[R_st(1,1)]
   1  2     1.64797695E-01   # Re[R_st(1,2)]
   2  1    -1.64797695E-01   # Re[R_st(2,1)]
   2  2    -9.86327390E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99999999E-01   # Re[R_sb(1,1)]
   1  2     3.43225335E-05   # Re[R_sb(1,2)]
   2  1    -3.43225335E-05   # Re[R_sb(2,1)]
   2  2     9.99999999E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -3.72410780E-03   # Re[R_sta(1,1)]
   1  2     9.99993065E-01   # Re[R_sta(1,2)]
   2  1    -9.99993065E-01   # Re[R_sta(2,1)]
   2  2    -3.72410780E-03   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.93109148E-01   # Re[N(1,1)]
   1  2    -3.66479975E-03   # Re[N(1,2)]
   1  3     1.13786168E-01   # Re[N(1,3)]
   1  4    -2.78118052E-02   # Re[N(1,4)]
   2  1    -6.08967224E-02   # Re[N(2,1)]
   2  2    -2.83831811E-02   # Re[N(2,2)]
   2  3    -7.04982260E-01   # Re[N(2,3)]
   2  4    -7.06035408E-01   # Re[N(2,4)]
   3  1     1.00118320E-01   # Re[N(3,1)]
   3  2    -3.90542276E-02   # Re[N(3,2)]
   3  3     6.99994710E-01   # Re[N(3,3)]
   3  4    -7.06015932E-01   # Re[N(3,4)]
   4  1     1.45965712E-03   # Re[N(4,1)]
   4  2    -9.98827178E-01   # Re[N(4,2)]
   4  3    -7.75421140E-03   # Re[N(4,3)]
   4  4     4.77703886E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1     1.09198136E-02   # Re[U(1,1)]
   1  2    -9.99940377E-01   # Re[U(1,2)]
   2  1     9.99940377E-01   # Re[U(2,1)]
   2  2     1.09198136E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -6.76593206E-02   # Re[V(1,1)]
   1  2     9.97708483E-01   # Re[V(1,2)]
   2  1     9.97708483E-01   # Re[V(2,1)]
   2  2     6.76593206E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02882487E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.86299206E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     3.69965323E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     9.99910771E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.38828344E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     9.06513586E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     1.17591390E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.01912364E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     6.06118800E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02894226E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.86276384E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     3.70535513E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     1.00045806E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
DECAY   1000013     1.38828935E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     9.06510303E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     1.17800798E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     3.01911089E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     6.06116240E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     5.06214239E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     9.79876275E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     5.30070111E-03    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.15109728E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     3.26616581E-03    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
DECAY   2000015     1.38993970E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     9.05544083E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.76798932E-03    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     6.59950260E-04    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     3.01549572E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     6.05390345E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.38828285E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.81748483E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.71335132E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02834087E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     2.90112869E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.03371130E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.38828876E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.81744757E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.71333986E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02832808E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     2.90533901E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.03368583E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.38995103E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.80695958E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     2.71011254E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     3.02472787E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     4.09065762E-03    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     6.02651399E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     6.88785758E+02   # ~d_R
#    BR                NDA      ID1      ID2
     8.06043093E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.91827595E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.14088775E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.63879991E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.76369147E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     5.20836227E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.04345482E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.41725956E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     6.88786093E+02   # ~s_R
#    BR                NDA      ID1      ID2
     8.06043017E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.91827126E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.14093221E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.63879376E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.76466616E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     5.20833450E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.04344948E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.41721490E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     6.59672288E+01   # ~b_1
#    BR                NDA      ID1      ID2
     4.88358585E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     8.26618886E-04    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.26849555E-03    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     5.94599590E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     8.12518377E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.21042964E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     1.70392341E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.53539143E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     6.63916555E-04    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     7.52140600E-04    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     1.21758090E-03    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     9.82011925E-01    2     1000021         5   # BR(~b_2 -> ~g b)
DECAY   2000002     7.05929940E+02   # ~u_R
#    BR                NDA      ID1      ID2
     3.14674697E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.18040230E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     3.19029507E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.68095396E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.14078106E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.78042619E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     5.20284880E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     4.99080600E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.03878306E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.41705259E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.05938421E+02   # ~c_R
#    BR                NDA      ID1      ID2
     3.14671022E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.20970929E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     3.21956442E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.68084013E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.14082521E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.78042063E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     5.20282237E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     4.99278838E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.03877756E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.41700788E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     6.67858918E+01   # ~t_1
#    BR                NDA      ID1      ID2
     9.51461708E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     4.03707155E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     3.95365993E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     5.95302024E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     1.38717769E-02    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.17110035E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     9.00221367E-04    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     1.38753360E+02   # ~t_2
#    BR                NDA      ID1      ID2
     4.17782509E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.21248362E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.24998213E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.50703069E-04    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     4.41306062E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     2.62569380E-04    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.87754731E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.44243467E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     2.70560201E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.93583671E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99389427E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     6.10573046E-04    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     9.86977551E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     3.21861734E-03    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.45199273E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.39972241E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.43917460E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.40583208E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     7.47181664E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     6.36266329E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     1.31727769E-02    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.83679452E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     7.76073866E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     2.23918232E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     2.37270588E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     2.29169973E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     7.70817641E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.22158109E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     1.98391696E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     1.98391696E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.30159255E-01    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     6.74470956E-02    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     2.33311080E-03    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     7.07824798E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     3.14913762E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.93901925E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     5.13146313E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     4.72144061E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     4.72144061E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     7.55201918E+00   # ~g
#    BR                NDA      ID1      ID2
     2.44166368E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     2.44166368E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.34208121E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     2.34208121E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     9.11745165E-04    2     1000023        21   # BR(~g -> chi^0_2 g)
     9.04155250E-04    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     1.99915214E-03    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     9.70857896E-03    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     9.81409005E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     9.92000331E-03    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     9.92000331E-03    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
DECAY        25     2.32952618E-03   # Gamma(h0)
     2.30742081E-03   2        22        22   # BR(h0 -> photon photon)
     4.12673006E-04   2        22        23   # BR(h0 -> photon Z)
     4.89249253E-03   2        23        23   # BR(h0 -> Z Z)
     5.18466995E-02   2       -24        24   # BR(h0 -> W W)
     8.25485160E-02   2        21        21   # BR(h0 -> gluon gluon)
     6.12705132E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.72531308E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.85514079E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.78188546E-07   2        -2         2   # BR(h0 -> Up up)
     3.45733919E-02   2        -4         4   # BR(h0 -> Charm charm)
     7.26231372E-07   2        -1         1   # BR(h0 -> Down down)
     2.62658916E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.97645913E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     4.66853851E-02   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     3.53712704E+01   # Gamma(HH)
     4.13153596E-07   2        22        22   # BR(HH -> photon photon)
     5.26945439E-08   2        22        23   # BR(HH -> photon Z)
     7.30576025E-05   2        23        23   # BR(HH -> Z Z)
     2.18071234E-05   2       -24        24   # BR(HH -> W W)
     1.15454765E-04   2        21        21   # BR(HH -> gluon gluon)
     8.59285910E-11   2       -11        11   # BR(HH -> Electron electron)
     3.82596436E-06   2       -13        13   # BR(HH -> Muon muon)
     1.10498684E-03   2       -15        15   # BR(HH -> Tau tau)
     2.35306180E-11   2        -2         2   # BR(HH -> Up up)
     4.56471508E-06   2        -4         4   # BR(HH -> Charm charm)
     3.41468056E-01   2        -6         6   # BR(HH -> Top top)
     6.31851538E-09   2        -1         1   # BR(HH -> Down down)
     2.28543512E-06   2        -3         3   # BR(HH -> Strange strange)
     5.99414127E-03   2        -5         5   # BR(HH -> Bottom bottom)
     2.72902029E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.83084725E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     1.83084725E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     5.61058177E-06   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     1.89421150E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     7.71639880E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.54279854E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.77576880E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     2.04348893E-06   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     2.49412327E-03   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     1.41448394E-01   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     1.00180897E-03   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     4.06928352E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     2.76507094E-06   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     4.03343362E-04   2        25        25   # BR(HH -> h0 h0)
DECAY        36     3.46675048E+01   # Gamma(A0)
     5.39560719E-07   2        22        22   # BR(A0 -> photon photon)
     2.36962638E-07   2        22        23   # BR(A0 -> photon Z)
     1.48216840E-04   2        21        21   # BR(A0 -> gluon gluon)
     8.45941156E-11   2       -11        11   # BR(A0 -> Electron electron)
     3.76654346E-06   2       -13        13   # BR(A0 -> Muon muon)
     1.08782679E-03   2       -15        15   # BR(A0 -> Tau tau)
     2.23435529E-11   2        -2         2   # BR(A0 -> Up up)
     4.33427236E-06   2        -4         4   # BR(A0 -> Charm charm)
     3.28464513E-01   2        -6         6   # BR(A0 -> Top top)
     6.22033007E-09   2        -1         1   # BR(A0 -> Down down)
     2.24991647E-06   2        -3         3   # BR(A0 -> Strange strange)
     5.90091427E-03   2        -5         5   # BR(A0 -> Bottom bottom)
     3.54645106E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.86494614E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     1.86494614E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     2.56478449E-04   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     2.68897609E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.76226785E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     7.42519617E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     3.62337496E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     4.88474756E-07   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     5.74393868E-04   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     4.24739725E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     4.93097974E-03   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     1.41177698E-01   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     1.28671369E-04   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     1.22042911E-04   2        23        25   # BR(A0 -> Z h0)
     7.44644965E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     3.87637308E+01   # Gamma(Hp)
     1.00449993E-10   2       -11        12   # BR(Hp -> Electron nu_e)
     4.29455092E-06   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.21474322E-03   2       -15        16   # BR(Hp -> Tau nu_tau)
     6.43705672E-09   2        -1         2   # BR(Hp -> Down up)
     1.08222080E-07   2        -3         2   # BR(Hp -> Strange up)
     6.97009375E-08   2        -5         2   # BR(Hp -> Bottom up)
     2.36392099E-07   2        -1         4   # BR(Hp -> Down charm)
     7.35657284E-06   2        -3         4   # BR(Hp -> Strange charm)
     9.76845797E-06   2        -5         4   # BR(Hp -> Bottom charm)
     2.58326362E-05   2        -1         6   # BR(Hp -> Down top)
     5.63240503E-04   2        -3         6   # BR(Hp -> Strange top)
     4.03495105E-01   2        -5         6   # BR(Hp -> Bottom top)
     8.85321690E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     4.88032091E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     1.49361636E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.67351012E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     3.83207375E-04   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.64362031E-01   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     1.67567356E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     2.31143545E-10   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     1.09526251E-04   2        24        25   # BR(Hp -> W h0)
     3.64185730E-12   2        24        35   # BR(Hp -> W HH)
     6.93509548E-12   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.12821361E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    7.06563385E+00    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    6.97845521E+00        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.01249254E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.30805649E-01    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.43298190E-01        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.12821361E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    7.06563385E+00    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    6.97845521E+00        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99716978E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.83022267E-04        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99716978E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.83022267E-04        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.28581611E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.24855425E-01        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    3.83438429E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.83022267E-04        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99716978E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.25398878E-04   # BR(b -> s gamma)
    2    1.59044599E-06   # BR(b -> s mu+ mu-)
    3    3.52660392E-05   # BR(b -> s nu nu)
    4    2.53852801E-15   # BR(Bd -> e+ e-)
    5    1.08443035E-10   # BR(Bd -> mu+ mu-)
    6    2.27014809E-08   # BR(Bd -> tau+ tau-)
    7    8.55771565E-14   # BR(Bs -> e+ e-)
    8    3.65585339E-09   # BR(Bs -> mu+ mu-)
    9    7.75437461E-07   # BR(Bs -> tau+ tau-)
   10    9.68039127E-05   # BR(B_u -> tau nu)
   11    9.99947358E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.44117685E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.94417774E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16604512E-03   # epsilon_K
   17    2.28174811E-15   # Delta(M_K)
   18    2.48126829E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29344631E-11   # BR(K^+ -> pi^+ nu nu)
   20   -5.07313864E-17   # Delta(g-2)_electron/2
   21   -2.16892530E-12   # Delta(g-2)_muon/2
   22   -6.13497853E-10   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39   -1.08840865E-03   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.93907970E-01   # C7
     0305 4322   00   2    -1.26243675E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.02941598E-01   # C8
     0305 6321   00   2    -1.63799453E-04   # C8'
 03051111 4133   00   0     1.61109958E+00   # C9 e+e-
 03051111 4133   00   2     1.61209389E+00   # C9 e+e-
 03051111 4233   00   2     2.53123527E-06   # C9' e+e-
 03051111 4137   00   0    -4.43379168E+00   # C10 e+e-
 03051111 4137   00   2    -4.43344133E+00   # C10 e+e-
 03051111 4237   00   2    -1.87036134E-05   # C10' e+e-
 03051313 4133   00   0     1.61109958E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61209389E+00   # C9 mu+mu-
 03051313 4233   00   2     2.53123526E-06   # C9' mu+mu-
 03051313 4137   00   0    -4.43379168E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.43344133E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.87036134E-05   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50527551E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     4.05802193E-06   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50527551E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     4.05802193E-06   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50527551E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     4.05802234E-06   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85823336E-07   # C7
     0305 4422   00   2    -1.30126149E-06   # C7
     0305 4322   00   2     2.06274497E-08   # C7'
     0305 6421   00   0     3.30483203E-07   # C8
     0305 6421   00   2     4.57215487E-07   # C8
     0305 6321   00   2     1.56712793E-08   # C8'
 03051111 4133   00   2     3.55476558E-07   # C9 e+e-
 03051111 4233   00   2     4.86232564E-08   # C9' e+e-
 03051111 4137   00   2     1.32379339E-06   # C10 e+e-
 03051111 4237   00   2    -3.59923482E-07   # C10' e+e-
 03051313 4133   00   2     3.55476543E-07   # C9 mu+mu-
 03051313 4233   00   2     4.86232563E-08   # C9' mu+mu-
 03051313 4137   00   2     1.32379340E-06   # C10 mu+mu-
 03051313 4237   00   2    -3.59923483E-07   # C10' mu+mu-
 03051212 4137   00   2    -2.61948414E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     7.80915249E-08   # C11' nu_1 nu_1
 03051414 4137   00   2    -2.61948414E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     7.80915249E-08   # C11' nu_2 nu_2
 03051616 4137   00   2    -2.61948235E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     7.80915249E-08   # C11' nu_3 nu_3
