# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 08.09.2021,  18:01
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.98033522E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    4.44479483E+03  # scale for input parameters
    1    4.48060977E+01  # M_1
    2    1.39321072E+03  # M_2
    3    1.71788289E+03  # M_3
   11    4.22448584E+03  # A_t
   12    1.07747779E+03  # A_b
   13   -2.34875129E+01  # A_tau
   23    3.75966991E+02  # mu
   25    2.86223004E+01  # tan(beta)
   26    4.29085163E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.70489767E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.22104876E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.35301924E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  4.44479483E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  4.44479483E+03  # (SUSY scale)
  1  1     8.38948776E-06   # Y_u(Q)^DRbar
  2  2     4.26185978E-03   # Y_c(Q)^DRbar
  3  3     1.01351751E+00   # Y_t(Q)^DRbar
Block Yd Q=  4.44479483E+03  # (SUSY scale)
  1  1     4.82525454E-04   # Y_d(Q)^DRbar
  2  2     9.16798362E-03   # Y_s(Q)^DRbar
  3  3     4.78514049E-01   # Y_b(Q)^DRbar
Block Ye Q=  4.44479483E+03  # (SUSY scale)
  1  1     8.42042202E-05   # Y_e(Q)^DRbar
  2  2     1.74107621E-02   # Y_mu(Q)^DRbar
  3  3     2.92820377E-01   # Y_tau(Q)^DRbar
Block Au Q=  4.44479483E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     4.22448585E+03   # A_t(Q)^DRbar
Block Ad Q=  4.44479483E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.07747779E+03   # A_b(Q)^DRbar
Block Ae Q=  4.44479483E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -2.34875129E+01   # A_tau(Q)^DRbar
Block MSOFT Q=  4.44479483E+03  # soft SUSY breaking masses at Q
   1    4.48060977E+01  # M_1
   2    1.39321072E+03  # M_2
   3    1.71788289E+03  # M_3
  21    1.83341576E+07  # M^2_(H,d)
  22    2.95723361E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.70489767E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.22104876E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.35301924E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.24741781E+02  # h0
        35     4.29003459E+03  # H0
        36     4.29085163E+03  # A0
        37     4.29103596E+03  # H+
   1000001     1.01199854E+04  # ~d_L
   2000001     1.00954962E+04  # ~d_R
   1000002     1.01196250E+04  # ~u_L
   2000002     1.00981756E+04  # ~u_R
   1000003     1.01199856E+04  # ~s_L
   2000003     1.00954963E+04  # ~s_R
   1000004     1.01196252E+04  # ~c_L
   2000004     1.00981758E+04  # ~c_R
   1000005     4.39651759E+03  # ~b_1
   2000005     4.71633035E+03  # ~b_2
   1000006     4.18134923E+03  # ~t_1
   2000006     4.72483881E+03  # ~t_2
   1000011     1.00206374E+04  # ~e_L-
   2000011     1.00094520E+04  # ~e_R-
   1000012     1.00198727E+04  # ~nu_eL
   1000013     1.00206380E+04  # ~mu_L-
   2000013     1.00094522E+04  # ~mu_R-
   1000014     1.00198730E+04  # ~nu_muL
   1000015     1.00095277E+04  # ~tau_1-
   2000015     1.00208032E+04  # ~tau_2-
   1000016     1.00199569E+04  # ~nu_tauL
   1000021     2.12549117E+03  # ~g
   1000022     4.39306311E+01  # ~chi_10
   1000023     3.92228508E+02  # ~chi_20
   1000025     3.95794949E+02  # ~chi_30
   1000035     1.49451180E+03  # ~chi_40
   1000024     3.90848296E+02  # ~chi_1+
   1000037     1.49447746E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -3.35861406E-02   # alpha
Block Hmix Q=  4.44479483E+03  # Higgs mixing parameters
   1    3.75966991E+02  # mu
   2    2.86223004E+01  # tan[beta](Q)
   3    2.42818590E+02  # v(Q)
   4    1.84114077E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -1.16651721E-01   # Re[R_st(1,1)]
   1  2     9.93172883E-01   # Re[R_st(1,2)]
   2  1    -9.93172883E-01   # Re[R_st(2,1)]
   2  2    -1.16651721E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     6.89789127E-03   # Re[R_sb(1,1)]
   1  2     9.99976209E-01   # Re[R_sb(1,2)]
   2  1    -9.99976209E-01   # Re[R_sb(2,1)]
   2  2     6.89789127E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     8.58282868E-02   # Re[R_sta(1,1)]
   1  2     9.96309944E-01   # Re[R_sta(1,2)]
   2  1    -9.96309944E-01   # Re[R_sta(2,1)]
   2  2     8.58282868E-02   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.93221863E-01   # Re[N(1,1)]
   1  2    -1.09213179E-03   # Re[N(1,2)]
   1  3     1.15001879E-01   # Re[N(1,3)]
   1  4    -1.68435678E-02   # Re[N(1,4)]
   2  1    -9.33769582E-02   # Re[N(2,1)]
   2  2    -5.29618248E-02   # Re[N(2,2)]
   2  3     7.02981194E-01   # Re[N(2,3)]
   2  4    -7.03059905E-01   # Re[N(2,4)]
   3  1     6.91929212E-02   # Re[N(3,1)]
   3  2    -2.91413894E-02   # Re[N(3,2)]
   3  3    -7.01644509E-01   # Re[N(3,3)]
   3  4    -7.08560585E-01   # Re[N(3,4)]
   4  1     1.84768715E-03   # Re[N(4,1)]
   4  2    -9.98170643E-01   # Re[N(4,2)]
   4  3    -1.69408591E-02   # Re[N(4,3)]
   4  4     5.80082887E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -2.39516068E-02   # Re[U(1,1)]
   1  2     9.99713119E-01   # Re[U(1,2)]
   2  1     9.99713119E-01   # Re[U(2,1)]
   2  2     2.39516068E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -8.20533475E-02   # Re[V(1,1)]
   1  2     9.96627939E-01   # Re[V(1,2)]
   2  1     9.96627939E-01   # Re[V(2,1)]
   2  2     8.20533475E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02898258E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.86530406E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     8.69320725E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     4.77309502E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.39095978E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.89019876E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     3.34368441E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     3.01436172E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     3.62711074E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.05936095E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.04101812E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.84190899E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     9.26238563E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     5.34935740E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.19311735E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.39156231E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.88692421E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.55618128E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     2.32462325E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.01305792E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     3.62554044E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.05673768E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     8.48725256E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     5.87362538E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.04386316E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     9.89835880E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     2.79764121E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     2.00842914E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     5.62700326E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.55600936E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.08467913E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     5.70450386E-02    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     5.53380491E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.68000771E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     1.09320334E-04    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     5.38660030E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.39099920E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.96275759E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.38120981E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02597101E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     4.25637119E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.02134748E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.39160164E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.95887775E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.38061190E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02466112E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     4.68717074E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.01874335E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.56141034E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     7.98463611E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.23047602E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.69574354E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     1.12862392E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     5.36483749E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.33427790E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.58068474E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.92315681E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.59169517E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.64182300E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     4.94592150E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     9.91592054E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.49523301E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.33461365E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.58064131E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.92270267E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.59190006E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.64204362E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     4.94580422E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     9.91568707E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.49503062E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     2.45589158E+02   # ~b_1
#    BR                NDA      ID1      ID2
     1.10029243E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     3.97497892E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     3.94958573E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     7.99993870E-02    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     8.29732184E-01    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     4.12646172E+02   # ~b_2
#    BR                NDA      ID1      ID2
     2.23839935E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     2.54385497E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.53901292E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     4.06099630E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.28186305E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     8.29152033E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     5.72910802E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     2.22862711E-02    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
DECAY   2000002     7.50570072E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.96380587E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     2.61181080E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     1.43404123E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.69957258E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.59158002E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.60173061E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     2.50310911E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     4.93927915E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     6.96072369E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     9.85456244E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.49498848E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.50577333E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.96377729E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     2.63574876E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     1.45835608E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.69947857E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.59178460E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.60169365E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     2.52401876E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     4.93916304E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     7.15626188E-04    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     9.85432912E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.49478605E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     3.54663550E+02   # ~t_1
#    BR                NDA      ID1      ID2
     2.53805768E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.17055259E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.17479752E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.29414955E-03    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.29732739E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     4.56467430E-03    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     1.58383870E-03    2     1000021         4   # BR(~t_1 -> ~g c)
     5.01895425E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     4.18944279E+02   # ~t_2
#    BR                NDA      ID1      ID2
     2.13352113E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     1.12119156E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.14759542E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     3.90718043E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     5.65004183E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     7.65013567E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     5.69384968E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     1.12261589E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.82853878E-02    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     2.60997022E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.98533557E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.46643879E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     1.36054452E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.24035880E-04    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.45642450E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.46023636E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.44085077E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.42520700E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     8.25059217E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     6.91740100E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     6.42835084E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.25733516E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     4.27853924E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     5.72141902E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     2.95721343E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     6.04752158E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     3.95213974E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.50891550E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.21513955E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.21513955E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     5.09916810E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.71049089E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     1.68048621E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.48062812E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.32889869E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     3.07463812E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     6.15431827E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     6.15431827E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.23657399E-02   # ~g
#    BR                NDA      ID1      ID2
     7.03754752E-03    2     1000023        21   # BR(~g -> chi^0_2 g)
     7.83288757E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
#    BR                NDA      ID1      ID2       ID3
     6.65578539E-04    3     1000022         2        -2   # BR(~g -> chi^0_1 u u_bar)
     6.65578569E-04    3     1000022         4        -4   # BR(~g -> chi^0_1 c c_bar)
     2.31928546E-02    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     1.96306375E-04    3     1000022         1        -1   # BR(~g -> chi^0_1 d d_bar)
     1.96324402E-04    3     1000022         3        -3   # BR(~g -> chi^0_1 s s_bar)
     7.47176267E-03    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     1.96466755E-01    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     4.15487561E-02    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.93849971E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     4.12410643E-02    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     5.75488431E-04    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     9.03307484E-04    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     2.37326807E-01    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     2.37326807E-01    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.51207771E-03    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     1.51207771E-03    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.64487616E-03   # Gamma(h0)
     2.47486364E-03   2        22        22   # BR(h0 -> photon photon)
     1.55754826E-03   2        22        23   # BR(h0 -> photon Z)
     2.85002164E-02   2        23        23   # BR(h0 -> Z Z)
     2.36643938E-01   2       -24        24   # BR(h0 -> W W)
     7.67860961E-02   2        21        21   # BR(h0 -> gluon gluon)
     4.94104222E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.19786264E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.33720460E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.40619201E-07   2        -2         2   # BR(h0 -> Up up)
     2.72901034E-02   2        -4         4   # BR(h0 -> Charm charm)
     5.72133335E-07   2        -1         1   # BR(h0 -> Down down)
     2.06927062E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.50059656E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     1.28881004E-02   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     8.84217818E+01   # Gamma(HH)
     1.14468979E-08   2        22        22   # BR(HH -> photon photon)
     2.86127091E-08   2        22        23   # BR(HH -> photon Z)
     2.64229579E-07   2        23        23   # BR(HH -> Z Z)
     5.15230871E-08   2       -24        24   # BR(HH -> W W)
     1.90751463E-06   2        21        21   # BR(HH -> gluon gluon)
     6.14150264E-09   2       -11        11   # BR(HH -> Electron electron)
     2.73480451E-04   2       -13        13   # BR(HH -> Muon muon)
     7.89889200E-02   2       -15        15   # BR(HH -> Tau tau)
     1.26112749E-13   2        -2         2   # BR(HH -> Up up)
     2.44668542E-08   2        -4         4   # BR(HH -> Charm charm)
     1.78897034E-03   2        -6         6   # BR(HH -> Top top)
     4.30424105E-07   2        -1         1   # BR(HH -> Down down)
     1.55685519E-04   2        -3         3   # BR(HH -> Strange strange)
     3.93352753E-01   2        -5         5   # BR(HH -> Bottom bottom)
     2.89436223E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     1.55031789E-01   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     1.55031789E-01   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     9.46993596E-05   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     1.49964629E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     2.56905421E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     2.80456213E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.77933343E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     7.93132446E-06   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     3.36117500E-04   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     6.69708340E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     9.26145602E-04   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     8.70789074E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     4.76402206E-05   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     2.10711758E-06   2        25        25   # BR(HH -> h0 h0)
DECAY        36     8.63129562E+01   # Gamma(A0)
     4.55602816E-08   2        22        22   # BR(A0 -> photon photon)
     6.24330823E-08   2        22        23   # BR(A0 -> photon Z)
     6.78308432E-06   2        21        21   # BR(A0 -> gluon gluon)
     5.97458367E-09   2       -11        11   # BR(A0 -> Electron electron)
     2.66047979E-04   2       -13        13   # BR(A0 -> Muon muon)
     7.68422726E-02   2       -15        15   # BR(A0 -> Tau tau)
     1.16602224E-13   2        -2         2   # BR(A0 -> Up up)
     2.26209720E-08   2        -4         4   # BR(A0 -> Charm charm)
     1.66346080E-03   2        -6         6   # BR(A0 -> Top top)
     4.18719971E-07   2        -1         1   # BR(A0 -> Down down)
     1.51450638E-04   2        -3         3   # BR(A0 -> Strange strange)
     3.82654825E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     3.18394834E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     1.58669542E-01   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     1.58669542E-01   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     2.65143614E-04   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     1.56844532E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     3.02065161E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     2.49121989E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.81367430E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     9.58806074E-06   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     3.93099842E-04   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     8.92835630E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     8.55272733E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     6.84503063E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     1.33146464E-04   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     6.18070673E-07   2        23        25   # BR(A0 -> Z h0)
     2.91234120E-38   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     9.26328072E+01   # Gamma(Hp)
     6.80437475E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     2.90908273E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     8.22854312E-02   2       -15        16   # BR(Hp -> Tau nu_tau)
     4.16232691E-07   2        -1         2   # BR(Hp -> Down up)
     7.05363435E-06   2        -3         2   # BR(Hp -> Strange up)
     4.38006506E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.06958231E-08   2        -1         4   # BR(Hp -> Down charm)
     1.50027161E-04   2        -3         4   # BR(Hp -> Strange charm)
     6.13365970E-04   2        -5         4   # BR(Hp -> Bottom charm)
     1.29589231E-07   2        -1         6   # BR(Hp -> Down top)
     3.04921618E-06   2        -3         6   # BR(Hp -> Strange top)
     4.15284636E-01   2        -5         6   # BR(Hp -> Bottom top)
     5.33834410E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     3.39712859E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     1.22109703E-03   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     1.48266777E-01   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     7.64375915E-05   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     1.46648700E-01   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     1.48366417E-01   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     4.99164378E-10   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     5.76151837E-07   2        24        25   # BR(Hp -> W h0)
     1.48008967E-14   2        24        35   # BR(Hp -> W HH)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.24902104E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    8.19311178E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    8.19236080E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00009167E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.12898116E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    1.22064936E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.24902104E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    8.19311178E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    8.19236080E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99998211E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    1.78876334E-06        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99998211E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    1.78876334E-06        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.27126954E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.81057689E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    9.83799442E-02        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    1.78876334E-06        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99998211E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.27750843E-04   # BR(b -> s gamma)
    2    1.58916071E-06   # BR(b -> s mu+ mu-)
    3    3.52478552E-05   # BR(b -> s nu nu)
    4    2.42487743E-15   # BR(Bd -> e+ e-)
    5    1.03588170E-10   # BR(Bd -> mu+ mu-)
    6    2.16944649E-08   # BR(Bd -> tau+ tau-)
    7    8.13592200E-14   # BR(Bs -> e+ e-)
    8    3.47566890E-09   # BR(Bs -> mu+ mu-)
    9    7.37552372E-07   # BR(Bs -> tau+ tau-)
   10    9.65139782E-05   # BR(B_u -> tau nu)
   11    9.96952445E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42042957E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93661920E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15753270E-03   # epsilon_K
   17    2.28166060E-15   # Delta(M_K)
   18    2.47971565E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28977301E-11   # BR(K^+ -> pi^+ nu nu)
   20    2.82743067E-16   # Delta(g-2)_electron/2
   21    1.20882009E-11   # Delta(g-2)_muon/2
   22    3.42319083E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    6.07056425E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.97085894E-01   # C7
     0305 4322   00   2    -1.03039584E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.03136909E-01   # C8
     0305 6321   00   2    -1.25820234E-04   # C8'
 03051111 4133   00   0     1.63032314E+00   # C9 e+e-
 03051111 4133   00   2     1.63055655E+00   # C9 e+e-
 03051111 4233   00   2     2.12760866E-04   # C9' e+e-
 03051111 4137   00   0    -4.45301524E+00   # C10 e+e-
 03051111 4137   00   2    -4.45092974E+00   # C10 e+e-
 03051111 4237   00   2    -1.56587323E-03   # C10' e+e-
 03051313 4133   00   0     1.63032314E+00   # C9 mu+mu-
 03051313 4133   00   2     1.63055647E+00   # C9 mu+mu-
 03051313 4233   00   2     2.12760810E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.45301524E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.45092983E+00   # C10 mu+mu-
 03051313 4237   00   2    -1.56587329E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50490071E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     3.38323633E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50490071E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     3.38323639E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50490072E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     3.38325188E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85813702E-07   # C7
     0305 4422   00   2     8.41050661E-06   # C7
     0305 4322   00   2    -5.04093345E-07   # C7'
     0305 6421   00   0     3.30474951E-07   # C8
     0305 6421   00   2    -1.17443252E-05   # C8
     0305 6321   00   2    -4.83195745E-07   # C8'
 03051111 4133   00   2     1.16062814E-07   # C9 e+e-
 03051111 4233   00   2     4.02759679E-06   # C9' e+e-
 03051111 4137   00   2     1.31517896E-06   # C10 e+e-
 03051111 4237   00   2    -2.96436374E-05   # C10' e+e-
 03051313 4133   00   2     1.16061336E-07   # C9 mu+mu-
 03051313 4233   00   2     4.02759624E-06   # C9' mu+mu-
 03051313 4137   00   2     1.31518065E-06   # C10 mu+mu-
 03051313 4237   00   2    -2.96436391E-05   # C10' mu+mu-
 03051212 4137   00   2    -2.62882239E-07   # C11 nu_1 nu_1
 03051212 4237   00   2     6.40482529E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -2.62882170E-07   # C11 nu_2 nu_2
 03051414 4237   00   2     6.40482529E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -2.62862837E-07   # C11 nu_3 nu_3
 03051616 4237   00   2     6.40482528E-06   # C11' nu_3 nu_3
