# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  16:34
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    2.08718646E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.64695228E+03  # scale for input parameters
    1    3.32787042E+02  # M_1
    2    8.74401480E+02  # M_2
    3    1.37250587E+03  # M_3
   11   -5.80428391E+02  # A_t
   12    9.20123868E+02  # A_b
   13    7.05047830E+02  # A_tau
   23   -9.29092159E+02  # mu
   25    1.99635754E+01  # tan(beta)
   26    6.89498268E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.83332377E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.71976898E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    3.35548727E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.64695228E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.64695228E+03  # (SUSY scale)
  1  1     8.39488429E-06   # Y_u(Q)^DRbar
  2  2     4.26460122E-03   # Y_c(Q)^DRbar
  3  3     1.01416945E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.64695228E+03  # (SUSY scale)
  1  1     3.36769914E-04   # Y_d(Q)^DRbar
  2  2     6.39862837E-03   # Y_s(Q)^DRbar
  3  3     3.33970226E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.64695228E+03  # (SUSY scale)
  1  1     5.87688127E-05   # Y_e(Q)^DRbar
  2  2     1.21515266E-02   # Y_mu(Q)^DRbar
  3  3     2.04368687E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.64695228E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -5.80428396E+02   # A_t(Q)^DRbar
Block Ad Q=  3.64695228E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     9.20123868E+02   # A_b(Q)^DRbar
Block Ae Q=  3.64695228E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     7.05047827E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  3.64695228E+03  # soft SUSY breaking masses at Q
   1    3.32787042E+02  # M_1
   2    8.74401480E+02  # M_2
   3    1.37250587E+03  # M_3
  21   -4.50221295E+05  # M^2_(H,d)
  22   -6.22260671E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.83332377E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.71976898E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    3.35548727E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.21256447E+02  # h0
        35     6.89605561E+02  # H0
        36     6.89498268E+02  # A0
        37     6.98030765E+02  # H+
   1000001     1.01212817E+04  # ~d_L
   2000001     1.00964020E+04  # ~d_R
   1000002     1.01209229E+04  # ~u_L
   2000002     1.00999451E+04  # ~u_R
   1000003     1.01212832E+04  # ~s_L
   2000003     1.00964038E+04  # ~s_R
   1000004     1.01209243E+04  # ~c_L
   2000004     1.00999460E+04  # ~c_R
   1000005     3.39892563E+03  # ~b_1
   2000005     4.87191385E+03  # ~b_2
   1000006     2.72905190E+03  # ~t_1
   2000006     4.87358300E+03  # ~t_2
   1000011     1.00211699E+04  # ~e_L-
   2000011     1.00086034E+04  # ~e_R-
   1000012     1.00204071E+04  # ~nu_eL
   1000013     1.00211756E+04  # ~mu_L-
   2000013     1.00086123E+04  # ~mu_R-
   1000014     1.00204120E+04  # ~nu_muL
   1000015     1.00110810E+04  # ~tau_1-
   2000015     1.00228201E+04  # ~tau_2-
   1000016     1.00218038E+04  # ~nu_tauL
   1000021     1.72153006E+03  # ~g
   1000022     3.34029801E+02  # ~chi_10
   1000023     8.89201257E+02  # ~chi_20
   1000025     9.42349902E+02  # ~chi_30
   1000035     9.95352672E+02  # ~chi_40
   1000024     8.89211916E+02  # ~chi_1+
   1000037     9.95455863E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -5.00959219E-02   # alpha
Block Hmix Q=  3.64695228E+03  # Higgs mixing parameters
   1   -9.29092159E+02  # mu
   2    1.99635754E+01  # tan[beta](Q)
   3    2.43037758E+02  # v(Q)
   4    4.75407862E+05  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     5.95592268E-03   # Re[R_st(1,1)]
   1  2     9.99982263E-01   # Re[R_st(1,2)]
   2  1    -9.99982263E-01   # Re[R_st(2,1)]
   2  2     5.95592268E-03   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -3.63403473E-03   # Re[R_sb(1,1)]
   1  2     9.99993397E-01   # Re[R_sb(1,2)]
   2  1    -9.99993397E-01   # Re[R_sb(2,1)]
   2  2    -3.63403473E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -1.47839507E-01   # Re[R_sta(1,1)]
   1  2     9.89011365E-01   # Re[R_sta(1,2)]
   2  1    -9.89011365E-01   # Re[R_sta(2,1)]
   2  2    -1.47839507E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.98388278E-01   # Re[N(1,1)]
   1  2     1.81495596E-03   # Re[N(1,2)]
   1  3     5.41097094E-02   # Re[N(1,3)]
   1  4     1.70203331E-02   # Re[N(1,4)]
   2  1     3.65042153E-02   # Re[N(2,1)]
   2  2     7.25795057E-01   # Re[N(2,2)]
   2  3     5.01550601E-01   # Re[N(2,3)]
   2  4     4.69399588E-01   # Re[N(2,4)]
   3  1    -2.61247171E-02   # Re[N(3,1)]
   3  2     3.13950147E-02   # Re[N(3,2)]
   3  3    -7.05607241E-01   # Re[N(3,3)]
   3  4     7.07425102E-01   # Re[N(3,4)]
   4  1    -3.47244565E-02   # Re[N(4,1)]
   4  2     6.87191818E-01   # Re[N(4,2)]
   4  3    -4.97631949E-01   # Re[N(4,3)]
   4  4    -5.28132617E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -6.94518247E-01   # Re[U(1,1)]
   1  2    -7.19475090E-01   # Re[U(1,2)]
   2  1     7.19475090E-01   # Re[U(2,1)]
   2  2    -6.94518247E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -7.38205935E-01   # Re[V(1,1)]
   1  2     6.74575420E-01   # Re[V(1,2)]
   2  1     6.74575420E-01   # Re[V(2,1)]
   2  2     7.38205935E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.01751182E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.96828642E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.31459985E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     6.71989899E-04    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     1.18475455E-03    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.42549899E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.72305558E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     1.69179154E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     1.35621796E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     2.93823139E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     3.14052911E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.02329273E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.95684072E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.45800156E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     9.57474167E-04    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.32547612E-03    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     2.98212215E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
     2.76763887E-04    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
DECAY   1000013     1.42578903E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.72134639E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     1.69195972E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.93409285E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     1.35644402E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     2.93763542E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     3.13989211E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     6.83940277E+01   # ~tau^-_1
#    BR                NDA      ID1      ID2
     7.22175501E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     6.72986360E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     5.73998024E-02    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     8.36510831E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.30923145E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.38378072E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.48890829E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     8.81325000E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.59196516E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     2.86333008E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.53576924E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.49644694E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     3.20816065E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.42553785E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.84168455E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.51919536E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     6.25136261E-04    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     1.51073826E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     3.31915699E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     2.76048957E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.42582774E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.83989124E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.51888725E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     6.25009478E-04    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     1.51043187E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     3.31953593E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     2.76090574E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.50755150E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.36185073E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.43675485E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     5.91213002E-04    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     1.42875830E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     3.42054785E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     2.87184180E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.56900796E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.40691518E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     9.92569455E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.86089620E+02   # ~d_L
#    BR                NDA      ID1      ID2
     1.61820822E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     2.56120657E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     2.37059121E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     4.77560090E-02    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     5.10480964E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.50195495E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.56917065E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.40678900E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     9.92548319E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.86101442E+02   # ~s_L
#    BR                NDA      ID1      ID2
     1.61821400E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     2.56140278E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     2.37078507E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     4.77572144E-02    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     5.10496486E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.50184293E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.62969710E+02   # ~b_1
#    BR                NDA      ID1      ID2
     1.15300739E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.02616619E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     1.96285108E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     9.45163614E-03    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     2.09437771E-02    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     1.82526956E-02    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     9.09931641E-01    2     1000021         5   # BR(~b_1 -> ~g b)
DECAY   2000005     4.60633715E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.54886642E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     2.79522495E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.08892512E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.59577417E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     1.31810442E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.54385311E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     6.39379710E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     1.95345087E-03    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     5.39149281E-03    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
     1.51607605E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     1.70102721E-04    2     1000005        36   # BR(~b_2 -> ~b_1 A^0)
     2.39691558E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
     1.70082975E-04    2     1000005        35   # BR(~b_2 -> ~b_1 H^0)
DECAY   2000002     7.74076320E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.89804806E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     9.70927294E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.86078696E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.55342204E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     2.65456321E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     2.28672517E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     5.39517276E-02    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     4.48743867E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.50172245E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.74083546E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.89802128E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     9.70918288E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.86090478E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.55340276E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     2.65461709E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     2.28680894E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     5.39557506E-02    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     4.48781815E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.50161038E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.68291035E+02   # ~t_1
#    BR                NDA      ID1      ID2
     3.47851227E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     5.84499244E-02    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     1.27315384E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     6.84628121E-02    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     1.21665553E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.34934349E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     4.54386202E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     4.59697290E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.48246462E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     6.66395631E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.00286062E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     7.66561426E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     5.81366889E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     5.01809860E-02    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     6.38690141E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     3.05137427E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     3.38971521E-04    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     9.79985319E-04    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     2.70711736E-03    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     8.89853223E-04    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     2.70688742E-03    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     2.76829446E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.90312676E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     9.60411385E-03    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     6.36988315E-01   # chi^+_2
#    BR                NDA      ID1      ID2
     5.08479525E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     3.35499267E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     1.46685988E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
#    BR                NDA      ID1      ID2       ID3
     6.94874731E-03    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     1.17963093E-04    3     1000022       -15        16   # BR(chi^+_2 -> chi^0_1 tau^+ nu_tau)
     7.56605686E-04    3     1000025        -1         2   # BR(chi^+_2 -> chi^0_3 d_bar u)
     7.54973105E-04    3     1000025        -3         4   # BR(chi^+_2 -> chi^0_3 s_bar c)
     2.52201448E-04    3     1000025       -11        12   # BR(chi^+_2 -> chi^0_3 e^+ nu_e)
     2.52198686E-04    3     1000025       -13        14   # BR(chi^+_2 -> chi^0_3 mu^+ nu_mu)
     2.51148104E-04    3     1000025       -15        16   # BR(chi^+_2 -> chi^0_3 tau^+ nu_tau)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.45382069E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     2.33488774E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     7.63750653E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.83815865E-03    3     1000022         6        -6   # BR(chi^0_2 -> chi^0_1 t t_bar)
     8.18696935E-04    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     1.00643001E-04    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
DECAY   1000025     1.00713778E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     4.88582985E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     5.04979648E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.09615912E-03    3     1000022         6        -6   # BR(chi^0_3 -> chi^0_1 t t_bar)
     5.65088061E-04    3     1000022         5        -5   # BR(chi^0_3 -> chi^0_1 b b_bar)
     1.10502180E-04    3     1000023         2        -2   # BR(chi^0_3 -> chi^0_2 u u_bar)
     1.09785502E-04    3     1000023         4        -4   # BR(chi^0_3 -> chi^0_2 c c_bar)
     1.41684805E-04    3     1000023         1        -1   # BR(chi^0_3 -> chi^0_2 d d_bar)
     1.41680447E-04    3     1000023         3        -3   # BR(chi^0_3 -> chi^0_2 s s_bar)
     1.33535391E-04    3     1000023         5        -5   # BR(chi^0_3 -> chi^0_2 b b_bar)
     1.89475041E-04    3     1000023        12       -12   # BR(chi^0_3 -> chi^0_2 nu_e nu_bar_e)
     4.52595381E-04    3     1000024         1        -2   # BR(chi^0_3 -> chi^+_1 d u_bar)
     4.52595381E-04    3    -1000024        -1         2   # BR(chi^0_3 -> chi^-_1 d_bar u)
     4.51631544E-04    3     1000024         3        -4   # BR(chi^0_3 -> chi^+_1 s c_bar)
     4.51631544E-04    3    -1000024        -3         4   # BR(chi^0_3 -> chi^-_1 s_bar c)
     1.50865410E-04    3     1000024        11       -12   # BR(chi^0_3 -> chi^+_1 e^- nu_bar_e)
     1.50865410E-04    3    -1000024       -11        12   # BR(chi^0_3 -> chi^-_1 e^+ nu_e)
     1.50863845E-04    3     1000024        13       -14   # BR(chi^0_3 -> chi^+_1 mu^- nu_bar_mu)
     1.50863845E-04    3    -1000024       -13        14   # BR(chi^0_3 -> chi^-_1 mu^+ nu_mu)
     1.50260133E-04    3     1000024        15       -16   # BR(chi^0_3 -> chi^+_1 tau^- nu_bar_tau)
     1.50260133E-04    3    -1000024       -15        16   # BR(chi^0_3 -> chi^-_1 tau^+ nu_tau)
DECAY   1000035     6.66898893E-01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.70830704E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.70830704E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     1.15461918E-01    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     3.38246941E-01    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.67918918E-03    3     1000022         6        -6   # BR(chi^0_4 -> chi^0_1 t t_bar)
     1.20064454E-03    3     1000022         5        -5   # BR(chi^0_4 -> chi^0_1 b b_bar)
     1.48626718E-04    3     1000022        15       -15   # BR(chi^0_4 -> chi^0_1 tau^- tau^+)
     1.84171174E-04    3     1000025         2        -2   # BR(chi^0_4 -> chi^0_3 u u_bar)
     1.82964927E-04    3     1000025         4        -4   # BR(chi^0_4 -> chi^0_3 c c_bar)
     2.36146089E-04    3     1000025         1        -1   # BR(chi^0_4 -> chi^0_3 d d_bar)
     2.36138760E-04    3     1000025         3        -3   # BR(chi^0_4 -> chi^0_3 s s_bar)
     2.22442932E-04    3     1000025         5        -5   # BR(chi^0_4 -> chi^0_3 b b_bar)
     3.15792314E-04    3     1000025        12       -12   # BR(chi^0_4 -> chi^0_3 nu_e nu_bar_e)
DECAY   1000021     2.41223163E-03   # ~g
#    BR                NDA      ID1      ID2
     1.75011386E-04    2     1000022        21   # BR(~g -> chi^0_1 g)
     1.04395416E-02    2     1000023        21   # BR(~g -> chi^0_2 g)
     1.93330970E-02    2     1000025        21   # BR(~g -> chi^0_3 g)
     9.92136827E-03    2     1000035        21   # BR(~g -> chi^0_4 g)
#    BR                NDA      ID1      ID2       ID3
     1.11146419E-03    3     1000022         2        -2   # BR(~g -> chi^0_1 u u_bar)
     1.11146389E-03    3     1000022         4        -4   # BR(~g -> chi^0_1 c c_bar)
     2.22572296E-01    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     3.28598115E-04    3     1000022         1        -1   # BR(~g -> chi^0_1 d d_bar)
     3.28600490E-04    3     1000022         3        -3   # BR(~g -> chi^0_1 s s_bar)
     2.70095910E-02    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     2.35617213E-04    3     1000023         2        -2   # BR(~g -> chi^0_2 u u_bar)
     2.35607242E-04    3     1000023         4        -4   # BR(~g -> chi^0_2 c c_bar)
     7.70894960E-02    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     2.27078980E-04    3     1000023         1        -1   # BR(~g -> chi^0_2 d d_bar)
     2.27121061E-04    3     1000023         3        -3   # BR(~g -> chi^0_2 s s_bar)
     1.17231533E-02    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.50727028E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     1.02865970E-02    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     1.19028062E-04    3     1000035         2        -2   # BR(~g -> chi^0_4 u u_bar)
     1.19051778E-04    3     1000035         4        -4   # BR(~g -> chi^0_4 c c_bar)
     7.57764185E-02    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     1.23248307E-04    3     1000035         1        -1   # BR(~g -> chi^0_4 d d_bar)
     1.23272283E-04    3     1000035         3        -3   # BR(~g -> chi^0_4 s s_bar)
     6.32741229E-03    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     4.50482431E-04    3     1000024         1        -2   # BR(~g -> chi^+_1 d u_bar)
     4.50482431E-04    3    -1000024        -1         2   # BR(~g -> chi^-_1 d_bar u)
     4.50515983E-04    3     1000024         3        -4   # BR(~g -> chi^+_1 s c_bar)
     4.50515983E-04    3    -1000024        -3         4   # BR(~g -> chi^-_1 s_bar c)
     9.45947130E-02    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     9.45947130E-02    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     2.48986003E-04    3     1000037         1        -2   # BR(~g -> chi^+_2 d u_bar)
     2.48986003E-04    3    -1000037        -1         2   # BR(~g -> chi^-_2 d_bar u)
     2.49032005E-04    3     1000037         3        -4   # BR(~g -> chi^+_2 s c_bar)
     2.49032005E-04    3    -1000037        -3         4   # BR(~g -> chi^-_2 s_bar c)
     9.11698110E-02    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     9.11698110E-02    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     3.44067366E-03   # Gamma(h0)
     2.32961020E-03   2        22        22   # BR(h0 -> photon photon)
     1.18678844E-03   2        22        23   # BR(h0 -> photon Z)
     1.92057540E-02   2        23        23   # BR(h0 -> Z Z)
     1.71092562E-01   2       -24        24   # BR(h0 -> W W)
     7.46090641E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.61208365E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.49633222E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.19724465E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.46542962E-07   2        -2         2   # BR(h0 -> Up up)
     2.84374765E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.53468315E-07   2        -1         1   # BR(h0 -> Down down)
     2.36344581E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.30679515E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     4.62794994E+00   # Gamma(HH)
     6.10297550E-08   2        22        22   # BR(HH -> photon photon)
     8.83724883E-09   2        22        23   # BR(HH -> photon Z)
     5.98736253E-05   2        23        23   # BR(HH -> Z Z)
     1.11623048E-04   2       -24        24   # BR(HH -> W W)
     9.71070578E-05   2        21        21   # BR(HH -> gluon gluon)
     9.80480618E-09   2       -11        11   # BR(HH -> Electron electron)
     4.36359603E-04   2       -13        13   # BR(HH -> Muon muon)
     1.25993270E-01   2       -15        15   # BR(HH -> Tau tau)
     1.09295633E-12   2        -2         2   # BR(HH -> Up up)
     2.11928277E-07   2        -4         4   # BR(HH -> Charm charm)
     1.15915279E-02   2        -6         6   # BR(HH -> Top top)
     8.79401389E-07   2        -1         1   # BR(HH -> Down down)
     3.18071980E-04   2        -3         3   # BR(HH -> Strange strange)
     8.60772930E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.90417277E-05   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     5.99023431E-04   2        25        25   # BR(HH -> h0 h0)
DECAY        36     4.57224345E+00   # Gamma(A0)
     2.94756591E-07   2        22        22   # BR(A0 -> photon photon)
     1.66792646E-07   2        22        23   # BR(A0 -> photon Z)
     1.94698243E-04   2        21        21   # BR(A0 -> gluon gluon)
     9.77911791E-09   2       -11        11   # BR(A0 -> Electron electron)
     4.35216364E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.25666621E-01   2       -15        15   # BR(A0 -> Tau tau)
     1.00500993E-12   2        -2         2   # BR(A0 -> Up up)
     1.94707462E-07   2        -4         4   # BR(A0 -> Charm charm)
     1.44427950E-02   2        -6         6   # BR(A0 -> Top top)
     8.77115564E-07   2        -1         1   # BR(A0 -> Down down)
     3.17245206E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.58590522E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     2.51706600E-04   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     9.96523878E-05   2        23        25   # BR(A0 -> Z h0)
     4.23877765E-37   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     4.27025722E+00   # Gamma(Hp)
     1.17761598E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     5.03467591E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.42407524E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     9.12277392E-07   2        -1         2   # BR(Hp -> Down up)
     1.51087844E-05   2        -3         2   # BR(Hp -> Strange up)
     9.84737586E-06   2        -5         2   # BR(Hp -> Bottom up)
     5.22705928E-08   2        -1         4   # BR(Hp -> Down charm)
     3.28992724E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.37897662E-03   2        -5         4   # BR(Hp -> Bottom charm)
     1.01544866E-06   2        -1         6   # BR(Hp -> Down top)
     2.25631451E-05   2        -3         6   # BR(Hp -> Strange top)
     8.55218934E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.12565739E-04   2        24        25   # BR(Hp -> W h0)
     1.33804947E-08   2        24        35   # BR(Hp -> W HH)
     1.42535274E-08   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    1.00185853E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    3.98542484E+02    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    3.98544343E+02        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.99995337E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    2.51379438E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    2.50913109E-03        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    1.00185853E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    3.98542484E+02    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    3.98544343E+02        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999998E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.16471019E-09        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999998E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.16471019E-09        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26500603E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    9.84769301E-03        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.09493057E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.16471019E-09        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999998E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.81759803E-04   # BR(b -> s gamma)
    2    1.58665843E-06   # BR(b -> s mu+ mu-)
    3    3.52374169E-05   # BR(b -> s nu nu)
    4    2.46252509E-15   # BR(Bd -> e+ e-)
    5    1.05196305E-10   # BR(Bd -> mu+ mu-)
    6    2.20234669E-08   # BR(Bd -> tau+ tau-)
    7    8.30977940E-14   # BR(Bs -> e+ e-)
    8    3.54993601E-09   # BR(Bs -> mu+ mu-)
    9    7.53026064E-07   # BR(Bs -> tau+ tau-)
   10    9.14315746E-05   # BR(B_u -> tau nu)
   11    9.44453161E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.42630299E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93651863E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.16023981E-03   # epsilon_K
   17    2.28169426E-15   # Delta(M_K)
   18    2.47874430E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.28755500E-11   # BR(K^+ -> pi^+ nu nu)
   20   -2.26133657E-16   # Delta(g-2)_electron/2
   21   -9.66793759E-12   # Delta(g-2)_muon/2
   22   -2.73608793E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    5.71549105E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -2.49892677E-01   # C7
     0305 4322   00   2    -1.17844149E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.61612951E-01   # C8
     0305 6321   00   2    -1.29318006E-03   # C8'
 03051111 4133   00   0     1.62316722E+00   # C9 e+e-
 03051111 4133   00   2     1.62352090E+00   # C9 e+e-
 03051111 4233   00   2    -7.19593943E-06   # C9' e+e-
 03051111 4137   00   0    -4.44585932E+00   # C10 e+e-
 03051111 4137   00   2    -4.44268337E+00   # C10 e+e-
 03051111 4237   00   2     5.51181482E-05   # C10' e+e-
 03051313 4133   00   0     1.62316722E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62352084E+00   # C9 mu+mu-
 03051313 4233   00   2    -7.19940798E-06   # C9' mu+mu-
 03051313 4137   00   0    -4.44585932E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44268343E+00   # C10 mu+mu-
 03051313 4237   00   2     5.51215930E-05   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50466390E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -1.19290122E-05   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50466390E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -1.19282626E-05   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50466390E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -1.17170223E-05   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85819883E-07   # C7
     0305 4422   00   2    -4.57215672E-06   # C7
     0305 4322   00   2    -3.76815089E-07   # C7'
     0305 6421   00   0     3.30480246E-07   # C8
     0305 6421   00   2     1.00237077E-05   # C8
     0305 6321   00   2     8.07240165E-08   # C8'
 03051111 4133   00   2     3.63838542E-07   # C9 e+e-
 03051111 4233   00   2     1.54280099E-06   # C9' e+e-
 03051111 4137   00   2    -3.31221626E-07   # C10 e+e-
 03051111 4237   00   2    -1.14542982E-05   # C10' e+e-
 03051313 4133   00   2     3.63837908E-07   # C9 mu+mu-
 03051313 4233   00   2     1.54280088E-06   # C9' mu+mu-
 03051313 4137   00   2    -3.31221012E-07   # C10 mu+mu-
 03051313 4237   00   2    -1.14542985E-05   # C10' mu+mu-
 03051212 4137   00   2     9.09939937E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     2.47879550E-06   # C11' nu_1 nu_1
 03051414 4137   00   2     9.09940233E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     2.47879550E-06   # C11' nu_2 nu_2
 03051616 4137   00   2     9.10023638E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     2.47879549E-06   # C11' nu_3 nu_3
