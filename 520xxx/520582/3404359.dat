# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  17:18
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    5.25679139E+01  # tanb at m_Z    
    4   -1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    4.45690502E+03  # scale for input parameters
    1    4.48267217E+01  # M_1
    2   -1.41488951E+03  # M_2
    3    4.57364071E+03  # M_3
   11    1.77114083E+03  # A_t
   12   -1.61679844E+03  # A_b
   13   -9.77857482E+02  # A_tau
   23   -3.02328307E+02  # mu
   25    5.22314622E+01  # tan(beta)
   26    3.23374680E+03  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    4.75326679E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    3.97755228E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.41761963E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  4.45690502E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  4.45690502E+03  # (SUSY scale)
  1  1     8.38590865E-06   # Y_u(Q)^DRbar
  2  2     4.26004159E-03   # Y_c(Q)^DRbar
  3  3     1.01308512E+00   # Y_t(Q)^DRbar
Block Yd Q=  4.45690502E+03  # (SUSY scale)
  1  1     8.80161887E-04   # Y_d(Q)^DRbar
  2  2     1.67230758E-02   # Y_s(Q)^DRbar
  3  3     8.72844789E-01   # Y_b(Q)^DRbar
Block Ye Q=  4.45690502E+03  # (SUSY scale)
  1  1     1.53594685E-04   # Y_e(Q)^DRbar
  2  2     3.17585096E-02   # Y_mu(Q)^DRbar
  3  3     5.34125886E-01   # Y_tau(Q)^DRbar
Block Au Q=  4.45690502E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3     1.77114085E+03   # A_t(Q)^DRbar
Block Ad Q=  4.45690502E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.61679849E+03   # A_b(Q)^DRbar
Block Ae Q=  4.45690502E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -9.77857440E+02   # A_tau(Q)^DRbar
Block MSOFT Q=  4.45690502E+03  # soft SUSY breaking masses at Q
   1    4.48267217E+01  # M_1
   2   -1.41488951E+03  # M_2
   3    4.57364071E+03  # M_3
  21    1.06977377E+07  # M^2_(H,d)
  22    3.15333051E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    4.75326679E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    3.97755228E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.41761963E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.22996260E+02  # h0
        35     3.23158325E+03  # H0
        36     3.23374680E+03  # A0
        37     3.22698782E+03  # H+
   1000001     1.01061253E+04  # ~d_L
   2000001     1.00821278E+04  # ~d_R
   1000002     1.01057618E+04  # ~u_L
   2000002     1.00845475E+04  # ~u_R
   1000003     1.01061265E+04  # ~s_L
   2000003     1.00821297E+04  # ~s_R
   1000004     1.01057630E+04  # ~c_L
   2000004     1.00845477E+04  # ~c_R
   1000005     2.56067958E+03  # ~b_1
   2000005     4.85847134E+03  # ~b_2
   1000006     4.08680825E+03  # ~t_1
   2000006     4.86051734E+03  # ~t_2
   1000011     1.00205216E+04  # ~e_L-
   2000011     1.00096057E+04  # ~e_R-
   1000012     1.00197549E+04  # ~nu_eL
   1000013     1.00205273E+04  # ~mu_L-
   2000013     1.00096149E+04  # ~mu_R-
   1000014     1.00197600E+04  # ~nu_muL
   1000015     1.00123130E+04  # ~tau_1-
   2000015     1.00221825E+04  # ~tau_2-
   1000016     1.00212376E+04  # ~nu_tauL
   1000021     5.07215508E+03  # ~g
   1000022     4.44311133E+01  # ~chi_10
   1000023     3.19792966E+02  # ~chi_20
   1000025     3.24832407E+02  # ~chi_30
   1000035     1.51636536E+03  # ~chi_40
   1000024     3.18773642E+02  # ~chi_1+
   1000037     1.51633174E+03  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -1.90686784E-02   # alpha
Block Hmix Q=  4.45690502E+03  # Higgs mixing parameters
   1   -3.02328307E+02  # mu
   2    5.22314622E+01  # tan[beta](Q)
   3    2.42848627E+02  # v(Q)
   4    1.04571184E+07  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1    -3.10832318E-02   # Re[R_st(1,1)]
   1  2     9.99516800E-01   # Re[R_st(1,2)]
   2  1    -9.99516800E-01   # Re[R_st(2,1)]
   2  2    -3.10832318E-02   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1    -1.87925927E-03   # Re[R_sb(1,1)]
   1  2     9.99998234E-01   # Re[R_sb(1,2)]
   2  1    -9.99998234E-01   # Re[R_sb(2,1)]
   2  2    -1.87925927E-03   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1    -1.34123675E-01   # Re[R_sta(1,1)]
   1  2     9.90964601E-01   # Re[R_sta(1,2)]
   2  1    -9.90964601E-01   # Re[R_sta(2,1)]
   2  2    -1.34123675E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1     9.89955253E-01   # Re[N(1,1)]
   1  2     7.85980082E-04   # Re[N(1,2)]
   1  3    -1.40371161E-01   # Re[N(1,3)]
   1  4    -1.68498454E-02   # Re[N(1,4)]
   2  1     8.77355366E-02   # Re[N(2,1)]
   2  2     4.80368306E-02   # Re[N(2,2)]
   2  3     7.03479691E-01   # Re[N(2,3)]
   2  4    -7.03641431E-01   # Re[N(2,4)]
   3  1     1.10853223E-01   # Re[N(3,1)]
   3  2    -3.04145138E-02   # Re[N(3,2)]
   3  3     6.96602908E-01   # Re[N(3,3)]
   3  4     7.08188470E-01   # Re[N(3,4)]
   4  1     1.62370234E-03   # Re[N(4,1)]
   4  2    -9.98382092E-01   # Re[N(4,2)]
   4  3     1.25160168E-02   # Re[N(4,3)]
   4  4    -5.54428573E-02   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -1.77037825E-02   # Re[U(1,1)]
   1  2    -9.99843276E-01   # Re[U(1,2)]
   2  1    -9.99843276E-01   # Re[U(2,1)]
   2  2     1.77037825E-02   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1     7.84416933E-02   # Re[V(1,1)]
   1  2     9.96918703E-01   # Re[V(1,2)]
   2  1     9.96918703E-01   # Re[V(2,1)]
   2  2    -7.84416933E-02   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02905824E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.80051314E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     7.68248017E-03    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     1.22635957E-02    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
DECAY   1000011     1.38923404E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     8.90560449E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.85385364E-03    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     2.62896486E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     3.01602601E-01    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     1.98609827E-04    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     6.06025994E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.06914955E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.72379165E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     9.57848672E-03    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     1.40851798E-02    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     3.95289350E-03    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
DECAY   1000013     1.39124143E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     8.89560735E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     3.56348974E-03    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     9.62336721E-04    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     3.01167827E-01    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     1.98323371E-04    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     6.05151950E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.64321129E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     3.08797132E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     1.73214211E-01    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.66107710E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     3.66879252E-03    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     3.40836571E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     7.37558388E-03    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.95118981E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     6.75123770E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     1.45954183E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     1.45661151E-01    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     2.11772421E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     3.66607873E-03    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     4.25433789E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.38927377E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     8.85188909E-02    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.54513234E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     3.02618597E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     3.89870613E-03    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     6.02418242E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.39128091E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     8.83912334E-02    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.54146189E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     3.02182191E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     5.33475411E-03    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     6.01549929E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.95711127E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     6.28452630E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.80695451E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     2.14851415E-01    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     2.92707012E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     4.27789050E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     4.49590184E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.22690762E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.53696881E-04    2     1000025         1   # BR(~d_R -> chi^0_3 d)
     9.87480502E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     5.75373013E+02   # ~d_L
#    BR                NDA      ID1      ID2
     2.38106453E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     1.94202260E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     7.36724614E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     1.47691277E-01    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.75931434E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     4.49702034E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.22684779E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.57758901E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     2.13894060E-04    2     1000025         3   # BR(~s_R -> chi^0_3 s)
     1.24447161E-04    2    -1000024         4   # BR(~s_R -> chi^-_1 c)
     9.87235334E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     5.75432753E+02   # ~s_L
#    BR                NDA      ID1      ID2
     2.38273758E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     1.29313855E-04    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     2.41368659E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     7.36648365E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     1.47676001E-01    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.75851093E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     7.62940112E+01   # ~b_1
#    BR                NDA      ID1      ID2
     2.84478477E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     2.44094426E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     2.39126131E-01    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     4.88247773E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
DECAY   2000005     2.26020488E+02   # ~b_2
#    BR                NDA      ID1      ID2
     9.28344009E-03    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.59939480E-01    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     1.56962742E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     7.69197846E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     4.31351480E-01    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     1.56742767E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     8.20479199E-03    2     1000006       -24   # BR(~b_2 -> ~t_1 W^-)
     2.29050717E-04    2     1000005        23   # BR(~b_2 -> ~b_1 Z)
     3.66464087E-04    2     1000005        25   # BR(~b_2 -> ~b_1 h^0)
DECAY   2000002     4.66731243E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.72852606E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     3.70671092E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     5.91706736E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     9.51752240E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     5.75354602E+02   # ~u_L
#    BR                NDA      ID1      ID2
     2.42389534E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     3.12681754E-04    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     7.35866304E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     9.49771992E-04    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.46827468E-01    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.75890762E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     4.66738499E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.72845269E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     3.74519150E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     5.95602620E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     9.51737424E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     5.75414308E+02   # ~c_L
#    BR                NDA      ID1      ID2
     2.42364584E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     3.15783009E-04    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     7.35790171E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.04689040E-03    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.46812282E-01    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.75810418E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.73593587E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.12868735E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     2.34615685E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.38949952E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     6.00089421E-04    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     4.73340460E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.20492294E-03    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     2.26815061E+02   # ~t_2
#    BR                NDA      ID1      ID2
     3.22887757E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.14605934E-01    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.16102667E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     7.74579858E-02    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     3.21778908E-01    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.53844837E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     4.57341926E-04    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     4.09527566E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     8.42817149E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     2.02494029E-01   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99588514E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     4.11469947E-04    3     1000022        -5         6   # BR(chi^+_1 -> chi^0_1 b_bar t)
DECAY   1000037     1.36279413E+01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.12672237E-03    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     2.45949206E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     2.43202873E-01    2     1000025        24   # BR(chi^+_2 -> chi^0_3 W^+)
     2.43741503E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     2.42871463E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     8.80562192E-03    3     1000024         6        -6   # BR(chi^+_2 -> chi^+_1 t t_bar)
     7.03563930E-03    3     1000023        -5         6   # BR(chi^+_2 -> chi^0_2 b_bar t)
     7.18999417E-03    3     1000025        -5         6   # BR(chi^+_2 -> chi^0_3 b_bar t)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     2.29777936E-01   # chi^0_2
#    BR                NDA      ID1      ID2
     5.83329319E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
     4.16639381E-01    2     1000022        25   # BR(chi^0_2 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000025     1.75948858E-01   # chi^0_3
#    BR                NDA      ID1      ID2
     4.81139516E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     5.18854655E-01    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000035     1.49645377E+01   # chi^0_4
#    BR                NDA      ID1      ID2
     2.24206455E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     2.24206455E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     5.72903679E-04    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     6.34118293E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.59306325E-01    2     1000025        23   # BR(chi^0_4 -> chi^0_3 Z)
     4.42161315E-04    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     1.58398330E-01    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.47244545E-01    2     1000025        25   # BR(chi^0_4 -> chi^0_3 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.77073876E-03    3     1000023         6        -6   # BR(chi^0_4 -> chi^0_2 t t_bar)
     3.68398112E-03    3     1000025         6        -6   # BR(chi^0_4 -> chi^0_3 t t_bar)
     7.32914327E-03    3     1000024         5        -6   # BR(chi^0_4 -> chi^+_1 b t_bar)
     7.32914327E-03    3    -1000024        -5         6   # BR(chi^0_4 -> chi^-_1 b_bar t)
DECAY   1000021     1.03422223E+02   # ~g
#    BR                NDA      ID1      ID2
     8.92677584E-02    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     8.92677584E-02    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     2.67962453E-03    2     2000006        -6   # BR(~g -> ~t_2 t_bar)
     2.67962453E-03    2    -2000006         6   # BR(~g -> ~t^*_2 t)
     4.02998304E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     4.02998304E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     4.93689999E-03    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     4.93689999E-03    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
DECAY        25     3.43307303E-03   # Gamma(h0)
     2.47761668E-03   2        22        22   # BR(h0 -> photon photon)
     1.40777048E-03   2        22        23   # BR(h0 -> photon Z)
     2.42136689E-02   2        23        23   # BR(h0 -> Z Z)
     2.08242809E-01   2       -24        24   # BR(h0 -> W W)
     7.82447070E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.21664821E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.32044712E-04   2       -13        13   # BR(h0 -> Muon muon)
     6.69041293E-02   2       -15        15   # BR(h0 -> Tau tau)
     1.48480567E-07   2        -2         2   # BR(h0 -> Up up)
     2.88145498E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.05611005E-07   2        -1         1   # BR(h0 -> Down down)
     2.19035111E-04   2        -3         3   # BR(h0 -> Strange strange)
     5.82574837E-01   2        -5         5   # BR(h0 -> Bottom bottom)
     6.66807297E-03   2   1000022   1000022   # BR(h0 -> neutralino1 neutralino1)
DECAY        35     1.44423134E+02   # Gamma(HH)
     3.70348017E-09   2        22        22   # BR(HH -> photon photon)
     1.39033419E-08   2        22        23   # BR(HH -> photon Z)
     1.01798027E-07   2        23        23   # BR(HH -> Z Z)
     2.79083931E-08   2       -24        24   # BR(HH -> W W)
     8.24952477E-06   2        21        21   # BR(HH -> gluon gluon)
     8.76709507E-09   2       -11        11   # BR(HH -> Electron electron)
     3.90362939E-04   2       -13        13   # BR(HH -> Muon muon)
     1.12742868E-01   2       -15        15   # BR(HH -> Tau tau)
     2.09077906E-14   2        -2         2   # BR(HH -> Up up)
     4.05591707E-09   2        -4         4   # BR(HH -> Charm charm)
     3.16017480E-04   2        -6         6   # BR(HH -> Top top)
     6.52089584E-07   2        -1         1   # BR(HH -> Down down)
     2.35871246E-04   2        -3         3   # BR(HH -> Strange strange)
     6.85237151E-01   2        -5         5   # BR(HH -> Bottom bottom)
     1.21921427E-03   2  -1000024   1000024   # BR(HH -> Chargino1 chargino1)
     5.78686126E-02   2  -1000024   1000037   # BR(HH -> Chargino1 chargino2)
     5.78686126E-02   2  -1000037   1000024   # BR(HH -> Chargino2 chargino1)
     6.29357680E-06   2  -1000037   1000037   # BR(HH -> Chargino2 chargino2)
     1.05659611E-03   2   1000022   1000022   # BR(HH -> neutralino1 neutralino1)
     1.20605240E-02   2   1000022   1000023   # BR(HH -> neutralino1 neutralino2)
     1.17359951E-02   2   1000022   1000025   # BR(HH -> neutralino1 neutralino3)
     1.13258046E-03   2   1000022   1000035   # BR(HH -> neutralino1 neutralino4)
     1.41437372E-06   2   1000023   1000023   # BR(HH -> neutralino2 neutralino2)
     3.40544731E-04   2   1000023   1000025   # BR(HH -> neutralino2 neutralino3)
     2.47464671E-02   2   1000023   1000035   # BR(HH -> neutralino2 neutralino4)
     7.60704357E-04   2   1000025   1000025   # BR(HH -> neutralino3 neutralino3)
     3.22674748E-02   2   1000025   1000035   # BR(HH -> neutralino3 neutralino4)
     3.16263680E-06   2   1000035   1000035   # BR(HH -> neutralino4 neutralino4)
     4.71096409E-07   2        25        25   # BR(HH -> h0 h0)
DECAY        36     1.34610768E+02   # Gamma(A0)
     4.30290125E-08   2        22        22   # BR(A0 -> photon photon)
     5.71724686E-08   2        22        23   # BR(A0 -> photon Z)
     1.30047675E-05   2        21        21   # BR(A0 -> gluon gluon)
     8.60359603E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.83081763E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.10640111E-01   2       -15        15   # BR(A0 -> Tau tau)
     1.96899591E-14   2        -2         2   # BR(A0 -> Up up)
     3.81955378E-09   2        -4         4   # BR(A0 -> Charm charm)
     3.01426558E-04   2        -6         6   # BR(A0 -> Top top)
     6.39879043E-07   2        -1         1   # BR(A0 -> Down down)
     2.31453752E-04   2        -3         3   # BR(A0 -> Strange strange)
     6.72445616E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.38009279E-03   2  -1000024   1000024   # BR(A0 -> Chargino1 chargino1)
     6.21133848E-02   2  -1000024   1000037   # BR(A0 -> Chargino1 chargino2)
     6.21133848E-02   2  -1000037   1000024   # BR(A0 -> Chargino2 chargino1)
     4.13901703E-05   2  -1000037   1000037   # BR(A0 -> Chargino2 chargino2)
     1.12426236E-03   2   1000022   1000022   # BR(A0 -> neutralino1 neutralino1)
     1.38994016E-02   2   1000022   1000023   # BR(A0 -> neutralino1 neutralino2)
     1.16911456E-02   2   1000022   1000025   # BR(A0 -> neutralino1 neutralino3)
     1.14863016E-03   2   1000022   1000035   # BR(A0 -> neutralino1 neutralino4)
     1.69801873E-06   2   1000023   1000023   # BR(A0 -> neutralino2 neutralino2)
     3.83214376E-04   2   1000023   1000025   # BR(A0 -> neutralino2 neutralino3)
     3.52648938E-02   2   1000023   1000035   # BR(A0 -> neutralino2 neutralino4)
     7.84180616E-04   2   1000025   1000025   # BR(A0 -> neutralino3 neutralino3)
     2.60179117E-02   2   1000025   1000035   # BR(A0 -> neutralino3 neutralino4)
     2.07738531E-05   2   1000035   1000035   # BR(A0 -> neutralino4 neutralino4)
     1.88357528E-07   2        23        25   # BR(A0 -> Z h0)
     3.86822545E-13   2        23        35   # BR(A0 -> Z HH)
     2.09438832E-39   2        25        25   # BR(A0 -> h0 h0)
DECAY        37     1.54992923E+02   # Gamma(Hp)
     9.51453589E-09   2       -11        12   # BR(Hp -> Electron nu_e)
     4.06776126E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     1.15059430E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     6.17636848E-07   2        -1         2   # BR(Hp -> Down up)
     1.04345337E-05   2        -3         2   # BR(Hp -> Strange up)
     7.37549719E-06   2        -5         2   # BR(Hp -> Bottom up)
     2.90439291E-08   2        -1         4   # BR(Hp -> Down charm)
     2.22599720E-04   2        -3         4   # BR(Hp -> Strange charm)
     1.03283354E-03   2        -5         4   # BR(Hp -> Bottom charm)
     1.92884801E-08   2        -1         6   # BR(Hp -> Down top)
     7.50708988E-07   2        -3         6   # BR(Hp -> Strange top)
     6.96682361E-01   2        -5         6   # BR(Hp -> Bottom top)
     2.39207779E-02   2   1000022   1000024   # BR(Hp -> neutralino1 chargino1)
     2.04821604E-03   2   1000022   1000037   # BR(Hp -> neutralino1 chargino2)
     5.26058805E-04   2   1000023   1000024   # BR(Hp -> neutralino2 chargino1)
     5.37935957E-02   2   1000023   1000037   # BR(Hp -> neutralino2 chargino2)
     7.59911091E-06   2   1000024   1000025   # BR(Hp -> chargino1 neutralino3)
     5.24171298E-02   2   1000025   1000037   # BR(Hp -> neutralino3 chargino2)
     5.38632232E-02   2   1000024   1000035   # BR(Hp -> chargino1 neutralino4)
     4.08655887E-11   2   1000035   1000037   # BR(Hp -> neutralino4 chargino2)
     1.62646839E-07   2        24        25   # BR(Hp -> W h0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    9.92229373E-01    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    2.72813341E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    2.72812564E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    1.00000285E+00    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    3.63703694E-04    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    3.66552033E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    9.92229373E-01    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    2.72813341E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    2.72812564E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99999994E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    5.55494042E-09        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99999994E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    5.55494042E-09        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.26790751E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.41198161E-02        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    1.45680524E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    5.55494042E-09        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99999994E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    3.25679628E-04   # BR(b -> s gamma)
    2    1.58956399E-06   # BR(b -> s mu+ mu-)
    3    3.52509397E-05   # BR(b -> s nu nu)
    4    2.57536139E-15   # BR(Bd -> e+ e-)
    5    1.10016297E-10   # BR(Bd -> mu+ mu-)
    6    2.30183586E-08   # BR(Bd -> tau+ tau-)
    7    8.88444991E-14   # BR(Bs -> e+ e-)
    8    3.79542058E-09   # BR(Bs -> mu+ mu-)
    9    8.04252763E-07   # BR(Bs -> tau+ tau-)
   10    9.54155255E-05   # BR(B_u -> tau nu)
   11    9.85605849E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41857400E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.93541670E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15694652E-03   # epsilon_K
   17    2.28165613E-15   # Delta(M_K)
   18    2.47985648E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29017364E-11   # BR(K^+ -> pi^+ nu nu)
   20    5.01973482E-16   # Delta(g-2)_electron/2
   21    2.14611307E-11   # Delta(g-2)_muon/2
   22    6.08410182E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.03981734E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -1.94470612E-01   # C7
     0305 4322   00   2    -1.17413382E-04   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -1.02375625E-01   # C8
     0305 6321   00   2    -1.47429029E-04   # C8'
 03051111 4133   00   0     1.63147946E+00   # C9 e+e-
 03051111 4133   00   2     1.63170267E+00   # C9 e+e-
 03051111 4233   00   2     4.82328833E-04   # C9' e+e-
 03051111 4137   00   0    -4.45417156E+00   # C10 e+e-
 03051111 4137   00   2    -4.45247000E+00   # C10 e+e-
 03051111 4237   00   2    -3.54481894E-03   # C10' e+e-
 03051313 4133   00   0     1.63147946E+00   # C9 mu+mu-
 03051313 4133   00   2     1.63170247E+00   # C9 mu+mu-
 03051313 4233   00   2     4.82327935E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.45417156E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.45247020E+00   # C10 mu+mu-
 03051313 4237   00   2    -3.54481895E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50498350E+00   # C11 nu_1 nu_1
 03051212 4237   00   2     7.65697913E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50498350E+00   # C11 nu_2 nu_2
 03051414 4237   00   2     7.65698058E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50498350E+00   # C11 nu_3 nu_3
 03051616 4237   00   2     7.65738802E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85822183E-07   # C7
     0305 4422   00   2     4.93140408E-06   # C7
     0305 4322   00   2     3.41696091E-08   # C7'
     0305 6421   00   0     3.30482215E-07   # C8
     0305 6421   00   2     7.26021709E-06   # C8
     0305 6321   00   2     1.26200352E-07   # C8'
 03051111 4133   00   2     1.80611099E-07   # C9 e+e-
 03051111 4233   00   2     9.73299242E-06   # C9' e+e-
 03051111 4137   00   2    -6.28243748E-08   # C10 e+e-
 03051111 4237   00   2    -7.15365491E-05   # C10' e+e-
 03051313 4133   00   2     1.80607660E-07   # C9 mu+mu-
 03051313 4233   00   2     9.73298837E-06   # C9' mu+mu-
 03051313 4137   00   2    -6.28205649E-08   # C10 mu+mu-
 03051313 4237   00   2    -7.15365613E-05   # C10' mu+mu-
 03051212 4137   00   2     2.99945517E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     1.54522405E-05   # C11' nu_1 nu_1
 03051414 4137   00   2     2.99947091E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     1.54522405E-05   # C11' nu_2 nu_2
 03051616 4137   00   2     3.00381156E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     1.54522404E-05   # C11' nu_3 nu_3
