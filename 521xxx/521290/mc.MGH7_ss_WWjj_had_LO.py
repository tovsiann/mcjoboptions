from MadGraphControl.MadGraphUtils import *
import os

# set low_mem_multicore_nlo_generation True

MADGRAPH_CATCH_ERRORS=False

evgenConfig.generators += ["MadGraph", "EvtGen"]
evgenConfig.keywords = ['SM', 'diboson', 'VBS', 'WW', 'SameSign', 'electroweak' ]
evgenConfig.contact = ['Karolos Potamianos <karolos.potamianos@cern.ch>' ]

physShort = get_physics_short()

evgenConfig.nEventsPerJob = 50000
safetyFactor = 1.1
nevents = int(runArgs.maxEvents*safetyFactor if runArgs.maxEvents>0 else safetyFactor*evgenConfig.nEventsPerJob)

required_accuracy = 0.01

isLO = True
isWWcmFrame = False

isPythia = physShort.find("Py8") != -1
isHerwig = physShort.find("H7") != -1

# Use dipole shower for LO and Pythia
useDipoleRecoil = isLO and isPythia
# Use dipole shower for LO and Herwig
useDipoleShower = isLO and isHerwig

if isPythia: evgenConfig.generators += ["Pythia8"]
elif isHerwig: evgenConfig.generators += ["Herwig7"]

gridpack_mode=True

# Processes

evgenConfig.description = 'MadGraph_' + physShort

gen = """
  generate p p > w+ w+ j j QCD=0, w+ > j j @0
  add process p p > w- w- j j QCD=0, w- > j j @0
  """

process = """
  import model sm-no_b_mass
  define l+ = e+ mu+ ta+
  define vl = ve vm vt
  define l- = e- mu- ta-
  define vl~ = ve~ vm~ vt~
  define p = g u c d s u~ c~ d~ s~ b b~
  define j = g u c d s u~ c~ d~ s~ b b~
  {}
  output -f""".format(gen)

#Fetch default NLO run_card.dat and set parameters
extras = { 'lhe_version'  :'3.0',
           'pdlabel'      :"'lhapdf'",
           'lhaid'        : 260000,
           'bwcutoff'     :'15',
           'nevents'      :nevents,
           'dynamical_scale_choice': 0,
           'maxjetflavor': 5, 
           'ptl': 4.0, 
           'ptj': 15.0, 
           'etal': 3.0, 
           'etaj': 5.5, 
           'drll': 0.2, 
           'systematics_program': 'systematics',  
           'systematics_arguments': "['--mur=0.5,1,2', '--muf=0.5,1,2', '--dyn=-1,1,2,3,4', '--weight_info=MUR%(mur).1f_MUF%(muf).1f_PDF%(pdf)i_DYNSCALE%(dyn)i', '--pdf=errorset,NNPDF30_nlo_as_0119@0,NNPDF30_nlo_as_0117@0,CT14nlo@0,MMHT2014nlo68clas118@0,PDF4LHC15_nlo_30_pdfas']"}

process_dir = new_process(process)

if isLO:
  if not is_gen_from_gridpack(): # When generating the gridpack
    os.system("cp setscales_lo.f  "+process_dir+"/SubProcesses/setscales.f")
  lo_extras = { 'asrwgtflavor': 5, 
                'auto_ptj_mjj': False, 
                'cut_decays': True, 
                'ptb': 15.0, 
                'etab': 5.5, 
                'dral': 0.1, 
                'drbb': 0.2, 
                'drbj': 0.2, 
                'drbl': 0.2, 
                'drjj': 0.2,
                'drjl': 0.2, 
                'mmll': 0.0, 
                'gridpack': '.true.',
                'use_syst': True, 
              }
  extras.update(lo_extras)

if isWWcmFrame:
  lo_extras = { 'me_frame' : '3, 4, 5, 6' }
  extras.update(lo_extras)

#modify_config_card(process_dir=process_dir,settings={'cluster_type':'condor'})
modify_run_card(runArgs=runArgs,process_dir=process_dir,settings=extras)
#modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs,required_accuracy=required_accuracy)
outputDS = arrange_output(runArgs=runArgs,process_dir=process_dir,lhe_version=3,saveProcDir=True)

#### Shower                    

if isPythia:
  include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
  # fix for VBS processes (requires version>=8.230)
  genSeq.Pythia8.Commands += [ "SpaceShower:pTmaxMatch=1",
                               "SpaceShower:pTmaxFudge=1",
                               "SpaceShower:MEcorrections=off",
                               "TimeShower:pTmaxMatch=1",
                               "TimeShower:pTmaxFudge=1",
                               "TimeShower:MEcorrections=off",
                               "TimeShower:globalRecoil=on",
                               "TimeShower:limitPTmaxGlobal=on",
                               "TimeShower:nMaxGlobalRecoil=1",
                               "TimeShower:globalRecoilMode=2",
                               "TimeShower:nMaxGlobalBranch=1",
                               "TimeShower:weightGluonToQuark=1",
                               "Check:epTolErr=1e-2"]

  if useDipoleRecoil:
    genSeq.Pythia8.Commands += [ 'SpaceShower:dipoleRecoil = on' ]


  include("Pythia8_i/Pythia8_MadGraph.py")
  include("Pythia8_i/Pythia8_ShowerWeights.py")

elif isHerwig:
  from Herwig7_i.Herwig7_iConf import Herwig7
  from Herwig7_i.Herwig72ConfigLHEF import Hw72ConfigLHEF
  evgenConfig.tune = "H7.1-Default"

  genSeq += Herwig7()
  Herwig7Config = Hw72ConfigLHEF(genSeq, runArgs)

# configure Herwig7
  Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
  Herwig7Config.tune_commands()
  if hasattr(runArgs,'outputTXTFile') and runArgs.outputTXTFile is not None:
      # To go around issue when outputTXTFile is specified and contains .TXT., see.
      # https://gitlab.cern.ch/atlas/athena/-/blob/21.6/Generators/MadGraphControl/python/MadGraphUtils.py#L1556
      lhe_filename = runArgs.outputTXTFile.split('.tar.gz')[0]+'.events'
  else:
      lhe_filename = outputDS.split(".tar.gz")[0] + ".events"
  Herwig7Config.lhef_mg5amc_commands(lhe_filename=lhe_filename, me_pdf_order="NLO")
  
  if useDipoleShower:
     dipoleShowerCommands = """
     cd /Herwig/EventHandlers
     set EventHandler:CascadeHandler /Herwig/DipoleShower/DipoleShowerHandler
     cd /Herwig/DipoleShower
     do DipoleShowerHandler:AddVariation isr:muRfac=2.0_fsr:muRfac=2.0 2.0 2.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=2.0_fsr:muRfac=1.0 2.0 1.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=2.0_fsr:muRfac=0.5 2.0 0.5 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=2.0" 1.0 2.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=0.5" 1.0 0.5 All
     do DipoleShowerHandler:AddVariation isr:muRfac=0.5_fsr:muRfac=2.0" 0.5 2.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=0.5_fsr:muRfac=1.0 0.5 1.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=0.5_fsr:muRfac=0.5 0.5 0.5 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.75_fsr:muRfac=1.0 1.75 1.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.5_fsr:muRfac=1.0 1.5 1.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.25_fsr:muRfac=1.0 1.25 1.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=0.625_fsr:muRfac=1.0" 0.625 1.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=0.75_fsr:muRfac=1.0 0.75 1.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=0.875_fsr:muRfac=1.0 0.875 1.0 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=1.75 1.0 1.75 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=1.5 1.0 1.5 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=1.25 1.0 1.25 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=0.625 1.0 0.625 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=0.75 1.0 0.75 All
     do DipoleShowerHandler:AddVariation isr:muRfac=1.0_fsr:muRfac=0.875 1.0 0.85 All
     """
     print(dipoleShowerCommands)
     Herwig7Config.add_commands(dipoleShowerCommands)
  
# add EvtGen
  include("Herwig7_i/Herwig71_EvtGen.py")

# run Herwig7
  Herwig7Config.run()
