from MadGraphControl.MadGraphUtils import *
from MadGraphControl.MadGraphParamHelpers import set_top_params
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *

import fileinput


# Make some excess of events - make sure we protect against maxEvents=-1
nevents=1.1*runArgs.maxEvents 

process="""
import model loop_sm-no_b_mass
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
generate p p > t t~ [QCD]
output -f"""

process_dir = new_process(process)

#Fetch default NLO run_card.dat and set parameters
settings = {'parton_shower' : 'PYTHIA8',
            'maxjetflavor'  : 5,
            'dynamical_scale_choice' : '10', #user-defined scale -> Dominic's definition of mt+1/2*(pt^2+ptx^2)
            'jetalgo'   : '-1',  # use anti-kT jet algorithm
            'jetradius' : '0.4', # set jet cone size of 0.4
            'ptj'	: '0.1', # minimum jet pT
            'req_acc'   : '0.01',
            'bwcutoff'  : '50', 
            'nevents':int(nevents)            
}


modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
# Modify the param card
set_top_params(process_dir,mTop=172.5,FourFS=False)


# Cook the setscales file for the user defined dynamical scale
fileN = process_dir+'/SubProcesses/setscales.f'
mark  = '      elseif(dynamical_scale_choice.eq.10) then'
rmLines = ['ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc',
           'cc      USER-DEFINED SCALE: ENTER YOUR CODE HERE                                 cc',
           'cc      to use this code you must set                                            cc',
           'cc                 dynamical_scale_choice = 0                                    cc',
           'cc      in the run_card (run_card.dat)                                           cc',
           'write(*,*) "User-defined scale not set"',
           'stop 1',
           'temp_scale_id=\'User-defined dynamical scale\' ! use a meaningful string',
           'tmp = 0',
           'cc      USER-DEFINED SCALE: END OF USER CODE                                     cc'
           ]

for line in fileinput.input(fileN, inplace=1):
    toKeep = True
    for rmLine in rmLines:
        if line.find(rmLine) >= 0:
           toKeep = False
           break
    if toKeep:
        print(line)
    if line.startswith(mark):
        print("""
c         Q^2= mt^2 + 0.5*(pt^2+ptbar^2)
          xm2=dot(pp(0,3),pp(0,3))
          tmp=sqrt(xm2+0.5*(pt(pp(0,3))**2+pt(pp(0,4))**2))
          temp_scale_id='mt**2 + 0.5*(pt**2+ptbar**2)'
              """)


### Decay with MadSpin
madspin_card_loc=process_dir + '/Cards/madspin_card.dat'
mscard = open(madspin_card_loc,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
 set Nevents_for_max_weight 250 # number of events for the estimate of the max. weight
 set max_weight_ps_point 1000  # number of PS to estimate the maximum for each event
 set BW_cut 50
 set seed %i
 define j = g u c d s b u~ c~ d~ s~ b~
 define lv = e+ mu+ ta+ ve vm vt e- mu- ta- ve~ vm~ vt~
 decay t > w+ b, w+ > all all
 decay t~ > w- b~, w- > all all
 launch
"""%(runArgs.randomSeed))
mscard.close()


generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)  

### Metadata
evgenConfig.description = 'MG5_aMC@NLO+MadSpin ttbar production, pp collisions'
evgenConfig.generators    = ['aMcAtNlo']
evgenConfig.keywords = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact  = [ "dominic.hirschbuehl@cern.ch" ]
evgenConfig.nEventsPerJob = 20000

