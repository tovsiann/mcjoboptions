#! /usr/bin/env python

import os,sys,time,subprocess,shutil,glob
from AthenaCommon import Logging
mglog = Logging.logging.getLogger('MadGraphControl_BulkRS_VBF_fixed')

## to run systematic variations on the fly, new in MG2.6.2 (https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/MadGraph5aMCatNLOForAtlas#PDF_and_systematics_via_Base_fra)
## the following setting is needed only if the default base fragments can not be used
import MadGraphControl.MadGraphUtils
MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':247000, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets
    'pdf_variations':[247000], # pdfs for which all variations (error sets) will be included as weights
    'alternative_pdfs':[246800,13205,25000], # pdfs for which only the central set will be included as weights
    'scale_variations':[0.5,1.,2.], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated
}

from MadGraphControl.MadGraphUtils import *

# extract dataset short name from filename, should be of the form MadGraphPythia8EvtGen_AU2MSTW2008LO_VBF_RS_G_WW_qqqq_kt1_m1000
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
shortname = get_physics_short().split('_')

gentype=shortname[4]
decaytype=shortname[5]
scenario=shortname[6]
mass=int(shortname[7][1:])
isVBF=('VBF' in shortname)

# PDF information, in MadGraph's PDF naming scheme.  
# Should probably extract this from shortname[1] in some fancy way.
# For now, specify lhapdf and the ID so that madgraph doesn't get 
# confused trying to interpret some shorthand name (which it can).
# Note that if you change these numbers, you'll probably want to 
# change the "sys_pdf" tag in the run card too.  That's not done
# automatically yet.
pdf='lhapdf'
lhaid=247000 # NNPDF23_lo_as_0130_qed

scalevariation=1.0
alpsfact=1.0

# -----------------------------------------------------------------------
# Need to arrange several things:
# -- proc_card.dat, this will be determined mostly by the run number.
#    once we have this, we can also initialize the process in madgraph
#
# -- param_card.dat, this is mostly invariant, but does depend on the model scenario
#
# -- run_card.dat, this is also mostly invariant
#
# At that point we can generate the events, arrange the output, and cleanup.
# -----------------------------------------------------------------------
# proc card
# -----------------------------------------------------------------------
# first, figure out what process we should run
processes=[]
if gentype == "WW" or gentype == "ww":
    if decaytype == "lvlv" or decaytype == "llvv":
        processes.append("generate p p > h2, (h2 > w+ w-, w+ > l+ vl, w- > vl~ l-)")
    elif decaytype == "jjjj" or decaytype == "qqqq":
        processes.append("generate p p > h2, (h2 > w+ w-, w+ > j j, w- > j j)")
    elif decaytype == "lvqq" or decaytype == "lvjj":
        processes.append("generate p p > h2, (h2 > w+ w-, w+ > l+ vl, w- > j j)")
        processes.append("add process p p > h2, (h2 > w+ w-, w+ > j j, w- > l- vl~)")
    else:
      raise RuntimeError('Could not configure process for %s > %s' % (gentype, decaytype))

elif gentype == "ZZ" or gentype == "zz":
    if decaytype == "llll":
        processes.append("generate p p > h2, (h2 > z z, z > l+ l-, z > l+ l-)")
    elif decaytype == "llvv" or gentype == "vvll":
        processes.append("generate p p > h2, (h2 > z z, z > l+ l-, z > vl vl~)")
    elif decaytype == "jjjj" or decaytype == "qqqq":
        processes.append("generate p p > h2, (h2 > z z, z > j j, z > j j)")
    elif decaytype == "llqq" or decaytype == "lljj":
        processes.append("generate p p > h2, (h2 > z z, z > l+ l-, z > j j)")
    elif decaytype == "vvqq" or decaytype == "vvjj" or decaytype == "qqvv" or decaytype == "jjvv":
        processes.append("generate p p > h2, (h2 > z z, z > vl vl~, z > j j)")
    else:
      raise RuntimeError('Could not configure process for %s > %s' % (gentype, decaytype))

else:
  raise RuntimeError('Could not configure process for %s > %s' % (gentype, decaytype))

#if VBF change proccess
if isVBF:
  mglog.info("Preparing VBF Radion production")
  for i in range(0,len(processes)):
    processes[i]=processes[i].replace("> h2","> h2 j j  $$ z w+ w- / g a h",1);

processstring=""
for i in range(0,len(processes)):
  processstring+=processes[i]+"\n"

# Write the proc card
process_str="""
set group_subprocesses Auto
set ignore_six_quark_processes False
set gauge unitary
set complex_mass_scheme False
import model sm

define p = g u c d s u~ c~ d~ s~ b b~
define j = u c d s u~ c~ d~ s~ b b~
define jprime = u c d s u~ c~ d~ s~ b b~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~

import model heft_radion

%s

output -f""" % processstring

# Generate the new process!
if not is_gen_from_gridpack():
  process_dir = new_process(process_str)
else:
  process_dir = MADGRAPH_GRIDPACK_LOCATION

# -----------------------------------------------------------------------
# param card
# -----------------------------------------------------------------------
## couplings
kl=scenario.split("L")[0].strip("kl")
lambda_r=str(float(scenario.split("L")[-1])*1000)
widths={300:"0.06",400:"0.22",500:"0.50",600:"0.89",700:"1.33",800:"2.1",900:"3.0",1000:"3.9",1200:"6.5",1400:"10",1500:"13",1600:"15",1800:"22",2000:"28",2200:"39.0",2400:"50",2600:"64",2800:"77",3000:"90",3500:"150",4000:"200",4500:"320",5000:"400",6000:"650"}
params={}
mass_inputs={}
width_inputs={}
mass_inputs['mh02']=mass
mass_inputs['K']=kl
mass_inputs['LR']=lambda_r
width_inputs['wh02']=widths[mass]
params['mass']=mass_inputs
params['decay']=width_inputs

modify_param_card(process_dir=process_dir,params=params)

#----------------------------------------------------------------------------
# Random Seed
#----------------------------------------------------------------------------
randomSeed = 0
if hasattr(runArgs,'randomSeed'): randomSeed = runArgs.randomSeed
else:
    raise RuntimeError("No random seed given.")

#----------------------------------------------------------------------------
# Beam energy
#----------------------------------------------------------------------------
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = int(runArgs.ecmEnergy) / 2.
else:
    raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------
safefactor = 1.1
if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:  nevents = int(runArgs.maxEvents*safefactor)
else: nevents = int(evgenConfig.nEventsPerJob*safefactor)

#---------------------------------------------------------------------------
# run card
#---------------------------------------------------------------------------
## generator cuts
extras = {
  'ptj':"0",
  'ptb':"0",
  'pta':"0",
  'ptl':"0",
  'etaj':"-1",
  'etab':"-1",
  'etaa':"-1",
  'etal':"-1",
  'drjj':"0",
  'drbb':"0",
  'drll':"0",
  'draa':"0",
  'drbj':"0",
  'draj':"0",
  'drjl':"0",
  'drbl':"0",
  'dral':"0",
  'nevents': nevents,
}
if isVBF:
  extras['cut_decays']="F"
  extras['mmjj']=150

## TODO: are the following needed, since fixed_ren_scale and fixed_fac_scale are F by default?
extras["scale"] = mass
extras["dsqrt_q2fact1"] = mass
extras["dsqrt_q2fact2"] = mass

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

# -----------------------------------------------------------------------
# Generation
# -----------------------------------------------------------------------
gridpack_mode=False
try:
  generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)
except RuntimeError as rte:
  for an_arg in rte.args:
      if 'Gridpack sucessfully created' in an_arg:
          print('Handling exception and exiting')
          theApp.finalize()
          theApp.exit()
  print('Unhandled exception - re-raising')
  raise rte

# -----------------------------------------------------------------------
# Cleanup
# -----------------------------------------------------------------------
try:
    arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)
except:
    raise RuntimeError('Error arranging output dataset!')

mglog.info('All done generating events!!')

# Turn on single-core for pythia
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print(opts)

#--------------------------------------------------------------------------------------------------------------------
# Metadata
#--------------------------------------------------------------------------------------------------------------------
# Some more information
evgenConfig.description = "Bulk RS Radion Signal Point"
evgenConfig.keywords = ["exotic", "BSM", "RandallSundrum", "warpedED"]
evgenConfig.contact = ["Robert Les <robert.les@cern.ch>"]
evgenConfig.process = "pp>phi>%s>%s" % (gentype,decaytype) # e.g. pp>phi*>WW>qqqq

#--------------------------------------------------------------------------------------------------------------------
# Shower
#--------------------------------------------------------------------------------------------------------------------
# Finally, run the parton shower and configure MadGraph+Pythia
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")
if isVBF:
  genSeq.Pythia8.Commands += ["SpaceShower:dipoleRecoil = on"]
