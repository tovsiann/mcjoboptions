parameters={                                                                                                              
    'yukawa':{
        'ymt':   '-1.725000e+02'},
    'frblock':{
        'cosa':  '0.7071068',
        'kSM':   '1.41421356'},
}

include('MadGraphControl_tHjb_CP_NLO.py')
evgenConfig.inputconfcheck = "tHjb"
