# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 07.09.2021,  18:36
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.70031916E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.21425327E+03  # scale for input parameters
    1    1.88386724E+02  # M_1
    2    2.72241749E+02  # M_2
    3    2.03109900E+03  # M_3
   11   -5.55867176E+03  # A_t
   12    1.89989794E+03  # A_b
   13    1.42515350E+03  # A_tau
   23    3.46870871E+02  # mu
   25    3.59772287E+01  # tan(beta)
   26    2.45411740E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    3.58806365E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    2.92224099E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    4.73392855E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.21425327E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.21425327E+03  # (SUSY scale)
  1  1     8.38761032E-06   # Y_u(Q)^DRbar
  2  2     4.26090604E-03   # Y_c(Q)^DRbar
  3  3     1.01329070E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.21425327E+03  # (SUSY scale)
  1  1     6.06381858E-04   # Y_d(Q)^DRbar
  2  2     1.15212553E-02   # Y_s(Q)^DRbar
  3  3     6.01340791E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.21425327E+03  # (SUSY scale)
  1  1     1.05818068E-04   # Y_e(Q)^DRbar
  2  2     2.18798205E-02   # Y_mu(Q)^DRbar
  3  3     3.67982586E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.21425327E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -5.55867638E+03   # A_t(Q)^DRbar
Block Ad Q=  3.21425327E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3     1.89989591E+03   # A_b(Q)^DRbar
Block Ae Q=  3.21425327E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3     1.42515229E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.21425327E+03  # soft SUSY breaking masses at Q
   1    1.88386724E+02  # M_1
   2    2.72241749E+02  # M_2
   3    2.03109900E+03  # M_3
  21   -2.19252353E+05  # M^2_(H,d)
  22    1.09523878E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    3.58806365E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    2.92224099E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    4.73392855E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.28212281E+02  # h0
        35     2.45594739E+02  # H0
        36     2.45411740E+02  # A0
        37     2.74932828E+02  # H+
   1000001     1.01156676E+04  # ~d_L
   2000001     1.00903281E+04  # ~d_R
   1000002     1.01153066E+04  # ~u_L
   2000002     1.00936493E+04  # ~u_R
   1000003     1.01156728E+04  # ~s_L
   2000003     1.00903370E+04  # ~s_R
   1000004     1.01153118E+04  # ~c_L
   2000004     1.00936506E+04  # ~c_R
   1000005     3.57803806E+03  # ~b_1
   2000005     4.77429532E+03  # ~b_2
   1000006     2.87103702E+03  # ~t_1
   2000006     3.59849909E+03  # ~t_2
   1000011     1.00215727E+04  # ~e_L-
   2000011     1.00087913E+04  # ~e_R-
   1000012     1.00208053E+04  # ~nu_eL
   1000013     1.00215968E+04  # ~mu_L-
   2000013     1.00088372E+04  # ~mu_R-
   1000014     1.00208290E+04  # ~nu_muL
   1000015     1.00218753E+04  # ~tau_1-
   2000015     1.00285137E+04  # ~tau_2-
   1000016     1.00275943E+04  # ~nu_tauL
   1000021     2.44190529E+03  # ~g
   1000022     1.84584179E+02  # ~chi_10
   1000023     2.68942372E+02  # ~chi_20
   1000025     3.60254042E+02  # ~chi_30
   1000035     3.96006860E+02  # ~chi_40
   1000024     2.67512918E+02  # ~chi_1+
   1000037     3.95958739E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -4.27489476E-02   # alpha
Block Hmix Q=  3.21425327E+03  # Higgs mixing parameters
   1    3.46870871E+02  # mu
   2    3.59772287E+01  # tan[beta](Q)
   3    2.43134445E+02  # v(Q)
   4    6.02269221E+04  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     1.68638341E-01   # Re[R_st(1,1)]
   1  2     9.85677995E-01   # Re[R_st(1,2)]
   2  1    -9.85677995E-01   # Re[R_st(2,1)]
   2  2     1.68638341E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.99997111E-01   # Re[R_sb(1,1)]
   1  2     2.40361924E-03   # Re[R_sb(1,2)]
   2  1    -2.40361924E-03   # Re[R_sb(2,1)]
   2  2     9.99997111E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     1.52241626E-01   # Re[R_sta(1,1)]
   1  2     9.88343304E-01   # Re[R_sta(1,2)]
   2  1    -9.88343304E-01   # Re[R_sta(2,1)]
   2  2     1.52241626E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.71858780E-01   # Re[N(1,1)]
   1  2     7.76548879E-02   # Re[N(1,2)]
   1  3    -1.95462488E-01   # Re[N(1,3)]
   1  4     1.06087915E-01   # Re[N(1,4)]
   2  1    -1.81760490E-01   # Re[N(2,1)]
   2  2    -8.43104273E-01   # Re[N(2,2)]
   2  3     4.01651972E-01   # Re[N(2,3)]
   2  4    -3.07918825E-01   # Re[N(2,4)]
   3  1    -5.58536525E-02   # Re[N(3,1)]
   3  2     8.46198614E-02   # Re[N(3,2)]
   3  3     6.96746123E-01   # Re[N(3,3)]
   3  4     7.10115969E-01   # Re[N(3,4)]
   4  1    -1.39046775E-01   # Re[N(4,1)]
   4  2     5.25342158E-01   # Re[N(4,2)]
   4  3     5.61261926E-01   # Re[N(4,3)]
   4  4    -6.24232859E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -7.97150683E-01   # Re[U(1,1)]
   1  2     6.03780414E-01   # Re[U(1,2)]
   2  1     6.03780414E-01   # Re[U(2,1)]
   2  2     7.97150683E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -8.87052812E-01   # Re[V(1,1)]
   1  2     4.61667964E-01   # Re[V(1,2)]
   2  1     4.61667964E-01   # Re[V(2,1)]
   2  2     8.87052812E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02523729E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.44583620E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     3.30142355E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     3.11393596E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     1.92881645E-02    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.44648772E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     5.94092714E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.69180226E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     9.12799703E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     6.18495554E-02    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     3.87004482E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     2.21643665E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.04427696E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.41094744E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     3.31941918E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     4.01718704E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.98088760E-02    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     6.87922765E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
     1.19707879E-03    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
DECAY   1000013     1.44744326E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     5.93953711E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.69109390E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.23147802E-03    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     6.20159502E-02    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     3.86749930E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     2.21497881E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.05761534E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     4.62065037E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     9.43793760E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.17269213E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     5.81453802E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.69784450E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     9.83565428E-02    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.70115124E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     5.48006392E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.31264350E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     8.25360331E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.21908202E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     2.81484148E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     2.28006629E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.44653634E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     1.08542621E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     1.69446637E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     3.98424175E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     1.09289047E-01    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     4.79166179E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     1.29571274E-01    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.44749175E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.08471236E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     1.69335197E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     3.98162146E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     1.09217172E-01    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     4.79091046E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     1.29903728E-01    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.71698014E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     9.15079684E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.42853870E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     3.35896449E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     9.21375557E-02    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     4.61237226E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     2.08904416E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     7.11532748E+02   # ~d_R
#    BR                NDA      ID1      ID2
     7.47288368E-03    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     2.61205231E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     1.52632865E-04    2     1000035         1   # BR(~d_R -> chi^0_4 d)
     9.92088489E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     8.42837812E+02   # ~d_L
#    BR                NDA      ID1      ID2
     3.31843446E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     3.46737055E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     4.71259198E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     1.59371199E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     6.70435298E-02    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     3.83981126E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     8.40157839E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     7.11586543E+02   # ~s_R
#    BR                NDA      ID1      ID2
     7.47375089E-03    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     2.67201501E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     1.64348836E-04    2     1000035         3   # BR(~s_R -> chi^0_4 s)
     9.92014579E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     8.42868553E+02   # ~s_L
#    BR                NDA      ID1      ID2
     3.31952172E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     3.46775492E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     4.86546589E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     1.59464739E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     6.70420355E-02    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     3.84001389E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     8.40127734E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     2.63485223E+02   # ~b_1
#    BR                NDA      ID1      ID2
     7.49143134E-03    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     5.45266583E-02    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     4.69658031E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     4.75638822E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     1.32226150E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     2.56054456E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
     3.05924528E-01    2     1000021         5   # BR(~b_1 -> ~g b)
     1.48988011E-01    2     1000006       -24   # BR(~b_1 -> ~t_1 W^-)
     2.59080449E-04    2     1000006       -37   # BR(~b_1 -> ~t_1 H^-)
DECAY   2000005     2.80812567E+02   # ~b_2
#    BR                NDA      ID1      ID2
     1.35429897E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.97820769E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     5.87679445E-02    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     3.83358810E-02    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     4.38829933E-02    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     7.67654523E-02    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
     7.31697705E-01    2     1000021         5   # BR(~b_2 -> ~g b)
     1.18390306E-04    2     2000006       -24   # BR(~b_2 -> ~t_2 W^-)
     8.19657840E-04    2     1000006       -37   # BR(~b_2 -> ~t_1 H^-)
     7.72738420E-03    2     2000006       -37   # BR(~b_2 -> ~t_2 H^-)
     4.19204592E-03    2     1000005        36   # BR(~b_2 -> ~b_1 A^0)
     4.21024768E-03    2     1000005        35   # BR(~b_2 -> ~b_1 H^0)
DECAY   2000002     7.28717633E+02   # ~u_R
#    BR                NDA      ID1      ID2
     2.91962257E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     1.02045302E-03    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     5.96203815E-04    2     1000035         2   # BR(~u_R -> chi^0_4 u)
     9.69090866E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     8.42826094E+02   # ~u_L
#    BR                NDA      ID1      ID2
     4.80528979E-04    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     4.04309131E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     2.93817978E-04    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     1.31965903E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     8.30167338E-02    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     2.24493017E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     8.40132114E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     7.28724990E+02   # ~c_R
#    BR                NDA      ID1      ID2
     2.91959894E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     1.02091680E-03    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     5.98142042E-04    2     1000035         4   # BR(~c_R -> chi^0_4 c)
     9.69081196E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     8.42856803E+02   # ~c_L
#    BR                NDA      ID1      ID2
     4.80560503E-04    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     4.04298678E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     2.95987199E-04    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     1.31978030E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     8.30252549E-02    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     2.24685193E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     8.40102007E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.36686621E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.10654633E-02    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     5.79660731E-02    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.12106320E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     1.36989367E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     1.30591484E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     2.82425949E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
     1.28760935E-01    2     1000021         6   # BR(~t_1 -> ~g t)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000006     2.71353652E+02   # ~t_2
#    BR                NDA      ID1      ID2
     1.70482893E-03    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     5.82112373E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     1.30544503E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.32719407E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     9.93426209E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     1.10646330E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     2.87696489E-01    2     1000021         6   # BR(~t_2 -> ~g t)
     7.56526008E-02    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     1.32227598E-04    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     1.03312138E-01    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
DECAY   1000024     1.97429941E-03   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99885936E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
     1.05552314E-04    3     1000022       -15        16   # BR(chi^+_1 -> chi^0_1 tau^+ nu_tau)
DECAY   1000037     7.32854423E-01   # chi^+_2
#    BR                NDA      ID1      ID2
     1.16270390E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     5.35049523E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     3.37885196E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     1.02944890E-02    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     1.89703420E-04    3     1000024         5        -5   # BR(chi^+_2 -> chi^+_1 b b_bar)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.01418286E-04   # chi^0_2
#    BR                NDA      ID1      ID2
     1.18332177E-03    2     1000022        22   # BR(chi^0_2 -> chi^0_1 photon)
#    BR                NDA      ID1      ID2       ID3
     1.12432455E-01    3     1000022         2        -2   # BR(chi^0_2 -> chi^0_1 u u_bar)
     1.12248664E-01    3     1000022         4        -4   # BR(chi^0_2 -> chi^0_1 c c_bar)
     1.44173394E-01    3     1000022         1        -1   # BR(chi^0_2 -> chi^0_1 d d_bar)
     1.44190602E-01    3     1000022         3        -3   # BR(chi^0_2 -> chi^0_1 s s_bar)
     1.89858823E-01    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     3.25055548E-02    3     1000022        11       -11   # BR(chi^0_2 -> chi^0_1 e^- e^+)
     3.25259468E-02    3     1000022        13       -13   # BR(chi^0_2 -> chi^0_1 mu^- mu^+)
     3.82198322E-02    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
     1.92661145E-01    3     1000022        12       -12   # BR(chi^0_2 -> chi^0_1 nu_e nu_bar_e)
DECAY   1000025     1.56834959E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     3.21652371E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     3.21652371E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     2.29404499E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     3.30515675E-02    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     9.38472699E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     2.00760221E-04    2     1000023        22   # BR(chi^0_3 -> chi^0_2 photon)
#    BR                NDA      ID1      ID2       ID3
     1.39621926E-04    3     1000022         5        -5   # BR(chi^0_3 -> chi^0_1 b b_bar)
DECAY   1000035     8.42544411E-01   # chi^0_4
#    BR                NDA      ID1      ID2
     4.18281192E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     4.18281192E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     2.71098136E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     5.94625986E-03    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     1.29810425E-01    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     2.67609575E-04    3     1000022         5        -5   # BR(chi^0_4 -> chi^0_1 b b_bar)
DECAY   1000021     1.63916366E-01   # ~g
#    BR                NDA      ID1      ID2
     7.38958693E-04    2     1000023        21   # BR(~g -> chi^0_2 g)
     3.40738012E-03    2     1000025        21   # BR(~g -> chi^0_3 g)
     2.39760581E-03    2     1000035        21   # BR(~g -> chi^0_4 g)
#    BR                NDA      ID1      ID2       ID3
     2.84908191E-02    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     3.59170809E-03    3     1000022         5        -5   # BR(~g -> chi^0_1 b b_bar)
     1.55479948E-04    3     1000023         2        -2   # BR(~g -> chi^0_2 u u_bar)
     1.55483547E-04    3     1000023         4        -4   # BR(~g -> chi^0_2 c c_bar)
     4.78447890E-02    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     1.31280268E-04    3     1000023         1        -1   # BR(~g -> chi^0_2 d d_bar)
     1.31328665E-04    3     1000023         3        -3   # BR(~g -> chi^0_2 s s_bar)
     1.98235208E-02    3     1000023         5        -5   # BR(~g -> chi^0_2 b b_bar)
     1.88185181E-01    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     2.13136582E-02    3     1000025         5        -5   # BR(~g -> chi^0_3 b b_bar)
     1.55933932E-01    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     1.88905805E-02    3     1000035         5        -5   # BR(~g -> chi^0_4 b b_bar)
     2.82106879E-04    3     1000024         1        -2   # BR(~g -> chi^+_1 d u_bar)
     2.82106879E-04    3    -1000024        -1         2   # BR(~g -> chi^-_1 d_bar u)
     2.82165749E-04    3     1000024         3        -4   # BR(~g -> chi^+_1 s c_bar)
     2.82165749E-04    3    -1000024        -3         4   # BR(~g -> chi^-_1 s_bar c)
     7.22224707E-02    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     7.22224707E-02    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     1.08186370E-04    3     1000037         1        -2   # BR(~g -> chi^+_2 d u_bar)
     1.08186370E-04    3    -1000037        -1         2   # BR(~g -> chi^-_2 d_bar u)
     1.08287271E-04    3     1000037         3        -4   # BR(~g -> chi^+_2 s c_bar)
     1.08287271E-04    3    -1000037        -3         4   # BR(~g -> chi^-_2 s_bar c)
     1.81154515E-01    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     1.81154515E-01    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     7.52025811E-03   # Gamma(h0)
     1.35963634E-03   2        22        22   # BR(h0 -> photon photon)
     1.02299300E-03   2        22        23   # BR(h0 -> photon Z)
     2.10724256E-02   2        23        23   # BR(h0 -> Z Z)
     1.63832596E-01   2       -24        24   # BR(h0 -> W W)
     3.80568894E-02   2        21        21   # BR(h0 -> gluon gluon)
     5.99169580E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.66523340E-04   2       -13        13   # BR(h0 -> Muon muon)
     7.68479713E-02   2       -15        15   # BR(h0 -> Tau tau)
     6.63410345E-08   2        -2         2   # BR(h0 -> Up up)
     1.28764164E-02   2        -4         4   # BR(h0 -> Charm charm)
     6.88626502E-07   2        -1         1   # BR(h0 -> Down down)
     2.49075681E-04   2        -3         3   # BR(h0 -> Strange strange)
     6.84414712E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     5.69660285E+00   # Gamma(HH)
     3.53068000E-07   2        22        22   # BR(HH -> photon photon)
     1.10866572E-07   2        22        23   # BR(HH -> photon Z)
     4.80258010E-05   2        23        23   # BR(HH -> Z Z)
     1.14479540E-04   2       -24        24   # BR(HH -> W W)
     4.70306033E-04   2        21        21   # BR(HH -> gluon gluon)
     8.47682331E-09   2       -11        11   # BR(HH -> Electron electron)
     3.77140880E-04   2       -13        13   # BR(HH -> Muon muon)
     1.08849185E-01   2       -15        15   # BR(HH -> Tau tau)
     2.47226805E-13   2        -2         2   # BR(HH -> Up up)
     4.79234931E-08   2        -4         4   # BR(HH -> Charm charm)
     8.70436219E-07   2        -1         1   # BR(HH -> Down down)
     3.14864014E-04   2        -3         3   # BR(HH -> Strange strange)
     8.89824608E-01   2        -5         5   # BR(HH -> Bottom bottom)
DECAY        36     5.69799038E+00   # Gamma(A0)
     1.32435114E-07   2        22        22   # BR(A0 -> photon photon)
     1.55084151E-08   2        22        23   # BR(A0 -> photon Z)
     4.55793071E-04   2        21        21   # BR(A0 -> gluon gluon)
     8.47365221E-09   2       -11        11   # BR(A0 -> Electron electron)
     3.76999988E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.08831486E-01   2       -15        15   # BR(A0 -> Tau tau)
     8.67443407E-14   2        -2         2   # BR(A0 -> Up up)
     1.68426378E-08   2        -4         4   # BR(A0 -> Charm charm)
     8.70214997E-07   2        -1         1   # BR(A0 -> Down down)
     3.14784037E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.90001591E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.83022325E-05   2        23        25   # BR(A0 -> Z h0)
DECAY        37     2.92276830E+00   # Gamma(Hp)
     2.12995900E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     9.10623731E-04   2       -13        14   # BR(Hp -> Muon nu_mu)
     2.57554909E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     1.88143036E-06   2        -1         2   # BR(Hp -> Down up)
     3.06900967E-05   2        -3         2   # BR(Hp -> Strange up)
     2.06527095E-05   2        -5         2   # BR(Hp -> Bottom up)
     8.70689312E-08   2        -1         4   # BR(Hp -> Down charm)
     6.78133898E-04   2        -3         4   # BR(Hp -> Strange charm)
     2.89199575E-03   2        -5         4   # BR(Hp -> Bottom charm)
     8.70448124E-08   2        -1         6   # BR(Hp -> Down top)
     2.25197051E-06   2        -3         6   # BR(Hp -> Strange top)
     7.37737378E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.52403175E-04   2        24        25   # BR(Hp -> W h0)
     9.29455768E-06   2        24        35   # BR(Hp -> W HH)
     9.59032071E-06   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    2.36579494E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.29299519E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.29436098E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.98944811E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    1.82777059E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    7.72582001E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    2.36579494E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.29299519E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.29436098E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99776193E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    2.23807133E-04        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99776193E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    2.23807133E-04        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.19433752E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.10507827E+00        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    6.34605462E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    2.23807133E-04        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99776193E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    4.64628282E-04   # BR(b -> s gamma)
    2    1.59695439E-06   # BR(b -> s mu+ mu-)
    3    3.52616464E-05   # BR(b -> s nu nu)
    4    5.43897278E-15   # BR(Bd -> e+ e-)
    5    2.32324522E-10   # BR(Bd -> mu+ mu-)
    6    4.73434218E-08   # BR(Bd -> tau+ tau-)
    7    1.82605146E-13   # BR(Bs -> e+ e-)
    8    7.80016991E-09   # BR(Bs -> mu+ mu-)
    9    1.61231316E-06   # BR(Bs -> tau+ tau-)
   10    2.47692644E-05   # BR(B_u -> tau nu)
   11    2.55857019E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41733771E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.91040505E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15852586E-03   # epsilon_K
   17    2.28179471E-15   # Delta(M_K)
   18    2.48047712E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29158927E-11   # BR(K^+ -> pi^+ nu nu)
   20    1.95334521E-16   # Delta(g-2)_electron/2
   21    8.35121355E-12   # Delta(g-2)_muon/2
   22    2.36633068E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.23487862E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -3.24933673E-01   # C7
     0305 4322   00   2    -2.98338034E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -2.37665295E-01   # C8
     0305 6321   00   2    -2.91953862E-03   # C8'
 03051111 4133   00   0     1.61922298E+00   # C9 e+e-
 03051111 4133   00   2     1.61976061E+00   # C9 e+e-
 03051111 4233   00   2    -4.28822931E-04   # C9' e+e-
 03051111 4137   00   0    -4.44191508E+00   # C10 e+e-
 03051111 4137   00   2    -4.44100074E+00   # C10 e+e-
 03051111 4237   00   2     3.21159677E-03   # C10' e+e-
 03051313 4133   00   0     1.61922298E+00   # C9 mu+mu-
 03051313 4133   00   2     1.61976042E+00   # C9 mu+mu-
 03051313 4233   00   2    -4.29416240E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44191508E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44100092E+00   # C10 mu+mu-
 03051313 4237   00   2     3.21219114E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50515410E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -6.95635970E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50515410E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -6.95507177E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50515410E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -6.59212282E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85839075E-07   # C7
     0305 4422   00   2     1.20874949E-05   # C7
     0305 4322   00   2     2.65025630E-06   # C7'
     0305 6421   00   0     3.30496685E-07   # C8
     0305 6421   00   2    -1.26075119E-05   # C8
     0305 6321   00   2     8.96407735E-07   # C8'
 03051111 4133   00   2     6.43554776E-07   # C9 e+e-
 03051111 4233   00   2     6.58230580E-06   # C9' e+e-
 03051111 4137   00   2     5.36338996E-06   # C10 e+e-
 03051111 4237   00   2    -4.92392115E-05   # C10' e+e-
 03051313 4133   00   2     6.43550959E-07   # C9 mu+mu-
 03051313 4233   00   2     6.58230437E-06   # C9' mu+mu-
 03051313 4137   00   2     5.36339372E-06   # C10 mu+mu-
 03051313 4237   00   2    -4.92392158E-05   # C10' mu+mu-
 03051212 4137   00   2    -1.12559501E-06   # C11 nu_1 nu_1
 03051212 4237   00   2     1.06652570E-05   # C11' nu_1 nu_1
 03051414 4137   00   2    -1.12559481E-06   # C11 nu_2 nu_2
 03051414 4237   00   2     1.06652570E-05   # C11' nu_2 nu_2
 03051616 4137   00   2    -1.12554031E-06   # C11 nu_3 nu_3
 03051616 4237   00   2     1.06652570E-05   # C11' nu_3 nu_3
