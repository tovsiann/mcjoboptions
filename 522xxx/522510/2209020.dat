# SUSY Les Houches Accord 2 - MSSM spectrum + Decays
# SPheno v4.0.5be
# W. Porod, Comput. Phys. Commun. 153 (2003) 275-315, hep-ph/0301101;
# W. Porod, F.~Staub, Comput. Phys. Commun. 183 (2012) 2458
#                     arXiv:1104.1573 [hep-ph] 
# in case of problems send email to porod@physik.uni-wuerzburg.de
# Created: 08.09.2021,  13:26
Block SPINFO         # Program information
     1   SPheno      # spectrum calculator
     2   v4.0.5be    # version number
#
Block SPhenoINFO     # SPheno specific information
    1      2         # using 2-loop RGEs
    2      2         # using pole masses for boundary conditions at mZ
Block MODSEL  # Model selection
    1    0    # general MSSM
Block MINPAR  # Input parameters
    3    3.59620711E+01  # tanb at m_Z    
    4    1.00000000E+00  # Sign(mu)
Block EXTPAR  # non-universal input parameters
    0    3.79857210E+03  # scale for input parameters
    1    1.86362775E+02  # M_1
    2    2.78521102E+02  # M_2
    3    4.01472604E+03  # M_3
   11   -1.26569543E+03  # A_t
   12   -1.44703737E+03  # A_b
   13   -1.86985142E+03  # A_tau
   23    3.83829115E+02  # mu
   25    3.48141489E+01  # tan(beta)
   26    2.29309766E+02  # m_A, pole mass
   31    1.00000000E+04  # M_L11
   32    1.00000000E+04  # M_L22
   33    1.00000000E+04  # M_L33
   34    1.00000000E+04  # M_E11
   35    1.00000000E+04  # M_E22
   36    1.00000000E+04  # M_E33
   41    1.00000000E+04  # M_Q11
   42    1.00000000E+04  # M_Q22
   43    2.99308572E+03  # M_Q33
   44    1.00000000E+04  # M_U11
   45    1.00000000E+04  # M_U22
   46    4.54420108E+03  # M_U33
   47    1.00000000E+04  # M_D11
   48    1.00000000E+04  # M_D22
   49    2.99728557E+03  # M_D33
Block SMINPUTS  # SM parameters
         1     1.27908970E+02  # alpha_em^-1(MZ)^MSbar
         2     1.16637870E-05  # G_mu [GeV^-2]
         3     1.18400000E-01  # alpha_s(MZ)^MSbar
         4     9.11876000E+01  # m_Z(pole)
         5     4.18000000E+00  # m_b(m_b), MSbar
         6     1.73200000E+02  # m_t(pole)
         7     1.77700000E+00  # m_tau(pole)
         8     0.00000000E+00  # m_nu_3
        11     5.10998930E-04  # m_e(pole)
        12     0.00000000E+00  # m_nu_1
        13     1.05658372E-01  # m_muon(pole)
        14     0.00000000E+00  # m_nu_2
        21     5.00000000E-03  # m_d(2 GeV), MSbar
        22     2.50000000E-03  # m_u(2 GeV), MSbar
        23     9.50000000E-02  # m_s(2 GeV), MSbar
        24     1.27000000E+00  # m_c(m_c), MSbar
Block gauge Q=  3.79857210E+03  # (SUSY scale)
   1    3.55362975E-01  # g'(Q)^DRbar
   2    6.65198874E-01  # g(Q)^DRbar
   3    1.21977796E+00  # g3(Q)^DRbar
Block Yu Q=  3.79857210E+03  # (SUSY scale)
  1  1     8.38783025E-06   # Y_u(Q)^DRbar
  2  2     4.26101777E-03   # Y_c(Q)^DRbar
  3  3     1.01331727E+00   # Y_t(Q)^DRbar
Block Yd Q=  3.79857210E+03  # (SUSY scale)
  1  1     5.86793998E-04   # Y_d(Q)^DRbar
  2  2     1.11490860E-02   # Y_s(Q)^DRbar
  3  3     5.81915771E-01   # Y_b(Q)^DRbar
Block Ye Q=  3.79857210E+03  # (SUSY scale)
  1  1     1.02399844E-04   # Y_e(Q)^DRbar
  2  2     2.11730400E-02   # Y_mu(Q)^DRbar
  3  3     3.56095701E-01   # Y_tau(Q)^DRbar
Block Au Q=  3.79857210E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_u(Q)^DRbar
  2  2     0.00000000E+00   # A_c(Q)^DRbar
  3  3    -1.26569551E+03   # A_t(Q)^DRbar
Block Ad Q=  3.79857210E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_d(Q)^DRbar
  2  2     0.00000000E+00   # A_s(Q)^DRbar
  3  3    -1.44703729E+03   # A_b(Q)^DRbar
Block Ae Q=  3.79857210E+03  # (SUSY scale)
  1  1     0.00000000E+00   # A_e(Q)^DRbar
  2  2     0.00000000E+00   # A_mu(Q)^DRbar
  3  3    -1.86985139E+03   # A_tau(Q)^DRbar
Block MSOFT Q=  3.79857210E+03  # soft SUSY breaking masses at Q
   1    1.86362775E+02  # M_1
   2    2.78521102E+02  # M_2
   3    4.01472604E+03  # M_3
  21   -1.67225271E+05  # M^2_(H,d)
  22    1.55097787E+05  # M^2_(H,u)
  31    1.00000000E+04  # M_(L,11)
  32    1.00000000E+04  # M_(L,22)
  33    1.00000000E+04  # M_(L,33)
  34    1.00000000E+04  # M_(E,11)
  35    1.00000000E+04  # M_(E,22)
  36    1.00000000E+04  # M_(E,33)
  41    1.00000000E+04  # M_(Q,11)
  42    1.00000000E+04  # M_(Q,22)
  43    2.99308572E+03  # M_(Q,33)
  44    1.00000000E+04  # M_(U,11)
  45    1.00000000E+04  # M_(U,22)
  46    4.54420108E+03  # M_(U,33)
  47    1.00000000E+04  # M_(D,11)
  48    1.00000000E+04  # M_(D,22)
  49    2.99728557E+03  # M_(D,33)
Block MASS  # Mass spectrum
#   PDG code      mass          particle
         6     1.73200000E+02  # m_t(pole)
        23     9.11876000E+01  # m_Z(pole)
        24     8.04299946E+01  # W+
        15     1.77700000E+00  # m_tau(pole)
        25     1.23265185E+02  # h0
        35     2.29502514E+02  # H0
        36     2.29309766E+02  # A0
        37     2.51197029E+02  # H+
   1000001     1.00998207E+04  # ~d_L
   2000001     1.00748030E+04  # ~d_R
   1000002     1.00994493E+04  # ~u_L
   2000002     1.00782813E+04  # ~u_R
   1000003     1.00998239E+04  # ~s_L
   2000003     1.00748082E+04  # ~s_R
   1000004     1.00994524E+04  # ~c_L
   2000004     1.00782821E+04  # ~c_R
   1000005     3.12340588E+03  # ~b_1
   2000005     3.13899493E+03  # ~b_2
   1000006     3.12713576E+03  # ~t_1
   2000006     4.61417447E+03  # ~t_2
   1000011     1.00217186E+04  # ~e_L-
   2000011     1.00085804E+04  # ~e_R-
   1000012     1.00209437E+04  # ~nu_eL
   1000013     1.00217334E+04  # ~mu_L-
   2000013     1.00086081E+04  # ~mu_R-
   1000014     1.00209580E+04  # ~nu_muL
   1000015     1.00164046E+04  # ~tau_1-
   2000015     1.00260294E+04  # ~tau_2-
   1000016     1.00250563E+04  # ~nu_tauL
   1000021     4.47157251E+03  # ~g
   1000022     1.83735238E+02  # ~chi_10
   1000023     2.81940468E+02  # ~chi_20
   1000025     3.98008903E+02  # ~chi_30
   1000035     4.26119062E+02  # ~chi_40
   1000024     2.81355310E+02  # ~chi_1+
   1000037     4.26448114E+02  # ~chi_2+
# Higgs mixing
Block alpha # Effective Higgs mixing angle
          -4.60780508E-02   # alpha
Block Hmix Q=  3.79857210E+03  # Higgs mixing parameters
   1    3.83829115E+02  # mu
   2    3.48141489E+01  # tan[beta](Q)
   3    2.43014633E+02  # v(Q)
   4    5.25829688E+04  # m^2_A(Q)
Block stopmix # stop mixing matrix
   1  1     9.99855388E-01   # Re[R_st(1,1)]
   1  2     1.70059677E-02   # Re[R_st(1,2)]
   2  1    -1.70059677E-02   # Re[R_st(2,1)]
   2  2     9.99855388E-01   # Re[R_st(2,2)]
Block sbotmix # sbottom mixing matrix
   1  1     9.33313372E-01   # Re[R_sb(1,1)]
   1  2     3.59062877E-01   # Re[R_sb(1,2)]
   2  1    -3.59062877E-01   # Re[R_sb(2,1)]
   2  2     9.33313372E-01   # Re[R_sb(2,2)]
Block staumix # stau mixing matrix
   1  1     1.44162518E-01   # Re[R_sta(1,1)]
   1  2     9.89554025E-01   # Re[R_sta(1,2)]
   2  1    -9.89554025E-01   # Re[R_sta(2,1)]
   2  2     1.44162518E-01   # Re[R_sta(2,2)]
Block Nmix # neutralino mixing matrix
   1  1    -9.82980380E-01   # Re[N(1,1)]
   1  2     5.36814391E-02   # Re[N(1,2)]
   1  3    -1.57654407E-01   # Re[N(1,3)]
   1  4     7.75433043E-02   # Re[N(1,4)]
   2  1    -1.25426109E-01   # Re[N(2,1)]
   2  2    -8.91255325E-01   # Re[N(2,2)]
   2  3     3.52540836E-01   # Re[N(2,3)]
   2  4    -2.56217088E-01   # Re[N(2,4)]
   3  1    -5.16859902E-02   # Re[N(3,1)]
   3  2     7.95317680E-02   # Re[N(3,2)]
   3  3     6.98308786E-01   # Re[N(3,3)]
   3  4     7.09484387E-01   # Re[N(3,4)]
   4  1    -1.23880679E-01   # Re[N(4,1)]
   4  2     4.43234641E-01   # Re[N(4,2)]
   4  3     6.02681414E-01   # Re[N(4,3)]
   4  4    -6.51898569E-01   # Re[N(4,4)]
Block Umix # chargino mixing matrix
   1  1    -8.55116151E-01   # Re[U(1,1)]
   1  2     5.18436466E-01   # Re[U(1,2)]
   2  1     5.18436466E-01   # Re[U(2,1)]
   2  2     8.55116151E-01   # Re[U(2,2)]
Block Vmix # chargino mixing matrix
   1  1    -9.26143471E-01   # Re[V(1,1)]
   1  2     3.77171409E-01   # Re[V(1,2)]
   2  1     3.77171409E-01   # Re[V(2,1)]
   2  2     9.26143471E-01   # Re[V(2,2)]
DECAY        23     2.49520000E+00   # Z
DECAY        24     2.08500000E+00   # W
DECAY   2000011     5.02522433E+01   # ~e^-_R
#    BR                NDA      ID1      ID2
     9.66314401E-01    2     1000022        11   # BR(~e^-_R -> chi^0_1 e^-)
     1.57183889E-02    2     1000023        11   # BR(~e^-_R -> chi^0_2 e^-)
     2.66499149E-03    2     1000025        11   # BR(~e^-_R -> chi^0_3 e^-)
     1.53021768E-02    2     1000035        11   # BR(~e^-_R -> chi^0_4 e^-)
DECAY   1000011     1.44640247E+02   # ~e^-_L
#    BR                NDA      ID1      ID2
     6.77376183E-02    2     1000022        11   # BR(~e^-_L -> chi^0_1 e^-)
     2.79599053E-01    2     1000023        11   # BR(~e^-_L -> chi^0_2 e^-)
     8.19518916E-04    2     1000025        11   # BR(~e^-_L -> chi^0_3 e^-)
     4.32010171E-02    2     1000035        11   # BR(~e^-_L -> chi^0_4 e^-)
     4.45298824E-01    2    -1000024        12   # BR(~e^-_L -> chi^-_1 nu_e)
     1.63343969E-01    2    -1000037        12   # BR(~e^-_L -> chi^-_2 nu_e)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000013     5.04303555E+01   # ~mu^-_R
#    BR                NDA      ID1      ID2
     9.62948157E-01    2     1000022        13   # BR(~mu^-_R -> chi^0_1 mu^-)
     1.58825526E-02    2     1000023        13   # BR(~mu^-_R -> chi^0_2 mu^-)
     3.51596536E-03    2     1000025        13   # BR(~mu^-_R -> chi^0_3 mu^-)
     1.58887519E-02    2     1000035        13   # BR(~mu^-_R -> chi^0_4 mu^-)
     4.74988008E-04    2    -1000024        14   # BR(~mu^-_R -> chi^-_1 nu_mu)
     1.28958549E-03    2    -1000037        14   # BR(~mu^-_R -> chi^-_2 nu_mu)
DECAY   1000013     1.44729566E+02   # ~mu^-_L
#    BR                NDA      ID1      ID2
     6.77112543E-02    2     1000022        13   # BR(~mu^-_L -> chi^0_1 mu^-)
     2.79503540E-01    2     1000023        13   # BR(~mu^-_L -> chi^0_2 mu^-)
     1.11920502E-03    2     1000025        13   # BR(~mu^-_L -> chi^0_3 mu^-)
     4.33979248E-02    2     1000035        13   # BR(~mu^-_L -> chi^0_4 mu^-)
     4.45024671E-01    2    -1000024        14   # BR(~mu^-_L -> chi^-_1 nu_mu)
     1.63243405E-01    2    -1000037        14   # BR(~mu^-_L -> chi^-_2 nu_mu)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000015     1.02128792E+02   # ~tau^-_1
#    BR                NDA      ID1      ID2
     4.82596304E-01    2     1000022        15   # BR(~tau^-_1 -> chi^0_1 tau^-)
     7.35583242E-02    2     1000023        15   # BR(~tau^-_1 -> chi^0_2 tau^-)
     1.14622525E-01    2     1000025        15   # BR(~tau^-_1 -> chi^0_3 tau^-)
     6.96688828E-02    2     1000035        15   # BR(~tau^-_1 -> chi^0_4 tau^-)
     1.36481789E-01    2    -1000024        16   # BR(~tau^-_1 -> chi^-_1 nu_tau)
     1.23072176E-01    2    -1000037        16   # BR(~tau^-_1 -> chi^-_2 nu_tau)
DECAY   2000015     1.68460847E+02   # ~tau^-_2
#    BR                NDA      ID1      ID2
     6.15457191E-02    2     1000022        15   # BR(~tau^-_2 -> chi^0_1 tau^-)
     2.37486415E-01    2     1000023        15   # BR(~tau^-_2 -> chi^0_2 tau^-)
     7.79090066E-02    2     1000025        15   # BR(~tau^-_2 -> chi^0_3 tau^-)
     1.08080297E-01    2     1000035        15   # BR(~tau^-_2 -> chi^0_4 tau^-)
     3.39966854E-01    2    -1000024        16   # BR(~tau^-_2 -> chi^-_1 nu_tau)
     1.75011708E-01    2    -1000037        16   # BR(~tau^-_2 -> chi^-_2 nu_tau)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000012     1.44645228E+02   # ~nu_e
#    BR                NDA      ID1      ID2
     1.02091000E-01    2     1000022        12   # BR(~nu_e -> chi^0_1 nu_e)
     2.06841587E-01    2     1000023        12   # BR(~nu_e -> chi^0_2 nu_e)
     3.48953839E-03    2     1000025        12   # BR(~nu_e -> chi^0_3 nu_e)
     7.88456732E-02    2     1000035        12   # BR(~nu_e -> chi^0_4 nu_e)
     5.22287014E-01    2     1000024        11   # BR(~nu_e -> chi^+_1 e^-)
     8.64451872E-02    2     1000037        11   # BR(~nu_e -> chi^+_2 e^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000014     1.44734532E+02   # ~nu_mu
#    BR                NDA      ID1      ID2
     1.02028154E-01    2     1000022        14   # BR(~nu_mu -> chi^0_1 nu_mu)
     2.06714258E-01    2     1000023        14   # BR(~nu_mu -> chi^0_2 nu_mu)
     3.48739030E-03    2     1000025        14   # BR(~nu_mu -> chi^0_3 nu_mu)
     7.87971373E-02    2     1000035        14   # BR(~nu_mu -> chi^0_4 nu_mu)
     5.22131193E-01    2     1000024        13   # BR(~nu_mu -> chi^+_1 mu^-)
     8.68418671E-02    2     1000037        13   # BR(~nu_mu -> chi^+_2 mu^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000016     1.69917331E+02   # ~nu_tau
#    BR                NDA      ID1      ID2
     8.69425413E-02    2     1000022        16   # BR(~nu_tau -> chi^0_1 nu_tau)
     1.76150166E-01    2     1000023        16   # BR(~nu_tau -> chi^0_2 nu_tau)
     2.97176004E-03    2     1000025        16   # BR(~nu_tau -> chi^0_3 nu_tau)
     6.71465630E-02    2     1000035        16   # BR(~nu_tau -> chi^0_4 nu_tau)
     4.84727748E-01    2     1000024        15   # BR(~nu_tau -> chi^+_1 tau^-)
     1.82061222E-01    2     1000037        15   # BR(~nu_tau -> chi^+_2 tau^-)
#    BR                NDA      ID1      ID2       ID3
DECAY   2000001     5.18408087E+02   # ~d_R
#    BR                NDA      ID1      ID2
     1.04767766E-02    2     1000022         1   # BR(~d_R -> chi^0_1 d)
     1.70437159E-04    2     1000023         1   # BR(~d_R -> chi^0_2 d)
     1.65960539E-04    2     1000035         1   # BR(~d_R -> chi^0_4 d)
     9.89157734E-01    2     1000021         1   # BR(~d_R -> ~g d)
DECAY   1000001     6.49807851E+02   # ~d_L
#    BR                NDA      ID1      ID2
     3.57658946E-03    2     1000022         1   # BR(~d_L -> chi^0_1 d)
     5.15720852E-02    2     1000023         1   # BR(~d_L -> chi^0_2 d)
     5.37056938E-04    2     1000025         1   # BR(~d_L -> chi^0_3 d)
     1.47584704E-02    2     1000035         1   # BR(~d_L -> chi^0_4 d)
     9.98936229E-02    2    -1000024         2   # BR(~d_L -> chi^-_1 u)
     3.66440137E-02    2    -1000037         2   # BR(~d_L -> chi^-_2 u)
     7.93018161E-01    2     1000021         1   # BR(~d_L -> ~g d)
DECAY   2000003     5.18458154E+02   # ~s_R
#    BR                NDA      ID1      ID2
     1.04769604E-02    2     1000022         3   # BR(~s_R -> chi^0_1 s)
     1.76367423E-04    2     1000023         3   # BR(~s_R -> chi^0_2 s)
     1.83288731E-04    2     1000035         3   # BR(~s_R -> chi^0_4 s)
     9.89063223E-01    2     1000021         3   # BR(~s_R -> ~g s)
DECAY   1000003     6.49836684E+02   # ~s_L
#    BR                NDA      ID1      ID2
     3.57738380E-03    2     1000022         3   # BR(~s_L -> chi^0_1 s)
     5.15745682E-02    2     1000023         3   # BR(~s_L -> chi^0_2 s)
     5.55665600E-04    2     1000025         3   # BR(~s_L -> chi^0_3 s)
     1.47716941E-02    2     1000035         3   # BR(~s_L -> chi^0_4 s)
     9.98900115E-02    2    -1000024         4   # BR(~s_L -> chi^-_1 c)
     3.66472062E-02    2    -1000037         4   # BR(~s_L -> chi^-_2 c)
     7.92983471E-01    2     1000021         3   # BR(~s_L -> ~g s)
DECAY   1000005     1.12144275E+02   # ~b_1
#    BR                NDA      ID1      ID2
     2.13109055E-02    2     1000022         5   # BR(~b_1 -> chi^0_1 b)
     1.31323190E-01    2     1000023         5   # BR(~b_1 -> chi^0_2 b)
     8.19904002E-02    2     1000025         5   # BR(~b_1 -> chi^0_3 b)
     5.78873250E-02    2     1000035         5   # BR(~b_1 -> chi^0_4 b)
     2.87778401E-01    2    -1000024         6   # BR(~b_1 -> chi^-_1 t)
     4.19709779E-01    2    -1000037         6   # BR(~b_1 -> chi^-_2 t)
DECAY   2000005     5.30852736E+01   # ~b_2
#    BR                NDA      ID1      ID2
     1.96118791E-02    2     1000022         5   # BR(~b_2 -> chi^0_1 b)
     1.23862304E-02    2     1000023         5   # BR(~b_2 -> chi^0_2 b)
     2.04064841E-01    2     1000025         5   # BR(~b_2 -> chi^0_3 b)
     2.10719651E-01    2     1000035         5   # BR(~b_2 -> chi^0_4 b)
     2.87874845E-02    2    -1000024         6   # BR(~b_2 -> chi^-_1 t)
     5.24429914E-01    2    -1000037         6   # BR(~b_2 -> chi^-_2 t)
DECAY   2000002     5.35628211E+02   # ~u_R
#    BR                NDA      ID1      ID2
     4.05738271E-02    2     1000022         2   # BR(~u_R -> chi^0_1 u)
     6.59995362E-04    2     1000023         2   # BR(~u_R -> chi^0_2 u)
     1.11901275E-04    2     1000025         2   # BR(~u_R -> chi^0_3 u)
     6.42536829E-04    2     1000035         2   # BR(~u_R -> chi^0_4 u)
     9.58011739E-01    2     1000021         2   # BR(~u_R -> ~g u)
DECAY   1000002     6.49790256E+02   # ~u_L
#    BR                NDA      ID1      ID2
     1.00693411E-03    2     1000022         2   # BR(~u_L -> chi^0_1 u)
     5.70103271E-02    2     1000023         2   # BR(~u_L -> chi^0_2 u)
     3.37312515E-04    2     1000025         2   # BR(~u_L -> chi^0_3 u)
     1.20921832E-02    2     1000035         2   # BR(~u_L -> chi^0_4 u)
     1.17176340E-01    2     1000024         1   # BR(~u_L -> chi^+_1 d)
     1.93948711E-02    2     1000037         1   # BR(~u_L -> chi^+_2 d)
     7.92982032E-01    2     1000021         2   # BR(~u_L -> ~g u)
DECAY   2000004     5.35635519E+02   # ~c_R
#    BR                NDA      ID1      ID2
     4.05733156E-02    2     1000022         4   # BR(~c_R -> chi^0_1 c)
     6.60432112E-04    2     1000023         4   # BR(~c_R -> chi^0_2 c)
     1.15309697E-04    2     1000025         4   # BR(~c_R -> chi^0_3 c)
     6.45407025E-04    2     1000035         4   # BR(~c_R -> chi^0_4 c)
     9.57998762E-01    2     1000021         4   # BR(~c_R -> ~g c)
DECAY   1000004     6.49819045E+02   # ~c_L
#    BR                NDA      ID1      ID2
     1.00692356E-03    2     1000022         4   # BR(~c_L -> chi^0_1 c)
     5.70081824E-02    2     1000023         4   # BR(~c_L -> chi^0_2 c)
     3.40114043E-04    2     1000025         4   # BR(~c_L -> chi^0_3 c)
     1.20940320E-02    2     1000035         4   # BR(~c_L -> chi^0_4 c)
     1.17181468E-01    2     1000024         3   # BR(~c_L -> chi^+_1 s)
     1.94219466E-02    2     1000037         3   # BR(~c_L -> chi^+_2 s)
     7.92947334E-01    2     1000021         4   # BR(~c_L -> ~g c)
DECAY   1000006     1.23317646E+02   # ~t_1
#    BR                NDA      ID1      ID2
     5.10476021E-03    2     1000022         6   # BR(~t_1 -> chi^0_1 t)
     1.69294287E-03    2     1000023         4   # BR(~t_1 -> chi^0_2 c)
     1.24868375E-01    2     1000023         6   # BR(~t_1 -> chi^0_2 t)
     2.51446749E-01    2     1000025         6   # BR(~t_1 -> chi^0_3 t)
     2.28875031E-01    2     1000035         6   # BR(~t_1 -> chi^0_4 t)
     2.37442920E-01    2     1000024         5   # BR(~t_1 -> chi^+_1 b)
     1.47132986E-01    2     1000037         5   # BR(~t_1 -> chi^+_2 b)
#    BR                NDA      ID1      ID2       ID3
     3.40607834E-03    3     1000022        24         5   # BR(~t_1 -> chi^0_1 W^+ b)
DECAY   2000006     2.02710998E+02   # ~t_2
#    BR                NDA      ID1      ID2
     5.11899931E-02    2     1000022         6   # BR(~t_2 -> chi^0_1 t)
     2.95407013E-02    2     1000023         6   # BR(~t_2 -> chi^0_2 t)
     2.29491889E-01    2     1000025         6   # BR(~t_2 -> chi^0_3 t)
     1.97067900E-01    2     1000035         6   # BR(~t_2 -> chi^0_4 t)
     6.20999702E-02    2     1000024         5   # BR(~t_2 -> chi^+_1 b)
     3.95543013E-01    2     1000037         5   # BR(~t_2 -> chi^+_2 b)
     1.31504992E-02    2     1000005        24   # BR(~t_2 -> ~b_1 W^+)
     1.89720558E-03    2     2000005        24   # BR(~t_2 -> ~b_2 W^+)
     1.55138189E-03    2     1000005        37   # BR(~t_2 -> ~b_1 H^+)
     7.49066549E-03    2     1000006        23   # BR(~t_2 -> ~t_1 Z)
     7.03455448E-04    2     1000006        36   # BR(~t_2 -> ~t_1 A^0)
     9.65353201E-03    2     1000006        25   # BR(~t_2 -> ~t_1 h^0)
     6.15741036E-04    2     1000006        35   # BR(~t_2 -> ~t_1 H^0)
DECAY   1000024     1.02568100E-02   # chi^+_1
#    BR                NDA      ID1      ID2
     9.99949042E-01    2     1000022        24   # BR(chi^+_1 -> chi^0_1 W^+)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000037     1.18057666E+00   # chi^+_2
#    BR                NDA      ID1      ID2
     1.18410087E-01    2     1000022        24   # BR(chi^+_2 -> chi^0_1 W^+)
     4.39669754E-01    2     1000023        24   # BR(chi^+_2 -> chi^0_2 W^+)
     3.16868220E-01    2     1000024        23   # BR(chi^+_2 -> chi^+_1 Z)
     1.24307998E-01    2     1000024        25   # BR(chi^+_2 -> chi^+_1 h^0)
#    BR                NDA      ID1      ID2       ID3
     3.61703937E-04    3     1000024         5        -5   # BR(chi^+_2 -> chi^+_1 b b_bar)
     1.55659059E-04    3     1000022        -5         6   # BR(chi^+_2 -> chi^0_1 b_bar t)
     1.01001456E-04    3     1000022       -15        16   # BR(chi^+_2 -> chi^0_1 tau^+ nu_tau)
DECAY   1000022     0.00000000E+00   # chi^0_1
DECAY   1000023     1.77731905E-03   # chi^0_2
#    BR                NDA      ID1      ID2
     9.93605227E-01    2     1000022        23   # BR(chi^0_2 -> chi^0_1 Z)
#    BR                NDA      ID1      ID2       ID3
     5.63758133E-03    3     1000022         5        -5   # BR(chi^0_2 -> chi^0_1 b b_bar)
     6.84647497E-04    3     1000022        15       -15   # BR(chi^0_2 -> chi^0_1 tau^- tau^+)
DECAY   1000025     2.69730343E+00   # chi^0_3
#    BR                NDA      ID1      ID2
     2.82160464E-01    2     1000024       -24   # BR(chi^0_3 -> chi^+_1 W^-)
     2.82160464E-01    2    -1000024        24   # BR(chi^0_3 -> chi^-_1 W^+)
     1.25434297E-01    2     1000022        23   # BR(chi^0_3 -> chi^0_1 Z)
     2.42290136E-01    2     1000023        23   # BR(chi^0_3 -> chi^0_2 Z)
     6.69168978E-02    2     1000022        25   # BR(chi^0_3 -> chi^0_1 h^0)
     1.47639363E-04    2     1000023        22   # BR(chi^0_3 -> chi^0_2 photon)
#    BR                NDA      ID1      ID2       ID3
     7.67834765E-04    3     1000022         5        -5   # BR(chi^0_3 -> chi^0_1 b b_bar)
DECAY   1000035     1.29343539E+00   # chi^0_4
#    BR                NDA      ID1      ID2
     3.63035964E-01    2     1000024       -24   # BR(chi^0_4 -> chi^+_1 W^-)
     3.63035964E-01    2    -1000024        24   # BR(chi^0_4 -> chi^-_1 W^+)
     2.49463726E-02    2     1000022        23   # BR(chi^0_4 -> chi^0_1 Z)
     1.01551181E-02    2     1000023        23   # BR(chi^0_4 -> chi^0_2 Z)
     4.65497729E-04    2     1000022        36   # BR(chi^0_4 -> chi^0_1 A^0)
     1.19670492E-01    2     1000022        25   # BR(chi^0_4 -> chi^0_1 h^0)
     9.88000517E-02    2     1000023        25   # BR(chi^0_4 -> chi^0_2 h^0)
     1.97458932E-02    2     1000022        35   # BR(chi^0_4 -> chi^0_1 H^0)
#    BR                NDA      ID1      ID2       ID3
DECAY   1000021     1.04077020E+02   # ~g
#    BR                NDA      ID1      ID2
     3.06612821E-03    2     1000006        -4   # BR(~g -> ~t_1 c_bar)
     3.06612821E-03    2    -1000006         4   # BR(~g -> ~t^*_1 c)
     1.64200422E-01    2     1000006        -6   # BR(~g -> ~t_1 t_bar)
     1.64200422E-01    2    -1000006         6   # BR(~g -> ~t^*_1 t)
     1.66341593E-01    2     1000005        -5   # BR(~g -> ~b_1 b_bar)
     1.66341593E-01    2    -1000005         5   # BR(~g -> ~b^*_1 b)
     1.63989205E-01    2     2000005        -5   # BR(~g -> ~b_2 b_bar)
     1.63989205E-01    2    -2000005         5   # BR(~g -> ~b^*_2 b)
#    BR                NDA      ID1      ID2       ID3
     2.50848333E-04    3     1000022         6        -6   # BR(~g -> chi^0_1 t t_bar)
     1.46714216E-04    3     1000023         6        -6   # BR(~g -> chi^0_2 t t_bar)
     1.09597572E-03    3     1000025         6        -6   # BR(~g -> chi^0_3 t t_bar)
     9.50654688E-04    3     1000035         6        -6   # BR(~g -> chi^0_4 t t_bar)
     1.50664657E-04    3     1000024         5        -6   # BR(~g -> chi^+_1 b t_bar)
     1.50664657E-04    3    -1000024        -5         6   # BR(~g -> chi^-_1 b_bar t)
     9.45368290E-04    3     1000037         5        -6   # BR(~g -> chi^+_2 b t_bar)
     9.45368290E-04    3    -1000037        -5         6   # BR(~g -> chi^-_2 b_bar t)
DECAY        25     6.88760285E-03   # Gamma(h0)
     1.25716098E-03   2        22        22   # BR(h0 -> photon photon)
     7.22119185E-04   2        22        23   # BR(h0 -> photon Z)
     1.24924427E-02   2        23        23   # BR(h0 -> Z Z)
     1.06853921E-01   2       -24        24   # BR(h0 -> W W)
     3.68586603E-02   2        21        21   # BR(h0 -> gluon gluon)
     6.74383949E-09   2       -11        11   # BR(h0 -> Electron electron)
     2.99976710E-04   2       -13        13   # BR(h0 -> Muon muon)
     8.64850437E-02   2       -15        15   # BR(h0 -> Tau tau)
     6.87206782E-08   2        -2         2   # BR(h0 -> Up up)
     1.33370579E-02   2        -4         4   # BR(h0 -> Charm charm)
     7.78090394E-07   2        -1         1   # BR(h0 -> Down down)
     2.81425637E-04   2        -3         3   # BR(h0 -> Strange strange)
     7.41411338E-01   2        -5         5   # BR(h0 -> Bottom bottom)
DECAY        35     4.53552334E+00   # Gamma(HH)
     3.71426828E-07   2        22        22   # BR(HH -> photon photon)
     8.91000454E-08   2        22        23   # BR(HH -> photon Z)
     5.89032340E-05   2        23        23   # BR(HH -> Z Z)
     1.45877471E-04   2       -24        24   # BR(HH -> W W)
     5.17404895E-04   2        21        21   # BR(HH -> gluon gluon)
     9.23374690E-09   2       -11        11   # BR(HH -> Electron electron)
     4.10808688E-04   2       -13        13   # BR(HH -> Muon muon)
     1.18559887E-01   2       -15        15   # BR(HH -> Tau tau)
     3.26822941E-13   2        -2         2   # BR(HH -> Up up)
     6.33616473E-08   2        -4         4   # BR(HH -> Charm charm)
     9.51651389E-07   2        -1         1   # BR(HH -> Down down)
     3.44215982E-04   2        -3         3   # BR(HH -> Strange strange)
     8.79961418E-01   2        -5         5   # BR(HH -> Bottom bottom)
DECAY        36     4.54054416E+00   # Gamma(A0)
     1.66315153E-07   2        22        22   # BR(A0 -> photon photon)
     9.96732238E-09   2        22        23   # BR(A0 -> photon Z)
     5.03644370E-04   2        21        21   # BR(A0 -> gluon gluon)
     9.22963043E-09   2       -11        11   # BR(A0 -> Electron electron)
     4.10625793E-04   2       -13        13   # BR(A0 -> Muon muon)
     1.18535715E-01   2       -15        15   # BR(A0 -> Tau tau)
     1.04031250E-13   2        -2         2   # BR(A0 -> Up up)
     2.01927556E-08   2        -4         4   # BR(A0 -> Charm charm)
     9.51360283E-07   2        -1         1   # BR(A0 -> Down down)
     3.44110741E-04   2        -3         3   # BR(A0 -> Strange strange)
     8.80192200E-01   2        -5         5   # BR(A0 -> Bottom bottom)
     1.25470063E-05   2        23        25   # BR(A0 -> Z h0)
DECAY        37     1.94796388E+00   # Gamma(Hp)
     2.75793264E-08   2       -11        12   # BR(Hp -> Electron nu_e)
     1.17910193E-03   2       -13        14   # BR(Hp -> Muon nu_mu)
     3.33484045E-01   2       -15        16   # BR(Hp -> Tau nu_tau)
     2.45613406E-06   2        -1         2   # BR(Hp -> Down up)
     3.99957556E-05   2        -3         2   # BR(Hp -> Strange up)
     2.44527739E-05   2        -5         2   # BR(Hp -> Bottom up)
     1.13839152E-07   2        -1         4   # BR(Hp -> Down charm)
     8.85213353E-04   2        -3         4   # BR(Hp -> Strange charm)
     3.42408920E-03   2        -5         4   # BR(Hp -> Bottom charm)
     9.68077650E-08   2        -1         6   # BR(Hp -> Down top)
     2.45929811E-06   2        -3         6   # BR(Hp -> Strange top)
     6.60780461E-01   2        -5         6   # BR(Hp -> Bottom top)
     1.71212492E-04   2        24        25   # BR(Hp -> W h0)
     3.06826001E-06   2        24        35   # BR(Hp -> W HH)
     3.20668658E-06   2        24        36   # BR(Hp -> W A0)
DECAY         6     2.43000000E+00   # top
#    BR                NDA      ID1      ID2
     1.00000000E+00    2           5        24   # BR(t -> b W)
Block HiggsBoundsInputHiggsCouplingsFermions
# ScalarNormEffCoupSq PseudoSNormEffCoupSq NP IP1 IP2 IP2
    2.57365632E+00    0.00000000E+00        3  25   5   5  # h0-b-b eff. coupling^2, normalised to SM
    1.21045131E+03    0.00000000E+00        3  35   5   5  # H0-b-b eff. coupling^2, normalised to SM
    0.00000000E+00    1.21202496E+03        3  36   5   5  # A0-b-b eff. coupling^2, normalised to SM
#
    9.98701630E-01    0.00000000E+00        3  25   6   6  # h0-t-t eff. coupling^2, normalised to SM
    2.12343508E-03    0.00000000E+00        3  35   6   6  # H0-t-t eff. coupling^2, normalised to SM
    0.00000000E+00    8.25065514E-04        3  36   6   6  # A0-t-t eff. coupling^2, normalised to SM
#
    2.57365632E+00    0.00000000E+00        3  25  15  15  # h0-tau-tau eff. coupling^2, normalised to SM
    1.21045131E+03    0.00000000E+00        3  35  15  15  # H0-tau-tau eff. coupling^2, normalised to SM
    0.00000000E+00    1.21202496E+03        3  36  15  15  # A0-tau-tau eff. coupling^2, normalised to SM
#
Block HiggsBoundsInputHiggsCouplingsBosons
    9.99698591E-01        3  25  24  24  # h0-W-W eff. coupling^2, normalised to SM
    3.01408512E-04        3  35  24  24  # H0-W-W eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  24  24  # A0-W-W eff. coupling^2, normalised to SM
#
    9.99698591E-01        3  25  23  23  # h0-Z-Z eff. coupling^2, normalised to SM
    3.01408512E-04        3  35  23  23  # H0-Z-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  23  23  # A0-Z-Z eff. coupling^2, normalised to SM
#
    1.19088675E+00        3  25  21  21  # h0-g-g eff. coupling^2, normalised to SM
    1.33755285E+00        3  35  21  21  # H0-g-g eff. coupling^2, normalised to SM
    7.09763283E-01        3  36  21  21  # A0-g-g eff. coupling^2, normalised to SM
#
    0.00000000E+00        3  25  25  23  # h0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  25  23  # H0-h0-Z eff. coupling^2, normalised to SM
    3.01408512E-04        3  36  25  23  # A0-h0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  35  35  23  # H0-H0-Z eff. coupling^2, normalised to SM
    9.99698591E-01        3  36  35  23  # A0-H0-Z eff. coupling^2, normalised to SM
    0.00000000E+00        3  36  36  23  # A0-A0-Z eff. coupling^2, normalised to SM
#
    0.00000000E+00        4  25  21  21  23  # h0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  35  21  21  23  # H0-g-g-Z eff. coupling^2, normalised to SM
    0.00000000E+00        4  36  21  21  23  # A0-g-g-Z eff. coupling^2, normalised to SM
Block SPhenoLowEnergy  # low energy observables
    1    5.06958573E-04   # BR(b -> s gamma)
    2    1.60623848E-06   # BR(b -> s mu+ mu-)
    3    3.52576851E-05   # BR(b -> s nu nu)
    4    2.92647811E-15   # BR(Bd -> e+ e-)
    5    1.25014767E-10   # BR(Bd -> mu+ mu-)
    6    2.61092058E-08   # BR(Bd -> tau+ tau-)
    7    9.72501610E-14   # BR(Bs -> e+ e-)
    8    4.15449623E-09   # BR(Bs -> mu+ mu-)
    9    8.79581400E-07   # BR(Bs -> tau+ tau-)
   10    1.53838129E-05   # BR(B_u -> tau nu)
   11    1.58908897E-01   # BR(B_u -> tau nu)/BR(B_u -> tau nu)_SM
   12    5.41493628E-01   # |Delta(M_Bd)| [ps^-1] 
   13    1.90691898E+01   # |Delta(M_Bs)| [ps^-1] 
   14    0.00000000E+00   # neutron EDM according to the chiral quark model
   15    0.00000000E+00   # neutron EDM according to the relativistic quark-parton model
   16    2.15759780E-03   # epsilon_K
   17    2.28180363E-15   # Delta(M_K)
   18    2.48011428E-11   # BR(K^0 -> pi^0 nu nu)
   19    8.29078444E-11   # BR(K^+ -> pi^+ nu nu)
   20    1.93069092E-16   # Delta(g-2)_electron/2
   21    8.25436280E-12   # Delta(g-2)_muon/2
   22    2.33952962E-09   # Delta(g-2)_tau/2
   23    0.00000000E+00   # electric dipole moment of the electron
   24    0.00000000E+00   # electric dipole moment of the muon
   25    0.00000000E+00   # electric dipole moment of the tau
   26    0.00000000E+00   # Br(mu -> e gamma)
   27    0.00000000E+00   # Br(tau -> e gamma)
   28    0.00000000E+00   # Br(tau -> mu gamma)
   29    0.00000000E+00   # Br(mu -> 3 e)
   30    0.00000000E+00   # Br(tau -> 3 e)
   31    0.00000000E+00   # Br(tau -> 3 mu)
   39    7.27209160E-05   # Delta(rho_parameter)
   40    0.00000000E+00   # BR(Z -> e mu)
   41    0.00000000E+00   # BR(Z -> e tau)
   42    0.00000000E+00   # BR(Z -> mu tau)
Block FWCOEF Q=  1.60000000E+02  # Wilson coefficients at scale Q
#    id        order  M        value         comment
     0305 4422   00   0    -1.88819110E-01   # C7
     0305 4422   00   2    -3.63821659E-01   # C7
     0305 4322   00   2    -3.38698957E-03   # C7'
     0305 6421   00   0    -9.52278348E-02   # C8
     0305 6421   00   2    -2.62821391E-01   # C8
     0305 6321   00   2    -3.18222572E-03   # C8'
 03051111 4133   00   0     1.62505003E+00   # C9 e+e-
 03051111 4133   00   2     1.62537664E+00   # C9 e+e-
 03051111 4233   00   2    -4.79333020E-04   # C9' e+e-
 03051111 4137   00   0    -4.44774213E+00   # C10 e+e-
 03051111 4137   00   2    -4.44642297E+00   # C10 e+e-
 03051111 4237   00   2     3.55961414E-03   # C10' e+e-
 03051313 4133   00   0     1.62505003E+00   # C9 mu+mu-
 03051313 4133   00   2     1.62537656E+00   # C9 mu+mu-
 03051313 4233   00   2    -4.80019190E-04   # C9' mu+mu-
 03051313 4137   00   0    -4.44774213E+00   # C10 mu+mu-
 03051313 4137   00   2    -4.44642305E+00   # C10 mu+mu-
 03051313 4237   00   2     3.56030184E-03   # C10' mu+mu-
 03051212 4137   00   0     1.50535012E+00   # C11 nu_1 nu_1
 03051212 4137   00   2     1.50506662E+00   # C11 nu_1 nu_1
 03051212 4237   00   2    -7.70007066E-04   # C11' nu_1 nu_1
 03051414 4137   00   0     1.50535012E+00   # C11 nu_2 nu_2
 03051414 4137   00   2     1.50506662E+00   # C11 nu_2 nu_2
 03051414 4237   00   2    -7.69858258E-04   # C11' nu_2 nu_2
 03051616 4137   00   0     1.50535012E+00   # C11 nu_3 nu_3
 03051616 4137   00   2     1.50506662E+00   # C11 nu_3 nu_3
 03051616 4237   00   2    -7.27923188E-04   # C11' nu_3 nu_3
Block IMFWCOEF Q=  1.60000000E+02  # Im(Wilson coefficients) at scale Q
#    id        order  M        value         comment
     0305 4422   00   0     3.85823893E-07   # C7
     0305 4422   00   2     1.03409602E-05   # C7
     0305 4322   00   2     7.41008938E-07   # C7'
     0305 6421   00   0     3.30483680E-07   # C8
     0305 6421   00   2    -4.18489904E-06   # C8
     0305 6321   00   2     9.58557450E-08   # C8'
 03051111 4133   00   2     1.06127072E-06   # C9 e+e-
 03051111 4233   00   2     6.13170610E-06   # C9' e+e-
 03051111 4137   00   2     3.87965087E-07   # C10 e+e-
 03051111 4237   00   2    -4.54767519E-05   # C10' e+e-
 03051313 4133   00   2     1.06126737E-06   # C9 mu+mu-
 03051313 4233   00   2     6.13170480E-06   # C9' mu+mu-
 03051313 4137   00   2     3.87968576E-07   # C10 mu+mu-
 03051313 4237   00   2    -4.54767558E-05   # C10' mu+mu-
 03051212 4137   00   2    -5.07316362E-08   # C11 nu_1 nu_1
 03051212 4237   00   2     9.83739421E-06   # C11' nu_1 nu_1
 03051414 4137   00   2    -5.07314596E-08   # C11 nu_2 nu_2
 03051414 4237   00   2     9.83739421E-06   # C11' nu_2 nu_2
 03051616 4137   00   2    -5.06813935E-08   # C11 nu_3 nu_3
 03051616 4237   00   2     9.83739419E-06   # C11' nu_3 nu_3
