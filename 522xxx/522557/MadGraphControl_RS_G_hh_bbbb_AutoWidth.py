import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment
from MadGraphControl.MadGraphUtils import *
import re

#Getting the Physics short, as while running in release 21, need to pass the directory itself##
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()

mh  = 125

#Getting Graviton Mass from JO name
parameter_re = re.compile('RS_G_hh_bbbb_AutoWidth_c([0-9]+)_M([0-9]+)')
c_str, mhh = parameter_re.search(jofile).groups()
c = float(c_str) * 0.1


params={'frblock' : {'c':str(c)},
        'mass' : {'Mh':str(mh), 'Mhh':str(mhh)},
        'decay': {'Whh':'auto'} }

nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

#Change defaults for run_card.dat
extras = { 'lhe_version':'2.0',
           'cut_decays':'F',
           'nevents':int(nevents)}

process = """
set group_subprocesses Auto
set ignore_six_quark_processes False
set gauge unitary
set complex_mass_scheme False
import model SMRS_Decay
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~

generate p p > hh, (hh > h h)
output -f
"""

# second process to generate hh -> cc cc events using parameters from first process
process_dir = new_process(process)
modify_run_card(process_dir=process_dir,runArgs=runArgs, settings=extras)
modify_param_card(process_dir=process_dir,params=params)
generate(process_dir=process_dir,runArgs=runArgs)

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py") 

genSeq.Pythia8.Commands += ["25:oneChannel = on 1.0 100 5 -5 "] #bb final state

# MC21 metadata
evgenConfig.contact = ["valentina.vecchio@cern.ch"]
evgenConfig.description = "Bulk Randall-Sundrum model KK graviton -> hh -> bbbb with NNPDF2.3 LO A14 tune"
evgenConfig.keywords = ["exotic", "RandallSundrum", "warpedED", "graviton", "Higgs"]
evgenConfig.process = "RS_G_hh_bbbb"
