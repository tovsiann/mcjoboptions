import MadGraphControl.MadGraphUtils
from MadGraphControl.MadGraphUtils import *
import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment

# Some includes that are necessary to interface MadGraph with Pythia
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")
include("Pythia8_i/Pythia8_EvtGen.py")

evgenConfig.description     = "ttHH"
evgenConfig.keywords        = ['SM', 'top', 'ttVV']
evgenConfig.contact         = ["lfaldaul@cern.ch"]
evgenConfig.generators      = ['aMcAtNlo', 'Pythia8']
evgenConfig.nEventsPerJob   = 50000

# Number of events to produce
safety = 3 # safety factor to account for filter efficiency
if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:  nevents = int(runArgs.maxEvents*safety)
else: nevents = int(evgenConfig.nEventsPerJob*safety)

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

process="""
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define w = w+ w-
generate p p > t t~ h h 
output -f"""

extras = {
           'nevents':int(nevents),
	}

process_dir = new_process(process)

modify_run_card(process_dir=process_dir,
               settings=extras, runArgs=runArgs)

generate(process_dir=process_dir, runArgs=runArgs)
arrange_output(process_dir=process_dir, runArgs=runArgs,lhe_version=3,saveProcDir=True)
 
genSeq.Pythia8.Commands += [ "25:onMode = off","25:onIfAny = 5" ] # bb decay

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
include('GeneratorFilters/xAODTTbarWToLeptonFilter_Common.py')

# semi leptonic channel
filtSeq.xAODTTbarWToLeptonFilter.Ptcut = 0.
filtSeq.xAODTTbarWToLeptonFilter.NumLeptons = 1 