from MadGraphControl.MadGraphUtils import *

evgenConfig.nEventsPerJob = 20000
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]

evgenConfig.keywords = ['SM', 'diboson', 'VBS', 'WW', 'sameSign', 'electroweak', '2lepton', '2jet']
evgenConfig.contact = ['karolos.potamianos@cern.ch']

physShort = get_physics_short()

gridpack_dir='madevent/'

if physShort.find("lvlvjj_ss_EW6_LSMT_T3_1_int") != -1:
    runName = 'lvlvjj_ss_EW6_LSMT_T3_1_int'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 T3^2==1
add process p p > l- vl~ l- vl~ j j QCD=0 T3^2==1
"""
elif physShort.find("lvlvjj_ss_EW6_LSMT_T3_1_quad") != -1:
    runName = 'lvlvjj_ss_EW6_LSMT_T3_1_quad'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 T3^2==2
add process p p > l- vl~ l- vl~ j j QCD=0 T3^2==2
"""
elif physShort.find("lvlvjj_ss_EW6_LSMT_T0_0p60_T3_1_int") != -1:
    runName = 'lvlvjj_ss_EW6_LSMT_T0_0p60_T3_1_int'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 T0^2==1 T3^2==1
add process p p > l- vl~ l- vl~ j j QCD=0 T0^2==1 T3^2==1
"""
elif physShort.find("lvlvjj_ss_EW6_LSMT_T1_0p30_T3_1_int") != -1:
    runName = 'lvlvjj_ss_EW6_LSMT_T1_0p30_T3_1_int'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 T1^2==1 T3^2==1
add process p p > l- vl~ l- vl~ j j QCD=0 T1^2==1 T3^2==1
"""
elif physShort.find("lvlvjj_ss_EW6_LSMT_T2_1_T3_1_int") != -1:
    runName = 'lvlvjj_ss_EW6_LSMT_T2_1_T3_1_int'
    description = 'MadGraph_' + runName
    mgproc = """
generate p p > l+ vl l+ vl j j QCD=0 T2^2==1 T3^2==1
add process p p > l- vl~ l- vl~ j j QCD=0 T2^2==1 T3^2==1
"""
else:
    raise RuntimeError(
        "physShort %i not recognised in these jobOptions." % physShort)

evgenConfig.description = description

# ---------------------------------------------------------------------------
# write MG5 Proc card
# ---------------------------------------------------------------------------
#fcard = open('proc_card_mg5.dat', 'w')
#fcard.write("""

## https://feynrules.irmp.ucl.ac.be/attachment/wiki/AnomalousGaugeCoupling/quartic21v2.tgz

import os
##os.system("wget -O- https://feynrules.irmp.ucl.ac.be/raw-attachment/wiki/AnomalousGaugeCoupling/quartic21v2.tgz | tar xzf -")
# Alternative becuase wget doesn't work
#import urllib
#urllib.urlretrieve("https://feynrules.irmp.ucl.ac.be/raw-attachment/wiki/AnomalousGaugeCoupling/quartic21v2.tgz", filename="quartic21v2.tgz")
#os.system("tar xzf quartic21v2.tgz")

process = """
import model QAll_5_Aug21v2
define l+ = e+ mu+ ta+
define  vl = ve vm vt
define l- = e- mu- ta-
define vl~ = ve~ vm~ vt~
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
""" + mgproc + """
output -f
"""
#""")
#fcard.close()

# ----------------------------------------------------------------------------
# Random Seed
# ----------------------------------------------------------------------------
randomSeed = 0
if hasattr(runArgs, 'randomSeed'):
    randomSeed = runArgs.randomSeed

# ----------------------------------------------------------------------------
# Beam energy
# ----------------------------------------------------------------------------
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

# ---------------------------------------------------------------------------
# Number of Events
# ---------------------------------------------------------------------------
safefactor = 1.1
if hasattr(runArgs, 'maxEvents') and runArgs.maxEvents > 0:
    nevents = int(int(runArgs.maxEvents) * safefactor)
else:
    nevents = int(2000 * safefactor)

#Fetch default LO run_card.dat and set parameters
extras = {
    'gridpack': '.true.',
    'nevents': nevents,
    'pdlabel': "'lhapdf'", 
    'lhaid': 260000, 
    'dynamical_scale_choice': 0, 
    'maxjetflavor': 5, 
    'asrwgtflavor': 5, 
    'auto_ptj_mjj': False, 
    'cut_decays': True, 
    'ptl': 4.0, 
    'ptj': 15.0, 
    'ptb': 15.0, 
    'drbb': 0.2, 
    'etal': 3.0, 
    'etaj': 5.5, 
    'etab': 5.5, 
    'mmll': 0.0, 
    'dral': 0.1, 
    'drbj': 0.2, 
    'drll': 0.2, 
    'drbl': 0.2, 
    'drjl': 0.2, 
    'drjj': 0.2,
    'use_syst': True, 
    'systematics_program': 'systematics',  
    'systematics_arguments': "['--mur=0.5,1,2', '--muf=0.5,1,2', '--dyn=-1,1,2,3,4', '--pdf=errorset,NNPDF30_nlo_as_0119@0,NNPDF30_nlo_as_0117@0,CT14nlo@0,MMHT2014nlo68clas118@0,PDF4LHC15_nlo_30_pdfas']"}

process_dir = new_process(process)
if not is_gen_from_gridpack(): # When generating the gridpack
    import os
    os.system("cp setscales_lo.f  "+process_dir+"/SubProcesses/setscales.f")

#modify_config_card(process_dir=process_dir,settings={'cluster_type':'condor'})

#build_run_card(run_card_old=get_default_runcard(proc_dir=process_dir),
#               run_card_new='run_card.dat', xqcut=0,
#               nevts=nevents,rand_seed=runArgs.randomSeed,beamEnergy=beamEnergy)
#modify_run_card(run_card='run_card.dat',
#                run_card_backup='run_card_backup.dat',
#                settings=extras)
modify_run_card(runArgs=runArgs,process_dir=process_dir,settings=extras)

param_card_name = 'param_card_' + physShort + '.dat' 
modify_param_card(param_card_input=param_card_name,output_location=process_dir+"/Cards/param_card.dat")
print_cards()

#generate(required_accuracy=0.001,run_card_loc='run_card.dat',
#         param_card_loc=param_card_name,mode=0,njobs=1,cluster_type=None,
#         cluster_queue=None,proc_dir=process_dir,run_name=runName,grid_pack=True,
#         gridpack_compile=False,gridpack_dir=gridpack_dir,
#         nevents=nevents,random_seed=runArgs.randomSeed)
required_accuracy=0.001
gridpack_mode = True
generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs,required_accuracy=required_accuracy)

# replaced output_suffix+'._00001.events.tar.gz' with runArgs.outputTXTFile
#arrange_output(run_name=runName,proc_dir=process_dir,
#               outputDS=runArgs.outputTXTFile,lhe_version=3,saveProcDir=True)
arrange_output(runArgs=runArgs,process_dir=process_dir,lhe_version=3,saveProcDir=True)

## Provide config information for Pythia8
#runArgs.inputGeneratorFile=runArgs.outputTXTFile

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

# fix for VBS processes (requires version>=8.230)
genSeq.Pythia8.Commands += ["SpaceShower:pTmaxMatch=1"]
genSeq.Pythia8.Commands += ["SpaceShower:pTmaxFudge=1"]
genSeq.Pythia8.Commands += ["SpaceShower:MEcorrections=off"]
genSeq.Pythia8.Commands += ["TimeShower:pTmaxMatch=1"]
genSeq.Pythia8.Commands += ["TimeShower:pTmaxFudge=1"]
genSeq.Pythia8.Commands += ["TimeShower:MEcorrections=off"]
genSeq.Pythia8.Commands += ["TimeShower:globalRecoil=on"]
genSeq.Pythia8.Commands += ["TimeShower:limitPTmaxGlobal=on"]
genSeq.Pythia8.Commands += ["TimeShower:nMaxGlobalRecoil=1"]
genSeq.Pythia8.Commands += ["TimeShower:globalRecoilMode=2"]
genSeq.Pythia8.Commands += ["TimeShower:nMaxGlobalBranch=1"]
genSeq.Pythia8.Commands += ["TimeShower:weightGluonToQuark=1"]
genSeq.Pythia8.Commands += ["Check:epTolErr=1e-2"]
genSeq.Pythia8.Commands += ["SpaceShower:dipoleRecoil = on"]

include("Pythia8_i/Pythia8_MadGraph.py")
