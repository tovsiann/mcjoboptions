import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

beamEnergy=-999
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else:
    raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------------------------------
# Due to the low filter efficiency, the number of generated events are set to safefactor times maxEvents,
# to avoid crashing due to not having enough events
# Also putting protection to avoid from crashing when maxEvents=-1
#---------------------------------------------------------------------------------------------------                                                                  
safefactor=10   # To be consistent with the Powerheg job
nevents=5000*safefactor
if runArgs.maxEvents > 0:
    nevents=runArgs.maxEvents*safefactor

mode=0

#---------------------------------------------------------------------------------------------------
# Setting parameters for param_card.dat
#---------------------------------------------------------------------------------------------------
parameters = {}


#---------------------------------------------------------------------------------------------------
# Setting higgs mass to 125 GeV for param_card.dat
# Setting heavy higgs mass and width (width = 40 MeV) for param_card.dat
#---------------------------------------------------------------------------------------------------
parameters['MASS']={'25':'1.250000e+02', 
                    '35': '1.600000e+03' }  
parameters['DECAY']={'35': '4.000000e-02' }


#---------------------------------------------------------------------------------------------------
# Setting some parameters for run_card.dat
#---------------------------------------------------------------------------------------------------
extras = { 'lhe_version':'2.0', 
           'cut_decays':'F', 
           'scale':'125',
           'dsqrt_q2fact1':'125',
           'dsqrt_q2fact2':'125',
           'fixed_ren_scale':'F',
           'fixed_fac_scale':'F',
           'nevents':int(nevents)}

#---------------------------------------------------------------------------------------------------
# Generating resonant VBF-Only HH process with MadGraph
# Parameters are set above
#---------------------------------------------------------------------------------------------------
process="""
import model sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
import model /cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/2HDMtypeII
generate p p > h2 > h1 h1 j j $$ z w+ w- / a j QED=4
output -f"""

process_dir = new_process(process)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the run_card
# https://gitlab.cern.ch/atlas/athena/-/tree/master/Generators/MadGraphControl
# Build a new run_card.dat from an existing one
# Using the values given in "extras" above for the selected parameters when setting up the run_card
# If not set in "extras", default values are used 
#---------------------------------------------------------------------------------------------------
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------------------------------
# Using the helper function from MadGraphControl for setting up the param_card
# https://gitlab.cern.ch/atlas/athena/-/tree/master/Generators/MadGraphControl
# Build a new param_card.dat from an existing one
# Higgs mass is set to 125 GeV by "higgsMass"
#---------------------------------------------------------------------------------------------------
modify_param_card(process_dir=process_dir,params=parameters)

#---------------------------------------------------------------------------------------------------
# Printing cards   
#---------------------------------------------------------------------------------------------------
print_cards()

#---------------------------------------------------------------------------------------------------
# Generate events                                                               
#---------------------------------------------------------------------------------------------------     
generate(process_dir=process_dir,runArgs=runArgs)

#---------------------------------------------------------------------------------------------------
# Move output files into the appropriate place, with the appropriate name
#---------------------------------------------------------------------------------------------------   
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=2,saveProcDir=True)

#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators = ["MadGraph", "Herwig7"]
evgenConfig.description = "Resonant di-Higgs production through vector-boson-fusion (VBF) which decays to bbbb."
evgenConfig.keywords = ["hh", "heavyBoson", "VBFHiggs", "bottom"]
evgenConfig.contact = ['Katharina Behr <katharina.behr@cern.ch>', 'Marcus V. G. Rodrigues <marcus.rodrigues@cern.ch>']

evgenConfig.nEventsPerJob = 10000
evgenConfig.tune = "H7.1-Default"

#--------------------------------------------------------------
# Herwig7 (H7UE) showering
#--------------------------------------------------------------
include("Herwig7_i/Herwig72_LHEF.py")

# configure Herwig7
Herwig7Config.add_commands("set /Herwig/Partons/RemnantDecayer:AllowTop Yes")
Herwig7Config.me_pdf_commands(order="NLO", name="NNPDF30_nlo_as_0118")
Herwig7Config.tune_commands()
Herwig7Config.lhef_mg5amc_commands(lhe_filename="tmp_LHE_events.events", me_pdf_order="NLO")

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

#HW7 settings and Higgs BR
Herwig7Config.add_commands("""
set /Herwig/Shower/ShowerHandler:SpinCorrelations Yes
cd /Herwig/Particles
create /ThePEG/ParticleData XH
setup XH 35  XH MASS WIDTH MAX_WIDTH 0 0 0 1 0
set /Herwig/EventHandlers/LHEReader:Decayer /Herwig/Decays/Mambo
set /Herwig/Particles/h0/h0->b,bbar;:BranchingRatio      1.0
do /Herwig/Particles/h0:SelectDecayModes h0->b,bbar; h0->b,bbar;
do /Herwig/Particles/h0:PrintDecayModes
""")

# run Herwig7
Herwig7Config.run()

