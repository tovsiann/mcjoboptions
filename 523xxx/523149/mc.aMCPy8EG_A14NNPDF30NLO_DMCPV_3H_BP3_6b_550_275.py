get_param_card = subprocess.Popen(['get_files', '-data', 'aMcAtNlo_param_card_DMCPV_HHH.dat'])
if get_param_card.wait():
        print ("Could not get hold of aMcAtNlo_param_card_DMCPV_HHH.dat, exiting...")
        sys.exit(2)
get_run_card = subprocess.Popen(['get_files', '-data', 'aMcAtNlo_run_card_DMCPV_HHH.dat'])
if get_run_card.wait():
        print ("Could not get hold of aMcAtNlo_run_card_DMCPV_HHH.dat, exiting...")
        sys.exit(2)

include('MadGraphControl_DMCPV_3H_BP3_6b_550_275.py')
