#! /usr/bin/env python

import os,sys,time,subprocess,shutil,glob
from AthenaCommon import Logging
mglog = Logging.logging.getLogger('ConfigureHVT')

import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

gridpack_mode=False
#----------------------------------------------------------------------------
# Parse the production and decay modes
#----------------------------------------------------------------------------
## get the top JO name
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
shortname = get_physics_short().split('_')

# decode dataset short name, should be of the form MadGraphPythia8EvtGen_AU14NNPDF23LO_HVT_Agv1_VzWW_qqqq_m1000 (split by '_')
scenario=shortname[3]
HVTboson=shortname[4][:2]
HVTdecay=shortname[4][2:]
VVdecay=shortname[5]
HVTmass=int(shortname[6][1:])
# check if VBF
vbfProd = ( 'VBF' in shortname[2] )
# check if VcXH
isXH = ( 'XH' in shortname[4] )
if len(shortname) > 7: Xmass=int(shortname[7][1:])

gridpack_mode=False
if vbfProd:
  gridpack_mode=True

# -----------------------------------------------------------------------
# proc card and process generation
#
# first, figure out what process we should run.
# Note that the syntax 'p p > vz, (vz > z h)' means that the vz is always
# on shell, with mass within some multiple of the width of the vz (usually 15, see 
# bwcutoff in the run card).  If we want to allow the low-mass tail as well as the
# peak, then generate 'p p > vz > z h' instead.
processes=[]
addHbb=False
addHcc=False
# If we've got Higgs, and we only want H->bb or H->bb/cc, tell Pythia:
pythiachans=[]

VBFString = ""
if vbfProd:
    mglog.info("Setting up for VBF HVT production")
    VBFString = "j j QCD=0"

if HVTdecay == "Zh" or HVTdecay == "ZH" or HVTdecay == "zh" or HVTdecay == "zH":
    addHbb=True
    if VVdecay == "lljj" or VVdecay == "llqq":
        processes.append("p p > vz, (vz > z h, z > l+ l-)")
        addHcc=True
    elif VVdecay == "jjjj" or VVdecay == "qqqq":
        processes.append("p p > vz, (vz > z h, z > jprime jprime)") 
        addHcc=True
    elif VVdecay == "vvqq" or VVdecay == "vvjj":
        processes.append("p p > vz, (vz > z h, z > vl vl~)")
        addHcc=True
    elif VVdecay == "llbb" or VVdecay == "llbb":
        processes.append("p p > vz, (vz > z h, z > l+ l-)")
    elif VVdecay == "jjbb" or VVdecay == "qqbb":
        processes.append("p p > vz, (vz > z h, z > jprime jprime)")
    elif VVdecay == "vvbb" or VVdecay == "vvbb":
        processes.append("p p > vz, (vz > z h, z > vl vl~)")
    elif VVdecay == "all":
        processes.append("p p > vz, (vz > z h)")
        addHbb=False
        addHcc=False
    else:
        raise RuntimeError('Could not configure process for %s > %s' % (HVTdecay, VVdecay))
elif HVTdecay == "WW" or HVTdecay == "ww":
    if VVdecay == "lvlv" or VVdecay == "llvv":
        processes.append("p p > vz, (vz > w+ w-, w+ > l+ vl, w- > l- vl~)")
    elif VVdecay == "jjjj" or VVdecay == "qqqq":
        processes.append("p p > vz, (vz > w+ w-, w+ > jprime jprime, w- > jprime jprime)")
    elif VVdecay == "lvqq" or VVdecay == "lvjj":
        processes.append("p p > vz, (vz > w+ w-, w+ > jprime jprime, w- > l- vl~)")
        processes.append("p p > vz, (vz > w+ w-, w- > jprime jprime, w+ > l+ vl)")
    elif VVdecay == "all":
        processes.append("p p > vz, (vz > w+ w-)")
    else:
        raise RuntimeError('Could not configure process for %s > %s' % (HVTdecay, VVdecay))
elif HVTdecay == "XH" : #do not decay Y in Madgraph, create X in Pythia & decay
    processes.append("p p > vc-" )
    processes.append("p p > vc+" )
    addHbb=True
elif HVTdecay == "tt" or HVTdecay == "ttbar":
    if VVdecay == "all":
        processes.append("p p > vz, (vz > t t~, t > j l+ vl, t~ > j l- vl~)")
        processes.append("p p > vz, (vz > t t~, t > jprime jprime j, t~ > jprime jprime j)")
        processes.append("p p > vz, (vz > t t~, t > jprime jprime j, t~ > j l- vl~)")
        processes.append("p p > vz, (vz > t t~, t > j l+ vl, t~ > jprime jprime j)")
    elif VVdecay == "allj":
        processes.append("p p > vz, (vz > t t~, t > b j j, t~ > b~ j j)")
    elif VVdecay == "ljet":
        processes.append("p p > vz, (vz > t t~, t > b j j, t~ > b~ l- vl~)")
        processes.append("p p > vz, (vz > t t~, t > b l+ vl, t~ > b~ j j)")
    elif VVdecay == "alll":
        processes.append("p p > vz, (vz > t t~, t > b l+ vl, t~ > b~ l- vl~)")
    else:
        raise RuntimeError('Could not configure process for %s > %s' % (HVTdecay, VVdecay))
elif HVTdecay == "tb":
    if VVdecay == "all":
        processes.append("p p > vc+, (vc+ > t b~, t > b l+ vl)")
        processes.append("p p > vc-, (vc- > t~ b, t~ > b~ l- vl~)")
        processes.append("p p > vc+, (vc+ > t b~, t > b j j)")
        processes.append("p p > vc-, (vc- > t~ b, t~ > b~ j j)")
    elif VVdecay == "had":
        processes.append("p p > vc+, (vc+ > t b~, t > b j j)")
        processes.append("p p > vc-, (vc- > t~ b, t~ > b~ j j)")
    elif VVdecay == "lep":
        processes.append("p p > vc+, (vc+ > t b~, t > b l+ vl)")
        processes.append("p p > vc-, (vc- > t~ b, t~ > b~ l- vl~)")
    else:
        raise RuntimeError('Could not configure process for %s > %s' % (HVTdecay, VVdecay))
elif HVTdecay == "Wh" or HVTdecay == "WH" or HVTdecay == "wh" or HVTdecay == "wH":
    addHbb=True
    if VVdecay == "lvqq" or VVdecay == "lvjj":
        processes.append("p p > vc+, (vc+ > w+ h, w+ > l+ vl)")
        processes.append("p p > vc-, (vc- > w- h, w- > l- vl~)")
        addHcc=True
    elif VVdecay == "qqqq" or VVdecay == "qqjj":
        processes.append("p p > vc+, (vc+ > w+ h, w+ > jprime jprime)")
        processes.append("p p > vc-, (vc- > w- h, w- > jprime jprime)")
        addHcc=True
    elif VVdecay == "lvbb" or VVdecay == "lvbb":
        processes.append("p p > vc+, (vc+ > w+ h, w+ > l+ vl)")
        processes.append("p p > vc-, (vc- > w- h, w- > l- vl~)")
    elif VVdecay == "qqbb" or VVdecay == "qqbb":
        processes.append("p p > vc+, (vc+ > w+ h, w+ > jprime jprime)")
        processes.append("p p > vc-, (vc- > w- h, w- > jprime jprime)")
    elif VVdecay == "all":
        processes.append("p p > vc+, (vc+ > w+ h)")
        processes.append("p p > vc-, (vc- > w- h)")
        addHbb=False
        addHcc=False
    else:
        raise RuntimeError('Could not configure process for %s > %s' % (HVTdecay, VVdecay))
elif HVTdecay == "WZ" or HVTdecay == "wz":
    if VVdecay == "qqqq" or VVdecay == "jjjj": 
        processes.append("p p > vc+, (vc+ > w+ z, w+ > jprime jprime, z > jprime jprime)")
        processes.append("p p > vc-, (vc- > w- z, w- > jprime jprime, z > jprime jprime)")
    elif VVdecay == "lvqq" or VVdecay == "lvjj": 
        processes.append("p p > vc+, (vc+ > w+ z, w+ > l+ vl, z > jprime jprime)")
        processes.append("p p > vc-, (vc- > w- z, w- > l- vl~, z > jprime jprime)")
    elif VVdecay == "lvll" or VVdecay == "lnull": 
        processes.append("p p > vc+, (vc+ > w+ z, w+ > l+ vl, z > l+ l-)")
        processes.append("p p > vc-, (vc- > w- z, w- > l- vl~, z > l+ l-)")
    elif VVdecay == "llqq" or VVdecay == "lljj" or VVdecay == "qqll" or VVdecay == "jjll": 
        processes.append("p p > vc+, (vc+ > w+ z, w+ > jprime jprime, z > l+ l-)")
        processes.append("p p > vc-, (vc- > w- z, w- > jprime jprime, z > l+ l-)")
    elif VVdecay == "vvqq" or VVdecay == "vvjj" or VVdecay == "nunuqq" or VVdecay == "nunujj":
        processes.append("p p > vc+, (vc+ > w+ z, w+ > jprime jprime, z > vl vl~)")
        processes.append("p p > vc-, (vc- > w- z, w- > jprime jprime, z > vl vl~)")
    elif VVdecay == "lvvv" or VVdecay == "lnununu": 
        processes.append("p p > vc+, (vc+ > w+ z, w+ > l+ vl, z > vl vl~)")
        processes.append("p p > vc-, (vc- > w- z, w- > l- vl~, z > vl vl~)")
    elif VVdecay == "all": 
        processes.append("p p > vc+, (vc+ > w+ z)")
        processes.append("p p > vc-, (vc- > w- z)")
    else:
        raise RuntimeError('Could not configure process for %s > %s' % (HVTdecay, VVdecay))
elif HVTdecay == "Wg" or HVTdecay == "Wgamma" or HVTdecay == "wg" or HVTdecay == "wgamma" or HVTdecay == "Wa" or HVTdecay == "wa" or HVTdecay == "Wy" or HVTdecay == "wy":
    if ("lv" in VVdecay) or ("lnu" in VVdecay):
        processes.append("p p > vc+, (vc+ > w+ a, w+ > l+ vl)")
        processes.append("p p > vc-, (vc- > w- a, w- > l- vl~)")
    elif ("qq" in VVdecay) or ("jj" in VVdecay):
        processes.append("p p > vc+, (vc+ > w+ a, w+ > jprime jprime)")
        processes.append("p p > vc-, (vc- > w- a, w- > jprime jprime)")
    else:
        raise RuntimeError('Could not configure process for %s > %s' % (HVTdecay, VVdecay))
elif HVTdecay == "lv":
    if ("ev" in VVdecay) or ("enu" in VVdecay):
        processes.append("p p > vc+, (vc+ > e+ ve)")
        processes.append("p p > vc-, (vc- > e- ve~)")
    if ("muv" in VVdecay) or ("munu" in VVdecay):
        processes.append("p p > vc+, (vc+ > mu+ vm)")
        processes.append("p p > vc-, (vc- > mu- vm~)")
    if ("tauv" in VVdecay) or ("taunu" in VVdecay):
        processes.append("p p > vc+, (vc+ > ta+ vt)")
        processes.append("p p > vc-, (vc- > ta- vt~)")
elif HVTdecay == "ll":
    if ("ee" in VVdecay):
        processes.append("p p > vz, (vz > e+ e-)")
    if ("mumu" in VVdecay):
        processes.append("p p > vz, (vz > mu+ mu-)")
    if ("tautau" in VVdecay):
        processes.append("p p > vz, (vz > ta+ ta-)")
elif HVTdecay == "dijet":
    if VVdecay == "jj":
        processes.append("p p > vz, (vz > j j)")
    elif VVdecay == "bb":
        processes.append("p p > vz, (vz > b b~)")
    else:
        raise RuntimeError('Could not configure process for %s > %s' % (HVTdecay, VVdecay))
else:
    raise RuntimeError('Could not configure process for %s > %s' % (HVTdecay, VVdecay))
        
if addHbb:
    pythiachans.append("25:oneChannel = on 0.5809 100 5 -5 ")
    if addHcc:
        pythiachans.append("25:addChannel = on 0.0288 100 4 -4 ")
elif addHcc:
    pythiachans.append("25:oneChannel = on 0.0288 100 4 -4 ")

#added tell pythia to decay vc+...
pythiachans.append("34:onMode off ")
pythiachans.append("-34:onMode off ")
pythiachans.append("34:oneChannel = on 1.0 100 25 +6666 ") #onMode bRatio meMode products (meMode = 0:isotropic phase space)
pythiachans.append("-34:oneChannel = on 1.0 100 25 -6666 ") #onMode bRatio meMode products (meMode = 0:isotropic phase space)

processstring=""
processcount=1
for i in processes:
    if vbfProd:
        
        i = i.replace("> vc+,", ( "> vc+ %s," % VBFString ) , 1)
        i = i.replace("> vc-,", ( "> vc- %s," % VBFString ) , 1)
        i = i.replace("> vz,",  ( "> vz %s,"  % VBFString ) , 1)
        
        if processstring=="":
            processstring="generate %s @%d " % (i, processcount)
        else:
            processstring+="\nadd process %s @%d" % (i, processcount)
        processcount+=1
    else:
        if processstring=="":
            processstring="generate %s QED=99 QCD=99 @%d " % (i, processcount)
        else:
            processstring+="\nadd process %s QED=99 QCD=99 @%d" % (i, processcount)
        processcount+=1

if not processstring:
    raise RuntimeError("process not recognized!")

#----------------------------------------------------------------------------
# Random Seed
#----------------------------------------------------------------------------
randomSeed = 0
if hasattr(runArgs,'randomSeed'): randomSeed = runArgs.randomSeed
else:
    raise RuntimeError("No random seed given.")

#----------------------------------------------------------------------------
# Beam energy
#----------------------------------------------------------------------------
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = int(runArgs.ecmEnergy) / 2.
else:
    raise RuntimeError("No center of mass energy found.")

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------
safefactor = 1.1
if ("VBFHVT_Agv1_VcWZ_vvqq_m2800" in get_physics_short()): safefactor = 2
if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:  nevents = int(runArgs.maxEvents*safefactor)
else: nevents = int(evgenConfig.nEventsPerJob*safefactor)


#---------------------------------------------------------------------------
# MG5 Proc card
#---------------------------------------------------------------------------
process_str="""
import model sm
# Define multiparticle labels
define p = g u c d s u~ c~ d~ s~ b b~
define j = g u c d s u~ c~ d~ s~ b b~
define jprime = g u c d s u~ c~ d~ s~ b b~
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~

import model Vector_Triplet_UFO

%s

# Output processes to MadEvent directory
output -f
""" % processstring

if not is_gen_from_gridpack():
  process_dir = new_process(process_str)
else:
  process_dir = MADGRAPH_GRIDPACK_LOCATION
    


#-----------------------------------------------------------------------
# param card
#-----------------------------------------------------------------------
## for HVT couplings, masses and decay widths
RHOINPUTS={
  'gst'  : 1.,
  'MVz'  : HVTmass,
  'cq'   : -1.3159,
  'cl'   : -1.3159,
  'c3'   : -1.3159,
  'ch'   : -0.555969,
  'cvvv' : -0.203965,
  'cvvw' : 1.,
  'cvvvv': 1.,
  'cvvhh': 0.0772754,
  }
masses={
  '32': HVTmass, ## Vz
  '34': HVTmass, ## Vc+
  }
if isXH: masses['6666'] = Xmass 
decays={ # calculate the decay width and branching ratios on-the-fly
  '32': 'Auto', ## Vz
  '34': 'Auto', ## Vc+
  }

params={}
params['MASS']=masses
if not isXH: params['DECAY']=decays #do not use defaults for XH...
params['RHOINPUTS']=RHOINPUTS

## for VBF production
if vbfProd:
    ## params is a dictionary of dictionaries (each dictionary is a separate block)
    mglog.info('Change default coupling values for VBF production')
    RHOINPUTS={}
    RHOINPUTS["cvvw"]  = 0
    RHOINPUTS["cq"]    = 0
    RHOINPUTS["cl"]    = 0
    RHOINPUTS["c3"]    = 0
    RHOINPUTS["ch"]    = 1
    RHOINPUTS["cvvhh"] = 0
    RHOINPUTS["cvvv"]  = 0
    RHOINPUTS["cvvvv"] = 0
    #
    if 'RHOINPUTS' in params:
        params['RHOINPUTS'].update(RHOINPUTS)
    else:
        params['RHOINPUTS']=RHOINPUTS


## for Wgamma production
if HVTdecay == "Wg" or HVTdecay == "Wgamma" or HVTdecay == "wg" or HVTdecay == "wgamma" \
                   or HVTdecay == "Wa" or HVTdecay == "wa" or HVTdecay == "Wy" or HVTdecay == "wy":
    mglog.info('Change default coupling value of Cvvw to deal with Wgamma ')
    RHOINPUTS={}
    RHOINPUTS["cq"]    = 0.01
    RHOINPUTS["cl"]    = 0
    RHOINPUTS["c3"]    = 0
    RHOINPUTS["ch"]    = 0.02
    #
    if 'RHOINPUTS' in params:
        params['RHOINPUTS'].update(RHOINPUTS)
    else:
        params['RHOINPUTS']=RHOINPUTS

mglog.info('Updating parameters:')
print(params)

modify_param_card(process_dir=process_dir,params=params)

if isXH:
  #Adding X information
  print('modification for XH Pythia decay')
  str_param_card=os.path.join(process_dir,'Cards/param_card.dat')
  os.system("cp %s temp.data"%str_param_card)
  tempParamCardO = open(str_param_card,'r')
  tempParamCardN = open('temp.dat','w')
  
  # X decays
  for line in tempParamCardO:
      if 'Block QNUMBERS 32  # vz' in line:
          tempParamCardN.write( 'Block QNUMBERS 6666  #X \n        1 3  # 3 times electric charge \n        2 3  # number of spin states (2S+1) \n        3 1  # colour rep (1: singlet, 3: triplet, 8: octet) \n        4 1  # Particle/Antiparticle distinction (0=own anti)\n' )
          tempParamCardN.write(line)
      elif 'DECAY' in line and '34' in line:
           tempParamCardN.write(line)
           tempParamCardN.write('DECAY  6666 2.0 #X \n') #width 0.0
           tempParamCardN.write('#  BR             NDA  ID1    ID2   ... \n')
           tempParamCardN.write('        1.0    2     2        -1   # BR(X -> quarks) \n')
  
      else:
          tempParamCardN.write(line)
  
  tempParamCardO.close()
  tempParamCardN.close()
  
  os.system("mv temp.dat %s"%str_param_card)
  print('isXH modification end')

#-----------------------------------------------------------------------
# run card
#-----------------------------------------------------------------------
## generator cuts
extras = {
  'ptj':"0",
  'ptb':"0",
  'pta':"0",
  'ptl':"0",
  'etaj':"-1",
  'etab':"-1",
  'etaa':"-1",
  'etal':"-1",
  'drjj':"0",
  'drbb':"0",
  'drll':"0",
  'draa':"0",
  'drbj':"0",
  'draj':"0",
  'drjl':"0",
  'drbl':"0",
  'dral':"0",
  'nevents': nevents,
}

## TODO: are the following needed, since fixed_ren_scale and fixed_fac_scale are F by default?
extras["scale"] = HVTmass
extras["dsqrt_q2fact1"] = HVTmass
extras["dsqrt_q2fact2"] = HVTmass

# Cut off tails for high mass
if HVTmass > 5000:
    extras["bwcutoff"] = 5
if vbfProd:
    extras['cut_decays'] = "F"
    extras['mmjj'] = 150

# update run card
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)
#-----------------------------------------------------------------------

#-----------------------------------------------------------------------
# Generation
#-----------------------------------------------------------------------
try:
    generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)
except RuntimeError as rte:
    for an_arg in rte.args:
        if 'Gridpack sucessfully created' in an_arg:
            print('Handling exception and exiting')
            theApp.finalize()
            theApp.exit()
    print('Unhandled exception - re-raising')
    raise rte

#--------------------------------------------------------------------------------------------------------------------
# Output
#--------------------------------------------------------------------------------------------------------------------

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)
check_reset_proc_number(opts)

#--------------------------------------------------------------------------------------------------------------------
# Metadata
#--------------------------------------------------------------------------------------------------------------------

# Some more information
evgenConfig.description = "HVT Signal Point"
evgenConfig.keywords = ["exotic"] 
evgenConfig.contact = ["Daniel Hayden <danhayden0@gmail.com>", "Lailin Xu <lailin.xu@cern.ch>"]
evgenConfig.process = "pp>%s>%s>%s" % (HVTboson,HVTdecay,VVdecay) # e.g. pp>Vc>WZ>qqqq

#--------------------------------------------------------------------------------------------------------------------
# Shower
#--------------------------------------------------------------------------------------------------------------------
# Finally, run the parton shower and configure MadGraph+Pythia
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# Make Pythia 8.2 happy with our new particles....
genSeq.Pythia8.Commands += [" 32:all vz vz 3 0 0 1000",
                            " 34:all vc+ vc- 3 3 0 1000",
                            "Init:showAllParticleData = on"]
genSeq.Pythia8.Commands += pythiachans
if vbfProd:
  genSeq.Pythia8.Commands += ["SpaceShower:dipoleRecoil = on"]
