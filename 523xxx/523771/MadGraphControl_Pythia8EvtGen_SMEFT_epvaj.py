# definitions that will be used for process
definitions="""
"""
#import model /cvmfs/atlas.cern.ch/repo/sw/Generators/madgraph/models/latest/SMEFTsim_A_U35_MwScheme_UFO  

# process (EFTORDER: NP<=1)
processes={
           'eva' : 'generate p p > e+ ve a EFTORDER\n add process p p > e- ve~ a EFTORDER\n',
           'muva' : 'generate p p > mu+ vm a EFTORDER\n add process p p > mu- vm~ a EFTORDER\n',
           'mutvaj' : 'generate p p > mu+ vm a EFTORDER @0 \n add process p p > mu- vm~ a EFTORDER @0 \n add process p p > mu+ vm a j EFTORDER @1 \n add process p p > mu- vm~ a j EFTORDER @1 \n',
           'mupvaj' : 'generate p p > mu+ vm a EFTORDER @0 \n add process p p > mu+ vm a j EFTORDER @1 \n',
           'mumvaj' : 'generate p p > mu- vm~ a EFTORDER @0 \n add process p p > mu- vm~ a j EFTORDER @1 \n',
           'elpvaj' : 'generate p p > e+ ve a EFTORDER @0 \n add process p p > e+ ve a j EFTORDER @1 \n',
           'elmvaj' : 'generate p p > e- ve~ a EFTORDER @0 \n add process p p > e- ve~ a j EFTORDER @1 \n',
}

# cuts and other run card settings
settings={
          'lhe_version' : '3.0',
          'cut_decays'  : 'F',
          'drjj'        : 0.0,
          'dynamical_scale_choice':'4',
          'ickkw'       : 0,
          'maxjetflavor': 4,
          'ktdurham'    : 30,
          'dparameter'  : 0.4,
          'xqcut' : 0,
          'ptl':'22',
          'etal':'3',
          'pta':'22',

}

# param card settings 
params={}

# EFT parameters with non-zero values for initial sample
smeftpars_bsm={'cW': 1,
               'cHWB': 1, 
               'cWtil': 1, 
               'cHWBtil': 1}

# parameters used in reweighting:
relevant_coeffs = 'cW cHWB cWtil cHWBtil'.split()

evgenConfig.description = 'epvaj production with SMEFTsim'
evgenConfig.keywords+=['SM','diboson','Wgamma','electroweak']
evgenConfig.generators = ["MadGraph","Pythia8","EvtGen"]
evgenConfig.contact = ['Antonio Costa <antonio.jacques.costa@cern.ch>']

include("MadGraphControl_SMEFTepvaj.py")
