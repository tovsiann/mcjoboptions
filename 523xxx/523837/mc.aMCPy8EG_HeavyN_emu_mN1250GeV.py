import subprocess
retcode = subprocess.call(['get_files', '-jo', 'HeavyNCommon_emu.py'])
if retcode != 0:
    raise IOError('could not locate HeavyNCommon_emu.py')

import HeavyNCommon_emu

HeavyNCommon_emu.process = HeavyNCommon_emu.available_processes['emuchannel']
HeavyNCommon_emu.parameters_paramcard['mass']['mN1'] = 1250
HeavyNCommon_emu.parameters_paramcard['mass']['mN2'] = 1000000
HeavyNCommon_emu.parameters_paramcard['mass']['mN3'] = 1000000

HeavyNCommon_emu.parameters_paramcard['numixing']['VeN1'] = 1
HeavyNCommon_emu.parameters_paramcard['numixing']['VeN2'] = 0
HeavyNCommon_emu.parameters_paramcard['numixing']['VeN3'] = 0

HeavyNCommon_emu.parameters_paramcard['numixing']['VmuN1'] = 1
HeavyNCommon_emu.parameters_paramcard['numixing']['VmuN2'] = 0
HeavyNCommon_emu.parameters_paramcard['numixing']['VmuN3'] = 0

HeavyNCommon_emu.parameters_paramcard['numixing']['VtaN1'] = 1
HeavyNCommon_emu.parameters_paramcard['numixing']['VtaN2'] = 0
HeavyNCommon_emu.parameters_paramcard['numixing']['VtaN3'] = 0

HeavyNCommon_emu.run_evgen(runArgs, evgenConfig, opts)
