import subprocess
retcode = subprocess.Popen(['get_files', '-jo', 'SMWeinbergCommon_v2.py'])
if retcode.wait() != 0:
    raise IOError('could not locate SMWeinbergCommon_v2.py')

import SMWeinbergCommon_v2

evgenConfig.contact.append("Karolos Potamianos <karolos.potamianos@cern.ch>")

SMWeinbergCommon_v2.process = SMWeinbergCommon_v2.available_processes['mutauchannel']
SMWeinbergCommon_v2.parameters_paramcard['nuphysics']['Lambda'] = 900e3
SMWeinbergCommon_v2.parameters_paramcard['nuphysics']['Cmm'] = 1.0
SMWeinbergCommon_v2.parameters_paramcard['nuphysics']['Cmt'] = 1.0
SMWeinbergCommon_v2.parameters_paramcard['nuphysics']['Ctt'] = 1.0

SMWeinbergCommon_v2.run_evgen(runArgs, evgenConfig, opts)
