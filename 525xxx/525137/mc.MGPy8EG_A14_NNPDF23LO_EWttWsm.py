from MadGraphControl.MadGraphUtils import *
import os
from MadGraphControl.MadGraphParamHelpers import set_top_params
from MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment import *

#---------------------------------------------------------------------------------------------------
#General Settings
#---------------------------------------------------------------------------------------------------
#nevents=5000
#runName='run_01'

safefactor=1.1

defs = """
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define w = w+ w-
define l+ = e+ mu+ ta+
define l- = e- mu- ta-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
define uc = u c
define uc~ = u~ c~
define ds = d s
define ds~ = d~ s~
"""

mcprod = defs+"""
generate p p > t t~ w j QED=3 QCD=1
"""

process = """
set gauge unitary
"""+mcprod+"""
output -f"""

process_dir = new_process(process)

extras = {'lhe_version'    :'3.0',
          'event_norm'     : 'average',
          'cut_decays'     : 'False',
          'drll'           : '0.4',
          'bwcutoff'       : '15.0',
          'pta'            : '10.',
		  'ptl'            : '10.',
          'etal'           : '2.5',
          'drjj'           : '0.4',
          'draa'           : '0.4',
          'etaj'           : '-5',
          'draj'           : '0.4',
          'drjl'           : '0.4',
          'dral'           : '0.4',
          'etaa'           : '2.5',
          'ptj'            : '20.',
          'ptj1min'        : '0.',
          'ptj1max'        : '-1.0',
          'mmjj'           : '0.0',
          'mmjjmax'        : '-1',
          'nevents'        : int(runArgs.maxEvents*safefactor)
          }


modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)
set_top_params(process_dir,mTop=172.5,FourFS=False)


mcprod_maddec = defs+"""
define w+child = l+ vl uc ds~
define w-child = l- vl~ ds uc~
decay t > w+ b, w+ > w+child w+child
decay t~ > w- b~, w- > w-child w-child
"""


#---------------------------------------------------------------------------------------------------
# Decay with MadSpin
#---------------------------------------------------------------------------------------------------

# Add madspin card
madspin_card=process_dir+'/Cards/madspin_card.dat'
if os.access(madspin_card,os.R_OK):
    os.unlink(madspin_card)

mscard = open(madspin_card,'w')
mscard.write("""#************************************************************
#*                        MadSpin                           *
#*                                                          *
#*    P. Artoisenet, R. Frederix, R. Rietkerk, O. Mattelaer *
#*                                                          *
#*    Part of the MadGraph5_aMC@NLO Framework:              *
#*    The MadGraph5_aMC@NLO Development Team - Find us at   *
#*    https://server06.fynu.ucl.ac.be/projects/madgraph     *
#*                                                          *
#************************************************************
set max_weight_ps_point 100  # number of PS to estimate the maximum for each event
# set Nevents_for_max_weigth 100
set BW_cut 15
set seed %i
%s
launch
"""%(runArgs.randomSeed, mcprod_maddec))
mscard.close()


params = {}
# params['loop']={
#     "1" :  9.118800e+01 # MU_R
# }

params['MASS'] = {
    "5"  : 4.950000e+00, # MB
    "6"  : 1.725000e+02, # MT
    "15" : 1.776820e+00, # MTA
    "23" : 9.118760e+01, # MZ
    "25" : 1.250000e+02, # MH
    ## Dependent parameters, given by model restrictions.
    ## Those values should be edited following the
    ## analytical expression. MG5 ignores those values
    ## but they are important for interfacing the output of MG5
    ## to external program such as Pythia.
    "1"  : 0.000000, # d : 0.0
    "2"  : 0.000000, # u : 0.0
    "3"  : 0.000000, # s : 0.0
    "4"  : 0.000000, # c : 0.0
    "11" : 0.000000, # e- : 0.0
    "12" : 0.000000, # ve : 0.0
    "13" : 0.000000, # mu- : 0.0
    "14" : 0.000000, # vm : 0.0
    "16" : 0.000000, # vt : 0.0
    "21" : 0.000000, # g : 0.0
    "22" : 0.000000, # a : 0.0
    "24" : 80.399000, # w+ : cmath.sqrt(MZ__exp__2/2. + cmath.sqrt(MZ__exp__4/4. - (aEW*cmath.pi*MZ__exp__2)/(Gf*sqrt__2)))
}

params['SMINPUTS'] = {
    "1" : 1.323489e+02, # aEWM1
    "2" : 1.166370e-05, # Gf
    "3" : 1.184000e-01 # aS
}

params['YUKAWA'] = {
    "5"  : 4.950000e+00, # ymb
    "6"  : 1.725000e+02, # ymt
    "15" : 1.777000e+00, # ymtau
}

params['DECAY'] = {
    "1"   : "DECAY  1   0.000000e+00",
    "2"   : "DECAY  2   0.000000e+00",
    "3"   : "DECAY  3   0.000000e+00",
    "4"   : "DECAY  4   0.000000e+00",
    "5"   : "DECAY  5   0.000000e+00",
    "6"   : """DECAY  6   1.320000e+00
   1.000000e+00   2    5  24""",
#     "-6"  : """DECAY -6   1.320000e+00
#    1.000000e+00   2   -5 -24""",
    "11"  : "DECAY  11   0.000000e+00",
    "12"  : "DECAY  12   0.000000e+00",
    "13"  : "DECAY  13   0.000000e+00",
    "14"  : "DECAY  14   0.000000e+00",
    "15"  : "DECAY  15   0.000000e+00",
    "16"  : "DECAY  16   0.000000e+00",
    "21"  : "DECAY  21   0.000000e+00",
    "22"  : "DECAY  22   0.000000e+00",
    "23"  : "DECAY  23   2.495200e+00",
#     "-24" : """DECAY  -24   2.085000e+00
#    3.377000e-01   2    1  -2
#    3.377000e-01   2    3  -4
#    1.082000e-01   2   11 -12
#    1.082000e-01   2   13 -14
#    1.082000e-01   2   15 -16""",
    "24"  : """DECAY  24   2.085000e+00
   3.377000e-01   2   -1   2
   3.377000e-01   2   -3   4
   1.082000e-01   2  -11  12
   1.082000e-01   2  -13  14
   1.082000e-01   2  -15  16""",
    "25"  : "DECAY  25   4.070000e-03",
    # "82"  : "DECAY  82   0.000000e+00"
}

modify_param_card(process_dir=process_dir,params=params)

generate(process_dir=process_dir,runArgs=runArgs)

#---------------------------------------------------------------------------------------------------
# Multi-core capability
#---------------------------------------------------------------------------------------------------
check_reset_proc_number(opts)

outputDS = arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=False)

#---------------------------------------------------------------------------------------------------
#### Showering with Pythia 8
#---------------------------------------------------------------------------------------------------
evgenConfig.description = "ttW+j EW LO production"
evgenConfig.keywords = ["ttW", "SM","top"]
evgenConfig.process = "generate p p > t t~ w j"
evgenConfig.generators  = ['MadGraph', 'Pythia8', 'EvtGen']
evgenConfig.inputfilecheck = outputDS
runArgs.inputGeneratorFile=outputDS
evgenConfig.contact = ["Olga Bessidskaia Bylund <olga.bylund@cern.ch>, Laura Barranco Navarro <lbarranc@cern.ch>"]

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

