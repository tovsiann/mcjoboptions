#--------------------------------------------------------------
# Pythia8 showering setup
#--------------------------------------------------------------
# initialize Pythia8 generator configuration for showering

runArgs.inputGeneratorFile=runArgs.inputGeneratorFile

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

#--------------------------------------------------------------
# Edit merged LHE file to remove problematic lines
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_Powheg.py")

fname = "merged_lhef._0.events"

f = open(fname, "r")
lines = f.readlines()
f.close()

f = open(fname, 'w')
for line in lines:
  if not "#pdf" in line:
    f.write(line)
f.close()

include("Pythia8_i/Pythia8_Powheg_Main31.py")

# configure Pythia8
genSeq.Pythia8.Commands += [ "25:oneChannel = on 0.5 100 5 -5 ",  # bb decay
                             "25:addChannel = on 0.5 100 22 22 "] # yy decay

#--------------------------------------------------------------
# Dipole option Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ "SpaceShower:dipoleRecoil = on" ]


#---------------------------------------------------------------------------------------------------
# EVGEN Configuration
#---------------------------------------------------------------------------------------------------
evgenConfig.generators    += ["Powheg", "Pythia8"]
evgenConfig.description    = "DiHiggs production with cHHH +10.0, decay to bbyy, with Powheg-Box-V2, at NLO + full top mass."
evgenConfig.keywords       = ["hh", "SM", "SMHiggs", "nonResonant", "bbbar","bottom"]
evgenConfig.contact        = ['Jannicke Pearkes <Jannicke.Pearkes@cern.ch>']
evgenConfig.nEventsPerJob  = 10000
evgenConfig.maxeventsfactor = 1.0
evgenConfig.inputFilesPerJob = 4


#---------------------------------------------------------------------------------------------------
# Filter for bbyy
#---------------------------------------------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import ParentChildFilter
filtSeq += ParentChildFilter("HbbFilter", PDGParent = [25], PDGChild = [5])
filtSeq += ParentChildFilter("HyyFilter", PDGParent = [25], PDGChild = [22])
filtSeq.Expression = "HbbFilter and HyyFilter"
