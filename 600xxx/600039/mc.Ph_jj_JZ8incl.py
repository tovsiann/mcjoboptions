#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 dijet production with A14 NNPDF2.3 tune."
evgenConfig.keywords = ["SM", "QCD", "jets", "2jet"]
evgenConfig.contact = ["amoroso@cern.ch"]
evgenConfig.nEventsPerJob = 500
evgenConfig.generators = ["Powheg"]

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg jj process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_jj_Common.py")
PowhegConfig.bornktmin =750
PowhegConfig.bornsuppfact =9000

PowhegConfig.mu_F         = [1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0] # List of factorisation scales which pairs with renormalisation scale below
PowhegConfig.mu_R         = [1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5] # List of renormalisation scales
PowhegConfig.PDF          = [304400, 25300, 14200, 14100, 14300, 14400, 25100, 91500, 42560, 317500, 303600, 319300, 319500, 322500, 322700, 322900, 323100, 323300, 323500, 323700, 323900]
# PDF variations with nominal scale variation:
# CT18NNLO,	 14000, 
# NNPDF31_nnlo_as_0118_hessian, 
# MMHT2014nnlo68cl, 
# CT18ANNLO, 
# CT18ZNNLO, 
# CT18XNNLO, 
# CT18NLO, 
# MMHT2014nlo68cl, 
# PDF4LHC15_nnlo_mc, 
# ABMP16_5_nnlo, 
# NNPDF31_nnlo_as_0118_nojets, 
# NNPDF31_nnlo_as_0118
# NNPDF31_nnlo_as_0116
# NNPDF31_nnlo_as_0120
# NNPDF31_nnlo_as_0108
# NNPDF31_nnlo_as_0110
# NNPDF31_nnlo_as_0112
# NNPDF31_nnlo_as_0114
# NNPDF31_nnlo_as_0117
# NNPDF31_nnlo_as_0119
# NNPDF31_nnlo_as_0122
# NNPDF31_nnlo_as_0124

# PowhegConfig.PDF.extend(range(14001, 14059))                            # Include the CT18NNLO error set
PowhegConfig.PDF.extend(range(304401, 304501))                          # Include the NNPDF31_nnlo_as_0118_hessian error set
PowhegConfig.PDF.extend(range(25301, 25351))                            # Include the MMHT2014nnlo68cl error set
# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()
