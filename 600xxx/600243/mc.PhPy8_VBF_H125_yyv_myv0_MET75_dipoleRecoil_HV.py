#--------------------------------------------------------------
# Use LHE files as input
# Defining the function to extract parameters
#--------------------------------------------------------------
dict_pdgIds = {}
dict_pdgIds["y"]   = 22

def getParameters():
    import re

    #--- Read parts of the job option
    #--- Assumes the form MC15.111111.PowhegPy8EG_ggH_H125_Xy_Zd${m}_ctauYY
    jonamelist = jofile.rstrip(".py").split("_") 
    process = jonamelist[1]
    decayChan = jonamelist[3]
    mzd = float(jonamelist[4].split("myv")[1].replace("p", "."))

    return process, decayChan, mzd

process, decayChan, mzd  = getParameters()
print "Parameters: "
print process, decayChan, mzd 

#--------------------------------------------------------------
# MET Filter 
#--------------------------------------------------------------
include("GeneratorFilters/MissingEtFilter.py")
filtSeq.MissingEtFilter.METCut = 75*GeV

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
# For VBF, Nfinal = 3
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
# For VBF, also use dipole recoil option
genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']

#--------------------------------------------------------------
# Higgs-> y yv in Pythia8
#--------------------------------------------------------------

genSeq.Pythia8.Commands += [
                            'Higgs:useBSM = on',
                            '25:m0 = 125',
                            '25:mWidth = 0.00407',
                            '25:doForceWidth = off',
                            '25:onMode = off',
                            '25:addChannel = 1 1. 103 22 4900022',
                            '25:onIfMatch = 22 4900022',
                            '4900022:m0 = %.1f' % mzd,
                            '4900022:onMode = off',
                            '4900022:tau0 = 0',
                            '4900022:onIfAny = 12 14 16' # Just in case it does decay...neutrinos
                            ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.keywords    = [ "BSM", "Higgs", "BSMHiggs", "mH125"]
evgenConfig.description = "POWHEG+Pythia8 VBF H production, H->yyv, Hidden Valley, mh=125 GeV"
evgenConfig.process     = "VBF H->yyv"
evgenConfig.contact     = [ 'christopher.hayes@cern.ch','ben.carlson@cern.ch' ]

evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 6
