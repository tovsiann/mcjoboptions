# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

decay_mode = "w+ > e+ ve"


mass_low=250
mass_high=400

include("PowhegControl_W_H7.py")

evgenConfig.nEventsPerJob = 2000
