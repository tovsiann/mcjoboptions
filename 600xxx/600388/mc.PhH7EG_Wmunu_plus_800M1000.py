# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

decay_mode = "w+ > mu+ vm"


mass_low=800
mass_high=1000

include("PowhegControl_W_H7.py")

evgenConfig.nEventsPerJob = 2000
