process="ggH"

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
# include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
if process=="ggH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2']
elif process=="VBF":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="WpH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="WmH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="ZH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3']
elif process=="ggZH":
    genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2']

genSeq.Pythia8.Commands += [ 'WeakZ0:gmZmode = 2']
genSeq.Pythia8.Commands += [ '25:onMode = off',
                             '25:addChannel = 1 0.002 100 223  22'
                             ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+PYTHIA8, ggH H->OmegaGamma"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs","photon", "mH125" ]
evgenConfig.contact     = [ 'g.s.virdee@cern.ch' ]
evgenConfig.generators  = [ "Pythia8", "Powheg"]
evgenConfig.inputFilesPerJob = 8 #Specify the number of LHEs files needed 
evgenConfig.nEventsPerJob = 10000