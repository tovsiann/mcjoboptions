#--------------------------------------------------------------
# Powheg WW setup starting from ATLAS defaults
#--------------------------------------------------------------
# based on 361600

include('PowhegControl/PowhegControl_WW_Common.py')
PowhegConfig.decay_mode = 'w+ w- > l+ vl l\'- vl\'~'
PowhegConfig.withdamp = 1
PowhegConfig.bornzerodamp = 1
PowhegConfig.PDF = [10800,13000, 303600, 25100] #CT10nnlo, CT14nnlo, NNPDF3.1nnlo, MMHT2014nlo68cl
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 1.0, 1.0, 2.0, 2.0, 2.0 ]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 2.0, 0.5, 1.0, 2.0 ]
PowhegConfig.nEvents *= 30
PowhegConfig.generate()


#--------------------------------------------------------------
# Pythia8 showering with main31 and AZNLO CTEQ6L1 tune
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
genSeq.Pythia8.Commands += ['Powheg:NFinal = 2']

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 WW_lvlv production with AZNLO CTEQ6L1 tune'
evgenConfig.keywords    = [ 'electroweak', 'diboson', 'WW', '2lepton', 'neutrino' ]
evgenConfig.contact     = [ 'oldrich.kepka@cern.ch' ]
evgenConfig.nEventsPerJob   = 2000

#--------------------------------------------------------------
# FILTERS
#--------------------------------------------------------------
include('GeneratorFilters/MultiLeptonFilter.py')
## Default cut params 
filtSeq.MultiLeptonFilter.Ptcut = 3500.
filtSeq.MultiLeptonFilter.Etacut = 2.7
filtSeq.MultiLeptonFilter.NLeptons = 2

include('GeneratorFilters/ChargedTrackWeightFilter.py')
filtSeq.ChargedTracksWeightFilter.NchMin = 7
filtSeq.ChargedTracksWeightFilter.NchMax = 20
filtSeq.ChargedTracksWeightFilter.Ptcut = 500
filtSeq.ChargedTracksWeightFilter.Etacut= 2.5
filtSeq.ChargedTracksWeightFilter.SplineX =[ 0, 2, 5, 15, 25, 35 ]
filtSeq.ChargedTracksWeightFilter.SplineY =[ 0.01, 0.01, 0.03, 0.065, 0.085, 0.085 ]
