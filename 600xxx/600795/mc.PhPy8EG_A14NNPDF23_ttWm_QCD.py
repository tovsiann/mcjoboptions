# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
# ME part can be found in https://its.cern.ch/jira/browse/ATLMCPROD-9266

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 top pair W minus boson QCD production with A14 NNPDF2.3 tune."
evgenConfig.keywords = ["SM", "top"]
evgenConfig.contact = ["metsai@umich.edu"]
evgenConfig.generators = [ 'Powheg', 'Pythia8', 'EvtGen' ]


#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")

genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
