#--------------------------------------------------------------
# Modify the events.lhe, since Pythia doesn't like to decay the
# SM higgs to BSM products: 25 --> 35
#--------------------------------------------------------------

#evgenConfig.inputfilecheck = "TXT"
#evgenConfig.inputfilecheck = "merged_lhef"

import os, sys, glob
for f in glob.glob("*.events"):
    infile = f
    f1 = open( infile )
    newfile = infile+'.temp'
    f2 = open(newfile,'w')
    for line in f1:
        if line.startswith('      25     1'):
            f2.write(line.replace('      25     1','      35     1'))
        else:
            f2.write(line)
    f1.close()
    f2.close()
    os.system('mv %s %s '%(infile, infile+'.old') )
    os.system('mv %s %s '%(newfile, infile) )

###Run Number Encoding and decoding
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
print(get_physics_short())
tokens = get_physics_short().replace(".py","").replace("p",".").split('_')

ma=float(tokens[-2].split('ma')[-1])
ctau=float(tokens[-1].split('ctau')[-1])

print('#############################################################')
print('ma='+str(ma))
print('ctau='+str(ctau))
print('#############################################################')

#################################################
#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2']
#################################################

#--------------------------------------------------------------
# Higgs->2a->4b at Pythia8
#--------------------------------------------------------------

genSeq.Pythia8.Commands += [
                            'Higgs:useBSM = on',

                            '35:m0 = 125',
                            '35:doForceWidth = on',
                            '35:mWidth = 0.00407',
                            '35:oneChannel = 1 1.0 100 70 70',


                            '70:new = S S 1 0 0 %.1f' % ma,#scalar mass
                            '70:tau0 = %.1f' % ctau,#nominal proper lifetime (in mm/c)
                            '70:isResonance = 0',
                            '70:oneChannel = 1 1.0 0 5 5',
                            
]

genSeq.Pythia8.Commands = [i for i in genSeq.Pythia8.Commands if (("limitTau0" not in i) and ("tau0Max" not in i))]
genSeq.Pythia8.Commands += [
                            'ParticleDecays:tau0Max = 100000.0',
                            'ParticleDecays:limitTau0 = off'
                           ]

print(genSeq.Pythia8.Commands)

testSeq.TestHepMC.MaxTransVtxDisp = 100000000 #in mm
testSeq.TestHepMC.MaxVtxDisp = 100000000 #in mm

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 ggF->H->2a->4b production"
evgenConfig.contact = ["stefanie.morgenstern@cern.ch"]
