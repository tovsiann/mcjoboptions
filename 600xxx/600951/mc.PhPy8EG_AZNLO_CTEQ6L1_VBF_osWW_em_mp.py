# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 VBF opposite-sign WW production with AZNLO CTEQ6L1 tune."
evgenConfig.keywords = ["SM", "diboson", "WW", "VBF"]
evgenConfig.contact = ["christophe.pol.a.roland@cern.ch"]
evgenConfig.nEventsPerJob=2000
# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg VBF_osWW process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_VBF_osWW_Common.py")

PowhegConfig.decay_mode = "w+ w- > mu+ vm e- ve~"

# Continuum only
PowhegConfig.ww_res_type = 1
PowhegConfig.bornsuppfact = 1

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate(use_XML_reweighting=False)

#--------------------------------------------------------------
# Pythia8 showering with the AZNLO CTEQ6L1 tune
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Photospp.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")

genSeq.Pythia8.Commands += [
    'Powheg:NFinal = 4',
    'SpaceShower:dipoleRecoil = on'
]
