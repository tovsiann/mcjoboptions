#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 ttbar production A14 shower and exactly one BSM tau->3mu decay"
evgenConfig.keywords = ["BSM", "top","ttbar", "tau", "muon"]
evgenConfig.contact = ["jan.kretzschmar@cern.ch"]

# 16500 events per input LHE file, need 6 for technical W->taunu branching 10.8%, about 10 for full filter
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 10

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

#--------------------------------------------------------------
# Event filter

#--------------------------------------------------------------
# force half of the taus to decay to three muons, keeping the SM decays
genSeq.Pythia8.Commands += [' 15:addChannel = 1 1.0 0 13 -13 13']

# --------------------------------------------------------------
# filter
# --------------------------------------------------------------    

# filter ttbar to have exactly one tau->3mu present
# this filter is encodes the W->taunu branching and is exactly equal to 10.8%
# (factor 2 for two W and technical factor 0.5 from branching settings compensate)

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonWithParentFilter
filtSeq += MultiLeptonWithParentFilter("tau3mu")
filtSeq.tau3mu.NLeptonsMin = 3 # tau->3mu
filtSeq.tau3mu.NLeptonsMax = 4 # allows at most one extra leptonic tau, but vetoes a second tau->3mu
filtSeq.tau3mu.MinPt = 0.
filtSeq.tau3mu.MaxEta = 10.
filtSeq.tau3mu.PDGOrigin = [15]
filtSeq.tau3mu.IncludeLepTaus = False
filtSeq.tau3mu.IncludeHadTaus = False
#filtSeq.tau3mu.OutputLevel = DEBUG


# second filter step to ensure most of the tau->3mu decays are close to detector acceptance
# roughly 65% efficiency?

from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
filtSeq += MultiMuonFilter("TwoMuonsFilter")
filtSeq.TwoMuonsFilter.Ptcut = 3250.
filtSeq.TwoMuonsFilter.Etacut = 3.0
filtSeq.TwoMuonsFilter.NMuons = 2

filtSeq += MultiMuonFilter("ThreeMuonsFilter")
filtSeq.ThreeMuonsFilter.Ptcut = 2000.
filtSeq.ThreeMuonsFilter.Etacut = 3.0
filtSeq.ThreeMuonsFilter.NMuons = 3

