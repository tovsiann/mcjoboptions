evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune Var1 Down, at least one lepton filter, Jpsi->mumu filter, ME NNPDF30 NLO, A14 NNPDF23 LO from DSID 410450 LHE files with Shower Weights added '
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton', 'Jpsi' ]
evgenConfig.contact     = [ 'dpanchal@utexas.edu' ]
##evgenConfig.inputfilecheck="410450.Powheg_ttbar_hdamp258p75_LHE"
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 5

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_Var1Down_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")

genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]


#--------------------------------------------------------------
# Special decay of anti-B->Jpsi->mumu
#--------------------------------------------------------------
from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
evgenConfig.auxfiles += ['AntiB2Jpsimumu.DEC']
genSeq.EvtInclusiveDecay.userDecayFile = 'AntiB2Jpsimumu.DEC'

## NonAllHad filter
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

# Jpsi->mumu filter
include('GeneratorFilters/TTbarWithJpsimumuFilter.py')
filtSeq.TTbarWithJpsimumuFilter.JpsipTMinCut = 5000.