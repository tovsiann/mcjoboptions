

#-------------------------------------------------------------
# Powheg ZZ setup starting from ATLAS defaults
#--------------------------------------------------------------
# based on 361603

include('PowhegControl/PowhegControl_ZZ_Common.py')

PowhegConfig.decay_mode = 'z z > l+ l- l\'+ l\'-'
PowhegConfig.withdamp = 1
PowhegConfig.bornzerodamp = 1
PowhegConfig.mllmin = 4.0   # GeV
PowhegConfig.PDF = [10800, 13000, 303600, 25100, 260000] #CT10nnlo, CT14nnlo, NNPDF3.1nnlo, MMHT2014nlo68cl, NNPDF30_nlo
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 1.0, 1.0, 2.0, 2.0, 2.0 ]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 2.0, 0.5, 1.0, 2.0 ]
PowhegConfig.nEvents *= 12
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with main31 and AZNLO CTEQ6L1 tune
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
genSeq.Pythia8.Commands += ['Powheg:NFinal = 2']

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ZZ->llll production with AZNLO CTEQ6L1 tune, mllmin4, and etalmax2.6'
evgenConfig.keywords    = [ 'electroweak', 'diboson', 'ZZ', '4lepton' ]
evgenConfig.contact     = [ 'philipp.sebastian.ott@cern.ch' ]
evgenConfig.nEventsPerJob = 2000

#--------------------------------------------------------------
# FILTERS
#--------------------------------------------------------------
include('GeneratorFilters/MultiLeptonFilter.py')
## Default cut params 
filtSeq.MultiLeptonFilter.Ptcut = 3500.
filtSeq.MultiLeptonFilter.Etacut = 2.6
filtSeq.MultiLeptonFilter.NLeptons = 4
