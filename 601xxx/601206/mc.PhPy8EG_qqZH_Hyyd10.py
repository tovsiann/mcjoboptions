#---------------------------------------------------------------
# LHE files of qqZH used as inputs: mc15_13TeV.345038.PowhegPythia8EvtGen_NNPDF30_AZNLO_ZH125J_Zincl_MINLO.evgen.TXT.e5590 
# POWHEG+Pythia8 qqZH,Z->ll  H-> gam gam_d
#---------------------------------------------------------------
evgenConfig.nEventsPerJob = 500
evgenConfig.inputFilesPerJob = 86

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
	genSeq.Pythia8.Commands  += ['Powheg:NFinal = 3']
else:
	genSeq.Pythia8.UserModes += [ 'Main31:NFinal = 3' ]
#--------------------------------------------------------------
# H->y+yd decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [
                            'Higgs:useBSM = on', # BSM Higgs
                            '25:onMode = off', # decay of Higgs
                            '25:addChannel = 1 1. 103 22 4900022', # H->y+yd HV
                            '25:onIfMatch = 22 4900022',
                            '4900022:m0 = 10.0', # yd mass
                            '4900022:onMode = off', # yd decay off
                            '4900022:tau0 = off', # yd no decay to tau
                            ]
#--------------------------------------------------------------
# Lepon filter for Z->ll
#--------------------------------------------------------------
if not hasattr( filtSeq, "MultiLeptonFilter" ):
    from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
    filtSeq += MultiLeptonFilter()
    pass
MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 10000.
MultiLeptonFilter.Etacut = 2.7
MultiLeptonFilter.NLeptons = 2
 
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 qqZH production: H->y+yd, Hidden Valley, 10 GeV"
evgenConfig.keywords    = [ 'BSM' , 'Higgs' , 'BSMHiggs' , 'ZHiggs' , 'photon' , 'darkPhoton' , 'hiddenValley']
evgenConfig.contact     = [ 'hassnae.el.jarrari@cern.ch' ]
evgenConfig.process = "qq->ZH, H->y+yd"
