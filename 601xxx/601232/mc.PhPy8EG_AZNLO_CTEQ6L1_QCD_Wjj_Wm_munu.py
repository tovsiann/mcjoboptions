# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 Wjj production with AZNLO CTEQ6L1 tune."
evgenConfig.keywords = ["SM", "W", "2jet"]
evgenConfig.contact = ["christophe.pol.a.roland@cern.ch"]

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg Wjj process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_Wjj_Common.py")

PowhegConfig.bornsuppfact = 30
PowhegConfig.mjjminsuppfact = 200
PowhegConfig.ptborncut = 20
PowhegConfig.doublefsr = 1

PowhegConfig.decay_mode = "w- > mu- vm~"

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()


#--------------------------------------------------------------
# Pythia8 showering with the AZNLO CTEQ6L1 tune
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Photospp.py')

genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
