#--------------------------------------------------------------
# Powheg Z setup starting from ATLAS defaults
#--------------------------------------------------------------
# in rel.19  361106 
include('PowhegControl/PowhegControl_Z_Common.py')
PowhegConfig.decay_mode = "z > e+ e-"

# Configure Powheg setup
PowhegConfig.ptsqmin = 4.0 # needed for AZNLO tune
PowhegConfig.running_width = 1
PowhegConfig.nEvents   *=  1.2
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with AZNLO_CTEQ6L1 and Photos
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Photospp.py')

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 Z->ee production without lepton filter and AZNLO CT10 tune'
evgenConfig.contact = ["daniel.hayden@cern.ch"]
evgenConfig.keywords    = [ 'NLO', 'SM', 'electroweak', 'Z', 'drellYan', '2electron' ]
evgenConfig.nEventsPerJob = 10000

