# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# This is an example joboption to generate events with Powheg
# using ATLAS' interface. Users should optimise and carefully
# validate the settings before making an official sample request.
#--------------------------------------------------------------

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 H+jet production with A14 NNPDF2.3 tune."
evgenConfig.keywords = ["SM", "Higgs", "1jet", "WW"]
evgenConfig.contact = ["stephane.willocq@cern.ch"]
evgenConfig.nEventsPerJob = 2000

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg Hj process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_Hj_Common.py")

# Further modifications to those defaults

# NNPDF30_nnlo central + all 101 NNPDF30_nlo variations + all 30 PDF variations + alphaS (31 + 32) for PDF4LHC15_nlo
PowhegConfig.PDF = [261000]+[91400]+range(260000,260101)+range(90400,90433)+[11000]+[25200]+[13100]

PowhegConfig.mass_H = 125.0
PowhegConfig.width_H = 0.00407

# Filter 
PowhegConfig.bornktmin = 80
PowhegConfig.bornsuppfact = 600

# Powheg QCD scale variations
#   The first weight is the nominal. (mu0 = mH, I think)
#   Second will be stored as 1001, i.e. mu_fact = mu0 * 0.5, mu_ren = mu0 * 0.5
#   Third  will be stored as 1002, i.e. mu_fact = mu0 * 0.5, mu_ren = mu0 * 1.0, etc
PowhegConfig.mu_F = [ 1.0, 0.5, 0.5, 0.5, 1.0, 1.0, 2.0, 2.0, 2.0 ]
PowhegConfig.mu_R = [ 1.0, 0.5, 1.0, 2.0, 0.5, 2.0, 0.5, 1.0, 2.0 ]

# which quark mass variations to save.
PowhegConfig.define_event_weight_group( group_name='quark_mass_variation', parameters_to_vary=['bmass_in_minlo','quarkmasseffects'] )

# mtmb means both top and b masses are considered in the loop. This is the nominal choice.
# mtinf is 'EFT', meaning the top quark mass is set to infinity, and mb=0 (ignored)
# mtmb-bminlo is an alternative treatment of the b-mass
# these weghts are stored in a separate weight group, and I think we will have:
# 3001 = mtmb (copy of nominal Powheg weight = weight 0), 3002 = mtinf, 3003 mtmb-bminlo
PowhegConfig.add_weight_to_group( group_name='quark_mass_variation', weight_name='mtmb', parameter_values=[0,1] )
PowhegConfig.add_weight_to_group( group_name='quark_mass_variation', weight_name='mtinf', parameter_values=[0,0] )
PowhegConfig.add_weight_to_group( group_name='quark_mass_variation', weight_name='mtmb-bminlo', parameter_values=[1,1] )

# Account for leptonic filter efficiency
PowhegConfig.nEvents *= 2.5

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune, main31 routine
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:m0 = 125',        # Higgs mass (just in case)
                             '25:mWidth = 0.00407',# Higgs width (just in case)
                             '25:onIfMatch = 24 -24',
                             '24:onMode = off', # decay of W
                             '24:mMin = 2.0',
                             '24:onNegIfAny = 11 13 15',    # W- decay to lv
                             '24:onPosIfAny = 1 2 3 4 5 6'] # W+ decay to qq

#--------------------------------------------------------------
# Lepton filter
#--------------------------------------------------------------
from GeneratorFilters.GeneratorFiltersConf import LeptonFilter
filtSeq += LeptonFilter("LeptonFilter")

LeptonFilter = filtSeq.LeptonFilter
LeptonFilter.Ptcut = 20.*GeV
LeptonFilter.Etacut = 5.0

filtSeq.Expression = "(LeptonFilter)"
