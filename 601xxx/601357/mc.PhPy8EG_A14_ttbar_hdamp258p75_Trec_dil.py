#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, with the recoil-to-top user hook for diepton events.'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton']
evgenConfig.contact     = [ 'aknue@cern.ch' ]
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 6


#--------------------------------------------------------------
# Powheg/Pythia matching
#--------------------------------------------------------------


include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")  
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

# set the user hook for recoil to top                                                                                                                                                                   
genSeq.Pythia8.Commands += [ 'TimeShower:recoilToColoured=off' ]
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    genSeq.Pythia8.UserHooks += ['TopRecoilHook']

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
   
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = 2
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.

