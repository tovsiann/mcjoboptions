evgenConfig.description = 'POWHEG+Pythia8+EvtGen tW production, DR scheme, dynamic scale, dilepton, with Powheg hdamp equal 3*top mass, A14 tune, ME NNPDF30 NLO, A14 NNPDF23 LO'
evgenConfig.keywords    = [ 'SM', 'top', 'singleTop', 'Wt']
evgenConfig.contact     = [ 'dominic.hirschbuehl@cern.ch', 'jens.roggel@cern.ch' ]
evgenConfig.generators += ['Powheg', 'Pythia8', 'EvtGen']

#--------------------------------------------------------------
# Powheg tW setup - V2
#--------------------------------------------------------------
include("PowhegControl/PowhegControl_Wt_DR_modified_Common.py")

PowhegConfig.decay_mode_top = 't > b l+ vl'
PowhegConfig.decay_mode_W = 'w > l vl'
PowhegConfig.hdamp        = 517.5 # 3 * mtop

PowhegConfig.nEvents     *= 1.1     # Add safety factor
PowhegConfig.fulloffshell=1
PowhegConfig.numwidth_W=80.4
PowhegConfig.mass_W_low=40.
PowhegConfig.runningscales=1
PowhegConfig.btlscalect=1
PowhegConfig.btlscalereal=1
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")

genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

