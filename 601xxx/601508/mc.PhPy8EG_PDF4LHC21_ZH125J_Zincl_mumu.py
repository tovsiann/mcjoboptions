#--------------------------------------------------------------
# Pythia A14 tune
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg.py')

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]

#--------------------------------------------------------------
# H->mumu decay in Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 13 13']

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+MiNLO+Pythia8 H+Z+jet production: Z->all, H->mumu"
evgenConfig.keywords    = [ "SMHiggs", "ZHiggs", "2muon" ]
evgenConfig.contact     = [ 'Yanlin Liu <yanlin.liu@cern.ch>' ]
evgenConfig.process     = "qq->ZH, H->mumu, Z->all"
evgenConfig.inputFilesPerJob = 55
evgenConfig.nEventsPerJob = 5000
