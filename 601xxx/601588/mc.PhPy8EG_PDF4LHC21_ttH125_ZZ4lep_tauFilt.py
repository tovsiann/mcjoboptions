#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.process          = "ttH H->ZZ->4l"
evgenConfig.description      = 'POWHEG+Pythia8 ttH production with A14 NNPDF2.3 tune, H->4l w. tau filter'
evgenConfig.keywords         = [ "SM", "Higgs", "SMHiggs", "ZZ", "mH125" ]
evgenConfig.contact          = [ 'guglielmo.frattari@cern.ch' ]
evgenConfig.generators       = [ 'Powheg', 'Pythia8', 'EvtGen' ]
evgenConfig.inputFilesPerJob = 2
evgenConfig.nEventsPerJob    = 20000

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Powheg_Main31.py')
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

#--------------------------------------------------------------
# H->ZZ->4l decay
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off',
                             '25:onIfMatch = 23 23',
                             '23:onMode = off',
                             '23:mMin = 2.0',
                             '23:onIfAny = 11 13 15']

#--------------------------------------------------------------
# HVV Tau filter
#--------------------------------------------------------------
include('GeneratorFilters/xAODXtoVVDecayFilterExtended_Common.py')
filtSeq.xAODXtoVVDecayFilterExtended.PDGGrandParent = 25
filtSeq.xAODXtoVVDecayFilterExtended.PDGParent = 23
filtSeq.xAODXtoVVDecayFilterExtended.StatusParent = 22
filtSeq.xAODXtoVVDecayFilterExtended.PDGChild1 = [15]
filtSeq.xAODXtoVVDecayFilterExtended.PDGChild2 = [11,13,15]
