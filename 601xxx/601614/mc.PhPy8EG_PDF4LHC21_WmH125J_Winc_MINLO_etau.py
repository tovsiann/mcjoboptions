#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:oneChannel = 1 0.5 100 15 -11',
                             '25:addChannel = 1 0.5 100 11 -15' ]

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.process     = "qq->WmH, W->all, H->etau"
evgenConfig.description = "POWHEG+MiNLO+PYTHIA8, H+Wm+jet, W->all, H->etau"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "WHiggs", "mH125" ]
evgenConfig.contact     = [ 'huanguo.li@cern.ch' ]
evgenConfig.inputFilesPerJob = 11 
evgenConfig.nEventsPerJob    = 10000
