#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 dijet production with A14 NNPDF2.3 tune."
evgenConfig.keywords = ["SM", "QCD", "jets", "2jet"]
evgenConfig.contact = ["amoroso@cern.ch", "jan.kretzschmar@cern.ch", "javier.llorente.merino@cern.ch"]
evgenConfig.nEventsPerJob = 20000
evgenConfig.generators = ["Powheg"]

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg jj process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_jj_Common.py")
PowhegConfig.bornktmin =750
PowhegConfig.bornsuppfact =9000

PowhegConfig.mu_F         = [1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0] # List of factorisation scales which pairs with renormalisation scale below
PowhegConfig.mu_R         = [1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5] # List of renormalisation scales
PowhegConfig.PDF          =  [14000, 331500, 14200, 14100, 14300, 14400, 27100, 27400, 93300, 42560, 331100, 332700, 332900, 333100, 333300, 333500]
PowhegConfig.rwl_group_events = 10000

PowhegConfig.ncall1       = 160000
PowhegConfig.ncall2       = 160000
PowhegConfig.nubound      = 9600000
PowhegConfig.itmx1        = 20
PowhegConfig.itmx2        = 20

### Fold parameter reducing the negative eventweight fraction
PowhegConfig.foldcsi      = 1
PowhegConfig.foldphi      = 1
PowhegConfig.foldy        = 2

PowhegConfig.nEvents = runArgs.maxEvents if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob

# PDF variations with nominal scale variation:
# CT18NNLO 14000
# NNPDF40_nnlo_as_0118_hessian 331500
# CT18ANNLO 14200
# CT18ZNNLO 14100
# CT18XNNLO 14300
# CT18NLO 14400
# MSHT20nlo_as118 27100
# MSHT20nnlo_as118 27400
# PDF4LHC21_40_pdfas 93300
# ABMP16_5_nnlo 42560
# NNPDF40_nnlo_as_01180 331100
# NNPDF40_nnlo_as_01160 332700
# NNPDF40_nnlo_as_01170 332900
# NNPDF40_nnlo_as_01175 333100
# NNPDF40_nnlo_as_01185 333300
# NNPDF40_nnlo_as_01190 333500

PowhegConfig.PDF.extend(range(14001, 14059))                            # Include the CT18NNLO error set
PowhegConfig.PDF.extend(range(331501, 331551))                          # Include the NNPDF40_nnlo_as_01180_hessian error set
PowhegConfig.PDF.extend(range(27401, 27465))                            # Include the MSHT20nnlo_as118 error set
# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()
