#--------------------------------------------------------------
# Pythia8 showering with Photos
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

include("Pythia8_i/Pythia8_Powheg_Main31.py")
#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]


#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------


evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBF H->gamgam mh=125 GeV"
evgenConfig.keywords    = [ "Higgs", "SMHiggs", "diphoton" ]
evgenConfig.contact     = [ 'diallo.boye@cern.ch','christian.sander@desy.de' ]
evgenConfig.generators  = [ 'Powheg','Pythia8','EvtGen' ]

evgenConfig.nEventsPerJob   = 10000
evgenConfig.inputFilesPerJob = 2
