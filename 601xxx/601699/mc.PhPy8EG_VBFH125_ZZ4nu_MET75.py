#--------------------------------------------------------------
# EVGEN configuration                                                                                                                                                                             
#--------------------------------------------------------------                                                                                                                                
evgenConfig.description = "POWHEG+PYTHIA8+EVTGEN, VBF H->ZZ->4nu mh=125 GeV CPS"
evgenConfig.keywords    = [ "Higgs", "SMHiggs", "ZZ" ]
evgenConfig.contact     = [ 'diallo.boye@cern.ch','christian.sander@desy.de']
evgenConfig.generators = [ 'Powheg','Pythia8','EvtGen' ]
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 4
#evgenConfig.inputfilecheck = "TXT"
#del testSeq.TestLHE

#--------------------------------------------------------------
# Pythia8 showering
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')

#--------------------------------------------------------------
# Pythia8 main31 update
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 3' ]
genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']

#--------------------------------------------------------------
# Higgs at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off',    # decay of Higgs
                             '25:onIfMatch = 23 23',
                             '23:onMode = off',    # decay of Z
                             '23:mMin = 2.0',
                             '23:onIfMatch = 12 12',
                             '23:onIfMatch = 14 14',
                             '23:onIfMatch = 16 16' ]

#--------------------------------------------------------------
# Missing Et filter 
#--------------------------------------------------------------
include('GeneratorFilters/MissingEtFilter.py')
filtSeq.MissingEtFilter.METCut = 75*GeV
filtSeq.Expression = "MissingEtFilter" 
