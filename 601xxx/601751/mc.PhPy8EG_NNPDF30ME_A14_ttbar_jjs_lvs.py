#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg with t~ -> Ws~ and t -> Ws, w+ > j j, w- > l- vl~'
evgenConfig.keywords    =['SM', 'top', 'ttbar', 'CKM']
evgenConfig.contact     = [ 'benedikt.gocke@cern.ch']
evgenConfig.nEventsPerJob = 10000

include('PowhegControl/PowhegControl_tt_Common.py')
PowhegConfig.decay_mode      = "t t~ > undecayed"

DoSingleWeight = False
if DoSingleWeight:
    PowhegConfig.mu_F         = 1.0
    PowhegConfig.mu_R         = 1.0
    PowhegConfig.PDF          = 260000
else:
    PowhegConfig.mu_F         = [1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0] # List of factorisation scales which pairs with renormalisation scale below
    PowhegConfig.mu_R         = [1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5] # List of renormalisation scales

PowhegConfig.nEvents     *= 1.1 # compensate filter efficiency

# Set top quark Branching ratios
PowhegConfig.BR_t_to_Wb = 0.0
PowhegConfig.BR_t_to_Wd = 0.0
PowhegConfig.BR_t_to_Ws = 1.0

PowhegConfig.MadSpin_model = "loop_sm-zeromass_ckm"
PowhegConfig.MadSpin_decays= ["decay t > w+ s, w+ > j j","decay t~ > w- s~, w- > l- vl~"]
PowhegConfig.MadSpin_process = "generate p p > t t~ [QCD]"
PowhegConfig.MadSpin_nFlavours = 5

PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------

# Go to serial mode for Pythia8
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'POWHEG:pThard = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:nFinal = 2' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'POWHEG:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'POWHEG:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'POWHEG:MPIveto = 0' ]
