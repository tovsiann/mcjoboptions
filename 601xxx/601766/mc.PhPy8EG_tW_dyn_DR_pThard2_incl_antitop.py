#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8+EvtGen tW production (antitop), DR scheme, dynamic scale, inclusive, pTHard=2, hdamp equal 1.5*top mass, A14 tune, ME NNPDF30 NLO, A14 NNPDF23 LO from DSID 601619 LHE files with Shower Weights added'
evgenConfig.keywords    = [ 'SM', 'top', 'singleTop', 'Wt' ]
evgenConfig.contact     = [ 'tetiana.moskalets@cern.ch' ]
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1

#--------------------------------------------------------------
# Pythia8 showering
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31_tW.py")

genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]

