import os

from MadGraphControl.MadGraphUtils import *

# read out the LQ mass and generation for evgen description
# physics short has has the following format: mc.PhH7EG_LO_LQ_S43_ResProd_lam11_2000_1p0.py
job_option_name = get_physics_short()
JO_name_list = job_option_name.split('_')
leptoquark_mass = float( JO_name_list[-2] )
leptoquark_coupling = float( JO_name_list[-1].replace("p",".") )
# use S43, S13, S23, S53 to label LQs in physics short
leptoquark_charge = int( JO_name_list[3][1] ) 
input_order = JO_name_list[1]

if "lam11_" in job_option_name:
    generation = "1e"
elif "lam21_" in job_option_name:
    generation = "2e"
elif "lam31_" in job_option_name:
    generation = "3e"
elif "lam12_" in job_option_name:
    generation = "1m"
elif "lam22_" in job_option_name:
    generation = "2m"
elif "lam32_" in job_option_name:
    generation = "3m"
elif "lam13_" in job_option_name:
    generation = "1t"
elif "lam23_" in job_option_name:
    generation = "2t"
elif "lam33_" in job_option_name:
    generation = "3t"
elif "lam1122_" in job_option_name:
    generation = "1e2m"
elif "lam1121_" in job_option_name:
    generation = "1e2e"
elif "lam1222_" in job_option_name:
    generation = "1m2m"
else:
    raise RuntimeError("Cannot determine LQ generational couplings from job option name: {:s}.".format(job_option_name))

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = 'Scalar {0:s} resonant production of S43 with, generations: {1:s}, mLQ={2:d}'.format(
    input_order, generation, int(leptoquark_mass))
evgenConfig.keywords    += ['BSM', 'exotic', 'NLO', 'leptoquark', 'scalar']
evgenConfig.process     = 'pp -> S43 -> lepton + jet'
evgenConfig.generators 	= ["Powheg", "Herwig7", "EvtGen"]
evgenConfig.contact     = ["Gustavs Kehris <gustavs.kehris@cern.ch>, Daniel Buchin <daniel.buchin@cern.ch>, Michael Holzbock <michael.holzbock@cern.ch>"]
evgenConfig.tune       	= "H7.2-Default"

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg LQ-s-chan process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_LQ_s_chan_Common.py")

# Compensate remnant extraction error in Herwig!
# First generation does not suffer as much from remnant extraction error
if generation == "1e" or generation == "3e":
    PowhegConfig.nEvents     *= 1.1
# Herwig discards high fractions of events when initial state muons are involved (> 30% for high LQ masses and lower couplings)
if "2m" in generation or "3m" in generation:
    PowhegConfig.nEvents     *= 1.5 
# Adding the same compensation for tau events
if "1t" in generation or "2t" in generation or "3t" in generation:
    PowhegConfig.nEvents     *= 1.5

PowhegConfig.mLQ 		     = leptoquark_mass

# set width to -1 to let Powheg calculate the width. 
# But, it will only take into account couplings involving charged leptons, neutrinos are neglected
PowhegConfig.widthLQ         = -1
PowhegConfig.BWgen		     = 1
PowhegConfig.LQmasslow		 = 100
PowhegConfig.LQmasshigh		 = leptoquark_mass + 3000
PowhegConfig.runningscale	 = 1

# Options deciding if the generation is at LO or NLO
if input_order == "LO":
    PowhegConfig.bornonly    = 1
    PowhegConfig.LOevents    = 1


PowhegConfig.bornsmartsig    = 0

# --------------------------------------------------------------
# Integration settings
# --------------------------------------------------------------
PowhegConfig.ncall1 		 = 80000
PowhegConfig.ncall2 		 = 100000
PowhegConfig.nubound 		 = 100000

PowhegConfig.xupbound        = 2

PowhegConfig.PDF	         = list(range(82400, 82501))

# --------------------------------------------------------------
# Couplings and Charge
# --------------------------------------------------------------

# Settings for the couplings. 
#  / y_1e y_1m y_1t \    u/d
#  | y_2e y_2m y_2t |    c/s
#  \ y_3e y_3m y_3t /    t/b

PowhegConfig.y_1e = leptoquark_coupling if "1e" in generation else 0
PowhegConfig.y_2e = leptoquark_coupling if "2e" in generation else 0
PowhegConfig.y_3e = leptoquark_coupling if "3e" in generation else 0
PowhegConfig.y_1m = leptoquark_coupling if "1m" in generation else 0
PowhegConfig.y_2m = leptoquark_coupling if "2m" in generation else 0
PowhegConfig.y_3m = leptoquark_coupling if "3m" in generation else 0
PowhegConfig.y_1t = leptoquark_coupling if "1t" in generation else 0
PowhegConfig.y_2t = leptoquark_coupling if "2t" in generation else 0
PowhegConfig.y_3t = leptoquark_coupling if "3t" in generation else 0

# Set this to the charge of the desired LQ's absolute charge times 3. Expect 1,2,4 or 5 
PowhegConfig.charge          = leptoquark_charge

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

# --------------------------------------------------------------
# remap the PDG ID in the LHE file after its generation
# --------------------------------------------------------------

LQ_pwg_id = 42
if leptoquark_charge == 1:
    LQ_pwg_id = 9911561
if leptoquark_charge == 2:
    LQ_pwg_id = 9941561
if leptoquark_charge == 4:
    LQ_pwg_id = 9921561
if leptoquark_charge == 5:
    LQ_pwg_id = 9941561

remap_lhe_pdgids(runArgs.inputGeneratorFile, pdgid_map={LQ_pwg_id:42})

# --------------------------------------------------------------
# Herwig PS
# --------------------------------------------------------------
include("Herwig7_i/Herwig72_LHEF.py")

pdf_order = "NLO"
me_order  = input_order

# configure Herwig7
Herwig7Config.add_commands("""
### Define LQ to avoid clashes with IDs (use correct electromagnetic charge!):
create /ThePEG/ParticleData S1pbar
setup S1pbar 42 S1pbar {0} 0.0 0.0 0.0 -{1} 3 1 0
create /ThePEG/ParticleData S1p
setup S1p -42 S1p {0} 0.0 0.0 0.0 {1} -3 1 0
makeanti S1pbar S1p

### Mismatching of UE and hard process can occur quite often in muon initiated processes
set /Herwig/Generators/EventGenerator:MaxErrors 100000

### We are dealing with lepton PDFs!
set /Herwig/Partons/RemnantDecayer:AllowLeptons Yes

### Muon width has to be set to zero to avoid far away vertices (10^23 mm) when dealing with initial state muons
### Common practice in e.g. MadGraph anyway
set /Herwig/Particles/mu-:Width 0
set /Herwig/Particles/mu+:Width 0

### Necessary commands when running a process with intial state leptons
set /Herwig/Particles/e-:PDF /Herwig/Partons/NoPDF
set /Herwig/Particles/e+:PDF /Herwig/Partons/NoPDF

### Arguments used in the Herwig input by Luca et al.
set /Herwig/Shower/ShowerHandler:HardEmission 0
set /Herwig/Shower/ShowerHandler:Interactions QCDandQED
set /Herwig/Generators/EventGenerator:EventHandler:StatLevel Full
""".format(leptoquark_mass, leptoquark_charge))

Herwig7Config.me_pdf_commands(order=pdf_order, name="LUXlep-NNPDF31_nlo_as_0118_luxqed")   #LUXlep-NNPDF31_nlo_as_0118_luxqed  #NNPDF23_lo_as_0130_qed (247000)
Herwig7Config.tune_commands()
Herwig7Config.lhef_powhegbox_commands(lhe_filename=runArgs.inputGeneratorFile, me_pdf_order=me_order, usespin=True, usepwhglhereader=True)

Herwig7Config.add_commands("""
### Setting shower PDFs to LUXlep likely necessary as LUXlep is the only PDF set including leptons!
set /Herwig/Shower/ShowerHandler:PDFA /Herwig/Partons/Hard{0}PDF
set /Herwig/Shower/ShowerHandler:PDFB /Herwig/Partons/Hard{0}PDF

#####
### These commands are needed to drastically reduce the ratio of the
### "Remnant extraction failed in ShowerHandler::cascade() from primary interaction"
### error.
set /Herwig/Shower/ShowerHandler:PDFARemnant /Herwig/Partons/Hard{0}PDF
set /Herwig/Shower/ShowerHandler:PDFBRemnant /Herwig/Partons/Hard{0}PDF
#####

set /Herwig/EventHandlers/LHEReader:PDFA /Herwig/Partons/Hard{0}PDF
set /Herwig/EventHandlers/LHEReader:PDFB /Herwig/Partons/Hard{0}PDF
set /Herwig/EventHandlers/LHEReader:WeightWarnings false
""".format(pdf_order))

# add EvtGen
include("Herwig7_i/Herwig71_EvtGen.py")

Herwig7Config.run()
