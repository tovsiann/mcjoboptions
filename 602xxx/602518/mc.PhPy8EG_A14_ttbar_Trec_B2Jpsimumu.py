#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = 'POWHEG+Pythia8 ttbar production with Powheg hdamp equal 1.5*top mass, A14 tune, with the recoil-to-top user hook, at least one single lepton filter, B->Jpsi->mumu filter'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar', 'lepton', 'Jpsi']
evgenConfig.contact     = [ 'aknue@cern.ch', 'asada@hepl.phys.nagoya-u.ac.jp', 'derue@lpnhe.in2p3.fr' ]
evgenConfig.nEventsPerJob = 10000
#evgenConfig.inputfilecheck="TXT" # D.P.: maybe not needed? 
#evgenConfig.nEventsPerJob = 2000  # H.A. optional? to be adjusted.
evgenConfig.inputFilesPerJob = 4 # H.A. optional? to be adjusted.

#--------------------------------------------------------------
# Powheg/Pythia matching
#--------------------------------------------------------------


include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")  
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += [ 'Powheg:pTHard = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTdef = 2' ]
genSeq.Pythia8.Commands += [ 'Powheg:veto = 1' ]
genSeq.Pythia8.Commands += [ 'Powheg:vetoCount = 3' ]
genSeq.Pythia8.Commands += [ 'Powheg:pTemt  = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:emitted = 0' ]
genSeq.Pythia8.Commands += [ 'Powheg:MPIveto = 0' ]


# set the user hook for recoil to top                                                                                                                                                                    
genSeq.Pythia8.Commands += [ 'TimeShower:recoilToColoured=off' ]
if "UserHooks" in genSeq.Pythia8.__slots__.keys():
    genSeq.Pythia8.UserHooks += ['TopRecoilHook']


#--------------------------------------------------------------
# Special decay of anti-B->Jpsi->mumu
#--------------------------------------------------------------
from EvtGen_i.EvtGen_iConf import EvtInclusiveDecay
evgenConfig.auxfiles += ['B2Jpsimumu.DEC']
genSeq.EvtInclusiveDecay.userDecayFile = 'B2Jpsimumu.DEC'

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
   
include('GeneratorFilters/TTbarWToLeptonFilter.py')
filtSeq.TTbarWToLeptonFilter.NumLeptons = -1
filtSeq.TTbarWToLeptonFilter.Ptcut = 0.


# apply a J/psi to muons filter
include('GeneratorFilters/TTbarWithJpsimumuFilter.py')
filtSeq.TTbarWithJpsimumuFilter.JpsipTMinCut = 5000.

