#--------------------------------------------------------------
# POWHEG+MiNLO+Pythia8 gg->ZH->ZWW->Zincl+lvlv production
#--------------------------------------------------------------

#--------------------------------------------------------------
# Pythia showering with A14 tune
#--------------------------------------------------------------
include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Powheg_Main31.py")

#--------------------------------------------------------------
# Pythia8 main31 matching
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ 'Powheg:NFinal = 2' ]

#--------------------------------------------------------------
# Higgs->WW->lvlv at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '25:onMode = off', # decay of Higgs
                             '25:onIfMatch = 24 24',
                             '24:onMode = off',#decay of W
                             '24:mMin = 2.0',
                             '24:onMode = off',
                             '24:onIfAny = 11 12 13 14 15 16']

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
# uses LHE: mc21_13p6TeV.601411.Ph_PDF4LHC21_A14_ggZH125_LHE.evgen.TXT
evgenConfig.description = "POWHEG+MiNLO+Pythia8 gg->ZH->Zincl+lvlv production"
evgenConfig.keywords    = [ "SM", "Higgs", "SMHiggs", "mH125" , "ZHiggs" ]
evgenConfig.contact     = [ 'michiel.jan.veen@cern.ch' ]
evgenConfig.inputFilesPerJob = 11
evgenConfig.process = "gg->ZH, H->WW, Z->inclusive, WW->lvlv"
