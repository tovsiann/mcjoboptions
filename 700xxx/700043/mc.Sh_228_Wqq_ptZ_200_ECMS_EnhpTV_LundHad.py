
include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")
include("Sherpa_i/Lund_Hadronisation.py")

evgenConfig.description = "Sherpa W -> qq' + 1j@NLO + 4j@LO with pTV > 200 GeV."
evgenConfig.keywords = ["SM", "W", "jets" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "matthew.gignac@cern.ch" ]

genSeq.Sherpa_i.RunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};
  PP_RS_SCALE VAR{sqr(sqrt(H_T2)-PPerp(p[2])+MPerp(p[2]))/4};

  OL_PARAMETERS=ew_scheme 2 ew_renorm_scheme 1
  ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1 EW|LO1|LO2 EW|LO1|LO2|LO3;
  EW_SCHEME=3
  GF=1.166397e-5
  METS_BBAR_MODE=5

  %tags for process setup
  NJET:=3; LJET:=1,2; QCUT:=20.;

  %me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops

  HARD_DECAYS=1
  STABLE[24]=0
  WIDTH[24]=0
  HDH_STATUS[24,2,-1]=2
  HDH_STATUS[24,4,-3]=2
  HDH_STATUS[-24,-2,1]=2
  HDH_STATUS[-24,-4,3]=2

  % Negative weights
  NLO_CSS_PSMODE=1

}(run)

(processes){
  Process 93 93 -> 24 93 93{NJET};
  Enhance_Observable VAR{log10(PPerp(p[2]))}|2.30|3 {2,3,4,5}
  Cut_Core 1;
  Order (*,1); CKKW sqr(QCUT/E_CMS);
  Associated_Contributions EW|LO1|LO2|LO3 {LJET};
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.99 {2,3,4,5};
  End process;

  Process 93 93 -> -24 93 93{NJET};
  Enhance_Observable VAR{log10(PPerp(p[2]))}|2.30|3 {2,3,4,5}
  Cut_Core 1;
  Order (*,1); CKKW sqr(QCUT/E_CMS);
  Associated_Contributions EW|LO1|LO2|LO3 {LJET};
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Integration_Error 0.99 {2,3,4,5};
  End process;
}(processes)

(selector){
  PTNLO 24 200.0 E_CMS
  PTNLO -24 200.0 E_CMS
}(selector)
"""

genSeq.Sherpa_i.Parameters += [ "WIDTH[24]=0" ]
genSeq.Sherpa_i.Parameters += [ "OL_PARAMETERS=ew_scheme=2 ew_renorm_scheme=1 write_parameters=1" ]
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]
genSeq.Sherpa_i.NCores = 16
evgenConfig.nEventsPerJob = 1000
