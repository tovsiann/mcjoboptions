evgenConfig.description = "Sherpa Z/gamma* -> tau tau + 0,1,2j@NLO + 3,4,5j@LO with hadronic tau decays and tau filter taking input from existing unfiltered input file."
evgenConfig.keywords = ["SM", "Z", "2tau", "2lepton", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "matthew.gignac@cern.ch", "chris.g@cern.ch" ]
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 4

if runArgs.trfSubstepName == 'generate' :
   print "ERROR: These JO require an input file.  Please use the --afterburn option"
 
if runArgs.trfSubstepName == 'afterburn':
   evgenConfig.generators += ["Sherpa"]
 
   ## Loop removal should not be necessary anymore with HEPMC_TREE_LIKE=1 below
   if hasattr(testSeq, "FixHepMC"):
      fixSeq.FixHepMC.LoopsByBarcode = False
 
   ## Disable TestHepMC for the time being, cf.  
   ## https://its.cern.ch/jira/browse/ATLMCPROD-1862
   if hasattr(testSeq, "TestHepMC"):
      testSeq.remove(TestHepMC())

   # at least two hadronic taus, pTvis>20 GeV, |etavis|<2.6
   include("GeneratorFilters/MultiElecMuTauFilter.py")
   MultiElecMuTauFilter = filtSeq.MultiElecMuTauFilter
   MultiElecMuTauFilter.NLeptons       = 2
   MultiElecMuTauFilter.MinPt          = 1E9 # disable e or mu for this filter
   MultiElecMuTauFilter.IncludeHadTaus = True
   MultiElecMuTauFilter.MinVisPtHadTau = 20000.0
   MultiElecMuTauFilter.MaxEta         = 2.6

   # if not hasattr(filtSeq, "TauLeadFilter" ):
   #    from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
   #    filtSeq += MultiElecMuTauFilter(name="TauLeadFilter")

   # TauLeadFilter = filtSeq.TauLeadFilter
   # TauLeadFilter.NLeptons       = 1
   # TauLeadFilter.MinPt          = 1E9 # disable e or mu for this filter
   # TauLeadFilter.IncludeHadTaus = True
   # TauLeadFilter.MinVisPtHadTau = 30000.0
   # TauLeadFilter.MaxEta         = 2.6

   postSeq.CountHepMC.CorrectRunNumber = True

