include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")
include("Sherpa_i/Fusing_Fragmentation.py")

evgenConfig.description = "Sherpa W+/W- -> munu + 0,1j@NLO with EvtGen"
evgenConfig.keywords = ["SM", "W", "muon", "jets", "NLO" ]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch","chris.g@cern.ch","mdshapiro@lbl.gov" ]
evgenConfig.nEventsPerJob = 10000

genSeq.Sherpa_i.RunCard="""
(run){
  %scales, tags for scale variations
  FSF:=1.; RSF:=1.; QSF:=1.;
  SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};

  # Shower improvements
  NLO_SUBTRACTION_SCHEME=2;

  # me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic LOOPGEN;
  LOOPGEN:=OpenLoops

  # tags for process setup
  NJET:=2; LJET:=2,3; QCUT:=20.;

  # EW corrections setup
  OL_PARAMETERS=ew_scheme 2 ew_renorm_scheme 1
  ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1 EW|LO1|LO2 EW|LO1|LO2|LO3;
  EW_SCHEME=3
  GF=1.166397e-5
  METS_BBAR_MODE=5

  # speed and neg weight fraction improvements
  PP_RS_SCALE VAR{sqr(sqrt(H_T2)-PPerp(p[2])-PPerp(p[3])+MPerp(p[2]+p[3]))/4};
  NLO_CSS_PSMODE=1

}(run)

(processes){
  Process 93 93 -> 13 -14 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  Associated_Contributions EW|LO1|LO2|LO3 {LJET};
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  End process;

  Process 93 93 -> -13 14 93{NJET};
  Order (*,2); CKKW sqr(QCUT/E_CMS);
  Associated_Contributions EW|LO1|LO2|LO3 {LJET};
  NLO_QCD_Mode MC@NLO {LJET};
  ME_Generator Amegic {LJET};
  RS_ME_Generator Comix {LJET};
  Loop_Generator LOOPGEN {LJET};
  Max_N_Quarks 4 {6,7,8};
  Max_Epsilon 0.01 {6,7,8};
  Integration_Error 0.99 {3,4,5,6,7,8};
  End process;

}(processes)

(selector){
  Mass 13 -14 2.0 E_CMS
  Mass -13 14 2.0 E_CMS
}(selector)
"""

genSeq.Sherpa_i.NCores = 16
genSeq.Sherpa_i.Parameters += [ "OL_PARAMETERS=ew_scheme=2 ew_renorm_scheme=1 write_parameters=1" ]
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]

include("EvtGen_i/EvtGen_Fragment.py")
evgenConfig.auxfiles += ['inclusiveP8DsDPlus.pdt']
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8DsDPlus.pdt"

# Don't some states with mass mismatches between EvtGen and Sherpa
# These are all strongly decaying states, so it is fine to use Sherpa decays
genSeq.EvtInclusiveDecay.whiteList.remove(10411)
genSeq.EvtInclusiveDecay.whiteList.remove(-10411)
genSeq.EvtInclusiveDecay.whiteList.remove(10421)
genSeq.EvtInclusiveDecay.whiteList.remove(-10421)
genSeq.EvtInclusiveDecay.whiteList.remove(-10423)
genSeq.EvtInclusiveDecay.whiteList.remove(10423)
genSeq.EvtInclusiveDecay.whiteList.remove(10431)
genSeq.EvtInclusiveDecay.whiteList.remove(-10431)
genSeq.EvtInclusiveDecay.whiteList.remove(-10433)
genSeq.EvtInclusiveDecay.whiteList.remove(10433)
genSeq.EvtInclusiveDecay.whiteList.remove(-20433)
genSeq.EvtInclusiveDecay.whiteList.remove(20433)


