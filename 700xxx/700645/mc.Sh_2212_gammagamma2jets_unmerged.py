include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")

evgenConfig.description = "Sherpa 2.2.12 gamma gamma + 2j@LO EW production"
evgenConfig.keywords = ["SM", "diphoton", "LO"]
evgenConfig.contact  = [ "atlas-generators-sherpa@cern.ch", "frank.siegert@cern.ch", "orcun.kolay@cern.ch" ]
evgenConfig.nEventsPerJob = 10000

genSeq.Sherpa_i.RunCard="""
(run){
  EXCLUSIVE_CLUSTER_MODE=1;
  
  # perturbative scales
  CORE_SCALE=VAR{Abs2(p[2]+p[3])};
  ALPHAQED_DEFAULT_SCALE=0.0;
  
  % tags for process setup
  NJET:=0; QCUT:=10;
  
  % me generator settings
  ME_SIGNAL_GENERATOR Comix Amegic;
  
  % improve colour-flow treatment
  CSS_CSMODE = 1;
  
  % improve integration performance
  CDXS_VSOPT=5;
  PSI_ItMin 25000;
  Integration_Error 0.05;
}(run)

(processes){
  Process 93 93 -> 22 22 93 93 93{NJET};
  Order (0,4);
  CKKW sqr(QCUT/E_CMS)/(1.0+sqr(QCUT/0.6)/Abs2(p[2]+p[3]));
  End process;
}(processes)

(selector){
  "PT" 22 20,E_CMS:18,E_CMS [PT_UP];
  IsolationCut 22 0.1 2 0.10;
  DeltaR 22 22 0.2 1000.0;
  FastjetFinder antikt 2 20 0.0 0.4;
}(selector)
"""

genSeq.Sherpa_i.NCores = 128


