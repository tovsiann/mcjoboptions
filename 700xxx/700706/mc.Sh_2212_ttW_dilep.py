include("Sherpa_i/Base_Fragment.py")
include("Sherpa_i/NNPDF30NNLO.py")
include("Sherpa_i/Fusing_Fragmentation.py")

evgenConfig.description = "Sherpa ttW+0,1j@NLO+2j@LO with Sherpa 2.2.12 and EW correction weights"
evgenConfig.keywords = ["ttW", "SM", "multilepton"]
evgenConfig.contact = ["rohin.narayan@cern.ch", "atlas-generators-sherpa@cern.ch"]
evgenConfig.nEventsPerJob = 1000

genSeq.Sherpa_i.RunCard="""
(run){
    # perturbative scales
    CORE_SCALE VAR{H_TM2/4}
    EXCLUSIVE_CLUSTER_MODE 1;

    # merging setup
    QCUT:=30.;
    LJET:=3,4; NJET:=2; 
    ME_SIGNAL_GENERATOR Comix Amegic OpenLoops;
    INTEGRATION_ERROR=0.05;

    # EW corrections
    ASSOCIATED_CONTRIBUTIONS_VARIATIONS=EW EW|LO1 EW|LO1|LO2 EW|LO1|LO2|LO3
    NLO_CSS_PSMODE=1

    # top/W decays
    HARD_DECAYS On; HARD_SPIN_CORRELATIONS 1;
    STABLE[24] 0;
    STABLE[6] 0;

    SPECIAL_TAU_SPIN_CORRELATIONS=1
    SOFT_SPIN_CORRELATIONS=1

}(run)

(processes){
    Process 93 93 -> 6 -6 24 93{NJET};
    Order (*,1);
    CKKW sqr(QCUT/E_CMS);
    NLO_QCD_Mode MC@NLO {LJET}; 
    ME_Generator Amegic {LJET};
    RS_ME_Generator Comix {LJET};
    Loop_Generator OpenLoops;
    Associated_Contributions EW|LO1|LO2|LO3 {LJET};
    End process;

    Process 93 93 -> 6 -6 -24 93{NJET};
    Order (*,1);
    CKKW sqr(QCUT/E_CMS);
    NLO_QCD_Mode MC@NLO {LJET}; 
    ME_Generator Amegic {LJET};
    RS_ME_Generator Comix {LJET};
    Loop_Generator OpenLoops;
    Associated_Contributions EW|LO1|LO2|LO3 {LJET};
    End process;
}(processes)
"""

from GeneratorFilters.GeneratorFiltersConf import MultiElecMuTauFilter
filtSeq += MultiElecMuTauFilter("ElecMuTauFilter")
filtSeq.ElecMuTauFilter.NLeptons = 2
filtSeq.ElecMuTauFilter.IncludeHadTaus = True
filtSeq.ElecMuTauFilter.MinVisPtHadTau = 7000.
filtSeq.ElecMuTauFilter.MinPt = 7000. #Each lepton must be at least 7GeV 
filtSeq.ElecMuTauFilter.MaxEta = 2.8

genSeq.Sherpa_i.Parameters += [ "WIDTH[6]=0.0", "WIDTH[24]=0.0", "OL_PARAMETERS=ew_scheme=2 ew_renorm_scheme=1 write_parameters=1"]
genSeq.Sherpa_i.Parameters += [ "EW_SCHEME=3", "GF=1.166397e-5" ]

genSeq.Sherpa_i.NCores = 24
genSeq.Sherpa_i.OpenLoopsLibs = [ "ppwtt", "ppwttj", "ppwtt_ew", "ppwttj_ew" ]
