# Base JO to include for using the DecayToSUEP Pythia8 user hook
#
# This fragment is intended to be included from a main jobOptions file whose name determine the setup.
#
# The main jobOptions file can just include this general jobOptions fragment for the setup, as:
# include('Py8_HtoSUEP_prod_decay_mH_mPhi_T.py')
#
# Main jobOption name convention:
#  mc.Py8_HtoSUEP_[prod]_[decay]_[scalarMass]_[darkMesonMass]_[darkTemperature].py
# where:
#  prod = ggH | VH | VBF | ttH -- Higgs production mechanism
#  decay = lep | had | lephad | ele -- decay of dark photon, see below
#  scalarMass [GeV] -- mass of Higgs-like particle to produce via ggH (use 'p' for '.')
#  darkMesonMass [GeV] -- mass of the dark-mesons the Higgs-like particle decays to (use 'p' for '.')
#  darkTemperature [GeV] -- Temperature parameter for the strongly-coupled theory (use 'p' for '.')
#


# General description
evgenConfig.description = "Higgs(-like) production with decay to SUEP via Pythia8 UserHook"
evgenConfig.keywords = ["Higgs", "SUEP", "EXOT"]
evgenConfig.contact = ['simone.pagan.griso@cern.ch', 'simon.knapen@cern.ch']


# Include base Pythia8 common jobOptions
include('Pythia8_i/Pythia8_A14_NNPDF23LO_Common.py')


# Get parameters from actual jobOptions configuration file name
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()
tokens = phys_short.split('_')

controlKeyword=str(tokens[1])
suepProd=str(tokens[2])
suepDecay=str(tokens[3])
scalarMass=str(tokens[4]).replace('p','.')
darkMesonMass=str(tokens[5]).replace('p','.')
darkTemperature=str(tokens[6]).replace('.py','').replace('p','.')


# Sanity check
if controlKeyword != 'HtoSUEP':
    raise Exception('ERROR: Invalid main jobOptions name, was expecting "mc.Py8_HtoSUEP_[prod]_[decay]_[scalarMass]_[darkMesonMass]_[darkTemperature].py". Aborting.')


# Decide what Higgs production mode to use
prodCommands= [ "HiggsSM:all = off" ]
if suepProd == 'ggH':
    prodCommands = [ "HiggsSM:gg2H = on" ]
elif suepProd == 'VH':
    # For VH production, force leptonic W/Z decays
    prodCommands = [ "HiggsSM:ffbar2HZ = on",
                     "HiggsSM:ffbar2HW = on",
                     "23:onMode = off",
                     "23:onIfAny = 11 13 15",
                     "24:onMode = off",
                     "24:onIfAny = 11 13 15"]
elif suepProd == 'VBF':
    prodCommands = [ "HiggsSM:ff2Hff(t:ZZ) = on",
                     "HiggsSM:ff2Hff(t:WW) = on" ]
elif suepProd == 'ttH':
    # Note: inclusive t/tbar decays!
    prodCommands = [ "HiggsSM:gg2Httbar = on",
                     "HiggsSM:qqbar2Httbar = on" ]
else:
    raise Exception('ERROR: Invalid Higgs production mechanism selected. Valid values = ggH, VH, VBF, ttH')


# Hard-coded dark meson decay chain in this card:
#  darkMeson -> 2 darkPhotons 
#  darkPhotons -> e+e- / mu+mu- / pi+pi- (BRs set depending on dark photon mass, see below)
# Note: BRs below are valid only for the given mass value, see Fig. 2 of arXiv:1505.07459
decayCommands = []
if suepDecay == 'lephad':
    darkPhotonMass = "0.5" ## GeV
    decayCommands += [ "999998:addChannel = 1 0.4 101 11 -11",
                       "999998:addChannel = 1 0.4 101 13 -13",
                       "999998:addChannel = 1 0.2 101 211 -211" ]
elif suepDecay == 'lep':
    darkPhotonMass = "0.3" ## GeV
    decayCommands += [ "999998:addChannel = 1 0.53 101 11 -11",
                       "999998:addChannel = 1 0.47 101 13 -13" ]    
elif suepDecay == 'had':
    darkPhotonMass = "0.7" ## GeV
    decayCommands += [ "999998:addChannel = 1 0.15 101 11 -11",
                       "999998:addChannel = 1 0.15 101 13 -13",
                       "999998:addChannel = 1 0.70 101 211 -211" ]
elif suepDecay == 'ele':
    darkPhotonMass = "0.04" ## GeV
    decayCommands += [ "999998:addChannel = 1 1.0 101 11 -11" ]
else:
    raise Exception('Invalid SUEP decay selected via jobOptions name. Valid values = lep,had,lephad,ele')


# Now add Pythia8 commands and user hook

# Production commands
# Note: enabling only one decay channel to make it safer to undo Higgs decay in the user hook
genSeq.Pythia8.Commands += prodCommands
genSeq.Pythia8.Commands += [ "25:m0 = "+scalarMass,
                             "25:onMode = off",
                             "25:0:onMode = on" ]

# Decay commands
genSeq.Pythia8.Commands += ["999999:all = GeneralResonance void 0 0 0 "+darkMesonMass+" 0.001 0.0 0.0 0.0",
                            "999998:all = GeneralResonance void 1 0 0 "+darkPhotonMass+" 0.001 0.0 0.0 0.0",
                            "999999:addChannel = 1 1.0 101 999998 999998"]
genSeq.Pythia8.Commands += decayCommands

# SUEP User Hook setup
genSeq.Pythia8.Commands += ["Check:event = off",
                            "DecayToSUEP:PDGId = 25",
                            "DecayToSUEP:Mass = "+scalarMass,
                            "DecayToSUEP:DarkMesonMass = "+darkMesonMass,
                            "DecayToSUEP:DarkTemperature="+darkTemperature]

genSeq.Pythia8.UserHooks += ["DecayToSUEP"]


# Finally, need to disable testHepMC energy/momentum balance checks, due to a small non-conservation introduced in the SUEP decay
testSeq.TestHepMC.EnergyImbalanceTest = False
testSeq.TestHepMC.MomImbalanceTest    = False
