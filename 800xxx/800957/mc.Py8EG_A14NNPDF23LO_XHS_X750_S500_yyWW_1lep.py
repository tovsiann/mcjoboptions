include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
evgenConfig.generators = ["Pythia8", "EvtGen"]
evgenConfig.process     = "gg->X->SH->WW+yy, 1 lepton"
evgenConfig.contact = ['Kaili Zhang <kazhang@cern.ch>']
evgenConfig.description = "Generation of gg > X > SH where S decays to W+W- with 1 lepton and H decays to yy"
evgenConfig.keywords = ["BSMHiggs"]

genSeq.Pythia8.Commands += ['Higgs:useBSM = on',
                            'ParticleDecays:mSafety = 0.0',
                            'HiggsBSM:gg2A3 = on',
                            'HiggsA3:parity = 1',
                            'Higgs:clipWings = off',
                            '36:m0 = 750.0',
                            '36:mWidth = 0.01',
                            '36:doForceWidth = yes',
                            '36:addChannel = 1 1 100 25 35',
                            '36:onMode = off',
                            '36:onIfMatch = 25 35',
                            '36:mayDecay = on',
                            '35:mMin = 50.0',
                            '25:mMin = 50.0',
                            '35:m0 = 500.0',
                            '35:mWidth = 0.01',
                            '35:doForceWidth = yes',
                            '25:onMode = off',
                            '25:onIfMatch = 22 22',
                            '35:onMode = off',
                            '35:onIfMatch = 24 -24',
                            ]


from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter
filtSeq += MultiLeptonFilter("LepOneFilter")
filtSeq.LepOneFilter.NLeptons = 1
filtSeq.LepOneFilter.Ptcut = 7000
filtSeq.LepOneFilter.Etacut = 3

filtSeq += MultiLeptonFilter("LepTwoFilter")
filtSeq.LepTwoFilter.NLeptons = 2
filtSeq.LepTwoFilter.Ptcut = 7000
filtSeq.LepTwoFilter.Etacut = 3
filtSeq.Expression = "LepOneFilter and not LepTwoFilter"