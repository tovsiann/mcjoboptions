
evgenConfig.description = "Excited Muon production: muStarMu -> muqQMu, mmuStar=700 Lambda=10000, Pythia8 with the A14 NNPDF23LO tune"
evgenConfig.process = "excitedMuon + muon -> muon + muon + quark + quark"
evgenConfig.keywords = ["BSM","exotic","excitedMuon","muon","quark"]
evgenConfig.contact = ["Alexei Myagkov <alexei.miagkov@cern.ch>"]

evgenConfig.nEventsPerJob = 10000

# Excited Lepton ID
leptID = 4000013

# Excited lepton Mass (in GeV)
M_ExLep = 700

# Excited lepton Width (in GeV)
W_ExLep = 2.515807E-02

# Mass Scale parameter (Lambda, in GeV)
M_Lam = 10000

# Coupling constants
f = 1.0
fPrime = 1.0

# Branching ratios
br_GI_gamma=2.486916E-01
br_GI_Z=1.149492E-01
br_GI_W=5.852993E-01
br_CI_ddbar=6.639619E-03
br_CI_uubar=6.640017E-03
br_CI_ssbar=6.640414E-03
br_CI_ccbar=6.639619E-03
br_CI_bbbar=6.637234E-03
br_CI_ttbar=2.369339E-03
br_CI_eebar=2.213405E-03
br_CI_nuenuebar=2.213445E-03
br_CI_mumubar=4.426810E-03
br_CI_numunumubar=2.213445E-03
br_CI_tautaubar=2.213087E-03
br_CI_nutaunutaubar=2.213445E-03


include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

if leptID > 0 :
    SMleptID=leptID-4000000
    if SMleptID% 2 == 1 :
        WleptID=-24
        SMPairleptID = SMleptID+1
    else                :
        SMPairleptID = SMleptID-1
        WleptID=24

else :
    SMleptID=leptID+4000000
    if SMleptID% 2 == 1 :
        WleptID=24
        SMPairleptID = SMleptID-1
    else                :
        WleptID=-24
        SMPairleptID = SMleptID+1

if leptID == 4000011 :
    genSeq.Pythia8.Commands  += ["ExcitedFermion:qqbar2eStare = on" ]
elif leptID == 4000013 :
    genSeq.Pythia8.Commands += ["ExcitedFermion:qqbar2muStarmu = on"]
elif leptID == 4000015 :
    genSeq.Pythia8.Commands += ["ExcitedFermion:qqbar2tauStartau = on"]
elif leptID == 4000012 :
    genSeq.Pythia8.Commands  += ["ExcitedFermion:qqbar2nueStarnue = on" ]
elif leptID == 4000014 :
    genSeq.Pythia8.Commands += ["ExcitedFermion:qqbar2numuStarnumu = on"]
elif leptID == 4000016 :
    genSeq.Pythia8.Commands += ["ExcitedFermion:qqbar2nutauStarnutau = on"]
else :
    from AthenaCommon.Logging import logging
    logging.error("Can not generate this process for particle "+str(leptID))
    
genSeq.Pythia8.Commands += [
     str(leptID)+":m0 = "+str(M_ExLep),
     str(leptID)+":mWidth = "+str(W_ExLep),
     str(leptID)+":doForceWidth = on",
    "ExcitedFermion:Lambda = "+str(M_Lam),
    "ExcitedFermion:coupF = "+str(f),
    "ExcitedFermion:coupFprime = "+str(fPrime),
    "ExcitedFermion:coupFcol = "+str(f),
    str(leptID)+":onMode = off",
    str(leptID)+':oneChannel = 0 '+str(br_GI_gamma)+' 102 22 '+str(SMleptID),
    str(leptID)+':addChannel = 0 '+str(br_GI_Z)+' 102 23 '+str(SMleptID),
    str(leptID)+':addChannel = 0 '+str(br_GI_W)+' 102 '+str(WleptID)+' '+str(SMPairleptID),
    str(leptID)+':addChannel = 1 '+str(br_CI_ddbar)+' 102 1 -1 '+str(SMleptID),
    str(leptID)+':addChannel = 1 '+str(br_CI_uubar)+' 102 2 -2 '+str(SMleptID),
    str(leptID)+':addChannel = 1 '+str(br_CI_ssbar)+' 102 3 -3 '+str(SMleptID),
    str(leptID)+':addChannel = 1 '+str(br_CI_ccbar)+' 102 4 -4 '+str(SMleptID),
    str(leptID)+':addChannel = 1 '+str(br_CI_bbbar)+' 102 5 -5 '+str(SMleptID),
    str(leptID)+':addChannel = 1 '+str(br_CI_ttbar)+' 102 6 -6 '+str(SMleptID),
    str(leptID)+':addChannel = 0 '+str(br_CI_eebar)+' 102 11 -11 '+str(SMleptID),
    str(leptID)+':addChannel = 0 '+str(br_CI_nuenuebar)+' 102 12 -12 '+str(SMleptID),
    str(leptID)+':addChannel = 0 '+str(br_CI_mumubar)+' 102 13 -13 '+str(SMleptID),
    str(leptID)+':addChannel = 0 '+str(br_CI_numunumubar)+' 102 14 -14 '+str(SMleptID),
    str(leptID)+':addChannel = 0 '+str(br_CI_tautaubar)+' 102 15 -15 '+str(SMleptID),
    str(leptID)+':addChannel = 0 '+str(br_CI_nutaunutaubar)+' 102 16 -16 '+str(SMleptID),
]
