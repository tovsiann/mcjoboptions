include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

evgenConfig.description = "Dijet truth jet slice JZ5, with the A14 NNPDF23 LO tune, with b hadron filter "
evgenConfig.keywords = ["QCD", "jets", "SM"]
evgenConfig.contact = ["simone.amoroso@cern.ch","alex.emerman@cern.ch"]
evgenConfig.nEventsPerJob = 2000

genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:Bias2Selection=on",
                            "PhaseSpace:pTHatMin = 350."]

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)    

AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.4)  
include("GeneratorFilters/JetFilter_JZX.py")
JZSlice(5,filtSeq)  

if not hasattr( filtSeq, "MultiCjetFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import MultiCjetFilter
  filtSeq += MultiCjetFilter()
  print ("MultiCjetFilter added ...")
  pass

filtSeq.MultiCjetFilter.NCJetsMin = 1 

if not hasattr( filtSeq, "MultiBjetFilter" ):
  from GeneratorFilters.GeneratorFiltersConf import MultiBjetFilter
  filtSeq += MultiBjetFilter()
  print ("MultiBjetFilter added ...")
  pass

filtSeq.MultiBjetFilter.NBJetsMin = 1

filtSeq.Expression = "(QCDTruthJetFilter) and (not MultiCjetFilter) and (not MultiBjetFilter)"
