include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
evgenConfig.generators = ["Pythia8", "EvtGen"]
evgenConfig.contact = ['Yesenia Hernandez <yesenia@cern.ch>']
evgenConfig.description = "Generation of gg > H > Sh where h,S goes to everything with a two lepton filter"
evgenConfig.keywords = ["BSMHiggs"]

genSeq.Pythia8.Commands += ['Higgs:useBSM = on',
                            'ParticleDecays:mSafety = 0.0',
                            'HiggsBSM:gg2A3 = on',
                            'Higgs:clipWings = off',
                            'HiggsA3:parity = 1',
                            '36:m0 = 350.0',
                            '36:mWidth = 0.01',
                            '36:doForceWidth = yes',
                            '36:addChannel = 1 1 100 25 35',
                            '36:onMode = off',
                            '36:onIfMatch = 25 35',
                            '36:mayDecay = on',
                            '35:mMin = 50.0',
                            '25:mMin = 50.0',
                            '35:m0 = 280.0',
                            '35:mWidth = 0.01',
                            '35:doForceWidth = yes'
                            ]

from GeneratorFilters.GeneratorFiltersConf import MultiLeptonFilter  
filtSeq += MultiLeptonFilter("MultiLeptonFilter")

MultiLeptonFilter = filtSeq.MultiLeptonFilter
MultiLeptonFilter.Ptcut = 8000.
MultiLeptonFilter.Etacut = 3.0
MultiLeptonFilter.NLeptons = 2

filtSeq.Expression = "(MultiLeptonFilter)"

