evgenConfig.description = "Inclusive pp->bb->J/psi(e3e13) production with Photos"
evgenConfig.keywords = ["egamma","charmonium","2electron","inclusive"]
evgenConfig.process  = "bb -> Jpsi -> ee"
evgenConfig.contact  = [ "ocariz@in2p3.fr", "jan.kretzschmar@cern.ch"  ]
evgenConfig.nEventsPerJob = 100

include('Pythia8B_i/Pythia8B_A14_CTEQ6L1_Common.py')
include('Pythia8B_i/Pythia8B_Photospp.py')
include("Pythia8B_i/Pythia8B_inclusiveBJpsi_Common.py")

genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 11']	
genSeq.Pythia8B.QuarkPtCut = 0.0
genSeq.Pythia8B.AntiQuarkPtCut = 0.0
genSeq.Pythia8B.QuarkEtaCut = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut = 102.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True

# Close all J/psi decays apart from J/psi->ee
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:1:onMode = on']

genSeq.Pythia8B.SignalPDGCodes = [443,-11,11]

genSeq.Pythia8B.NHadronizationLoops = 2

genSeq.Pythia8B.TriggerPDGCode = 11
genSeq.Pythia8B.TriggerStatePtCut = [13.0, 3.0]
genSeq.Pythia8B.TriggerStateEtaCut = 2.7
genSeq.Pythia8B.MinimumCountPerCut = [1, 2]
