evgenConfig.description = "Pythia8 e/gamma performance sample (jets, gamma+jet, W/Z, ttbar), with EM jet pT > 35 GeV"
evgenConfig.keywords = ["egamma", "performance", "jets", "photon", "electron", "QCD"]
evgenConfig.generators =  ["Pythia8", "EvtGen"]
evgenConfig.contact  = [ "ocariz@in2p3.fr", "jan.kretzschmar@cern.ch" ]
evgenConfig.nEventsPerJob = 10000

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PromptPhoton:qg2qgamma = on",
                            "PromptPhoton:qqbar2ggamma = on",
                            "WeakSingleBoson:all = on",
                            "Top:gg2ttbar = on",
                            "Top:qqbar2ttbar = on",
                            "PhaseSpace:pTHatMin = 33",
                            "PhaseSpace:mHatMin = 66"]

#include missing in rel 22
#include("GeneratorFilters/JetFilter.py")
if not hasattr( filtSeq, "JetFilter" ):
     from GeneratorFilters.GeneratorFiltersConf import JetFilter
     filtSeq += JetFilter()
     pass

filtSeq.JetFilter.JetNumber = 1
filtSeq.JetFilter.EtaRange = 2.7
filtSeq.JetFilter.JetType = False # True = cone, False = grid
filtSeq.JetFilter.GridSizeEta = 2 # Number of (approx 0.06 size) eta cells
filtSeq.JetFilter.GridSizePhi = 2 # Number of (approx 0.06 size) phi cells
filtSeq.JetFilter.JetThreshold = 35000.
