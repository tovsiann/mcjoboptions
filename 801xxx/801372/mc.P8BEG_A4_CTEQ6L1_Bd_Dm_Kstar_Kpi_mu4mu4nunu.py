##############################################################
# Job options for B0 -> D-(K*0 mu nu) mu nu
##############################################################

evgenConfig.description   = "Exclusive B0 -> D-(K*0(K+ pi-) mu- anti-nu_mu) mu+ nu_mu decay production"
evgenConfig.process       = "B0 -> D-(K*(Kpi)mu-nu)mu+nu"
evgenConfig.keywords      = [ "bottom", "B0", "2muon", "exclusive" ]
evgenConfig.contact       = [ "kin.yip.fung@cern.ch" ]
evgenConfig.nEventsPerJob = 50

# Create EvtGen decay rules
f = open("Bd_mu_nu_Dm_mu_nu_KstarKpi_USER.DEC","w")
f.write("Define dm_incohMix_B_s0 0.0e12\n")
f.write("Define dm_incohMix_B0 0.0e12\n")

f.write("Alias my_K*0 K*0\n")
f.write("Decay  my_K*0\n")
f.write("1.0000   K+ pi-   VSS;\n")
f.write("Enddecay\n")
f.write("Alias myD- D-\n")
f.write("Decay myD-\n")
f.write("1.000  my_K*0 mu- anti-nu_mu   PHOTOS ISGW2;\n")
f.write("Enddecay\n")
f.write("Decay B0\n")
f.write("1.000  myD- mu+ nu_mu   PHOTOS HQET2 1.185 1.081;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

include("Pythia8B_i/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")
include("GeneratorFilters/BSignalFilter.py")
include("Pythia8B_i/Pythia8B_BPDGCodes.py") # list of B-species

genSeq.Pythia8B.Commands                 += [ 'HardQCD:all = on' ] # Equivalent of MSEL1
genSeq.Pythia8B.Commands                 += [ 'ParticleDecays:mixB = off' ]
genSeq.Pythia8B.Commands                 += [ 'HadronLevel:all = off' ]
genSeq.Pythia8B.Commands                 += [ 'PhaseSpace:pTHatMin = 7' ]
genSeq.Pythia8B.QuarkPtCut                = 0.0
genSeq.Pythia8B.AntiQuarkPtCut            = 7.5
genSeq.Pythia8B.QuarkEtaCut               = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut           = 3.0
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.SelectBQuarks             = True
genSeq.Pythia8B.SelectCQuarks             = False
genSeq.Pythia8B.VetoDoubleBEvents         = True
genSeq.Pythia8B.VetoDoubleCEvents         = True

genSeq.Pythia8B.NHadronizationLoops = 4
genSeq.Pythia8B.NDecayLoops         = 1

# Final state selections
genSeq.Pythia8B.TriggerPDGCode = 0
genSeq.Pythia8B.SignalPDGCodes = [ 511 ]
genSeq.EvtInclusiveDecay.userDecayFile = "Bd_mu_nu_Dm_mu_nu_KstarKpi_USER.DEC"

filtSeq.BSignalFilter.LVL1MuonCutOn             = True
filtSeq.BSignalFilter.LVL2MuonCutOn             = True
filtSeq.BSignalFilter.LVL2MuonCutEta            = 2.7
filtSeq.BSignalFilter.LVL1MuonCutEta            = 2.7
filtSeq.BSignalFilter.LVL1MuonCutPT             = 4000.0
filtSeq.BSignalFilter.LVL2MuonCutPT             = 4000.0
filtSeq.BSignalFilter.B_PDGCode                 = 511
filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter.Cuts_Final_hadrons_pT     = 400.0
filtSeq.BSignalFilter.Cuts_Final_hadrons_eta    = 2.7

