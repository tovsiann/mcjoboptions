#Pythia8 minimum bias CD with A3

evgenConfig.description = "Central-diffractive events, with the A3 MSTW2008LO tune. Default PomFlux4"
evgenConfig.keywords = ["QCD", "minBias","diffraction","CD"]
evgenConfig.generators = ["Pythia8"]
evgenConfig.contact = ['ewelina@mail.desy.de']
evgenConfig.nEventsPerJob = 50

# include("../Pythia8_A3_NNPDF23LO_Common.py")
include("Pythia8_A3_ALT_NNPDF23LO_Common.py")

genSeq.Pythia8.Commands += ["SoftQCD:centralDiffractive = on"]
genSeq.Pythia8.Commands += ["SigmaTotal:zeroAXB = off"]

from GeneratorFilters.GeneratorFiltersConf import ForwardProtonFilter
if  "ForwardProtonFilter" not in filtSeq:
    filtSeq += ForwardProtonFilter()

filtSeq.ForwardProtonFilter.xi_min = 0.00
filtSeq.ForwardProtonFilter.xi_max = 0.20
filtSeq.ForwardProtonFilter.beam_energy = 6500.*GeV
filtSeq.ForwardProtonFilter.pt_min = 0.0*GeV
filtSeq.ForwardProtonFilter.pt_max = 1.5*GeV

include ("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.4)
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.4)

from AthenaCommon.SystemOfUnits import GeV        
from GeneratorFilters.GeneratorFiltersConf import QCDTruthMultiJetFilter
if "QCDTruthMultiJetFilter" not in filtSeq:
    filtSeq += QCDTruthMultiJetFilter()

filtSeq.QCDTruthMultiJetFilter.TruthJetContainer = "AntiKt4TruthJets"
filtSeq.QCDTruthMultiJetFilter.NjetMinPt = 12.*GeV
filtSeq.QCDTruthMultiJetFilter.Njet = 2
filtSeq.QCDTruthMultiJetFilter.DoShape = False
filtSeq.QCDTruthMultiJetFilter.MaxEta = 4.

