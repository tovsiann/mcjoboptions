evgenConfig.description = "Signal anti_B0 -> X+ (J/psi rho+) K-"
evgenConfig.keywords = ["bottom","exclusive","Jpsi","2muon"]
evgenConfig.contact = [ 'Semen.Turchikhin@cern.ch' ]
evgenConfig.nEventsPerJob = 200
 
include("Pythia8B_i/Pythia8B_A14_NNPDF23LO_Common.py")
include("Pythia8B_i/Pythia8B_exclusiveAntiB_Common.py")
 
# Event selection
genSeq.Pythia8B.Commands += ['PhaseSpace:pTHatMin = 8.']
genSeq.Pythia8B.QuarkPtCut = 8.0
genSeq.Pythia8B.AntiQuarkPtCut = 0.0
genSeq.Pythia8B.QuarkEtaCut = 3.5
genSeq.Pythia8B.AntiQuarkEtaCut = 102.5
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
 
genSeq.Pythia8B.NHadronizationLoops = 1

# X+ resonance
genSeq.Pythia8B.Commands += ['9920443:new = X+ X- 3 3 0 3.871165 0.00119 3.870 3.880 0.'] # PDG 2020+ chi_c1(3872)
genSeq.Pythia8B.Commands += ['9920443:onMode = off'] 
genSeq.Pythia8B.Commands += ['9920443:oneChannel = on 1. 0 443 213'] # onMode bRatio meMode products
 
# J/psi:
genSeq.Pythia8B.Commands += ['443:m0 = 3.096900']  # PDG 2020+
genSeq.Pythia8B.Commands += ['443:mWidth = 0.0000926'] # PDG 2020+
genSeq.Pythia8B.Commands += ['443:onMode = off']
genSeq.Pythia8B.Commands += ['443:2:onMode = on']

# rho+
genSeq.Pythia8B.Commands += ['213:m0 = 0.77511']  # PDG 2020+
genSeq.Pythia8B.Commands += ['213:mWidth = 0.1491'] # PDG 2020+
 
# B0 decay:
genSeq.Pythia8B.Commands += ['511:addChannel = 3 1.0 0 -9920443 321']
 
genSeq.Pythia8B.TriggerPDGCode = 13
genSeq.Pythia8B.TriggerStatePtCut = [3.5]
genSeq.Pythia8B.TriggerStateEtaCut = 2.6
genSeq.Pythia8B.MinimumCountPerCut = [2]

