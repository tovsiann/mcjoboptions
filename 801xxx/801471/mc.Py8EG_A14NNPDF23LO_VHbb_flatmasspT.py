evgenConfig.description = "VH->mumubb with A14 tune and NNPDF23LO PDF"
evgenConfig.process = "H -> b + bbar"
evgenConfig.contact = ["fdibello@cern.ch"] 
evgenConfig.keywords    = [ 'SM','Higgs', 'resonance', 'electroweak']
evgenConfig.generators += [ 'Pythia8' ]
evgenConfig.generators += ["EvtGen"]
evgenConfig.nEventsPerJob = 10000

if runArgs.trfSubstepName == 'generate' :
  include( "Pythia8_i/Pythia8_A14_NNPDF23LO_Common.py" )
  genSeq.Pythia8.Commands += ["HiggsSM:ffbar2HZ = on",
                              "25:m0 = 130.0",
                              "25:mWidth = 400.0",
                              "25:doForceWidth = true ",
                              "25:mMax = 200 ",
                              "25:mMin = 50 ",
                              "25:onMode = off",
                              "25:onIfAny = 5",
                              "23:onMode = off",
                              "23:onIfAny = 13",
                              "PhaseSpace:bias2Selection = on",
                              "PhaseSpace:bias2SelectionPow = 5."]


from GeneratorFilters.GeneratorFiltersConf import ParticleFilter
filtSeq += ParticleFilter("ParticleFilter")
filtSeq.ParticleFilter.Ptcut = 200.*GeV
filtSeq.ParticleFilter.Etacut = 10.0
filtSeq.ParticleFilter.StatusReq = -1
filtSeq.ParticleFilter.PDG = 25
filtSeq.ParticleFilter.MinParts = 1
filtSeq.ParticleFilter.Exclusive = True
