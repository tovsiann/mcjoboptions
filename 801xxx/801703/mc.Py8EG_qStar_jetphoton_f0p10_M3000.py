evgenConfig.description = "PYTHIA 8 q* -> gamma+jet, q* mass = lambda = 3000. GeV, f = 0.10"
evgenConfig.keywords    = ["exotic", "excitedQuark", "photon", "jets"]
evgenConfig.contact     = ["francisco.alonso@cern.ch"]
evgenConfig.process     = "q* -> gamma+jet"

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += [
    "ExcitedFermion:dg2dStar = on",             # switch on dg -> d*
    "ExcitedFermion:ug2uStar = on",             # switch on ug -> u*
    "ExcitedFermion:Lambda = 3000.",           # Compositness scale
    "4000001:m0 = 3000.",                      # d* mass
    "4000002:m0 = 3000.",                      # u* mass
    "4000001:onMode = off",                     # switch off all d* decays
    "4000001:onIfAny = 22",                     # switch on d*->gamma+X decays
    "4000002:onMode = off",                     # switch off all u* decays
    "4000002:onIfAny = 22",                     # switch on u*->gamma+X decays
    
    "ExcitedFermion:coupF = 0.10",        # coupling strength of SU(2)
    "ExcitedFermion:coupFprime = 0.10",   # coupling strength of U(1)
    "ExcitedFermion:coupFcol = 0.10"      # coupling strength of SU(3)
]
