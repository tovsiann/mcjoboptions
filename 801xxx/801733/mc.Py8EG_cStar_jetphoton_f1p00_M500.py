evgenConfig.description = "PYTHIA 8 c* -> gamma+jet, c* mass = lambda = 500. GeV, f = 1.00"
evgenConfig.keywords    = ["exotic", "excitedQuark", "photon", "jets"]
evgenConfig.contact     = ["francisco.alonso@cern.ch"]
evgenConfig.process     = "c* -> gamma+jet"

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

genSeq.Pythia8.Commands += [
    "ExcitedFermion:cg2cStar = on",             # switch on cg -> c*
    "ExcitedFermion:Lambda = 500.",           # Compositness scale
    "4000004:m0 = 500.",                      # c* mass
    "4000004:onMode = off",                     # switch off all c* decays
    "4000004:onIfAny = 22",                     # switch on c*->gamma+X decays
    
    "ExcitedFermion:coupF = 1.00",        # coupling strength of SU(2)
    "ExcitedFermion:coupFprime = 1.00",   # coupling strength of U(1)
    "ExcitedFermion:coupFcol = 1.00"      # coupling strength of SU(3)
]
