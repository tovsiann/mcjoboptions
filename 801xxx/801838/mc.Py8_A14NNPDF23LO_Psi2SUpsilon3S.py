#--------------------------------------------------------------
#JHUgen+Pythia8 pp/gg ->  eta_b -> 2 J/Psi ->4mu production with JP=0-
#--------------------------------------------------------------
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1
evgenConfig.description = "HelacOnia+Pythia8 pp/gg-> SPS J/Psi + Upsilon(3S) -> 4mu/2mu2e production"
evgenConfig.keywords = ["Jpsi","Upsilon","4muon"]
evgenConfig.contact = ["hpang@cern.ch"]
evgenConfig.process = "pp/gg-> SPS J/Psi + Upsilon(3S) ->4mu/2mu2e production"
evgenConfig.generators += ['Lhef']
#runArgs.inputGeneratorFile = runName+'*.tar.gz'

#include("Pythia8B_i/Pythia8B_A14_NNPDF23LO_Common.py")
#include("Pythia8B_i/Pythia8B_Photospp.py")
#include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
#include("Pythia8_i/Pythia8_Photospp.py")
#include("Pythia8_i/Pythia8_LHEF.py")
include('Pythia8_i/Pythia8_A14_NNPDF23LO_Common.py')
include("Pythia8_i/Pythia8_Photospp.py")
include('Pythia8_i/Pythia8_LHEF.py')



genSeq.Pythia8.Commands += [
    "200553:onMode = off", # Upsilon(3S)
    "200553:onIfMatch = 11 11", # ee
    "200553:onIfMatch = 13 13", # mumu
    "200553:onIfMatch = 15 15", # tautau (dummy)
    "200553:onIfMatch = 553 111 111", # Upsilon(1S) pi0 pi0
    "200553:onIfMatch = 553 211 211", # Upsilon(1S) pi- pi+
    "200553:onIfMatch = 100553 111 111", # Upsilon(2S) pi0 pi0
    "200553:onIfMatch = 100553 211 211", # Upsilon(2S) pi- pi+
    "200553:onIfMatch = 100553 22 22", # Upsilon(2S) gam gam
    "200553:0:bRatio = 0.336", # ee
    "200553:1:bRatio = 0.336", # mumu
    "200553:2:bRatio = 0.165", # tautau (dummy)
    "200553:3:bRatio = 0.022", # Upsilon(1S) pi0 pi0
    "200553:4:bRatio = 0.044", # Upsilon(1S) pi- pi+
    "200553:5:bRatio = 0.019", # Upsilon(2S) pi0 pi0
    "200553:6:bRatio = 0.028", # Upsilon(2S) pi- pi+
    "200553:7:bRatio = 0.050", # Upsilon(2S) gam gam

    "100553:onMode = off", # Upsilon(2S)
    "100553:onIfMatch = 11 11", # ee
    "100553:onIfMatch = 13 13", # mumu
    "100553:onIfMatch = 555 22", # chi_2b gam (chi_2b -> Upsilon(1S) gam)
    "100553:onIfMatch = 20553 22", # chi_1b gam (chi_1b -> Upsilon(1S) gam)
    "100553:onIfMatch = 553 111 111", # Upsilon(1S) pi0 pi0
    "100553:onIfMatch = 553 211 211", # Upsilon(1S) pi- pi+
    "100553:0:bRatio = 0.294", # ee
    "100553:1:bRatio = 0.298", # mumu
    "100553:3:bRatio = 0.067", # chi_2b gam (chi_2b -> Upsilon(1S) gam)
    "100553:5:bRatio = 0.066", # chi_1b gam (chi_1b -> Upsilon(1S) gam)
    "100553:6:bRatio = 0.090", # Upsilon(1S) pi0 pi0
    "100553:7:bRatio = 0.185", # Upsilon(1S) pi- pi+

    "553:onMode = off", # Upsilon(1S)
    "553:onIfMatch = 11 11", # ee
    "553:onIfMatch = 13 13", # mumu
    "553:onIfMatch = 15 15", # tautau (dummy)
    "553:2:bRatio = 0.367", # ee
    "553:3:bRatio = 0.382", # mumu
    "553:4:bRatio = 0.251", # tautau (dummy)


        "100443:onMode = off",
        "100443:onIfMatch = 11 11", # ee
        "100443:onIfMatch = 13 13", # mumu
        "100443:onIfMatch = 15 15", # tautau(dummy)
        "100443:onIfAny = 443", # Jpsi
        "100443:onIfAny = 445", # chi_2c
        "100443:onIfAny = 10441", # chi_0c
        "100443:onIfAny = 10443", # h_1c
        "100443:onIfAny = 20443", # chi_1c
        "100443:0:bRatio = 0.06144", # ee
        "100443:1:bRatio = 0.06144", # mumu
        "100443:2:bRatio = 0.06402", # tautau(dummy)
        "100443:21:bRatio = 0.0013", # Jpsi+pi0
        "100443:22:bRatio = 0.0324", # Jpsi+eta
        "100443:43:bRatio = 0.1658", # Jpsi+2pi0
        "100443:44:bRatio = 0.3366", # Jpsi+2pi
        "100443:23:bRatio = 0.0933", # chi_2c+gamma
        "100443:35:bRatio = 0.0922", # chi_0c+gamma
        "100443:36:bRatio = 0.0008", # h_1c+pi0
        "100443:37:bRatio = 0.0907", # chi_1c+gamma
        "443:onMode = off", #
        "443:onIfMatch = 11 11",
        "443:onIfMatch = 13 13",
        "443:1:bRatio = 0.5",
        "443:2:bRatio = 0.5" 
        
]


### Set lepton filters
if not hasattr(filtSeq, "MultiMuonFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
   fourmuonfilter1 = MultiMuonFilter("fourmuonfilter1")
   fourmuonfilter2 = MultiMuonFilter("fourmuonfilter2")
   fourmuonfilter3 = MultiMuonFilter("fourmuonfilter3")
   filtSeq += fourmuonfilter1
   filtSeq += fourmuonfilter2
   filtSeq += fourmuonfilter3

filtSeq.fourmuonfilter1.Ptcut = 1500.0 #MeV
filtSeq.fourmuonfilter1.Etacut = 2.7
filtSeq.fourmuonfilter1.NMuons = 4 #minimum

filtSeq.fourmuonfilter2.Ptcut = 2500.0 #MeV
filtSeq.fourmuonfilter2.Etacut = 2.7
filtSeq.fourmuonfilter2.NMuons = 3 #minimum

filtSeq.fourmuonfilter3.Ptcut = 3500.0 #MeV
filtSeq.fourmuonfilter3.Etacut = 2.7
filtSeq.fourmuonfilter3.NMuons = 2 #minimum

if not hasattr(filtSeq, "MultiMuonFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
   twomuonfilter1 = MultiMuonFilter("twomuonfilter1")
   twomuonfilter2 = MultiMuonFilter("twomuonfilter2")
   filtSeq += twomuonfilter1
   filtSeq += twomuonfilter2

filtSeq.twomuonfilter1.Ptcut = 1500.0 #MeV
filtSeq.twomuonfilter1.Etacut = 2.7
filtSeq.twomuonfilter1.NMuons = 2 #minimum

filtSeq.twomuonfilter2.Ptcut = 2500.0 #MeV
filtSeq.twomuonfilter2.Etacut = 2.7
filtSeq.twomuonfilter2.NMuons = 1 #minimum

if not hasattr(filtSeq, "MultiElectronFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import MultiElectronFilter
   twoelectronfilter1 = MultiElectronFilter("twoelectronfilter1")
   filtSeq += twoelectronfilter1

filtSeq.twoelectronfilter1.Ptcut = 3500.0 #MeV
filtSeq.twoelectronfilter1.Etacut = 2.7
filtSeq.twoelectronfilter1.NElectrons = 2 #minimum

filtSeq.Expression = "(fourmuonfilter1 and fourmuonfilter2 and fourmuonfilter3) or (twomuonfilter1 and twomuonfilter2 and twoelectronfilter1)"

