##############################################################
# Job options fragment for B+->J/psi(mu3p5mu3p5)K+
##############################################################

evgenConfig.description   = "Exclusive Bplus->Jpsi_mu3p5mu3p5_Kplus production"
evgenConfig.process       = "B+ -> J/psi(mu+mu-) K+"
evgenConfig.keywords      = [ "bottom", "Bplus", "Jpsi", "2muon", "exclusive" ]
evgenConfig.contact       = [ "pavel.reznicek@cern.ch" ]
evgenConfig.nEventsPerJob = 1000

# Create EvtGen decay rules
f = open("BU_JPSI_K_USER.DEC","w")
f.write("Particle  B+ 5.27929 0.0\n") # PDG2014 mass
f.write("Particle  B- 5.27929 0.0\n") # PDG2014 mass
f.write("Define dm_incohMix_B0 0.0\n") #disable neutral meson mixing
f.write("Define dm_incohMix_B_s0 0.0\n") #same, LEAVE AS B_s0
f.write("Alias myJ/psi J/psi\n")
f.write("Decay B+\n")
f.write("1.0000  myJ/psi   K+             SVS;\n")
f.write("Enddecay\n")
f.write("Decay myJ/psi\n")
f.write("1.0000    mu+  mu-             VLL;\n")
f.write("Enddecay\n")
f.write("End\n")
f.close()

include("Pythia8B_i/Pythia8B_A14_CTEQ6L1_EvtGen_Common.py")
include("GeneratorFilters/BSignalFilter.py")
include("Pythia8B_i/Pythia8B_BPDGCodes.py") # list of B-species

genSeq.Pythia8B.Commands                 += [ 'HardQCD:all = on' ] # Equivalent of MSEL1
genSeq.Pythia8B.Commands                 += [ 'ParticleDecays:mixB = off' ]
genSeq.Pythia8B.Commands                 += [ 'HadronLevel:all = off' ]
genSeq.Pythia8B.Commands                 += [ 'PhaseSpace:pTHatMin = 7' ]
genSeq.Pythia8B.QuarkPtCut                = 0.0
genSeq.Pythia8B.AntiQuarkPtCut            = 7.0
genSeq.Pythia8B.QuarkEtaCut               = 102.5
genSeq.Pythia8B.AntiQuarkEtaCut           = 2.6
genSeq.Pythia8B.RequireBothQuarksPassCuts = True
genSeq.Pythia8B.SelectBQuarks             = True
genSeq.Pythia8B.SelectCQuarks             = False
genSeq.Pythia8B.VetoDoubleBEvents         = True
genSeq.Pythia8B.VetoDoubleCEvents         = True
genSeq.Pythia8B.Commands += ['521:m0 = 5.27929'] # PDG2014 mass

genSeq.Pythia8B.NHadronizationLoops = 1
genSeq.Pythia8B.NDecayLoops         = 1

# Final state selections
genSeq.Pythia8B.TriggerPDGCode = 0
genSeq.Pythia8B.SignalPDGCodes = [ 521 ]
genSeq.EvtInclusiveDecay.userDecayFile = "BU_JPSI_K_USER.DEC"

filtSeq.BSignalFilter.LVL1MuonCutOn             = True
filtSeq.BSignalFilter.LVL2MuonCutOn             = True
filtSeq.BSignalFilter.LVL2MuonCutEta            = 2.6
filtSeq.BSignalFilter.LVL1MuonCutEta            = 2.6
filtSeq.BSignalFilter.LVL1MuonCutPT             = 3500.0
filtSeq.BSignalFilter.LVL2MuonCutPT             = 3500.0
filtSeq.BSignalFilter.B_PDGCode                 = 521
filtSeq.BSignalFilter.Cuts_Final_hadrons_switch = True
filtSeq.BSignalFilter.Cuts_Final_hadrons_pT     = 900.0
filtSeq.BSignalFilter.Cuts_Final_hadrons_eta    = 2.6
