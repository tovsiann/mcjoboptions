evgenConfig.description = "Pythia8 non diffractive minimum bias events with the A3 NNPDF23LO tune and EvtGen, no filters."
evgenConfig.keywords = ["QCD", "minBias", "SM"]
evgenConfig.contact  = [ "jeff.dandoy@cern.ch", "jan.kretzschmar@cern.ch", "judith.katzy@cern.ch"]
evgenConfig.nEventsPerJob = 2000

#include("Pythia8_i/Pythia8_Base_Fragment.py")
include("Pythia8_i/Pythia8_A3_NNPDF23LO_EvtGen_Common.py")

evgenConfig.tune = "A3 NNPDF23LO"
genSeq.Pythia8.Commands += [ "SoftQCD:nonDiffractive = on"]

#--------------------------------------------------------------
# FILTERS
#--------------------------------------------------------------

include('GeneratorFilters/ChargedTrackFilter.py')
filtSeq.ChargedTracksFilter.NTracks = 120
