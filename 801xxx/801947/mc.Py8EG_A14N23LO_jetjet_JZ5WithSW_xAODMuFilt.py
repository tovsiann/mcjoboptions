include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_ShowerWeights.py")

evgenConfig.description = "Dijet truth jet slice JZ5, with the A14 NNPDF23 LO tune, mu-filtered"
evgenConfig.keywords = ["QCD", "jets", "SM"]
evgenConfig.contact = ["yi.yu@cern.ch"]

genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:Bias2Selection=on",
                            "PhaseSpace:pTHatMin = 350."]

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.6)    
AddJetsFilter(filtSeq,runArgs.ecmEnergy, 0.6)  
include("GeneratorFilters/JetFilter_JZX.py")
JZSlice(5,filtSeq) 

include("GeneratorFilters/xAODMuonFilter_Common.py")
filtSeq.xAODMuonFilter.Ptcut = 3000.0
filtSeq.xAODMuonFilter.Etacut = 2.8

evgenConfig.nEventsPerJob = 10000
