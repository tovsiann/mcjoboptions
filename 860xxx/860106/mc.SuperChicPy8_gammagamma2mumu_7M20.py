import Superchic_i.LheConverter as LC
# import Superchic_i.EventFiller as EF
from Superchic_i.SuperChicUtils import SuperChicConfig, SuperChicRun

evgenConfig.description = 'Superchic gamma + gamma to mu mu UPC collisions at 5020 GeV'
evgenConfig.keywords = ['2photon', '2lepton']
# evgenConfig.weighting = 0
evgenConfig.contact = ['mateusz.dyndal@cern.ch']
evgenConfig.generators += ['Superchic']
evgenConfig.nEventsPerJob = 1000

scConfig = SuperChicConfig(runArgs)

scConfig.isurv = 4
scConfig.PDFname = 'MMHT2015qed_nnlo'
scConfig.PDFmember = 0
scConfig.proc = 57
scConfig.beam = 'ion'
scConfig.sfaci = True
scConfig.diff = 'el'
scConfig.genunw = True
scConfig.ymin = -9.0
scConfig.ymax = 9.0
scConfig.mmin = 7
scConfig.mmax = 20
scConfig.gencuts = True
scConfig.scorr = True
scConfig.fwidth = True
scConfig.ptxmax = 10000000000
scConfig.ptamin = 3.5
scConfig.ptbmin = 3.5
scConfig.etaamin = -2.7
scConfig.etaamax = 2.7
scConfig.etabmin = -2.7
scConfig.etabmax = 2.7
scConfig.acoabmax = 100000000000
scConfig.mcharg = 120
scConfig.mneut = 100

SuperChicRun(scConfig, genSeq)

# ef = EF.LheEVNTFiller()
# ef.fileName = scConfig.outputLHEFile()
# ef.outputFileName = 'outputs/output' + scConfig.outtg + '.dat'
# genSeq += ef

lc = LC.LheConverter()
lc.fileName = scConfig.outputLHEFile()
genSeq += lc

include('Pythia8_i/Pythia8_A14_NNPDF23LO_Common.py')
include('Pythia8_i/Pythia8_LHEF.py')
genSeq.Pythia8.Commands += ['SpaceShower:QEDshowerByL = 1']

