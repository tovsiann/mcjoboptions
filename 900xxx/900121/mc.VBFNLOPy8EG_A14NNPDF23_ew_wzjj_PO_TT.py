include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_LHEF.py")
genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']

evgenConfig.generators = ["VBFNLO", "Pythia8", "EvtGen"]
evgenConfig.description = 'VBFNLO_WZVBS_LO'
evgenConfig.keywords = ['diboson','WZ','electron','muon','jets']
evgenConfig.contact = ["Lucia.Di.Ciaccio@cern.ch"]


evgenConfig.nEventsPerJob = 10000
