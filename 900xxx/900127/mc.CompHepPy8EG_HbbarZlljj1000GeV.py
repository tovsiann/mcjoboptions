#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.generators += [ "Lhef", "Pythia8", "EvtGen" ] 
evgenConfig.description = "E6 IsoSinglet Quark Pair Production"
evgenConfig.keywords = ["exotic"]
evgenConfig.contact = ['Aytul Adiguzel <Aytul.Adiguzel@cern.ch>']
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 2 

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_LHEF.py")

genSeq.Pythia8.Commands += [ "25:onMode = off","25:onIfAny = 5",         # decay of Higgs
                             "23:onMode = off","23:onIfAny = 11 13" ]    # decay of Z
