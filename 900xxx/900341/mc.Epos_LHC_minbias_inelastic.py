evgenConfig.description = "EPOS inelastic minimum bias events for pileup, no filters."
evgenConfig.keywords = ["QCD", "minBias" , "SM"]
evgenConfig.contact  = [ "jeff.dandoy@cern.ch", "jan.kretzschmar@cern.ch" ]
evgenConfig.nEventsPerJob = 50000
evgenConfig.tune = "EPOS LHC"

include("Epos_i/Epos_Base_Fragment.py")

