evgenConfig.description = "Single electrons in FCAL acceptance at fixed energy"
evgenConfig.keywords = ["singleParticle"] 
evgenConfig.contact  = ["tommaso.lari@cern.ch"]
evgenConfig.nEventsPerJob = 10000

include("ParticleGun/ParticleGun_Common.py")

import ParticleGun as PG
topSeq += PG.ParticleGun()
topSeq.ParticleGun.sampler.pid = 11
topSeq.ParticleGun.sampler.mom = PG.EEtaMPhiSampler(energy=1000000., eta=[-5.0,-3.0, 3.0,5.0])
