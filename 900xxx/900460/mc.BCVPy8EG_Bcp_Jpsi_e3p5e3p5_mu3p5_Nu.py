evgenConfig.description = "Bc+ -> J/psi(ee) mu+ nu_e sample with BCVEGPY"
evgenConfig.contact = ["Semen.Turchikhin@cern.ch"]
evgenConfig.keywords = ["exclusive","Jpsi","2electron"]
evgenConfig.nEventsPerJob = 5000

include("Pythia8_i/Pythia8_A14_CTEQ6L1_EvtGen_Common.py")
include("Pythia8_i/Pythia8_BCVEGPY.py")
include("Pythia8_PDG2020Masses.py")
include("Pythia8_BcStates_v2.py")

genSeq.EvtInclusiveDecay.whiteList+=[100541, 100543, -100541, -100543]
genSeq.EvtInclusiveDecay.userDecayFile = "Bc_JpsiEE_Mu_Nu.dec"
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8_BcPDG20.pdt"
evgenConfig.auxfiles += [genSeq.EvtInclusiveDecay.pdtFile,genSeq.EvtInclusiveDecay.userDecayFile]

from GeneratorFilters.GeneratorFiltersConf import MultiElectronFilter     
filtSeq += MultiElectronFilter("TwoElectronFilter")
from GeneratorFilters.GeneratorFiltersConf import MuonFilter     
filtSeq += MuonFilter("MuonFilter")

TwoElectronFilter = filtSeq.TwoElectronFilter
TwoElectronFilter.Ptcut = 3500.
TwoElectronFilter.Etacut = 2.7
TwoElectronFilter.NElectrons = 2

MuonFilter.Ptcut = 3500.
MuonFilter.Etacut = 2.7
