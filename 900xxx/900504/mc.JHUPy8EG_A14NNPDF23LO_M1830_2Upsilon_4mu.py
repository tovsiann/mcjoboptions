#--------------------------------------------------------------
#JHUgen+Pythia8 pp/gg ->  18.3 -> 2 Upsilon ->4mu production
#--------------------------------------------------------------
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 11
evgenConfig.description = "JHUGen+Pythia8 pp/gg-> 18p3 -> 2 Upsilon ->4mu production"
evgenConfig.keywords = ["heavyFlavour","Upsilon","Muon"]
evgenConfig.contact = ["yue.xu@cern.ch"]
evgenConfig.process = "pp/gg-> 18p3 -> 2 Upsilon ->4mu production"
evgenConfig.generators += ['Lhef']

include('Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include("Pythia8_i/Pythia8_Photospp.py")
include("Pythia8_i/Pythia8_LHEF.py")

### Set lepton filters
if not hasattr(filtSeq, "MultiMuonFilter" ):
   from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter
   muonfilter1 = MultiMuonFilter("muonfilter1")
   muonfilter2 = MultiMuonFilter("muonfilter2")
   muonfilter3 = MultiMuonFilter("muonfilter3")
   filtSeq += muonfilter1
   filtSeq += muonfilter2
   filtSeq += muonfilter3

filtSeq.muonfilter1.Ptcut = 1500.0 #MeV
filtSeq.muonfilter1.Etacut = 2.7
filtSeq.muonfilter1.NMuons = 4 #minimum

filtSeq.muonfilter2.Ptcut = 2500.0 #MeV
filtSeq.muonfilter2.Etacut = 2.7
filtSeq.muonfilter2.NMuons = 3 #minimum

filtSeq.muonfilter3.Ptcut = 3500.0 #MeV
filtSeq.muonfilter3.Etacut = 2.7
filtSeq.muonfilter3.NMuons = 2 #minimum
