evgenConfig.description = "Bc*+ -> Bc+ -> J/psi(mumu) mu+ nu_mu sample with BCVEGPY"
evgenConfig.contact = ["Semen.Turchikhin@cern.ch"]
evgenConfig.keywords = ["exclusive","Jpsi","2muon"]
evgenConfig.nEventsPerJob = 10000

include("Pythia8_i/Pythia8_A14_CTEQ6L1_EvtGen_Common.py")
include("Pythia8_i/Pythia8_BCVEGPY.py")
include("Pythia8_PDG2020Masses.py")
include("Pythia8_BcStates_v2.py")

genSeq.EvtInclusiveDecay.whiteList+=[100541, 100543, -100541, -100543]
genSeq.EvtInclusiveDecay.userDecayFile = "BcStp_Bcp_Jpsi_mu_inclusive.dec"
genSeq.EvtInclusiveDecay.pdtFile = "inclusiveP8_BcPDG20.pdt"
evgenConfig.auxfiles += [genSeq.EvtInclusiveDecay.pdtFile,genSeq.EvtInclusiveDecay.userDecayFile]

from GeneratorFilters.GeneratorFiltersConf import MultiMuonFilter     
filtSeq += MultiMuonFilter("ThreeMuonFilter")

ThreeMuonFilter = filtSeq.ThreeMuonFilter
ThreeMuonFilter.Ptcut = 3500.
ThreeMuonFilter.Etacut = 2.7
ThreeMuonFilter.NMuons = 3

from GeneratorFilters.GeneratorFiltersConf import ParentChildwStatusFilter
filtSeq += ParentChildwStatusFilter("ParentChildwStatusFilter")

## Example usage for Z leptonic decay from Pythia8
filtSeq.ParentChildwStatusFilter.PDGParent  = [543]
filtSeq.ParentChildwStatusFilter.StatusParent = [2]
#filtSeq.ParentChildwStatusFilter.PtMinParent =  -1e12
#filtSeq.ParentChildwStatusFilter.PtMaxParent = 1e12
#filtSeq.ParentChildwStatusFilter.EtaRangeParent = 1e12
#filtSeq.ParentChildwStatusFilter.MassMinParent = -1e12
#filtSeq.ParentChildwStatusFilter.MassMaxParent = 1e12
filtSeq.ParentChildwStatusFilter.PDGChild = [22]
filtSeq.ParentChildwStatusFilter.PtMinChild = 400.
#filtSeq.ParentChildwStatusFilter.EtaRangeChild = 1e12
