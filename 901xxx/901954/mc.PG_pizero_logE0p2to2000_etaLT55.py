evgenConfig.description = "Single neutral pion with log energy distribution and |eta| up to 5.5"
evgenConfig.keywords = ["singleParticle"]
evgenConfig.contact = ["lderamo@cern.ch"] 
evgenConfig.nEventsPerJob = 10000

import ParticleGun as PG
genSeq += PG.ParticleGun()
evgenConfig.generators += ['ParticleGun']
genSeq.ParticleGun.sampler.pid = (111)
genSeq.ParticleGun.sampler.mom = PG.EEtaMPhiSampler(energy=PG.LogSampler(200, 2000000.), eta=[-5.5, 5.5])


