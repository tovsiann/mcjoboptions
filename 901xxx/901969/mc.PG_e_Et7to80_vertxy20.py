evgenConfig.description = "Single electrons with flat ET in [7,80] GeV, vertex (x,y) flat in [-20,20] mm, vertex z = Gaus(0,40 mm)"
evgenConfig.keywords = ["singleParticle"]
evgenConfig.contact = ["martindl@cern.ch"]
evgenConfig.nEventsPerJob = 10000
evgenConfig.generators += ['ParticleGun']

import ParticleGun as PG
pg = PG.ParticleGun()
pg.sampler.pid = (11,-11)
pg.sampler.mom = PG.PtEtaMPhiSampler(pt=[7000, 80000],eta=[-2.5, 2.5])
pg.sampler.pos = PG.PosSampler(x=[-20,20], y=[-20,20], z=PG.GaussianSampler(0, 40))

genSeq += pg
