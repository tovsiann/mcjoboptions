evgenConfig.description = "Single muons with flat pt in [4,100] GeV"
evgenConfig.keywords = ["singleParticle"]
evgenConfig.contact = ["martindl@cern.ch"]
evgenConfig.nEventsPerJob = 10000
evgenConfig.generators += ['ParticleGun']

import ParticleGun as PG
pg = PG.ParticleGun()
pg.sampler.pid = (13,-13)
pg.sampler.mom = PG.PtEtaMPhiSampler(pt=[4000, 100000],eta=[-3.0, 3.0])

genSeq += pg
