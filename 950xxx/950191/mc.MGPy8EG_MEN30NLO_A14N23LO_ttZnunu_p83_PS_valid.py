from MadGraphControl.MadGraphUtils import *

#### Shower
evgenConfig.description = 'aMcAtNlo_ttZqq_NLO'
evgenConfig.keywords+=['SM','ttZ']
evgenConfig.contact = ['chrisg.@cern.ch']
evgenConfig.nEventsPerJob = 1000
evgenConfig.inputFilesPerJob = 1

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")

# Reset to serial processing
check_reset_proc_number(opts)

include("Pythia8_i/Pythia8_aMcAtNlo.py")
evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
