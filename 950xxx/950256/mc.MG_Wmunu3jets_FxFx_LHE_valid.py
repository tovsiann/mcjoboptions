import MadGraphControl.MadGraphUtils
from MadGraphControl.MadGraphUtils import *

#Job bookkeping infos
evgenConfig.description = 'aMcAtNlo Wmunu+3Np FxFx'
evgenConfig.contact = ["francesco.giuli@cern.ch","federico.sforza@cern.ch"]
evgenConfig.keywords += ['W','jets']
evgenConfig.generators += ["aMcAtNlo"]

# General settings
evgenConfig.nEventsPerJob = 200000
nevents = evgenConfig.nEventsPerJob

#Madgraph run card and shower settings
# Shower/merging settings
maxjetflavor=5
parton_shower='PYTHIA8'

gridpack_mode=True

if not is_gen_from_gridpack():
    process = """
    import model loop_sm-no_b_mass
    define p = p b b~
    define j = p
    define mu = mu+ mu-
    define nu = vm vm~   
    generate p p > mu nu [QCD] @0
    add process p p > mu nu j [QCD] @1
    add process p p > mu nu j j [QCD] @2
    add process p p > mu nu j j j [QCD] @3
    output -f"""

    process_dir = str(new_process(process))
else:
    process_dir = str(MADGRAPH_GRIDPACK_LOCATION)

MadGraphControl.MadGraphUtils.MADGRAPH_PDFSETTING={
    'central_pdf':325100, # the lhapf id of the central pdf, see https://lhapdf.hepforge.org/pdfsets - NNDP31 LUXqed
    'pdf_variations':[325100], # list of pdfs ids for which all variations (error sets) will be included as weights - NNDP31 LUXqed
    'alternative_pdfs':[304400,13000,25300,14000,14100,14200,61200,42560,303200], #  NNPDF31 hessian, CT14, MMHT14, CT18, CT18Z, CT18A, HERAPDF2.0, ABMP16 5FS, NNPDF30 hessian
    'scale_variations':[0.5,1,2], # variations of muR and muF wrt the central scale, all combinations of muF and muR will be evaluated
}

#Fetch default run_card.dat and set parameters
settings = {
            'maxjetflavor'  : int(maxjetflavor),
            'parton_shower' : parton_shower,
            'nevents'       : int(nevents),
            'ickkw'         : 3,
            'jetradius'     : 1.0,
            'ptj'           : 8,
            'etaj'          : 10,
        }

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)

input_events=process_dir+'/Events/GridRun_'+str(runArgs.randomSeed)+'/events.lhe.gz'

paramToCopy      = 'param_card.Torrielli.dat'
paramDestination = process_dir+'/Cards/param_card.dat'
paramfile        = subprocess.Popen(['get_files','-data',paramToCopy])
paramfile.wait()

if not os.access(paramToCopy,os.R_OK):
  raise RuntimeError("ERROR: Could not get %s"%(paramToCopy))
shutil.copy(paramToCopy,paramDestination)

generate(process_dir=process_dir,runArgs=runArgs,grid_pack=gridpack_mode)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)
