# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 VBF W-(taunu) production with A14 NNPDF2.3 tune."
evgenConfig.keywords = ["SM", "VBF", "W"]
evgenConfig.contact = ["jan.kretzschmar@cern.ch"]
evgenConfig.nEventsPerJob = 10000

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg VBF_W process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_VBF_W_Common.py")

PowhegConfig.decay_mode = "w- > tau- vt~"
#PowhegConfig.ptj_gencut = 20

PowhegConfig.ncall1 = 2000000
PowhegConfig.ncall2 = 2000000
PowhegConfig.itmx1 = 5
PowhegConfig.itmx2 = 10

PowhegConfig.nubound = 1000000
PowhegConfig.xupbound = 4

# fold (2,2,2) gives <1% neg weights
PowhegConfig.foldphi = 2
PowhegConfig.foldcsi = 2
PowhegConfig.foldy = 2

### default PDFs + NNPDF3.0 replica + a few newer ones
PowhegConfig.PDF             = [260000, 25200, 13165, 90900, 265000, 266000, 303400] # NNPDF30_nlo_as_0118, MMHT2014nlo68clas118, CT14nlo_as_0118, PDF4LHC15_nlo_30, NNPDF30_nlo_as_0117, NNPDF30_nlo_as_0119, NNPDF31_nlo_as_0118
PowhegConfig.PDF.extend(range(260001, 260101)) # Include the NNPDF3.0 error set
PowhegConfig.PDF.extend([303600, 14400, 14000, 14200, 27100, 27400]) # NNPDF31_nnlo_as_0118, CT18NLO, CT18NNLO, CT18ANNLO, MSHT20nlo_as118, MSHT20nnlo_as118

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
genSeq.Pythia8.Commands += ['Powheg:NFinal = 3']
genSeq.Pythia8.Commands += ['SpaceShower:dipoleRecoil = on']
