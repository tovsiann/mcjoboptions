#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = 'POWHEG+Pythia8 ttbar production with ATLAS/CMS Common settings for both Powheg and Pythia8 using Monash CMW tune.'
evgenConfig.keywords    = [ 'SM', 'top', 'ttbar']
evgenConfig.contact     = [ 'dominic.hirschbuehl@cern.ch']
evgenConfig.generators  = [ 'Powheg', 'Pythia8' ]

evgenConfig.nEventsPerJob = 20000

include('PowhegControl/PowhegControl_tt_Common.py')

PowhegConfig.PDF = [303400, 260000, 14400, 27100]  #NNPDF3.1, NNPDF3.0,CT18,MSHT20
PowhegConfig.PDF.extend(range(303401, 303501))


PowhegConfig.hdamp = 250.0

PowhegConfig.decay_mode = "t t~ > all"

PowhegConfig.__setattr__("mass_t", 172.5)
PowhegConfig.__setattr__("width_t", 1.311) #tdec/twidth

PowhegConfig.__setattr__("mass_W", 80.4) #tdec/wmass
PowhegConfig.__setattr__("width_W", 2.085) #tdec/wwidth
PowhegConfig.__setattr__("BR_W_to_enu", 0.1083) #tdec/elbranching
PowhegConfig.__setattr__("sin2cabibbo", 0.051) #tdec/sin2cabibbo

PowhegConfig.__setattr__("mass_e", 0.00051) #tdec/emass
PowhegConfig.__setattr__("mass_mu", 0.1057) #tdec/mumass
PowhegConfig.__setattr__("mass_tau", 1.777) #tdec/taumass

PowhegConfig.__setattr__("mass_d", 0.21) #tdec/dmass
PowhegConfig.__setattr__("mass_u", 0.21) #tdec/umass
PowhegConfig.__setattr__("mass_s", 0.35) #tdec/smass
PowhegConfig.__setattr__("mass_c", 1.525) #tdec/cmass
PowhegConfig.__setattr__("mass_b", 5.06) #tdec/bmass

PowhegConfig.generate()

include("Pythia8_i/Pythia8_MonashCMW_NNPDF23LO_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")


genSeq.Pythia8.Commands += ['POWHEG:veto = 1',
                            'POWHEG:pTdef = 1',
                            'POWHEG:emitted = 0',
                            'POWHEG:pTemt = 0',
                            'POWHEG:pThard = 0',
                            'POWHEG:vetoCount = 100',
                            'POWHEG:nFinal = 2',
                            'POWHEG:veto = 1',
                            'POWHEG:MPIveto = 0',
                           ]

