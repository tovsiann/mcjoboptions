# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 Zmumu production, AZNLO, from existing LHEs, disable Photos y*->ll split"
evgenConfig.keywords = ["SM", "Z", "muon"]
evgenConfig.contact = ["jan.kretzschmar@cern.ch"]
evgenConfig.generators = ["Powheg","Pythia8"]
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1

# --------------------------------------------------------------
# Shower settings
# --------------------------------------------------------------    
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
include('Pythia8_i/Pythia8_Photospp.py')
# this is needed to approximately reproduce low pT(V) in Pythia8.245!
genSeq.Pythia8.Commands += ["BeamRemnants:primordialKThard = 1.4"]

# next level of Photos
genSeq.Photospp.ZMECorrection = True
genSeq.Photospp.WMECorrection = False
genSeq.Photospp.PhotonSplitting = False
genSeq.Photospp.WtInterference = 4.0 # increase Photos upper limit for ME corrections
# probably there's no double-counting of y*->ll splitting
#genSeq.Pythia8.Commands += ["TimeShower:QEDshowerByGamma = off"] # this would disable all y->ffbar
#genSeq.Pythia8.Commands += ["TimeShower:nGammaToLepton = 0"] # this would presumably just disable y->l+l- splitting
