# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 Zmumu production, AZNLO, from existing LHEs, default Pythia8 QED FSR"
evgenConfig.keywords = ["SM", "Z", "muon"]
evgenConfig.contact = ["jan.kretzschmar@cern.ch"]
evgenConfig.generators = ["Powheg","Pythia8"]
evgenConfig.nEventsPerJob = 10000
evgenConfig.inputFilesPerJob = 1

# --------------------------------------------------------------
# Shower settings
# --------------------------------------------------------------    
include('Pythia8_i/Pythia8_AZNLO_CTEQ6L1_EvtGen_Common.py')
# this is needed to approximately reproduce low pT(V) in Pythia8.245!
genSeq.Pythia8.Commands += ["BeamRemnants:primordialKThard = 1.4"]
