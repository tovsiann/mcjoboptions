#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.description = "POWHEG+Pythia8 dib production with A14 NNPDF2.3 tune, 2mu filter"
evgenConfig.keywords = ["SM", "QCD", "jets", "2jet", "bbbar", "bottom"]
evgenConfig.contact = ["jan.kretzschmar@cern.ch"]
evgenConfig.generators = ["Powheg", "Pythia8"]

# Born suppression factor 25 GeV
# Filter Efficiency = 0.000925
# Weighted Filter Efficiency = 0.000006

evgenConfig.nEventsPerJob = 1000
filterMulti = 35 # put more into the LHE file
lheMulti = 35    # reshower+filter each LHE multiple times

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg jj process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_bb_Common.py")
# switch to ccbar mode!
PowhegConfig.mass_b = 1.55


# Set Born suppression factor to about ~2*minMuonPt*5
PowhegConfig.bornsuppfact = 25

PowhegConfig.mu_F         = 1.0
PowhegConfig.mu_R         = 1.0
PowhegConfig.PDF          = 13000 # CT14nlo central only for testing

# use default factorisation+renormalisation scales
# PowhegConfig.mu_F         = [1.0, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0] # List of factorisation scales which pairs with renormalisation scale below
# PowhegConfig.mu_R         = [1.0, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5] # List of renormalisation scales
#PowhegConfig.PDF          = [14000, 304400, 25300, 14200, 14100, 14300, 14400, 25100, 91500, 42560, 317500, 303600, 319300, 319500, 322500, 322700, 322900, 323100, 323300, 323500, 323700, 323900, 27400, 331500]
PowhegConfig.rwl_group_events = 100000

PowhegConfig.foldcsi      = 5
PowhegConfig.foldphi      = 5
PowhegConfig.foldy        = 5
PowhegConfig.ncall1       = 100000
PowhegConfig.ncall2       = 100000
PowhegConfig.nubound      = 1000000
PowhegConfig.itmx1        = 5
PowhegConfig.itmx2        = 5

#PowhegConfig.storemintupb = 0 # smaller grids

PowhegConfig.nEvents = runArgs.maxEvents*filterMulti if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob*filterMulti

# full PDF set variations cost a lot of time
#PowhegConfig.PDF.extend(range(14001, 14059))                            # Include the CT18NNLO error set
#PowhegConfig.PDF.extend(range(304401, 304501))                          # Include the NNPDF31_nnlo_as_0118_hessian error set
#PowhegConfig.PDF.extend(range(27400, 27465))                            # Include the MSHT20nnlo_as118 error set
# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.generate()

############################################################
# probably want to duplicate LHE file here for 'reshowering'
############################################################

include("merge_LHE.py")
import glob, os
inputLHE = glob.glob('*.events')
copies = lheMulti * inputLHE
outputlhe = "replicatedLHE.lhe.events"
merge_lhe_files(copies,outputlhe)
# change LHE file under foot to satisfy Gen_tf&logParser
# this is not totally clean code, as it depends on exactly one file being replicated
os.remove(inputLHE[0])
os.rename(outputlhe, inputLHE[0])


#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune, main31 routine
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")

# faster?
genSeq.Pythia8.Commands += ["UncertaintyBands:doVariations=off"]

# muon filter
include("GeneratorFilters/MultiMuonFilter.py")
MultiMuonFilter = filtSeq.MultiMuonFilter
MultiMuonFilter.Ptcut = 3500.
MultiMuonFilter.Etacut = 2.7
MultiMuonFilter.NMuons = 2
