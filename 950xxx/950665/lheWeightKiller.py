import os, glob, math

# set minWgt and maxWgt before including this fragment
# minWgt = 0.
# maxWgt = 1E9

nKilled = 0
nEvent = 0
sumWgt = 0.
sumWgt2 = 0.
sumWgt_cleaned = 0.
sumWgt2_cleaned = 0.

for infile in glob.glob("*.events"):
    print ("Killing LHE events with abs(weight) outside {:e} -- {:e} in file {}".format(minWgt, maxWgt, infile))

    f1 = open(infile)
    newfile = infile+'.temp'
    f2 = open(newfile,'w')

    startEvent = False
    inEvent = False
    wgt = 0.
    eventBuffer = []
    
    for line in f1:
        if line.startswith('<event'):
            # flag new event, buffer, go to next line
            inEvent = True
            startEvent = True
            nEvent += 1
            eventBuffer += [line]
            continue

        if startEvent:
            # continue buffering the event, record weight, go to next line
            wgt = float(line.split()[2])
            sumWgt += wgt
            sumWgt2 += wgt**2
            eventBuffer += [line]
            startEvent = False
            continue

        if inEvent and not line.startswith('</event'):
            # buffer
            eventBuffer += [line]
            continue

        if inEvent and line.startswith('</event'):
            # flag end event, buffer
            inEvent = False
            eventBuffer += [line]

            if minWgt <= abs(wgt) <= maxWgt:
                # write event out
                f2.write(''.join(eventBuffer))
                sumWgt_cleaned += wgt
                sumWgt2_cleaned += wgt**2
            else:
                print("Killing the following event")
                print(''.join(eventBuffer))
                nKilled += 1

            # clear event buffer
            eventBuffer = []
            wgt = 0.
            inEvent = False
            continue

        # this will write any other lines
        f2.write(line)


    f1.close()
    f2.close()
    os.system('rm %s' % (infile) )
    os.system('mv %s %s' % (newfile, infile) )

print ("I killed {} events out of {} in total".format(nKilled, nEvent))
print ("All events: sumWgt {:e} +- {:e}".format(sumWgt, math.sqrt((sumWgt2-sumWgt**2/nEvent)/nEvent)))
print ("Cleaned events: sumWgt {:e} +- {:e}".format(sumWgt_cleaned, math.sqrt((sumWgt2_cleaned-sumWgt_cleaned**2/(nEvent-nKilled))/(nEvent-nKilled))))
meanWgt = sumWgt/nEvent
meanWgt_cleaned = sumWgt_cleaned/(nEvent-nKilled)
print ("sumWgt/nEvent: before {:e}, after {:e}".format(meanWgt, meanWgt_cleaned))
