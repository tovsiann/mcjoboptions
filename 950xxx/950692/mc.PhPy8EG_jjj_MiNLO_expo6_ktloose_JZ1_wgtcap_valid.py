minWgt = 0.
maxWgt = 3E14
include("lheWeightKiller.py")
#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.generators += ["Powheg", "Pythia8"]
evgenConfig.description = "POWHEG+Pythia8 trijet MiNLO production with A14 NNPDF2.3 tune."
evgenConfig.keywords = ["SM", "QCD", "jets", "3jet"]
evgenConfig.contact = ["saad.el.farkh@cern.ch", "jan.kretzschmar@cern.ch"]
# 1 LHE file = 2000 events, filteff = 0.825627 (weighted 0.046758)
evgenConfig.nEventsPerJob = 20000
evgenConfig.inputFilesPerJob = 14

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
#include("Pythia8_i/Pythia8_ShowerWeights.py")

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.6)
AddJetsFilter(filtSeq, runArgs.ecmEnergy, 0.6)
include("GeneratorFilters/JetFilter_JZX.py")

# customize pT cut to push a bit below the usual 20 GeV cut of JZ1
from AthenaCommon.SystemOfUnits import GeV
filtSeq.QCDTruthJetFilter.MinPt = 15.0*GeV
filtSeq.QCDTruthJetFilter.MaxPt = maxDict[1]*GeV
