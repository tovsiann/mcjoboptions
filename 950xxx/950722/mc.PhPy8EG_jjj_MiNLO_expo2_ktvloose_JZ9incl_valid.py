#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------
evgenConfig.generators += ["Powheg", "Pythia8"]
evgenConfig.description = "POWHEG+Pythia8 trijet MiNLO production with A14 NNPDF2.3 tune."
evgenConfig.keywords = ["SM", "QCD", "jets", "3jet"]
evgenConfig.contact = ["saad.el.farkh@cern.ch", "jan.kretzschmar@cern.ch"]
# 1 LHE file = 100000 events, filteff = 0.007233 (weighted 0.000692)
evgenConfig.nEventsPerJob = 2000
evgenConfig.inputFilesPerJob = 3

#--------------------------------------------------------------
# Pythia8 showering with the A14 NNPDF2.3 tune
#--------------------------------------------------------------
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_Powheg_Main31.py")
#include("Pythia8_i/Pythia8_ShowerWeights.py")

include("GeneratorFilters/FindJets.py")
CreateJets(prefiltSeq, 0.6)
AddJetsFilter(filtSeq, runArgs.ecmEnergy, 0.6)
include("GeneratorFilters/JetFilter_JZX.py")
JZSlice(9, filtSeq)
from AthenaCommon.SystemOfUnits import GeV
filtSeq.QCDTruthJetFilter.MaxPt = maxDict[12]*GeV
