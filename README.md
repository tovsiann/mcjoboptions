ATLAS MC JobOptions
====================

For any technical issues contact [atlas-phys-mcprod-jo@cern.ch](mailto:atlas-phys-mcprod-jo@cern.ch)

# Instructions for registering new jobOptions (MC contacts)

**IMPORTANT: DO NOT FORK THE REPO - SECRET VARIABLES NEEDED IN THE CI WILL NOT BE AVAILABLE IN FORKS AND THE CI CAN FAIL - ONLY USE BRANCHES**

For detailed step-by-step instructions on how to request a Monte Carlo sample, 
please see [McSampleRequestProcedure](https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/McSampleRequestProcedure).

Before uploading new jO files to the repository, you need to make sure that you have tested the jO locally with `Gen_tf.py` and have included the `log.generate` produced by `Gen_tf.py` in the DSID directories you want to upload.  

**Important: if `log.generate` files are not provided, merge requests will in general not be accepted!**  

Once you have made sure that your jO run, the request has been discussed in an `ATLMCPROD` JIRA ticket and the relevant physics group is happy with the physics output, you can follow the next steps in order to
upload the jO to the repository.  

The repository is copied to `/cvmfs/atlas.cern.ch/repo/sw/Generators/MCJobOptions` every 4 hours starting at 00.00 CERN time.

## Sparse checkout of `mcjoboptions` repo

Instead of cloning the whole repository it is possible to do a sparse checkout using the following instructions.

#### Recommended (requires git 2.26 or higher)

```
mkdir mcjoboptions
cd mcjoboptions
git init
git remote add -f origin ssh://git@gitlab.cern.ch:7999/atlas-physics/pmg/mcjoboptions.git
git sparse-checkout init
git sparse-checkout set common scripts .gitignore
git pull origin master
```

#### Alternative (for git versions lower than 2.26)

```
mkdir mcjoboptions
cd mcjoboptions
git init
git remote add -f origin ssh://git@gitlab.cern.ch:7999/atlas-physics/pmg/mcjoboptions.git
git config core.sparseCheckout true
echo scripts > .git/info/sparse-checkout
echo common >> .git/info/sparse-checkout
echo .gitignore >> .git/info/sparse-checkout
git pull origin master
```

## Using the automatic commit script

The procedure describe below makes use of the automatic commit script `scripts/commit_new_dsid`, which contains several options. 
To get a list of options and their explanation run  
```
./scripts/commit_new_dsid -h
```

## Steps for booking DSIDs and uploading new jO

1.  For each jO you want to upload, prepare a dummy DSID directory outside the `500xxx-999xxx` range, 
(e.g. `100xxx/100000`) and add all the necessary files (jO, gridpacks, etc) in the directory. To upload the directories
to git it is **strongly recommended** that you use the automatic commit script `commit_new_dsid` as described below.

2.  **Dry run of automatic commit script** . From the `mcjoboptions` directory run the script using the `--dry-run` flag:
```
./scripts/commit_new_dsid -m "Commit message" --dry-run /path/with/DSIDs/*
```  
or  
```
./scripts/commit_new_dsid -d DSID1,DSID2,DSIDn-DSIDk -m "Commit message" --dry-run
```
There are three ways to specify which DSID directories to commit:  
1.  using a comma-separated list of DSID directories with the `-d` option:  e.g. `-d 100000,100001`  
2.  using a continuous range of DSID directories with the `-d` option: e.g. `-d 100000-100005`
3.  directly providinng the paths to the directories where the jO files are stored, e.g. `./scripts/commit_new_dsid /path/with/DSIDs/*`.  
The last option should expand to a **list of directories** containing the jO files, not the files themselves.

The script will loop through the directories and 
*  check if the jO files in the DSID directories specified with the `-d` flag contain any naming errors. If they do, it will print out the ERRORs. The user should fix them and rerun the command above.
*  the script will identify the generator used in each jO and will search for the smallest continuous block of DSIDs that is not already used in any of the remote branches. It will then print out where each dummy DSID will be copied
(e.g. `Will move 999xxx/999995 to 800xxx/800045`)
*  locate the `log.generate` and check if there are no errors from the `logParser`. 
  If errors are found, you will not be allowed to commit to the repository and you will first have to fix the errors in your local jO file.  
  If no errors are found, the script will create a file called `log.generate.short` in every DSID directory you want to upload.  

If `logParser` gives an ERROR due to low or high CPU time:
   - discuss with the requester if the requested number of events can be adjusted so that the job takes ideally 6-12 hours (or at the very least longer than 1h and less than 18h)
   - if it is not possible to adjust the number of events (e.g. showering job running on few input LHE) write to the JIRA ticket and ask the conveners if it is ok to ignore this error
   - if the answer to the above is yes, then run the commit script adding the `-u` option

**Before proceeding to the next step**:
*  **make sure that the list of DSIDs that the script suggested make sense to you**. If you are not sure contact the mailing list.
*  **check the list of files which will be added to the commit and make sure that it is as expected.** If not contact the mailing list.

3. **Assignment of DSIDs**: Assuming the dry run (step 2) was successful, run the commit script using the `-n` or `--nogit` flag:  
```
./scripts/commit_new_dsid -d DSID1,DSID2,DSIDn-DSIDk -m "Commit message" -n
```  
This will go through the same steps as step (2) above with the difference that now the **dummy DSIDs will actually be copied to their final location**. At the end the script should print out where each initial DSID
has been copied and it will suggest the command to use in order to commit the DSIDs to the repository, e.g.  
```
The following DSIDs have been assigned:
    999xxx/999995 -> 800xxx/800045
    999xxx/999996 -> 800xxx/800046
Run: ./scripts/commit_new_dsid -d 800045-800046 -m "Commit message" to push them to git
```

4.  **Upload to git**: run the script as suggested by the previous command  
```
./scripts/commit_new_dsid -d 800045-800046 -m "Commit message"
```  
This will upload all the necessary files to git in a new branch called `dsid_USER_800045`. If you want to specify a custom branch name you can use `-b=branchName` when running the script.  

5.  A CI pipeline will be launched with this commit, as described in the pipelines section below. **Make sure that the pipelines run and are successful!** If they don't run please contact the package maintainers. If they fail, look into the pipeline output to see why they fail and update your MR accordingly. If it's unclear why they fail, contact the package maintainers.  

6.  Create a merge request [here](https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/merge_requests) . The source branch should be the one created in step (2) above (e.g. `dsid_USER_DSID1`) and the target branch should be `atlas-physics/pmg/mcjoboptions/master`  

7.  Select the `new_jO` template in the title drop menu and fill in the description

## Skipping CI jobs

**This is in general discouraged and should only be performed by expert users. MR with skipped CI jobs will in general not be accepted!!!**  

To skip certain CI jobs run the commit script adding the `-m="[skip X],[skip Y]"` option, where `X,Y=modfiles,athena,logparser`, for example to commit the directory 800045 skipping the checks for modified files and the athena running do   
```
./scripts/commit_new_dsid -d 800045 -m "Adding 800045 [skip modfiles] [skip athena]"
```

## Automatic creation and merging of MR (only to be used for active learning jO!)

To commit jobOptions that can be merged automatically to the master branch follow the instructions above and add `-a` when running the commit script, e.g.
```
./scripts/commit_new_dsid -a -d 800045 -m "Adding jO for process X"
```
This will create a branch called `dsid_auto_USERNAME_800045` on which the `create_mr` CI job will run. This job will create a new MR to the master branch.
In this MR the `merge_mr` CI job will run and will automatically approve and merge the MR into the master branch.

**NB: this feature is still under development. For any hickups open an issue and assign to @sargyrop**

# Accepting a MR with new jobOptions

Before accepting a MR make sure that all the items below have been completed successfully:  

* All the items in the todo list of the MR have been completed successfully
* The pipeline has run and the status is green
* CI jobs have not been skipped (if jobs have been skipped, confirmation from the PMG conveners is necessary before merging). 
* Look at the output of the `run_athena` pipeline and make sure that it has run for one of the DSIDs added to the commit. If it's not the case, confirmation from the PMG conveners is necessary before merging
* Check that no `log.generate.short` files are included in the commit to be merged to master. If such files are present, it indicates that something went wrong in the pipeline. Check the job logs and contact the package maintainers if needed.

# Instructions for developers

To push new changes to the repository follow the next steps:

1.  Check out the master branch and **before changing any file from the master** create a new branch with `git fetch --all && git checkout -b myBranch origin/master`  

2.  Add your changes with `git add ...`  

3.  Finally commit using an appropriate git message, e.g.  `git commit -m "appropriate message [skip modfiles]"` if you have modified pre-existing files residing in the DSID directories. Don't forget to push your changes using `git push origin myBranch`.  

4.  Create a merge request [here](https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/merge_requests) . The source branch should be the one created in step (1) above (e.g. `myBranch`) and the target branch should be `atlas-physics/pmg/mcjoboptions/master`. Explain what your MR does.  

5.  **Make sure that an appropriate message is picked up with the correct CI tag** (by default the squashed commit message will coincide with the title of the MR, so if you want to skip certain CI jobs, you might need to add these in the squashed commit message by hand, if the title of the MR does not already contain the tag) 

## New password for `mcgensvc` account

When a new log-in password for `mcgensvc` is set, you need to update the value `K8S_SECRET_SERVICE_PASSWORD` in Settings > CI/CD > Variables.

## New grid certificate for `mcgensvc` account

When a new grid certificate is requested for `mcgensvc`:

1.  Encode the value of `ucercert.pem` with `base64 usercert.pem` and update the value `GRID_USERCERT` in Settings > CI/CD > Variables.
2.  Encode the value of `userkey.pem` with `base64 userkey.pem` and update the value `GRID_USERKEY` in Settings > CI/CD > Variables.
3.  The password that was used in the generation of `userkey.pem` should be copied to `GRID_PW` in Settings > CI/CD > Variables.
4.  Potentially the `export RUCIO_ACCOUNT` might need to be changed in `scripts/run_athena.sh` to match the person who requested the certificate.

# Pipelines

This repository uses CI pipelines that run **only when a MR is open**. 

A brief description of the CI jobs is given below. Skipping pipelines is an expert operation restricted to the package maintainers. 

**NB: No MR will be approved if pipelines have been skipped!**

1.  `check_jo_consistency`: 
*  **checks**: naming convention for jO files, length of jO file name, DSID ranges
*  **skippable**: NO  

2.    `check_unique_physicsShort`:
* **checks**: that there is no other jO file with the same physics short in the repository
* **skippable**: NO

3.    `check_unique_file`:
* **checks**: that there is no other python file with exactly the same name in the repository. If the job fails the requester should check if the files have the same content and if so try to rename the files if possible.
* **skippable**: NO

4.    `check_modified_files`:
* **checks**: that no pre-existing files have been modified or deleted (excluding files listed in `scripts/modifiable.txt`) and that no files have been added in pre-existing DSID directories
* **skippable**: with `[skip modfiles]`

5.    `notify_changes`:
* **checks**: runs only when `[skip modfiles]` is added to the commit. It checks if the modified files are in pre-existing DSID directories and reassigns the MR to the conveners. **Do not merge the MR by yourself if this happens**. The job also checks if there are EVNT files registered on rucio with the same DSID as the jO that are being registered and if it finds EVNT files the CI job returns a failure. In this case existing EVNTs have to be deprecated before the MR can be merged. 
* **skippable**: NO

6.  `check_added_files:`
* **checks**: that the files added are jO files (named like `mc.*.py`), integration grids (named like `*.GRID.tar.gz`) or directories that contain control files for generators (these have to be named after the generator)
* **skippable**: NO

7.    `check_grid_readable`:
* **checks**: that the GRID files have been put on `/eos/atlas` or `/eos/user` in a directory that is readable by `atlcvmfs`, `mcgensvc` and if the files are on `cvmfs` that they are readable by all users. 
* **skippable**: NO

8.    `check_grid_nfiles`:
* **checks**: that the number of files in the gridpack are less than 80k
* **skippable**: with `[skip nfiles]`

9.    `check_grid_size`:
* **checks**: that the GRID file size is less than 100 MB
* **skippable**:NO (but the job is allowed to fail)

10. `setup_athena`:
* **checks**: if new jO are added in a commit with a `log.generate.short` file  in the respective DSID directory, this job will create the configuration for a dynamic child pipeline that will consist of the `run_athena_DSID` and 
`check_logParser_DSID` jobs described below. A maximum of 5 jobs are created. If there are no new jO committed, a `dummy` job will be created in the child pipeline, which will always succeed.
* **skippable**: with `[skip athena]`

11. `run_athena_DSID`:
* **checks**: that `Gen_tf.py` can run successfully with the `DSID` file. `Gen_tf.py` will run **only if `log.generate.short` is provided with the commit**. 
* **skippable**: with `[skip athena]`

12. `check_logParser_DSID`:
* **checks**: that the `run_athena_DSID` job produced no error
* **skippable**: with `[skip logparser]`. The job is also not run when `[skip athena]` is used

13. `remove_logs`:
* **checks**: if `log.generate.short` files are present in the branch it will remove them
* **skippable**: NO

14. `create_mr`:
* creates a new MR from the branch where the CI is running (this happens only if all CI jobs of the previous stages have succeeded)
* runs only when commit message contains the keyword `[auto]`

15. `merge_mr`:
* approves and merges open MR
* runs only as a merge request pipeline when the source branch name starts with `dsid_auto`
