#!/bin/bash

# For coloured output
. scripts/helpers.sh

# Make sure that if a command fails the script will continue
set +e

# Directory for which to run athena
dir=$1
printInfo "INFO: Will run logParser for $dir"

# There should be only 1 file called log.generate.short in each of the DSID directory
# This should have been uploaded by the user (running scripts/commit_new_dsid.sh locally)
if [ ! -f $dir/log.generate.short ] ; then
  printWarning "WARNING: No log.generate.short in $dir"
  printWarning "athena running was skipped for this directory. Make sure that the requesters have tested the jO localy!!!"
  return 0
else
  printGood "OK: log.generate.short found"
fi

# There should be only 1 file called log.generate_ci in each of these directories
# This should have been produced as an artifact by the run_athena CI job
if [ ! -f $dir/log.generate_ci ] ; then
  # log.generate_ci might be missing if athena running has been skipped in the previous iteration
  # due to the presence of external LHE or because of an unsupported release
  inputGeneratorFile=$(grep inputGeneratorFile $dir/log.generate.short)
  minorRel=$(grep 'using release' $dir/log.generate.short | awk '{print $NF}' | awk 'BEGIN {FS="-"} ; {print $2}' | awk 'BEGIN {FS="."} ; {print $3}')
  if [[ ! -z $inputGeneratorFile ]] ; then
    printInfo "INFO: athena running has been skipped in $dir since jO uses an external LHE file"
    return 0
  else
    printError "ERROR: No log.generate_ci in $dir."
    printError "The run_athena CI job must have failed. Please check output of run_athena CI job."
    return 1
  fi
else
  printGood "OK: log.generate_ci found"
fi

printInfo "Running: python scripts/logParser.py -i $dir/log.generate_ci -j $joFile -c -u ..."

# Check output
joFile=$(ls $dir/mc.*.py)
python scripts/logParser.py -i $dir/log.generate_ci -j $joFile -u | tee log.generate_ci_out
if [ "${PIPESTATUS[0]}" != "0" ] ; then
  printError "ERROR: logParser execution failed. This usually implies that $dir/log.generate_ci is malformatted or there is a run-time error from logParser."
  printError "Check the output of the previous CI job and the content of log.generate_ci (in the artifacts of this job)."
  return 1
fi

# If no error: remove log.generate file from repository
errors=$(grep -E 'Errors.*Warnings' log.generate_ci_out | awk '{printf("%d\n"),$3}')
warnings=$(grep -E 'Errors.*Warnings' log.generate_ci_out | awk '{printf("%d\n"),$7}')
rm -f log.generate_ci_out
if (( errors > 0 )) ; then
  printError "ERROR: $dir/log.generate_ci contains errors"
  return 1
else
  if (( warnings > 0 )) ; then
    printWarning "WARNING: $dir/log.generate_ci contains warnings"
    return 2
  else
    printGood "OK: logParser found no errors."
  fi
fi

return 0
