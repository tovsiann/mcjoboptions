#!/bin/bash

# For coloured output
. scripts/helpers.sh

# Make sure that if a command fails the script will continue
set +e

printInfo "Find files that have been modified, copied, renamed..."
pass=true

# First get any changes from master
git pull origin master >& /dev/null
if [ $? -ne 0 ] ; then
  printError "ERROR: the branch contains changes that are in conflict with master"
  exit 1
fi

# Find number of files that have been:
# Copied (C), Deleted (D), Modified (M), Renamed (R), have their type
# (i.e. regular file, symlink, submodule, …​) changed (T), are Unmerged (U),
# are Unknown (X), or have had their pairing Broken (B).
nchanged=$(git diff --name-status origin/master..HEAD --diff-filter=MDCRTUXB --find-renames | grep -v -f scripts/modifiable.txt | wc -l)

if [ $nchanged -eq 0 ] ; then
  printGood "OK: No file modified in DSID or common directories."
else
   printError "ERROR: Your commit has modified or deleted pre-existing files: "
   git diff --name-status origin/master..HEAD --diff-filter=MDCRTUXB --find-renames | grep -v -f scripts/modifiable.txt
   printError "This is not allowed!"
   pass=false
fi

if [ "$pass" != true  ] ; then
  exit 1
fi

exit 0
