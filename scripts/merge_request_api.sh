#!/usr/bin/env bash

# For coloured output
. scripts/helpers.sh

##############
# Project info
##############
PROJECT_ID=62692
PROJECT_URL=https://gitlab.cern.ch/api/v4/projects
TARGET_BRANCH=master

##############
# Functions for API
##############
LISTMR=`curl --silent --header "PRIVATE-TOKEN: $CI_UPLOAD_TOKEN" \
        "$PROJECT_URL/$PROJECT_ID/merge_requests/?author_username=mcgensvc&state=opened&labels=auto&source_branch=$CI_COMMIT_REF_ID"`
COUNTMR=`echo ${LISTMR} | grep -o "\"source_branch\":\"${CI_COMMIT_REF_NAME}\"" | wc -l`
    
open_new_MR() {
    # Check how many open MRs there are for the source branch
    printInfo "There are: $COUNTMR open MR for $CI_COMMIT_REF_NAME"
    
    # The curl request for opening a MR
    BODY="{
        \"id\": ${PROJECT_ID},
        \"source_branch\": \"${CI_COMMIT_REF_NAME}\",
        \"target_branch\": \"${TARGET_BRANCH}\",
        \"remove_source_branch\": true,
        \"title\": \"Automatic merge: ${CI_COMMIT_REF_NAME}\",
        \"labels\":[\"auto\",\"jobOptions\"]
    }";
    
    
    if [ ${COUNTMR} -eq "0" ]; then
        curl --request POST \
            --header "PRIVATE-TOKEN: ${CI_UPLOAD_TOKEN}" \
            --header "Content-Type: application/json" \
            --data "${BODY}" \
            "${PROJECT_URL}/${PROJECT_ID}/merge_requests";
        printGood "\tOpened a new merge request";
    else
        printError "No new MR created - the following open MR exist for the branch $CI_COMMIT_REF_NAME"
        for i in `seq 0 $((COUNTMR-1))` ; do
            echo $LISTMR | jq -c -r ".[$i].iid"
            exit 1
        done
    fi
}

list_approve_merge_MR() {
    # Make sure there is only 1 open MR for this branch
    if [ ${COUNTMR} -ne "1" ]; then
        printError "There are $COUNTMR open MR for $CI_COMMIT_REF_NAME - this indicates an error somewhere upstream"
        exit 1
    else
        iid=`echo $LISTMR | jq -c -r ".[0].iid"`
    fi
    
    # Get number of approvals left
    approvals_left=`curl --silent --header "PRIVATE-TOKEN: $CI_UPLOAD_TOKEN" \
                    "$PROJECT_URL/$PROJECT_ID/merge_requests/$iid/approvals" | jq -c -r ".approvals_left"`
    printInfo "MR: $iid - approvals left: $approvals_left"
    
    # approve MR
    if $approve; then
        printInfo "Approving $iid"
        if [ ${approvals_left} -gt "0" ] ; then
            curl --silent --request POST \
                 --header "PRIVATE-TOKEN: ${CI_UPLOAD_TOKEN}" \
                 "${PROJECT_URL}/${PROJECT_ID}/merge_requests/$iid/approve";
        fi
    fi
    
    # merge MR
    if $merge ; then
        BODY="{
            \"id\": ${PROJECT_ID},
            \"squash\": true,
            \"should_remove_source_branch\": true,
            \"force_remove_source_branch\": true,
            \"merge_request_iid\": \"${iid}\",
            \"merge_when_pipeline_succeeds\": true
        }";
    
        printInfo "Toggling merge_when_pipeline_succeeds for $iid"
        curl --silent --request PUT \
             --header "PRIVATE-TOKEN: ${CI_UPLOAD_TOKEN}" \
             --header "Content-Type: application/json" \
             --data "${BODY}" \
             "${PROJECT_URL}/${PROJECT_ID}/merge_requests/$iid/merge";
    fi
}

##############
# Main script
##############
merge=false
approve=false
list=false
open=false
for opt in $@ ; do
    case $opt in
      -a|--approve)
        approve=true
        list=true ;;
      -m|--merge)
        merge=true
        list=true ;;
      -l|--list)
        list=true ;;
      -o|--open)
        open=true ;;
      *)
        ;;
    esac
done

export PATH=$PWD:$PATH

if $list; then
  list_approve_merge_MR
elif $open ; then
  open_new_MR
fi

